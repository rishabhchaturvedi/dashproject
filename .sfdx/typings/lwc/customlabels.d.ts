declare module "@salesforce/label/c.Count_for_Complaint" {
    var Count_for_Complaint: string;
    export default Count_for_Complaint;
}
declare module "@salesforce/label/c.Count_for_Query_or_Request" {
    var Count_for_Query_or_Request: string;
    export default Count_for_Query_or_Request;
}
declare module "@salesforce/label/c.Customer_Portal_Link" {
    var Customer_Portal_Link: string;
    export default Customer_Portal_Link;
}
declare module "@salesforce/label/c.Customer_portal_link_here" {
    var Customer_portal_link_here: string;
    export default Customer_portal_link_here;
}
declare module "@salesforce/label/c.DepositComplaint" {
    var DepositComplaint: string;
    export default DepositComplaint;
}
declare module "@salesforce/label/c.Deposit_Statuary_Escalation_L2_Owner" {
    var Deposit_Statuary_Escalation_L2_Owner: string;
    export default Deposit_Statuary_Escalation_L2_Owner;
}
declare module "@salesforce/label/c.HDFC_Bank_Support_Domain" {
    var HDFC_Bank_Support_Domain: string;
    export default HDFC_Bank_Support_Domain;
}
declare module "@salesforce/label/c.HDFC_DASH_Active_Cases_Limit" {
    var HDFC_DASH_Active_Cases_Limit: string;
    export default HDFC_DASH_Active_Cases_Limit;
}
declare module "@salesforce/label/c.HDFC_DASH_CIBIL_Score_Check_Assignment" {
    var HDFC_DASH_CIBIL_Score_Check_Assignment: string;
    export default HDFC_DASH_CIBIL_Score_Check_Assignment;
}
declare module "@salesforce/label/c.HDFC_DASH_Case_Ageing" {
    var HDFC_DASH_Case_Ageing: string;
    export default HDFC_DASH_Case_Ageing;
}
declare module "@salesforce/label/c.HDFC_DASH_Case_Ageing_Bank_Domain" {
    var HDFC_DASH_Case_Ageing_Bank_Domain: string;
    export default HDFC_DASH_Case_Ageing_Bank_Domain;
}
declare module "@salesforce/label/c.HDFC_DASH_Case_Ageing_E2C" {
    var HDFC_DASH_Case_Ageing_E2C: string;
    export default HDFC_DASH_Case_Ageing_E2C;
}
declare module "@salesforce/label/c.HDFC_DASH_Case_Ageing_Existing_Case" {
    var HDFC_DASH_Case_Ageing_Existing_Case: string;
    export default HDFC_DASH_Case_Ageing_Existing_Case;
}
declare module "@salesforce/label/c.HDFC_DASH_Case_Ageing_for_Resolved_Status" {
    var HDFC_DASH_Case_Ageing_for_Resolved_Status: string;
    export default HDFC_DASH_Case_Ageing_for_Resolved_Status;
}
declare module "@salesforce/label/c.HDFC_DASH_Case_Origin_for_E2C" {
    var HDFC_DASH_Case_Origin_for_E2C: string;
    export default HDFC_DASH_Case_Origin_for_E2C;
}
declare module "@salesforce/label/c.HDFC_DASH_Check_Reply_Mail" {
    var HDFC_DASH_Check_Reply_Mail: string;
    export default HDFC_DASH_Check_Reply_Mail;
}
declare module "@salesforce/label/c.HDFC_DASH_Deposit_Number_Pattern" {
    var HDFC_DASH_Deposit_Number_Pattern: string;
    export default HDFC_DASH_Deposit_Number_Pattern;
}
declare module "@salesforce/label/c.HDFC_DASH_ERROR_STATUS_CODE" {
    var HDFC_DASH_ERROR_STATUS_CODE: string;
    export default HDFC_DASH_ERROR_STATUS_CODE;
}
declare module "@salesforce/label/c.HDFC_DASH_File_No_Bank_Domain" {
    var HDFC_DASH_File_No_Bank_Domain: string;
    export default HDFC_DASH_File_No_Bank_Domain;
}
declare module "@salesforce/label/c.HDFC_DASH_File_No_Pattern" {
    var HDFC_DASH_File_No_Pattern: string;
    export default HDFC_DASH_File_No_Pattern;
}
declare module "@salesforce/label/c.HDFC_DASH_File_Number_Length" {
    var HDFC_DASH_File_Number_Length: string;
    export default HDFC_DASH_File_Number_Length;
}
declare module "@salesforce/label/c.HDFC_DASH_File_Stages_for_ROI_Request" {
    var HDFC_DASH_File_Stages_for_ROI_Request: string;
    export default HDFC_DASH_File_Stages_for_ROI_Request;
}
declare module "@salesforce/label/c.HDFC_DASH_Forwarded_Email_String" {
    var HDFC_DASH_Forwarded_Email_String: string;
    export default HDFC_DASH_Forwarded_Email_String;
}
declare module "@salesforce/label/c.HDFC_DASH_Forwarding_Email_Domain" {
    var HDFC_DASH_Forwarding_Email_Domain: string;
    export default HDFC_DASH_Forwarding_Email_Domain;
}
declare module "@salesforce/label/c.HDFC_DASH_HDB_Number" {
    var HDFC_DASH_HDB_Number: string;
    export default HDFC_DASH_HDB_Number;
}
declare module "@salesforce/label/c.HDFC_DASH_HDB_Number_Length" {
    var HDFC_DASH_HDB_Number_Length: string;
    export default HDFC_DASH_HDB_Number_Length;
}
declare module "@salesforce/label/c.HDFC_DASH_HDB_Number_Pattern" {
    var HDFC_DASH_HDB_Number_Pattern: string;
    export default HDFC_DASH_HDB_Number_Pattern;
}
declare module "@salesforce/label/c.HDFC_DASH_HDFC_Bank_Domain" {
    var HDFC_DASH_HDFC_Bank_Domain: string;
    export default HDFC_DASH_HDFC_Bank_Domain;
}
declare module "@salesforce/label/c.HDFC_DASH_Hub_Queue" {
    var HDFC_DASH_Hub_Queue: string;
    export default HDFC_DASH_Hub_Queue;
}
declare module "@salesforce/label/c.HDFC_DASH_Integration_User" {
    var HDFC_DASH_Integration_User: string;
    export default HDFC_DASH_Integration_User;
}
declare module "@salesforce/label/c.HDFC_DASH_Portal_Link" {
    var HDFC_DASH_Portal_Link: string;
    export default HDFC_DASH_Portal_Link;
}
declare module "@salesforce/label/c.HDFC_DASH_ProcessExcepFlag" {
    var HDFC_DASH_ProcessExcepFlag: string;
    export default HDFC_DASH_ProcessExcepFlag;
}
declare module "@salesforce/label/c.HDFC_DASH_ROI_Conversion_String" {
    var HDFC_DASH_ROI_Conversion_String: string;
    export default HDFC_DASH_ROI_Conversion_String;
}
declare module "@salesforce/label/c.HDFC_DASH_Status_Duplicate_Case" {
    var HDFC_DASH_Status_Duplicate_Case: string;
    export default HDFC_DASH_Status_Duplicate_Case;
}
declare module "@salesforce/label/c.HDFC_DASH_Status_New" {
    var HDFC_DASH_Status_New: string;
    export default HDFC_DASH_Status_New;
}
declare module "@salesforce/label/c.HDFC_DASH_Status_Pending" {
    var HDFC_DASH_Status_Pending: string;
    export default HDFC_DASH_Status_Pending;
}
declare module "@salesforce/label/c.HDFC_DASH_Unregistered_Mail_Status" {
    var HDFC_DASH_Unregistered_Mail_Status: string;
    export default HDFC_DASH_Unregistered_Mail_Status;
}
declare module "@salesforce/label/c.HDFC_Domain" {
    var HDFC_Domain: string;
    export default HDFC_Domain;
}
declare module "@salesforce/label/c.HDFC_String" {
    var HDFC_String: string;
    export default HDFC_String;
}
declare module "@salesforce/label/c.IntegrationUsers" {
    var IntegrationUsers: string;
    export default IntegrationUsers;
}
declare module "@salesforce/label/c.LoanComplaint" {
    var LoanComplaint: string;
    export default LoanComplaint;
}
declare module "@salesforce/label/c.LoanQuery" {
    var LoanQuery: string;
    export default LoanQuery;
}
declare module "@salesforce/label/c.LoanRequest" {
    var LoanRequest: string;
    export default LoanRequest;
}
declare module "@salesforce/label/c.OmniChannelPresenceStatus" {
    var OmniChannelPresenceStatus: string;
    export default OmniChannelPresenceStatus;
}
declare module "@salesforce/label/c.Processing_Request_Link" {
    var Processing_Request_Link: string;
    export default Processing_Request_Link;
}
declare module "@salesforce/label/c.Reassignment_error_for_Complaint" {
    var Reassignment_error_for_Complaint: string;
    export default Reassignment_error_for_Complaint;
}
declare module "@salesforce/label/c.Reassignment_error_for_Query_and_Request" {
    var Reassignment_error_for_Query_and_Request: string;
    export default Reassignment_error_for_Query_and_Request;
}
declare module "@salesforce/label/c.Statuary_Escalation_L1_Owner" {
    var Statuary_Escalation_L1_Owner: string;
    export default Statuary_Escalation_L1_Owner;
}
declare module "@salesforce/label/c.simplify_360" {
    var simplify_360: string;
    export default simplify_360;
}