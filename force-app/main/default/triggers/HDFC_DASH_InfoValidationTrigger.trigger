/**
* Trigger Name:HDFC_DASH_InfoValidationTrigger
DevelopedBy:Janani Mohankumar
Date: 08 November 2021
Company: Accenture
Trigger Description: This trigger fires on HDFC_DASH_Info_Validation__c object .
**/
trigger HDFC_DASH_InfoValidationTrigger on HDFC_DASH_Info_Validation__c (before insert,after insert,before update,after update) {
HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.INFOVAL_CONFIG); 
}