/**
Trigger Name:HDFC_DASH_Application_Trigger 
DevelopedBy: Sai Suman
Date: 10 Nov 2021
Company: Accenture
Trigger Description: This trigger fires on  Opportunity object.
**/
trigger HDFC_DASH_Application_Trigger on Opportunity (before insert,before update,after insert, after update) {
    HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.APPLICATION_CONFIG);
}