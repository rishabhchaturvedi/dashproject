/**
* Trigger Name:HDFC_DASH_processException_Trigger
DevelopedBy: Anmol
Date: 26 May 2021
Company: Accenture
Trigger Description: This trigger fires when an Event occurs on Platform Event.
**/
trigger HDFC_DASH_processException_Trigger on HDFC_DASH_Exception_Logs__e (after insert) {
    HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.PROCESS_EXCEP_CONFIG);

}