/**
* Trigger Name:HDFC_DASH_AppDetails_Trigger
DevelopedBy: Anmol
Date: 26 May 2021
Company: Accenture
Trigger Description: This trigger fires when an Event occurs on Account Object.
**/
trigger HDFC_DASH_AppDetails_Trigger on HDFC_DASH_Applicant_Details__c (before insert,before update,after update, after insert) {
     HDFC_DASH_TriggerConfig appDetTrigConfig = HDFC_DASH_TriggerConfig.APPLICANT_DETAILS_CONFIG;
    
HDFC_DASH_TriggerHandler.handle(appDetTrigConfig);
}