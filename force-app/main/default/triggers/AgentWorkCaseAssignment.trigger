trigger AgentWorkCaseAssignment on AgentWork (after insert) {
    
    List<Id> caseIdList = new List<Id>();
    for(AgentWork a : Trigger.New){
        if(a.Status == 'Assigned')
            caseIdList.add(a.WorkItemId);    
    }
    
    if(caseIdList.size() > 0){
        List<Case> caseList = [select id,casenumber,ContactId,OwnerId,Owner.Email,HDFC_DASH_Deposit_Number__c,HDFC_DASH_Case_Type__c from Case where id in :caseIdList];
        HDFC_DASH_CaseTriggerOpsHelper.caseAssignmentOmniChannel(caseList);
    }
}