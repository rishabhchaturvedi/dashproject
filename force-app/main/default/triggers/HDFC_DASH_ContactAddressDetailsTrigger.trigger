/**
* Trigger Name:HDFC_DASH_ContactAddressDetailsTrigger
DevelopedBy: Sai Shruthi
Date: 05 August 2021
Company: Accenture
Trigger Description: This trigger fires on HDFC_DASH_Contact_Address_Details__c object .
**/
trigger HDFC_DASH_ContactAddressDetailsTrigger on HDFC_DASH_Contact_Address_Details__c (after insert,before insert,after update,before update) {
    HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.CONTACT_ADDRESS_DETAILS);
}