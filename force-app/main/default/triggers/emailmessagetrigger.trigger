trigger emailmessagetrigger on EmailMessage (before Insert,before update) {
    
    
    for(EmailMessage e : Trigger.New){
      List<String> toAddressList = new List<String>();
      system.debug('Trigger isBefore: '+Trigger.isBefore);
      system.debug('Trigger isAfter: '+Trigger.isInsert);
      system.debug('Trigger isUpdate: '+Trigger.isUpdate);
      system.debug('emailmessage parent... '+e.ParentId);
      system.debug('emailmessage status... '+e.Status);
      system.debug('emailmessage Incoming ... '+e.Incoming );
       system.debug('emailmessage EMail composer... '+e.Email_Composer__c);
         system.debug('to address...'+e.ToAddress);
         if(e.ToAddress != '' && e.ToAddress != null){
             toAddressList = (e.ToAddress.contains(',') ? e.ToAddress.split(',') : new List<String>{e.ToAddress});
              system.debug('to address list...'+toAddressList);
          }
      
      if(e.ParentId != null && e.Incoming != true && e.Email_Composer__c == true){ //(e.Status == '3' || e.Status == '5')
          system.debug('e.Parent.HDFC_DASH_File_Number__c... '+e.Parent.HDFC_DASH_File_Number__c);
          String caseList = [Select HDFC_DASH_File_Number__c from Case where Id =: e.ParentId].HDFC_DASH_File_Number__c ;
          system.debug('caseList ... '+caseList );
          List<HDFC_DASH_Applicant_Details__c> applicantDetailList = [select Id,HDFC_DASH_Application__c,HDFC_DASH_Contact__c,HDFC_DASH_Email__c from HDFC_DASH_Applicant_Details__c where 
                                                                      HDFC_DASH_Application__c =: caseList and (HDFC_DASH_Applicant_Capacity__c = 'B' OR HDFC_DASH_Applicant_Capacity__c = 'C')]; 
              
              if(applicantDetailList.size() > 0){                                                        
          system.debug('applicantDetailList...'+applicantDetailList);
          List<String> applicantDetailEmails = new List<String>();
          for(HDFC_DASH_Applicant_Details__c a : applicantDetailList){
              applicantDetailEmails.add(a.HDFC_DASH_Email__c );   
              system.debug('a.HDFC_DASH_Email__c...'+a.HDFC_DASH_Email__c);      
          }
          for(String s : toAddressList){
              if(!applicantDetailEmails.contains(s)){ 
                  system.debug('before add error...');             
                  e.addError('To Address can only be of Borrower and Co-Borrower');
              }
          
          }
          }else {
              Case caseList1 = [Select HDFC_DASH_Deposit_Number__c,ContactEmail from Case where Id =: e.ParentId limit 1];
              if(caseList1.HDFC_DASH_Deposit_Number__c != null){                 
                      if(!toAddressList.contains(caseList1.ContactEmail)){
                           e.addError('Email can only be sent to Customer');
                      }                  
              }
              
          } //end else
      
      } //end if
  }
  
    
    
    
    
    
    
    
    
    
    
    /*for(EMailMessage e : Trigger.New){
    
        if(e.Incoming == true && e.Headers == ''){
            List<Case> casList = [select id,caseNumber,subject,Priority,description,hdfc_dash_mode__c,hdfc_dash_sub_mode__c from case where Id =: e.ParentId];
            HDFC_DASH_CaseTriggerOpsHelperNew.executeCaseRecForListener(casList);
        
        }
    }*/
    
   
}