/**
* Trigger Name:HDFC_DASH_ContactTrigger
DevelopedBy: Tejeswari
Date: 18 November 2021
Company: Accenture
Trigger Description: This trigger fires on Contact object .
**/
trigger HDFC_DASH_ContactTrigger on Contact (before insert,after insert) {
    HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.CONTACT_CONFIG);
}