/*
Trigger Name:HDFC_DASH_ApplicantBankDetailsDetailsTrigger
DevelopedBy: Sai Suman
Date: 05 October 2021
Company: Accenture
Trigger Description: This trigger fires on Applicant Bank Details object .
*/
trigger HDFC_DASH_ApplicantBankDetailsDetailsTrigger on HDFC_DASH_Applicant_Bank_Detail__c (before insert,before update,after insert,after update) {
 HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.APPLICANT_BANK_DETAILS);
}