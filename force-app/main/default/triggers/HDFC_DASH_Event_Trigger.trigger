/**
* Trigger Name: HDFC_DASH_Event_Trigger
DevelopedBy: Anisha Arumugam
Date: 25 November 2021
Company: Accenture
Trigger Description: This trigger fires on Event object .
**/
trigger HDFC_DASH_Event_Trigger on Event (before insert, before update) {
 	HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.EVENT_CONFIG);
}