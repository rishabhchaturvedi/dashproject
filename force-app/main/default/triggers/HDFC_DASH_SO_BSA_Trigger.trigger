/**
* Trigger Name: HDFC_DASH_SO_BSA_Trigger
DevelopedBy: Tejeswari
Date: 19 November 2021
Company: Accenture
Trigger Description: This trigger fires on HDFC_DASH_SO_BSA_Association__c object .
**/
trigger HDFC_DASH_SO_BSA_Trigger on HDFC_DASH_SO_BSA_Association__c (before insert) {
 	HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.SO_BSA_CONFIG);
}