/*
Trigger Name:HDFC_DASH_ApplicantEmploymentDetailsTrigger
DevelopedBy: Sai Suman
Date: 05 October 2021
Company: Accenture
Trigger Description: This trigger fires on Applicant Employment Details object .
*/
 trigger HDFC_DASH_ApplicantEmploymentDetailsTrigger on HDFC_DASH_Applicant_Employment_Details__c (after insert,after update,before insert,before update) {
    HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.APPLICANT_EMPLOYMENT_DETAILS);
}