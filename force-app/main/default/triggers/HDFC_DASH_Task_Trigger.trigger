/**
* Trigger Name: HDFC_DASH_Task_Trigger
DevelopedBy: Anisha Arumugam
Date: 25 November 2021
Company: Accenture
Trigger Description: This trigger fires on Task object .
**/
trigger HDFC_DASH_Task_Trigger on Task (after insert) {
 	HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.TASK_CONFIG);
}