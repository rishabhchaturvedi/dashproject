/**
* Trigger Name:HDFC_DASH_CaseTrigger
DevelopedBy: Deepali Gupta
Date: 30 Sept 2021
Company: Accenture
Trigger Description: This trigger fires on Case object .
**/
trigger HDFC_DASH_CaseTrigger on Case (before insert, before update, after insert, after update) {
  
           
    if(Trigger.isBefore){
        HDFC_DASH_CaseTriggerOpsHelper.entitlementtrigger(Trigger.new,trigger.old);
    }
    
  
    if(trigger.isbefore && trigger.isupdate)
    {
    if(trigger.old[0].Ownerid !=null && string.valueOf(trigger.old[0].ownerid).startsWith('005'))
    {
    for (case c: Trigger.New)
    {
    c.Previous_Owner__c=trigger.old[0].Ownerid;
    }
    }
    }
 HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.CASE_CONFIG);   
    
    if(Trigger.isBefore && Trigger.isInsert)
      {
      for(Case c:Trigger.new)
      {
      if(c.id !=null && c.id !='')
      {
      //e2c
      system.debug('incoming case origin before insert...'+c.Origin);
      if(c.HDFC_DASH_File_NumberText__c !=null && c.HDFC_DASH_File_Number__c!=null )
     {
     opportunity o= [select id from opportunity where HDFC_DASH_File_Number__c =:c.HDFC_DASH_File_NumberText__c ];
     if(o.id !=null)
     {
     c.HDFC_DASH_File_Number__c=o.id;
     }
      }
      if(c.HDFC_DASH_File_Number__c != null){
          
          string emailId = [Select account.personemail from Opportunity where Id = :c.HDFC_DASH_File_Number__c].account.personemail;
          c.Account_Email__c = emailId ;
      }
      
      if(c.Origin == 'Email'){
          HDFC_DASH_CaseTriggerOpsHelper.executeCaseRecForListener(Trigger.New);
      }
      }
      }
      }
   
   if(Trigger.isBefore && Trigger.isUpdate)
   {
    for(Case c:Trigger.new)
      {
      if(c.status=='Re-Opened')
      {
      c.HDFC_DASH_Remarks__c='';
      }
          if(c.Status=='Escalated L2' && (c.Priority=='Medium' || c.Priority=='Low'))
          {
              c.Priority='High';
          }
          
      }
   }
  
    if(Trigger.isBefore && Trigger.isUpdate)
            {
         for(Case c : Trigger.New ){
        system.debug('incoming case origin before update...'+c.Origin);
        system.debug('incoming case origin before update...'+c); 
        case cold=[select id,HDFC_DASH_Social_media_link__c, Priority,HDFC_DASH_Sub_Category__c,HDFC_DASH_Category__c from case where id=:Trigger.Old limit 1];
       if(cold.id !=null)
        {
        if(cold.HDFC_DASH_Social_media_link__c !=null && c.HDFC_DASH_Social_media_link__c==null)
              c.HDFC_DASH_Social_media_link__c=cold.HDFC_DASH_Social_media_link__c;
             if(cold.Priority =='Influencer')
      c.Priority =cold.Priority;
      else if(cold.Priority =='Critical')
      c.Priority =cold.Priority;                
            }
            }
     }
      
     
if(trigger.isafter && trigger.isUpdate){

   List<Case> caseListToBeEscalated = new List<Case>();
   for(Case c : Trigger.New){
     
         if(trigger.oldMap.get(c.Id).status !='Escalated L1' && trigger.newMap.get(c.Id).status =='Escalated L1' )
       {
           caseListToBeEscalated.add(c);
          
          
        }
       else if(trigger.oldMap.get(c.Id).status !='Escalated L2' && trigger.newMap.get(c.Id).status =='Escalated L2')
       {    
           caseListToBeEscalated.add(c);
             
       }
       
     
   }  
       HDFC_DASH_CaseTriggerOpsHelper.executeEscalatedCase(caseListToBeEscalated);
      

   }
     if(trigger.isAfter && trigger.isUpdate)
   {
   if(trigger.old[0].ownerid !=trigger.New[0].ownerid && (Trigger.New[0].Status =='Escalated L1' || Trigger.New[0].Status =='Escalated L2' || Trigger.New[0].Status =='Statutory Escalated L1' || Trigger.New[0].Status =='Statutory Escalated L2'))
   {
    
      
       if(string.valueOf(Trigger.New[0].ownerid).startsWith('005'))
       {
        sendemailtosocialmedia.sendescalationemails(trigger.New);
       }
   
   
    
   }
   }
 
  
   //Deposit
   if(trigger.isafter && trigger.isInsert && trigger.new[0].HDFC_DASH_Deposit_Case__c && !System.isFuture() )
   {
       system.debug('HDFC_DASH_DepositEmailToCase@@');
       if(trigger.new[0].Origin == 'Email'){
            HDFC_DASH_DepositEmailToCase.emailToCaseDepositListener(trigger.new[0].Id);
       }else if(trigger.new[0].Origin == 'Web' || trigger.new[0].Origin == 'Phone'){
            HDFC_DASH_CaseCreationForDeposit.CaseDepositListener(trigger.new[0].Id);
          
             }
   
   }  
   if(trigger.isAfter)
   {
    
   if(trigger.New[0].HDFC_DASH_Social_media_link__c !=null && trigger.New[0].HDFC_DASH_Social_media_link__c !='')
   {
   if(trigger.New[0].status !='Escalated L1' && trigger.New[0].status !='Escalated L2' && trigger.New[0].status !='Not Registered' && trigger.New[0].status !='Closed')
   {
   Sendemailtosocialmedia.sendemailtosocialmedia(Trigger.New);
   }
   }
   
   if(trigger.isinsert)
   {
   if(trigger.new[0].HDFC_DASH_Sub_mode__c=='CGRO'||trigger.New[0].HDFC_DASH_Sub_mode__c=='MD Level 2- Website'){
          sendemailforstatutoryescalation.sendemailtocustomer(Trigger.New);
              
            }
   
   }
   else if(trigger.isUpdate)
   {  
     if(Trigger.old[0].HDFC_DASH_Sub_Mode__c != trigger.new[0].HDFC_DASH_Sub_Mode__c &&(trigger.new[0].HDFC_DASH_Sub_mode__c=='CGRO'||trigger.New[0].HDFC_DASH_Sub_mode__c=='MD Level 2- Website')){
          sendemailforstatutoryescalation.sendemailtocustomer(Trigger.New);
              
            }
   }
   }


    //Notification to Case Owner 
    if(Trigger.isAfter && Trigger.isInsert && trigger.new[0].HDFC_DASH_Deposit_Case__c == false){
        HDFC_DASH_CaseTriggerOpsHelper.caseAssignmentOmniChannel(Trigger.New);
    
    }
if(Trigger.isAfter && trigger.isUpdate)
{
if(Trigger.New[0].HDFC_DASH_Mode__c !='' && Trigger.New[0].HDFC_DASH_Mode__c !=null && Trigger.New[0].HDFC_DASH_Mode__c !='Social Media' && Trigger.Old[0].Ownerid !=Trigger.New[0].Ownerid && (Trigger.New[0].Status !='Escalated L1' || Trigger.New[0].Status !='Escalated L2' || Trigger.New[0].Status !='Statutory Escalated L1' || Trigger.New[0].Status !='Statutory Escalated L2'))
{
sendemailtoSocialMedia.sendemailtocaseowner(Trigger.New);
}

else if(Trigger.New[0].HDFC_DASH_Mode__c == 'Social Media' && Trigger.old[0].Description !=null && Trigger.old[0].Description !='' && Trigger.New[0].Description != Trigger.old[0].Description){
Sendemailtosocialmedia.caseUpdatesFromSocialMedia(Trigger.New);

}
else if(Trigger.New[0].HDFC_DASH_Mode__c !='' && Trigger.New[0].HDFC_DASH_Mode__c !=null && Trigger.New[0].HDFC_DASH_Mode__c =='Social Media' && Trigger.Old[0].Ownerid !=Trigger.New[0].Ownerid && (Trigger.New[0].Status !='Escalated L1' || Trigger.New[0].Status !='Escalated L2' || Trigger.New[0].Status !='Statutory Escalated L1' || Trigger.New[0].Status !='Statutory Escalated L2'))
{
if(Trigger.New[0].Priority=='High' && (Trigger.New[0].Status=='New' ||Trigger.New[0].Status=='Pending'))
{
sendemailtoSocialMedia.sendemailtocaseowner(Trigger.New);
}
}

}

//24th Feb - Bulk Assignment
if(Trigger.isBefore && Trigger.IsUpdate && Trigger.New.size() > 1){
    for(Case c : Trigger.New){
        if(c.OwnerId != Trigger.OldMap.get(c.id).OwnerId){
            c.HDFC_DASH_Skip_AutoAssignment__c = true;
        }
    
    }

}
   
}