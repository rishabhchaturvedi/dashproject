/**
* Trigger Name:HDFC_DASH_LeadTrigger
DevelopedBy: Sai Shruthi
Date: 05 August 2021
Company: Accenture
Trigger Description: This trigger fires on Lead object .
**/
trigger HDFC_DASH_LeadTrigger on Lead(before insert,before update) {
     HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.LEAD_CONFIG);    
}