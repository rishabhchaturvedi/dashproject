/**
* Trigger Name:HDFC_DASH_Account_trigger
DevelopedBy: Anmol
Date: 26 May 2021
Company: Accenture
Trigger Description: This trigger fires when an Event occurs on Account Object.
**/
trigger HDFC_DASH_Account_trigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    system.debug('Account Trigger***');       
	HDFC_DASH_TriggerHandler.handle(HDFC_DASH_TriggerConfig.ACCOUNT_CONFIG);
}