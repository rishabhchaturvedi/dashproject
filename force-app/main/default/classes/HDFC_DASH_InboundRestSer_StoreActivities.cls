/*
Author: Kumar Gourav
Date: 27 Sep 2021
Company: Accenture
Class:HDFC_DASH_InboundRestSer_StoreActivities
Description: Service Class for Store Activities API
*/
public with sharing  class HDFC_DASH_InboundRestSer_StoreActivities 
{
    Public static String param_TL;
    /* 
Method:processActivities
Description: Method to process the list of Activities
*/
    public static HDFC_DASH_APIResponse_StoreActivities processActivities(RestRequest request)
    {      
        //Calling JSON Parser Class
        HDFC_DASH_ParseActivities result = HDFC_DASH_ParseActivities.parse(request);
        
        //Store list of Activities
        HDFC_DASH_APIResponse_StoreActivities apiRes = saveActivities(result, request);
        
        return apiRes;  
    }
    
    /*   
Method: saveActivities
Description: This method will upsert Event records.
*/
    private static HDFC_DASH_APIResponse_StoreActivities saveActivities(HDFC_DASH_ParseActivities result, RestRequest request)
    {             
        HDFC_DASH_APIResponse_StoreActivities apiRes =  new HDFC_DASH_APIResponse_StoreActivities();  
        List<Opportunity> listApp_FileNumber = new List<Opportunity>();
        List<Lead> listLead_LmsId = new List<Lead>();
        List<Contact> listCon_ExeCode = new List<Contact>();
        List<Account> listAcc_SourceRSNO = new List<Account>();
       	List<Event> listEvents = new List<Event>();
        param_TL = RestContext.request.params.get(HDFC_DASH_Constants.STRING_TL);
        list<string> list_FileNumber = new list<string>();
        list<string> list_LeadId = new list<string>();
        list<string> list_BSA = new list<string>();
        list<string> list_BSAOwner = new list<string>();
        List<String> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_name, HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID};
             List<String> leadFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID};  
                Map<string, Opportunity> mapApplications = new Map<string, Opportunity>();
        		Map<string, Lead> mapLeads = new Map<string, Lead>();
        //Map<string, Contact> mapContacts = new Map<string, Contact>();
        Map<String, Contact> mapContact_BSA = new Map<String, Contact>();
        Map<String, Account> mapAccount_BSA = new Map<String, Account>();
       
        
        for(HDFC_DASH_ParseActivities.Activities activity : result?.activities)
        {
            if(String.isNotBlank(activity?.fileNumber))
            {
                list_FileNumber.add(activity?.fileNumber);
            }
            if(String.isNotBlank(activity?.leadId))
            {
                list_LeadId.add(activity?.leadId);
            }
            //BSASubject
            if(String.isNotBlank(activity?.bsa))
            {
                list_BSA.add(activity?.bsa);
            }
            //BSAOwner
            if(String.isNotBlank(activity?.assignedToSfId) && activity?.assignedToSfId?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) != HDFC_DASH_Constants.PREFIX_USER)//If not prefix of user
            {
                list_BSAOwner.add(activity?.assignedToSfId);
            }
        }
        
        if(!list_FileNumber.isEmpty())
            listApp_FileNumber = HDFC_DASH_Application_Selector.getApplication(list_FileNumber, listOfFields_App, HDFC_DASH_Constants.STRING_name);
        
        if(!list_LeadId.isEmpty()) 
           listLead_LmsId= HDFC_DASH_Lead_Selector.getLeadsBasedOnCondition(list_LeadId,leadFieldList,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID);
           
        //BSAOwner
        if(!list_BSAOwner.isEmpty()) 
           	listCon_ExeCode = HDFC_DASH_Contact_Selector.getContacts(list_BSAOwner, new List<String>{HDFC_DASH_Constants.STRING_ID,HDFC_DASH_Constants.HDFC_DASH_Executive_Code}, HDFC_DASH_Constants.HDFC_DASH_Executive_Code);
        
        //BSASubject
        if(!list_BSA.isEmpty()) 
           listAcc_SourceRSNO = HDFC_DASH_Account_Selector.getAccounts(list_BSA, new List<String>{HDFC_DASH_Constants.STRING_ID,HDFC_DASH_Constants.HDFC_DASH_Source_Rec_SrNo}, HDFC_DASH_Constants.HDFC_DASH_Source_Rec_SrNo);
        
        
        for(Opportunity app : listApp_FileNumber)
            mapApplications.put(app.Name, app);
        
        for(Lead leadRec : listLead_LmsId)
            mapLeads.put(leadRec.HDFC_DASH_LMS_Lead_ID__c, leadRec);
        
       for(Contact conRec : listCon_ExeCode)
            mapContact_BSA.put(conRec.HDFC_DASH_Executive_Code__c, conRec);
        
        for(Account accRec : listAcc_SourceRSNO)
            mapAccount_BSA.put(accRec.HDFC_DASH_Source_Rec_SrNo__c, accRec);
         
        
        for(HDFC_DASH_ParseActivities.Activities activity : result.activities)
        {
            Event objEve = new Event();
            if(String.isNotBlank(activity?.type) && String.isNotBlank(activity?.status) && String.isNotBlank(activity?.description) && activity?.startDateTime != null && 
               activity?.endDateTime != null && String.isNotBlank(activity?.meetingType))
            {
                if(activity?.type == HDFC_DASH_Constants.STRING_ACTIVITY || activity?.type == HDFC_DASH_Constants.STRING_HUDDLE)
                {
                    if(activity?.startDateTime < activity?.endDateTime)
                    { 
                        objEve.Subject = activity?.title;
                        objEve.Type = activity?.type;
                        objEve.id = activity?.sfActivityId;
                        objEve.HDFC_DASH_Status__c = activity?.status;
                        objEve.Description = activity?.description;
                        objEve.StartDateTime = activity?.startDateTime;
                        objEve.EndDateTime = activity?.endDateTime;
                        objEve.HDFC_DASH_Actual_End_DateTime__c = activity?.actualEndDateTime;
                        objEve.HDFC_DASH_Meeting_Type__c = activity?.meetingType;
                        objEve.HDFC_DASH_Location__Latitude__s = activity?.latitude;
                        objEve.HDFC_DASH_Location__Longitude__s = activity?.longitude;
                        objEve.HDFC_DASH_Subject_Name_From_OS__c = activity?.subjectName;
                        objEve.HDFC_DASH_Application_Stage__c = activity?.applicationStage;
                        objEve.HDFC_DASH_Application_Status__c = activity?.applicationStatus;
                        
                        //Scenario for Huddle
                        if(activity?.type == HDFC_DASH_Constants.STRING_HUDDLE )
                        {
                            if(String.isNotBlank(param_TL))
                            {
                                objEve.OwnerId = param_TL;
                            }
                            else 
                            {
                                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_PARAMS_EXP); 
                            }
                            
                            if(activity?.assignedToSfId != null)
                            {
                                if(mapContact_BSA.get(activity?.assignedToSfId) != null)
                                {
                                    objEve.whoid = mapContact_BSA.get(activity?.assignedToSfId).Id;
                                }
                                else
                                {
                                     HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ExecutiveCode_Exp);
                                }
                            }
                        }

                        //Scenario 1 
                        else if(String.isNotBlank(activity?.fileNumber) && activity?.type ==HDFC_DASH_Constants.STRING_ACTIVITY && String.isBlank(activity?.bsa) && String.isBlank(activity?.leadID) && String.isNotBlank(activity?.assignedToSfId) && activity?.assignedToSfId?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) != HDFC_DASH_Constants.PREFIX_USER)
                        {                            
                            //Check if the Sales Officer Id Exist
                            if(mapContact_BSA.get(activity?.assignedToSfId) != null){
                                objEve.HDFC_DASH_BSA__c =mapContact_BSA.get(activity?.assignedToSfId).Id;
                            }
                            else{
                                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_BSA_EXP);
                            }
                            if(mapApplications.get(activity?.fileNumber) != null){
                            	objEve.WhatId = mapApplications.get(activity?.fileNumber).id;
                            }
                            else{
                                 HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_FILENUMBER_EXP); 
                            }
                             objEve.OwnerId = userInfo.getUserId();
                        }
                        //Scenario 2
                        else if(String.isNotBlank(activity?.leadID) && activity?.type ==HDFC_DASH_Constants.STRING_ACTIVITY && String.isBlank(activity?.bsa) &&  String.isBlank(activity?.fileNumber) && String.isNotBlank(activity?.assignedToSfId) && activity?.assignedToSfId?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) != HDFC_DASH_Constants.PREFIX_USER)
                        {
                            //Check if the Sales Officer Id Exist
                            if(mapContact_BSA.get(activity?.assignedToSfId) != null){
                                objEve.HDFC_DASH_BSA__c = mapContact_BSA.get(activity?.assignedToSfId).Id;
                            }
                            else{
                                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_BSA_EXP);  
                            }
                            if(mapLeads.get(activity?.leadID) != null){
                            	objEve.WhoId = mapLeads.get(activity?.leadID).Id;
                            }
                            else{
                                 HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_LEADID_EXP); 
                            }
                     		objEve.OwnerId = userInfo.getUserId();
                        }
                        //Scenario 3
                        else if(String.isNotBlank(activity?.bsa) && activity?.type ==HDFC_DASH_Constants.STRING_ACTIVITY && String.isBlank(activity?.fileNumber) && String.isBlank(activity?.leadID) && String.isNotBlank(activity?.assignedToSfId) && activity?.assignedToSfId?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_USER)
                        {   
                                objEve.OwnerId = activity?.assignedToSfId;
                                
                            //Check if the Sales Officer Id Exist
                            if(mapAccount_BSA.get(activity?.bsa) != null)
                                objEve.WhatId = mapAccount_BSA.get(activity?.bsa).Id;
                            else
                                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_BSA_EXP);    
                        }
                        //Scenario 4
                        else if(String.isNotBlank(activity?.fileNumber) && activity?.type ==HDFC_DASH_Constants.STRING_ACTIVITY &&  String.isBlank(activity?.bsa) && String.isBlank(activity?.leadID) && String.isNotBlank(activity?.assignedToSfId) && activity?.assignedToSfId?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_USER)
                        {
                            objEve.OwnerId = activity?.assignedToSfId;
                            
                            if(mapApplications.get(activity?.fileNumber) != null){
                            	objEve.WhatId = mapApplications.get(activity?.fileNumber).id;
                            }
                            else{
                                 HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_FILENUMBER_EXP); 
                            }
                                        }
                        //Scenario 5
                        else if(String.isNotBlank(activity?.leadID) && activity?.type ==HDFC_DASH_Constants.STRING_ACTIVITY && String.isBlank(activity?.bsa) &&  String.isBlank(activity?.fileNumber) && String.isNotBlank(activity?.assignedToSfId) && activity?.assignedToSfId.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_USER)
                        {
                            objEve.OwnerId = activity?.assignedToSfId;
                            if(mapLeads.get(activity?.leadID) != null){
                            	objEve.WhoId = mapLeads.get(activity?.leadID).Id;
                            }
                            else{
                                 HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_LEADID_EXP); 
                            }
                        }
                        listEvents.add(objEve);
                    }
                    else 
                    { 
                        HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_EVENTDATETIME_EXP); 
                    }
                }
                else
                {
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_ACTIVITYTYPE_EXP); 
                }
            }
            else{
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_REQUIREDFIELD_MISSING);
            }
            apiRes.SuccessMessage = HDFC_DASH_Constants.STOREACTIVITIES_SUCCESSMESSAGE;
            Database.upsertResult[] result_Upsert = Database.upsert(listEvents); 
            List<HDFC_DASH_APIResponse_StoreActivities.activity> listActivity = new List<HDFC_DASH_APIResponse_StoreActivities.activity>();
            List<String> ListEventId = new List<String>();
            
            for(Database.upsertResult res : result_Upsert){
                
                ListEventId.add(res.getId());
                
            }
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Activity_Number};
                Map<Id,Event> eventMap = new Map<Id,Event>(HDFC_DASH_Event_Selector.getEvent(ListEventId, fieldList, HDFC_DASH_Constants.ID_STRING));
            for(Database.upsertResult res_Event : result_Upsert){
                HDFC_DASH_APIResponse_StoreActivities.activity activities= new HDFC_DASH_APIResponse_StoreActivities.activity();
                activities.sfActivityId = res_Event.getId();
                activities.sfActivityNumber = eventMap.get(res_Event.getId()).HDFC_DASH_Activity_Number__c;
                listActivity.add(activities);
            }
            apiRes.activities = listActivity;
        }        
        return apiRes;
        
    }
 
}