/*
	Author: Anmol Srivastava
	Class: HDFC_DASH_APIResponse_DMSDocument
	Description: Apex Class for handling success message for Document Details API
*/
public inherited sharing class HDFC_DASH_APIResponse_StoreDetail {
    
    public String sfApplicationId;
    public String sfApplicationNo; 
    public String successMessage;
}