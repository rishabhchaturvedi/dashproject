/**
className: emailmessagetriggerTest
DevelopedBy: Arunima Banerjee
Date: 9 February 2022
Company: Accenture
Class Description: Test class for emailmessagetrigger trigger.
**/

@isTest()
public class emailmessagetriggerTest {
    @istest
    static void applicationListNotBlank(){
        
       Date closeDt = Date.Today();
        
        Account a2 = new Account();
        a2.Name='testing acc';
        insert a2;
        
        contact con =new contact();
        con.LastName='testing user';
        con.email='testing@testing.com';
        con.accountid=a2.id;        
        insert con;
        
        Opportunity opp = new Opportunity();
        opp.Name='testing 1122';
        opp.HDFC_DASH_Owner_Agency__c=a2.id;   
        opp.StageName = 'Onboarding';
        opp.CloseDate = closeDt;
        opp.HDFC_DASH_LMS_Origin_Branch_Id__c='branch 001';
        insert opp;
        
        
        
        case cs = new case();
        cs.Subject='test';
        cs.HDFC_DASH_File_Number__c=opp.id;
        cs.HDFC_DASH_Case_Type__c = 'Query';
        insert cs;
        
        
        HDFC_DASH_Applicant_Details__c Apl = new HDFC_DASH_Applicant_Details__c();
        Apl.Name='Applicant test';
        Apl.HDFC_DASH_Applicant_Capacity__c='B';
        Apl.HDFC_DASH_Application__c= opp.Id;
        Apl.HDFC_DASH_LastName__c='lastname test';
        Apl.HDFC_DASH_Email__c='Dimple123@gmail.com';
        insert Apl;

        
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.com';
        email.Incoming = false;
        email.ToAddress= 'Dimple123@gmail.com';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.Email_Composer__c=true;
        email.ParentId = cs.Id; 
        try{
            insert email;
        }
        catch(Exception e){
                  
        }   
    }
    @istest
    static void applicationListIsBlank(){
        Date closeDt = Date.Today();
        
        Account a2 = new Account();
        a2.Name='testing acc';
        insert a2;
        
        contact con =new contact();
        con.LastName='testing user';
        con.email='testing@testing.com';
        con.accountid=a2.id;        
        insert con;
        
        Opportunity opp = new Opportunity();
        opp.Name='testing 1122';
        opp.HDFC_DASH_Owner_Agency__c=a2.id;   
        opp.StageName = 'Onboarding';
        opp.CloseDate = closeDt;
        opp.HDFC_DASH_LMS_Origin_Branch_Id__c='branch 001';
        insert opp;
        
        
/*        Deposit__x mockRequest = new Deposit__x();
        mockRequest.BORR_CUST_NO__c=a2.id;
        insert insertAsync();

*/

        
        HDFC_DASH_Applicant_Details__c Apl = new HDFC_DASH_Applicant_Details__c();
        Apl.Name='Applicant test';
        Apl.HDFC_DASH_Applicant_Capacity__c='P';
        Apl.HDFC_DASH_Application__c= opp.Id;
        Apl.HDFC_DASH_LastName__c='lastname test';
        Apl.HDFC_DASH_Email__c='Dimple123@gmail.com';
        insert Apl;

          
        case cs = new case();
        cs.Subject='test';
        cs.HDFC_DASH_File_Number__c=opp.id;
        cs.contactid=con.id;
        cs.HDFC_DASH_Case_Type__c='Query';
        insert cs;
        
        
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.com';
        email.Incoming = false;
        email.ToAddress= 'dimple@abc.com';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.Email_Composer__c=true;
        email.ParentId = cs.Id;
        insert email;
        

    }
    
    

}