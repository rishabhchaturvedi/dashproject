/*
Class: HDFC_DASH_Update_Lead_Stage_Callout
Author: Ayesha Gavandi
Date: 24 November 2021
Company: Accenture
Description: Queueable Class to update lead stage Callout
*/
public inherited sharing class HDFC_DASH_Update_Lead_Stage_Callout implements Queueable, Database.AllowsCallouts {
    Id appId;
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    RestRequest request;
    Exception exp; 
    public HDFC_DASH_Update_Lead_Stage_Callout(Id appId) //applicationID
    {
        this.appId = appId;
    }
    
    /*
Method: execute
Description: This method will Post the Document Details to ILPS.
*/
    public void execute(QueueableContext context) 
    {
        System.debug('This is Update Lead Stage Callout');
        try
        {
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
            
            //Get the Document details request body
            HDFC_DASH_APIRequest_Update_Lead_Stage reqBody = getRequestBody();            
            system.debug('HDFC_DASH_Update_Lead_Stage_Callout Request '+ reqBody);            
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_LEAD_STAGE);
            system.debug('Request Generated'+ requestHeader);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_LEAD_STAGE, 
                                              HDFC_DASH_Constants.METHOD_TYPE_POST,string.ValueOf(Json.serialize(reqBody)), 
                                              requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,
                                              requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            system.debug('Response Generated'+response);
            updateNewLeadno(response);
        } 
        catch(Exception e)
        {      
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_LEAD_STAGE, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }   
    
    /*
Method: getRequestBody
Parameters:None
Description: This method will create the requestBody from the request.
*/ 
    public HDFC_DASH_APIRequest_Update_Lead_Stage getRequestBody() 
    {
        HDFC_DASH_APIRequest_Update_Lead_Stage leadStage = new HDFC_DASH_APIRequest_Update_Lead_Stage();
        List<String> oppFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID,HDFC_DASH_Constants.HDFC_DASH_Stage_Number,HDFC_DASH_Constants.HDFC_DASH_File_Number};
            Opportunity app = HDFC_DASH_Application_Selector.getApplicationBasedOnId(oppFieldList,appId); 
        leadStage.lead_no = app.HDFC_DASH_LMS_Lead_ID__c;
        leadStage.lead_stage = app.HDFC_DASH_Stage_Number__c;
        leadStage.file_no = app.HDFC_DASH_File_Number__c;
        return leadStage;
    }
    /*
Method: updateNewLeadno
Parameters: Response
Description: This method will process the response and update the new lead number in Application.
*/
    public void updateNewLeadno (HttpResponse response){
        system.debug('updateNewLeadno');
        HDFC_DASH_APIResponse_Update_Lead_Stage leadApiResponse = New HDFC_DASH_APIResponse_Update_Lead_Stage() ;
       List<String> oppFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID,HDFC_DASH_Constants.VALIDATION_LOOKUP_LEAD};
        Opportunity app = HDFC_DASH_Application_Selector.getApplicationBasedOnId(oppFieldList,appId);
        system.debug('response'+response);
        if(response !=NULL && response?.getStatusCode() == HDFC_DASH_Constants.INT_TWO_HUNDRED){
            system.debug('responsenotnull'+response);
            leadApiResponse = HDFC_DASH_APIResponse_Update_Lead_Stage.parse(response);
            system.debug('NewLeadNo==='+leadApiResponse?.new_lead_no);
            if(leadApiResponse.new_lead_no != HDFC_DASH_Constants.STRING_ZERO){
                system.debug('afterIf==='+leadApiResponse?.new_lead_no);
                           
                app.HDFC_DASH_LMS_Lead_ID__c = leadApiResponse.new_lead_no;
                database.update(app);
                system.debug(+app);
               // list<String> leadFieldList = new list<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID};
                   // Lead leadRec = HDFC_DASH_Lead_Selector.getLeadsBasedOnId(leadFieldList,app.HDFC_DASH_Lead__c);
               if(app.HDFC_DASH_Lead__c != NULL){
                   Lead leadRec = new Lead();
                   leadRec.id = app.HDFC_DASH_Lead__c;
                    leadRec.HDFC_DASH_LMS_Lead_ID__c = leadApiResponse.new_lead_no;
                    database.update(leadRec);
                    system.debug(+leadRec);
                }
            }
        }
    }
}