/**
className: HDFC_DASH_EmailMessage_Selector
DevelopedBy: Utkarsh Patidar
Date: 14 Dec 2021
Company: Accenture
Class Description: This class would create the select queries for EmailMessage object.
**/
public with sharing class HDFC_DASH_EmailMessage_Selector 
{
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from EmailMessage ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    
    /* 
Method Name :getEmailMessage
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get a list of EmailMessage based on one condition.
*/   
    public static List<EmailMessage> getEmailMessage(List<String> queryParameters,List<string> fieldList, String conditionOn)
    {
        string  querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<EmailMessage> listEmailMessage = database.query(querystring); 
        //system.debug('querystring => ' + querystring);
        return listEmailMessage;
    }
    
    /* 
Method Name :getEmailMessageBasedOnQuery
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
Description :This method would get the List of EmailMessage based on fieldsAndparameters sent to it.
*/
    public static List<EmailMessage> getEmailMessageBasedOnQuery(List<String> fieldList, Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +
            FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<EmailMessage> emailMessageList = database.query(querystring); 
        return EmailMessageList;   
    }
    
    /* 
Method Name :getCondition
Parameters  :Map<String,String> fieldsAndparameters
Description :This method would be called from getEmailMessageBasedOnQuery.
*/
    public static String getCondition(Map<String,String> fieldsAndparameters)
    {
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }
    
}