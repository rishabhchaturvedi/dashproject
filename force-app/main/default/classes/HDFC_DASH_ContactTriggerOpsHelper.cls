/**
className: HDFC_DASH_ContactTriggerOpsHelper
DevelopedBy: Tejeswari
Date: 18 November 2021
Company: Accenture 
Class Description:  Helper class for HDFC_DASH_ContactTriggerOps.
**/
public with sharing class HDFC_DASH_ContactTriggerOpsHelper {
    /* 
Method Name :filterContactsWithReportsToIdField
Description :This method would be called to filter the list of Contacts for which have the ReportsToID field.
*/
    public static List<Contact> filterContactsWithReportsToIdField(){
        List<Contact> listContacts = new List<Contact>();
        List<Contact> listUpdateContact = new List<Contact>();
        listContacts = trigger.new;  
        for(Contact conRec:listContacts)
        {
            if(String.isNotBlank(conRec.HDFC_DASH_ReportsTo_External_Id__c) || String.isNotBlank(conRec.HDFC_DASH_Agency_Code__c)){
                listUpdateContact.add(conRec);
            }
        }
        return listUpdateContact;
    } 
    /* 
Method Name :updateReportsToIdLookupField
Description :This method would be to Update the lookup field of the Contact.
*/
    public static void updateReportsToIdLookupField(List<Contact> listContacts)
    {
        List<String> listAgencyCode= new List<String>();
        List<Contact> listCon = new List<Contact>();
        List<Account> listAcc = new List<Account>();
        List<String> listHsalesExteralId= new List<String>();
        List<String> listNonHsalesExteralId= new List<String>();
        Map<String,Account> mapAcc = new Map<String,Account>();
        Map<String,String> mapCondition = new Map<String,String>();
        Map<String,String> mapConEmpNo = new Map<String,String>();
        Map<String,String> mapConExeCode = new Map<String,String>();
        String dsa_Group;
        List<String> listAccFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Agency_Code,HDFC_DASH_Constants.HDFC_DASH_DSA_Group};
            List<string> listConFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Executive_Code,HDFC_DASH_Constants.HDFC_DASH_Emp_Number,HDFC_DASH_Constants.ACCOUNT_RECORDTYPE_NAME}; 
                
                for(Contact conRec:listContacts){
                    if(String.isNotBlank(conRec.HDFC_DASH_Agency_Code__c)){
                        listAgencyCode.add(conRec.HDFC_DASH_Agency_Code__c);
                    }
                    else{
                        HDFC_DASH_Error_Code__c errorCode_RequiredField_Missing= HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_REQUIREDFIELD_MISSING);
                        conRec.adderror(errorCode_RequiredField_Missing.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Agency_Code);            
                    }
                }
        
        if(!listAgencyCode.isEmpty()){
            listAcc = HDFC_DASH_Account_Selector.getAccounts(listAgencyCode, listAccFields, HDFC_DASH_Constants.HDFC_DASH_Agency_Code);
        }
        
        if(!listAcc.isEmpty()){
            for(Account accRec:listAcc){
                mapAcc.put(accRec.HDFC_DASH_Agency_Code__c,accRec);
            }
        }
        
        for(Contact conRec : listContacts)
        {
            if(mapAcc.containsKey(conRec.HDFC_DASH_Agency_Code__c))
            {
                dsa_Group = mapAcc.get(conRec.HDFC_DASH_Agency_Code__c).HDFC_DASH_DSA_Group__c; 
                
                if(String.isNotBlank(dsa_Group) && String.isNotBlank(conRec.HDFC_DASH_ReportsTo_External_Id__c))
                {
                    if(dsa_Group == HDFC_DASH_Constants.HDFC_SALES)
                    {
                        listHsalesExteralId.add(conRec.HDFC_DASH_ReportsTo_External_Id__c);
                    }
                    else if(dsa_Group != HDFC_DASH_Constants.HDFC_SALES)
                    {
                        listNonHsalesExteralId.add(conRec.HDFC_DASH_ReportsTo_External_Id__c);
                    }
                }
            }
            else if(String.isNotBlank(conRec.HDFC_DASH_Agency_Code__c) && ! mapAcc.containsKey(conRec.HDFC_DASH_Agency_Code__c)){
                HDFC_DASH_Error_Code__c errorCode_AgencyCode = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_AGENCYCODE_EXP);
                conRec.adderror(errorCode_AgencyCode.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Agency_Code + HDFC_DASH_Constants.COLON + conRec.HDFC_DASH_Agency_Code__c);            
            }
        }
        
        if(!listHsalesExteralId.isEmpty()){
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Emp_Number, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listHsalesExteralId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE + HDFC_DASH_Constants.STRING_AND);
        }
        else if(!listNonHsalesExteralId.isEmpty()){
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Executive_Code, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listNonHsalesExteralId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE + HDFC_DASH_Constants.STRING_AND);
        }
        mapCondition.put(HDFC_DASH_Constants.ACCOUNT_RECORDTYPE_NAME, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.AGENCY_CODE + HDFC_DASH_Constants.STRING_QUOTE);
        
        listCon = HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(listConFields,mapCondition);
        
        for(Contact conRec:listCon)
        {
            mapConEmpNo.put(conRec.HDFC_DASH_Emp_Number__c,conRec.Id);
            mapConExeCode.put(conRec.HDFC_DASH_Executive_Code__c,conRec.Id);
        }
        
        for(Contact conRec : listContacts)
        {
            if(mapAcc.containsKey(conRec.HDFC_DASH_Agency_Code__c))
            {
                conRec.AccountId = mapAcc.get(conRec.HDFC_DASH_Agency_Code__c).Id; 
            }
            if(String.isNotBlank(conRec.HDFC_DASH_ReportsTo_External_Id__c) && String.isNotBlank(conRec.HDFC_DASH_Agency_Code__c) && mapAcc.containsKey(conRec.HDFC_DASH_Agency_Code__c))
            {
                if((mapConEmpNo.containsKey(conRec.HDFC_DASH_ReportsTo_External_Id__c) && dsa_Group == HDFC_DASH_Constants.HDFC_SALES) || (mapConExeCode.containsKey(conRec.HDFC_DASH_ReportsTo_External_Id__c) && dsa_Group != HDFC_DASH_Constants.HDFC_SALES))
                {
                    if(dsa_Group == HDFC_DASH_Constants.HDFC_SALES)
                    {
                        conRec.ReportsToId = mapConEmpNo.get(conRec.HDFC_DASH_ReportsTo_External_Id__c);
                    }
                    else if(dsa_Group != HDFC_DASH_Constants.HDFC_SALES)
                    {
                        conRec.ReportsToId = mapConExeCode.get(conRec.HDFC_DASH_ReportsTo_External_Id__c);
                    }
                }
                else{
                    
                    HDFC_DASH_Error_Code__c errorCode_ReportsTo_External_Id = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_REPORTSTO_EXTERNAL_ID_EXP);
                    conRec.adderror(errorCode_ReportsTo_External_Id.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_ReportsTo_ID + HDFC_DASH_Constants.COLON + conRec.HDFC_DASH_ReportsTo_External_Id__c);            
                }
            }
        }
    }
    /* 
Method Name :filterContactsForCreatingUser
Description :This method would be called to filter the list of Contacts for which have the ReportsToID field.
*/
    public static List<Contact> filterContactsForCreatingUser()
    {
        List<Contact> listContacts = new List<Contact>();
        List<Contact> listUpdateContact = new List<Contact>();
        List<String> listAccountid = new List<String>();
        listContacts = trigger.new;  
        List<String> listAccFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_DSA_Group,HDFC_DASH_Constants.OWNER_USERROLEID};
            
            for(Contact conRec:listContacts){
                listAccountid.add(conRec.AccountId);
            }
        Map<String,Account> mapAccounts = new Map<String,Account>(HDFC_DASH_Account_Selector.getAccountsBasedOnIds(listAccFields,listAccountid));
        
        if(!mapAccounts.isEmpty())
        {
            for(Contact conRec:listContacts)
            {                
                if((mapAccounts.get(conRec.AccountId).HDFC_DASH_DSA_Group__c == HDFC_DASH_Constants.HDFC_SALES)  && (conRec.HDFC_DASH_Function_Role_Code__c == HDFC_DASH_Constants.STRING_TL || conRec.HDFC_DASH_Function_Role_Code__c == HDFC_DASH_Constants.STRING_TE))
                {   
                    listUpdateContact.add(conRec);
                }
            }
        }
        return listUpdateContact;
    } 
}