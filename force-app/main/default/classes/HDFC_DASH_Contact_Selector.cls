/**
* 	className: HDFC_DASH_Contact_Selector
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class would create the select queries for Contact object .
**/
public inherited sharing class HDFC_DASH_Contact_Selector {
    
    private static final string QUERY_PARAMETER = '=:queryParameters';
    
    /* 
Method Name :getContactsBasedOnIds
Parameters  :List<string> fieldList,List<ID> conIds
Description :This method would get the contacts based on the IDs.
*/
    public static List<Contact> getContactsBasedOnIds(List<string> fieldList,List<ID> conIds){
        
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_CONTACT_WHERE+ HDFC_DASH_CONSTANTS.STRING_ID + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(conIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<Contact> conList = database.query(querystring); 
        return conList;
    }
    
    /* 
Method Name :getContactBasedOnId
Parameters  :List<string> fieldList, Id contactId
Description :This method would get a single Contact based on the ID.
*/
    public static Contact getContactBasedOnId(List<string> fieldList, Id contactId)
    {    
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_CONTACT_WHERE+ HDFC_DASH_CONSTANTS.STRING_ID+ HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ contactId +HDFC_DASH_Constants.STRING_QUOTE;
        Contact conRec = database.query(querystring); 
        return conRec;
    }
    
    /* 
Method Name :getContactsBasedOnQuery
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
Description :This method would get the List of contacts based on fieldsAndparameters sent to it.
*/
    public static List<Contact> getContactsBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_CONTACT_WHERE +getCondition(fieldsAndparameters) ;
        List<Contact> conList = database.query(querystring); 
        return conList;   
    }
    
    /* 
Method Name :getCondition
Parameters  :Map<String,String> fieldsAndparameters
Description :This method would be called from getContactsBasedOnQuery .
*/
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }
    
    /* 
Method Name :getContacts
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of Contacts based on the condition.
*/
    public static List<Contact> getContacts(List<string> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + HDFC_DASH_CONSTANTS.STRING_FROM_CONTACT_WHERE + conditionOn + QUERY_PARAMETER;  
        List<Contact> resultList = database.query(querystring);        
        
        return resultList;
    } 
     /* 
Method Name :getContactsWithMultipleList
Parameters  :List<string> queryParameters, List<string> fieldList, List<String> conditionOn
Description :This method would get the List of Contacts based on the condition.
*/
public static List<Contact> getContactsWithMultipleList(List<string> fieldList, Map<String,List<String>> mapConOnList,String orAnd)
{   
    string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + HDFC_DASH_CONSTANTS.STRING_FROM_CONTACT_WHERE;  
    string queryString2 = '';
    for(String condition : mapConOnList.keySet()){
    String queryCondition = getQueryConditionIn(mapConOnList.get(condition));
    queryString2 = queryString2 + condition + HDFC_DASH_CONSTANTS.STRING_IN + queryCondition + orAnd;    
    }
    querystring = querystring+queryString2;
    querystring = querystring.substring(HDFC_DASH_Constants.INT_ZERO, querystring.length() - HDFC_DASH_Constants.INT_FOUR);
    List<Contact> resultList = database.query(querystring);         
    return resultList;
}
    /* 
Method Name :getQueryConditionIn
Parameters  :List<string> valuesList
Description :This method would get the contacts based on the condition.
*/
public static String getQueryConditionIn(List<String> valuesList){
    String queryString;
    if(valuesList != null && valuesList.size() > HDFC_DASH_Constants.INT_ZERO){
        queryString = HDFC_DASH_Constants.STRING_OPEN_BRACE;
        for(String str: valuesList){
            queryString += HDFC_DASH_Constants.STRING_QUOTE + str + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.COMMA;
        }
        queryString = queryString.substring(HDFC_DASH_Constants.INT_ZERO,querystring.length()- HDFC_DASH_Constants.INT_ONE);
        queryString += HDFC_DASH_Constants.STRING_CLOSING_BRACE;
    }

    if(queryString == null){
        queryString = HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        return queryString;
    }
    return queryString;
}
    
    
    
     /* 
    Method Name :runConQueryForDedupePanNull
    Parameters  :List<String> fieldList,List<Date> dateOfBirth,List<String> emailList,List<String> firstNameList,List<String> lastNameList,List<String> mobileList
    Description :This method would return list of contacts for a a dedupe senario where we need to fetch the details based on DOB,Email,Mobile,FirstName,LastName.
    */
        public static  List<Contact> runConQueryForDedupePanNull(List<String> fieldList,List<String> dateOfBirth,List<String> emailList,List<String> firstNameList,List<String> lastNameList,List<String> mobileList){ 
            string querystring =HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_CONTACT_WHERE +
                HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth +HDFC_DASH_CONSTANTS.STRING_IN +
                HDFC_DASH_Constants.STRING_OPEN_BRACE+String.join(dateOfBirth,HDFC_DASH_Constants.COMMA)+ HDFC_DASH_Constants.STRING_CLOSING_BRACE +HDFC_DASH_Constants.STRING_OR +
                HDFC_DASH_Constants.STRING_FirstName+HDFC_DASH_CONSTANTS.STRING_IN +
                HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_QUOTE+String.join(firstNameList,HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+ HDFC_DASH_Constants.STRING_CLOSING_BRACE +HDFC_DASH_Constants.STRING_OR +
                HDFC_DASH_Constants.STRING_LastName +HDFC_DASH_CONSTANTS.STRING_IN +
                HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_QUOTE+String.join(lastNameList,HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) +HDFC_DASH_Constants.STRING_QUOTE+ HDFC_DASH_Constants.STRING_CLOSING_BRACE +HDFC_DASH_Constants.STRING_OR +
                HDFC_DASH_Constants.STRING_Email+HDFC_DASH_CONSTANTS.STRING_IN +
                HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_QUOTE+ String.join(emailList,HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) +HDFC_DASH_Constants.STRING_QUOTE+ HDFC_DASH_Constants.STRING_CLOSING_BRACE +HDFC_DASH_Constants.STRING_OR +
                HDFC_DASH_Constants.STRING_MobilePhone+HDFC_DASH_CONSTANTS.STRING_IN +
                HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_QUOTE+ String.join(mobileList,HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+ HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
            List<Contact> conRecs = database.query(querystring); 
            
            return conRecs;
        } 
    
}