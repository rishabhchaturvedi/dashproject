/* 
ClassName:  HDFC_DASH_TestDataFactory_EmailMessage
DevelopedBy: Kumar Gourav
Date: 14 Dec 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Email Message object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_EmailMessage 
{
    public static final String SUBJECT = 'HELLO';
    public static final String FROMNAME = 'Client';
    public static final String TOADDRESS = 'name@email.com';
    public static final String FROMADDRESS = 'name2@email.com';
    public static final String STATUS = '3';
    
    /* Method Name: createEmailMessage
parameters: ID appId
Method Description: This method is to create test data for Email Message.
Pass fields of Email Message Object
*/
    public static EmailMessage createEmailMessage(ID appId)
    {
        EmailMessage objEmailMes = new EmailMessage();
        
        objEmailMes.Status = STATUS;
        objEmailMes.FromName = FROMNAME;
        objEmailMes.Subject = SUBJECT;
        objEmailMes.ToAddress = TOADDRESS;
        objEmailMes.FromAddress = FROMADDRESS;
        objEmailMes.RelatedToId = appId;
        return objEmailMes;        
    }
    
    /* Method Name: getBasicEmailMes
parameters: ID appId
Method Description: This method is to generate test data for Email Message.
*/
    public static EmailMessage getBasicEmailMes(ID appId)
    {
        EmailMessage emailMessageDetails = HDFC_DASH_TestDataFactory_EmailMessage.createEmailMessage(appId);
        if(Schema.sObjectType.EmailMessage.isCreateable()){
            Database.insert(emailMessageDetails);
        }
        return emailMessageDetails;
    }  
}