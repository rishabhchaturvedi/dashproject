/*
	Author: Shruthi Sai Akkireddy
	Class: HDFC_DASH_APIResponse_StoreApp
	Description: Apex Class for handling success message for Store Application Details API
*/
public class HDFC_DASH_APIResponse_StoreApp {
    public String successMessage; 
    public Application application;
    public List<CoApplicant> coApplicant;
    public HDFC_DASH_ParseApplication applicationDetails;
    /*Application Node*/	
    public class Application{
            public String applicationId;
            
        }
	/*CoApplicant Node*/
	public class CoApplicant {
		public String sfApplicantId;
        public String sfCustomerId;
        public String sfCustomerNumber;
		public Boolean isExistingCustomer;
		public Boolean detailsMatching;
		public String customerMobile;
		public String customerEmail;
	}
	
}