/**
className: HDFC_DASH_SO_BSA_SelectorTest
DevelopedBy: Utkarsh Patidar
Date: 08 Nov 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_SO_BSA_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_SO_BSA_SelectorTest {
    
//Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static HDFC_DASH_SO_BSA_Association__c bsa = HDFC_DASH_TestDataFactory_SO_BSA.getBasicSoBsa(acc.ID);
    private static final string SUCCESS ='Success';
    
     /* 
    Method Name :testGetBSA
    Description :This method will test the method getBSA().
    */
    static testmethod void testGetBSA(){
        List<HDFC_DASH_SO_BSA_Association__c> resBSARec = new List<HDFC_DASH_SO_BSA_Association__c>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest(); 
            resBSARec = HDFC_DASH_SO_BSA_Selector.getBSA(new List<String> {BSA.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, resBSARec);
            system.assertEquals(1,resBSARec.size());  
        }
    }
    /* 
    Method Name :testGetBSABasedOnQuery
    Description :This method will test the method getBSABasedOnQuery().
    */
    static testmethod void testGetBSABasedOnQuery(){
        List<HDFC_DASH_SO_BSA_Association__c> listBSARec  = new List<HDFC_DASH_SO_BSA_Association__c>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.Id_STRING =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                BSA.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listBSARec = HDFC_DASH_SO_BSA_Selector.getBSABasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listBSARec);
        system.assertEquals(1,listBSARec.size(),SUCCESS);       
      }
        
}