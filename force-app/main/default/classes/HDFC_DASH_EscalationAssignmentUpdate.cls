public with sharing class HDFC_DASH_EscalationAssignmentUpdate {

    Public static void UpdateOwnerEscalation(case c)
    {
        if(HDFC_DASH_TriggerOpsRecursionFlags.Case_Escalation_Update){
            
        Id varOwnerID;
        system.debug('@@@@ HDFC_DASH_EscalationAssignmentUpdate for case Number '+ c.CaseNumber);
        varOwnerID =HDFC_DASH_GetUserForCaseEscalationNew.GetUSerForEscalation(c);
          system.debug('@@@@ HDFC_DASH_EscalationAssignmentUpdate varOwnerID '+ varOwnerID);
        if(c.OwnerId!=varOwnerID && varOwnerID!=null){
            //if(!System.isFuture()){
        
            updatecaseownerforescalation(c.Id,varOwnerID);
           // }
        }
        
        //disable recursion
       // HDFC_DASH_TriggerOpsRecursionFlags.Case_Escalation_Update = false;
            
        }        
       
    }
    //@future(callout=true)
    public static void updatecaseownerforescalation(Id CaseId,Id cOwnerId){
        system.debug('@@@@ cOwnerId '+ cOwnerId);
        Case c =[select id,ownerid from case where Id= : CaseId limit 1];
        c.OwnerId=cOwnerId;
        system.debug('@@@@ OwnerID Mapped to case '+ c.OwnerId);
        database.update(c);
        
        //disable recursion
        //HDFC_DASH_TriggerOpsRecursionFlags.Case_Escalation_Update = false;
    }
}