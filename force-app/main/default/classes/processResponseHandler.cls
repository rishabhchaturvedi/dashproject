/* ================================================
 * @Class Name :  processResponseHandler
 * @author : Accenture
 * @Purpose: This purpose of this class is to handle doRetryProcess method
 * @created date:
 ================================================*/
 public virtual class processResponseHandler{
     /***************************************************************************************************************
    *   @Name        :  doRetryProcess                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  This method will do retry process 
    ***************************************************************************************************************/
    public virtual void doRetryProcess(HttpResponse response, String ProcessData){}
}