/**
className: HDFC_DASH_Event_WOSharing_Selector
DevelopedBy:Tejeswari
Date: 21 December 2021
Company: Accenture
Class Description: This class would create the select queries for Event object.
**/
public without sharing class HDFC_DASH_Event_WOSharing_Selector {
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from Event ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    public static final string  QUERY_LIMIT = ' Limit';
    /* 
Method Name :getEvent
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get a list of Events based on one condition.
*/   
    public static List<Event> getEvent(List<String> queryParameters,List<string> fieldList, String conditionOn)
    {     
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<Event> listEvent = database.query(querystring); 
        return listEvent;
    }
    
    /* 
Method Name :getEventBasedOnQuery
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
Description :This method would get the List of Event  based on fieldsAndparameters sent to it.
*/
  /*  public static List<Event> getEventBasedOnQuery(List<String> fieldList, Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_CONSTANTS.COMMA) + FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters);
       
        List<Event> eventList = database.query(querystring); 
       
        return eventList;   
    } */
    
    /* 
Method Name :getEventBasedOnLimit
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters,Integer queryLimit
Description :This method would get the List of Event  based on fieldsAndparametersAndquerylimit sent to it.
*/
    public static List<Event> getEventBasedOnLimit(List<String> fieldList, Map<String,String> fieldsAndparameters, Integer queryLimit)
    {
        
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_CONSTANTS.COMMA) + FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) + QUERY_LIMIT + HDFC_DASH_CONSTANTS.STRING_SPACE + queryLimit;
        List<Event> eventList = database.query(querystring); 
       
        return eventList;
    }
    
    /* 
Method Name :getCondition
Parameters  :Map<String,String> fieldsAndparameters
Description :This method would be called from getEventBasedOnQuery.
*/
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }   
    
}