/**
className: HDFC_DASH_ApplicantDetailsTriggerOpsHelper
DevelopedBy: Anmol
Date: 15 July 2021
Company: Accenture
Class Description: Helper class for HDFC_DASH_ApplicantDetailsTriggerOps.
**/
public inherited sharing class HDFC_DASH_ApplicantTriggerOpsHelper {
    
    /* 
Method Name :executeOnInsert
Description :This method would excecute method's logic on insert.
*/      
    public static void executeOnInsert(HDFC_DASH_Applicant_Details__c[] appDetailRecs){
        Exception exp;
        List<String> fieldList=new List<String>{HDFC_DASH_Constants.STRING_ID,HDFC_DASH_Constants.HDFC_DASH_Application,
            HDFC_DASH_Constants.HDFC_DASH_PAN};
                List<String> queryParameters;
        Map<Id,List<String>> panMap = new  Map<Id,List<String>>();
        
        List<HDFC_DASH_Applicant_Details__c> primaryAndCoapplicantsList=new List<HDFC_DASH_Applicant_Details__c>();
        Set<String> panListSet;
        List<String> panList=new List<String>();
        List<String> panListfromUser;
        List<id> listOfApplication;
        String ApplicationId;
        
        try{
            for (HDFC_DASH_Applicant_Details__c ApplicantObj : appDetailRecs) 
            {
                if(ApplicantObj.HDFC_DASH_PAN__c != NULL){
                    ApplicationId=ApplicantObj.HDFC_DASH_Application__c;
                    if(panMap.containsKey(ApplicantObj.HDFC_DASH_Application__c)){
                        panListfromUser=panMap.get(ApplicantObj.HDFC_DASH_Application__c);
                        panListfromUser.add(ApplicantObj.HDFC_DASH_PAN__c);
                        panMap.put(ApplicantObj.HDFC_DASH_Application__c,PANListfromUser);
                        
                    }
                    else{
                        panListfromUser=new List<String>();     
                        panListfromUser.add(ApplicantObj.HDFC_DASH_PAN__c);
                        panMap.put(ApplicantObj.HDFC_DASH_Application__c,PANListfromUser);
                    }
                }
                
            }
            
            Map<String,String> fieldsAndparameters=new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Application => 
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+ApplicationId+
                HDFC_DASH_Constants.STRING_QUOTE};
                    
                    primaryAndCoapplicantsList =HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(fieldList,fieldsAndparameters);
            
            for(HDFC_DASH_Applicant_Details__c applicant:primaryAndCoapplicantsList)
            {
                if(applicant.HDFC_DASH_PAN__c!=null){
                    panList.add(applicant.HDFC_DASH_PAN__c);                   
                }   
            }
            
            for (HDFC_DASH_Applicant_Details__c appDetRec : appDetailRecs) 
            {
                panListfromUser=panMap.get(appDetRec.HDFC_DASH_Application__c);
                
                panListSet=new set<string>();
                panListSet.addAll(panList);
                panListSet.addAll(panListfromUser);
                
                if(panListSet.size()!=panList.size()+panListfromUser.size())   {
                    HDFC_DASH_Exceptionclass.Duplicate_PAN_Exception panexception = new HDFC_DASH_Exceptionclass.Duplicate_PAN_Exception();
                    panexception.setMessage(HDFC_DASH_Constants.DUPLICATE_PAN_FOUND);
                    appDetRec.addError(HDFC_DASH_Constants.PAN_SHOULD_BE_UNIQUE);
                    throw panexception;
                }
            }
        }
        catch(Exception e){
            
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_APPDETAILS,HDFC_DASH_Constants.DUPLICATE_PAN,NULL,expDetails);
        }
    } 
    
    /* 
Method Name :executeOnUpdate
Description :This method will run on Update trigger and will check for duplicate PAN
*/
    public static void executeOnUpdate(HDFC_DASH_Applicant_Details__c[] appDetailRecs)
    {        
        Exception exp;
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>();
        List<Opportunity> listApp = new List<Opportunity>();
        List<String> listAppDetailIds = new List<String>();
        List<String> listAppIds = new List<String>();
        
        Map<Id, Map<Id,HDFC_DASH_Applicant_Details__c>> mapNewTriggerRecords = new Map<Id, Map<Id,HDFC_DASH_Applicant_Details__c>>();   
        Map<Id, set<string>> mapAppToAllPan = new Map<Id, set<string>>();   
        
        try
        {
            for (HDFC_DASH_Applicant_Details__c objAppDetail : appDetailRecs) 
            {                
                listAppIds.add(objAppDetail.HDFC_DASH_Application__c);   
                listAppDetailIds.add(objAppDetail.id);
                
                Map<Id,HDFC_DASH_Applicant_Details__c> mapTemp = new Map<Id,HDFC_DASH_Applicant_Details__c>();
                if(mapNewTriggerRecords.containsKey(objAppDetail.HDFC_DASH_Application__c))
                {                   
                    mapTemp = mapNewTriggerRecords.get(objAppDetail.HDFC_DASH_Application__c);
                    mapTemp.put(objAppDetail.id, objAppDetail);
                    mapNewTriggerRecords.put(objAppDetail.HDFC_DASH_Application__c, mapTemp);
                }
                else
                { 
                    mapTemp.put(objAppDetail.id, objAppDetail);
                    mapNewTriggerRecords.put(objAppDetail.HDFC_DASH_Application__c, mapTemp);
                }   
            }
            
            List<String> listFields_AppDetails = new List<String>{HDFC_DASH_Constants.STRING_ID, HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.HDFC_DASH_PAN};
                List<String> listFields_App = new List<String>{HDFC_DASH_Constants.STRING_ID};         
                    
                    listApp = HDFC_DASH_Application_Selector.getApplication(listAppIds, listFields_App, HDFC_DASH_Constants.ID_STRING);
            listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppIds, listFields_AppDetails, HDFC_DASH_Constants.HDFC_DASH_Application);
            
            for (HDFC_DASH_Applicant_Details__c objAppDetail : listAppDetails) 
            {  
                Set<String> setPan = new Set<String>();
                
                if(objAppDetail.HDFC_DASH_PAN__c != null)
                {
                    if(mapAppToAllPan.containsKey(objAppDetail.HDFC_DASH_Application__c))
                    {                    
                        setPan = mapAppToAllPan.get(objAppDetail.HDFC_DASH_Application__c);
                        setPan.add(objAppDetail.HDFC_DASH_PAN__c);
                        mapAppToAllPan.put(objAppDetail.HDFC_DASH_Application__c, setPan);
                    }
                    else
                    { 
                        setPan.add(objAppDetail.HDFC_DASH_PAN__c);
                        mapAppToAllPan.put(objAppDetail.HDFC_DASH_Application__c, setPan);
                    }   
                }
            }
            
            for (Id appId : mapNewTriggerRecords.Keyset()) 
            {  
                Set<String> setPan = new Set<String>();
                setPan = mapAppToAllPan.get(appId);
                Map<Id, HDFC_DASH_Applicant_Details__c> mapNew_AllAppDetails = mapNewTriggerRecords.get(appId);
                
                if(setPan != null)
                {
                    //Checking for PAN duplication
                    for (Id appDetailId : mapNew_AllAppDetails.Keyset()) 
                    {                 
                        HDFC_DASH_Applicant_Details__c newAppDetail = mapNew_AllAppDetails.get(appDetailId);
                        
                        if(setPan.contains(newAppDetail.HDFC_DASH_PAN__c))
                        {
                            newAppDetail.addError(HDFC_DASH_Constants.PAN_SHOULD_BE_UNIQUE);                           
                            HDFC_DASH_Exceptionclass.Duplicate_PAN_Exception panexception = new HDFC_DASH_Exceptionclass.Duplicate_PAN_Exception();
                            panexception.setMessage(HDFC_DASH_Constants.DUPLICATE_PAN_FOUND);
                            throw panexception; 
                        }
                    }
                }
            }        
            
            //to populate contact fields on applicant details update
           // updateContactPassportDetBasedOnAppRec(appDetailRecs);
        }
        catch(Exception e)
        {
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_APPDETAILS,HDFC_DASH_Constants.DUPLICATE_PAN,NULL,expDetails);
        }
    }
    
    /* 
Method Name :fetchOldContacts
Description :This method will fetchOldContacts.
*/
    public static List<HDFC_DASH_Applicant_Details__c> fetchContacts()
    { 
        List<HDFC_DASH_Applicant_Details__c> appDetRecs = new List<HDFC_DASH_Applicant_Details__c>();
        try{
           
           // Map<Id,HDFC_DASH_Applicant_Details__c> mapAppDet = new Map<Id,HDFC_DASH_Applicant_Details__c>() ;
           // mapAppDet = (Map<Id,HDFC_DASH_Applicant_Details__c>) Trigger.oldMap;
            List<HDFC_DASH_Applicant_Details__c> appNewRecs = new List<HDFC_DASH_Applicant_Details__c>();
            appNewRecs = (List<HDFC_DASH_Applicant_Details__c>)trigger.new;
            List<Id> contactIds = new List<Id>();
            //appDetRecs = trigger.oldMap;
            for(HDFC_DASH_Applicant_Details__c appDet : appNewRecs) { 
                HDFC_DASH_Applicant_Details__c oldMapDetail = (HDFC_DASH_Applicant_Details__c) Trigger.oldMap?.get(appDet.id);
                //if(appDet.HDFC_DASH_Contact__c != NULL && !appDet.HDFC_DASH_Application_Created_From_ILPS__c){
                if(appDet.HDFC_DASH_Contact__c != oldMapDetail?.HDFC_DASH_Contact__c && appDet.HDFC_DASH_Contact__c != NULL && !appDet.HDFC_DASH_Application_Created_From_ILPS__c){
                    appDetRecs.add(appDet);
                  
                
                }
            }
           /* List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Contact,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.USER_CONSENT_FOR_CIBIL};
                appDetRecs = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(
                    contactIds, fieldList, HDFC_DASH_Constants.HDFC_DASH_Contact);*/
           
        }
        catch(Exception e)
        {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_APPDETAILS,HDFC_DASH_Constants.UPDATE_IS_CUSTOMER,NULL,expDetails);
        }
        return appDetRecs; 
    }
    /*
    Method Name :filterforPanIsUpdated
    Description :This method will filter the records in which PAN is updated.
    */
    public static List<HDFC_DASH_Applicant_Details__c> filterforPanIsUpdated(){
       
        List<HDFC_DASH_Applicant_Details__c> listPANUpdatedRecords = new List<HDFC_DASH_Applicant_Details__c>();
        List<HDFC_DASH_Applicant_Details__c> listNewApplDetails =Trigger.new;
        for(HDFC_DASH_Applicant_Details__c objAppDetail : listNewApplDetails)
            {  
                //HDFC_DASH_Applicant_Details__c newMapData = (HDFC_DASH_Applicant_Details__c) Trigger.newMap.get(objAppDetail.Id);
            	HDFC_DASH_Applicant_Details__c oldMapData = (HDFC_DASH_Applicant_Details__c) Trigger.oldMap?.get(objAppDetail.Id);
                
                if(objAppDetail.HDFC_DASH_PAN__c != oldMapData?.HDFC_DASH_PAN__c && (!objAppDetail.HDFC_DASH_Application_Created_From_ILPS__c))
   
                {  
                    listPANUpdatedRecords.add(objAppDetail);
                }
            }
   
        return listPANUpdatedRecords;
    }
    /* 
Method Name :updateConRecBasedOnAppRec
Description :This method will update the contact records based on applicant detail records.
*/
    public static void updateConRecBasedOnAppRec(List<HDFC_DASH_Applicant_Details__c> appDetRecs){
        try{
            
            Map<Id,Contact> conToUpdate = new Map<Id,Contact>();
            Set<Contact> conToUpdateIfTrue = new Set<Contact>();
            for(HDFC_DASH_Applicant_Details__c appDetRec : appDetRecs){
                Contact conRec = new Contact();
                if((appDetRec.HDFC_DASH_User_Consent_For_CIBIL__c).equals(HDFC_DASH_Constants.STRING_NO)){
                    conRec.id = appDetRec.HDFC_DASH_Contact__c;
                    conRec.HDFC_DASH_Is_Customer__c = false;
                   
                    if(conToUpdate.get(conRec.id) == NULL){
                        conToUpdate.put(conRec.id,conRec);
                    }
                }else if((appDetRec.HDFC_DASH_User_Consent_For_CIBIL__c).equals(HDFC_DASH_Constants.STRING_YES)){
                    
                    conRec.id = appDetRec.HDFC_DASH_Contact__c;
                    conRec.HDFC_DASH_Is_Customer__c = true;
                   
                    if(conToUpdate.get(conRec.id) != NULL){
                        conToUpdate.remove(conRec.id);
                        conToUpdate.put(conRec.id,conRec);
                    }else{
                        conToUpdate.put(conRec.id,conRec);
                    }
                }
            }
            if(Schema.sObjectType.Contact.isUpdateable()){
                update conToUpdate.values();
            }
        }
        catch(Exception e)
        {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_APPDETAILS,HDFC_DASH_Constants.UPDATE_IS_CUSTOMER,NULL,expDetails);
        }  
    }
    
      /* 
    Method Name :updateContactPassportDetBasedOnAppRec
    Description :This method will run on Update trigger and will update contact PassportDetails Based applicant Details
    */
   /* public static void updateContactPassportDetBasedOnAppRec(List<HDFC_DASH_Applicant_Details__c> appDetRecs){
        System.debug('xxx In updateContactPassportDetBasedOnAppRec '+appDetRecs);
        System.debug('xxx In Length '+appDetRecs.size());
        try{
        Map<Id,Account> conToUpdate = new Map<Id,Account>();
        for(HDFC_DASH_Applicant_Details__c appDetRec : appDetRecs) 
        {   
            System.debug('xxx In HDFC_DASH_Applicant_Details__c '+appDetRec);
            //Contact conRec = new Contact();
            Account conRec = new Account(); 
            List<String> ConFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,'AccountId '};
            Contact conRec1 = HDFC_DASH_Contact_Selector.getContactBasedOnId(ConFieldList,appDetRec.HDFC_DASH_Contact__c);
            system.debug('ConRec1--->'+conRec1);
            conRec.id = conRec1.AccountId ;
            conRec.HDFC_DASH_Passport_FirstName__pc= appDetRec.HDFC_DASH_Passport_FirstName__c;
            conRec.HDFC_DASH_Passport_LastName__pc= appDetRec.HDFC_DASH_Passport_LastName__c;
            conRec.HDFC_DASH_Passport_MiddleName__pc= appDetRec.HDFC_DASH_Passport_MiddleName__c;
            conRec.HDFC_DASH_Passport_Nationality__pc= appDetRec.HDFC_DASH_Passport_Nationality__c;
            conRec.HDFC_DASH_Passport_Number__pc= appDetRec.HDFC_DASH_Passport_Number__c;
            conRec.HDFC_DASH_Passport_Date_Of_Expiry__pc= appDetRec.HDFC_DASH_Passport_Date_Of_Expiry__c;
            conRec.HDFC_DASH_Highest_Qualification__pc = appDetRec.HDFC_DASH_Highest_Qualification__c;
            conRec.HDFC_DASH_Highest_Qualification_Code__pc = appDetRec.HDFC_DASH_Highest_Qualification_Code__c;
            conRec.HDFC_DASH_Marital_Status__pc = appDetRec.HDFC_DASH_Marital_Status__c;
            conRec.HDFC_DASH_Social_Category__pc = appDetRec.HDFC_DASH_Social_Category__c;
            conRec.HDFC_DASH_Fixed_Monthly_Income__pc = appDetRec.HDFC_DASH_SO_Fixed_Monthly_Income__c;
            conRec.HDFC_DASH_Capacity__pc = appDetRec.HDFC_DASH_Applicant_Capacity__c;
            conRec.HDFC_DASH_Father_First_Name__pc = appDetRec.HDFC_DASH_Father_First_Name__c;
            conRec.HDFC_DASH_Father_Middle_Name__pc = appDetRec.HDFC_DASH_Father_Middle_Name__c;
            conRec.HDFC_DASH_Father_Last_Name__pc = appDetRec.HDFC_DASH_Father_Last_Name__c;
                      
            system.debug('con====false==='+conRec);
            if(conToUpdate.get(conRec.id) == NULL){
                conToUpdate.put(conRec.id,conRec);
            }
          
        }
        update conToUpdate.values();
    }
    catch(Exception e)
    {
        String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
            + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            system.debug('expDetails-->'+expDetails);
        HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_APPDETAILS,HDFC_DASH_Constants.UPDATE_IS_CUSTOMER,NULL,expDetails);
    }  
    }*/
}