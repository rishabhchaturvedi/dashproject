/**
className: HDFC_DASH_AggregateResultIterable
DevelopedBy: Anisha Arumugam
Date: 27 January 2022
Company: Accenture 
Class Description: This class would construct the Query for Batch notification.
**/
global class HDFC_DASH_AggregateResultIterable implements Iterable<AggregateResult> {
    private String query;
    
    /**
ConstructorName : HDFC_DASH_AggregateResultIterable
* Parameters : String query
* Description : It is a constructor for class HDFC_DASH_AggregateResultIterable.
*/ 
    global HDFC_DASH_AggregateResultIterable(String soql){
        query = soql;
    }
    
    /* 
Method Name :Iterator
Description :This method would call the HDFC_DASH_AggregateResultIterator class
*/   
    global Iterator<AggregateResult> Iterator(){
        return new HDFC_DASH_AggregateResultIterator(query);
    }
}