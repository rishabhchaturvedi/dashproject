@RestResource(urlMapping='/hdfc/getleapdashboard/V1.0/*')
/*
Author: Tejeswari
Class Name:HDFC_DASH_RestResource_GetLeapDashboard
*/
global with sharing class HDFC_DASH_RestResource_GetLeapDashboard {
    @HttpGet
    /*
Method:getLeapDashboard()
Description:This method will get the list of Applications
*/
    global static void getLeapDashboard() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders = new Map<string,String>();
        String response;
        Exception exp;
        HDFC_DASH_APIResponse_GetleapDashboard apiResponse = new HDFC_DASH_APIResponse_GetleapDashboard();  
        try
        {    
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestSer_GetLeapDash.getLeapDashboardDetails(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
            
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_LEAPDASHBOARD, HDFC_DASH_Constants.HANDLE_LEAPDASHBOARD, HDFC_DASH_Constants.METHOD_TYPE_GET, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_GETLEAPDASHBOARD, request.requestBody.tostring(), (res.responseBody)?.tostring(), 
                                                                HDFC_DASH_Constants.METHOD_TYPE_GET, HDFC_DASH_Constants.HANDLE_LEAPDASHBOARD, exp, request.headers);        
            
            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                    res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }        
    }    
}