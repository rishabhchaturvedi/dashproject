/**
className: HDFC_DASH_RestResource_QuoteInfoTest
DevelopedBy: Punam Marbate
Date: 14 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_QuoteInfo and HDFC_DASH_InboundRestService_QuoteInfo.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_RestResource_QuoteInfoTest {

    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string STRING_POST ='PATCH';
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Contact_Address_Details__c addressDetail=
                                  HDFC_DASH_TestDataFactory_ContAddDetail.getBasicContAddressDetail(con.Id);
    private static List <HDFC_DASH_Master_Table__c> masterData = HDFC_DASH_TestDataFactory_MasterTables.getBasicMasterTable();                                
    private static HDFC_DASH_City_Master__c cityMasterData = HDFC_DASH_TestDataFactory_MasterTables.getBasicCityMaster(); 
    private static HDFC_DASH_Pincode_Master__c pinCodeMasterData = HDFC_DASH_TestDataFactory_MasterTables.getBasicPincodeMaster();
    private static final string REQUEST_BODY = '{"QuoteInfo":{"sfApplicationID":"'+ app.Id+'","sfApplicationNumber":"'+ app.HDFC_DASH_ApplicationNumber__c+'","sfApplicantID":"'+appDetails.Id+'","prePopulatedAddressDetails":{"addressLine1":"","addressLine2":"","addressLine3":"","addressLine4":"","landmark":"","city":"","cityCode":"'+cityMasterData.HDFC_DASH_City_Code__c+'","taluka":"","district":"","state":"","stateCode":"JK","Country":"","countryCode":"INDIA","pinCode":"'+pinCodeMasterData.HDFC_DASH_Pincode__c+'","postOfficeName":""},"customerSelectedAddressDetails":{"addressLine1":"","addressLine2":"","addressLine3":"","addressLine4":"","landmark":"","city":"ACHABAL","cityCode":"'+cityMasterData.HDFC_DASH_City_Code__c+'","taluka":"","district":"","state":"","stateCode":"KA","country":"","countryCode":"INDIA","pinCode":"'+pinCodeMasterData.HDFC_DASH_Pincode__c+'","postOfficeName":""},"addressType":"C","addressSource":"Passport","addressChangedByCustomer":"Yes","totalExistingEMI":"80000","loanAmount":"80000","noEMIPaid":"No","eligibilityAmount":{"loan_product":"511","loan_term":"240","requested_loan":"100000","current_roi":"7.3","possible_term":"180","stretched_term":"204","loan_possible":"90000","stretched_amt":"100000","possible_emi":"9000","stretched_emi":"10000","concession":"","normal_iir":"55","normal_foir":"55","max_iir":"55","max_foir":"55","campaign_code":"","niir":"29","nfoir":"35","nlcr":"71","nlvr":"67","comb_lcr_ltv":"67","ltv_val":"71","plr_rate":"16.05","plr_id":"P1"},"customerSelectedSpotOffer":{"customerSelectedLoanAmount":"5000000","customerSelectedEMIAmount":"30000","customerSelectedInterestType":"Floating","customerSelectedInterestRateMin":"6.75","customerSelectedInterestRateMax":"7","customerSelectedTenureInMonths":"180"},"spotOfferEligible":"Y","spotOfferAccepted":"Yes"}}';
    private static final string REQUEST_BODY_WITHOUT_APPLICATION = '{"QuoteInfo":{"sfApplicationID":"","sfApplicationNumber":"","sfApplicantID":"","prePopulatedAddressDetails":{"addressLine1":"","addressLine2":"","addressLine3":"","addressLine4":"","landmark":"","city":"","cityCode":"","taluka":"","district":"","state":"","stateCode":"","Country":"","countryCode":"","pinCode":"","postOfficeName":""},"customerSelectedAddressDetails":{"addressLine1":"","addressLine2":"","addressLine3":"","addressLine4":"","landmark":"","city":"","cityCode":"","taluka":"","district":"","state":"","stateCode":"","country":"","countryCode":"","pinCode":"","postOfficeName":""},"addressType":"C","addressSource":"Passport","addressChangedByCustomer":"Yes","totalExistingEMI":"80000","loanAmount":"80000","noEMIPaid":"No","eligibilityAmount":{"loan_product":"511","loan_term":"240","requested_loan":"100000","current_roi":"7.3","possible_term":"180","stretched_term":"204","loan_possible":"90000","stretched_amt":"100000","possible_emi":"9000","stretched_emi":"10000","concession":"","normal_iir":"55","normal_foir":"55","max_iir":"55","max_foir":"55","campaign_code":"","niir":"29","nfoir":"35","nlcr":"71","nlvr":"67","comb_lcr_ltv":"67","ltv_val":"71","plr_rate":"16.05","plr_id":"P1"},"customerSelectedSpotOffer":{"customerSelectedLoanAmount":"5000000","customerSelectedEMIAmount":"30000","customerSelectedInterestType":"Floating","customerSelectedInterestRateMin":"6.75","customerSelectedInterestRateMax":"7","customerSelectedTenureInMonths":"180"},"spotOfferEligible":"Y","spotOfferAccepted":"Yes"}}';
    private static final string REQUEST_BODY_WITH_APPLICATION = '{"QuoteInfo":{"sfApplicationID":"006720000006OyvAAE","sfApplicationNumber":"10000002","sfApplicantID":"a06720000006P0XAAU","prePopulatedAddressDetails":{"addressLine1":"","addressLine2":"","addressLine3":"","addressLine4":"","landmark":"","city":"","cityCode":"","taluka":"","district":"","state":"","stateCode":"","Country":"","countryCode":"","pinCode":"","postOfficeName":""},"customerSelectedAddressDetails":{"addressLine1":"","addressLine2":"","addressLine3":"","addressLine4":"","landmark":"","city":"","cityCode":"","taluka":"","district":"","state":"","stateCode":"","country":"","countryCode":"","pinCode":"","postOfficeName":""},"addressType":"C","addressSource":"Passport","addressChangedByCustomer":"Yes","totalExistingEMI":"80000","loanAmount":"80000","noEMIPaid":"No","eligibilityAmount":{"loan_product":"511","loan_term":"240","requested_loan":"100000","current_roi":"7.3","possible_term":"180","stretched_term":"204","loan_possible":"90000","stretched_amt":"100000","possible_emi":"9000","stretched_emi":"10000","concession":"","normal_iir":"55","normal_foir":"55","max_iir":"55","max_foir":"55","campaign_code":"","niir":"29","nfoir":"35","nlcr":"71","nlvr":"67","comb_lcr_ltv":"67","ltv_val":"71","plr_rate":"16.05","plr_id":"P1"},"customerSelectedSpotOffer":{"customerSelectedLoanAmount":"5000000","customerSelectedEMIAmount":"30000","customerSelectedInterestType":"Floating","customerSelectedInterestRateMin":"6.75","customerSelectedInterestRateMax":"7","customerSelectedTenureInMonths":"180"},"spotOfferEligible":"Y","spotOfferAccepted":"Yes"}}';

    /* 
    Method Name : testStoreQuoteInfo
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_QuoteInfo.storeQuoteInfo().
    */
    static testmethod void testStoreQuoteInfo(){
        System.runAs(sysAdmin)
        {
            //List<HDFC_DASH_City_Master__c> listcitymasterRec = new List<HDFC_DASH_City_Master__c>{cityMasterData} ;
            //List<HDFC_DASH_Pincode_Master__c> listPincodeMastRec = new List<HDFC_DASH_Pincode_Master__c>{pinCodeMasterData};
            //Id recordTypeIdStateCode = Schema.SObjectType.HDFC_DASH_Master_Table__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_STATECODE).getRecordTypeId();
            //masterData[0].RecordTypeId=recordTypeIdStateCode;
            Database.update(masterData);
            Database.update(cityMasterData);
            Database.update(pinCodeMasterData);
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
            request.requestBody = Blob.valueOf(REQUEST_BODY);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_QuoteInfo.storeQuoteInfo();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }

    /* 
    Method Name : testException_APP_IDS_MISSING
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_QuoteInfo.storeQuoteInfo() when ApplicationId is null.
    */
    static TestMethod void testException_APP_IDS_MISSING() 
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            Exception testException;          
            RestRequest request = new RestRequest();
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITHOUT_APPLICATION);       
            RestContext.request = request;
            Test.startTest();   
            try
            {
                HDFC_DASH_RestResource_QuoteInfo.storeQuoteInfo();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    }

    /* 
    Method Name : testException_APPLICATIONID_DOESNOTEXIST
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_QuoteInfo.storeQuoteInfo() when when ApplicationId is  not null.
    */
    static TestMethod void testException_APPLICATIONID_DOESNOTEXIST() 
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            Exception testException;    
            RestRequest request = new RestRequest();
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITH_APPLICATION);       
            RestContext.request = request;
            Test.startTest();
            try
            {
                HDFC_DASH_RestResource_QuoteInfo.storeQuoteInfo();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    }
}