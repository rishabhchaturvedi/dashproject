/*
Author: Anisha Arumugam
Class: HDFC_DASH_Exceptionclass
Description: Apex Class for Exceptions
*/
public with sharing class HDFC_DASH_Exceptionclass {
/*
Class: API_header_Exception
Description: Apex Class to handle header exceptions
*/
public with sharing class API_header_Exception extends exception {}
    

/*
Class: API_RequestBody_Exception
Description: Apex Class to handle request body exceptions
*/
public with sharing class API_RequestBody_Exception extends exception {}


/*
Class: API_LMS_OR_ILPS_Error_Response_Exception
Description: Apex Class to handle LMS Error response exceptions
*/
public with sharing class API_LMS_OR_ILPS_Error_Response_Exception extends exception {
    public Integer errorCode; 
    public String errorDesc;
}
    
/*
Class: API_LMS_LEAD_ENRICHMENT_Error_Response_Exception
Description: Apex Class to handle LMS Error response exceptions
*/
public with sharing class API_LMS_LEAD_ENRICHMENT_Error_Response_Exception extends exception {}
    
/*
Class: Duplicate_PAN_Exception
Description: Apex Class to duplicate PAN exceptions
*/
public with sharing class Duplicate_PAN_Exception extends exception {}
    
/*
Class: API_RequestURL_Exception
Description: Apex Class to handle request URL exceptions
*/
public with sharing class API_RequestURL_Exception extends exception {}
    
/*
Class: API_Request_Exception
Description: Apex Class to handle API request exceptions
*/
public with sharing class API_Request_Exception extends exception {}
    
    /*
Class: CustomException
Description: Apex Class to handle all the custom exceptions
*/
public with sharing class CustomException extends exception {
    public Integer errorCode; 
    public String errorDesc;
}
   
    
}