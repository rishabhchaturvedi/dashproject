/**
className: HDFC_DASH_InsertFundSrc_StoreAppService
DevelopedBy: Janani Mohankumar
Date: 30 Aug 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the Funding Source details.
**/
public class HDFC_DASH_InsertFundSrc_StoreAppService {
    /*
Method Name :mapFundingSource
Parameters  :Id applicationID, HDFC_DASH_ParseApplication.fundingSource parseFundingSource
Description :This method would map all the funding source details node for storeApp API and sends us the fundingSource detail record.
*/
    public static HDFC_DASH_Funding_Source__c mapFundingSource(Id applicationID, HDFC_DASH_ParseApplication.fundingSource parseFundingSource)
    {
        HDFC_DASH_Funding_Source__c fundingSourceObj = new HDFC_DASH_Funding_Source__c();
        fundingSourceObj.Id = parseFundingSource?.sfFundingSourceId;
        fundingSourceObj.HDFC_DASH_Application__c = applicationID;
        fundingSourceObj.HDFC_DASH_Source_Amount__c = parseFundingSource?.fundingAmount;
        fundingSourceObj.HDFC_DASH_Source_Type__c = parseFundingSource?.fundingSourceType;
        fundingSourceObj.HDFC_DASH_Source_Type_Code__c = parseFundingSource?.fundingSourceTypeCode;
        fundingSourceObj.HDFC_DASH_ILPS_Funding_Source_Id__c = parseFundingSource?.ILPSFundingSourceID;
        return fundingSourceObj;
    }
   
       
    }