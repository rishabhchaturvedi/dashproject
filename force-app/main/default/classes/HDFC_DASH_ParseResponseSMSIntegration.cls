/*
Author: Utkarsh Patidar
Class:HDFC_DASH_ParseResponseSMSIntegration
Description: This class will parse the response body of SMS Integration
*/
public with sharing class HDFC_DASH_ParseResponseSMSIntegration 
{
    public List<OBJECTResponse> objectResponse;

/*
Class: ReturnMsg
Description: Class to parse Return Message
*/
	public with sharing class ReturnMsg {
		public String StatusCode;
		public String Message;
		public String refno;
	}

/*
Class: OBJECTResponse
Description: Class to parse Object Response
*/
	public with sharing class OBJECTResponse {
		public List<ReturnMsg> ReturnMsg;
	}

    /*
	Method: parse
	Parameters:String json
	Description: This method will parse the Response.
	*/ 
    public static HDFC_DASH_ParseResponseSMSIntegration parse(String json) {
		return (HDFC_DASH_ParseResponseSMSIntegration) System.JSON.deserialize(json, HDFC_DASH_ParseResponseSMSIntegration.class);
	}
}