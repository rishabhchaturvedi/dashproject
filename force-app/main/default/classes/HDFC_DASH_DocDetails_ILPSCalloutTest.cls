/*
Class: HDFC_DASH_DocDetails_ILPSCalloutTest
Author: Tejeswari
Date: 26 Oct 2021
Company: Accenture
Description:This is the test class for Queueable Class for InfoValidation Callout to ILPS.
*/
@isTest(SeeAllData=false)
private class HDFC_DASH_DocDetails_ILPSCalloutTest {
	private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Info_Validation__c infoValAppDetails = HDFC_DASH_TestDataFactory_AppInfoValid.getBasicApplInfoValidation(appDetails.Id,HDFC_DASH_Constants.STRING_B);
    private static HDFC_DASH_Info_Validation__c infoValApp = HDFC_DASH_TestDataFactory_AppInfoValid.getBasicInfoValWithApplication(app.Id);
    private static HDFC_DASH_Applicant_Details__c coAppDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Info_Validation__c infoValCoAppDetails = HDFC_DASH_TestDataFactory_AppInfoValid.getBasicApplInfoValidation(coAppDetails.Id,HDFC_DASH_Constants.STRING_C);
	
    /* Method Name: testInfoValILPSCallout
    parameter:none
    Method Description: This method will enqueue and test the InfoValidationS callout to ILPS.
    */ 
    static testmethod void testInfoValILPSCallout(){
        System.runAs(sysAdmin)
        {
            Id recordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.RECORDTYPE_COAPP).getRecordTypeId();
            coAppDetails.RecordTypeId = recordTypeId;
            Database.update(coAppDetails); 
            
            RestRequest request = new RestRequest();   
            RestResponse response = new RestResponse();
            
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockDocDetailsResGenerator(infoValApp.HDFC_DASH_Storage_Identifier__c,infoValAppDetails.HDFC_DASH_Storage_Identifier__c));
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_TestDataFactory_API.createInterfaceSetting_DD_ILPS();

            Test.startTest();
            HDFC_DASH_DocDetails_ILPSCallout documentInfoILPSCallOut  = new HDFC_DASH_DocDetails_ILPSCallout(app.Id,false);  
            ID jobID = System.enqueueJob(documentInfoILPSCallOut);
            Test.stopTest();   
            System.assertNotEquals(null, response);          
        }        
    }
}