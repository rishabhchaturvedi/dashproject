/**
* 	className: HDFC_DASH_RuleEngine_Selector
DevelopedBy: Kumar Gourav
Date: 4 Dec 2021
Company: Accenture
Class Description: This class would create the select queries for HDFC_DASH_Rule_Engine_Capture__c object .
**/
public with sharing class HDFC_DASH_RuleEngine_Selector {
    private static final string QUERY_PARAMETER = '=:queryParameters';
/* 
    Method Name :getRuleEngineValBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of HDFC_DASH_Rule_Engine_Capture__c based on fieldsAndparameters sent to it.
*/
        public static List<HDFC_DASH_Rule_Engine_Capture__c> getRuleEngineValBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_RULEENGINECAPTURE_WHERE +getCondition(fieldsAndparameters) ;
			List<HDFC_DASH_Rule_Engine_Capture__c> ruleEngineList = database.query(querystring); 
            return ruleEngineList;   
        }
        
        /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getruleEngineValBasedOnQuery .
    */
        public static String getCondition(Map<String,String> fieldsAndparameters){
            String condition ='';
            for(String fieldName : fieldsAndparameters.keySet()){
                condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
            }
            return condition;
        }
}