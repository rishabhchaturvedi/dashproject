/**
className: HDFC_DASH_Contact_Address_Selector
DevelopedBy: Punam Marbate
Date: 13 Aug 2021
Company: Accenture
Class Description: This class would create the select queries for Contact Address Details object .
**/
public inherited sharing class HDFC_DASH_Contact_Address_Selector {
    
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Contact_Address_Details__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';

    /* 
    Method Name :getContactAddressBasedOnId
    Parameters  :List<string> fieldList, Id ApplicationId
    Description :This method would get a single Contact Address Details based on the ID.
    */   
    /*public static HDFC_DASH_Contact_Address_Details__c getContactAddressBasedOnId(List<string> fieldList,
                                                                                    Id addressId){
        
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + 
                            FROM_OBJECT+ WHERE_STRING+ ID_STRING +
                            HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+
                            addressId+ HDFC_DASH_Constants.STRING_QUOTE;
        HDFC_DASH_Contact_Address_Details__c result = database.query(querystring);
        return result;
    }*/
     /* 
    Method Name :getContactAddressBasedOnIds
    Parameters  :List<string> fieldList,List<ID> addressIds
    Description :This method would get the Contact Address Details based on the IDs.
    */
    public static List<HDFC_DASH_Contact_Address_Details__c> getContactAddressBasedOnIds(List<string> fieldList,List<ID> addressIds){
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + 
                            FROM_OBJECT + WHERE_STRING + ID_STRING + HDFC_DASH_CONSTANTS.STRING_IN + 
                            HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+
                            String.join(addressIds, HDFC_DASH_Constants.STRING_QUOTE+
                            HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+
                            HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<HDFC_DASH_Contact_Address_Details__c> conAddressList = database.query(querystring); 
        return conAddressList;
    } 
    /* 
    Method Name :getContactAddress
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get the List of Contact Address based on the only one condition.
    */
    public static List<HDFC_DASH_Contact_Address_Details__c> getContactAddress( List<String> queryParameters, 
                                        List<string> fieldList, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + 
                                    FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;  
        List<HDFC_DASH_Contact_Address_Details__c> resultList = database.query(querystring);        
        
        return resultList;
    }
    /* 
    Method Name :getContactAddressBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Contact Address Details based on fieldsAndparameters sent to it.
    */
    public static List<HDFC_DASH_Contact_Address_Details__c> getContactAddressBasedOnQuery(List<String> fieldList,
                                                                            Map<String,String> fieldsAndparameters){
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
                    +FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<HDFC_DASH_Contact_Address_Details__c> resultList = database.query(querystring); 
        return resultList;   
    }  
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getContactAddressBasedOnQuery .
    */
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }
     
    /*Method Name :getAppAddressDetBasedOnAppDetailIds
    Parameters  :List<string> fieldList,List<ID> appDetailIds
    Description :This method would get the HDFC_DASH_Contact_Address_Details__c based on HDFC_DASH_Contact_Address_Details__c IDs.
    */
    public static List<HDFC_DASH_Contact_Address_Details__c> getContactAddressDetBasedOnContactIds(List<string> fieldList,List<ID> contactIds){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT + WHERE_STRING + HDFC_DASH_CONSTANTS.HDFC_DASH_Contact + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(contactIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
            List<HDFC_DASH_Contact_Address_Details__c> appAddDetList = database.query(querystring); 
            return appAddDetList;
    }

    /* 
    Method Name :getCADDetailsByLatestAndContactIds
    Parameters  :List<string> fieldList,List<ID> contactIds
    Description :This method would get the ContactAddressDetails based on the Latest and contactIDs.
    */
    /*public static List<HDFC_DASH_Contact_Address_Details__c> getCADDetailsByLatestAndContactIds(List<string> fieldList,List<ID> contactIds){
        
        //SELECT Id, name,HDFC_DASH_From_Date__c, HDFC_DASH_Latest__c, HDFC_DASH_To_Date__c  FROM HDFC_DASH_Contact_Address_Details__c WHERE HDFC_DASH_Latest__c = 'Yes' AND HDFC_DASH_Contact__c IN :conId
        
        String queryString = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + HDFC_DASH_CONSTANTS.STRING_FROM_CAD_WHERE
                            + HDFC_DASH_Constants.HDFC_DASH_Latest + HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_YES 
                            + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Contact + HDFC_DASH_Constants.STRING_IN 
                            + HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_QUOTE + String.join(contactIds, HDFC_DASH_Constants.STRING_QUOTE
                            +HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+ HDFC_DASH_Constants.STRING_QUOTE +HDFC_DASH_Constants.STRING_CLOSING_BRACE;

        List<HDFC_DASH_Contact_Address_Details__c> contactAddressDetailsList = database.query(querystring); 
        return contactAddressDetailsList;
    }*/

}