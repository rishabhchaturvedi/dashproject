public with sharing class HDFC_DASH_CaseCreationForDeposit {
    
     @TestVisible private static List<Deposit__x> mockedRequests = new List<Deposit__x>();
     
    @future(callout=true)
    public static void CaseDepositListener(Id caseRecId)
    {
        system.debug('@@@@ CaseId' + caseRecId );
        
        List<Case> caseRecs = new List<Case>();
        List<String> forwardedDomains = new List<String>();
        List<User> userListOwner = new List<User>();
        Boolean fwdFromHdfcDomain = HDFC_DASH_Constants.BOOLEAN_FALSE;
        String ownerNameQueue;
        String depositNo;
        List<Group> queueList = new List<Group>();
        List<Account> accList = new List<Account>();
        List<Deposit__x> depList = new List<Deposit__x>();
        Deposit__x depRec = new Deposit__x();
        Boolean containsDepositNumber=false;
        List<User> userList = new List<User>(); 
        List<String> depositNumbersInMail = new List<String>();
        List<String> userFieldsList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.USEREMAIL};
            try
        {
            string depositNumber;
            caseRecs = [SELECT Id,ownerId,Subject,HDFC_DASH_Home_Branch__c,HDFC_DASH_Service_Centre__c,HDFC_DASH_Deposit_Number__c,
                        ContactId,AccountId,Status,Origin,SuppliedEmail,IsClosed,HDFC_DASH_Skip_AutoAssignment__c,
                        HDFC_DASH_No_Further_Response__c,Deposit_Number__c,HDFC_DASH_Mode__c,HDFC_DASH_Sub_mode__c,Description FROM Case WHERE Id = :caseRecId];
            //system.debug('@@@@ Excep' + caseRecId );
            if(caseRecs[0].Origin=='Phone')
            {
                caseRecs[0].Deposit_Number__c=caseRecs[0].HDFC_DASH_Deposit_Number__c;
            }
            if(caseRecs[0].Deposit_Number__c!=null){
                if(!Test.isRunningTest()){
                depList = [SELECT Id,BORR_CUST_NO__c,CUST_NAME__c,DEPOSIT_AMT__c,DEPOSIT_NO__c,MATURITY_DATE__c,HOME_BRANCH__c,SERVICE_CENTER__c 
                           FROM Deposit__x WHERE DEPOSIT_NO__c = :caseRecs[0].Deposit_Number__c LIMIT 1];// DEPOSIT_NO__c = :depositNumbersInMail[0] LIMIT 1];
                           }else{
                                depList = new List<Deposit__x>(mockedRequests);
                               system.debug('mockedRequests...'+mockedRequests);
                           }
                depositNumber=caseRecs[0].Deposit_Number__c;
                system.debug('depList...'+depList);
                if(depList==null)
                {
                    depositNumber=depositNumber.replace('/', '');
                    depList = [SELECT Id,BORR_CUST_NO__c,CUST_NAME__c,DEPOSIT_AMT__c,DEPOSIT_NO__c,MATURITY_DATE__c,HOME_BRANCH__c,SERVICE_CENTER__c 
                               FROM Deposit__x WHERE DEPOSIT_NO__c = :depositNumber LIMIT 1];// DEPOSIT_NO__c = :depositNumbersInMail[0] LIMIT 1];
                    
                }
            }
            system.debug('depList...2'+depList);
            if(depList.size()>0 && depList!=null)
            {
                depRec = depList[0];
                containsDepositNumber = HDFC_DASH_Constants.BOOLEAN_TRUE;
                caseRecs[0].HDFC_DASH_Home_Branch__c = depRec.HOME_BRANCH__c;
                caseRecs[0].HDFC_DASH_Service_Centre__c = depRec.SERVICE_CENTER__c;
                accList = [SELECT Id,Name,Recordtype.Name,PersonContactId FROM Account WHERE HDFC_DASH_Customer_Number__c  = :depRec.BORR_CUST_NO__c AND 
                           Recordtype.Name = 'Person Account' LIMIT 1];
            }
            
            //fwd from hdfc employee
            //  if(caseRecs[0].Subject.startsWithIgnoreCase(Label.HDFC_DASH_Forwarded_Email_String) && fwdFromHdfcDomain){
            //  userList = HDFC_DASH_User_Selector.getUserRecBasedOnString(userFieldsList,caseRecs[0].SuppliedEmail,HDFC_DASH_Constants.USEREMAIL);
            if(containsDepositNumber)
            {
                if(accList.size() > 0)
                {
                    caseRecs[0].accountId = accList[0].Id;
                    caseRecs[0].ContactId = accList[0].PersonContactId;
                }
                caseRecs[0].HDFC_DASH_Deposit_Number__c = depRec.DEPOSIT_NO__c;
                if(userList.size() > 0)
                {
                    caseRecs[0].OwnerId = userList[0].Id;
                }
            }
            //deposit number not found in database
            else
            {
                if(caseRecs[0].ContactId != null)
                {
                    caseRecs[0].ContactId = null;
                }
                caseRecs[0].Status = Label.HDFC_DASH_Unregistered_Mail_Status;
            }//from register mail id
            if(caseRecs[0].ContactId != null && containsDepositNumber)
            {
                caseRecs[0].HDFC_DASH_Deposit_Number__c = depRec.DEPOSIT_NO__c;
            }
            //from unregister mail id
            else if(caseRecs[0].ContactId == null && containsDepositNumber)
            {
                if(accList.size() > 0)
                {
                    caseRecs[0].accountId = accList[0].Id;
                    caseRecs[0].ContactId = accList[0].PersonContactId;
                    caseRecs[0].HDFC_DASH_Deposit_Number__c = depRec.DEPOSIT_NO__c;
                }
                else
                    caseRecs[0].Status = Label.HDFC_DASH_Unregistered_Mail_Status;
            }
            //no deposit number and from unregister mail
            else
            {
                caseRecs[0].Status = Label.HDFC_DASH_Unregistered_Mail_Status;
            }
            //}//OriginClose }
            //assign home branch and service centre for all other origin than email
            if(depositNumber != null){
                if(!Test.isRunningTest()){
                depList = [SELECT Id,BORR_CUST_NO__c,CUST_NAME__c,DEPOSIT_AMT__c,DEPOSIT_NO__c,MATURITY_DATE__c,HOME_BRANCH__c,SERVICE_CENTER__c
                           FROM Deposit__x WHERE DEPOSIT_NO__c = :depositNumber LIMIT 1];
                }else{
                    depList = new List<Deposit__x>(mockedRequests);
                    system.debug('depList...'+depList);
                }
                if(depList.size()>0){
                    depRec = depList[0];
                    caseRecs[0].HDFC_DASH_Home_Branch__c = depRec.HOME_BRANCH__c;
                    caseRecs[0].HDFC_DASH_Service_Centre__c = depRec.SERVICE_CENTER__c;
                    If(caseRecs[0].Status == Label.HDFC_DASH_Unregistered_Mail_Status)
                        caseRecs[0].Status ='New';
                }
                Id rectypeId=[select id from RecordType where developername like 'deposit%' limit 1 ].id;
                caseRecs[0].RecordTypeId=rectypeId;
            }
            string caseStatus=caseRecs[0].status;
            if(!String.isBlank(caseRecs[0].HDFC_DASH_Home_Branch__c) && !caseRecs[0].isClosed && 
               !caseRecs[0].HDFC_DASH_Skip_AutoAssignment__c && !caseRecs[0].HDFC_DASH_No_Further_Response__c && !caseStatus.contains('Statuary Escalation')){
                   
                   if(caseRecs[0].HDFC_DASH_Mode__c == 'Government of India' || caseRecs[0].HDFC_DASH_Mode__c == 'Regulator' ){
                       userListOwner = [SELECT Id,Name,UserRole.Name FROM User WHERE UserRole.Name = 'Regulator/DC Role'];
                           if(userListOwner.size() > 0){
                               caseRecs[0].ownerId = userListOwner[0].Id;
                           }
                   
                   }else if(caseStatus != 'Escalated L1' && caseStatus != 'Escalated L2'){
                   ownerNameQueue = caseRecs[0].HDFC_DASH_Home_Branch__c + ' Deposit Head';
                   queueList = [SELECT Id,Name FROM GROUP WHERE Name = :ownerNameQueue];
                    system.debug('@@@@ Deposit Head updated from here ' + ownerNameQueue );
                   if(queueList.size() > 0){
                       caseRecs[0].ownerId = queueList[0].Id;
                   }
                   }
                   
               }
            Database.update(caseRecs);
            //Notification to Case Owner
                HDFC_DASH_CaseTriggerOpsHelper.caseAssignmentOmniChannel(caseRecs);
            
            //Email message on Level 0
            if( caseRecs[0].Origin == HDFC_DASH_Constants.STRING_CASE_WEB && caseRecs[0].HDFC_DASH_Sub_mode__c == 'Complaints Website'){
                    EmailMessage emsg = new EmailMessage();
                try{
                    emsg.FromAddress = (caseRecs[0].SuppliedEmail != '' ? caseRecs[0].SuppliedEmail : UserInfo.getUserEmail());
                    emsg.ToAddress = '';
                     emsg.Incoming = HDFC_DASH_Constants.BOOLEAN_TRUE;
                    emsg.Subject = '['+caseRecs[0].HDFC_DASH_Sub_Mode__c+']'+caseRecs[0].Subject;                          
                     emsg.TextBody = caseRecs[0].Description;      
                     emsg.ParentId = caseRecs[0].Id;
                    system.debug('caseRec web emsg...'+caseRecs[0]);
                    insert emsg;
                }catch(Exception e){
                    system.debug('Exception @: '+e);
                }
                
            }
            
            
        }
        catch(Exception e){
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON 
                + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.CASE_REC_FOR_DEPOSIT,
                                                        HDFC_DASH_Constants.CASE_EVENT,NULL,expDetails);
            system.debug('@@@@ Excep' + expDetails);
        }
        
    }
    
}