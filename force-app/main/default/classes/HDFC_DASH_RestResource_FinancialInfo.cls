@RestResource(urlMapping='/hdfc/financialinfomation/V1.0')
/*
Author: Anisha Arumugam
Class Name:HDFC_DASH_RestResource_FinancialInfo
*/
global with sharing class HDFC_DASH_RestResource_FinancialInfo {
    @HttpPatch
    /*
Method:storeFinancialInfo()
Description:This method will store the financial info data from OS
*/
    global static void storeFinancialInfo() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders;
        Exception exp;
        HDFC_DASH_APIResponse_FinancialInfo apiResponse = new HDFC_DASH_APIResponse_FinancialInfo();
        
        try
        { 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_Financial.processFinancialDetails(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  

            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_FINANCIALINFO, HDFC_DASH_Constants.HANDLE_FINANCIALINFO_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_PATCH, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_FINANCIALINFO, request.requestBody.tostring(), (res.responseBody)?.tostring(), HDFC_DASH_Constants.METHOD_TYPE_PATCH, HDFC_DASH_Constants.HANDLE_FINANCIALINFO_REQUEST, exp, request.headers);        
            
            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                    res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
}