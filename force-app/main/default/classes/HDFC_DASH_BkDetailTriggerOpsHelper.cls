/**
* 	className: HDFC_DASH_BkDetailTriggerOpsHelper (ApplicantBankDetails)
DevelopedBy: Tejeswari
Date: 24 September 2021
Company: Accenture
Class Description: This class would acts as helper class for Applicant Bank Details Trigger and contains the methods for before and after insert Scenarios.
**/
public class HDFC_DASH_BkDetailTriggerOpsHelper {
    /* 
Method Name :changeBankDetRecBeforeUpdate
Description :This method would change the Latest field of Applicant Bank Details records before insert.
*/
    public static void changeBankDetRecBeforeUpdate(List<HDFC_DASH_Applicant_Bank_Detail__c> bankDetRecList){
        try{
            for(HDFC_DASH_Applicant_Bank_Detail__c bankDet :bankDetRecList){
                //system.debug('bankDet'+bankDet);
                bankDet.HDFC_DASH_Bank_Detail_Latest__c = HDFC_DASH_Constants.STRING_YES;
                //system.debug('bankDet=====latestflag'+bankDet.HDFC_DASH_Bank_Detail_Latest__c);
            }
        }catch(exception e)
        {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON 
                + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Applicant_Bank_Details,
                                                        HDFC_DASH_Constants.UPDATELATESTFIELD,NULL,expDetails);
        }
    }
    
     /* 
Method Name :filterBDWhereConIsPopulated
Description :This method would filter ApplicantBankDetails where contact is populated.
*/
    public static List<HDFC_DASH_Applicant_Bank_Detail__c> filterBDWhereConIsPopulated(){
         List<HDFC_DASH_Applicant_Bank_Detail__c> bkDetList = new List<HDFC_DASH_Applicant_Bank_Detail__c>();
            List<HDFC_DASH_Applicant_Bank_Detail__c> newBankDetails = Trigger.new;
           
            for(HDFC_DASH_Applicant_Bank_Detail__c bankdetail: newBankDetails){
                // HDFC_DASH_Applicant_Bank_Detail__c newMapDetail = (HDFC_DASH_Applicant_Bank_Detail__c) Trigger.newMap.get(bankdetail.id);
               HDFC_DASH_Applicant_Bank_Detail__c oldMapDetail = (HDFC_DASH_Applicant_Bank_Detail__c) Trigger.oldMap?.get(bankdetail.id);
                if(bankdetail.HDFC_DASH_Contact__c != oldMapDetail?.HDFC_DASH_Contact__c && 
                   bankdetail.HDFC_DASH_Contact__c != NULL){
                    bkDetList.add(bankdetail);
                }
            }
        

        //system.debug('bkDetList'+bkDetList);
        return bkDetList;
    }
    /* 
Method Name :filterBDforLatestFlagSenario
Description :This method would filter ApplicantBankDetails for latest flag.
*/
    public static List<HDFC_DASH_Applicant_Bank_Detail__c> filterBDforLatestFlagSenario(){
        Map<id,HDFC_DASH_Applicant_Bank_Detail__c> mapBKDetails;
        String queryCondition = HDFC_DASH_Constants.HDFC_DASH_Contact;
            List<HDFC_DASH_Applicant_Bank_Detail__c> newBankDetails = Trigger.new;
            List<String> conList = new List<String>();
            for(HDFC_DASH_Applicant_Bank_Detail__c bankdetail: newBankDetails){
                // HDFC_DASH_Applicant_Bank_Detail__c newMapDetail = (HDFC_DASH_Applicant_Bank_Detail__c) Trigger.newMap.get(bankdetail.id);
               HDFC_DASH_Applicant_Bank_Detail__c oldMapDetail = (HDFC_DASH_Applicant_Bank_Detail__c) Trigger.oldMap?.get(bankdetail.id);
                if(bankdetail.HDFC_DASH_Contact__c != oldMapDetail?.HDFC_DASH_Contact__c && 
                   bankdetail.HDFC_DASH_Contact__c != NULL){
                    conList.add(bankdetail.HDFC_DASH_Contact__c);
                       //system.debug('conList'+conList);
                }
            }
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Bank_Detail_Latest};
                mapBKDetails =  new Map<id,HDFC_DASH_Applicant_Bank_Detail__c>
                (HDFC_DASH_Applicant_BankDetail_Selector.getBankDetails(conList,fieldList,queryCondition));
            for(HDFC_DASH_Applicant_Bank_Detail__c bankDetails :newBankDetails){
                if(mapBKDetails.get(bankDetails.id) !=NULL){
                    mapBKDetails.remove(bankDetails.id);
                }
            }
       
        return mapBKDetails.values();
    }
    /* 
Method Name :updateBKDetRec
Description :This method would update the Latest Flag for Applicant Bank details records.
*/
    public static void updateBKDetRec(List<HDFC_DASH_Applicant_Bank_Detail__c> bankDetRecList){
        //system.debug('inside bank details');
        List<HDFC_DASH_Applicant_Bank_Detail__c> updateBankDetail = new List<HDFC_DASH_Applicant_Bank_Detail__c>();
        try{
            for(HDFC_DASH_Applicant_Bank_Detail__c bKDetail : bankDetRecList){
                if(bKDetail.HDFC_DASH_Bank_Detail_Latest__c == HDFC_DASH_Constants.STRING_YES){
                    bKDetail.HDFC_DASH_Bank_Detail_Latest__c = HDFC_DASH_Constants.STRING_NO;
                }
                updateBankDetail.add(bKDetail);
            }
            if(Schema.sObjectType.HDFC_DASH_Applicant_Bank_Detail__c.isUpdateable()){
                Database.update(updateBankDetail,true);
            }
        }
        catch(Exception e){
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON 
                + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Applicant_Bank_Details,
                                                        HDFC_DASH_Constants.UPDATELATESTFIELD,NULL,expDetails);
        }
    }
   
    
}