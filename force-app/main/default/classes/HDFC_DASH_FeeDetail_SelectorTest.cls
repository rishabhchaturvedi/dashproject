/**
className: HDFC_DASH_FeeDetail_SelectorTest
DevelopedBy: Punam Marbate
Date: 27 Aug 2021
Company: Accenture
Class Description: Test class for FeeDetail_Selector class.
**/
@isTest(SeeAllData = false)
public with sharing class HDFC_DASH_FeeDetail_SelectorTest {
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails =
                                HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Fee_Detail__c feeDetail = HDFC_DASH_TestDataFactory_FeeDetails.getBasicFeeDetail(app.id);
    private static final string SUCCESS ='Success';
    /* 
    Method Name :testGetFeeDetailsBasedOnId
    Description :This method will test the method getFeeDetailsBasedOnId().
    */
    /*static testmethod void testGetFeeDetailsBasedOnId(){
        System.runAs(sysAdmin){
            HDFC_DASH_Fee_Detail__c resFeeDetailRec;   
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            resFeeDetailRec = HDFC_DASH_FeeDetail_Selector.getFeeDetailsBasedOnId(fieldList,feeDetail.Id);       
            Test.stopTest();
            system.assertNotEquals( null, resFeeDetailRec);
            system.assertEquals(feeDetail.Id, resFeeDetailRec.Id, SUCCESS);
        }
    }*/
    
    /* 
    Method Name :testGetFeeDetailsBasedOnIds
    Description :This method will test the method getFeeDetailsBasedOnIds().
    */
    static testmethod void testGetFeeDetailsBasedOnIds(){
	
        System.runAs(sysAdmin){
             List<HDFC_DASH_Fee_Detail__c>  listFeeDetail = null;
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
			List<Id> listFeeDetailIds = new List<Id>{feeDetail.Id};
            Test.startTest();   
            listFeeDetail = HDFC_DASH_FeeDetail_Selector.getFeeDetailsBasedOnIds(fieldList,listFeeDetailIds);       
            Test.stopTest();
            system.assertNotEquals( null, listFeeDetail);
            system.assertEquals(listFeeDetailIds.size(),listFeeDetail.size(), SUCCESS);
        }
    }
    
    /* 
    Method Name :testGetFeeDetailsBasedOnQuery
    Description :This method will test the method getFeeDetailsBasedOnQuery().
    */
    static testmethod void testGetFeeDetailsBasedOnQuery(){
        List<HDFC_DASH_Fee_Detail__c> listFeeDetailRec = null;
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Application =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                app.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listFeeDetailRec = HDFC_DASH_FeeDetail_Selector.getFeeDetailsBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listFeeDetailRec);
        system.assertEquals(1,listFeeDetailRec.size(),SUCCESS);       
      }
    
    /* 
    Method Name :getappDetailRecsBasedOnString
    Parameters  :List<string> fieldList,String queryParameter,String conditionField
    Description :This method would get the List of Applicant Details based on single string condition sent to it.
    */
    static testmethod void testgetFeeDetailRecsBasedOnCondition()
    {
        
         List<HDFC_DASH_Fee_Detail__c> feeDetailRecsList=null;
         List<String> fieldList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Application};
         System.runAs(sysAdmin)
        {
          Test.startTest();
            feeDetailRecsList= HDFC_DASH_FeeDetail_Selector.getFeeDetails(new List<Id>{app.id},fieldList,HDFC_DASH_Constants.HDFC_DASH_Application);
         Test.stopTest();
        }
        system.assertNotEquals( null, feeDetailRecsList);
        system.assertEquals(HDFC_DASH_Constants.INT_ONE,feeDetailRecsList.size()); 

        
    }
}