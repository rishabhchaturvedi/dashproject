/*
Class: HDFC_DASH_EmailToActivtiy_Listener
Author: Anisha Arumugam
Date: 11 Dec 2021
Company: Accenture
Description: This class will handle the Inbound Email Services.
*/
global class HDFC_DASH_EmailToActivtiy_Listener implements Messaging.InboundEmailHandler 
{
    public boolean fileNumberFound = false;
    
    /* Method Name: handleInboundEmail
    Method Description: This method will create the EmailMessage and Info Validation records for the inbound email and link it with the recipient contact.
    */ 
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env)
    {
        List<Opportunity> list_App = new  List<Opportunity>();       
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>(); 
        List<String> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_name, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber, HDFC_DASH_Constants.HDFC_DASH_File_Number};
            List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.RECORDTYPE_NAME,
                HDFC_DASH_Constants.HDFC_DASH_Salutation,HDFC_DASH_Constants.HDFC_DASH_FirstName,HDFC_DASH_Constants.HDFC_DASH_MiddleName,
                HDFC_DASH_Constants.HDFC_DASH_LastName,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.HDFC_DASH_Email,HDFC_DASH_Constants.RELATION_WITH_PRIMARYAPPLICANT,
                HDFC_DASH_Constants.APPDETAIL_CONTACT_R_EMAIL, HDFC_DASH_Constants.APPDETAIL_CONTACT_R_CONTACT_NUMBER};
                    
                    Map<String,String> mapCondition_App = new Map<string,String>();
        Map<String,String> mapCondition_AppDet = new Map<string,String>();
        
        Opportunity objApp = new Opportunity();
        HDFC_DASH_Applicant_Details__c objAppDetail = new HDFC_DASH_Applicant_Details__c();
        string contactId;
        string fileNumber;
        Boolean contactFoundViaApplication = false;
        system.debug('subject'+email.subject);
        // Create an InboundEmailResult object for returning the result of the Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        try 
        { 
            //look for a file number using the code below
            if(!fileNumberFound)
            {
                //code to match file number:<fileNumber> in Subject
                Pattern pat = Pattern.compile(HDFC_DASH_Constants.STRING_PATTERN_1);
                fileNumber = patternMatcher(pat, email.subject);
                //system.debug('filenumber from subject'+fileNumber);
            }
            
            if(!fileNumberFound)
            {
                //code to match <fileNumber> with leading and trailing space in Subject
                Pattern pat = Pattern.compile(HDFC_DASH_Constants.STRING_PATTERN_2);
                fileNumber = patternMatcher(pat, email.subject);
                fileNumberFound=true;
                system.debug('filenumber from subject'+fileNumber);
            }
            
            if(!fileNumberFound)
            {
                //code to match file number:<fileNumber> in body
                Pattern pat = Pattern.compile(HDFC_DASH_Constants.STRING_PATTERN_1);
                fileNumber = patternMatcher(pat, email.plainTextBody);
                fileNumberFound=true;
                system.debug('filenumber from subject'+fileNumber);
            }
            
            if(!fileNumberFound)
            {
                //code to match <fileNumber> with leading and trailing space in body
                Pattern pat = Pattern.compile(HDFC_DASH_Constants.STRING_PATTERN_2);
                fileNumber = patternMatcher(pat, email.plainTextBody);
                fileNumberFound=true;
            }
 system.debug('filenumber from subject'+fileNumber);
            //Query the file number found to find the matching application - query in name field of opportuntiy
            //Returns the file object. If file is not found then directly query the contact with matching "To Email Address"
            //If file is found, Query the applicants of the application with the "to email address" and find the matching contact
            //Throw error if contact is not found
            //Create the email activtiy for the customer and application with the code below
            
            if(fileNumberFound && fileNumber != null)
            {
                system.debug('file number found');
                mapCondition_App.put(HDFC_DASH_Constants.STRING_name, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + fileNumber + HDFC_DASH_Constants.STRING_QUOTE);     
                
                list_App = HDFC_DASH_Application_Selector.getApplicationBasedOnQuery(listOfFields_App, mapCondition_App);
             system.debug('list app'+list_App);
                if(!list_App.isEmpty())
                {
                    objApp = list_App[HDFC_DASH_Constants.INT_ZERO];
                    mapCondition_AppDet.put(HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + objApp.id + HDFC_DASH_Constants.STRING_QUOTE); 
                   // mapCondition_AppDet.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.APPDETAIL_CONTACT_R_EMAIL, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + email.toAddresses[HDFC_DASH_Constants.INT_ZERO] + HDFC_DASH_Constants.STRING_QUOTE);      
                    
                    listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(listOfFields_AppDetails, mapCondition_AppDet);
                    
                    if(!listAppDetails.isEmpty())
                    {
                        objAppDetail = listAppDetails[HDFC_DASH_Constants.INT_ZERO];
                                          }
                }
            }
            system.debug('list app'+list_App);
            if(list_App.size()>0)
            {
                List<String> listToEmailAddress = new List<String>{email.fromAddress};
                    system.debug('emailaddresslist'+listToEmailAddress);
              
            }
           
            {
                EmailMessage emailMessage = new EmailMessage();
                emailMessage.status = HDFC_DASH_Constants.STRING_THREE; //Email was sent 
                
               // if(list_App.size() >0)
               // {
                  //  emailMessage.relatedToId = objApp.id; //related to record e.g. an opportunity
             //   }
            system.debug('from address'+email.fromAddress);
                emailMessage.fromAddress = email.fromAddress; // from address
                if(email.toAddresses[0] !=null)
                {
                string emaillist;
                for (string singletoadd :email.toAddresses)
                {
                if(emaillist == null)
                emaillist = singletoadd +';';
                else
                emaillist +=singletoadd +';';
                }
                emailMessage.ToAddress =emaillist ; // to address
                    }
                    emailMessage.fromName = email.fromName; // from name
                opportunity opp=[select id from opportunity where name=:filenumber];
                system.debug('filenumber 11'+filenumber);
               
                emailMessage.RelatedToId=opp.id;
                emailMessage.subject = email.subject; // email subject
                emailMessage.htmlBody = email.htmlBody;// email body
                emailMessage.Bypass_Validation__c = true;
               emailMessage.Incoming = true;
                String ccAddrestList;
                if(email.ccAddresses != null){
                    for(String s : email.ccAddresses){
                    if(ccAddrestList == null)
                        ccAddrestList = s+';';
                    else
                        ccAddrestList += s+';';
                }
                emailMessage.CcAddress = ccAddrestList;
                }                
                insert emailMessage; 
                
                             
                system.debug('email message'+emailMessage);
                if(emailMessage?.id != null)
               //if(taskRec?.id != null)
                { 
                    // Add Email Message Relation for id of the recipient
                    EmailMessageRelation emr = new EmailMessageRelation();
                    emr.emailMessageId = emailMessage?.id;
                     list<HDFC_DASH_Applicant_Details__c> appdet=[select id, HDFC_DASH_Contact__c from HDFC_DASH_Applicant_Details__c where HDFC_DASH_Application__r.name=:filenumber limit 1]; 
             		system.debug('appdet...'+appdet);
                    if(appdet.size() > 0){
                        if(appdet[0].HDFC_DASH_Contact__c != null) {
                            emr.relationId = appdet[0].HDFC_DASH_Contact__c;
                            //}
                            //emr.relationType = HDFC_DASH_Constants.STRING_ADDRESS; 
                            emr.relationType = 'OtherAddress';
                            if(!test.isRunningTest())
                            {
                                insert emr;
                            } 
                        }
                    }    
                   

                    //check if emailMessage exist
                    List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.ACTIVITYID};
                        List<EmailMessage> listEmailMessage = HDFC_DASH_EmailMessage_Selector.getEmailMessage(new List<String> {emailMessage.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
                    
                    if(!listEmailMessage.isEmpty())
                    {
                                                
                        if( email.binaryAttachments?.size() > HDFC_DASH_Constants.INT_ZERO) //objInfoVal?.id != null &&
                        {
                            list<ContentVersion> listConVersion1 = new list<ContentVersion>();
                            list<ContentVersion> listConVersion2 = new list<ContentVersion>(); 
                            list<ContentDocumentLink> listConDocLink = new list<ContentDocumentLink>();
                            List<String> fieldList_ConVersion = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.ContentDocumentId};
                                list<String> listConVersionIDs = new list<String>();
                            
                            for(Messaging.InboundEmail.BinaryAttachment attachment : email.binaryAttachments)
                            {
                                ContentVersion contentVer = new ContentVersion();
                                contentVer.Title = attachment.fileName;
                                contentVer.PathOnClient = attachment.fileName;
                                contentVer.VersionData = attachment.body;
                                contentVer.HDFC_DASH_Do_Not_Sync_To_Dms__c = true;
                               // contentVer.VersionData = Blob.valueOf('Test Content');
                                system.debug('contentversion'+attachment.fileName);
                                listConVersion1.add(contentVer);
                            }
                           system.debug('content version list'+listConVersion1.size());
                            Insert listConVersion1;
                            system.debug('content version'+listConVersion1);
                            for(ContentVersion objConVersion1 : listConVersion1)
                            {
                                listConVersionIDs.add(objConVersion1.id);
                            }
                            
                            if(!listConVersionIDs.isEmpty())
                            {
                                listConVersion2 = HDFC_DASH_ContentDocument_Selector.getContentVersion(listConVersionIDs, fieldList_ConVersion, HDFC_DASH_Constants.ID_STRING);

                                for(ContentVersion objConVersion2 : listConVersion2)
                                {
                                    ContentDocumentLink contentDocLink = new ContentDocumentLink();
                                    contentDocLink.LinkedEntityId = emailmessage.id; //opp.id; 
                                    contentDocLink.ContentDocumentId = objConVersion2.ContentDocumentId;
                                    listConDocLink.add(contentDocLink);
                                }
                                Insert listConDocLink;
                            }
                        }
                    } 
                }
                
                // Pass ContentVersion id and infoVal id to the method.
                // Set the result to true. No need to send an email back to the user
                result.success = true; 
                
            }
           
        }
        catch(Exception e)
        {    
            // HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_CONTACTNOTFOUND_EXP); 
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            
            result.success = false;
            result.message = e.getMessage();  
            system.debug('exception from exception log'+expDetails);
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_EMAILTOACTIVITY_LISTENER, HDFC_DASH_Constants.HANDLE_INBOUNDEMAIL, email?.messageId, expDetails); 
        }
         //system.debug('result'+result);
        
        // Return the result for the Apex Email Service
        return result;
        
       
    }
    
    /* Method Name: patternMatcher
    Method Description: This method extract the file number from the search text using regex.
    */ 
    public string patternMatcher(Pattern pat, string searchText)
    {
        string fileNumber;
        Matcher matcher = pat.matcher(searchText);
        
        if(matcher.find())
        {
            fileNumber = matcher.group();
            fileNumberFound = true;
        }
        if(fileNumber != null)
        {
            fileNumber = fileNumber.Trim();
            fileNumber = fileNumber.replace(HDFC_DASH_Constants.COLON, HDFC_DASH_Constants.STRING_BLANK);
        }
        return fileNumber;
    }
}