/* 
ClassName:  HDFC_DASH_TestDataFactory_Event
DevelopedBy: Ayesha Gavandi
Date: 28 Sep 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Event object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_Event {
    
    public static final String STRING_TYPE = 'Activity';
    public static final String STRING_STATUS = 'Open';
    
    /* Method Name: createBasicEvent
parameters:Id AppId,Id ConId,Id UserId 
Method Description: This method is to create test data for Event.
Pass fields of Event Object
*/
    public static Event createBasicEvent(Id AppId,Id ConId,Id UserId)
    {
        Event objEvent = new Event();
        
        objEvent.Type=STRING_TYPE;
        objEvent.HDFC_DASH_Status__c=STRING_STATUS;
        objEvent.OwnerId = UserId;
        objEvent.StartDateTime = DateTime.newInstance(2021, 9, 15, 04, 00, 00);
        objEvent.EndDateTime =  DateTime.newInstance(2021, 9, 15, 05, 00, 00);
        objEvent.WhatId = AppId;
        objEvent.WhoId = ConId;
        objEvent.HDFC_DASH_BSA__c = ConId;
        return objEvent;        
    }
    
    /* Method Name: getBasicEvent
parameters: Id AppId,Id ConId,Id UserId
Method Description: This method is to generate test data for Event.
*/
    public static Event getBasicEvent(Id AppId,Id ConId,Id UserId)
    {
        Event eventDetails = HDFC_DASH_TestDataFactory_Event.createBasicEvent(AppId, ConId, UserId);
        if(Schema.sObjectType.Event.isCreateable()){
            Database.insert(eventDetails);
        }
        return eventDetails;
    }
    
    /* Method Name: createBasicEventlist
parameters: Integer numberOfEvent,Id AppId,Id ConId,Id UserId
Method Description: This method is to create a list of test data for Event
by calling a createBasicEvent method.
*/
    /*public static List<Event> createBasicEventlist(Integer numberOfEvent, Id AppId,Id ConId,Id UserId)
    {
        list<Event> eventDetailslist = new list<Event>();
        for(integer i = 0 ; i < numberOfEvent; i++){
            eventDetailslist.add(HDFC_DASH_TestDataFactory_Event.createBasicEvent(AppId, ConId, UserId));
        }
        return eventDetailslist;
    }
    
    /* Method Name: getBasicEventlist
parameters: Integer numberOfevent,Id AppId,Id ConId,Id UserId
Method Description: This method is to generate a list of test data for Event.
*/
   /* public static List<Event> getBasicEventlist(Integer numberOfEvent, Id AppId,Id ConId,Id UserId)
    {
        list<Event> eventDetailslist = HDFC_DASH_TestDataFactory_Event.createBasicEventlist(numberOfEvent,  AppId, ConId, UserId);
        Database.insert(eventDetailslist,false);
        return eventDetailslist;
    } */  
    
}