/*
Class:HDFC_DASH_WhatsappIntegrationReq
Author: Kumar Gourav
Date: 16 Dec 2021
Company: Accenture
Description: This class will generate the request body for Whatsapp Integration to ILPS 
*/
public with sharing class HDFC_DASH_WhatsappIntegrationReq {
/*
Class: Type_template
Description: Class to parse Type Template
*/
    public with sharing class Type_template {
		public String name;
		public List<String> attributes;
		public Language language;
	}

	public List<Message> message;

/*
Class: Language
Description: Class to parse Language
*/
	public with sharing class Language {
		public String locale;
		public String policy;
	}

/*
Class: Message
Description: Class to parse Message
*/
	public with sharing class Message {
		public String recipient_whatsapp;
		public String message_type;
		public String recipient_type;
        public String source;
		public List<Type_template> type_template;
	}
    
}