/**
* 	className: HDFC_DASH_Case_Selector
DevelopedBy: Deepali Gupta
Date: 08 Oct 2021
Company: Accenture
Class Description: This class would create the select queries for Case object .
**/
public with sharing class HDFC_DASH_Case_Selector {
    
    private static final string SELECT_STRING = 'SELECT ';
    private static final string FROM_OBJECT = ' FROM Case ';
    private static final string WHERE_STRING = 'WHERE ';
    private static final string QUERY_PARAMETER = ' = :queryParameter LIMIT 1';
    private static final string QUERY_PARAMETER_IN = ' IN :queryParameter';
    private static final string ISCLOSED_CONDITION = ' AND IsClosed = false';
    
    /* 
Method Name :getCaseRecsBasedOnString
Parameters  :List<string> fieldList,String queryParameter,String conditionField
Description :This method would get the List of Cases based on single string condition sent to it.
*/
    public static List<Case> getCaseRecsBasedOnString(List<string> fieldList,String queryParameter,String conditionField){
        List<Case> caseRecsList = new List<Case>();
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
            +FROM_OBJECT + WHERE_STRING
            +conditionField +QUERY_PARAMETER;
        system.debug('querystring...'+querystring);
        
        caseRecsList = database.query(querystring);
        system.debug('caseRecsList...'+caseRecsList);
        return caseRecsList;
    }
    
    /* 
Method Name :getCaseRecsBasedOnList
Parameters  :List<string> fieldList,String queryParameter,String conditionField
Description :This method would get the List of Cases based on single string condition sent to it.
*/
    public static List<Case> getCaseRecsBasedOnList(List<string> fieldList,List<String> queryParameter,String conditionField){
        List<Case> caseRecsList = new List<Case>();
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
            +FROM_OBJECT + WHERE_STRING
            +conditionField +QUERY_PARAMETER_IN +ISCLOSED_CONDITION;
        caseRecsList = database.query(querystring);
        return caseRecsList;
    }
    
        /* 
Method Name :getCaseRecsBasedOnID
Parameters  :List<string> fieldList,String queryParameter,String conditionField
Description :This method would get the List of Cases based on single string condition sent to it.
*/
    
    public static Case getCaseBasedOnId(List<string> fieldList, Id caseId){
        
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT+ WHERE_STRING+ HDFC_DASH_Constants.ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+caseId+ HDFC_DASH_Constants.STRING_QUOTE;

        Case caseRec = database.query(querystring); 

        return caseRec;
    }
}