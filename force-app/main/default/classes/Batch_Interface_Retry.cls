/*@author        
Accenture 
@date           23/10/2017
@name           Batch_Interface_Retry 
@description    The purpose of this class is to retry the interfaces.
Modification Log: 
------------------------------------------------------------------------------------
Developer                     Date                    Description  
------------------------------------------------------------------------------------ 
Accenture                    23/10/2017               Class for  Batch_Interface_Retry   
*/
global with sharing class Batch_Interface_Retry implements 
                                                            database.Stateful,database.Batchable<Sobject>,Database.AllowsCallouts{
   private static final string OUTBOUND_MESSAGES = 'Outbound_Messages';  
   private static final string STRING_NULL ='null';                                                          
        
    /***************************************************************************************************************
    *   @Name        :  Start                                                              
    *   @Return      :  database.QueryLocator                                                                                       
    *   @Description :  All Callout records to cross check whether retries required or not..                              
    ***************************************************************************************************************/
    global database.QueryLocator start(database.batchablecontext BC){
        String outboundMessage = OUTBOUND_MESSAGES;
        String query = 'Select id,Name,Do_Retry__c,Number_Of_Retries__c,Message_Id__c,Status_Code__c,Callout_Date__c,'+
                       'Request_Body__c,Interface_Name__c,Header_Map__c,Method_Type__c,Post_Retry_Process__c'+ 
                       ' FROM Integration_Message_Log__c Where Do_Retry__c= true and '+
                       'recordtype.DeveloperName =: outboundMessage and Continuation_Id__c = null'; 
        return Database.getQueryLocator(query);
    }
    /***************************************************************************************************************
    *   @Name        :  Execute                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  Callout logic in execute methods.                              
    ***************************************************************************************************************/
    global void execute(Database.BatchableContext BC, 
                        List<Integration_Message_Log__c> IntMsg){
        Map<String,interface_settings__c> interfaceCSMapTemp = interface_settings__c.getAll(); 
        Map<String,interface_settings__c> interfaceCSMap = new  Map<String,interface_settings__c>();
        if(interfaceCSMapTemp<>null){
            for(string str :interfaceCSMapTemp.keyset()){
                interfaceCSMap.put(str.trim().tolowercase(),interfaceCSMapTemp.get(str));
            }
        }
        
        for(Integration_Message_Log__c objIntMsgLog: IntMsg){
            interface_settings__c objInterfaceSt;
            objInterfaceSt = interfaceCSMap.get(objIntMsgLog.Interface_Name__c.trim().tolowercase());
            if( objInterfaceSt <> null && !objInterfaceSt.No_Callout__c){
                InterfaceCallOutProcess objIntCallOutProcess 
                                                                    = new InterfaceCallOutProcess();
                map<string,string> headermp = new map<string,string>();
                map<string,object> headerObjectMap 
                                                    = new map<string,string>();
                if(String.isNotBlank(objIntMsgLog.Header_Map__c) && 
                   objIntMsgLog.Header_Map__c <> null && objIntMsgLog.Header_Map__c <> STRING_NULL){
                    headerObjectMap = (Map<String, object>) JSON.deserializeUntyped(objIntMsgLog.Header_Map__c);
                    for(string objstr : headerObjectMap.keyset()){  
                        headermp.put(objstr,(string)headerObjectMap.get(objstr));
                    }
                }
                InterfaceHandler.objIntegrationMsgLog = objIntMsgLog;
                HttpResponse responseBody;
                if(!objInterfaceSt.Request_Body_As_Blob__c){
                    responseBody =objIntCallOutProcess.doCallOut(objIntMsgLog.Interface_Name__c,
                                                                       objIntMsgLog.Method_Type__c ,objIntMsgLog.Request_Body__c,headermp,
                                                                       objIntMsgLog.Post_Retry_Process__c,objIntMsgLog.Message_Id__c);
                }else{
                    responseBody =objIntCallOutProcess.doCallOut(objIntMsgLog.Interface_Name__c,
                                                                       objIntMsgLog.Method_Type__c ,Blob.valueOf(objIntMsgLog.Request_Body__c),
                                                                       headermp,objIntMsgLog.Post_Retry_Process__c,objIntMsgLog.Message_Id__c);
                }                                                            
                if(objInterfaceSt.Retry_Process_Class__c <> null){
                    InterfaceHandler.callRetryProcessClass(responseBody,objInterfaceSt,objIntMsgLog);
                    /*type t = Type.forName(objInterfaceSt.Retry_Process_Class__c);
                    InterfaceRetryProcess objInterfaceRetryProcess = (InterfaceRetryProcess)t.newInstance();
                    objInterfaceRetryProcess.doRetryProcess(responseBody,objIntMsgLog.Post_Retry_Process__c ); */
                }
            }
            
        }
        
    }
    /***************************************************************************************************************
    *   @Name        :  finish                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  Callout logic in finish methods.                              
    ***************************************************************************************************************/
    global void finish(Database.BatchableContext BC){
        
    }
}