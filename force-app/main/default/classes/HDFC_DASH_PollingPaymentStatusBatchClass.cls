/**
* 	className: HDFC_DASH_PollingPaymentStatusBatchClass
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class would get all the Application Payment details whose AuthStatus is not 0300.
**/
global class HDFC_DASH_PollingPaymentStatusBatchClass implements  database.Stateful,database.Batchable<Sobject>,Database.AllowsCallouts{
    /* 
Method Name :start
Parameters  :Database.BatchableContext BC
Description :This method would query the list of Application Payment details whose AuthStatus is not 0300.
*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collecting the batches of records or objects to be passed to execute
        
         // system.debug('inside start method');
        //system.debug('Records in start method'+Database.getQueryLocator(HDFC_DASH_Constants.HDFC_DASH_PollingPaymentStatusBatchClass_Query));
        return Database.getQueryLocator(HDFC_DASH_Constants.HDFC_DASH_PollingPaymentStatusBatchClass_Query);  
        
    }
        /* 
Method Name :execute
Parameters  :Database.BatchableContext BC, List<HDFC_DASH_Application_Payment_Details__c> payDetList
Description :This method make callouts to Billdesk and return the current AuthStatus.
*/ 
    global void execute(Database.BatchableContext BC, List<HDFC_DASH_Application_Payment_Details__c> payDetList) {
        //system.debug('inside execute method');
       // system.debug('payDetList====='+payDetList);
       // system.debug('payDetList.size()====='+payDetList.size());
        try {
        // processing each batch of records
        Map<Id,String> payDetTxnStatusToUpdate = new Map<Id,String>();
            List<HDFC_DASH_Application_Payment_Details__c> appPayToUpdate = new List<HDFC_DASH_Application_Payment_Details__c>();
         integer payDetCount = 1;
            Interface_Settings__c billDeskIntCustSetting = Interface_Settings__c.getValues(HDFC_DASH_Constants.INTERFACE_SETTING_BilldeskStatus);
        for(HDFC_DASH_Application_Payment_Details__c payDet : payDetList) {
           
            system.debug('payDet===='+payDet);
            if(String.isNotBlank(payDet.HDFC_DASH_TxnReferenceNo__c)){
             InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
        HttpResponse response;
        map<String,String> headerMap = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_BilldeskStatus); 
        
        
        String reqEndPoint = billDeskIntCustSetting.End_point__c+payDet.HDFC_DASH_TxnReferenceNo__c;
        //String reqEndPoint = 'https://extapigtwyuat.hdfc.com/SPOTOFFER_OTHER_IS/rest/BilldeskStatus/';
        system.debug('reqEndPoint'+reqEndPoint); 
       if(payDetCount<payDetList.size()){
        response= interfaceObj.doMultiCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_BilldeskStatus,HDFC_DASH_Constants.METHOD_TYPE_GET,reqEndPoint,headerMap ,HDFC_DASH_Constants.STRING_RETRY_SET,headerMap.get(HDFC_DASH_Constants.TRANSACTION_ID),false);
                }else if(payDetCount==payDetList.size()){
                   // system.debug('inside else condition');
                     response= interfaceObj.doMultiCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_BilldeskStatus,HDFC_DASH_Constants.METHOD_TYPE_GET,reqEndPoint,headerMap ,HDFC_DASH_Constants.STRING_RETRY_SET,headerMap.get(HDFC_DASH_Constants.TRANSACTION_ID),true);
                }
       
        //system.debug('response'+response);
        if(response?.getStatusCode() == HDFC_DASH_Constants.INT_TWO_HUNDRED){ 
           // system.debug('response---'+response.getBody());
            Map<String, Object> mapResponseBody = (Map<String, Object>)JSON.deserializeUntyped(String.valueOf((response?.getBody())));
           // system.debug('mapResponseBody==='+mapResponseBody);
			string resTransactionStatus = String.valueOf(mapResponseBody.get(HDFC_DASH_Constants.STRING_TRANSACTIONSTATUS));
           // System.debug('resTransactionStatus--'+resTransactionStatus);
           // System.debug('Auth Status of Payment Detail---'+payDet.HDFC_DASH_AuthStatus__c);
            payDetTxnStatusToUpdate.put(payDet.Id,resTransactionStatus);
            payDet.HDFC_DASH_AuthStatus__c = resTransactionStatus;
            system.debug('payDet.HDFC_DASH_AuthStatus__c=='+payDet.HDFC_DASH_AuthStatus__c);
            appPayToUpdate.add(payDet);
        }
            } 
            payDetCount=payDetCount+1;
           // system.debug('payDetCount===='+payDetCount);
           
        }
            update appPayToUpdate;
           // system.debug('appPayToUpdate==='+appPayToUpdate);
        } catch(Exception e) {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                                    HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                                    + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  

            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INTERFACE_SETTING_BilldeskStatus, 
                    HDFC_DASH_Constants.HANDLE_BILLDESK_STATUS_CALLOUT, HDFC_DASH_Constants.METHOD_TYPE_GET, expDetails);
        }
         
    }   
         /* 
Method Name :finish
Parameters  :Database.BatchableContext BC
Description :
*/
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }
}