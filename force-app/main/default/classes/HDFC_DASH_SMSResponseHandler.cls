/**
className: HDFC_DASH_SMSResponseHandler
DevelopedBy: Anisha Arumugam
Date: 25 November 2021
Company: Accenture 
Class Description: This Class contains the update operation on Task object based on the SMS Integration Response.
**/
public with sharing class HDFC_DASH_SMSResponseHandler extends processResponseHandler 
{
    /*      
Method Name :doRetryProcess
Description :This method would Update the Task Description, status.
*/
    public Override void doRetryProcess(HttpResponse response, String processDataStr) 
    {
        List<Task> listUpdateTask = new List<Task>();
        try
        {
            if(response != null && response?.getBody() != null)
            {              
                string responseBody = response?.getBody()?.replace(HDFC_DASH_Constants.STRING_OBJECT, HDFC_DASH_Constants.STRING_OBJECTRESPONSE);
                responseBody = responseBody.replace(HDFC_DASH_Constants.STRING_SLASH, HDFC_DASH_Constants.STRING_BLANK);
                //Removing the extra double code at the beginning and end of the response body
                responseBody = responseBody.substring(1, responseBody.length() - 1);
                
                
                HDFC_DASH_ParseResponseSMSIntegration result = HDFC_DASH_ParseResponseSMSIntegration.parse(responseBody);
                HDFC_DASH_CommunicationWrapper.processData objProcessData = (HDFC_DASH_CommunicationWrapper.processData)json.deserialize(processDataStr, HDFC_DASH_CommunicationWrapper.processData.class);

                if(objProcessData?.taskId != null)
                {            
                    Task objTask = new Task();
                    objTask.id = objProcessData.taskId;
                    if(result.OBJECTResponse[HDFC_DASH_Constants.INT_ZERO].ReturnMsg[HDFC_DASH_Constants.INT_ZERO].StatusCode == HDFC_DASH_Constants.STRING_FAIL)
                    {
                        objTask.Status = HDFC_DASH_Constants.STRING_FAILED;
                    }
                    else
                    {   
                        objTask.Status = HDFC_DASH_Constants.STRING_CLOSED;
                    }
                    
                    if(objProcessData?.messageBody != null)
                    {
                        objTask.Description = objProcessData?.messageBody;  
                    }
                    
                    if(objProcessData?.msgLogId != null)
                    {
                        objTask.HDFC_DASH_Message_Log__c = objProcessData?.msgLogId;
                    }
                    listUpdateTask.add(objTask);
                    if(Schema.sObjectType.Task.isUpdateable()){
                        Database.update(listUpdateTask); 
                    }
                }
            }   
        }
        catch(Exception e)
        {     
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            //system.debug('expDetails => ' + expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_SMSRESPONSEHANDLER, HDFC_DASH_Constants.HANDLE_DORETRYPROCESS, processDataStr, expDetails);
        }
    }
}