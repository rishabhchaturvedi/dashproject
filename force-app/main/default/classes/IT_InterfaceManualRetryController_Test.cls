/* ================================================
 * @Class Name :  IT_InterfaceManualRetryController_Test
 * @author : Accenture
 * @Purpose: This class is used to test the functionality of class  IT_InterfaceManualRetryController
 * @created date:
 * @Last modified date:19/1/2016
 * @Last modified by : Accenture
 ================================================*/
@istest(SeeAllData = false)
public class IT_InterfaceManualRetryController_Test{
    
    /***************************************************************************************************************
    *   @Name        :  testMethodFirst                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for                        
    ***************************************************************************************************************/
    public static testMethod void testMethodFirst(){
                                                         
        /*IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                 'Complete',
                                                 '[{"Name": "sForceTest1"}]',
                                                 null);*/
        
        // generate header map
        Map<String, string> headermp = new Map<String, string>();
        headermp.put('adi','123');
        headermp.put('adit','1234');
        headermp.put('aditya','12345');
        string headermpString = JSON.serialize(headermp);
        
        // get utility class object
        IT_TestUtility utility = new IT_TestUtility();
        
        // insert Integration Message Log record
        Integration_Message_Log__c intLogObj = new Integration_Message_Log__c();
        utility.createInterationLog(intLogObj);
        intLogObj.Apex_Error__c = false;
        intLogObj.Status_Code__c = null;
        intLogObj.Header_Map__c = headermpString;
        intLogObj.Expiry_time_mins__c = 30;
        intLogObj.Number_Of_Retries__c = 0;
        intLogObj.Maximum_No_Of_Retries__c = 2;
        intLogObj.Retry_Interval__c = 0;
        intLogObj.Callout_Date__c = system.NOW()-1;
        Database.update(intLogObj);
        //insert framework setting record
        utility.insertFrameworkSettingsRecord(false);
        
        // insert interface setting record
        Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
        interfaceSettingObj = utility.insertIntefaceSettingsRecord();
        interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
        //interfaceSettingObject.Email_Template_Id__c = utility.createEmailTemplate();
        
        Database.update(interfaceSettingObj); 
        ApexPages.StandardController stdController;
        IT_InterfaceManualRetryController controllerObj = new IT_InterfaceManualRetryController(stdController);
        test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.IT_Manual_retry')); 
        System.currentPageReference().getParameters().put('Id',intLogObj.id);
        controllerObj.onLoad();     
        test.stoptest();             
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodSecond                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for                        
    ***************************************************************************************************************/
    public static testMethod void testMethodSecond(){
                                                         
        /*IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                 'Complete',
                                                 '[{"Name": "sForceTest1"}]',
                                                 null);*/
        
        // generate header map
        Map<String, string> headermp = new Map<String, string>();
        headermp.put('adi','123');
        headermp.put('adit','1234');
        headermp.put('aditya','12345');
        string headermpString = JSON.serialize(headermp);
        
        // get utility class object
        IT_TestUtility utility = new IT_TestUtility();
        
        // insert Integration Message Log record
        Integration_Message_Log__c intLogObj = new Integration_Message_Log__c();
        utility.createInterationLog(intLogObj);
        intLogObj.Apex_Error__c = false;
        intLogObj.Status_Code__c = null;
        intLogObj.Header_Map__c = headermpString;
        intLogObj.Expiry_time_mins__c = 30;
        intLogObj.Number_Of_Retries__c = 1;
        intLogObj.Maximum_No_Of_Retries__c = 0;
        intLogObj.Retry_Interval__c = 0;
        intLogObj.Callout_Date__c = system.NOW()-1;
        Database.update(intLogObj);
        //insert framework setting record
        utility.insertFrameworkSettingsRecord(false);
        
        // insert interface setting record
        Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
        interfaceSettingObj = utility.insertIntefaceSettingsRecord();
        interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
        //interfaceSettingObject.Email_Template_Id__c = utility.createEmailTemplate();
        
        Database.update(interfaceSettingObj); 
        ApexPages.StandardController stdController;
        IT_InterfaceManualRetryController controllerObj = new IT_InterfaceManualRetryController(stdController);
        test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.IT_Manual_retry')); 
        System.currentPageReference().getParameters().put('Id',intLogObj.id);
        controllerObj.onLoad();     
        test.stoptest();             
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodThree                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for                        
    ***************************************************************************************************************/
    public static testMethod void testMethodThree(){
                                                         
        /*IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                 'Complete',
                                                 '[{"Name": "sForceTest1"}]',
                                                 null);*/
        
        // generate header map
        Map<String, string> headermp = new Map<String, string>();
        headermp.put('adi','123');
        headermp.put('adit','1234');
        headermp.put('aditya','12345');
        string headermpString = JSON.serialize(headermp);
        
        // get utility class object
        IT_TestUtility utility = new IT_TestUtility();
        
        // insert Integration Message Log record
        Integration_Message_Log__c intLogObj = new Integration_Message_Log__c();
        utility.createInterationLog(intLogObj);
        intLogObj.Apex_Error__c = false;
        intLogObj.Status_Code__c = null;
        intLogObj.Header_Map__c = headermpString;
        intLogObj.Expiry_time_mins__c = 30;
        intLogObj.Number_Of_Retries__c = 0;
        intLogObj.Maximum_No_Of_Retries__c = 2;
        intLogObj.Retry_Interval__c = 0;
        intLogObj.Callout_Date__c = system.NOW()-1;
        Database.update(intLogObj);
        //insert framework setting record
        utility.insertFrameworkSettingsRecord(false);
        
        // insert interface setting record
        Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
        interfaceSettingObj = utility.insertIntefaceSettingsRecord();
        interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
        //interfaceSettingObject.Email_Template_Id__c = utility.createEmailTemplate();
        
        Database.update(interfaceSettingObj); 
        ApexPages.StandardController stdController;
        IT_InterfaceManualRetryController controllerObj = new IT_InterfaceManualRetryController(stdController);
        test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.IT_Manual_retry')); 
        System.currentPageReference().getParameters().put('Id','abc');
        controllerObj.onLoad();     
        test.stoptest();             
    }
}