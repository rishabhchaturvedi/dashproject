/**
className: HDFC_DASH_ApplicantDetailsTriggerOpsTest
DevelopedBy: Anmol
Date: 15 July 2021
Company: Accenture
Class Description: test class for HDFC_DASH_ApplicantDetailsTriggerOps.
**/
@isTest(SeeAllData = false)
public class HDFC_DASH_ApplicantDetailsTriggerOpsTest {
    private static final string ASSERT_MSG = 'Assert Message'; 
    private static final string PAN = 'EIQPS5342N';
    private static final INTEGER TWO = 2;
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id); 
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Details__c appDetailsUpd = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static List<HDFC_DASH_Applicant_Details__c> appDetailsList = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetailslist(TWO,con.id, app.id);
    private static List<HDFC_DASH_Applicant_Details__c> appDetailsListUpdate = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetailslist(TWO,con.id, app.id);
    /* 
Method Name :testInsertWithDuplicatePAN
Description :This method would test the trigger for inserting Applicant Details with duplicate PAN.
*/
    @isTest public static void testInsertWithDuplicatePAN() { 
        Exception testException ;
        System.runAs(sysAdmin){
            appDetails.HDFC_DASH_PAN__c=PAN;
            database.insert(appDetails);
            Test.startTest();
            try{
                for(HDFC_DASH_Applicant_Details__c AppDet:appDetailsList){
                    AppDet.HDFC_DASH_PAN__c=PAN;
                    //appDetailsList.add(AppDet);   
                }
                database.insert(appDetailsList);
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);
        }
    }
    
    /* 
Method Name :testUpdateWithDuplicatePAN
Description :This method would test the trigger for Updating Applicant Details with Duplicate PAN.
*/
    @isTest public static void testUpdateWithDuplicatePAN() { 
        Exception testException ;
        System.runAs(sysAdmin){
            appDetails.HDFC_DASH_PAN__c=PAN;
            database.insert(appDetails);
            Test.startTest();
            try{
                for(HDFC_DASH_Applicant_Details__c AppDet:appDetailsListUpdate){
                    AppDet.HDFC_DASH_PAN__c=PAN;
                }
                database.update(appDetailsListUpdate);
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);
        }
    } 
    
   /* 
Method Name :testupdateEmpDetRec
Description :This method would test the trigger for update Employment Details.
*/   
   @IsTest public static void testupdateEmpDetRec(){
            Exception testException ;
            System.runAs(sysAdmin){
             insert appDetails;
                 list<HDFC_DASH_Applicant_Details__c> appDetails = new list<HDFC_DASH_Applicant_Details__c>();
                for(HDFC_DASH_Applicant_Details__c appDet:appDetails)
                {
                    con.id = appDet.HDFC_DASH_Contact__c;
                    appDet.HDFC_DASH_User_Consent_For_CIBIL__c=HDFC_DASH_Constants.STRING_YES;
                       //  appDet.HDFC_DASH_Contact__c = con.id;
                         con.HDFC_DASH_Is_Customer__c= false;
                }
                update appDetails;
            }
        }
}