/*
Class: HDFC_DASH_PostApplicationFormCallout
Author: Sai Suman
Date: 11 Dec 2021
Company: Accenture
Description: Queueable Class to make PostApplicationForm Callout.
*/
public class HDFC_DASH_PostApplicationFormCallout implements Queueable, Database.AllowsCallouts 
{
    Id appId;
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    RestRequest request;
    Exception exp;
    public HDFC_DASH_PostApplicationFormCallout(Id appId)
    {
        this.appId = appId;
    }
    /*
Method: execute
Description: This method will Post the details for PostApplicationForm Callout.
*/
    public void execute(QueueableContext context)
    {
        //System.debug('This is PostApplicationForm Callout'+appId);
        try
        {
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
            
            //Get the PostApplicationForm callout request body
            HDFC_DASH_APIRequest_PostApplicationForm reqBody = getRequestBody(); 
            //system.debug('HDFC_DASH_APIRequest_PostApplicationForm Request '+ reqBody);   
            
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_PostApplicationForm);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_PostApplicationForm, 
                                            HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), 
                                            requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,
                                            requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            //system.debug('Response Generated'+response);
            //processResponse(response);   
            HDFC_DASH_RuleEngineCapture_ILPSCallout ruleEngCapCallout = new HDFC_DASH_RuleEngineCapture_ILPSCallout(appid);
            system.enqueueJob(ruleEngCapCallout);    
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_PostApplicationForm, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }

    }
    
        /*
    Method: getRequestBody
    Parameters:None
    Description: This method will create the requestBody from the request.
    */ 
    public HDFC_DASH_APIRequest_PostApplicationForm getRequestBody()
    {
        //System.debug('In Get RequestBody postAppForm');
        HDFC_DASH_APIRequest_PostApplicationForm appForm = new HDFC_DASH_APIRequest_PostApplicationForm();
        appForm.applicationId = appId;
        return appForm;
    }

}