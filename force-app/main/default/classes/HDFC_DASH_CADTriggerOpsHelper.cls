/**
* 	className: HDFC_DASH_CADTriggerOpsHelper (ContactAddressDetail)
DevelopedBy: Raghavendra
Date: 05 August 2021
Company: Accenture
Class Description: This class would acts as helper class for Contact Address Details Trigger and contains the methods for before and after insert Scenarios.
**/
public class HDFC_DASH_CADTriggerOpsHelper {
    
        /* 
Method Name :filterBDWhereConIsPopulated
Description :This method would filter Contact Address Detail where contact is populated.
*/
    public static List<HDFC_DASH_Contact_Address_Details__c> filterCADWhereConIsPopulated(){
         List<HDFC_DASH_Contact_Address_Details__c> cadDetList = new List<HDFC_DASH_Contact_Address_Details__c>();
            List<HDFC_DASH_Contact_Address_Details__c> newConAddDetails = Trigger.new;
           
            for(HDFC_DASH_Contact_Address_Details__c cad: newConAddDetails){
             
               HDFC_DASH_Contact_Address_Details__c oldMapDetail = (HDFC_DASH_Contact_Address_Details__c) Trigger.oldMap?.get(cad.id);
                //system.debug('CAD DETAILS' +oldMapDetail.HDFC_DASH_Contact__c);
                if(cad.HDFC_DASH_Contact__c != oldMapDetail?.HDFC_DASH_Contact__c && 
                   cad.HDFC_DASH_Contact__c != NULL){
                       system.debug('cad.HDFC_DASH_Contact__c' +cad.HDFC_DASH_Contact__c);
                    cadDetList.add(cad);
                }
            }
        return cadDetList;
    }
    /*
Method Name :filterCADforLatestFlagSenario
Description :This method would filter ContactAddressDetails for latest flag.
*/
    public static List<HDFC_DASH_Contact_Address_Details__c> filterCADforLatestFlagSenario(){
       //system.debug('filter contact add det ');
         list<HDFC_DASH_Contact_Address_Details__c> changeLatestConAdd = new list<HDFC_DASH_Contact_Address_Details__c>();
        try{
            List<HDFC_DASH_Contact_Address_Details__c> changes = Trigger.new;
            List<Id> conRecList = new List<Id>();
            for(HDFC_DASH_Contact_Address_Details__c cad: changes){
                 //HDFC_DASH_Contact_Address_Details__c newMapDetail = (HDFC_DASH_Contact_Address_Details__c) Trigger.newMap.get(cad.id);
               HDFC_DASH_Contact_Address_Details__c oldMapDetail = (HDFC_DASH_Contact_Address_Details__c) Trigger.oldMap?.get(cad.id);
                if(cad.HDFC_DASH_Contact__c != oldMapDetail?.HDFC_DASH_Contact__c && 
                   cad.HDFC_DASH_Contact__c != NULL){
                    conRecList.add(cad.HDFC_DASH_Contact__c);
                }
            }
           // system.debug('conRecList==='+conRecList);
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING, 
                HDFC_DASH_Constants.FROMDATE, HDFC_DASH_Constants.TODATE, HDFC_DASH_Constants.HDFC_DASH_Latest,HDFC_DASH_Constants.ADDRESS_TYPE,HDFC_DASH_Constants.HDFC_DASH_contact};
                Map<id,HDFC_DASH_Contact_Address_Details__c>  mapConAddDet = new Map<id,HDFC_DASH_Contact_Address_Details__c>
                (HDFC_DASH_Contact_Address_Selector.getContactAddressDetBasedOnContactIds(
                    fieldList,conRecList));
             Map<id,List<HDFC_DASH_Contact_Address_Details__c>> mapOfConAndAddDet  = new Map<id,List<HDFC_DASH_Contact_Address_Details__c>>();
            for(HDFC_DASH_Contact_Address_Details__c triggerRec :changes){
                if(mapConAddDet.get(triggerRec.id) !=NULL){
                    mapConAddDet.remove(triggerRec.id);
                }
            }
       //system.debug('mapConAddDet'+mapConAddDet);
            for(HDFC_DASH_Contact_Address_Details__c conAdd :mapConAddDet.values()){
                
               if(mapOfConAndAddDet.containsKey(conAdd.HDFC_DASH_contact__c)) {
		List<HDFC_DASH_Contact_Address_Details__c> contacAdds = mapOfConAndAddDet.get(conAdd.HDFC_DASH_contact__c);
		contacAdds.add(conAdd);
		mapOfConAndAddDet.put(conAdd.HDFC_DASH_contact__c, contacAdds);
	} else {
		mapOfConAndAddDet.put(conAdd.HDFC_DASH_contact__c, new List<HDFC_DASH_Contact_Address_Details__c> { conAdd });
	}
            }
            
            //system.debug('mapOfConAndAddDet=='+mapOfConAndAddDet);
       // Map<contact,List
      
            for(HDFC_DASH_Contact_Address_Details__c triggerRec :changes){
                List<HDFC_DASH_Contact_Address_Details__c> conAddDets =  mapOfConAndAddDet.get(triggerRec.HDFC_DASH_contact__c);
                for(HDFC_DASH_Contact_Address_Details__c conAdd:conAddDets){
                    if(conAdd.HDFC_DASH_Address_Type__c ==triggerRec.HDFC_DASH_Address_Type__c && conAdd.HDFC_DASH_Latest__c ==HDFC_DASH_Constants.STRING_YES){
                       // system.debug('conAdd.HDFC_DASH_Address_Type__c'+conAdd.HDFC_DASH_Address_Type__c);
                       // system.debug('triggerRec.HDFC_DASH_Address_Type__c'+triggerRec.HDFC_DASH_Address_Type__c);
                        changeLatestConAdd.add(conAdd);
                    }
                }
                
            }
            
        }catch(Exception e){
            // String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON
            //     + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
            //     + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Contact_Address_Details,
                                       HDFC_DASH_Constants.UPDATELATESTFIELD,NULL,e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON
                                       + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                                       + HDFC_DASH_Constants.SEMICOLON + e.getTypeName());
        }
        return changeLatestConAdd;
    }
    /*
Method Name :updateConAddDetRec
Description :This method would update the Latest Flag for contact address details records.
*/
    public static void updateConAddDetRec(List<HDFC_DASH_Contact_Address_Details__c> conAddDetRecList){
        List<HDFC_DASH_Contact_Address_Details__c> changes = Trigger.new;
        try{
            List<HDFC_DASH_Contact_Address_Details__c> updateCAD = new List<HDFC_DASH_Contact_Address_Details__c>();
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                HDFC_DASH_Constants.TODATE, HDFC_DASH_Constants.HDFC_DASH_Latest};
                for(HDFC_DASH_Contact_Address_Details__c cad : conAddDetRecList){
                    if(cad.HDFC_DASH_Latest__c==HDFC_DASH_Constants.STRING_YES){
                        cad.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_NO; 
                        cad.HDFC_DASH_To_Date__c = datetime.now().date();

                    updateCAD.add(cad);
                    }
                }
            //update cad
            if(Schema.sObjectType.HDFC_DASH_Contact_Address_Details__c.isCreateable()){
                Database.update(updateCAD,true);
            }
        }catch(Exception e){
            // String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON
            //     + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
            //     + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();
                HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Contact_Address_Details,
                                       HDFC_DASH_Constants.UPDATELATESTFIELD,NULL, e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON
                                       + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                                       + HDFC_DASH_Constants.SEMICOLON + e.getTypeName());
        }
    }
    /*
Method Name :changeConAddDetRecBeforeUpdate
Description :This method would change the Latest, From Date, To- Date fields8 of Contact Address Details records before insert.
*/
   public static void changeConAddDetRecBeforeUpdate(List<HDFC_DASH_Contact_Address_Details__c> conAddDetRecList){
        
        try{
           List<Id> conIds = new List<Id>();
            for(HDFC_DASH_Contact_Address_Details__c cad :conAddDetRecList){
                conIds.add(cad.HDFC_DASH_Contact__c);
            }
            List<Account> accListCurrentAdd = new List<Account>();
            List<Account> accListPermAdd = new List<Account>();
            List<string> fieldlist = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_ACCOUNTID};
           
             Map<Id,Contact> contactConIdMap = new Map<Id,Contact>(HDFC_DASH_Contact_Selector.getContactsBasedOnIds(fieldlist,conIds));
            for(HDFC_DASH_Contact_Address_Details__c cad :conAddDetRecList)
            {
                cad.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_YES;
                cad.HDFC_DASH_From_Date__c = datetime.now().date();
                system.debug('cad---'+cad.HDFC_DASH_Contact__c);
                system.debug('cad---'+cad);
                Contact conrec = contactConIdMap.get(cad.HDFC_DASH_Contact__c);
                system.debug('cad.HDFC_DASH_Contact__r.AccountId'+conrec.AccountId);
                system.debug('cad.HDFC_DASH_Address_Type__c'+cad.HDFC_DASH_Address_Type__c);
                Account acc = new Account();
                if(cad.HDFC_DASH_Address_Type__c == HDFC_DASH_Constants.STRING_C){
                    acc.id = conrec.AccountId;
                    acc.ShippingStreet = ((cad.HDFC_DASH_Address_Line1_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line1_CustomerSelected__c) +' '+(cad.HDFC_DASH_Address_Line2_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line2_CustomerSelected__c)+' '+(cad.HDFC_DASH_Address_Line3_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line3_CustomerSelected__c)+' '+(cad.HDFC_DASH_Address_Line4_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line4_CustomerSelected__c)+' '+(cad.HDFC_DASH_Taluka_CustomerSelected__c==null?'':cad.HDFC_DASH_Taluka_CustomerSelected__c));
                    system.debug('con.account.ShippingStreet' +acc.ShippingStreet);
                    acc.ShippingCity = cad.HDFC_DASH_City_CustomerSelected__c;
                    acc.ShippingState = cad.HDFC_DASH_State_CustomerSelected__c;
                    acc.ShippingPostalCode = cad.HDFC_DASH_Pincode_CustomerSelected__c;
       				acc.ShippingCountry = cad.HDFC_DASH_Country_CustomerSelected__c;
                    
                    if(!accListCurrentAdd.contains(acc)){
                    system.debug('accListCurrentAdd--'+accListCurrentAdd);
                    accListCurrentAdd.add(acc);
                    }
                }
                else if(cad.HDFC_DASH_Address_Type__c == HDFC_DASH_Constants.STRING_P){ 
                    acc.id = conrec.AccountId;
                    acc.BillingStreet = ((cad.HDFC_DASH_Address_Line1_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line1_CustomerSelected__c)+' '+(cad.HDFC_DASH_Address_Line2_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line2_CustomerSelected__c)+' '+(cad.HDFC_DASH_Address_Line3_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line3_CustomerSelected__c)+' '+(cad.HDFC_DASH_Address_Line4_CustomerSelected__c==null?'':cad.HDFC_DASH_Address_Line4_CustomerSelected__c)+' '+(cad.HDFC_DASH_Taluka_CustomerSelected__c==null?'':cad.HDFC_DASH_Taluka_CustomerSelected__c));
                    system.debug('con.account.BillingStreet' +acc.BillingStreet);
                    acc.BillingCity = cad.HDFC_DASH_City_CustomerSelected__c;
                    acc.BillingState = cad.HDFC_DASH_State_CustomerSelected__c;
                    acc.BillingPostalCode = cad.HDFC_DASH_Pincode_CustomerSelected__c;
       				acc.BillingCountry = cad.HDFC_DASH_Country_CustomerSelected__c;
                    if(!accListPermAdd.contains(acc)){
                    system.debug('accListPermAdd--'+accListPermAdd);
                    accListPermAdd.add(acc);
                	}
                }  
            }
            if(accListCurrentAdd!=null && !accListCurrentAdd.isEmpty()){
            update accListCurrentAdd;
            }
            if(accListPermAdd!=null && !accListPermAdd.isEmpty()){
            update accListPermAdd;
            }
        }catch(Exception e){
             HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Contact_Address_Details,
                                       HDFC_DASH_Constants.UPDATELATESTFIELD,NULL,e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON
                                       + e.getMessage() + HDFC_DASH_Constants.SEMICOLON
                                       + e.getStackTraceString() + HDFC_DASH_Constants.SEMICOLON
                                       + e.getTypeName());
        }
    }
}