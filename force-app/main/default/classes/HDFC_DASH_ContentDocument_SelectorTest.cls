/**
* 	className: HDFC_DASH_ContentDocument_SelectorTest
DevelopedBy: Tejeswari
Date: 19 October 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_ContentDocument_Selector class. .
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_ContentDocument_SelectorTest {
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Id sysAdminId = UserInfo.getUserId();
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static ContentVersion contentVer = HDFC_DASH_TestDataFactory_ContentDoc.getContestVersion();
    private static String contenDocId = [select id, ContentDocumentId from ContentVersion where Id=: contentVer.id].ContentDocumentId;
    private static ContentDocumentLink contentDocLink = HDFC_DASH_TestDataFactory_ContentDoc.getConDocLink(contenDocId,con.id);
    private static final string SUCCESS ='Success';
    
    /* 
Method Name :testGetContentVersion
Description :This method will test the method getContentVersion().
*/
    static testmethod void testGetContentVersion(){
        List<ContentVersion> conVerRec = new List<ContentVersion>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING};
                Test.startTest();   
            conVerRec = HDFC_DASH_ContentDocument_Selector.getContentVersion(new List<String> {contentVer.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, conVerRec);
            system.assertEquals(1,conVerRec.size());  
        }
    }
    /* 
Method Name :testGetContentDocumentLink
Description :This method will test the method getContentDocumentLink().
*/
    static testmethod void testGetContentDocumentLink(){
        List<ContentDocumentLink> conDocLink = new List<ContentDocumentLink>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING};
                Test.startTest();   
            conDocLink = HDFC_DASH_ContentDocument_Selector.getContentDocumentLink(new List<String> {contentDocLink.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, conDocLink);
            system.assertEquals(1,conDocLink.size());  
        }
    }
    /* 
Method Name :testGetContentVersionBasedOnQuery
Description :This method will test the method getContentVersionBasedOnQuery().
*/
    static testmethod void testGetContentVersionBasedOnQuery(){
        List<ContentVersion> listConVerRec = new List<ContentVersion>(); 
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING};
                Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.ID_STRING =>
                    HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                    contentVer.Id+HDFC_DASH_Constants.STRING_QUOTE};
                        Test.startTest();
            listConVerRec = HDFC_DASH_ContentDocument_Selector.getContentVersionBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listConVerRec);
        system.assertEquals(1,listConVerRec.size(),SUCCESS);       
    }
}