/*
Class: HDFC_DASH_RuleEngineCapture_ILPSCallout
Author: Kumar Gourav
Date: 4 Dec 2021
Company: Accenture
Description: Queueable Class to make Rule Engine Capture Callout to ILPS 
*/
public class HDFC_DASH_RuleEngineCapture_ILPSCallout implements Queueable, Database.AllowsCallouts {
    Id appId;
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    RestRequest request;
    Exception exp;
    
    //Add appId
    public HDFC_DASH_RuleEngineCapture_ILPSCallout(Id appId)
	{
		this.appId = appId ;
	}
/*
Method: execute
Description: This method will Post the Rule Engine Capture to ILPS.
*/
    public void execute(QueueableContext context) 
    {
        try
        {             
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();     
            
            //Get the Rule Engine Capture request body
            HDFC_DASH_RuleEngineReq_ILPSCallout reqBody = getRequestBody();    
            system.debug('Rule Engine reqBody => ' + string.ValueOf(Json.serialize(reqBody)));
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_RuleEngine);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_RuleEngine, HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            //processResponse(response);  
            HDFC_DASH_DocDetails_ILPSCallout ddILPSCallout = new HDFC_DASH_DocDetails_ILPSCallout(appId,HDFC_DASH_Constants.BOOLEAN_FALSE);
            system.enqueueJob(ddILPSCallout);    
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            system.debug('expDetails => ' + expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_RuleEngine, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
        
    }
    /*
Method: getRequestBody
Parameters:None
Description: This method will create the requestBody from the request.
*/ 
    public HDFC_DASH_RuleEngineReq_ILPSCallout getRequestBody()
    {
        HDFC_DASH_RuleEngineReq_ILPSCallout ruleEngineReq = new HDFC_DASH_RuleEngineReq_ILPSCallout();
        List<HDFC_DASH_RuleEngineReq_ILPSCallout.ruleEngineCapture> listruleEngineCapture = new List<HDFC_DASH_RuleEngineReq_ILPSCallout.ruleEngineCapture>();
		List<HDFC_DASH_Rule_Engine_Capture__c > listruleEngine = new List<HDFC_DASH_Rule_Engine_Capture__c >();
        Map<string,string> mapCondition = new Map<string,string>(); 
        List<string> listOfFields_RuleEngine = new list<string>{HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.HDFC_DASH_APPLICATION_R_NUMBER,HDFC_DASH_Constants.HDFC_DASH_APPLICATION_FILENUMBER,HDFC_DASH_Constants.HDFC_DASH_APPLICANT,HDFC_DASH_Constants.HDFC_DASH_RULEID,HDFC_DASH_Constants.HDFC_DASH_Stage,
            HDFC_DASH_Constants.HDFC_DASH_DOC_RULE_ENG_OUTCOME,HDFC_DASH_Constants.HDFC_DASH_CONFIDENCE_RULE_ENG_OUTCOME,HDFC_DASH_Constants.HDFC_DASH_CUSTOMER_RESOLVED,HDFC_DASH_Constants.HDFC_DASH_CUSTOMER_ACTION,
            HDFC_DASH_Constants.HDFC_DASH_CUSTOMER_SKIPPED,HDFC_DASH_Constants.HDFC_DASH_DOC_CODE,HDFC_DASH_Constants.HDFC_DASH_DOC_NAME,HDFC_DASH_Constants.HDFC_DASH_SUB_DETAILS};
                List<String> listOfFields_ApplicantDet = new list<string>{HDFC_DASH_Constants.HDFC_DASH_CON_ACC_CUSTNO,HDFC_DASH_Constants.ID_STRING};
        mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + appId +HDFC_DASH_Constants.STRING_QUOTE);
        //system.debug('MapCondition--'+mapCondition);
        listruleEngine = HDFC_DASH_RuleEngine_Selector.getRuleEngineValBasedOnQuery(listOfFields_RuleEngine, mapCondition);
        //system.debug('size listruleEngine'+listruleEngine.size());
        //system.debug('listruleEngine'+listruleEngine);
        
        for(HDFC_DASH_Rule_Engine_Capture__c  ruleEngine : listruleEngine)
        {
            HDFC_DASH_RuleEngineReq_ILPSCallout.ruleEngineCapture listruleEnginenode = new HDFC_DASH_RuleEngineReq_ILPSCallout.ruleEngineCapture();
            
            listruleEnginenode = populateData(listruleEnginenode, ruleEngine);
            
            listruleEngineCapture.add(listruleEnginenode);			
        }
        ruleEngineReq.ruleEngineCapture = listruleEngineCapture;
        return ruleEngineReq;  
    }
    
    
    /*
Method: populateData
Description: This method will populate data from Rule Engine Capture object to the request body.
*/
    public HDFC_DASH_RuleEngineReq_ILPSCallout.ruleEngineCapture populateData(HDFC_DASH_RuleEngineReq_ILPSCallout.ruleEngineCapture listruleEnginenode, HDFC_DASH_Rule_Engine_Capture__c ruleEngine){
        List<String> listOfFields_ApplicantDet = new list<string>{HDFC_DASH_Constants.HDFC_DASH_CON_ACC_CUSTNO,HDFC_DASH_Constants.ID_STRING};
        listruleEnginenode.sfApplicationID = ruleEngine?.HDFC_DASH_Application__c;
        listruleEnginenode.sfApplicationNumber = ruleEngine?.HDFC_DASH_Application__r.HDFC_DASH_ApplicationNumber__c;
        listruleEnginenode.fileNumber = ruleEngine?.HDFC_DASH_Application__r.HDFC_DASH_File_Number__c;
        //system.debug('fileNumber '+listruleEnginenode.fileNumber);
        listruleEnginenode.sfApplicantID = ruleEngine?.HDFC_DASH_Applicant_Detail__c;
        listruleEnginenode.ruleId = ruleEngine?.HDFC_DASH_Rule_Id__c;
        listruleEnginenode.stage = ruleEngine?.HDFC_DASH_Stage__c;
        listruleEnginenode.docRuleEngineOutcome = ruleEngine?.HDFC_DASH_Doc_Rule_Engine_Outcome__c;
        listruleEnginenode.confidenceRuleEngineOutcome = ruleEngine?.HDFC_DASH_Confidence_Rule_Engine_Outcome__c;
        listruleEnginenode.customerResolved = ruleEngine?.HDFC_DASH_Customer_Resolved__c;
        listruleEnginenode.customerAction = ruleEngine?.HDFC_DASH_Customer_Action__c;
        listruleEnginenode.customerSkipped = ruleEngine?.HDFC_DASH_Customer_Skipped__c;
        listruleEnginenode.docCode = ruleEngine?.HDFC_DASH_Doc_Code__c;
        listruleEnginenode.docName = ruleEngine?.HDFC_DASH_Doc_Name__c;
        listruleEnginenode.subDetails = ruleEngine?.HDFC_DASH_Sub_Details__c;
         if(String.isNotBlank(listruleEnginenode.sfApplicantID))
                {
                    List<HDFC_DASH_Applicant_Details__c> listAppDet = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(new List<String>{listruleEnginenode.sfApplicantID},listOfFields_ApplicantDet,HDFC_DASH_Constants.ID_STRING);
                    listruleEnginenode.customerNumber = listAppDet[0].HDFC_DASH_Contact__r.Account.HDFC_DASH_Customer_Number__c;
                //system.debug('customer Number '+listruleEnginenode.customerNumber);
                }
        
        return listruleEnginenode;
    }
}