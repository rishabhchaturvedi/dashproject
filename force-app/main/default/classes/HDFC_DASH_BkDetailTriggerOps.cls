/*
className: HDFC_DASH_BkDetailTriggerOps
DevelopedBy: Tejeswari
Date: 24 September 2021
Company: Accenture
Class Description: This Class contains the trigger operation on ApplicantBankDetails object.
*/
public class HDFC_DASH_BkDetailTriggerOps {
/*
Class Description: This Class contains the trigger operation for prosessing latest flag beforeinsert
*/
    public with sharing class ChangeBKLatestFlagBeforeUpdate implements HDFC_DASH_TriggerOps{
        /* 
        Method Name :isEnabled
        Description :This method would check if excecute method's logic needs to run.
        */
		 public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applicantBankDetailsFirstRun;
        } 
        /*
        Method Name :filter
        Description :This method would filter for the records.
        */
     	public SObject[] filter(){
             List<HDFC_DASH_Applicant_Bank_Detail__c> BankDetRecList =
           HDFC_DASH_BkDetailTriggerOpsHelper.filterBDWhereConIsPopulated();
            //system.debug('inside ChangeBKLatestFlagBeforeUpdate');
            return BankDetRecList;
        }
        /*
        Method Name :execute
        Description :This method would execute the logic.
        */
     	public void execute(Sobject[] BankDetRecList){
            HDFC_DASH_BkDetailTriggerOpsHelper.changeBankDetRecBeforeUpdate(BankDetRecList); 
        }
     }
/*
Class Description: This Class contains the trigger operation for prosessing latest flag after insert
*/
public with sharing class UpdateApplicantBankDetailsLatestFlag implements HDFC_DASH_TriggerOps{
        /*
        Method Name :isEnabled
        Description :This method would check if excecute method's logic needs to run.
        */
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applicantBankDetailsFirstRun;
        }
        /*
        Method Name :filter
        Description :This method would filter for the records.
        */ 
        public SObject[] filter(){
           List<HDFC_DASH_Applicant_Bank_Detail__c> BankDetRecList =
           HDFC_DASH_BkDetailTriggerOpsHelper.filterBDforLatestFlagSenario();
            return BankDetRecList;
        } 
        /*
        Method Name :execute
        Description :This method would execute the logic.
        */
     	public void execute(Sobject[] BankDetRecList){
            HDFC_DASH_BkDetailTriggerOpsHelper.updateBKDetRec(BankDetRecList);
            HDFC_DASH_TriggerOpsRecursionFlags.applicantBankDetailsFirstRun = false;
           
        }  
    }
   
}