/*
Author: Anmol Srivastava
Class:HDFC_DASH_InboundRestService_StoreDetail
Description: Service Class for Store Document API
*/
public inherited sharing class HDFC_DASH_InboundRestService_StoreDetail {
    private static final string APP_DETAIL_SELECTOR_COND2='RecordTypeId';
    
    /* 
Method:processStoreDetails
Description: Method to process Spot Offer Details from JSON Req
*/   
    
    public static HDFC_DASH_APIResponse_StoreDetail processStoreDetails(RestRequest request){
        //Calling Parser Class
        HDFC_DASH_ParseStoreDetails Details = HDFC_DASH_ParseStoreDetails.parse(request);
        //Save Document details
        HDFC_DASH_APIResponse_StoreDetail response = saveDetails(Details);
        
        return response;
    }
    
    /* 
Method:saveDetails
Description: Method to save Spot offer Details in Application/Applicant Detail Object
*/ 
    public static HDFC_DASH_APIResponse_StoreDetail saveDetails(HDFC_DASH_ParseStoreDetails Details){
        
        List<Opportunity> listApp=new List<Opportunity>();
        HDFC_DASH_APIResponse_StoreDetail resp = new HDFC_DASH_APIResponse_StoreDetail();
        List<HDFC_DASH_Applicant_Details__c> listAppDetails=new List<HDFC_DASH_Applicant_Details__c>();
        list<string> fieldList=new list<string>{HDFC_DASH_Constants.ID_STRING};
        list<string> queryParameter =new list<string>{Details.storeDetails.sfApplicationId};
            
            Id Record =  Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByDeveloperName().get(HDFC_DASH_Constants.PRIMARY_APPLICANT).getRecordTypeId();
        
        Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Application => HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + Details.storeDetails.sfApplicationId + HDFC_DASH_Constants.STRING_QUOTE,
            HDFC_DASH_Constants.STRING_AND + APP_DETAIL_SELECTOR_COND2 => HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + Record + HDFC_DASH_Constants.STRING_QUOTE };
                
                
                //updating Application and Applicant Detail
                if(!String.isBlank(Details?.storeDetails?.sfApplicationID)){
                    
                    //Calling Application and Applicant Detail Selector Classes
                    listApp = HDFC_DASH_Application_Selector.getApplication(queryParameter, fieldList, HDFC_DASH_Constants.ID_STRING);
                    listAppDetails=HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(fieldList,condition);
                    if(!listApp.isEmpty() && !listAppDetails.isEmpty())
                    {
                        Opportunity objApplication = listApp[HDFC_DASH_Constants.INT_ZERO];
                        HDFC_DASH_Applicant_Details__c objAppDetail = listAppDetails[HDFC_DASH_Constants.INT_ZERO];
                        
                        objApplication.HDFC_DASH_Post_Payment_Spot_Offer__c = Details?.storeDetails?.postPaymentspotOfferEligible;
                        //objApplication.HDFC_DASH_Stage_Description__c = Details?.storeDetails?.leadStageDescription;
                        //objApplication.HDFC_DASH_Stage_Number__c = Details?.storeDetails?.leadStageNumber;
                        objApplication.HDFC_DASH_TF_Post_Payment_Spot_Offer__c = Details?.storeDetails?.postPaymentSpotOfferEligibilityTechnicalFailure==null?false: Details?.storeDetails?.postPaymentSpotOfferEligibilityTechnicalFailure;
                        
                        objAppDetail.HDFC_DASH_Transunion_CIBIL_ID__c = Details?.storeDetails?.cibilTransunion;
                        objAppDetail.HDFC_DASH_TF_Transunion_CIBILID__c = Details?.storeDetails?.cibilTransunionTechnicalFailure ==null?false:Details?.storeDetails?.cibilTransunionTechnicalFailure;
                        
                        Database.Update(objApplication);
                        Database.Update(objAppDetail);
                        
                        //setting response               
                        resp.successMessage = HDFC_DASH_Constants.STORE_DETAIL_SUCCESSMESSAGE;
                        resp.sfApplicationId = objApplication.id;
                        resp.sfApplicationNo = Details?.storeDetails?.sfApplicationNo;
                    }
                    else
                    {
                        HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_NOTEXIST);
                    }
                }
                else
                {
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_MISSING); 
                }        
        return resp;
    } 
}