/**
className: HDFC_DASH_RestResource_StrActivitiesTest
DevelopedBy: Janani Mohankumar
Date: 01 Oct 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestSer_StoreActivities
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_RestResource_StrActivitiesTest {
    
    private static final string SUCCESS ='Success';
    private static final integer SUCCESSCODE = 200;
    private static final string STRING_TYPE = 'Activity';
    private static final string STRING_MEETINGTYPE = 'In-person';
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR); 
    private static Id sysAdminId = UserInfo.getUserId();
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static Event event= HDFC_DASH_TestDataFactory_Event.getBasicEvent(app.id,con.id,sysAdminId);
    //private static HDFC_DASH_ILPS_Applications__c ilpsApp= HDFC_DASH_TestDataFactory_ILPSApps.getBasicILPSApplication();
    private static Lead leadRec = HDFC_DASH_TestDataFactory_Leads.getBasicLead();
    //Request url
    private static string requestUri = '/hdfc/storeactivities/V1.0';
    
    //Request body
    private static string reqBodyActivities ='{"activities":[{"title":"Meeting","type":"Activity","id":null,"status":"Open","description":"This is an open activity for a TL to work on an application","startDateTime":"2021-09-15T06:00:00.000Z","endDateTime":"2021-09-15T11:00:00.000Z","actualEndDateTime":"2021-09-15T11:30:00.000Z","meetingType":"In-person","isMissed":false,"assignedToName":"Sushant Sethi","assignedToSfId":"'+con.HDFC_DASH_Executive_Code__c+'","bsa":null,"fileNumber":"'+app.HDFC_DASH_File_Number__c+'","leadID":null,"latitude":37.77587,"longitude":-122.399902},'+
        '{"title":"Meeting","type":"Activity","id":null,"status":"Open","description":"This is an open activity for a TL to follow up with a SO","startDateTime":"2021-09-15T06:00:00.000Z","endDateTime":"2021-09-15T11:00:00.000Z","actualEndDateTime":"2021-09-15T11:30:00.000Z","meetingType":"In-person","isMissed":false,"assignedToName":"Sushant Sethi","assignedToSfId":"'+con.HDFC_DASH_Executive_Code__c+'","bsa":null,"fileNumber":null,"leadID":"'+leadRec.HDFC_DASH_LMS_Lead_ID__c+'","latitude":37.77587,"longitude":-122.399902},'+
        '{"title":"Meeting","type":"Activity","id":null,"status":"Open","description":"This is an open activity for a TL to follow up with a SO","startDateTime":"2021-09-15T06:00:00.000Z","endDateTime":"2021-09-15T11:00:00.000Z","actualEndDateTime":"2021-09-15T11:30:00.000Z","meetingType":"In-person","isMissed":false,"assignedToName":"Sushant Sethi","assignedToSfId":"'+sysAdminId+'","bsa":"'+acc.HDFC_DASH_Source_Rec_SrNo__c+'","fileNumber":null,"leadID":null,"latitude":37.77587,"longitude":-122.399902},'+
        '{"title":"Meeting","type":"Activity","id":null,"status":"Open","description":"This is an open activity for a TL to follow up with a SO","startDateTime":"2021-09-15T06:00:00.000Z","endDateTime":"2021-09-15T11:00:00.000Z","actualEndDateTime":"2021-09-15T11:30:00.000Z","meetingType":"In-person","isMissed":false,"assignedToName":"Sushant Sethi","assignedToSfId":"'+sysAdminId+'","bsa":null,"fileNumber":"'+app.HDFC_DASH_File_Number__c+'","leadID":null,"latitude":37.77587,"longitude":-122.399902},'+
        '{"title":"Meeting","type":"Activity","id":null,"status":"Open","description":"This is an open activity for a TL to follow up with a SO","startDateTime":"2021-09-15T06:00:00.000Z","endDateTime":"2021-09-15T11:00:00.000Z","actualEndDateTime":"2021-09-15T11:30:00.000Z","meetingType":"In-person","isMissed":false,"assignedToName":"Sushant Sethi","assignedToSfId":"'+sysAdminId+'","bsa":null,"fileNumber":null,"leadID":"'+leadRec.HDFC_DASH_LMS_Lead_ID__c+'","latitude":37.77587,"longitude":-122.399902}]}';
    
    //testRequestBodyExcp
    private static string reqBodyExcp ='{"activities":[{"title":"Meeting","type":"Activity","id":null,"status":"Open","description":"This is an open activity for a TL to work on an application","startDateTime":"2021-09-15T06:00:00.000Z","endDateTime":"2021-09-15T11:00:00.000Z","actualEndDateTime":"2021-09-15T11:30:00.000Z","meetingType":"In-person","isMissed":false,"assignedToName":"Sushant Sethi","assignedToSfId":"'+con.HDFC_DASH_Executive_Code__c+'","bsa":null,"fileNumber":"'+app.HDFC_DASH_File_Number__c+'","leadID":null,"latitude":37.77587,"longitude":-122.399902}]}';
    
    //Request body for Huddle
    private static string reqBodyHuddle ='{"activities":[{"title":"Meeting","type":"Huddle","id":null,"status":"Open","description":"This is an open activity for a TL to work on an application","startDateTime":"2021-09-15T06:00:00.000Z","endDateTime":"2021-09-15T11:00:00.000Z","actualEndDateTime":"2021-09-15T11:30:00.000Z","meetingType":"In-person","isMissed":false,"latitude":37.77587,"longitude":-122.399902}]}';
    /* Method Name: testSaveActivities
parameters: NONE
Method Description: This method is for success scenario  which call HDFC_DASH_RestResource_StoreActivities.storeActivities().
*/
    static TestMethod void testSaveActivities() 
    {        
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        
        RestRequest request = new RestRequest();
        RestResponse response= new RestResponse();
        request.requestUri = requestUri;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_POST;
        request.requestBody = Blob.valueOf(reqBodyActivities);
        
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        RestContext.request = request;
        RestContext.response = response;
        
        test.startTest();
        System.runAs(sysAdmin)
        {
            HDFC_DASH_RestResource_StoreActivities.storeActivities();
            test.stopTest();
            
            system.assertNotEquals(0, [select count() from Event Limit 1], SUCCESS);
        }
    }
        /* Method Name: testSaveHuddle
parameters: NONE
Method Description: This method is for success scenario for type Huddle  which call HDFC_DASH_RestResource_StoreActivities.storeActivities().
*/
    static TestMethod void testSaveHuddle() 
    {        
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        
        RestRequest request = new RestRequest();
        RestResponse response= new RestResponse();
        request.requestUri = requestUri;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_POST;
        request.requestBody = Blob.valueOf(reqBodyHuddle);
        request.addParameter(HDFC_DASH_Constants.STRING_TL,sysAdminId);
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        RestContext.request = request;
        RestContext.response = response;
        
        test.startTest();
        System.runAs(sysAdmin)
        {
            HDFC_DASH_RestResource_StoreActivities.storeActivities();
            test.stopTest();
            
            system.assertNotEquals(0, [select count() from Event where HDFC_DASH_Meeting_Type__c =:STRING_MEETINGTYPE], SUCCESS);
        }
    }
    
    
    /* Method Name: testException
parameters: NONE
Method Description: This method would check if the exception is thrown when the request headers are not sent
*/
    static TestMethod void testException() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        
        //Request Header is not added to check whether the exception is throw       
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = requestUri;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_POST;
        request.requestBody = Blob.valueOf(reqBodyActivities);       
        
        RestContext.request = request;
        RestContext.response = response;
        
        test.startTest();
        System.runAs(sysAdmin)
        {
            HDFC_DASH_RestResource_StoreActivities.storeActivities();
            test.stopTest();
            
            system.assertEquals(HDFC_DASH_Constants.INT_FIVE_HUNDRED, response.statusCode);            
        }        
    }   
    
    /* Method Name: testRequestBodyException
parameters: NONE
Method Description: This method would check if the request body custom exception is thrown.
*/
    static TestMethod void testRequestBodyException() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        
        RestRequest request = new RestRequest();
        RestResponse response= new RestResponse();
        request.requestUri = requestUri;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_POST;
        request.requestBody = Blob.valueOf(reqBodyExcp);
        
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        RestContext.request = request;
        RestContext.response = response;
        
        test.startTest();
        System.runAs(sysAdmin)
        {
            HDFC_DASH_RestResource_StoreActivities.storeActivities();
            test.stopTest();
            
            system.assertNotEquals(0, [select count() from Event Limit 1], SUCCESS);
        }     
    }    
    
}