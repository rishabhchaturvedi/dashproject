/**
className: HDFC_DASH_ParseLeadDetails
DevelopedBy: Sai Shruthi
Date: 19 July 2021
Company: Accenture
Class Description: Parser Class for Fee Details API .
**/
public inherited sharing class HDFC_DASH_ParseFeeDetails {
    public String sfApplicationId;
	public String sfApplicationNumber;
	public Decimal applicationProcessingFee;
	public String paymentTermsAccepted;
	public String paymentTermsContent;
	public Date paymentTermsAcceptedDate; 
	//public String leadStageNumber;
	//public String leadStageDescription;
   
	public ApplicationPaymentDetails applicationPaymentDetails;
	public FeeConstruct feeConstruct;

	/*
        Class: FeeConstruct
        Description: holds the FeeConstruct node details
    */
	public class FeeConstruct {
		public String feeType;
		public Decimal pfExclTax;
		public Decimal pfInclTax;
		public Decimal stampDuty;
		public Decimal sfExclTax;
		public Decimal sfInclTax;
		public Decimal minFeesExclTax;
		public Decimal minFeesInclTax;
		public Decimal feeosExclTax;
		public Decimal feeosInclTax;
		public Decimal subventAmt;
		public Decimal payableLater;
		public Decimal totFeeExclTax; 
		public Decimal totFeeInclTax;
		public Boolean feeConstructTechnicalFailure;
       
	}
	/*
        Class: PaymentDetail
        Description: holds the PaymentDetail node details
    */
	public class ApplicationPaymentDetails{
        public String modeOfPayment;
		public String merchantId;
		public String customerId;
		public String txnReferenceNo;
		public String bankReferenceNo;
		public String txnAmount;
		public Date txnDate;
		public String authStatus;
		public String errorStatus;
		public String errorDescription;
		public String checksum;
	}

	/*
        Method: parse
        Description: Method to Deserialize Fee Details request body
    */
	public static HDFC_DASH_ParseFeeDetails parse(RestRequest request) {
         string json = request.requestBody.toString();
		return (HDFC_DASH_ParseFeeDetails) System.JSON.deserialize(json, HDFC_DASH_ParseFeeDetails.class);
	}

}