/**
className: HDFC_DASH_InsertAppFinInfo_StoreAppSer
DevelopedBy: Sai Suman
Date: 13 Oct 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the Applicant Financial Information details.
**/
public with sharing class HDFC_DASH_InsertAppFinInfo_StoreAppSer {
    
/*
    Method Name :mapObligationDetails
    Parameters  :Id applicantId, HDFC_DASH_ParseApplication.ApplicantfinancialInfo parseAppFinDetails, HDFC_DASH_Applicant_Financial_Info__c appFinDetails
    Description :This method would store the obligation details in financial info record.
    */
    public static HDFC_DASH_Applicant_Financial_Info__c	mapObligationDetails(Id applicantId, HDFC_DASH_ParseApplication.ApplicantfinancialInfo parseAppFinDetails, HDFC_DASH_Applicant_Financial_Info__c appFinDetails){
        
        appFinDetails.RecordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Financial_Info__c
                                .getRecordTypeInfosByDeveloperName().get(parseAppFinDetails.applFinancialInfo.appFinancialType).getRecordTypeId();
        
        appFinDetails.HDFC_DASH_Applicant_Details__c = applicantId;
        appFinDetails.HDFC_DASH_ILPS_Financial_Info_Id__c = parseAppFinDetails.applFinancialInfo.ILPSApplFinancialInfoId;
                                return appFinDetails;

    }
    /*
    Method Name :mapPrePopAppFinDet
    Parameters  :Id applicantID,HDFC_DASH_ParseApplication.ApplicantfinancialInfo parseAppFinDetails,HDFC_DASH_Applicant_Financial_Info__c appFinDetails
    Description :This method would map pre populated applicant financial information details node (PrePopulatedAppFinancialInfo) for storeApp API and sends us the applicant Financial Information record.
    */
public static HDFC_DASH_Applicant_Financial_Info__c mapPrePopAppFinDet(Id applDetailId, HDFC_DASH_ParseApplication.ApplicantfinancialInfo parseAppFinDetails, HDFC_DASH_Applicant_Financial_Info__c appFinDetails ){
        appFinDetails.HDFC_DASH_Account_Holder_Name__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopAccountHolderName;
        appFinDetails.HDFC_DASH_Loan_Type__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopLoanType;
        appFinDetails.HDFC_DASH_Loan_Type_Code__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopLoanTypeCode;   
        appFinDetails.HDFC_DASH_Bank_Code__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopBankCode;
        appFinDetails.HDFC_DASH_Bank_Name__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopBankName;
        //appFinDetails.HDFC_DASH_Contact__c = customerId;
        appFinDetails.HDFC_DASH_Outstanding_Amount__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopOutstandingAmount;
        appFinDetails.HDFC_DASH_Installment_Amount__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopInstallmentAmount;
        appFinDetails.HDFC_DASH_Balance_Term_of_loan_in_months__c = parseAppFinDetails?.PrePopulatedAppFinancialInfo?.prePopBalanceTermOfLoanInMonths;
        return appFinDetails;
}
        /*
    Method Name :mapCustSelAppFinDet
    Parameters  :HDFC_DASH_ParseApplication.ApplicantfinancialInfo parseAppFinDetails,HDFC_DASH_Applicant_Financial_Info__c appFinDetails
    Description :This method would map customer selected applicant financial information details node (CustomerSelectedAppFinancialInfo) for storeApp API and sends us the applicant Financial Information record.
    */
 public static HDFC_DASH_Applicant_Financial_Info__c mapCustSelAppFinDet(HDFC_DASH_ParseApplication.ApplicantfinancialInfo parseAppFinDetails, HDFC_DASH_Applicant_Financial_Info__c appFinDetails ){
        appFinDetails.HDFC_DASH_CS_Account_Holder_Name__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelAccountHolderName;
        appFinDetails.HDFC_DASH_CS_Loan_Type__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelLoanType;
        appFinDetails.HDFC_DASH_CS_Loan_Type_Code__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelLoanTypeCode;
        appFinDetails.HDFC_DASH_CS_Outstanding_Amount__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelOutstandingAmount;
        appFinDetails.HDFC_DASH_CS_Bank_Code__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelBankCode;
        appFinDetails.HDFC_DASH_CS_Bank_Name__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelBankName;
        appFinDetails.HDFC_DASH_CS_Installment_Amount__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelInstallmentAmount;
        appFinDetails.HDFC_DASH_CS_BalanceTermofloaninMonths__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelBalanceTermOfLoanInMonths;
        appFinDetails.HDFC_DASH_CS_Investment_Type__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelinvestmentType;
        appFinDetails.HDFC_DASH_CS_Investment_Amount__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelinvestmentAmount;
        appFinDetails.HDFC_DASH_CS_Property_Type__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelPropertyType;
       	appFinDetails.HDFC_DASH_CS_End_Use__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelEndUse;
        appFinDetails.HDFC_DASH_CS_Property_City__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelPropertyCity;
        appFinDetails.HDFC_DASH_CS_Property_City_Code__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelPropertyCityCode;
        appFinDetails.HDFC_DASH_CS_Property_State__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelPropertyState;
        appFinDetails.HDFC_DASH_CS_Property_State_Code__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelPropertyStateCode;
        appFinDetails.HDFC_DASH_CS_Live_Loan__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelLiveLoan;
        appFinDetails.HDFC_DASH_CS_Institution_Name__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelInstitutionName;
        appFinDetails.HDFC_DASH_CS_Institution_Code__c = parseAppFinDetails?.CustomerSelectedAppFinancialInfo?.custSelInstitutionCode;
        return appFinDetails;
 }   
}