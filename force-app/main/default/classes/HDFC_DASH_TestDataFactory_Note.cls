/* 
ClassName: HDFC_DASH_TestDataFactory_Note
DevelopedBy: Ashita Jain
Date: 1 Nov 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Note object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_Note {
    public static final string NAME = 'Name';
   
/*
Method Name: createBasicNote
parameters: ID aplDetailID
Method Description: This method is to create test data for Note.
Pass fields of Note, and based on doInsert it will either
pass an instance of uninserted Note or inserted Note id. 
*/
    public static Note createBasicNote(ID aplDetailID)
    {
        Note noteObj = new Note();
        noteObj.parentID =  aplDetailID  ; 
        noteObj.title = 'Remark';
        return noteObj;        
    }
    
    /* Method Name: getBasicNote
    parameters: ID aplDetailID
    Method Description: This method is to generate test data for Note.
    */
    public static Note getBasicNote(ID aplDetailID)
    {
        Note noteObj = HDFC_DASH_TestDataFactory_Note.createBasicNote(aplDetailID);
        Database.insert(noteObj);
        return noteObj;
    }
   
}