@RestResource(urlMapping='/hdfc/getapplicationlist/V1.0/*')
/*
Author: Anisha Arumugam
Class Name:HDFC_DASH_RestResource_GetAppList
*/
global with sharing class HDFC_DASH_RestResource_GetAppList {
    
    /*
    Method:getApplicationList()
    Description:This method will get the list of Applications associated with a customer
    */
    @HttpGet
    global static void getApplicationList() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders;
        String response;
        Exception exp;
        HDFC_DASH_APIResponse_GetAppList apiResponse = new HDFC_DASH_APIResponse_GetAppList();  
        try
        { 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_GetAppList.getApplicationList(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                                            HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                                            + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 

            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_GETAPPLIST, HDFC_DASH_Constants.HANDLE_GETAPPLIST_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_GET, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_GETAPPLIST, request.requestBody.tostring(), (res.responseBody)?.tostring(), 
                                                                HDFC_DASH_Constants.METHOD_TYPE_GET, HDFC_DASH_Constants.HANDLE_GETAPPLIST_REQUEST, exp, request.headers);        

            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                        res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
}