/* ================================================
 * @Class Name :  IT_integration_log_enqueue
 * @author : Accenture
 * @Purpose: This purpose of this class is to create log history for Integration message logs.
 * @created date:
 ================================================*/
public class IT_integration_log_enqueue implements Queueable {
    
    private Integration_Message_Log__c objIntMessage;
     /***************************************************************************************************************
    *   @Name        :  IT_integration_log_enqueue                                                              
    *   @Parameters  :  Integration_Message_Log__c objIntMessage                                                                            
    *   @Description :  It is a constructor for class IT_integration_log_enqueue.                             
    ***************************************************************************************************************/
    public IT_integration_log_enqueue(Integration_Message_Log__c objIntMessage){
        this.objIntMessage = objIntMessage;
    }
    /***************************************************************************************************************
    *   @Name        :  Execute                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  createLogHistory method of InterfaceHandler to be called in execute method.                              
    ***************************************************************************************************************/
    public void execute(QueueableContext context) {
        InterfaceHandler.createLogHistory(objIntMessage);    
    }
}