/**
className: HDFC_DASH_InsertPayment_StoreAppService
DevelopedBy: Janani Mohankumar
Date: 30 Aug 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the Application Payment details.
**/
public inherited sharing class HDFC_DASH_InsertPayment_StoreAppService {
    /*
    Method Name :mapPaymentDetails
    Parameters  :Id applicationID, HDFC_DASH_ParseApplication.applicationPaymentDetails parsePaymentDetails
    Description :This method would map all the application payment details node for storeApp API and sends us the application payment record.
    */
    public static HDFC_DASH_Application_Payment_Details__c mapPaymentDetails(Id applicationID, HDFC_DASH_ParseApplication.applicationPaymentDetails parsePaymentDetails)
    {

        HDFC_DASH_Application_Payment_Details__c paymentDet = new HDFC_DASH_Application_Payment_Details__c();
        paymentDet.Id = parsePaymentDetails?.sfPaymentDetailId;
        paymentDet.HDFC_DASH_Application__c = applicationID;
        paymentDet.HDFC_DASH_Mode_Of_Payment__c = parsePaymentDetails?.modeOfPayment;
        paymentDet.HDFC_DASH_AuthStatus__c = parsePaymentDetails?.authStatus;
        paymentDet.HDFC_DASH_BankReferenceNo__c  = parsePaymentDetails?.bankReferenceNo;
        paymentDet.HDFC_DASH_Checksum__c = parsePaymentDetails?.checksum;
        paymentDet.HDFC_DASH_CustomerId__c = parsePaymentDetails?.customerId;
        paymentDet.HDFC_DASH_ErrorDescription__c = parsePaymentDetails?.errorDescription;
        paymentDet.HDFC_DASH_ErrorStatus__c = parsePaymentDetails?.errorStatus;
        paymentDet.HDFC_DASH_MerchantId__c = parsePaymentDetails?.merchantId;
        paymentDet.HDFC_DASH_SO_PT_Accepted__c = parsePaymentDetails?.soPtAccepted;
        paymentDet.HDFC_DASH_SO_PT_AcceptedDate__c = parsePaymentDetails?.soPtAcceptedDate;
        paymentDet.HDFC_DASH_SO_PT_Content__c = parsePaymentDetails?.soPtContent;
        paymentDet.HDFC_DASH_TxnAmount__c = parsePaymentDetails?.amount;
        paymentDet.HDFC_DASH_TxnDate__c = parsePaymentDetails?.txnDate;
        paymentDet.HDFC_DASH_TxnReferenceNo__c = parsePaymentDetails?.txnReferenceNo;
        paymentDet.HDFC_DASH_ILPS_Payment_Detail_Id__c = parsePaymentDetails?.ILPSPaymentDetailId;
        //system.debug('paymentDet'+paymentDet);
        
        return paymentDet;
    }
}