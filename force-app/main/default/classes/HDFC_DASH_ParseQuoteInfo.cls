/*
    Author: Anisha Arumugam
    Class: HDFC_DASH_ParseQuoteInfo
    Description: Parser Class for Quote Info API
*/
public without sharing class HDFC_DASH_ParseQuoteInfo {
	
	/*
        Class: PrePopulatedAddressDetails
        Description: Class to parse PrePopulated Address Details
    */
	public without sharing class PrePopulatedAddressDetails {
		public String addressLine1;
		public String addressLine2;
		public String addressLine3;
		public String addressLine4;
		public String landmark;
		public String city;
		public String cityCode;
		public String taluka;
		public String district;
		public String state;
		public String stateCode;
		public String country;
		public String countryCode;
		public String pinCode;
		public String postOfficeName;
	}
	/*
        Class:QuoteInfo
        Description: Class to parse Quote Information
    */
	public without sharing class QuoteInfo {
		public String sfApplicationID;
		public String sfApplicationNumber;
		public String sfApplicantID;
		public PrePopulatedAddressDetails prePopulatedAddressDetails;
		public PrePopulatedAddressDetails customerSelectedAddressDetails;
		public String addressType;
        public String addressFrom;
		public String addressSource;
		public String addressChangedByCustomer;
		public decimal totalExistingEMI;
		public decimal loanAmount;
		public String noEMIPaid;
        public String cibilLoanPurpose;
        public String loanPurpose;
		public EligibilityAmount eligibilityAmount;
		public CustomerSelectedSpotOffer customerSelectedSpotOffer;
		public String spotOfferEligible;
		public String spotOfferAccepted;
        public Boolean spotOfferEligibilityTechnicalFailure;
        public Boolean eligibilityAmountTechnicalFailure;
	}

	public QuoteInfo quoteInfo;
	/*
        Class:EligibilityAmount
        Description: Class to parse Eligibility Amount
    */
   public without sharing class EligibilityAmount {
		public integer loan_product;
        public string product_desc;
		public integer loan_term;
		public decimal requested_loan;
		public decimal current_roi;
		public integer possible_term;
		public integer stretched_term;
		public decimal loan_possible;
		public decimal stretched_amt;
		public decimal possible_emi;
		public decimal stretched_emi;
		public String concession;
		public decimal normal_iir;
		public decimal normal_foir;
		public decimal max_iir;
		public decimal max_foir;
		public String campaign_code;
		public decimal niir;
		public decimal nfoir;
		public decimal nlcr;
		public decimal nlvr;
		public decimal comb_lcr_ltv;
		public decimal ltv_val;
        public decimal plr_rate;
		public String plr_id;
       
	}
      /*
        Class: CustomerSelectedSpotOffer
        Description: Class to parse Customer Selected SpotOffer
    */
	public without sharing class CustomerSelectedSpotOffer {
		public decimal customerSelectedLoanAmount;
		public decimal customerSelectedEMIAmount;
		public String customerSelectedInterestType;
		public decimal customerSelectedInterestRate;
		public integer customerSelectedTenureInMonths;
	}

	/*
        Method: HDFC_DASH_ParseQuoteInfo
        Description: Method to Deserialize Quote Info API request body
    */
	public static HDFC_DASH_ParseQuoteInfo parse(RestRequest request) {
        string json = request.requestBody.toString();
		return (HDFC_DASH_ParseQuoteInfo) System.JSON.deserialize(json, HDFC_DASH_ParseQuoteInfo.class);
	}
}