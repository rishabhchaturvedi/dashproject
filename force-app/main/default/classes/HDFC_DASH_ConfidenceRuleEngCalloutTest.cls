/*
Class: HDFC_DASH_ConfidenceRuleEngCalloutTest
Author: Sai Suman
Date: 19 Nov 2021
Company: Accenture
Description:This is the test class for Queueable Class for HDFC_DASH_ConfidenceRuleEngineCallout.
*/
@isTest(SeeAllData=false)
public with sharing class HDFC_DASH_ConfidenceRuleEngCalloutTest {
//Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Details__c appDet = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c applEmpDetail =
                        HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.Id,con.id);
    private static HDFC_DASH_Applicant_Employment_Details__c applEmpDet =
                        HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDet.Id,con.id);
                                          

    /* Method Name: testConfidenceRuleEngineCallout
    parameter:none
    Method Description: This method will enqueue and test the BRE Eligibility Callout.
    */ 
   static testmethod void testConfidenceRuleEngineCallout(){
        System.runAs(sysAdmin)
        { 
            app.HDFC_DASH_Loan_Type__c = 'HO';
            update app;
            appDetails.HDFC_DASH_Form16_Assessment_Year__c = '2020-2021';
            appDetails.HDFC_DASH_SO_Fixed_Monthly_Income__c = 100000;
            appDetails.HDFC_DASH_PAN__c = null;
			appDetails.HDFC_DASH_GrossBusinessIncome__c = (appDetails.HDFC_DASH_SO_Fixed_Monthly_Income__c)*12;
            update appDetails;
            applEmpDetail.HDFC_DASH_Nat_Of_Emp__c = 'S';
            applEmpDetail.HDFC_DASH_Self_Emp_Type__c = 'SEB';
            update applEmpDetail;
			appDet.name ='111134';
			appDet.HDFC_DASH_SO_Fixed_Monthly_Income__c = 100000;
			appDet.HDFC_DASH_PAN__c = null;
			appDet.HDFC_DASH_GrossBusinessIncome__c = (appDet.HDFC_DASH_SO_Fixed_Monthly_Income__c)*12;
			update appDet;
            applEmpDet.HDFC_DASH_Nat_Of_Emp__c = 'S';
            applEmpDet.HDFC_DASH_Self_Emp_Type__c = 'SEB';
            update applEmpDet;
        ID jobID=null;
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        HDFC_DASH_TestDataFactory_API.createInterfaceSetting_ConfidenceRuleEng();
        Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockConfidenceRuleEngResGen());
        HDFC_DASH_ConfidenceRuleEngineCallout confidenceRuleEngCallOut  = new HDFC_DASH_ConfidenceRuleEngineCallout(app.Id);
		Test.startTest();
        jobID = System.enqueueJob(confidenceRuleEngCallOut);
        Test.stopTest();  
        System.assertNotEquals(null,jobID);
        }
    }
    
    /*
  Method Name: testConfidenceRuleEngCalloutException
  Method Description: This method will test the exception scenario for EPFO callout
*/
    static testmethod void testConfidenceRuleEngCalloutException(){
        System.runAs(sysAdmin)
        {
        ID jobID=null;
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        HDFC_DASH_TestDataFactory_API.createInterfaceSettingPollingPaymentStatus();
        Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockConfidenceRuleEngResGen());
        HDFC_DASH_ConfidenceRuleEngineCallout confidenceRuleEngCallOut  = new HDFC_DASH_ConfidenceRuleEngineCallout(app.Id);
		Test.startTest();
        jobID = System.enqueueJob(confidenceRuleEngCallOut);
        Test.stopTest();  
        System.assertNotEquals(null,jobID);
        }
    }
}