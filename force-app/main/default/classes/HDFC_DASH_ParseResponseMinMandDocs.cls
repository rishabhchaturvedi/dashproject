/*
    Author: Tejeswari
    Class: HDFC_DASH_ParseMinMandDocs
    Description: Parser Class for Min Mand Docs API
*/
public without sharing class HDFC_DASH_ParseResponseMinMandDocs {
    
	public List<ListOfDocs> listOfDocs;
	public Integer errorCode;
	public String errorMsg;

     /*
        Class: listOfDocs
        Description: Class to parse the response body of Min Mand Doc callout to ILPS
    */
	public without sharing class ListOfDocs {
		public Integer sfapplicantno;
		public String doccode;
		public String dockey4val;
		public String dockey5val;
		public String docfromdate;
		public String doctodate;
		public String mindoc;
		public String manddoc;
	}

	/*
        Method: parse
        Description: Method to Deserialize the MinMand API's response body
    */
	public static HDFC_DASH_ParseResponseMinMandDocs parse(String json) {
		return (HDFC_DASH_ParseResponseMinMandDocs) System.JSON.deserialize(json, HDFC_DASH_ParseResponseMinMandDocs.class);
	}
}