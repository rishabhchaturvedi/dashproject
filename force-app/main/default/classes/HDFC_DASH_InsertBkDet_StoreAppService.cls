/**
className: HDFC_Dash_InsertBankDetails_StoreAppService
DevelopedBy: Raghavendra Koora
Date: 23 Sep 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the Applicant Bank details.
**/
public class HDFC_DASH_InsertBkDet_StoreAppService {

    public static HDFC_DASH_Applicant_Bank_Detail__c mapPrePopBKDetails(HDFC_Dash_ParseApplication.BankDetailInfo parseBankDetails, HDFC_DASH_Applicant_Bank_Detail__c bankDetails ){
        bankDetails.HDFC_DASH_Account_Holder_Name__c = parseBankDetails?.PrepopulatedBankDetails?.accountHolderName;
        bankDetails.HDFC_DASH_Account_Number__c = parseBankDetails?.PrepopulatedBankDetails?.acountNumber;
        bankDetails.HDFC_DASH_Account_Type__c = parseBankDetails?.PrepopulatedBankDetails?.accountType;
        //bankDetails.HDFC_DASH_Applicant_Details__c = applDetailId;
        bankDetails.HDFC_DASH_Bank_Code__c = parseBankDetails?.PrepopulatedBankDetails?.bankCode;
        bankDetails.HDFC_DASH_Bank_Name__c = parseBankDetails?.PrepopulatedBankDetails?.bankName;
        //bankDetails.HDFC_DASH_Contact__c = customerId;
        bankDetails.HDFC_DASH_From_Date__c = parseBankDetails?.PrepopulatedBankDetails?.fromDate;
        bankDetails.HDFC_DASH_To_Date__c = parseBankDetails?.PrepopulatedBankDetails?.toDate;
        bankDetails.HDFC_DASH_IFSC_Code__c = parseBankDetails?.PrepopulatedBankDetails?.ifscCode;

        return bankDetails;

    }

    /*
    Method Name :mapCustSelBKDetails
    Parameters :customerId, applDetailId, HDFC_Dash_ParseApplication.BankAccountDetails parseBankDetails,HDFC_DASH_Applicant_Bank_Detail__c bankDetails
    Description :This method would map all the applicant Bank details node for storeApp API and sends us the bank info record.
    */
    public static HDFC_DASH_Applicant_Bank_Detail__c mapCustSelBKDetails(HDFC_Dash_ParseApplication.BankDetailInfo parseBankDetails, HDFC_DASH_Applicant_Bank_Detail__c bankDetails ){
        bankDetails.HDFC_DASH_CS_Account_Holder_Name__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelAccountHolderName;
        bankDetails.HDFC_DASH_CS_Account_Number__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelAccountNumber;
        bankDetails.HDFC_DASH_CS_Account_Type__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelAccountType;
        bankDetails.HDFC_DASH_CS_Bank_Code__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelBankCode;
        bankDetails.HDFC_DASH_CS_Bank_Name__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelBankName;
        bankDetails.HDFC_DASH_CS_From_Date__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelFromDate;
        bankDetails.HDFC_DASH_CS_To_Date__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelToDate;
        bankDetails.HDFC_DASH_CS_Ifsc_Code__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelIfscCode;
        bankDetails.HDFC_DASH_CS_SWIFT_Code__c = parseBankDetails?.CustomerSelectedBankDetails?.custSelSwiftCode;

        return bankDetails;
    }
}