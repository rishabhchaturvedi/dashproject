/*
Class: HDFC_DASH_FirstFilePushCallout
Author: Raghavendra Koora
Date: 12 December 2021
Company: Accenture
Description: Queueable Class to update lead stage Callout
*/
public inherited sharing class HDFC_DASH_ILPS_FilePush implements Queueable,Database.AllowsCallouts{
    Id appId;
    Boolean ilpsSecondPush;
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    RestRequest request;
    Exception exp; 
    public HDFC_DASH_ILPS_FilePush(Id appId,Boolean ilpsSecondPush) //applicationID
    {
        this.appId = appId;
        this.ilpsSecondPush = ilpsSecondPush;
    }
    
    /*
Method: execute
Description: This method will Post the Document Details to ILPS.
*/
    public void execute(QueueableContext context) 
    {
        //System.debug('This is Update Lead Stage Callout');
        try
        {
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
    
            Boolean firstFilePush = HDFC_DASH_ILPS_FilePush_Callout.calloutToILPS(appId);
            if(!ilpsSecondPush){
                Opportunity app = new Opportunity();
                app.id = appId;
        		app.HDFC_DASH_File_First_Push_To_ILPS__c = firstFilePush;
                update app;
            }
        	
        } 
        catch(Exception e)
        {      
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ILPS_FILE_PUSH, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }   
}