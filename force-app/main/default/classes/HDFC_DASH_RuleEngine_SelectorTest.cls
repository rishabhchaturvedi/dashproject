/**
className: HDFC_DASH_RuleEngine_SelectorTest
DevelopedBy: Kumar Gourav
Date: 4 Dec 2021
Company: Accenture
Class Description: Test class for Rule Engine Capture class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_RuleEngine_SelectorTest {

    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appl = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id,app.id);
    private static HDFC_DASH_Rule_Engine_Capture__c ruleEngineCapture = 
                        HDFC_DASH_TestDataFactory_RuleEngine.getBasicRuleEngineCapture(app.id,appl.id);
    /* 
    Method Name :testGetRuleEngineValBasedOnQuery
    Description :This method will test the method getRuleEngineValBasedOnQuery().
    */
      static testmethod void testGetRuleEngineValBasedOnQuery(){
        List<HDFC_DASH_Rule_Engine_Capture__c> listRuleEngCapture = null;
        System.runAs(sysAdmin){
            //List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            //Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_APPDETAILS=>
              //  HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
               // appDetails.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Map<string,string> mapCondition = new Map<string,string>(); 
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + app.Id +HDFC_DASH_Constants.STRING_QUOTE);
            Test.startTest();
                listRuleEngCapture = HDFC_DASH_RuleEngine_Selector.getRuleEngineValBasedOnQuery(new List<String>{HDFC_DASH_Constants.HDFC_DASH_Application},mapCondition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listRuleEngCapture);
        system.assertEquals(1,listRuleEngCapture.size()); 
      }
}