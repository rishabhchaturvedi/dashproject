/**
className: HDFC_DASH_User_SelectorTest
DevelopedBy: Utkarsh Patidar
Date: 28 Sep 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_User_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_User_SelectorTest {
    
//Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string SUCCESS ='Success';
    
     /* 
    Method Name :testGetUser
    Description :This method will test the method getUser().
    */
    static testmethod void testGetUser(){
        List<User> resUserRec = new List<User>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            resUserRec = HDFC_DASH_User_Selector.getUser(new List<String> {sysAdmin.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, resUserRec);
            system.assertEquals(1,resUserRec.size());  
        }
    }
    /* 
    Method Name :testGetUserBasedOnQuery
    Description :This method will test the method getUserBasedOnQuery().
    */
    static testmethod void testGetUserBasedOnQuery(){
        List<User> listUserRec  = new List<User>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.Id_STRING =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                sysAdmin.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listUserRec = HDFC_DASH_User_Selector.getUserBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listUserRec);
        system.assertEquals(1,listUserRec.size(),SUCCESS);       
      }
        /* 
    Method Name :testGetUserRecBasedOnString
    Description :This method will test the method getUserRecBasedOnString().
    */
    static testmethod void testGetUserRecBasedOnString(){
        List<User> listUserRec1  = new List<User>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
             listUserRec1 = HDFC_DASH_User_Selector.getUserRecBasedOnString(fieldList,sysAdmin.Id, HDFC_DASH_Constants.ID_STRING);
            Test.stopTest();
        }
        system.assertNotEquals( null, listUserRec1);
        system.assertEquals(1,listUserRec1.size(),SUCCESS);       
      }
}