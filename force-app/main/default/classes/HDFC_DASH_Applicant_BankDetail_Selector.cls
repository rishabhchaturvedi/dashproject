/**
className: HDFC_DASH_Applicant_BankDetail_Selector
DevelopedBy:Punam Marbate
Date: 24 sep 2021
Company: Accenture
Class Description: This class would create the select queries for Applicant Bank Detail object .
**/
public with sharing class HDFC_DASH_Applicant_BankDetail_Selector {
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Applicant_Bank_Detail__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    /* 
Method Name :getBankDetailsBasedOnId
Parameters  :List<string> fieldList, Id bankDetailId
Description :This method would get a single Applicant Bank details based on the ID.
*/
  /*  public static HDFC_DASH_Applicant_Bank_Detail__c getBankDetailsBasedOnId(List<string> fieldList, Id bankDetailId)
    {
        string querystring = SELECT_STRING 
            +String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT
            + WHERE_STRING+ ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO
            + HDFC_DASH_Constants.STRING_QUOTE+bankDetailId+ HDFC_DASH_Constants.STRING_QUOTE;
        HDFC_DASH_Applicant_Bank_Detail__c bankDetail = database.query(querystring);
        return bankDetail;
    } */
    /* 
Method Name :getBankDetailBasedOnIds
Parameters:List<string> fieldList,List<ID> bankDetailIds
Description :This method would get the Applicant Bank details based on the IDs.
*/
   /* public static List<HDFC_DASH_Applicant_Bank_Detail__c>getBankDetailBasedOnIds
        (List<string> fieldList,List<ID> bankDetailIds)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT 
            +String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT+ WHERE_STRING 
            +HDFC_DASH_CONSTANTS.STRING_ID + HDFC_DASH_CONSTANTS.STRING_IN
            +HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE
            +String.join(bankDetailIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)
            +HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<HDFC_DASH_Applicant_Bank_Detail__c> bankDetList = database.query(querystring); 
        return bankDetList;
    } */
    /* 
Method Name :getBankDetailsBasedOnQuery
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
Description :This method would get the List of Applicant Bank Details based on fieldsAndparameters sent to it.
*/
    public static List<HDFC_DASH_Applicant_Bank_Detail__c> getBankDetailsBasedOnQuery
        (List<String> fieldList,Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT
            +String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT 
            + WHERE_STRING +getCondition(fieldsAndparameters) ;
        
        List<HDFC_DASH_Applicant_Bank_Detail__c> bankDetailList = database.query(querystring); 
        return bankDetailList;   
    }
    
    /* 
Method Name :getCondition
Parameters  :Map<String,String> fieldsAndparameters
Description :This method would be called from getBankDetailsBasedOnQuery .
*/
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
           
        }
        return condition;
    }
    /* 
Method Name :getBankDetails
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of Applicant Bank details based on only one condition.
*/
    public static List<HDFC_DASH_Applicant_Bank_Detail__c> getBankDetails
        (List<String> queryParameters, List<string> fieldList, String conditionOn)
    {
        string querystring = SELECT_STRING 
            +String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT 
            + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<HDFC_DASH_Applicant_Bank_Detail__c> bankDetailList = database.query(querystring);
        return bankDetailList;
    }
    /*
Method Name :getBankDetailsBasedOnContactIds
Parameters  :List<String> fieldList, List<Id> contactIds
Description :This method would get the Accounts based on the IDs.
*/
    /*public static List<HDFC_DASH_Applicant_Bank_Detail__c> getBankDetailsBasedOnContactIds(List<String> fieldList, List<Id> contactIds){

String querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA)
+ HDFC_DASH_Constants.STRING_FROMAPPLICANTBANKDETAIL_WHERE + HDFC_DASH_Constants.BANK_DETAIL_LATEST+ HDFC_DASH_Constants.STRING_EQUALTO
+ HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_YES + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Contact + HDFC_DASH_CONSTANTS.STRING_IN
+ HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(contactIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;

List<HDFC_DASH_Applicant_Bank_Detail__c> applBkDetailsList = database.query(querystring);

return applBkDetailsList;
}*/
    
}