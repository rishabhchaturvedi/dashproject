/**
className: HDFC_DASH_Funding_Source_SelectorTest
DevelopedBy: Janani Mohankumar
Date: 22 Sep 2021
Company: Accenture
Class Description: Test class for Funding_Source_Selector class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_Funding_Source_SelectorTest {
    
//Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Funding_Source__c fundingSource = HDFC_DASH_TestDataFactory_FundingSource.getBasicFundingSource(app.id);
    private static final string SUCCESS ='Success';
    
     /* 
    Method Name :testGetFundingSource
    Description :This method will test the method getFundingSource().
    */
    /*static testmethod void testGetFundingSource(){
        List<HDFC_DASH_Funding_Source__c> resFundingSourceRec = new List<HDFC_DASH_Funding_Source__c>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            resFundingSourceRec = HDFC_DASH_Funding_Source_Selector.getFundingSource(new List<String> {fundingSource.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, resFundingSourceRec);
            system.assertEquals(1,resFundingSourceRec.size());  
        }
    }*/
    /* 
    Method Name :testGetFundingSourceBasedOnQuery
    Description :This method will test the method getFundingSourceBasedOnQuery().
    */
    static testmethod void testGetFundingSourceBasedOnQuery(){
        List<HDFC_DASH_Funding_Source__c> listFundingSourceRec  = new List<HDFC_DASH_Funding_Source__c>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Application =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                app.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listFundingSourceRec = HDFC_DASH_Funding_Source_Selector.getFundingSourceBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listFundingSourceRec);
        system.assertEquals(1,listFundingSourceRec.size(),SUCCESS);       
      }
}