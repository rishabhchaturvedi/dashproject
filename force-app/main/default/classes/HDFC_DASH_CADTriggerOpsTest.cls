/**
className: HDFC_DASH_CADTriggerOpsTest
DevelopedBy: Sai Suman
Date: 01 October 2021
Company: Accenture
Class Description: test class for HDFC_DASH_CADTriggerOps.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_CADTriggerOpsTest {
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static HDFC_DASH_Contact_Address_Details__c cadDetail =
        HDFC_DASH_TestDataFactory_ContAddDetail.getBasicContAddressDetail(null);
    private static HDFC_DASH_Contact_Address_Details__c cadDetails =
        HDFC_DASH_TestDataFactory_ContAddDetail.getBasicContAddressDetail(null);
    private static List<HDFC_DASH_Contact_Address_Details__c> cadDetailsList =
        HDFC_DASH_TestDataFactory_ContAddDetail.getBasicCadDetail(3,con.id);
    
    /*
Method Name :testForException
Description :This method would test the trigger for exception.
*/
    @isTest public static void testForException()
    {
        Exception testException ;
        System.runAs(sysAdmin)
        {
            try{
                HDFC_DASH_Contact_Address_Details__c excep = new HDFC_DASH_Contact_Address_Details__c();
                //excep.HDFC_DASH_Address_From__c='18 characters';
                //excep.HDFC_DASH_Pincode__c='123456789A';
                excep.HDFC_DASH_Contact__c = con.id;
                database.insert (excep);
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_NULLPOINTER_EXP);
            }
            catch (Exception e) { 
                testException=e;
            }
            system.assertNotEquals(null, testException);
        }
    }
    /* 
Method Name :testUpdateConAddDetRec
Description :This method would test the trigger for contact address details records.
*/  
@isTest public static void testUpdateConAddDetRec() {
        
        Exception testException ;
        System.runAs(sysAdmin)
        {
            test.startTest();
            try
            {
                
                List<HDFC_DASH_Contact_Address_Details__c> cadList = new List<HDFC_DASH_Contact_Address_Details__c>(); 
                for(HDFC_DASH_Contact_Address_Details__c cad :cadDetailsList){
                    cad.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_YES;
                    cadList.add(cad);
                }
                update cadList;
                cadDetails.HDFC_DASH_Contact__c = con.Id;
                update cadDetails;
                
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            HDFC_DASH_Contact_Address_Details__c cadRec = [select id,HDFC_DASH_Latest__c from HDFC_DASH_Contact_Address_Details__c where id =:cadDetails.id];
            system.assertEquals(HDFC_DASH_Constants.STRING_YES,cadRec.HDFC_DASH_Latest__c);
            system.assertEquals(null, testException);
        }
    }
    /* 
Method Name :testUpdateConAddDetAddress
Description :This method would test the trigger for contact address details records.
*/  
    @isTest public static void testUpdateConAddDetAddress() {
        
        Exception testException ;
        System.runAs(sysAdmin)
        {
            test.startTest();
            try
            {
                HDFC_DASH_Contact_Address_Details__c cadDet = new HDFC_DASH_Contact_Address_Details__c();
                con.AccountId = acc.id;
                update con;
                cadDet.HDFC_DASH_Address_Type__c = HDFC_DASH_Constants.STRING_C;
                cadDet.HDFC_DASH_Address_Line1_CustomerSelected__c = 'Test';
                cadDet.HDFC_DASH_City_CustomerSelected__c = 'Bangalore';
                cadDet.HDFC_DASH_Contact__c = con.id;
                database.insert(cadDet);
                Contact newcon = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
                cadDet.HDFC_DASH_Contact__c = newcon.Id;
                database.update(cadDet);
                
                cadDetail.HDFC_DASH_Address_Type__c = HDFC_DASH_Constants.STRING_P;
                cadDetail.HDFC_DASH_Address_Line1_CustomerSelected__c = 'Test';
                cadDetail.HDFC_DASH_City_CustomerSelected__c = 'Bangalore';
                cadDetail.HDFC_DASH_Contact__c = con.id;
                database.update(cadDetail);
                Contact newContact = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
                cadDetail.HDFC_DASH_Contact__c = newContact.Id;
                database.update(cadDetail);
            }
            catch (Exception e) { 
                testException=e;
            }
             test.stopTest();
             HDFC_DASH_Contact_Address_Details__c cadRec = [select id,HDFC_DASH_Address_Type__c from HDFC_DASH_Contact_Address_Details__c where id =:cadDetail.id];
            system.assertEquals(HDFC_DASH_Constants.STRING_P,cadRec.HDFC_DASH_Address_Type__c);
            system.assertEquals(null, testException);
        }
    }
}