/**
className: HDFC_DASH_RestResource_StoreDetailTest
DevelopedBy: Anmol Srivastava
Date: 19 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_StoreDetails
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_RestResource_StoreDetailTest {

    private static final string SUCCESS ='Success';
    private static final string LEADSTAGEDESC ='Complete';
    private static final string TRANSUNIONCIBILID = '100032566162';
    private static final integer SUCCESSCODE = 200;
    private static final integer ERRORCODE = 500;

    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(con.id, app.id);

    //Request URL
    private static string requestUri = 'https://hdfclimited--hdfcdev.my.salesforce.com/services/apexrest/hdfc/storedetails/V1.0';
    
    //Request body
    private static string reqBodyStoreDetails = '{"storeDetails":{"sfApplicationId":"'+app.id+'","sfApplicationNo":"'+app.HDFC_DASH_ApplicationNumber__c+'","cibilTransunion":"100032566162","leadStageNumber":"6","leadStageDescription":"Complete","postPaymentspotOfferEligible":"Y"}  } ';


 /* Method Name: teststoreDetails
parameters: NONE
Method Description: This method would check if the Inbound PATCH request(Store Details) is getting processed and the request data is getting stored in the appropriate objects.
*/
static TestMethod void teststoreDetails() 
{        
    HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
    
    RestRequest request = new RestRequest();
    RestResponse response = new RestResponse();
    request.requestUri = requestUri;
    request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
    request.requestBody = Blob.valueOf(reqBodyStoreDetails);
    
    request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
    RestContext.request = request;
    RestContext.response = response;
    Id Record = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByDeveloperName().get(HDFC_DASH_Constants.PRIMARY_APPLICANT).getRecordTypeId();
    appDetails.RecordTypeId=Record;
    
    test.startTest();
    System.runAs(sysAdmin)
    {
        Database.insert(appDetails);
        HDFC_DASH_RestResource_StoreDetail.storeDetails();
        test.stopTest();
        
        //system.assertEquals(1, [select count() from Opportunity where HDFC_DASH_Stage_Description__c =:LEADSTAGEDESC], SUCCESS);
        system.assertEquals(1, [select count() from HDFC_DASH_Applicant_Details__c where HDFC_DASH_Transunion_CIBIL_ID__c =:TRANSUNIONCIBILID], SUCCESS);
        
    }
}

/* Method Name: testException
parameters: NONE
Method Description: This method would check if the exception is thrown when the request headers are not sent
*/
static TestMethod void testException() 
{
    HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();

    //Request Header is not added to check whether the exception is throw       
    RestRequest request = new RestRequest();
    RestResponse response = new RestResponse();
    request.requestUri = requestUri;
    request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
    request.requestBody = Blob.valueOf(reqBodyStoreDetails);       
    RestContext.request = request;
    RestContext.response = response;
    
    test.startTest();
    System.runAs(sysAdmin)
    {
        HDFC_DASH_RestResource_StoreDetail.storeDetails();
        test.stopTest();

        system.assertEquals(ERRORCODE, response.statusCode);            
    }        
}    

}