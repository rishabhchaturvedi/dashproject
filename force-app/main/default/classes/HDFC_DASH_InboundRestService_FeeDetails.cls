/**
className: HDFC_DASH_InboundRestService_FeeDetails
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class would store the fee details  received from fee details API.
**/
public inherited sharing class HDFC_DASH_InboundRestService_FeeDetails {
    
    /* 
    Method Name :processFeeDetails
    Parameters  :RestRequest request
    Description :This method return the response for the API request from HDFC_DASH_RestResource_FeeDetails.
    */
    public static HDFC_DASH_APIResponse_FeeDetails processFeeDetails(RestRequest request)
    {
        //Calling JSON Parser Class
        HDFC_DASH_ParseFeeDetails feeDetails = HDFC_DASH_ParseFeeDetails.parse(request);
        
        HDFC_DASH_APIResponse_FeeDetails apiRes = new HDFC_DASH_APIResponse_FeeDetails();
        
        if(!String.isBlank(feeDetails?.sfApplicationId)){
       
            if(feeDetails?.applicationPaymentDetails?.authStatus == HDFC_DASH_Constants.AUTHSTATUS_0300)
            {
                //Do Spot Offer File Callout to ILPS
                HDFC_DASH_SpotOfferFile_ILPSCallout soILPSCallout = new HDFC_DASH_SpotOfferFile_ILPSCallout();
                apiRes = soILPSCallout.doSpotOfferFileCallout(feeDetails, apiRes);
                
                //Enqueue the Document Details Callout to ILPS
                HDFC_DASH_DocDetails_ILPSCallout ddILPSCallout = new HDFC_DASH_DocDetails_ILPSCallout(feeDetails.sfApplicationId,HDFC_DASH_Constants.BOOLEAN_FALSE);
                system.enqueueJob(ddILPSCallout);
            }
                    
            //Update Application, insert HDFC_DASH_Application_Payment_Details__c records
            apiRes = updateFeeDetails(feeDetails, apiRes);
            
            //Enqueue the Enrich Lead callout to LMS
            HDFC_DASH_EnrichLead_LMSCallout enrichLead = new HDFC_DASH_EnrichLead_LMSCallout(feeDetails.sfApplicationId);        
            System.enqueueJob(enrichLead);
        }
        else {
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_MISSING); 
        } 
        return apiRes;
    }   
    /*Method:updateFeeDetails
    Parameters  :HDFC_DASH_ParseFeeDetails feeDetails
    Description:This method will update the Application and insert Fee Detail and Application payment detail record.
    */
    public static HDFC_DASH_APIResponse_FeeDetails updateFeeDetails(HDFC_DASH_ParseFeeDetails feeDetails, HDFC_DASH_APIResponse_FeeDetails apiRes)
    { 

            List<String> fieldAppList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Stage_Number,HDFC_DASH_Constants.HDFC_DASH_Stage_Description,HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber};
            Opportunity application = HDFC_DASH_Application_Selector.getApplicationBasedOnId(fieldAppList, feeDetails?.sfApplicationId);

            application.HDFC_DASH_SO_App_Processing_Fee__c = feeDetails?.applicationProcessingFee;
           // if(feeDetails?.applicationPaymentDetails?.authStatus == HDFC_DASH_Constants.AUTHSTATUS_0300){
                //application.HDFC_DASH_Stage_Number__c = HDFC_DASH_Constants.INT_ONE;
            //}
            
            //Insert fee Detail record
            Id feeId = insertFeeDetails(application?.id, feeDetails);
            
            //Insert Appliation Payment Detail record
            insertAppPaymentDetail(application?.id,feeDetails,feeId);
       
        
            //create  response for fee details API
            apiRes = createResponseStructure(application, apiRes);
        
       
        return apiRes;    
    }
    /*Method:insertFeeDetails
    Parameters  :Id applicationId,HDFC_DASH_ParseFeeDetails feeDetails
    Description:This method will This method will update the Application record.
    */
    public static Id insertFeeDetails(Id applicationId,HDFC_DASH_ParseFeeDetails feeDetails)
    {
        HDFC_DASH_Fee_Detail__c feeDetailObj = new HDFC_DASH_Fee_Detail__c();
        feeDetailObj.HDFC_DASH_Application__c = applicationId;
        feeDetailObj.HDFC_DASH_Fee_Construct_pfExclTax__c = feeDetails?.feeConstruct?.pfExclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_pfInclTax__c = feeDetails?.feeConstruct?.pfInclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_stampDuty__c = feeDetails?.feeConstruct?.stampDuty;
        feeDetailObj.HDFC_DASH_Fee_Construct_sfExclTax__c = feeDetails?.feeConstruct?.sfExclTax; 
        feeDetailObj.HDFC_DASH_Fee_Construct_sfInclTax__c = feeDetails?.feeConstruct?.sfInclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_minFeesExclTax__c = feeDetails?.feeConstruct?.minFeesExclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_minFeesInclTax__c = feeDetails?.feeConstruct?.minFeesInclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_feeosExclTax__c = feeDetails?.feeConstruct?.feeosExclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_feeosInclTax__c = feeDetails?.feeConstruct?.feeosInclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_subventAmt__c = feeDetails?.feeConstruct?.subventAmt;
        feeDetailObj.HDFC_DASH_Fee_Construct_payableLater__c = feeDetails?.feeConstruct?.payableLater;
        feeDetailObj.HDFC_DASH_Fee_Construct_totFeeExclTax__c = feeDetails?.feeConstruct?.totFeeExclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_totFeeInclTax__c = feeDetails?.feeConstruct?.totFeeInclTax;
        feeDetailObj.HDFC_DASH_Fee_Construct_feeType__c = feeDetails?.feeConstruct?.feeType;
        feeDetailObj.HDFC_DASH_Fee_Construct_Detail_Type__c = HDFC_DASH_CONSTANTS.SPOT_OFFER;
        feeDetailObj.HDFC_DASH_TF_Fee_Construct__c  = feeDetails?.feeConstruct?.feeConstructTechnicalFailure==null?false:feeDetails?.feeConstruct?.feeConstructTechnicalFailure;
        Database.insert(feeDetailObj);
        return feeDetailObj.Id;
    }
    /*Method:insertAppPaymentDetail
    Parameters  :Id applicationId,HDFC_DASH_ParseFeeDetails feeDetails
    Description:This method will insert Application_Payment_Detail__c record.
    */
    public static void  insertAppPaymentDetail(Id applicationId,HDFC_DASH_ParseFeeDetails feeDetails,Id feeId){ 
        HDFC_DASH_Application_Payment_Details__c appPayDetail = new HDFC_DASH_Application_Payment_Details__c();
        appPayDetail.HDFC_DASH_Application__c = applicationId;
        appPayDetail.HDFC_DASH_Mode_Of_Payment__c=feeDetails?.applicationPaymentDetails?.modeOfPayment;  
        appPayDetail.HDFC_DASH_MerchantId__c = feeDetails?.applicationPaymentDetails?.merchantId;
        appPayDetail.HDFC_DASH_CustomerId__c = feeDetails?.applicationPaymentDetails?.customerId;
        appPayDetail.HDFC_DASH_TxnReferenceNo__c = feeDetails?.applicationPaymentDetails?.txnReferenceNo;
        appPayDetail.HDFC_DASH_BankReferenceNo__c = feeDetails?.applicationPaymentDetails?.bankReferenceNo;
        appPayDetail.HDFC_DASH_TxnAmount__c = feeDetails?.applicationPaymentDetails?.txnAmount;
        appPayDetail.HDFC_DASH_TxnDate__c = feeDetails?.applicationPaymentDetails?.txnDate;
        appPayDetail.HDFC_DASH_AuthStatus__c = feeDetails?.applicationPaymentDetails?.authStatus;
        appPayDetail.HDFC_DASH_ErrorStatus__c = feeDetails?.applicationPaymentDetails?.errorStatus;
        appPayDetail.HDFC_DASH_ErrorDescription__c = feeDetails?.applicationPaymentDetails?.errorDescription;
        appPayDetail.HDFC_DASH_Checksum__c = feeDetails?.applicationPaymentDetails?.checksum;
        appPayDetail.HDFC_DASH_SO_PT_Accepted__c  = feeDetails?.paymentTermsAccepted;
        appPayDetail.HDFC_DASH_SO_PT_Content__c = feeDetails?.paymentTermsContent;
        appPayDetail.HDFC_DASH_SO_PT_AcceptedDate__c = feeDetails?.paymentTermsAcceptedDate;
        appPayDetail.HDFC_DASH_Fee_Detail__c = feeId;
        //appPayDetail.HDFC_DASH_TF_Payment_Detail__c = feeDetails?.paymentGatewayTechnicalFailure==null?false:feeDetails?.paymentGatewayTechnicalFailure;
        Database.insert(appPayDetail);        
    }  
    /* 
    Method Name :createResponseStructure
    Parameters  :Opportunity application
    Description :This method would generate the response for fee details API.
    */
    public static HDFC_DASH_APIResponse_FeeDetails createResponseStructure(Opportunity application, HDFC_DASH_APIResponse_FeeDetails apiRes)
    {
        apiRes.successMessage = HDFC_DASH_Constants.FEEDETAILS_SUCCESSMESSAGE;
        apiRes.sfApplicationId = application?.id;
        apiRes.sfApplicationNumber = application?.HDFC_DASH_ApplicationNumber__c;
        
        return apiRes;
    }
}