/**
className: HDFC_DASH_GST_Business_Detail_Selector
DevelopedBy: Kumar Gourav
Date: 05 Oct 2021
Company: Accenture
Class Description: This class would create the select queries for GST Business Details object .
**/
public with sharing class HDFC_DASH_GST_Business_Detail_Selector {
 
		private static final string SELECT_STRING = 'Select ';
        private static final string FROM_OBJECT = ' from HDFC_DASH_GST_Business_Details__c ';
        private static final string WHERE_STRING = 'where ';
        private static final string ID_STRING = 'ID ';
        private static final string QUERY_PARAMETER = '=:queryParameters';
    

/* 
Method Name :getGstBusinessDetails
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get a list of Gst Business based on one condition.
*/   
public static List<HDFC_DASH_GST_Business_Details__c> getGstBusinessDetails(List<String> queryParameters,List<string> fieldList, String conditionOn)
    {     
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<HDFC_DASH_GST_Business_Details__c> listGstBusiness = database.query(querystring); 
        return listGstBusiness;
    }
}