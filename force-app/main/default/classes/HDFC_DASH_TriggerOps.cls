/**
* 	className: HDFC_DASH_TriggerOps
DevelopedBy: Anmol
Date: 25 May 2021
Company: Accenture
Class Description:  This interface represents an individual trigger operation that encapsulates relatively independent 
*                      business logic in a trigger context.
*     The caller is the @TriggerHandler class that does these steps:
*     Use the isEnabled() method check if the operation needs to be executed;
*     Call the filter() method to return the sobjects that are concerned; normally the ones with some change;
*     Execute the business logic that is implemented in the execute() method.

*/
public interface HDFC_DASH_TriggerOps{
    Boolean isEnabled();
    SObject[] filter();
    void execute(SObject[] sobs);
}