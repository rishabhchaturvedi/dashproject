/**
className: HDFC_DASH_TriggerConfigTest
DevelopedBy: Sai
Date: 26 May 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_TriggerConfig class.
* 
*/
@isTest(SeeAllData = false)
private without sharing class HDFC_DASH_TriggerConfigTest {
private static final string ASSERT_MSG = 'Testing Trigger Config';
/* Method Name: testTriggerConfig
parameters: NONE
Class Description: This method would call the HDFC_DASH_TriggerConfig class 
*/
static TestMethod void testTriggerConfig() {
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    //HDFC_DASH_TriggerConfig tc_acc = HDFC_DASH_TriggerConfig.ACCOUNT_CONFIG;
    HDFC_DASH_TriggerConfig tc_pe = HDFC_DASH_TriggerConfig.PROCESS_EXCEP_CONFIG;
    Test.startTest();
    System.runAs(sysAdmin){
    Test.stopTest();
    //System.assertNotEquals(Null,tc_acc,ASSERT_MSG);
    System.assertNotEquals(Null,tc_pe,ASSERT_MSG);
	}
}
}