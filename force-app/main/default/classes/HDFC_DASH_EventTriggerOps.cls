/**
className: HDFC_DASH_EventTriggerOps
DevelopedBy: Tejeswari
Date: 20 January 2022
Company: Accenture 
Class Description: This Class contains the trigger operation on Event object.
**/
public class HDFC_DASH_EventTriggerOps {
    
    /**
className: PopulateSubjectNameField_Insert
DevelopedBy: Tejeswari
Date: 20 January 2022
Company: Accenture 
Class Description: This Class update the SubjectName field of Event for insert trigger.
*/
    public with sharing class PopulateSubjectNameField_Insert implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateSubNameInsert_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Event> listEventRecs = HDFC_DASH_EventTriggerOpsHelper.filterEventsWithRelatedFields_Insert();
            return listEventRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfEvents){
            HDFC_DASH_EventTriggerOpsHelper.populateSubjectNameField(listOfEvents); 
            HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateSubNameInsert_FirstRun = false;
            
        }  
    }
    /**
className: PopulateSubjectNameField_Update
DevelopedBy: Tejeswari
Date: 20 January 2022
Company: Accenture 
Class Description: This Class update the SubjectName field of Event for update trigger.
*/
    public with sharing class PopulateSubjectNameField_Update implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateSubNameUpdate_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Event> listEventRecs = HDFC_DASH_EventTriggerOpsHelper.filterEventsWithRelatedFields_Update();
            return listEventRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfEvents){
            HDFC_DASH_EventTriggerOpsHelper.populateSubjectNameField(listOfEvents); 
            HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateSubNameUpdate_FirstRun = false;
            
        }  
    }
    
    
    /**
className: PopulateActorExeCode_Insert
DevelopedBy: Tejeswari
Date: 25 January 2022
Company: Accenture 
Class Description: This Class update the Actor ExecutiveCode Text field of Event for insert trigger.
*/
    public with sharing class PopulateActorExeCode_Insert implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateActorExeCodeInsert_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            return Trigger.new;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfEvents){
            HDFC_DASH_EventTriggerOpsHelper.populateActorExeCode(listOfEvents); 
            HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateActorExeCodeInsert_FirstRun = false;
            
        }  
    }
    
    /**
className: PopulateActorExeCode_Update
DevelopedBy: Tejeswari
Date: 25 January 2022
Company: Accenture 
Class Description: This Class update the Actor ExecutiveCode Text field of Event for Update trigger.
*/
    public with sharing class PopulateActorExeCode_Update implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateActorExeCodeUpdate_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Event> listEventRecs = HDFC_DASH_EventTriggerOpsHelper.filterEventsWithActor();
            return listEventRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfEvents){
            HDFC_DASH_EventTriggerOpsHelper.populateActorExeCode(listOfEvents); 
            HDFC_DASH_TriggerOpsRecursionFlags.Event_PopulateActorExeCodeUpdate_FirstRun = false;
            
        }  
    }
}