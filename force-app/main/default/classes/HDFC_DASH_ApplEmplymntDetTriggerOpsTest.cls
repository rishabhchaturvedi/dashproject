/**
className: HDFC_DASH_ApplEmplymntDetTriggerOpsTest
DevelopedBy: Sai Suman 
Date: 07 October 2021
Company: Accenture
Class Description: test class for HDFC_DASH_ApplEmplymntDetTriggerOps.
**/
@isTest (SeeAllData = False)
public class HDFC_DASH_ApplEmplymntDetTriggerOpsTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id); 
    private static HDFC_DASH_Applicant_Details__c appDetails = 
    HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c empDetails =
    HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.id,null);
     private static List<HDFC_DASH_Applicant_Employment_Details__c> empDetailsList =
    HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmpDetailList(3,appDetails.id,con.id);
    private static final string NAT_OF_EMP ='E';

   
    /* 
Method Name :testupdateEmpDetRec
Description :This method would test the trigger for update Employment details records.
*/  
    @IsTest public static void testupdateEmpDetRec(){

            
        Exception testException ;
        System.runAs(sysAdmin)
        {
            test.startTest();
     
            try
            {  
                List<HDFC_DASH_Applicant_Employment_Details__c> empDetList = new List<HDFC_DASH_Applicant_Employment_Details__c>(); 
                for(HDFC_DASH_Applicant_Employment_Details__c empDet :empDetailsList){
                    empDet.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_YES;
                    empDetList.add(empDet);
                }
                update empDetList;
                empDetails.HDFC_DASH_Contact__c = con.Id;
                update empDetails;
                
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            HDFC_DASH_Applicant_Employment_Details__c empDetailRec = [select id,HDFC_DASH_Latest__c from HDFC_DASH_Applicant_Employment_Details__c where id =:empDetails.id];
            system.assertEquals(HDFC_DASH_Constants.STRING_YES,empDetailRec.HDFC_DASH_Latest__c);
            system.assertEquals(null, testException);
        }
    }
    /* 
Method Name :testForException
Description :This method would test the trigger for exception.
*/
    @isTest public static void testForException()
    {
        Exception testException ;
        System.runAs(sysAdmin)
        {
            try{
                HDFC_DASH_Applicant_Employment_Details__c excep = new HDFC_DASH_Applicant_Employment_Details__c();
                excep.HDFC_DASH_Contact__c = con.id;
                
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_NULLPOINTER_EXP);
                database.insert(excep);
            }
            catch (Exception e) { 
                testException=e;
            }
            system.assertNotEquals(null, testException);
        }
    }
    /* 
Method Name :testFilterEDforNatOfEmpScenario
Description :This method would test the method in trigger helper for the list of Emp Details for which have the Nature of employee field value.
*/
    @isTest public static void testFilterEDforNatOfEmpScenario() {
        Exception testException = null;
        System.runAs(sysAdmin){
            HDFC_DASH_Applicant_Employment_Details__c ObjEmpDetrec= HDFC_DASH_TestDataFactory_ApplEmpDetail.createBasicApplEmploymentDetail(appDetails.id,con.id);
            test.startTest();
            ObjEmpDetrec.HDFC_DASH_Nat_Of_Emp__c = NAT_OF_EMP;
            try{
                Database.insert(ObjEmpDetrec);   
            }
            catch(Exception ex){
                testException = ex;
            }
            test.stopTest();
            HDFC_DASH_Applicant_Details__c objApplDet = [Select id, HDFC_DASH_Nat_Of_Emp__c from HDFC_DASH_Applicant_Details__c where id =: appDetails.id];
            system.assertEquals(NAT_OF_EMP, objApplDet.HDFC_DASH_Nat_Of_Emp__c);
            system.assertEquals(null, testException);
        }
    }
}