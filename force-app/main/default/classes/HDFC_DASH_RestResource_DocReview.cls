@RestResource(urlMapping='/hdfc/documentreview/V1.0/*')
/*
Author: Kumar Gourav
Class Name:HDFC_DASH_RestResource_DocReview
Date: 27 Oct 2021
Company: Accenture
Class Description: This class is to implement the Document review API
*/
global with sharing class HDFC_DASH_RestResource_DocReview {
    
    /*
    Method:getDocReview()
    Description: This method will get list of documents
    */
    @HttpGet
    global static void getDocReview() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders  = new Map<string,String>();
        String response;
        Exception exp;
        HDFC_DASH_APIResponse_DocReview apiResponse = new HDFC_DASH_APIResponse_DocReview();  
        try
        { 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_DocReview.getDocReview(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                                            HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                                            + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_GETDOCREVIEW, HDFC_DASH_Constants.HANDLE_GETDOCREVIEW_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_GET, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_GETDOCREVIEW, request.requestBody.tostring(), (res.responseBody)?.tostring(), 
                                                                HDFC_DASH_Constants.METHOD_TYPE_GET, HDFC_DASH_Constants.HANDLE_GETDOCREVIEW_REQUEST, exp, request.headers);        

            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                        res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
}