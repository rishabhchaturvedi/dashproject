/**
className: processResponseHandler
DevelopedBy: Sai Shruthi
Date: 06 Aug 2021
Company: Accenture
Class Description: This class will retry the Spot Offer ILPS Callout
**/
public class HDFC_DASH_Process_SpotOfferILPS_Response extends processResponseHandler 
{    
    /*
Method: doRetryProcess
Description: This method will retry the Spot Offer ILPS Callout
*/
    public override void doRetryProcess(HttpResponse response, String processDataStr)
    {
        try
        {
            HDFC_DASH_APIResponse_FeeDetails apiRes = new HDFC_DASH_APIResponse_FeeDetails();
            processResponse(response, apiRes, processDataStr);    
        }
        catch(Exception e)
        {     
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            system.debug('expDetails => ' + expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_SO_ILPSCALLOUT, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }
    
    /*
Method: processResponse
Description: This method will process the response body
*/
    public HDFC_DASH_APIResponse_FeeDetails processResponse(HttpResponse response, HDFC_DASH_APIResponse_FeeDetails apiResponse_Fee, String processDataStr)
    {    
        Map<String, object> mapProcessData = (Map<String, object>)JSON.deserializeUntyped(processDataStr);    
            
        List<HDFC_DASH_APIResponse_FeeDetails.CustomerDetails> listCustDetails_Fee = new List<HDFC_DASH_APIResponse_FeeDetails.CustomerDetails>();
        List<HDFC_DASH_Applicant_Details__c> listUpdateAppDetail = new List<HDFC_DASH_Applicant_Details__c>();
        List<Opportunity> listUpdateApp=new List<Opportunity>();
        List<string> listSfCustNo = new List<string>();
        // List<string> listFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Contact_Number, HDFC_DASH_Constants.HDFC_DASH_ILPS_Cust_Number, HDFC_DASH_Constants.HDFC_DASH_ILPS_Unique_Cust_Number};        
        List<string> listFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ILPS_Customer_Number};  
            
            if(response != null && response.getStatusCode() == HDFC_DASH_Constants.INT_TWO_HUNDRED)
        {
            system.debug('response.getBody() => ' + response.getBody());
            HDFC_DASH_ParseResponseSpotOfferFileILPS result = HDFC_DASH_ParseResponseSpotOfferFileILPS.parse(response.getBody());
            system.debug('result => ' + result);
            system.debug('mapProcessData => ' + mapProcessData);
            
            for(HDFC_DASH_ParseResponseSpotOfferFileILPS.custDetails custDetail : result?.custDetails)
            {
                system.debug('custDetail?.sf_cust_no => ' + custDetail?.sf_cust_no);
                listSfCustNo.add(string.ValueOf(custDetail?.sf_cust_no));
                
                //Adding to Fee Details Response
                HDFC_DASH_APIResponse_FeeDetails.CustomerDetails custDetails_Fee = new HDFC_DASH_APIResponse_FeeDetails.CustomerDetails();
                custDetails_Fee.sfCustNo = custDetail?.sf_cust_no;
                custDetails_Fee.custNo = custDetail?.cust_no;
                custDetails_Fee.unqCustNo = custDetail?.unq_cust_no;
                listCustDetails_Fee.add(custDetails_Fee);
                
                //Updating the Applicant detail record
                if(mapProcessData.get(custDetail?.sf_cust_no) != null)
                {
                    String appDetailId = String.ValueOf(mapProcessData.get(custDetail?.sf_cust_no));
                    system.debug('app Id '+appDetailId);
                    HDFC_DASH_Applicant_Details__c objAppDetail = new HDFC_DASH_Applicant_Details__c();
                    objAppDetail.id = appDetailId;
                    system.debug('appDet Id '+objAppDetail.id);
                    objAppDetail.HDFC_DASH_ILPS_Customer_Number__c = custDetail?.cust_no;
                    listUpdateAppDetail.add(objAppDetail);
                }
            }
            
            List<string> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_File_Number, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber}; 
                List<String> appNo = new List<string>{result?.sf_appl_id}; 
                    
                    //Fetching the opportunity based on the application number
                    for(Opportunity application : HDFC_DASH_Application_Selector.getApplication(appNo, listOfFields_App, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber)){
                        application.HDFC_DASH_File_Number__c = result?.file_no;
                        listUpdateApp.add(application);
                        break;
                    }
            
            if(Schema.sObjectType.HDFC_DASH_Applicant_Details__c.isUpdateable()){
                Database.update(listUpdateAppDetail);   
            }
            if(Schema.sObjectType.Opportunity.isUpdateable()){
                Database.update(listUpdateApp); 
            }
            apiResponse_Fee.fileNo = result?.file_no;
            apiResponse_Fee.customerDetails = listCustDetails_Fee;
        }
        return apiResponse_Fee;
    }
}