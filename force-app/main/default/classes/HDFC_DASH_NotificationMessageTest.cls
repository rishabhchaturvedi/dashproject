/**
className: HDFC_DASH_NotificationMessageTest
DevelopedBy: Tejeswari
Date: 27 January 2022
Company: Accenture 
Class Description: Test class for  HDFC_DASH_NotificationMessage class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_NotificationMessageTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static final string MEETING_TYPE= 'In-person';
    
    /* 
Method Name :testNotificationMessageTest
Description :This method would test HDFC_DASH_NotificationMessage batch class.
*/
    static testmethod void testNotificationMessageTest(){
        Exception testException;
        System.runAs(sysAdmin) {
            try{
                Event event=HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.Id,sysAdmin.Id);
                event.HDFC_DASH_BSA__c = null;
                event.HDFC_DASH_Meeting_Type__c = MEETING_TYPE;
                Database.insert(event);
                List<Event> listEvents = [select id, HDFC_DASH_BSA__c,HDFC_DASH_Meeting_Type__c,HDFC_DASH_User_Type__c from Event where id =: event.id];
                Test.StartTest();
                HDFC_DASH_NotificationMessage.getMessageBody(listEvents);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            system.assertEquals(null, testException);
        }
    }
    
     /* 
Method Name :testExceptionForNotification
Description :This method would test Exception scenario for HDFC_DASH_NotificationMessage  class.
*/
    static testmethod void testExceptionForNotification(){
        Exception testException;
        System.runAs(sysAdmin) {
            try{
                Event event=HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.Id,sysAdmin.Id);
                event.HDFC_DASH_BSA__c = null;
                event.HDFC_DASH_Meeting_Type__c = MEETING_TYPE;
                Database.insert(event);
                List<Event> listEvents = [select id, HDFC_DASH_BSA__c,HDFC_DASH_Meeting_Type__c from Event where id =: event.id];
                Test.StartTest();
                HDFC_DASH_NotificationMessage.getMessageBody(listEvents);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            system.assertEquals(null, testException);
        }
    }
}