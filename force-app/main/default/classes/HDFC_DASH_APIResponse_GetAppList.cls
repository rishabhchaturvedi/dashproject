/*
	Author: Anisha Arumugam
	Class: HDFC_DASH_APIResponse_GetAppList
	Description: Apex Class for handling the API Response of GetApplicationList
*/
public inherited sharing class HDFC_DASH_APIResponse_GetAppList 
{
	/*sfApplications Node in Response*/
    public class SfApplications 
    {
		public String sfApplicationID;
		public String sfApplicationNumber;
		public String ilpsFileNo;
        public Decimal requiredLoanAmount;
		public String stage;
        public String loanType;
		public String status;  
        public Date applicationDate; 
        public Decimal loanAccountNo;
		public Decimal remainingTerm;
		public Date receiptDate;
		public Decimal loanEMI;
       public PropertyDetails propertyDetails;
	}
 /*
        Class: PropertyDetails
        Description: Class to get Property Details
    */
    public with sharing class PropertyDetails {
	  public String reraCode;
		public String reraName;
		public String towerOrWingName;
		public String propertyVillageTownCity;
		public String propertySubDistrict;
		public String propertyStateCode;
		public String propertyState;
		public String propertyPostOfficeName;
		public String propertyPostalCode;
		public String propertyPlannedCityCode;
		public String propertyPlannedCity;
		public String propertyLocationDecided;
		public String propertyLocality;
		public String propertyLandmark;
		public String propertyHouseIdentifier;
		public String propertyDistrict;
		public String propertyCountryCode;
		public String propertyCountry;
		public String propertyCityCode;
		public String propertyCity;
    }
	public String sfCustomerId;
	public List<SfApplications> sfApplications;    
}