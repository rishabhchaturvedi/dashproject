/**
className: HDFC_DASH_InboundRestService_GetAppTest
DevelopedBy: Punam Marbate
Date: 20 Aug 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestService_GetApp
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_InboundRestService_GetAppTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                                                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Application_Payment_Details__c appPaymentDetails = 
                                                    HDFC_DASH_TestDataFactory_ApplPayDetail.getBasicApplPaymentDetail(app.id);
    private static HDFC_DASH_Fee_Detail__c feeDetail = HDFC_DASH_TestDataFactory_FeeDetails.getBasicFeeDetail(app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c appEmpDetails = 
                                                    HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.id,con.id);
    private static HDFC_DASH_Contact_Address_Details__c addressDetail=
                                                    HDFC_DASH_TestDataFactory_ContAddDetail.getBasicContAddressDetail(con.Id);
    private static HDFC_DASH_Applicant_Income_Details__c appIncomeDetails =
                HDFC_DASH_TestDataFactory_ApplIncDetail.getBasicApplIncomeDetail(appDetails.id);
    private static HDFC_DASH_Funding_Source__c fundingsource =
                HDFC_DASH_TestDataFactory_FundingSource.getBasicFundingSource(app.id);
    private static Opportunity childApp = HDFC_DASH_TestDataFactory_Application.getAdditionalLoan(acc.id);
    private static HDFC_DASH_Applicant_Bank_Detail__c appBankDetail = 
                    HDFC_DASH_TestDataFactory_BankDetails.getBasicApplBankDetail(appDetails.Id,con.id);
    private static HDFC_DASH_Applicant_Financial_Info__c applicantFinancialInfo =   
    HDFC_DASH_TestDataFactory_ApplFinanInfo.getBasicApplicantFinancialInfo(appDetails.Id);    
    private static HDFC_DASH_GST_Business_Details__c gstDetails =
                    HDFC_DASH_TestDataFactory_GSTbusinessDet.getBasicGSTDetail(appEmpDetails.Id);
    private static final String APPDET_JSON = '{"applicantdetail":{"transunionCibilId":"100015498545","transunionAckId":null,'+
    '"soFixedMonthlyIncome":75000.0,"soAdditionalncome":20000.0,"skipEmploymentDetails":"Yes",'+
    '"sfCustomerId":"10000186","sfApplicantId":"'+ appDetails.id +'","salutation":"Mr.","salarySlipAttached":"Yes",'+
    '"retirementAge":null,"residentType":"RES","relationWithPrimApplicant":null,"propOwner":"Y","panStatusFromNsdl":"Status",'+
    '"panApplied":"No","pan":"PROPW1087R","obligation":null,"noAdditionalIncome":"Yes","nMbAckId":null,"name":"Mr. Mithil P Sharma",'+
    '"mobileCountryCode":"91","mobile":"7650198234","middleName":"P","lastName":"Sharma","incomeConsidered":"Y","genderCode":"2","firstName":"Mithil","experianId":"100032566162","experianCibilTechnicalFailure":true,"experianAckId":null,"email":"mithil213@gmail.com","dateOfBirth":"1990-02-12","confidenceScoreTechnicalFailure":true,"confidenceScore":100,"cibilTransunionTechnicalFailure":false,"applicantType":"Applicant","applicantCapacity":"B","age":31}}';

    private static final string SUCCESS ='Success';
    private static final string BASEURL = URL.getOrgDomainUrl().toExternalForm();
    private static final string STRING_URL = BASEURL + '/services/apexrest/hdfc/getApplication/V1.0/?sfApplicationId='+ 
            app.id +'&leadId='+app.HDFC_DASH_LMS_Lead_ID__c;
    /* 
    Method Name : testGetApplication
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.getApplication().
    */
    static testmethod void testGetApplication(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();    
            request.requestUri = STRING_URL; 
            request.addParameter(HDFC_DASH_Constants.STRING_SF_APPLICATIONID, app.id );
            request.addParameter(HDFC_DASH_Constants.STRING_LEADID, app.HDFC_DASH_LMS_Lead_ID__c );
            HDFC_DASH_ParseApplication appRes = new HDFC_DASH_ParseApplication();
            Test.startTest();
            appRes = HDFC_DASH_InboundRestService_GetApp.getApplication(request);
            Test.stopTest();
            System.assertNotEquals(null,appRes,SUCCESS);
        } 
    }
    /* 
    Method Name : testGetApplicationFields
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.getApplicationFields().
    */
    static testmethod void testGetApplicationFields(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication applicationRes = new HDFC_DASH_ParseApplication();
            Test.startTest();
            applicationRes = HDFC_DASH_InboundRestService_GetApp.getApplicationFields(app.Id);
            Test.stopTest();
            System.assertNotEquals(null,applicationRes,SUCCESS);
        } 
    }  
    /* 
    Method Name : testGeneratePropertyDetails
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generatePropertyDetails().
    */
    static testmethod void testGeneratePropertyDetails(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication.propertyDetails propertyDetRes = 
                            new HDFC_DASH_ParseApplication.propertyDetails();
            Test.startTest();
            propertyDetRes = HDFC_DASH_InboundRestService_GetApp.generatePropertyDetails(app.Id);
            Test.stopTest();
            System.assertNotEquals(null,propertyDetRes,SUCCESS);
        } 
    }
      /* 
    Method Name : testGenerateBalanceTransferLoan
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateBalanceTransferLoan().
    */
    static testmethod void testGenerateBalanceTransferLoan(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication.BalanceTransferLoan appBTRes = 
                                new HDFC_DASH_ParseApplication.BalanceTransferLoan();
            Test.startTest();
            appBTRes = HDFC_DASH_InboundRestService_GetApp.generateBalanceTransferLoan(app);
            Test.stopTest();
            System.assertNotEquals(null,appBTRes,SUCCESS);
        } 
    }
              /* 
    Method Name : testGenerateAdditionalLoan
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateAdditionalLoan().
    */
    static testmethod void testGenerateAdditionalLoan(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            List<HDFC_DASH_ParseApplication.AdditionalLoan> listAddlLoan = 
                                new List<HDFC_DASH_ParseApplication.AdditionalLoan>();
            Test.startTest();
            listAddlLoan = HDFC_DASH_InboundRestService_GetApp.generateAdditionalLoan(childApp.id);
            Test.stopTest();
            System.assertNotEquals(null,listAddlLoan,SUCCESS);
        } 
    }
    
        /* 
    Method Name : testGenerateFundingSource
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateFundingSource().
    */
    static testmethod void testGenerateFundingSource(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            List<HDFC_DASH_ParseApplication.FundingSource> listAppFundSource = new List<HDFC_DASH_ParseApplication.FundingSource>();
            Test.startTest();
            listAppFundSource = HDFC_DASH_InboundRestService_GetApp.generateFundingSource(app.id);
            Test.stopTest();
            System.assertNotEquals(null,listAppFundSource,SUCCESS);
        } 
    }
    
    /* 
    Method Name : testGenerateEligibilityAmount
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateEligibilityAmount().
    */
    static testmethod void testGenerateEligibilityAmount(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication.EligibilityAmount appEARes = 
                                new HDFC_DASH_ParseApplication.EligibilityAmount();
            Test.startTest();
            appEARes = HDFC_DASH_InboundRestService_GetApp.generateEligibilityAmount(app);
            Test.stopTest();
            System.assertNotEquals(null,appEARes,SUCCESS);
        } 
    }
    /* 
    Method Name : testGenerateCustomerSelectedSO
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateCustomerSelectedSO().
    */
    static testmethod void testGenerateCustomerSelectedSO(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id;
            HDFC_DASH_ParseApplication.CustomerSelectedSpotOffer appCSRes = 
                                new HDFC_DASH_ParseApplication.CustomerSelectedSpotOffer();
            Test.startTest();
            appCSRes = HDFC_DASH_InboundRestService_GetApp.generateCustomerSelectedSO(app);
            Test.stopTest();
            System.assertNotEquals(null,appCSRes,SUCCESS);
        } 
    }
    /* 
    Method Name : testGenerateBre
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateBre().
    */
    static testmethod void testGenerateBre(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication.Bre appBreRes = new HDFC_DASH_ParseApplication.Bre();
            Test.startTest();
             appBreRes = HDFC_DASH_InboundRestService_GetApp.generateBre(app);
            Test.stopTest();
            System.assertNotEquals(null,appBreRes,SUCCESS);
        } 
    }
    /* 
    Method Name : testGeneratefeeConstruct
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generatefeeConstruct().
    */
    static testmethod void testGeneratefeeConstruct(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            List<HDFC_DASH_ParseApplication.Fee> listAppFeeRes = new List<HDFC_DASH_ParseApplication.Fee>();
            Test.startTest();
            listAppFeeRes = HDFC_DASH_InboundRestService_GetApp.generatefeeConstruct(app.id);
            Test.stopTest();
            System.assertNotEquals(null,listAppFeeRes,SUCCESS);
        } 
    }
    /* 
    Method Name : testGenerateAppPaymentDetails
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateAppPaymentDetails().
    */
    static testmethod void testGenerateAppPaymentDetails(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            List<HDFC_DASH_ParseApplication.ApplicationPaymentDetails> listPaymentDetRes = 
                            new List<HDFC_DASH_ParseApplication.ApplicationPaymentDetails>();
            Test.startTest();
            listPaymentDetRes = HDFC_DASH_InboundRestService_GetApp.generateAppPaymentDetails(app.Id);
            Test.stopTest();
            System.assertNotEquals(null,listPaymentDetRes,SUCCESS);
        } 
    }
    /* 
    Method Name : testGetAppDetails
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.getAppDetails().
    */
    static testmethod void testGetAppDetails(){   
        System.runAs(sysAdmin){
            appDetails.HDFC_DASH_Current_Address__c = addressDetail.id;
            appDetails.HDFC_DASH_Permanent_Address__c = addressDetail.id;
            update(appDetails);
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            List<HDFC_DASH_ParseApplication.ApplicantInfo> listAppDetailRes = 
                                new List<HDFC_DASH_ParseApplication.ApplicantInfo>();
            Test.startTest();
            listAppDetailRes = HDFC_DASH_InboundRestService_GetApp.getAppDetails(app.Id);
            Test.stopTest();
            System.assertNotEquals(null,listAppDetailRes,SUCCESS);
        } 
    }
       /* 
    Method Name : testGenerateApplEmpIncomeDetail
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_GetApp.generateApplAddressEmpDetail().
    */
    static testmethod void testGenerateApplEmpIncomeDetail(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id;
            List<Id> appDetailIds = new List<Id>{appDetails.Id};
			HDFC_DASH_ParseApplication.ApplicantInfo appDetNode = 
                            (HDFC_DASH_ParseApplication.ApplicantInfo)System.JSON.deserialize(APPDET_JSON,
                            HDFC_DASH_ParseApplication.ApplicantInfo.class);
    		List<HDFC_DASH_ParseApplication.ApplicantInfo> listAppDetNode =  
                            new List<HDFC_DASH_ParseApplication.ApplicantInfo>{appDetNode};	
		
            List<HDFC_DASH_ParseApplication.ApplicantInfo> listAppDetailRes = 
                                new List<HDFC_DASH_ParseApplication.ApplicantInfo>();
            Test.startTest();
            listAppDetailRes = HDFC_DASH_InboundRestService_GetApp.generateApplEmpIncomeDetail(appDetailIds,listAppDetNode);
            Test.stopTest();
            System.assertNotEquals(null,listAppDetailRes,SUCCESS);
        } 
    }
    /* 
    Method Name :   testgenerateConsent
    Parameters  :   None
    Description :   This method would generate the Consent Node .
    */
    static testmethod void testgenerateConsent(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication.Consent appDetailRes = 
                                new HDFC_DASH_ParseApplication.Consent();
            Test.startTest();
            appDetailRes = HDFC_DASH_InboundRestService_GetApp.generateConsent(appDetails);
            Test.stopTest();
            System.assertNotEquals(null,appDetailRes,SUCCESS);
        } 
    }
    /* 
    Method Name :   testgenerateResLocation
    Parameters  :   None
    Description :   This method would generate the Residence Location Node .
    */
    static testmethod void testgenerateResLocation(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication.ResidenceLocation appDetailRes = 
                                new HDFC_DASH_ParseApplication.ResidenceLocation();
            Test.startTest();
            appDetailRes = HDFC_DASH_InboundRestService_GetApp.generateResLocation(appDetails);
            Test.stopTest();
            System.assertNotEquals(null,appDetailRes,SUCCESS);
        } 
    }
    /* 
    Method Name :   testgenerateRiskSCore
    Parameters  :   None
    Description :   This method would generate the Risk Score Node .
    */
    static testmethod void testgenerateRiskSCore(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            HDFC_DASH_ParseApplication.RiskScore appDetailRes = 
                                new HDFC_DASH_ParseApplication.RiskScore();
            Test.startTest();
            appDetailRes = HDFC_DASH_InboundRestService_GetApp.generateRiskSCore(appDetails);
            Test.stopTest();
            System.assertNotEquals(null,appDetailRes,SUCCESS);
        } 
    }
    /* 
    Method Name :   testCallEmpIncomeDetails
    Parameters  :   none
    Description :   This method would call the getAddDetailsNode() and getEmpDetailsNode() methods.
    */
    static testmethod void testCallEmpIncomeDetails(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            List<HDFC_DASH_ParseApplication.ApplicantInfo> listAppDetailRes = NULL;
            
            HDFC_DASH_ParseApplication.ApplicantInfo appDetNode = 
                            (HDFC_DASH_ParseApplication.ApplicantInfo)System.JSON.deserialize(APPDET_JSON,
                            HDFC_DASH_ParseApplication.ApplicantInfo.class);
            List<HDFC_DASH_ParseApplication.ApplicantInfo> listAppDetNode =  
                                        new List<HDFC_DASH_ParseApplication.ApplicantInfo>{appDetNode};
            List<HDFC_DASH_Applicant_Employment_Details__c> listEmpDetails = 
                                        new List<HDFC_DASH_Applicant_Employment_Details__c>{appEmpDetails};
            List<HDFC_DASH_Applicant_Income_Details__c> listIncomeDetails = 
                                        new List<HDFC_DASH_Applicant_Income_Details__c>{appIncomeDetails};
            List<HDFC_DASH_Applicant_Bank_Detail__c> listBankDetails = 
                                        new List<HDFC_DASH_Applicant_Bank_Detail__c>();
            List<HDFC_DASH_Applicant_Financial_Info__c> listApplicantFinancialInfo = new List<HDFC_DASH_Applicant_Financial_Info__c>();
            List<Note> listRemarks = new List<Note>();
            Test.startTest();
            listAppDetailRes = HDFC_DASH_InboundRestService_GetApp.callEmpIncomeDetails(listEmpDetails,listIncomeDetails,listBankDetails,listApplicantFinancialInfo,listRemarks,listAppDetNode);
            Test.stopTest();
            System.assertNotEquals(null,listAppDetailRes,SUCCESS); 
        } 
    }
    /* 
    Method Name :   testgetEmpDetailsNode
    Parameters  :   None
    Description :   This method would generate the applicant employment details for applicants employment details in listEmpDetails.
    */
    static testmethod void testgetEmpDetailsNode(){   
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS(); 
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL + app.id; 
            List<HDFC_DASH_ParseApplication.ApplicantEmploymentDetails> listEmpDetailRes = 
                                new List<HDFC_DASH_ParseApplication.ApplicantEmploymentDetails>();
            List<HDFC_DASH_Applicant_Employment_Details__c> listEmpDetails = 
                                new List<HDFC_DASH_Applicant_Employment_Details__c>{appEmpDetails};
            
            Test.startTest();
                listEmpDetailRes = HDFC_DASH_InboundRestService_GetApp.getEmpDetailsNode(listEmpDetails);
            Test.stopTest();
            System.assertNotEquals(null,listEmpDetailRes,SUCCESS);
        } 
    }
    

}