/* ================================================
 * @Class Name :  schedule_IT_Daily_Failed_Logs_Test
 * @author : Accenture
 * @Purpose: This class is used to test the functionality of class schedule_IT_Daily_Failed_Logs
 * @created date:
 * @Last modified date:03/04/2016
 * @Last modified by : Accenture
 ================================================*/
@istest(SeeAllData = false)
public with sharing class schedule_IT_Daily_Failed_Logs_Test{
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    /***************************************************************************************************************
    *   @Name        :  testMethodFirst                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  To log failed batch details   
    ***************************************************************************************************************/
    public static testMethod void testMethodFirst(){
        System.runAs(sysAdmin){
        schedule_IT_Daily_Failed_Logs sh1 = new schedule_IT_Daily_Failed_Logs();
        String sch = '0 0 23 * * ?'; 
        test.starttest();
        system.schedule('Interface Daily Failed Logs', sch, sh1); 
        Test.stopTest(); 
    }
  }
}