/**
* 	className: HDFC_DASH_InboundRestService_DedupeCheck
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class would check dedupe logic.
**/
public inherited sharing class HDFC_DASH_InboundRestService_DedupeCheck { 
    
    //variables used throught the class
    private static Boolean isCustomer =HDFC_DASH_Constants.BOOLEAN_FALSE;
    private static Boolean isRegisteredCustomer =HDFC_DASH_Constants.BOOLEAN_FALSE; 
    private static HDFC_DASH_APIResponse_DedupeCheck apiResponse = new HDFC_DASH_APIResponse_DedupeCheck();
    
    /* 
Method Name :processDedupeCheck
Parameters  :RestRequest request
Description :This method return the response for the dedupe request from HDFC_DASH_RestResource_DedupeCheck.
*/
    public static HDFC_DASH_APIResponse_DedupeCheck processDedupeCheck(RestRequest request){ 
        //Calling Parser Class
        HDFC_DASH_ParseleadDetails leadDetails = HDFC_DASH_ParseleadDetails.parse(request);	
      // String onlyCustomerDedupe = request.params.get(HDFC_DASH_Constants.STRING_ONLY_CUSTOMER_DEDUPE);
       boolean onlyCustomerDedupe;
        if(request.params.get(HDFC_DASH_Constants.STRING_ONLY_CUSTOMER_DEDUPE) !=NULL){
       		onlyCustomerDedupe= Boolean.valueOf(request.params.get(HDFC_DASH_Constants.STRING_ONLY_CUSTOMER_DEDUPE));   
        }
        Contact contactspassingDedupeCheck =  runSalesforceDedupe(leadDetails);
        //system.debug('inside else block===='+onlyCustomerDedupe);
        if(contactspassingDedupeCheck!= NULL){
            //system.debug('contactspassingDedupeCheck'+contactspassingDedupeCheck);
            checkForRegFlag(contactspassingDedupeCheck);
            
           // HDFC_DASH_UtilityClass.storeValidationOutcome(request,contactspassingDedupeCheck.id);
        }else{
            //call LMS 
            if(onlyCustomerDedupe ==NULL || onlyCustomerDedupe!= HDFC_DASH_Constants.BOOLEAN_TRUE ){ 
            HDFC_DASH_APIRequest_LMS lmsApiRequest = new HDFC_DASH_APIRequest_LMS();
            lmsApiRequest = setLMSRequestBody(leadDetails,lmsApiRequest);
            HDFC_DASH_APIResponse_LMS lmsRes = HDFC_DASH_UtilityClass.callLMS(lmsApiRequest);
            //system.debug('lmsRes===='+lmsRes);
           Lead existingOpenLead=  checkForOpenLeadInDatabase(lmsRes?.lead_no);
            Id convAppDetId = NULL;
            if(existingOpenLead != NULL){
               existingOpenLead = upsertLeadRecord(leadDetails,lmsRes,existingOpenLead.id);
                convAppDetId= convertLeadCreateRes(leadDetails,existingOpenLead);
            }else{
                
                Lead newLead = upsertLeadRecord(leadDetails,lmsRes,null);
                //system.debug('new lead created Lead'+newLead);
                //convert Lead
                convAppDetId = convertLeadCreateRes(leadDetails,newLead);
                 
            }
            
               HDFC_DASH_UtilityClass.storeValidationOutcome(request,convAppDetId);
            }else{
                //system.debug('inside else block===='+onlyCustomerDedupe);
                Contact conRec = insertPersonAccount(leadDetails);
                createResponseStructure(conRec,NULL,NULL,NULL);
            }
    } 
          return apiResponse;
    }
    /* 
Method Name :setLMSRequest
Parameters  :HDFC_DASH_ParseleadDetails leadDetails,HDFC_DASH_APIRequest_LMS  LMSAPIRequest
Description :This method generate the request parameters for LMS.
*/
    private static HDFC_DASH_APIRequest_LMS setLmsRequestBody(HDFC_DASH_ParseleadDetails leadDetails,HDFC_DASH_APIRequest_LMS  lmsApiRequest){
        lmsApiRequest.ref_id = leadDetails?.lead?.outSystemsID;
        lmsApiRequest.ref_type = HDFC_DASH_Constants.STRING_HDFC_DASH_SPOTOFFER;
        lmsApiRequest.lead_title = leadDetails?.lead?.salutation; 
        lmsApiRequest.first_name = leadDetails?.lead?.firstName; 
        lmsApiRequest.middle_name = leadDetails?.lead?.middleName;
        lmsApiRequest.last_name = leadDetails?.lead?.lastName;
        lmsApiRequest.isd_code = leadDetails?.lead?.mobileCountryCode;
        if(String.isNotBlank(leadDetails?.lead?.mobile)){
            lmsApiRequest.mobile_no = Long.valueof(leadDetails?.lead?.mobile); 
        }
        lmsApiRequest.email_id = leadDetails?.lead?.email;
        lmsApiRequest.dob = changeDateToString(leadDetails?.lead?.dateOfBirth);
        lmsApiRequest.pan_no = leadDetails?.lead?.pan;
        lmsApiRequest.resident_type = leadDetails?.lead?.residentType;
        
        if(String.isNotBlank(leadDetails?.lead?.cityCode) && String.isNotBlank(leadDetails?.lead?.countryCode)){
            Sobject cityCountry = new Lead();
            cityCountry.put(HDFC_DASH_CONSTANTS.HDFC_DASH_City_Code,leadDetails.lead.cityCode);
            cityCountry.put(HDFC_DASH_CONSTANTS.HDFC_DASH_Country_Code,leadDetails.lead.countryCode);
            cityCountry = HDFC_DASH_UtilityClass.generateMasterTabelFields(cityCountry);
            lmsApiRequest.city_name = (String)cityCountry.get(HDFC_DASH_CONSTANTS.HDFC_DASH_City);
            lmsApiRequest.country_name = (String)cityCountry.get(HDFC_DASH_CONSTANTS.HDFC_DASH_Country);
        }else{
            lmsApiRequest.city_name = leadDetails?.lead?.cityName;
            lmsApiRequest.country_name = leadDetails?.lead?.countryName; 
        }
        lmsApiRequest.lead_source = HDFC_DASH_Constants.STRING_INTERNET;  
        lmsApiRequest.lead_sub_source = HDFC_DASH_Constants.STRING_HDFC_DIGITAL;
        lmsApiRequest.lead_term_Source = HDFC_DASH_Constants.STRING_HDFCDASHSPOTOFFER;
        lmsApiRequest.loan_product = HDFC_DASH_Constants.STRING_LOAN;
        lmsApiRequest.voice_consent_flag= leadDetails?.lead?.consent?.voiceConsent; 
        lmsApiRequest.Voice_consent_dt= changeDateToString(leadDetails?.lead?.consent?.voiceConsentDate);
        lmsApiRequest.sms_consent_flag = leadDetails?.lead?.consent?.smsConsent; 
        lmsApiRequest.sms_consent_dt = changeDateToString(leadDetails?.lead?.consent?.smsConsentDate);  
        lmsApiRequest.whatsapp_consent_flag = leadDetails.lead.consent.whatsAppConsent;
        lmsApiRequest.whatsapp_consent_dt= changeDateToString(leadDetails?.lead?.consent?.whatsAppConsentDate);
        lmsApiRequest.consent_content = leadDetails?.lead?.consent?.consentContent;
        //system.debug('lmsApiRequest----'+lmsApiRequest);
        return lmsApiRequest;
    }
    /*  
Method Name :checkForLeadInDatabase
Parameters  :String lmsLeadId
Description :This method would check if an open lead is already present in salesforce.
*/
    private static Lead checkForOpenLeadInDatabase(String lmsLeadId){
        Lead existingOpenLead = new Lead();
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID,HDFC_DASH_Constants.STRING_contact,
            HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_Status,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_Type,
            HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id,
            HDFC_DASH_Constants.STRING_STATUS};
                Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+lmsLeadId+HDFC_DASH_Constants.STRING_QUOTE,
                    HDFC_DASH_Constants.STRING_AND +HDFC_DASH_Constants.STRING_STATUS => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_NEW+HDFC_DASH_Constants.STRING_QUOTE};
                        List<Lead> existingOpenLeads =   HDFC_DASH_Lead_Selector.getLeadsBasedOnQuery(fieldList, condition);
        if(existingOpenLeads.size()>HDFC_DASH_Constants.INT_ZERO){
            existingOpenLead = existingOpenLeads[HDFC_DASH_Constants.INT_ZERO];
        }else{
            existingOpenLead = NULL;
        }
        
        return existingOpenLead;   
    }
    
    
    
    
    /* 
Method Name :createResponseStructure
Parameters  :Contact conRec,Opportunity appRec,HDFC_DASH_Applicant_Details__c appDetailRec,lead leadRec
Description :This method would generate the response for dedupe.
*/
    private static void createResponseStructure(Contact conRec,Opportunity appRec,HDFC_DASH_Applicant_Details__c appDetailRec,lead leadRec){
        //system.debug('inside create response method');
        //system.debug('conRec'+conRec);
        //system.debug('appRec'+appRec); 
        //system.debug('appDetailRec'+appDetailRec);
        //system.debug('leadRec'+leadRec);
        apiResponse.isCustomer = isCustomer;
        apiResponse.isRegisteredCustomer = isRegisteredCustomer;
        apiResponse.sfCustomerId = conRec?.id;
        apiResponse.eligibleForExistingCustJourney = conRec?.HDFC_DASH_Eligble_For_Exstng_Cust_Journy__c;
        //apiResponse.sfCustomerNumber = conRec?.HDFC_DASH_Contact_Number__c;
        //system.debug('Customer number--->'+conRec?.Account.HDFC_DASH_Customer_Number__c);
        apiResponse.sfCustomerNumber = conRec?.Account.HDFC_DASH_Customer_Number__c;
        apiResponse.customerEmail = conRec?.Email;
        apiResponse.customerMobile = conRec?.MobilePhone;
        apiResponse.sfApplicantID =  appDetailRec?.Id;
        
        if(appRec !=NULL){
            HDFC_DASH_APIResponse_DedupeCheck.Application applicationResponse =new HDFC_DASH_APIResponse_DedupeCheck.Application();
            applicationResponse.sfApplicationId = appRec?.id;
            applicationResponse.sfApplicationNumber = appRec?.HDFC_DASH_ApplicationNumber__c;
            apiResponse.application = applicationResponse; 
            
        }
        
        
        if(leadRec !=NULL){ 
            HDFC_DASH_APIResponse_DedupeCheck.lmsDetails lmsResponse =new HDFC_DASH_APIResponse_DedupeCheck.lmsDetails();
            lmsResponse.lmsLeadID = leadRec?.HDFC_DASH_LMS_Lead_ID__c;
            lmsResponse.lmsLeadStatus = leadRec?.HDFC_DASH_LMS_Lead_Status__c;
            lmsResponse.lmsLeadType = leadRec?.HDFC_DASH_LMS_Lead_Type__c;
            lmsResponse.lmsOriginBranch = leadRec?.HDFC_DASH_LMS_Origin_Branch__c;
            lmsResponse.lmsOriginBranchId = leadRec?.HDFC_DASH_LMS_Origin_Branch_Id__c;
            lmsResponse.lmsLeadDate = leadRec?.HDFC_DASH_LMSLeadDate__c;
            lmsResponse.lmsLeadSource = leadRec?.HDFC_DASH_LMSLeadSource__c;
            lmsResponse.lmsLeadSubSource = leadRec?.HDFC_DASH_LMSLeadSubSource__c;
            lmsResponse.lmsLeadTermSource = leadRec?.HDFC_DASH_LMSLeadTermSource__c;
            lmsResponse.lmsLeadDistribution = leadRec?.HDFC_DASH_LMSLeadDistribution__c;
            lmsResponse.lmsLeadOwner = leadRec?.HDFC_DASH_LMSLeadOwner__c;
            apiResponse.lmsDetails = lmsResponse;
            
        }
        
    } 
    /* 
Method Name :createLeadRecord
Parameters  :HDFC_DASH_ParseleadDetails leadDetails,HDFC_DASH_APIResponse_LMS lmsRes
Description :This method would create a lead record based on the request.
*/
    private static Lead  upsertLeadRecord(HDFC_DASH_ParseleadDetails leadDetails,HDFC_DASH_APIResponse_LMS lmsRes,Id leadid){
        //system.debug('inside creating new lead rec');
        Lead leadRec = New Lead();
        Sobject aggregateLeadRec;
        if(leadid!=NULL){
        	leadRec.id = leadid;
        }
        leadRec.HDFC_DASH_LMS_Lead_ID__c = lmsRes?.lead_no;
        leadRec.HDFC_DASH_LMS_Origin_Branch__c = lmsRes?.lead_Details?.lead_branch;
        leadRec.HDFC_DASH_LMS_Origin_Branch_Id__c = lmsRes?.lead_Details?.lead_branch_cd; 
        leadRec.HDFC_DASH_LMS_Lead_Status__c = lmsRes?.lead_Details?.lead_status; 
        leadRec.HDFC_DASH_LMS_Lead_Type__c = lmsRes?.lead_Details?.lead_type; 
        leadRec.Email = leadDetails?.lead?.email;
        leadRec.MobilePhone = leadDetails?.lead?.mobile;
        leadRec.HDFC_DASH_LMSLeadDate__c = lmsRes?.lead_Details?.lead_dt;
        leadRec.HDFC_DASH_LMSLeadSource__c = lmsRes?.lead_Details?.source;
        leadRec.HDFC_DASH_LMSLeadSubSource__c = lmsRes?.lead_Details?.sub_source;
        leadRec.HDFC_DASH_LMSLeadTermSource__c = lmsRes?.lead_Details?.term_source;
        leadRec.HDFC_DASH_LMSLeadDistribution__c = lmsRes?.lead_Details?.lead_distribution;
        leadRec.HDFC_DASH_LMSLeadOwner__c = lmsRes?.lead_Details?.lead_owner;
        
        aggregateLeadRec = generateCommonFields(leadDetails,(Sobject)leadRec);  
        upsert aggregateLeadRec;
        //system.debug('aggregateLeadRec==='+aggregateLeadRec);
        return  (Lead)aggregateLeadRec;
    }
    /* 
Method Name :checkForRegFlag
Parameters  :Contact conRec
Description :This method check for the is registered and is contact flags.
*/
    private static void checkForRegFlag(Contact conRec){
        
        if(conRec.HDFC_DASH_Is_Registered_Customer__c){
            isRegisteredCustomer =HDFC_DASH_Constants.BOOLEAN_TRUE;
            isCustomer =HDFC_DASH_Constants.BOOLEAN_TRUE;
            //system.debug('is registered cust true');
            createResponseStructure(conRec,NULL,NULL,NULL);
        }else{
            isCustomer =HDFC_DASH_Constants.BOOLEAN_TRUE;
            //system.debug('is registered cust not true');
            createResponseStructure(conRec,NULL,NULL,NULL);
        }
        
        
    }
    
    /* 
Method Name :convertLeadCreateRes
Parameters  :HDFC_DASH_ParseleadDetails leadDetails,lead newLead
Description :This method would convert lead into person account and application.
*/
    public static Id convertLeadCreateRes(HDFC_DASH_ParseleadDetails leadDetails,lead newLead){
        HDFC_DASH_Applicant_Details__c appDetails;
        
        Database.LeadConvert leadConv = new database.LeadConvert();
        leadConv.setLeadId(newLead.Id);
        List<String> fieldListLeadConStatus = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_MasterLabel};
            Map<String,String> conditionLeadConStatus= new Map<string,String>{HDFC_DASH_Constants.STRING_IsConverted => HDFC_DASH_Constants.STRING_EQUALTO+ true};
                
                LeadStatus convertStatus = HDFC_DASH_Lead_Selector.getLeadStatus(fieldListLeadConStatus,conditionLeadConStatus);
        leadConv.setConvertedStatus(convertStatus.MasterLabel);
        
        Database.LeadConvertResult leadConvRes = Database.convertLead(leadConv);
        //system.debug('leadConvRes'+leadConvRes);
         Opportunity application = new Opportunity();
        if(leadConvRes.isSuccess()){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.CONVERTEDCONTACTID,HDFC_DASH_Constants.CONVERTEDOPPORTUNITYID};
                 
                Lead convLead = HDFC_DASH_Lead_Selector.getLeadsBasedOnId(fieldList,newLead.Id);
            //update converted contact record
            Contact updateConvertedConRec = new Contact(Id =convLead.ConvertedContactId);
            updateConvertedConRec.HDFC_DASH_Is_Customer__c = HDFC_DASH_Constants.BOOLEAN_TRUE;
             updateConvertedConRec.HDFC_DASH_Eligble_For_Exstng_Cust_Journy__c = HDFC_DASH_Constants.BOOLEAN_FALSE;
            update updateConvertedConRec;
            //fetching updated Contact Record with Account.HDFC_DASH_Customer_Number__c
            List<String> ConFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Contact_Number,HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number,
                HDFC_DASH_Constants.HDFC_DASH_Eligble_For_Exstng_Cust_Journy,HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.STRING_MobilePhone};
                Contact convertedConRec = HDFC_DASH_Contact_Selector.getContactBasedOnId(ConFieldList,convLead.ConvertedContactId);
            //create application detail record
            appDetails = createApplicantDetailsRecord(leadDetails,newLead.id);
            //system.debug('appDetails===='+appDetails);
            //update converted application record
            List<String> appFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber};
            application = HDFC_DASH_Application_Selector.getApplicationBasedOnId(appFieldList,convLead.ConvertedOpportunityId);
            //system.debug('application==='+application);
            application.HDFC_DASH_Lead__c = newLead.id;
            
            update application;
            //system.debug('application===='+application);
            //isCustomer =HDFC_DASH_Constants.BOOLEAN_FALSE;
            createResponseStructure(convertedConRec,application,appDetails,newLead); 
            
        }
        return appDetails.id;
    }
    
    /* 
Method Name :createApplicantDetailsRecord
Parameters  :HDFC_DASH_ParseleadDetails leadDetails,Id newLeadId
Description :This method would create a HDFC_DASH_Applicant_Details__c record based on the request.
*/
    private static HDFC_DASH_Applicant_Details__c  createApplicantDetailsRecord(HDFC_DASH_ParseleadDetails leadDetails,Id newLeadId){
        HDFC_DASH_Applicant_Details__c newAppDetail = new HDFC_DASH_Applicant_Details__c();
        SObject aggregateADRec;
        List<String> leadFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_convertedContactId,HDFC_DASH_Constants.STRING_convertedOpportunityId};
            Lead leadRec = HDFC_DASH_Lead_Selector.getLeadsBasedOnId(leadFieldList, newLeadId);
        
        newAppDetail.HDFC_DASH_User_Consent_For_CIBIL__c = HDFC_DASH_Constants.STRING_YES;
        newAppDetail.HDFC_DASH_Contact__c =  leadRec.ConvertedContactId; 
        
        newAppDetail.HDFC_DASH_Application__c =  leadRec.ConvertedOpportunityId; 
        newAppDetail.HDFC_DASH_Applicant_Capacity__c =  HDFC_DASH_Constants.STRING_B;
        newAppDetail.RecordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Details__c
            .getRecordTypeInfosByDeveloperName().get(HDFC_DASH_Constants.PRIMARY_APPLICANT).getRecordTypeId();
        newAppDetail.Name = leadDetails?.Lead?.Salutation+' '+leadDetails?.Lead?.FirstName+' '+(leadDetails?.Lead?.MiddleName==null?'':leadDetails?.Lead?.MiddleName+' ')+leadDetails?.Lead?.LastName;
        newAppDetail.HDFC_DASH_Customer_Has_Application__c = HDFC_DASH_Constants.BOOLEAN_FALSE;
        newAppDetail.HDFC_DASH_Stop_Customer_Data_Edit__c = HDFC_DASH_Constants.BOOLEAN_FALSE;
        newAppDetail.HDFC_DASH_Is_First_Application_For_Cust__c = HDFC_DASH_Constants.BOOLEAN_TRUE;
        
        aggregateADRec = generateCommonFields(leadDetails,(Sobject)newAppDetail);
        insert aggregateADRec;
        return  (HDFC_DASH_Applicant_Details__c)aggregateADRec; 
    }
    
    /* 
Method Name :generateCommonFields
Parameters  :HDFC_DASH_ParseleadDetails leadDetails,Sobject comObj
Description :This method would generate the common feilds in between Lead,Applicant Details records.
*/
     public static SObject generateCommonFields(HDFC_DASH_ParseleadDetails leadDetails,Sobject comObj){ 
        if(comObj.getSObjectType()==Schema.Lead.getsobjectType() ||comObj.getSObjectType()==Schema.Account.getsobjectType()){
            comObj.put(HDFC_DASH_Constants.STRING_Salutation,leadDetails?.lead?.salutation);
            comObj.put(HDFC_DASH_Constants.STRING_FirstName,leadDetails?.lead?.firstName);
            comObj.put(HDFC_DASH_Constants.STRING_MiddleName,leadDetails?.lead?.middleName);
            comObj.put(HDFC_DASH_Constants.STRING_LastName,leadDetails?.lead?.lastName);
            //comObj.put(HDFC_DASH_Constants.STRING_Email,leadDetails?.lead?.email);
            //comObj.put(HDFC_DASH_Constants.STRING_MobilePhone,leadDetails?.lead?.mobile); 
        }
         if(comObj.getSObjectType()==Schema.HDFC_DASH_Applicant_Details__c.getsobjectType()){
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_Salutation,leadDetails?.lead?.salutation);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_FirstName,leadDetails?.lead?.firstName);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_MiddleName,leadDetails?.lead?.middleName);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_LastName,leadDetails?.lead?.lastName);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_Email,leadDetails?.lead?.email);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_Mobile,leadDetails?.lead?.mobile);
        }
         if(comObj.getSObjectType()==Schema.Lead.getsobjectType() || comObj.getSObjectType()==Schema.HDFC_DASH_Applicant_Details__c.getsobjectType()){
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_PAN, leadDetails?.lead?.PAN);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Pan_Applied,leadDetails?.lead?.panApplied);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Pan_Status_From_NSDL, leadDetails?.lead?.panStatusFromNSDL);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth, leadDetails?.lead?.dateOfBirth);
        //comObj.put(HDFC_DASH_Constants.HDFC_DASH_Gender_Code,leadDetails?.lead?.genderCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Gender,leadDetails?.lead?.genderCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_City_Code,leadDetails?.lead?.cityCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_State_Code,leadDetails?.lead?.stateCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Country_Code,leadDetails?.lead?.countryCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_City,leadDetails?.lead?.cityName);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_State,leadDetails?.lead?.stateName);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Country,leadDetails?.lead?.countryName);
        comObj = HDFC_DASH_UtilityClass.generateMasterTabelFields(comObj);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Resident_Type,leadDetails?.lead?.residentType);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Mobile_Country_Code,leadDetails?.lead?.mobileCountryCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Outsystems_ID,leadDetails?.lead?.outSystemsID);
        
            //system.debug('leadDetails----'+leadDetails.lead.Consent.VoiceConsent);
            //comObj.put(HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_Type,LMSOrILPSorConDetails.get(HDFC_DASH_Constants.STRING_LMSLeadType));
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_Voice_Consent,leadDetails?.lead?.consent?.voiceConsent);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_Voice_Consent_Date, leadDetails?.lead?.consent?.voiceConsentDate);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_WhatsApp_Consent,leadDetails?.lead?.consent?.whatsAppConsent);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_WhatsApp_Consent_Date, leadDetails?.lead?.consent?.whatsAppConsentDate);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_SMS_Consent,leadDetails?.lead?.consent?.smsConsent);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_SMS_Consent_Date, leadDetails?.lead?.consent?.smsConsentDate);
            comObj.put(HDFC_DASH_Constants.HDFC_DASH_Consent_Content, leadDetails.lead?.consent?.consentContent);
         }else{
             comObj.put(HDFC_DASH_Constants.HDFC_DASH_PAN_PA, leadDetails?.lead?.PAN);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Pan_Applied_PA,leadDetails?.lead?.panApplied);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Pan_Status_From_NSDL_PA, leadDetails?.lead?.panStatusFromNSDL);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth_PA, leadDetails?.lead?.dateOfBirth);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Gender_PA,leadDetails?.lead?.genderCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_City_Code_PA,leadDetails?.lead?.cityCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_State_Code_PA,leadDetails?.lead?.stateCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Country_Code_PA,leadDetails?.lead?.countryCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_City_PA,leadDetails?.lead?.cityName);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_State_PA,leadDetails?.lead?.stateName);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Country_PA,leadDetails?.lead?.countryName);
        comObj = HDFC_DASH_UtilityClass.generateMasterTabelFields(comObj);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Resident_Type_PA,leadDetails?.lead?.residentType);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Mobile_Country_Code_PA,leadDetails?.lead?.mobileCountryCode);
        comObj.put(HDFC_DASH_Constants.HDFC_DASH_Outsystems_ID_PA,leadDetails?.lead?.outSystemsID);
         }
        return comObj;
    }
    
    /* 
Method Name :runSalesforceDedupe
Parameters  :HDFC_DASH_ParseleadDetails leadDetails
Description :This method would check if a contact is already present in Salesforce.
*/
    public static Contact runSalesforceDedupe(HDFC_DASH_ParseleadDetails leadDetails){
        Contact contactsPassingDedupeCheck =NULL;
        if(String.isNotBlank(leadDetails?.lead?.pan)){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,HDFC_DASH_Constants.STRING_FirstName,HDFC_DASH_Constants.STRING_LastName,
                HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.STRING_MobilePhone,HDFC_DASH_Constants.STRING_name,
                HDFC_DASH_Constants.HDFC_DASH_Is_Customer,HDFC_DASH_Constants.HDFC_DASH_Is_Registered_Customer,HDFC_DASH_Constants.HDFC_DASH_Contact_Number,
                HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number,HDFC_DASH_Constants.HDFC_DASH_Eligble_For_Exstng_Cust_Journy};
                    Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_PAN => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+leadDetails.lead.pan+HDFC_DASH_Constants.STRING_QUOTE};
                        //system.debug('contact condition'+condition);
            List<Contact> contactWithPan = HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(fieldList, condition);
            
            for(Contact con:contactWithPan){
                if((con.HDFC_DASH_Date_Of_Birth__c ==leadDetails?.lead?.dateOfBirth) ||(con.Email == leadDetails?.lead?.email) ||(con.MobilePhone == leadDetails?.lead?.mobile)||HDFC_DASH_UtilityClass.doesNameMatch(leadDetails?.lead?.firstName,leadDetails?.lead?.lastName,con.FirstName,con.LastName)){
                    contactsPassingDedupeCheck=con;
                    break;
                }
            }
            if(contactWithPan.isEmpty()){ 
                contactsPassingDedupeCheck = doNoMatchingPANSearch(leadDetails);
            }
        }else if(contactsPassingDedupeCheck == NULL){
            contactsPassingDedupeCheck = doNoMatchingPANSearch(leadDetails); 
        }
        //system.debug('contactspassingDedupeCheck'+contactspassingDedupeCheck);
        return contactsPassingDedupeCheck;
    }
    
    /* 
Method Name :doNoMatchingPANSearch
Parameters  : HDFC_DASH_ParseleadDetails leadDetails
Description :This method would check if contact is present in salesforce where the pan is not matching.
*/
    private static Contact doNoMatchingPANSearch(HDFC_DASH_ParseleadDetails leadDetails){
        Contact contactspassingDedupeCheck = NULL;
        Boolean doesNameMatch = HDFC_DASH_Constants.BOOLEAN_FALSE;
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.STRING_MobilePhone,HDFC_DASH_Constants.STRING_name,
            HDFC_DASH_Constants.STRING_FirstName,HDFC_DASH_Constants.STRING_LastName,HDFC_DASH_Constants.HDFC_DASH_Is_Customer,HDFC_DASH_Constants.HDFC_DASH_Is_Registered_Customer,HDFC_DASH_Constants.HDFC_DASH_Contact_Number,
            HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number,HDFC_DASH_Constants.HDFC_DASH_Eligble_For_Exstng_Cust_Journy};
                Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_Email => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+leadDetails.Lead.Email+HDFC_DASH_Constants.STRING_QUOTE,
                    HDFC_DASH_Constants.STRING_AND +HDFC_DASH_Constants.STRING_MobilePhone => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+leadDetails.Lead.Mobile+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE,
                    HDFC_DASH_Constants.STRING_OR+HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_Email=> HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+leadDetails.Lead.Email+HDFC_DASH_Constants.STRING_QUOTE,
                    HDFC_DASH_Constants.STRING_AND+HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth => HDFC_DASH_Constants.STRING_EQUALTO+changeDateToString(leadDetails.Lead.DateOfBirth)+HDFC_DASH_Constants.STRING_CLOSING_BRACE,
                    HDFC_DASH_Constants.STRING_OR+HDFC_DASH_Constants.STRING_OPEN_BRACE+HDFC_DASH_Constants.STRING_MobilePhone=> HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+leadDetails.Lead.Mobile+HDFC_DASH_Constants.STRING_QUOTE,
                    HDFC_DASH_Constants.STRING_AND+' '+ HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth => HDFC_DASH_Constants.STRING_EQUALTO+changeDateToString(leadDetails.Lead.DateOfBirth)+HDFC_DASH_Constants.STRING_CLOSING_BRACE};
                        //system.debug(HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(fieldList, condition)+'Query to get a contact without pan');
        for(Contact con:HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(fieldList, condition)){
            //system.debug('con name match'+con.FirstName +'----'+con.LastName);
            doesNameMatch = HDFC_DASH_UtilityClass.doesNameMatch(leadDetails?.Lead?.FirstName,leadDetails?.Lead?.LastName,con.FirstName,con.LastName);    
            if(doesNameMatch){
                contactspassingDedupeCheck =con;  
                break;
            }
        }
        return contactspassingDedupeCheck; 
        
    }
    
    /* 
Method Name :changeDateToString
Parameters  :Date dateFormat
Description :This method would remove ' 00:00:00' of the date and convert it to a string.
*/
    public static String changeDateToString(Date dateFormat){
        String lmsDateFormat;
        if(dateFormat !=NULL){
            lmsDateFormat = String.valueOf(dateFormat).removeEnd(HDFC_DASH_Constants.STRING_TIME);
        } 
        return lmsDateFormat; 
    } 
    
     /* 
Method Name :insertPersonAccount
Parameters  :Date dateFormat
Description :This method would create a person account if onlyCustomerDedupe .
*/
    public static Contact insertPersonAccount(HDFC_DASH_ParseleadDetails leadDetails){	       
        Account newPersonAcc = new Account(); 
        newPersonAcc.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.STRING_PERSONACCOUNT).getRecordTypeId();
        newPersonAcc = (Account)generateCommonFields(leadDetails, newPersonAcc);
        newPersonAcc.PersonEmail = leadDetails?.lead?.email;
        newPersonAcc.PersonMobilePhone = leadDetails?.lead?.mobile;
        newPersonAcc.HDFC_DASH_Is_Customer__pc = HDFC_DASH_Constants.BOOLEAN_TRUE;
        newPersonAcc.HDFC_DASH_Eligble_For_Exstng_Cust_Journy__pc = HDFC_DASH_Constants.BOOLEAN_FALSE;
         insert newPersonAcc;
        
         List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,HDFC_DASH_Constants.STRING_FirstName,HDFC_DASH_Constants.STRING_LastName,
                HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.STRING_MobilePhone,HDFC_DASH_Constants.STRING_name,
                HDFC_DASH_Constants.HDFC_DASH_Is_Customer,HDFC_DASH_Constants.HDFC_DASH_Is_Registered_Customer,HDFC_DASH_Constants.HDFC_DASH_Contact_Number,
             	HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number,HDFC_DASH_Constants.HDFC_DASH_Eligble_For_Exstng_Cust_Journy}; 
                    Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.STRING_ACCOUNTID => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+newPersonAcc.id+HDFC_DASH_Constants.STRING_QUOTE};
                        //system.debug('contact condition'+condition); 
            Contact contactRec = (HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(fieldList, condition))[HDFC_DASH_Constants.INT_ZERO]; 
        return contactRec; 
    } 
}