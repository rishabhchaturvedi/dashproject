/*   
ClassName: HDFC_DASH_TestDataFactory_Contacts
DevelopedBy: Punam Marbate
Date: 05 July 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
                      used throughout HDFC DASH.
                      In this class we will be creating data for Contact object based on parameters received
                      from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_Contacts {

    public static final string FIRSTNAME = 'FirstName';
    public static final string LASTNAME = 'LastName';    
    public static final string PAN = 'LKOPS';
    public static final string STRING_L = 'L';
    public static final string PHONENUMBER = '897656';
    public static final Date DATE_OF_BIRTH =Date.valueOf('1998-08-24');
    private static final string BSA_CON_ID = '67890';
    private static final string FEDERATION_IDENTIFIER = '123456';
    private static final string USER_ID = '123456';
    /* Method Name: createContactList
    parameters: None
    Class Description: This method is to generate test data for Contact.
    */
   public static Contact createBasicContacts()
    {
        Contact con = new Contact();
        con.FirstName =FIRSTNAME; 
        con.LastName = LASTNAME;   
        con.MobilePhone= PHONENUMBER+Integer.valueof((Math.random() * 1000)+1000);
        con.HDFC_DASH_PAN__c = PAN+Integer.valueof((Math.random() * 1000)+1000)+STRING_L;
        con.HDFC_DASH_Date_Of_Birth__c =DATE_OF_BIRTH;
        con.HDFC_DASH_Executive_Code__c = BSA_CON_ID;
        con.HDFC_DASH_Federation_Identifier__c = FEDERATION_IDENTIFIER;
        con.HDFC_DASH_User_Id__c = USER_ID;
        return con;
    }

    /* Method Name: getBasicContact
    parameters: NONE
    Class Description: This method is to generate test data for Contact.
    */
    public static Contact getBasicContact()
    {
        Contact con = HDFC_DASH_TestDataFactory_Contacts.createBasicContacts();
        if(Schema.sObjectType.Contact.isCreateable()){
            Database.insert(con);
        }
        return con;
    }
       
     /* Method Name: createBasicContactsWithAccountID
    parameters: None
    Class Description: This method is to generate test data for Contact.
    */
   public static Contact createBasicContactsWithAccountID(Id accId)
    {
        Contact con = new Contact();
        con.FirstName =FIRSTNAME; 
        con.LastName = LASTNAME;   
        con.MobilePhone= PHONENUMBER+Integer.valueof((Math.random() * 1000)+1000);
        con.HDFC_DASH_PAN__c = PAN+Integer.valueof((Math.random() * 1000)+1000)+STRING_L;
        con.HDFC_DASH_Date_Of_Birth__c =DATE_OF_BIRTH;
        con.HDFC_DASH_Executive_Code__c = BSA_CON_ID;
        con.AccountId = accId;
       con.HDFC_DASH_User_Id__c = USER_ID;

        return con;
    }

    /* Method Name: getBasicContactWithAccountId
    parameters: NONE
    Class Description: This method is to generate test data for Contact.
    */
    public static Contact getBasicContactWithAccountId(Id accId)
    {
        Contact con = HDFC_DASH_TestDataFactory_Contacts.createBasicContactsWithAccountID(accId);
        if(Schema.sObjectType.Contact.isCreateable()){
            Database.insert(con);
        }
        return con;
    }
        
    /* Method Name: createBasicContactlist
    parameters: Integer numberofcontacts
    Class Description: This method is to create a list of test data for Contact
                        by calling a createBasicContact method.
    */
    public static List<Contact> createBasicContactlist(Integer numberofcontacts)
    {
        list<Contact> contactlist = new list<Contact>();
        for(integer i=0;i<numberofcontacts; i++){       
            contactlist.add(HDFC_DASH_TestDataFactory_Contacts.createBasicContacts());
        }
        return contactlist;
    }

    /* Method Name: getBasicContactlist
    parameters: Integer numberofcontacts
    Class Description: This method is to generate a list of test data for Contact.
    */
    public static List<Contact> getBasicContactlist(Integer numberofcontacts)
    {
        list<Contact> contactlist = HDFC_DASH_TestDataFactory_Contacts.createBasicContactlist(numberofcontacts);
        if(Schema.sObjectType.Contact.isCreateable()){
            Database.insert(contactlist);
        }
        return contactlist;
    }
    
    /* Method Name: getBasicPersonContact
    parameters: Account Id
    Class Description: This method is to generate test data for Person Contact Record.
    */
    public static Id getBasicPersonContact(Id accountId)
    {
        List<String> accFieldsList = new List<String>{HDFC_DASH_Constants.STRING_ID,HDFC_DASH_Constants.PersonContactId};
        Account personAcc = HDFC_DASH_Account_Selector.getAccountsBasedOnId(accFieldsList,accountId);
        Id conId = personAcc.PersonContactId;
        return conId;
    }
}