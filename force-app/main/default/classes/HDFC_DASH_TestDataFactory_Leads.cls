/* 
    ClassName: HDFC_DASH_TestDataFactory_Leads
    ClassDescription: Test data factory class for maintaining and test data creation which will be
                      used throughout HDFC DASH.
                      In this class we will be creating data for all objects based on parameters recived
                      from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_Leads {
   
   	private static final string SALUTATION = 'MR';
    private static final string FIRSTNAME = 'FirstName';
    private static final string LASTNAME = 'LastName';    
    private static final string PAN = 'DRSPD1017B';
    private static final string BSA_ID = '12345';
    private static final string BSA_CON_ID = '67890';
    private static final string SO_ID = '13579';
    private static final string LMS_ID = '5476777884443';
    public static final string LEAD_BRANCH_ID = '201';

    /* Method Name: createBasicLeads
    parameters: None
    Class Description: This method is to generate test data for Lead.
    */
    public static Lead createBasicLead()
    {
        Lead leadRecord = new Lead();
       leadRecord.Salutation = SALUTATION;
        leadRecord.FirstName =FIRSTNAME; 
        leadRecord.LastName = LASTNAME;   
        leadRecord.HDFC_DASH_PAN__c = PAN ;
        leadRecord.MiddleName ='';
        leadRecord.HDFC_DASH_LMS_Lead_ID__c = LMS_ID;
        leadRecord.HDFC_DASH_LMS_Origin_Branch_Id__c = LEAD_BRANCH_ID;
        //leadRecord.HDFC_DASH_BSA_Id__c = BSA_ID;
        //leadRecord.HDFC_DASH_BSA_Contact_Id__c = BSA_CON_ID;
        //leadRecord.HDFC_DASH_Sales_Officer_Id__c = SO_ID;    
	    return leadRecord;
    }
    
    /* Method Name: getBasicLead
    parameters: NONE
    Class Description: This method is to generate test data for Lead.
    */
    public static Lead getBasicLead()
    {
        Lead leadRecord = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
        if(Schema.sObjectType.Lead.isCreateable()){
            Database.insert(leadRecord);
        }
        return leadRecord;
    }
        
    /* Method Name: createBasicLeadlist
    parameters: Integer numberofLeads
    Class Description: This method is to create a list of test data for Lead
                        by calling a createBasicLead method.
    */
    public static List<Lead> createBasicLeadlist(Integer numberofleads)
    {
        list<Lead> leadlist = new list<Lead>();
        for(integer i=0; i<numberofleads; i++){   
            leadlist.add(HDFC_DASH_TestDataFactory_Leads.createBasicLead());
        }
        return leadlist;
    }
    
    /* Method Name: getBasicLeadlist
    parameters: Integer numberofleads
    Class Description: This method is to generate a list of test data for Lead.
    */
    public static List<Lead> getBasicLeadlist(Integer numberofleads)
    {
        list<Lead> leadlist = HDFC_DASH_TestDataFactory_Leads.createBasicLeadlist(numberofleads);
        if(Schema.sObjectType.Lead.isCreateable()){
            Database.insert(leadlist);
        }
        return leadlist;
    }
}