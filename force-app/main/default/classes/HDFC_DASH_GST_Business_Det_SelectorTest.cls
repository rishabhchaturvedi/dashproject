/**
className: HDFC_DASH_GST_Business_Det_SelectorTest
DevelopedBy: Sai Suman
Date: 07 October 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_GST_Business_Det_Selector class.
**/
@isTest(SeeAllData = false) 
public class HDFC_DASH_GST_Business_Det_SelectorTest {
     //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails =
                    HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c empDetails =
                    HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.id,con.id);
    private static HDFC_DASH_GST_Business_Details__c gstDetails =
                    HDFC_DASH_TestDataFactory_GSTbusinessDet.getBasicGSTDetail(empDetails.Id);
     /*
    Method Name :testGetGstBusinessDetails
    Description :This method will test the method getGstBusinessDetails.
    */
    static testmethod void testGetGstBusinessDetails(){
        List<HDFC_DASH_GST_Business_Details__c>  listGstDetailRec = null;
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listGstDetailRec = HDFC_DASH_GST_Business_Detail_Selector.getGstBusinessDetails(
                                new List<String> {gstDetails.Id},
                                new List<String>{HDFC_DASH_Constants.STRING_ID},
                                HDFC_DASH_Constants.ID_STRING);
            Test.stopTest();
        }
        system.assertNotEquals( null, listGstDetailRec);
        system.assertEquals(1,listGstDetailRec.size(),'SUCCESS');
    }
}