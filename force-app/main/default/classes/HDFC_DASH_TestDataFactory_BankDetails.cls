/* 
ClassName: HDFC_DASH_TestDataFactory_BankDetails
DevelopedBy: Sindu Priya
Date:28 Aug 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Applicant Bank Detail object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_BankDetails {
      public static final string NAME = 'Name';
       
    /* Method Name: createBasicAppBankDetail
    parameters: ID appID
    Method Description: This method is to create test data for HDFC_DASH_Applicant_Bank_Detail__c.
    Pass fields of HDFC_DASH_Applicant_Bank_Detail__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Applicant_Bank_Detail__c or inserted HDFC_DASH_Applicant_Bank_Detail__c id.
    */
    public static HDFC_DASH_Applicant_Bank_Detail__c createBasicAppBankDetail(ID appID, ID conID)
    {
        HDFC_DASH_Applicant_Bank_Detail__c appBankDet = new HDFC_DASH_Applicant_Bank_Detail__c();
        appBankDet.HDFC_DASH_Applicant_details__c =  appID  ;
        appBankDet.HDFC_DASH_Contact__c  = conID;
        return appBankDet;
    }
    /* Method Name: getBasicApplBankDetail
    parameters: ID applID
    Method Description: This method is to generate test data for ApplBankDetail.
    */
    public static HDFC_DASH_Applicant_Bank_Detail__c getBasicApplBankDetail(ID appID,ID conID)
    {
        HDFC_DASH_Applicant_Bank_Detail__c applBankDetail = HDFC_DASH_TestDataFactory_BankDetails.createBasicAppBankDetail(appID,conID);
        if(Schema.sObjectType.HDFC_DASH_Applicant_Bank_Detail__c.isCreateable()){
            Database.insert(applBankDetail);
        }
        return applBankDetail;
    } 
    
    /* Method Name: createBasicApplBankDetail
    parameters: Integer numberofBankDet
    Class Description: This method is to create a list of test data for Applicant bank detail
                        by calling a createBasicContact method.
    */
    public static List<HDFC_DASH_Applicant_Bank_Detail__c> createBasicApplBankDetailList(Integer numberofBankDet,ID appID,ID conID)
    {
        list<HDFC_DASH_Applicant_Bank_Detail__c> appBankDetlist = new list<HDFC_DASH_Applicant_Bank_Detail__c>();
        for(integer i=0;i<numberofBankDet; i++){       
            appBankDetlist.add(HDFC_DASH_TestDataFactory_BankDetails.createBasicAppBankDetail(appID,conID));
        }
        return appBankDetlist;
    }
    
    /* Method Name: getBasicApplBankDetail
    parameters: Integer numberofcontacts
    Class Description: This method is to generate a list of test data for Applicant bank detail.
    */
    public static List<HDFC_DASH_Applicant_Bank_Detail__c> getBasicApplBankDetailList(Integer numberofBankDet,ID appID,ID conID) 
    {
        list<HDFC_DASH_Applicant_Bank_Detail__c> appBankDetlist = HDFC_DASH_TestDataFactory_BankDetails.createBasicApplBankDetailList(numberofBankDet,appID,conID);
        if(Schema.sObjectType.HDFC_DASH_Applicant_Bank_Detail__c.isCreateable()){
            Database.insert(appBankDetlist);
        }
        return appBankDetlist;
    }
    
}