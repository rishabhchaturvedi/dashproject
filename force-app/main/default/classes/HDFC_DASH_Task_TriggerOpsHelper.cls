/**
className: HDFC_DASH_Task_TriggerOpsHelper
DevelopedBy: Anisha Arumugam
Date: 25 November 2021
Company: Accenture 
Class Description:  Helper class for HDFC_DASH_Task_TriggerOps.
**/
public with sharing class HDFC_DASH_Task_TriggerOpsHelper 
{
    /* 
Method Name :filterTasksWithCommunication
Description :This method will filter the Tasks for sending out the SMS.
*/
    public static List<Task> filterTasksWithCommunication()
    {
        List<Task> listTask = new List<Task>();
        List<Task> listUpdateTask = new List<Task>();
        listTask = trigger.new;

        for(Task taskRec : listTask)
        {
            system.debug('inside  helper filer '+taskRec);
            if(taskRec.HDFC_DASH_Is_Outbound_Communication__c){
                listUpdateTask.add(taskRec);
            }
            if(listUpdateTask.size()>=50){
                break;
            }
        }
        return listUpdateTask; 
    }
    
    /*      
Method Name :sendMessage
Description :This method would send the SMS/WA message.
*/
    public static void sendMessage(List<Task> listTask)
    {
        for(Task objTask : listTask)
        {
            system.debug('inside sendMessage '+listTask);
            //Enqueue SMS Callout
            HDFC_DASH_CommunicationWrapper commCallout = new HDFC_DASH_CommunicationWrapper(objTask); 
            commCallout.processCommunication();
        }
    }
}