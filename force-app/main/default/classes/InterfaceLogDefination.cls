public abstract class InterfaceLogDefination{
    
    public abstract HttpResponse doCallOut(String intefaceName,string methodType,string reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId);
    public abstract HttpResponse doCallOut(String intefaceName,string methodType,blob reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId);
    public abstract void logContinuationCallout(String intefaceName,string methodType,string reqBody,map<string,string> headerMap,string PostRetryProcess,string continuationId,string messageId);
    public abstract void logContinuationCallout(String intefaceName,string methodType,blob reqBody,map<string,string> headerMap,string PostRetryProcess,string continuationId,string messageId);
    public abstract HttpResponse doMultiCallOut(String intefaceName,string methodType,string reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId,boolean isLast);
    public abstract HttpResponse doMultiCallOut(String intefaceName,string methodType,blob reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId,boolean isLast);
    //public abstract void createInboundIntegrationLog(String interfaceName, String requestBody, string response, String methodType,string messageId, Exception Ex);
   // public abstract void logContinuationCalloutResponse(String interfaceName, HttpResponse response,String continuationId);
}