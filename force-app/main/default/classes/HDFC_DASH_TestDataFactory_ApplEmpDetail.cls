/* 
ClassName: HDFC_DASH_TestDataFactory_ApplEmpDetail
DevelopedBy: Punam Marbate
Date: 22 July 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Applicant Employment Details object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_ApplEmpDetail {
    public static final string NAME = 'Name';
    private static final string STRING_ID='Id';
    //private static final string NAT_OF_EMP ='E';
   
    /* Method Name: createBasicApplEmploymentDetail
    parameters: ID aplDetailID
    Method Description: This method is to create test data for HDFC_DASH_Applicant_Employment_Details__c.
    Pass fields of HDFC_DASH_Applicant_Employment_Details__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Applicant_Employment_Details__c or inserted HDFC_DASH_Applicant_Employment_Details__c id. 
    */
    public static HDFC_DASH_Applicant_Employment_Details__c createBasicApplEmploymentDetail(ID aplDetailID,ID ConID)
    {
        HDFC_DASH_Applicant_Employment_Details__c appEmpDetail = new HDFC_DASH_Applicant_Employment_Details__c();
            
        //appEmpDetail.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        appEmpDetail.HDFC_DASH_Applicant_Details__c =  aplDetailID  ; 
        appEmpDetail.HDFC_DASH_Contact__c   = ConID;
       
        return appEmpDetail;        
    }
    
    /* Method Name: getBasicApplEmploymentDetail
    parameters: ID aplDetailID
    Method Description: This method is to generate test data for Applicant Employment Detail.
    */
    public static HDFC_DASH_Applicant_Employment_Details__c getBasicApplEmploymentDetail(ID aplDetailID,ID ConID)
    {
        HDFC_DASH_Applicant_Employment_Details__c appEmpDetail = 
                                HDFC_DASH_TestDataFactory_ApplEmpDetail.createBasicApplEmploymentDetail(aplDetailID,ConID);
        if(Schema.sObjectType.HDFC_DASH_Applicant_Employment_Details__c.isCreateable()){
            Database.insert(appEmpDetail);
        }
        return appEmpDetail;
    }
     /* Method Name: createBasicApplEmpDetailList
    parameters: Integer numberofEmpDet
    Class Description: This method is to create a list of test data for Applicant employment detail
                        by calling a createBasicApplEmploymentDetail method.
    */
    public static List<HDFC_DASH_Applicant_Employment_Details__c> createBasicApplEmpDetailList(Integer numberofEmpDet,ID appID,ID conID)
    {
        list<HDFC_DASH_Applicant_Employment_Details__c> appEmpDetlist = new list<HDFC_DASH_Applicant_Employment_Details__c>();
        for(integer i=0;i<numberofEmpDet; i++){       
            appEmpDetlist.add(HDFC_DASH_TestDataFactory_ApplEmpDetail.createBasicApplEmploymentDetail(appID,conID));
        }
        return appEmpDetlist;
    }
    
    /* Method Name: getBasicApplEmpDetailList
    parameters: Integer numberofempList
    Class Description: This method is to generate a list of test data for Applicant employment detail.
    */
    public static List<HDFC_DASH_Applicant_Employment_Details__c> getBasicApplEmpDetailList(Integer numberofempList,ID appID,ID conID) 
    {
        list<HDFC_DASH_Applicant_Employment_Details__c> empBankDetlist = HDFC_DASH_TestDataFactory_ApplEmpDetail.createBasicApplEmpDetailList(numberofempList,appID,conID);
        if(Schema.sObjectType.HDFC_DASH_Applicant_Employment_Details__c.isCreateable()){
            Database.insert(empBankDetlist);
        }
        return empBankDetlist;
    }
   
}