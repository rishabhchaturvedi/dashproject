/*
Class: HDFC_DASH_IntegrateIlpsLacs
Author: Pratheeksha Bayari
Date: 18 Nov 2021
Company: Accenture
Description: Queueable Class to make Case Callout to ILPS 
*/
public class HDFC_DASH_IntegrateIlpsLacs implements Queueable, Database.AllowsCallouts{
    
    Id caseId;
    HttpResponse response;
    RestRequest request;
    public Map<String,string> requestHeader = new Map<String,String>();
    Exception exp;
    
    public HDFC_DASH_IntegrateIlpsLacs(Id caseId){
        this.caseId = caseId;
    }
    
        /*
Method: execute
Description: This method will Post the Document Details to ILPS.
*/
    public void execute(QueueableContext context){
        try{
            
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
            HDFC_DASH_CaseDetailsReq_ILPSCallout.caseDetails reqBody = getRequestBody();
            system.debug('reqBody...'+string.ValueOf(Json.serialize(reqBody)));
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_CD);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_CD, HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            //processResponse(response);  
			system.debug('response..'+response); 
            system.debug('response.getBody()...'+response.getBody());
        }catch(Exception e){
            
        }
    }
    
    //getRequestBody
    public HDFC_DASH_CaseDetailsReq_ILPSCallout.caseDetails getRequestBody(){
        HDFC_DASH_CaseDetailsReq_ILPSCallout caseReq = new HDFC_DASH_CaseDetailsReq_ILPSCallout();
        HDFC_DASH_CaseDetailsReq_ILPSCallout.caseDetails caseReqDetail = new HDFC_DASH_CaseDetailsReq_ILPSCallout.caseDetails();
        //List<HDFC_DASH_CaseDetailsReq_ILPSCallout.caseDetails> caseReqDetailList = new List<HDFC_DASH_CaseDetailsReq_ILPSCallout.caseDetails>();
        String formData;
        List<Id> caseRecId = new List<Id>{caseId};
        Case caseR = new Case();
        List<String> listOfFields_Case = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.CaseNumber,HDFC_DASH_Constants.CASEREFTYPE,HDFC_DASH_Constants.CASEREFNO,HDFC_DASH_Constants.EVENT_Description,HDFC_DASH_Constants.CASEORIGIN,HDFC_DASH_Constants.HDFC_DASH_Case_Type,HDFC_DASH_Constants.HDFC_DASH_Category,HDFC_DASH_Constants.CUSTNAME,HDFC_DASH_Constants.CUSTACCNO,'HDFC_DASH_File_Number__r.Name','Owner.Name','HDFC_DASH_File_Number__r.HDFC_DASH_Reference_Type__c','HDFC_DASH_Sub_Category__c','HDFC_DASH_Mode__c','HDFC_DASH_Sub_mode__c','Status','LastModifiedDate'};
        
        caseR = HDFC_DASH_Case_Selector.getCaseBasedOnId(listOfFields_Case,caseId);
        system.debug('caseR...'+caseR);
        //caseR = caseList[HDFC_DASH_Constants.INT_ZERO];    
        system.debug('caseR'+caseR);
        
        caseReqDetail.CASE_ID = caseR?.CaseNumber;
        //caseReqDetail.REF_TYPE = caseR?.HDFC_DASH_Reference_Type__c;
        caseReqDetail.REF_TYPE = caseR?.HDFC_DASH_File_Number__r.HDFC_DASH_Reference_Type__c;
        //caseReqDetail.REF_NO = String.valueOf(caseR?.HDFC_DASH_Reference_Number__c);
        caseReqDetail.REF_NO = String.valueOf(caseR?.HDFC_DASH_File_Number__r.Name);
        //caseReqDetail.INTENT_DESC = caseR?.Description;
        caseReqDetail.INTENT_DESC = (caseR?.Status == HDFC_DASH_Constants.STATUS_RE_OPEN ? HDFC_DASH_Constants.INTENTCODEREOPEN : HDFC_DASH_Constants.INTENTCODE);
        caseReqDetail.INTENT_CODE = (caseR?.Status == HDFC_DASH_Constants.STATUS_RE_OPEN ? HDFC_DASH_Constants.INTENTCODEREOPEN : HDFC_DASH_Constants.INTENTCODE);
        caseReqDetail.INTENT_DIRECTION = HDFC_DASH_Constants.INTENTDIRECTION;
        //caseReqDetail.SOURCE_PERSON = caseR?.Origin;
        caseReqDetail.SOURCE_PERSON = String.valueOf('ACC_'+caseR?.Owner.Name);
        
        //string req2 = '"{\'CUSTOMER_NO\':\'1237\',\'CUSTOMER_NAME\' :\'PLZ\',\'INTERACTION_TYPE\' :\'COMPLAINT\',\'CATEGORY\' :\'ROI\'}"';
        //formData = '"{\''+HDFC_DASH_Constants.CUSTOMERNO+'\':\''+caseR?.Loan_Account_Number__c+'\',\''+HDFC_DASH_Constants.CUSTOMERNAME+'\' :\''+caseR?.Customer_Name__c+'\',\''+HDFC_DASH_Constants.INTERACTIONTYPE+'\' :\''+caseR.HDFC_DASH_Case_Type__c+'\',\''+HDFC_DASH_Constants.CATEGORYSTR+'\' :\''+caseR.HDFC_DASH_Category__c+'\'}"';
        //formData = '';
        //string req2 = '"{\''+HDFC_DASH_Constants.CUSTOMERNO+'\':\''+caseR?.Loan_Account_Number__c+'\',\''+HDFC_DASH_Constants.CUSTOMERNAME+'\' :\''+caseR?.Customer_Name__c+'\',\''+HDFC_DASH_Constants.INTERACTIONTYPE+'\' :\''+caseR.HDFC_DASH_Case_Type__c+'\',\''+HDFC_DASH_Constants.CATEGORYSTR+'\' :\''+caseR.HDFC_DASH_Category__c+'\'}"';
        //string req2 = '"{\''+HDFC_DASH_Constants.CUSTOMERNO+'\':\''+caseR?.Loan_Account_Number__c+'\',\''+HDFC_DASH_Constants.CUSTOMERNAME+'\' :\''+caseR?.Customer_Name__c+'\',\''+HDFC_DASH_Constants.INTERACTIONTYPE+'\' :\''+caseR.HDFC_DASH_Case_Type__c+'\',\''+HDFC_DASH_Constants.CATEGORYSTR+'\' :\''+caseR.HDFC_DASH_Category__c+'\',\'SUB_CATEGORY\' :\''+caseR.HDFC_DASH_Sub_Category__c+'\'}"';
        string req2 = '"{\''+HDFC_DASH_Constants.CUSTOMERNO+'\':\''+caseR?.Loan_Account_Number__c+'\',\''+HDFC_DASH_Constants.CUSTOMERNAME+'\' :\''+caseR?.Customer_Name__c+'\',\''+HDFC_DASH_Constants.INTERACTIONTYPE+'\' :\''+caseR.HDFC_DASH_Case_Type__c+'\',\''+HDFC_DASH_Constants.CATEGORYSTR+'\' :\''+caseR.HDFC_DASH_Category__c+'\',\'SUB_CATEGORY\' :\''+caseR.HDFC_DASH_Sub_Category__c+'\',\'MODE\' :\''+caseR.HDFC_DASH_Mode__c+'\',\'SUB_MODE\' :\''+caseR.HDFC_DASH_Sub_mode__c+'\',\'STATUS\' :\''+caseR.Status+'\',\'CreatedDate\' :\''+caseR.LastModifiedDate+'\',\'CaseId\' :\''+caseId+'\'}"';
        system.debug('formData...'+req2);
       caseReqDetail.FORM_DATA = req2;
        caseReq.caseDetails = caseReqDetail;
        return caseReqDetail;
        //return caseReq;
    }
    
   /* public static void postCases() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://extapigtwyuat.hdfc.com/SF_HDFC_INTGTN_API/api/SF_LAC_Intend/InsertCaseRequest');
        request.setMethod('POST');
        request.setHeader('x-api-key', 'ZVPeP3zn1t26JNVakJqkb2MBPtzEALTA1RZruaLS');
        request.setHeader('Content-Type', 'application/json');  
        
        string req2 = '"FORM_DATA": "{\'CUSTOMER_NO\':\'1237\',\'CUSTOMER_NAME\' :\'PLZ\',\'INTERACTION_TYPE\' :\'COMPLAINT\',\'CATEGORY\' :\'ROI\'}"';
        string requestBody = '{"CASE_ID":"11145374","REF_TYPE":"F","REF_NO":"632541791","INTENT_DESC":"CASE CREATION","INTENT_CODE":"IC_CASECR","INTENT_DIRECTION":"SF_TO_CC","SOURCE_PERSON":"EXECUTIVE",'+ req2 +' }';
        //string requestBody = '{"CASE_ID":"95667571","REF_TYPE":"F","REF_NO":"632541791","INTENT_DESC":"CASE CREATION","INTENT_CODE":"IC_CASECR","INTENT_DIRECTION":"SF_TO_CC","SOURCE_PERSON":"EXECUTIVE","FORM_DATA": "{'CUSTOMER_NO':'1237','CUSTOMER_NAME' :'PLZ','INTERACTION_TYPE' :'COMPLAINT','CATEGORY' :'ROI'}"}';
        //request.setBody('{"CASE_ID":"95667571","REF_TYPE":"F","REF_NO":"632541791","INTENT_DESC":"CASE CREATION","INTENT_CODE":"IC_CASECR","INTENT_DIRECTION":"SF_TO_CC","SOURCE_PERSON":"EXECUTIVE","FORM_DATA": {"CUSTOMER_NO":"1237","CUSTOMER_NAME" :"PLZ","INTERACTION_TYPE" :"COMPLAINT","CATEGORY" :"ROI"}}');
        //request.setBody('{"CASE_ID":"95667571","REF_TYPE":"F","REF_NO":"632541791","INTENT_DESC":"CASE CREATION","INTENT_CODE":"IC_CASECR","INTENT_DIRECTION":"SF_TO_CC","SOURCE_PERSON":"EXECUTIVE"}');
		request.setBody(requestBody);
        HttpResponse response = http.send(request);
        if(response.getStatusCode() != 200){
            system.debug('error...'+response.getStatusCode());
        }
        system.debug('status code...'+response.getStatusCode());
        system.debug('response body...'+response.getBody());
        system.debug('response....'+response.getStatus());   
        
    } */
    
    
    
}