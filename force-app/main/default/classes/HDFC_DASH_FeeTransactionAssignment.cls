public class HDFC_DASH_FeeTransactionAssignment {
    
    @TestVisible private static List<Fees_Transaction__x> mockedRequests = new List<Fees_Transaction__x>();
    
    @future
    public static void setCaseOwner(Id caseRecId, String fileNo) {
        String ownerNameQueue;
        Boolean checkStage = HDFC_DASH_Constants.BOOLEAN_TRUE;
        List<Group> queueList = new List<Group>();
        List<Fees_Transaction__x> feeRecs = new List<Fees_Transaction__x>();
        List<Case> caseList = new List<Case>();
        caseList = [SELECT Id,ownerId,HDFC_DASH_File_Number__r.StageName,HDFC_DASH_File_Number__r.HDFC_DASH_Credit_Appraiser__c,
                    HDFC_DASH_Home_Branch__c,HDFC_DASH_Service_Centre__c FROM Case WHERE Id = :caseRecId];
        if(!Test.isRunningTest()){
            feeRecs = [SELECT Id,FILE_NO__c,TRANS_CD__c,AMT_RECD__c,AMT_RECBLE__c FROM Fees_Transaction__x 
                   WHERE FILE_NO__c = :fileNo AND TRANS_CD__c = 'PF'];
        }else{
            feeRecs = new List<Fees_Transaction__x>(mockedRequests);
        }
        
        system.debug('feeRecs.size() : ' + feeRecs.size());
        if(feeRecs.size() > 0){
            system.debug('in fees recs');
            if(feeRecs[0].AMT_RECD__c > feeRecs[0].AMT_RECBLE__c){
                checkStage = HDFC_DASH_Constants.BOOLEAN_FALSE;
                ownerNameQueue = caseList[0].HDFC_DASH_Home_Branch__c + ' Acc Dept - Exec';
                system.debug('ownerNameQueue : ' + ownerNameQueue);
            }
        }
        system.debug('caseList[0].HDFC_DASH_File_Number__r.StageName : ' + caseList[0].HDFC_DASH_File_Number__r.StageName);
        if((HDFC_DASH_Constants.Onboarding.equals(caseList[0].HDFC_DASH_File_Number__r.StageName) || HDFC_DASH_Constants.PRERHDFC.equals(caseList[0].HDFC_DASH_File_Number__r.StageName) ||
               HDFC_DASH_Constants.CREDITAPPR.equals(caseList[0].HDFC_DASH_File_Number__r.StageName)) && (feeRecs.size() <=0 || checkStage)){
                   system.debug('inside if');
                   if(caseList[0].HDFC_DASH_File_Number__r.HDFC_DASH_Credit_Appraiser__c != null){
                       caseList[0].ownerId = caseList[0].HDFC_DASH_File_Number__r.HDFC_DASH_Credit_Appraiser__c;
                   }
                   else{
                       ownerNameQueue = caseList[0].HDFC_DASH_Service_Centre__c + ' SC-E';
                       system.debug('ownerNameQueue : ' + ownerNameQueue);
                   }
        }
        else if(feeRecs.size() <=0 || checkStage){
            ownerNameQueue = caseList[0].HDFC_DASH_Service_Centre__c + ' SC-E';
            system.debug('ownerNameQueue : ' + ownerNameQueue);
        }
        if(ownerNameQueue != null){
            queueList = [SELECT Id,Name FROM GROUP WHERE Name = :ownerNameQueue];
            if(queueList.size() > 0){
                caseList[0].ownerId = queueList[0].Id;
            }
        }
        Database.update(caseList);
    }

}