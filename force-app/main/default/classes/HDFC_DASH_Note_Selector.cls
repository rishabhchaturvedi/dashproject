/**
className: HDFC_DASH_Note_Selector
DevelopedBy:Punam Marbate
Date: 28 Oct 2021
Company: Accenture
Class Description: This class would create the select queries for Note object .
**/
public with sharing class HDFC_DASH_Note_Selector {
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from Note ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
     /* 
    Method Name :getNotes
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get the List of Note based on only one condition.
    */
    public static List<Note> getNotes( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;  

        List<Note> result = database.query(querystring);        
    
        return result;
    }   
    
}