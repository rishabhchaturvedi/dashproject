/*
    Author: Anisha Arumugam
    Class: HDFC_DASH_ParseFinancialDetails
    Description: Parser Class for Financial Info API
*/
public without sharing class HDFC_DASH_ParseFinancialDetails {
    
	 /*
        Class: processFinancialDetails
        Description: Class to parse Property Details
    */
    public without sharing class PropertyDetails {
		public String projectCode;
		public String projectName;
		public Decimal propertyCost;
		public String propertyHouseIdentifier;
		public String propertyStreetNameIdentifier;
		public String propertyLandmark;
		public String propertyLocality;
		public String propertyVillageTownCity;
		public String propertySubDistrict;
		public String propertyDistrict;
		public String propertyState;
		public String propertyCountry;
		public String propertyPostalCode;
		public String propertyPostOfficeName;
		public String propertyStatus;
	}
    
	/*
        Class: FinancialInfo
        Description: Class to parse Financial Info
    */
	public without sharing class FinancialInfo {
		public Decimal fixedMonthlyIncome;
		public Decimal additionalIncome;
		public String noAdditionalIncome;
		public String salarySlipAttached;
	}
        
    /*
        Class: RiskScore
        Description: Class to parse risk score
    */
    public without sharing class RiskScore {
		public String riskScoreCustomerstatus;
		public String riskScoreCustomerflags;
		public String riskScoreRtrexists;
		public String riskScoreCustomergrade;
		public String riskScoreRiskevent;
		public String riskScoreAvailedMoratorium;
		public String riskScoreAvailedRestruct;
		public String riskScoreHdfcStaff;
		public String riskScoreConfidenceScore;
		public String riskScoreExistInNegList;
	}
    
	/*
        Class: ApplicantEmploymentDetails
        Description: Class to parse Applicant Employment Details
    */
	public without sharing class ApplicantEmploymentDetails {
		public String occupation;
		public String workEmail;
		public String employerCode;
		public String employerName;
	}
    
	public ApplicationDetails applicationDetails;

	/*
        Class: ApplicationDetails
        Description: Class to parse Application Details
    */
	public without sharing class ApplicationDetails {
		public String sfApplicationID;
		public String sfApplicationNumber;
		public String sfApplicantID;
		public String loanType;
        public String financingFor;
		public String lmsLeadID;
        public Integer leadStageNumber;
        public String leadStageDescription;
		public String preferredInterestRate;
		public String balanceTransferLoan;
		public String propertyDecided;
		public String propertyCityCode;
		public String propertyStateCode;
		public String propertyCountryCode;
		public PropertyDetails propertyDetails;
		public Decimal propertyIndicativeCost;
		public String skipEmploymentDetails;
		public ApplicantEmploymentDetails applicantEmploymentDetails;
		public FinancialInfo financialInfo;
        public RiskScore riskScore;
        public Boolean riskScoreTechnicalFailure;
		public Integer confidenceScore;
        public Boolean confidenceScoreTechnicalFailure;
		public string experianCibilID;
        public Boolean experianCibilTechnicalFailure;
	}
    
	/*
        Method: parse
        Description: Method to Deserialize Financial Info API request body
    */
	public static HDFC_DASH_ParseFinancialDetails parse(RestRequest request) {
        string json = request.requestBody.toString();
		return (HDFC_DASH_ParseFinancialDetails) System.JSON.deserialize(json, HDFC_DASH_ParseFinancialDetails.class);
	}
}