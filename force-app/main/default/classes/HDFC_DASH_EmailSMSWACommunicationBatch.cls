/**
* 	className: HDFC_DASH_EmailSMSWACommunicationBatch
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class would get all the Applications for which HDFC_DASH_EmailSMSWACommunicationBatch communications need to be sent.
**/
global class HDFC_DASH_EmailSMSWACommunicationBatch implements  database.Stateful,database.Batchable<Sobject>,Database.AllowsCallouts{
    public static Map<String,EmailTemplate> emailTemplateMap = new Map<String,EmailTemplate>();
    public static Map<String,Id> orgWideEmailAddressMap = new Map<String,Id>(); 
    /*  
Method Name :start
Parameters  :Database.BatchableContext BC 
Description :This method would query the list of Applications where Latest Status Change Date Time > 24 hours.
*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collecting the batches of records or objects to be passed to execute
        //-24 need to be in custom metadata or label
        Integer communicationTime = (Integer) Communication_Time_Setting__mdt.getInstance(HDFC_DASH_Constants.HDFC_DASH_Communication_Email_Time).HDFC_DASH_Communication_Email_TimeFrame__c;
        DateTime twentyFourHours = System.now().addHours(communicationTime);
        
        return Database.getQueryLocator([SELECT ID,HDFC_DASH_Status__c,HDFC_DASH_Latest_Status_Change_Date_Time__c,stageName,HDFC_DASH_Pre_Approved_Loan_Flag__c,
                HDFC_DASH_Consent_For_Term_And_Condition__c,HDFC_DASH_Second_File_Push_To_ILPS__c 
                FROM Opportunity WHERE stageName = :HDFC_DASH_Constants.ONBOARDING AND HDFC_DASH_Latest_Status_Change_Date_Time__c <= :twentyFourHours]); 
    }
    /* 
Method Name :execute
Parameters  :Database.BatchableContext BC, List<HDFC_DASH_Application_Payment_Details__c> payDetList
Description :This method make callouts to Billdesk and return the current AuthStatus.
*/ 
    global void execute(Database.BatchableContext BC, List<Opportunity> appList) {
        try {
            // processing each batch of records
            Map<String,List<Opportunity>> oppListForEmailcomm = new Map<String,List<Opportunity>>();
            List<Id> appRecPreApproved = new List<Id>();
            for(Opportunity appRec:appList){
                //if(String.isNotBlank(appRec.HDFC_DASH_Status__c)){
                    //system.debug('appRec.HDFC_DASH_Status__c'+appRec.HDFC_DASH_Status__c);
                    //system.debug('oppEmail '+oppListForEmailcomm.containsKey(appRec.HDFC_DASH_Status__c));
                    //system.debug('oppEmail preapprovedLoanFlag  '+ !appRec.HDFC_DASH_Pre_Approved_Loan_Flag__c);
                    if(String.isNotBlank(appRec.HDFC_DASH_Status__c) && oppListForEmailcomm.containsKey(appRec.HDFC_DASH_Status__c) 
                    && oppListForEmailcomm.get(appRec.HDFC_DASH_Status__c) != NULL && !appRec.HDFC_DASH_Pre_Approved_Loan_Flag__c ){
                        List<Opportunity> appRecList = oppListForEmailcomm.get(appRec.HDFC_DASH_Status__c);
                        appRecList.add(appRec);
                        oppListForEmailcomm.put(appRec.HDFC_DASH_Status__c,appRecList); 
                    } 
                    else if(String.isNotBlank(appRec.HDFC_DASH_Status__c) && !appRec.HDFC_DASH_Pre_Approved_Loan_Flag__c 
                    && !appRec.HDFC_DASH_Second_File_Push_To_ILPS__c) {
                        //system.debug('appRec===== nor pte approved loan');
                        oppListForEmailcomm.put(appRec.HDFC_DASH_Status__c ,new List<Opportunity> {appRec});
                        //system.debug('oppListForEmailcomm'+oppListForEmailcomm);
                    }  
                    //System.debug('xxx preaproved loan flag ' + appRec.HDFC_DASH_Pre_Approved_Loan_Flag__c);
                    //System.debug('xxx Consent_For_Term_And_Condition__c' + appRec.HDFC_DASH_Consent_For_Term_And_Condition__c);
                    if(String.isNotBlank(appRec.HDFC_DASH_Status__c) && appRec.HDFC_DASH_Pre_Approved_Loan_Flag__c 
                    && !appRec.HDFC_DASH_Consent_For_Term_And_Condition__c){
                        appRecPreApproved.add(appRec.Id);
                    }
                //}
            }
            //system.debug('oppListForEmailcomm===='+oppListForEmailcomm);
             //system.debug('appRecPreApproved===='+appRecPreApproved);
            
            list<string> orgwidefieldlist = new list<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.DISPLAYNAME,HDFC_DASH_Constants.ADDRESS};
                list<OrgWideEmailAddress> orgEmailAdd = HDFC_DASH_OrgWideEmailAddress_Selector.getOrgWideEmailAddress(orgwidefieldlist);
            
            list<string> emailTempfieldlist = new list<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.DEVELOPERNAME,HDFC_DASH_Constants.STRING_Name,
                HDFC_DASH_Constants.FOLDER_DEVNAME, HDFC_DASH_Constants.SUBJECT};
            Map<String,String> fieldsAndparameters= new Map<string,String>{HDFC_DASH_Constants.FOLDER_DEVNAME => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.HDFC_DASH_LOS_Email_Template + 
                    HDFC_DASH_Constants.STRING_QUOTE};
            List<EmailTemplate> emailTempList = HDFC_DASH_EmailTemplate_Selector.getEmailTemplateBasedOnQuery(emailTempfieldlist,fieldsAndparameters);
           // System.debug('email templates ' + emailTempList);
            //get all the HDFC_DASH_Communication__mdt records
            Map<String, HDFC_DASH_Communication__mdt> communicationMetadata = HDFC_DASH_Communication__mdt.getAll();
            if(emailTemplateMap.isEmpty()){
                for(EmailTemplate emailTemp : emailTempList){
                    emailTemplateMap.put(emailTemp.Name, emailTemp);
                }
            }
            
            if(orgWideEmailAddressMap.isEmpty()){	
                for(OrgWideEmailAddress orgWideEmailAdd : orgEmailAdd ){
                    //[SELECT Id, DisplayName, Address FROM OrgWideEmailAddress]
                    
                    orgWideEmailAddressMap.put(orgWideEmailAdd.Address, orgWideEmailAdd.id);
                }
            } 
            //system.debug('emailTemplateMap=='+emailTemplateMap);
            //system.debug('orgWideEmailAddressMap=='+orgWideEmailAddressMap);
            //Call Helper class
            if(oppListForEmailcomm.size()>0){
                HDFC_DASH_Communication_Helper.sendCommunication(communicationMetadata,emailTemplateMap,orgWideEmailAddressMap,oppListForEmailcomm);
            }
            if(appRecPreApproved.size()>0){
            HDFC_DASH_Communication_Helper.sendCommunicationForPreApproved(communicationMetadata,
                                            appRecPreApproved, emailTemplateMap, orgWideEmailAddressMap);
            }
        } catch(Exception e) {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString() 
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Application,
                                                        HDFC_DASH_Constants.EMAICOMMUNICATIONBATCHAPP,NULL,expDetails);
        }
        
    }   
    /* 
Method Name :finish
Parameters  :Database.BatchableContext BC
Description :
*/
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email 
        
    }
}