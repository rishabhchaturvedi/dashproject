/**
className: HDFC_DASH_CADTriggerOps
DevelopedBy: Raghavendra
Date: 14 September 2021
Company: Accenture
Class Description: This Class contains the trigger operation on ContactAddressDetails object.
**/
public class HDFC_DASH_CADTriggerOps {
/**
* 	className: UpdateContactAddressDetailsLatestFlag
DevelopedBy: Sai Shruthi
Date: 26 May 2021
Company: Accenture
Class Description: This Class contains the trigger operation for prosessing latest flag after insert
**/
    public with sharing class UpdateContactAddressDetailsLatestFlag implements HDFC_DASH_TriggerOps{
        /*
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.contactAddressDetailsIsFirstRun;
        }
        /*
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<HDFC_DASH_Contact_Address_Details__c> conAddDetRecList = HDFC_DASH_CADTriggerOpsHelper.filterCADforLatestFlagSenario();
            return conAddDetRecList;
        }
        /*
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Sobject[] conAddDetRecList){
            HDFC_DASH_CADTriggerOpsHelper.updateConAddDetRec(conAddDetRecList);
            //HDFC_DASH_CADTriggerOpsHelper;
            HDFC_DASH_TriggerOpsRecursionFlags.contactAddressDetailsIsFirstRun = false;
        }
    }
/**
* 	className: changeCADLatestFlagBeforeUpdate
DevelopedBy: Sai Shruthi
Date: 26 May 2021
Company: Accenture
Class Description: This Class contains the trigger operation for prosessing latest flag before insert & update
**/
     public with sharing class changeCADLatestFlagBeforeUpdate implements HDFC_DASH_TriggerOps{
        /*
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.contactAddressDetailsIsFirstRun;
        }
        /*
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
  List<HDFC_DASH_Contact_Address_Details__c> conAddDetRecList = HDFC_DASH_CADTriggerOpsHelper.filterCADWhereConIsPopulated();
            return conAddDetRecList;
        }
        /*
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Sobject[] conAddDetRecList){
            HDFC_DASH_CADTriggerOpsHelper.changeConAddDetRecBeforeUpdate(conAddDetRecList); 
        }
    }
}