/**
className: HDFC_DASH_EmailMessage_SelectorTest
DevelopedBy: Utkarsh Patidar
Date: 14 Dec 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_EmailMessage_Selector class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_EmailMessage_SelectorTest 
{
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static EmailMessage emailMessage = HDFC_DASH_TestDataFactory_EmailMessage.getBasicEmailMes(app.id);
    private static final string SUCCESS ='Success';
    
    /* 
Method Name :testGetEmailMessage
Description :This method will test the method getEmailMessage().
*/
    static testmethod void testGetEmailMessage()
    {
        List<EmailMessage> resEmailMessageRec = new List<EmailMessage>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.RELATED_TO_ID};
                Test.startTest();   
            resEmailMessageRec = HDFC_DASH_EmailMessage_Selector.getEmailMessage(new List<String> {emailMessage.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, resEmailMessageRec);
            system.assertEquals(1,resEmailMessageRec.size(),SUCCESS);  
        }
    }
    
    /* 
Method Name :testGetEmailMessageBasedOnQuery
Description :This method will test the method getEmailMessageBasedOnQuery().
*/
    static testmethod void testGetEmailMessageBasedOnQuery()
    {
        List<EmailMessage> listEmailMessageRec  = new List<EmailMessage>();
        System.runAs(sysAdmin)
        {     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.RELATED_TO_ID};
                Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.Id_STRING =>
                    HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                    emailMessage.Id+HDFC_DASH_Constants.STRING_QUOTE};
                        Test.startTest();
            listEmailMessageRec = HDFC_DASH_EmailMessage_Selector.getEmailMessageBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listEmailMessageRec);
        system.assertEquals(1, listEmailMessageRec.size(),SUCCESS);       
    }
}