/**
* 	className: HDFC_DASH_ContentDocument_Selector
DevelopedBy: Tejeswari
Date: 19 October 2021
Company: Accenture
Class Description: This class would create the select queries for Content Document object .
**/
public inherited sharing class HDFC_DASH_ContentDocument_Selector 
{
    private static final string FROM_STRING = ' from ';
    private static final string OBJECT_CONTENTVERSION = 'ContentVersion ';
    private static final string OBJECT_CONTENTDOCUMENTLINK = 'ContentDocumentLink ';
    private static final string OBJECT_CONTENTDOCUMENT = 'ContentDocument ';
    private static final string ID_STRING = 'ID ';
    private static final string SELECT_STRING = 'Select ';
   	private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    public static final string  QUERY_LIMIT = ' Limit';
   
     /* 
Method Name :getContentVersion
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of ContentVersion based on the condition.
*/
    public static List<ContentVersion> getContentVersion( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_CONTENTVERSION + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<ContentVersion> listContentVersion = database.query(querystring);          
        return listContentVersion;
    }  
  /* 
Method Name :getContentDocumentLink
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of getContentDocumentLink based on the condition.
*/  
    public static List<ContentDocumentLink> getContentDocumentLink( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_CONTENTDOCUMENTLINK + WHERE_STRING + conditionOn + HDFC_DASH_Constants.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(queryParameters, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<ContentDocumentLink> listContentDocumentLink = database.query(querystring);            
       	return listContentDocumentLink;
    } 
 /* 
Method Name :getContentDocument
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of getContentDocument based on the condition.
*/     
/*public static List<ContentDocument> getContentDocument( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
		string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_CONTENTDOCUMENT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<ContentDocument> listContentDocument = database.query(querystring);            
        return listContentDocument;
    } */

    /* 
Method Name :getContentVersionBasedOnQuery
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
Description :This method would get the List of ContentVersion based on fieldsAndparameters sent to it.
*/ 
    public static List<ContentVersion> getContentVersionBasedOnQuery(List<String> fieldList, Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_CONSTANTS.COMMA) + FROM_STRING + OBJECT_CONTENTVERSION + WHERE_STRING +getCondition(fieldsAndparameters);
      
        List<ContentVersion> listConVer = database.query(querystring); 
        
		return listConVer;   
    }
  /* 
Method Name :getCondition
Parameters  :Map<String,String> fieldsAndparameters
Description :This method would be called from getContentVersionBasedOnQuery .
*/    
	public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    } 
}