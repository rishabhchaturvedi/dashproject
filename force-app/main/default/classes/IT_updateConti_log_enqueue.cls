/* ================================================
 * @Class Name :  IT_updateConti_log_enqueue
 * @author : Accenture
 * @Purpose: This purpose of this class is to execute method updateContunationResponseHandler of InterfaceHandler.
 * @created date:
 ================================================*/
public with sharing class IT_updateConti_log_enqueue implements Queueable {
    
    private String interfaceName;
    private Integration_Message_Log__c integrationLogObj;
    private string response;
    private String statusCode;
    
    /***************************************************************************************************************
    *   @Name        :  IT_updateConti_log_enqueue                                                              
    *   @Parameters  :  Integration_Message_Log__c integrationLogObj,String interfaceName, String response,String statusCode                                                                         
    *   @Description :  It is a constructor for class IT_updateConti_log_enqueue.                             
    ***************************************************************************************************************/
    public IT_updateConti_log_enqueue(Integration_Message_Log__c integrationLogObj,
                                        String interfaceName, String response,
                                        String statusCode){
        this.interfaceName = interfaceName;
        this.integrationLogObj = integrationLogObj;
        this.response = response;
        this.statusCode = statusCode;
    }
     /***************************************************************************************************************
    *   @Name        :  Execute                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  updateContunationResponseHandler method of InterfaceHandler to be called in execute method.                              
    ***************************************************************************************************************/
    public void execute(QueueableContext context) {
        InterfaceHandler.updateContunationResponseHandler(integrationLogObj,interfaceName,response,statusCode);    
    }
}