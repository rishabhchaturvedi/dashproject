/**
className: HDFC_DASH_RestResource_GetAppTest
DevelopedBy: Punam Marbate
Date: 17 Aug 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_GetApplication.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_RestResource_GetAppTest{
    //getting object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string STRING_POST ='GET';
    private static final string STRING_URL ='https://hdfclimited--hdfcdev.my.salesforce.com/services/apexrest/hdfc/getApplication/V1.0/';
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    /* 
    Method Name : testGetApplication
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_GetApplication.getApplication().
    */
    static testmethod void testGetApplication(){
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL + app.id;
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            //request.requestBody = Blob.valueOf(REQUEST_BODY);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
                HDFC_DASH_RestResource_GetApplication.getApplication();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    /* 
    Method Name :testGetApplicationException
    Parameters  :
    Description :This method is for error senario would call HDFC_DASH_RestResource_GetApplication.getApplication().
    */
    static testmethod void testGetApplicationException(){
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        Exception testException = Null;
        RestRequest request = new RestRequest();
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
        request.requestUri = STRING_URL;      
        RestContext.request = request;

        test.startTest();
        System.runAs(sysAdmin)
        {
            try
            {
                HDFC_DASH_RestResource_GetApplication.getApplication();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            system.assertNotEquals(null, testException);
        }   
    }   
}