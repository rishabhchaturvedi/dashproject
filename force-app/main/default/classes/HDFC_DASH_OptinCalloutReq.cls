/*
Class:HDFC_DASH_OptinCalloutReq
Author: Tejeswari
Description: This class will generate the request body for OptinCallout
*/
public class HDFC_DASH_OptinCalloutReq {
    
    
    public String type;
    public List<Recipients> recipients;
   
    /*
Class: Recipients
Description: Class to parse recipients
*/
    public class Recipients {
        public String recipient;
        public String source;

    }
    
}