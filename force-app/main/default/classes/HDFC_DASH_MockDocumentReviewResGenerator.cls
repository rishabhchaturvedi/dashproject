/**
className: HDFC_DASH_MockDocumentReviewResGenerator
Author: Janani Mohankumar
Date: 29 Oct 2021
Company: Accenture
Class Description: This class will generate a mock response for Min Mand Docs callout.
**/ 
@isTest(SeeAllData=false)
global with sharing class HDFC_DASH_MockDocumentReviewResGenerator implements HttpCalloutMock {
    
   /* 
Method Name :respond
Parameters  :HTTPRequest req
Description :This method would create a fake response for Min Mand Docs callout.
*/
    global HTTPResponse respond(HTTPRequest req) 
    {        
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');  
        res.setBody('{"listOfDocs":[{"sfapplicantno":1612344,"doccode":"ANNEX_SHEET","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"APPLFORM","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"EXT_PANCRD","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"FEESCHQ1","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"FILE_ORG_ST","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"KYC_ADDR1","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"KYC_ID1","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"SALSLP1","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612344,"doccode":"BK_ST","dockey4val":null,"dockey5val":"12334","docfromdate":"May-21","doctodate":"Oct-21","mindoc":null,"manddoc":"Y"},{"sfapplicantno":1612359,"doccode":"EXT_PANCRD","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612359,"doccode":"ITR_OVERSEAS","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612359,"doccode":"KYC_ADDR1","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612359,"doccode":"KYC_ID1","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612359,"doccode":"PASSPORT","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null},{"sfapplicantno":1612359,"doccode":"SALSLP1","dockey4val":null,"dockey5val":null,"docfromdate":null,"doctodate":null,"mindoc":"Y","manddoc":null}],"errorCode":0,"errorMsg":"Success"}');
        res.setStatusCode(200);
        system.assertNotEquals(null, req);
        
        return res;
    }
}