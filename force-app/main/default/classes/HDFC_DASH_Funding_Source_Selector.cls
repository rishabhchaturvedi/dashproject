/**
className: HDFC_DASH_Funding_Source_Selector
DevelopedBy:Janani Mohankumar
Date: 22 Sep 2021
Company: Accenture
Class Description: This class would create the select queries for Funding Source object .
**/
public inherited sharing class HDFC_DASH_Funding_Source_Selector {
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Funding_Source__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    
     
  /* 
    Method Name :getFundingSource
    Parameters  :List<String> queryParameters, List<string> fieldList, Id fundingSourceId
    Description :This method would get a list of Funding Source based on only one condition.
    */   
    /*public static List<HDFC_DASH_Funding_Source__c> getFundingSource(List<String> queryParameters,List<string> fieldList, String conditionOn){
            
       string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<HDFC_DASH_Funding_Source__c> listFundingSource = database.query(querystring); 
        return listFundingSource;
    }*/
    
    /* 
    Method Name :getFundingSourceBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Funding Source  based on fieldsAndparameters sent to it.
    */
    public static List<HDFC_DASH_Funding_Source__c> getFundingSourceBasedOnQuery(List<String> fieldList,
                                                            Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +
                                FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<HDFC_DASH_Funding_Source__c> fundingSourceList = database.query(querystring); 
        return fundingSourceList;   
    }
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getFeeDetailsBasedOnQuery .
    */
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }   

}