/* ================================================
* @Class Name :  InterfaceCallOutProcess
* @author : Accenture
* @Purpose: The Purpose of this class is to define different methods for interface callout Process.
* @created date:
================================================*/
public virtual class InterfaceCallOutProcess extends InterfaceLogDefination{
    
    private Boolean errorInCalloutProcess;
    private static Map<interface_settings__c,List<Integration_Message_Log__c>> multiIntegrationLogMap; 
    public static Map<String,interface_settings__c> interfaceCSstaticMap;
    public static Map<string,interface_Framework_Settings__c> staticMapInterfaceFramSt;
    public boolean commulticalloutlog = false;
    public boolean skipCustomSettingEndPoint = false;
    public string overriddenEndPoint;
    public Integration_Message_Log__c objIntMsgLog;
    /***************************************************************************************************************
    *   @Name        :  InterfaceCallOutProcess                                                              
    *   @Parameters  :  None                                                                              
    *   @Description :  It is a constructor for class InterfaceCallOutProcess                             
    ***************************************************************************************************************/
    public InterfaceCallOutProcess(){
        errorInCalloutProcess = false;
        if(multiIntegrationLogMap == null){
            multiIntegrationLogMap = new Map<interface_settings__c,List<Integration_Message_Log__c>>();
        }
        if(interfaceCSstaticMap == null){
            interfaceCSstaticMap = InterfaceHandler.RetriveInterfaceSetting();
        }
        if(staticMapInterfaceFramSt == null){
            staticMapInterfaceFramSt = interface_Framework_Settings__c.getAll();
        }
    }
    /***************************************************************************************************************
    *   @Name        :  doCallOut                                                              
    *   @Parameters  :  String  intefaceName,string methodType,blob reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId                                                                                
    *   @Return      :  HttpResponse
    *   @Description :  This method is used to call doCallout.                         
    ***************************************************************************************************************/
    public override HttpResponse doCallOut(String  intefaceName,string methodType,
                                            blob reqBody,map<string,string> headerMap,
                                            string PostRetryProcess,string messageId){
        return doCallOut(intefaceName,methodType,EncodingUtil.base64Encode(reqBody),headerMap,PostRetryProcess,messageId);
    }
    /***************************************************************************************************************
    *   @Name        :  doCallOut                                                              
    *   @Parameters  :  String  intefaceName,string methodType,blob reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId                                                                                
    *   @Return      :  HttpResponse                            
    ***************************************************************************************************************/
    public override HttpResponse doCallOut(String  intefaceName,string methodType,
                                        string reqBody,map<string,string> headerMap,
                                        string PostRetryProcess,string messageId){
        interface_settings__c objIntSetting;
        IntegrationLogstructure objIntegrationLogStr = new IntegrationLogstructure();
        HttpResponse hrs;
        boolean isError = false;
        boolean isErrorCodeMatch = false;
        DateTime requestTime;
        DateTime responseTime;
        try{
            if(skipCustomSettingEndPoint)
            {
                InterfaceHandler.skipCustomSettingEndPoint = true;
                InterfaceHandler.overriddenEndPoint = overriddenEndPoint;
            }
            InterfaceHandler.preCalloutLogUpdates(objIntegrationLogStr, reqBody, headerMap, methodType, PostRetryProcess,messageId);
            Boolean doRetry = false;            
            Integer loopCount = 0;
            Map<String,interface_settings__c> interfaceCSMap = InterfaceHandler.RetriveInterfaceSetting();
            Map<string,interface_Framework_Settings__c> mapInterfaceFramSt = interface_Framework_Settings__c.getAll();
            
            if(interfaceCSMap <>null){
               
                objIntSetting = interfaceCSMap.get(intefaceName.trim().toLowerCase());
            }
            if(mapInterfaceFramSt <> null && mapInterfaceFramSt.values().size()>0 &&  
                                    !mapInterfaceFramSt.values()[0].Stop_All_CallOuts__c){
                if(objIntSetting <> null){
                    if(!objIntSetting.No_Callout__c ){
                        doRetry = objIntSetting.Immediate_Retry__c;
                        objIntegrationLogStr.errorDescription = '';
                        Http htp = new Http();
                        do{ 
                            loopCount++;
                            requestTime = system.now();
                            if(!objIntSetting.Request_Body_As_Blob__c){
                                hrs = htp.send(InterfaceHandler.generateHttpRequest(methodType,reqBody,
                                                                            objIntSetting,headerMap));
                            }else{
                                hrs = htp.send(InterfaceHandler.generateHttpRequest(methodType,EncodingUtil.base64Decode(reqBody),
                                                                                            objIntSetting,headerMap));
                            }
                            responseTime = system.now();
                            objIntegrationLogStr.strRresponse=hrs.getBody();
                            objIntegrationLogStr.statusCode=string.valueof(hrs.getStatusCode());
                            objIntegrationLogStr.noOfRetry=loopCount;
                            //system.debug('decision '+
                            // !InterfaceHandler.interfaceErrorCodes(objIntSetting ).contains(string.valueOf(hrs.getStatusCode()).trim()));
                            if(!InterfaceHandler.interfaceErrorCodes(objIntSetting ).contains(string.valueOf(hrs.getStatusCode()).trim())){ 
                                isErrorCodeMatch =true; 
                            }
                        }while(doRetry && isErrorCodeMatch  && loopCount < 2 && 
                                    InterfaceHandler.objIntegrationMsgLog == null && 
                                    objIntSetting.Number_of_Retry__c > 1);
                    }else{
                        isError =true; 
                        errorInCalloutProcess = true;
                        InterfaceHandler.logForRespectiveIntLocked(objIntegrationLogStr);                        
                    }
                }else{
                    isError =true;
                    errorInCalloutProcess = true;
                    InterfaceHandler.logForInvalidInterface(objIntegrationLogStr);                      
                }
            }else{
                errorInCalloutProcess = true;
                InterfaceHandler.logForAllInterfacesLocked(objIntegrationLogStr);
            }
        }catch(CalloutException ex){
            isError =true;
            errorInCalloutProcess = true;
            system.debug('hrs'+ hrs);
            InterfaceHandler.logForCalloutException(objIntegrationLogStr,ex);            
        }catch(exception ex){
            isError =true;
            errorInCalloutProcess = true;
            InterfaceHandler.logForApexErrorInCallout(objIntegrationLogStr,ex);            
        }
        
        // Immediate Email incase callout exceeds threshold time
        InterfaceHandler.updateCalloutTime(objIntSetting,objIntegrationLogStr,requestTime,responseTime,isError);
        objIntMsgLog = InterfaceHandler.createInterfaceLog(objIntSetting, objIntegrationLogStr);
        system.debug('Test objIntMsgLog => '+ objIntMsgLog);
                                            
        InterfaceHandler.callRetryProcessClass(hrs,objIntSetting,objIntMsgLog);
        if(objIntSetting != null){
            InterfaceHandler.createLogHistory(objIntMsgLog);
        }
        if(InterfaceHandler.objIntegrationMsgLog == null && objIntSetting.Immediate_email__c && (isError || isErrorCodeMatch)){
            System.enqueueJob(new IT_immediate_email_enqueue(objIntMsgLog,objIntSetting));
        }
        InterfaceHandler.sendThresholdBreachMail(objIntSetting,objIntMsgLog);
        if(hrs != null){
            return hrs;
        }else{
            return null;
        }
    }
    /***************************************************************************************************************
    *   @Name        :  logContinuationCallout                                                              
    *   @Parameters  :  String interfaceName,string methodType,string reqBody,map<string,string> headerMap,string PostRetryProcess,string continuationId,string messageId                                                                               
    *   @Return      :  Void
    *   @Description :  This method is used to log continuation Callout.                           
    ***************************************************************************************************************/
    public override void logContinuationCallout(String interfaceName,string methodType,string reqBody,
                                                map<string,string> headerMap,string PostRetryProcess, 
                                                string continuationId,string messageId){
        InterfaceHandler.createContinuationLogHandler(interfaceName,continuationId,methodType,reqBody,
                                                                headerMap,PostRetryProcess,messageId);
    } 
    /***************************************************************************************************************
    *   @Name        :  logContinuationCallout                                                              
    *   @Parameters  :  String intefaceName,string methodType,blob reqBody,map<string,string> headerMap,string PostRetryProcess,string continuationId,string messageId                                                                              
    *   @Return      :  Void
    *   @Description :  This method is used to log continuation Callout.                             
    ***************************************************************************************************************/
    public override void logContinuationCallout(String intefaceName,string methodType,
                        blob reqBody,map<string,string> headerMap,string PostRetryProcess,
                        string continuationId,string messageId){
        logContinuationCallout(intefaceName,methodType,
                        EncodingUtil.base64Encode(reqBody),
                    headerMap,PostRetryProcess,continuationId,messageId);
    }
    /***************************************************************************************************************
    *   @Name        :  doManualRetry                                                              
    *   @Parameters  :  Integration_Message_Log__c objIntMsgLog
    *   @Return      :  Boolean
    *   @Description :  This method is used for manual retry.                             
    ***************************************************************************************************************/
    public Boolean doManualRetry(Integration_Message_Log__c objIntMsgLog){
        Map<String,interface_settings__c> interfaceCSMapTemp = interface_settings__c.getAll(); 
        Map<String,interface_settings__c> interfaceCSMap = new Map<String,interface_settings__c>();
        DateTime requestTime;
        DateTime responseTime; 
        HttpResponse hrs;
        interface_settings__c objInterfaceSt ;
        Boolean errorFlag = false;
        try{
            if(interfaceCSMapTemp<>null && !interfaceCSMapTemp.keyset().isEmpty()){
                for(string str :interfaceCSMapTemp.keyset()){
                    interfaceCSMap.put(str.trim().tolowercase(),interfaceCSMapTemp.get(str));
                }
            }
            
            objInterfaceSt = interfaceCSMap.get(objIntMsgLog.Interface_Name__c.trim().tolowercase());
            objIntMsgLog.Number_Of_Retries__c += 1;
            objIntMsgLog.Response_Body__c = '';
            objIntMsgLog.Status_Code__c = '';
            if( objInterfaceSt <> null){
                objIntMsgLog.Error_Description__c = '';
                InterfaceCallOutProcess objInterfCallOutPro= new InterfaceCallOutProcess();
                map<string,string> headermp = new map<string,string>();
                map<string,object> headerObjectMap = new map<string,string>();
                if(String.isNotBlank(objIntMsgLog.Header_Map__c) && 
                                            objIntMsgLog.Header_Map__c <> null && 
                                            objIntMsgLog.Header_Map__c <> 'null'){
                    headerObjectMap = (Map<String, object>) JSON.deserializeUntyped(objIntMsgLog.Header_Map__c);
                    for(string objstr : headerObjectMap.keyset()){
                        headermp.put(objstr,(string)headerObjectMap.get(objstr));
                    }
                }
                Http htp = new Http();
                requestTime = system.now();
                if(!objInterfaceSt.Request_Body_As_Blob__c){
                    hrs = htp.send(InterfaceHandler.generateHttpRequest(objIntMsgLog.Method_Type__c,
                                                objIntMsgLog.Request_Body__c,objInterfaceSt,headermp));
                }else{
                    hrs = htp.send(InterfaceHandler.generateHttpRequest(objIntMsgLog.Method_Type__c,
                                                EncodingUtil.base64Decode(objIntMsgLog.Request_Body__c),
                                                objInterfaceSt,headermp));
                }
                responseTime = system.now();
                objIntMsgLog.Response_Body__c=hrs.getBody();
                
                objIntMsgLog.Status_Code__c = string.valueof(hrs.getStatusCode()); 
                
                objIntMsgLog.Callout_Date__c = system.now();
                objIntMsgLog.Apex_Error__c = false;
                if(string.isNotBlank(string.valueOf(responseTime))){
                    objIntMsgLog.Calout_Time__c = ((responseTime.getTime())/1000) - ((requestTime.getTime())/1000);
                }
            }else{
                objIntMsgLog.Invalid_Custom_Setting__c = true;
            }
        }catch(CalloutException ex){
            objIntMsgLog.Error_Description__c = ex.getmessage()+ex.getLineNumber();
            errorFlag = true;
        }catch(Exception Ex){
            objIntMsgLog.Error_Description__c = ex.getmessage()+ex.getLineNumber();
            objIntMsgLog.Apex_Error__c = true;
            errorFlag = true;
        }
        update objIntMsgLog;
        InterfaceHandler.sendThresholdBreachMail(objInterfaceSt,objIntMsgLog);
        System.enqueueJob(new IT_integration_log_enqueue(objIntMsgLog));
        if(objInterfaceSt != null && objInterfaceSt.Retry_Process_Class__c <> null && hrs <> null){
            InterfaceHandler.callRetryProcessClass(hrs,objInterfaceSt,objIntMsgLog);
        } 
        return errorFlag;
    }
    /***************************************************************************************************************
    *   @Name        :  createInboundIntegrationLog                                                              
    *   @Parameters  :  String interfaceName, String requestBody, 
                                string response, String methodType,string messageId, 
                                Exception Ex, map<string,string> requestHeader
    *   @Return      :  void
    *   @Description :  This method is used for Inbound Integration log.                            
    ***************************************************************************************************************/
    public static void createInboundIntegrationLog(String interfaceName, String requestBody, 
                                string response, String methodType,string messageId, 
                                Exception Ex, map<string,string> requestHeader){
        Integration_Message_Log__c inboundLogObj = new Integration_Message_Log__c();
        List<Integration_Message_Log__c> parentLogObj;
        inboundLogObj.recordTypeId = Schema.SObjectType.Integration_Message_Log__c.getRecordTypeInfosByName().get(
                                            'Inbound Messages').getRecordTypeId();
        interface_settings__c objIntSetting;
        interface_settings__c parentObjIntSetting;
        Map<String,interface_settings__c> interfaceCSMap = InterfaceHandler.RetriveInterfaceSetting();
        //Map<string,interface_Framework_Settings__c> mapInterfaceFramSt = interface_Framework_Settings__c.getAll();
        string requestHeaders = JSON.serialize(requestHeader);
        try{
            if(interfaceCSMap <>null){
                objIntSetting = interfaceCSMap.get(interfaceName.trim().toLowerCase());
                if(objIntSetting != null && objIntSetting.Parent_Interface_Name__c != null){
                    parentObjIntSetting = interfaceCSMap.get(objIntSetting.Parent_Interface_Name__c.trim().toLowerCase());
                }
            }
            if(objIntSetting != null){
                if(parentObjIntSetting != null && parentObjIntSetting.Fire_And_Forget__c){
                    parentLogObj = new List<Integration_Message_Log__c>();
                    InterfaceHandler.fireAndForgetResponseLog(objIntSetting,messageId,parentLogObj,inboundLogObj);
                    populateInboundLog( interfaceName, requestBody, methodType, response,  requestHeaders,  messageId,  ex,  inboundLogObj);
                    if(!parentLogObj.isEmpty()){
                        update parentLogObj;
                    }
                    
                }else{
                    populateInboundLog( interfaceName, requestBody, methodType, response,  requestHeaders,  messageId,  ex,  inboundLogObj);
                    
                }
            }
            
        }catch(exception e){
            
            String exdetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON +
                                e.getMessage()+ HDFC_DASH_Constants.SEMICOLON +
                                e.getStackTraceString()
                        + HDFC_DASH_Constants.SEMICOLON +e.getTypeName() ;  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INT_MSGE_LOG_OBJECT,
                                                HDFC_DASH_Constants.EXC_ON_INT_MSG_LOG,'',exdetails);
            //logForApexErrorInCallout(objIntegrationLogStr,ex); 
        }
    }
    /***************************************************************************************************************
    *   @Name        :  populateInboundLog                                                              
    *   @Parameters  :  string interfaceName,string requestBody,string methodType,string response, string requestHeaders,string messageId, exception ex,Integration_Message_Log__c inboundLogObj)
    *   @Return      :  void
    *   @Description :  This method is used to populate Inbound log.                            
    ***************************************************************************************************************/
    private static void populateInboundLog(string interfaceName,string requestBody,
                                    string methodType,string response, string requestHeaders, 
                                    string messageId, exception ex, 
                                    Integration_Message_Log__c inboundLogObj){
        inboundLogObj.Callout_Date__c = system.now();
        inboundLogObj.Interface_Name__c= interfaceName;
        inboundLogObj.Request_Body__c= requestBody; 
        inboundLogObj.Method_Type__c= methodType;
        inboundLogObj.Response_Body__c= response;
        inboundLogObj.Header_Map__c = requestHeaders;
        inboundLogObj.Message_Id__c = messageId;
        if(ex != null){
            inboundLogObj.Error_Description__c= ex.getMessage();
            inboundLogObj.Apex_Error__c = true;
        }
        
        insert inboundLogObj;

    }
    /***************************************************************************************************************
    *   @Name        :  doCallOutLogging                                                              
    *   @Parameters  :  String intefaceName,HttpRequest request,HttpResponse  response,string PostRetryProcess,string messageId,DateTime requestTime,DateTime responseTime
    *   @Return      :  void
    *   @Description :  This method is used for do callout for logging.                             
    ***************************************************************************************************************/
    public static void doCallOutLogging(String intefaceName,HttpRequest request,HttpResponse  response,
                                        string PostRetryProcess,string messageId,
                                        DateTime requestTime,DateTime responseTime){
        String reqBody = InterfaceHandler.getRequestBodyAsString(request);
        boolean isErrorCodeMatch = false;
        map<string,string> headerMap = new map<string,string>();
        headerMap = InterfaceHandler.getRequestHeader(response);
        String methodType = InterfaceHandler.getRequestMethodType(request);
        interface_settings__c objIntSetting ;
        IntegrationLogstructure objIntegrationLogStr = new IntegrationLogstructure();
        
        // Get respective interface setting data
        Map<String,interface_settings__c> interfaceCSMap = InterfaceHandler.RetriveInterfaceSetting();
        //Map<string,interface_Framework_Settings__c> mapInterfaceFramSt = interface_Framework_Settings__c.getAll();
        
        if(interfaceCSMap != null){
            objIntSetting = interfaceCSMap.get(intefaceName.trim().toLowerCase());
        }
        
        if(String.isNotBlank(methodType) && methodType.equalsIgnoreCase('GET')){
            reqBody = InterfaceHandler.getRequestEndPoint(request);
        }
            
        try{
            // pre callout updates on lo integration log
            InterfaceHandler.preCalloutLogUpdates(objIntegrationLogStr, reqBody, headerMap, 
                                                    methodType, PostRetryProcess,messageId);
            
            // get request-response tme difference
            objIntegrationLogStr.strCalloutTime = ((responseTime.getTime() - requestTime.getTime())/1000.00);
            
            // status code verification
            if(!InterfaceHandler.interfaceErrorCodes(objIntSetting).contains(string.valueOf(response.getStatusCode()).trim())){ 
                isErrorCodeMatch =true;
            }   
            // get body from response
            objIntegrationLogStr.strRresponse = InterfaceHandler.getResponseBody(response);        
        }catch(exception ex){
            InterfaceHandler.logForApexErrorInCallout(objIntegrationLogStr,ex);            
        }
        // create interface log
         Integration_Message_Log__c objIntMsgLog = InterfaceHandler.createInterfaceLog(objIntSetting, objIntegrationLogStr);
         
        // create log history
        if(objIntSetting != null){
            InterfaceHandler.createLogHistory(objIntMsgLog);
        }
        // send immediate email if configured
        if(objIntSetting != null && isErrorCodeMatch && objIntSetting.Immediate_email__c){
            System.enqueueJob(new IT_immediate_email_enqueue(objIntMsgLog,objIntSetting));
        }
        // send callout threshold time breach mail
        InterfaceHandler.sendThresholdBreachMail(objIntSetting,objIntMsgLog);
    }
    /***************************************************************************************************************
    *   @Name        :  doMultiCallOut                                                              
    *   @Parameters  :  String  intefaceName,string methodType,blob reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId,Boolean isLast
    *   @Return      :  HttpResponse
    *   @Description :  Api to call multi callout in single transaction(Blob)                        
    ***************************************************************************************************************/
    public override HttpResponse doMultiCallOut(String  intefaceName,string methodType,blob reqBody,
                                            map<string,string> headerMap,string PostRetryProcess,
                                            string messageId,Boolean isLast){
        return doMultiCallOut(intefaceName,methodType,EncodingUtil.base64Encode(reqBody),
                                    headerMap,PostRetryProcess,messageId,isLast);
    }
    /***************************************************************************************************************
    *   @Name        :  doMultiCallOut                                                              
    *   @Parameters  :  String  intefaceName,string methodType,string reqBody,map<string,string> headerMap,string PostRetryProcess,string messageId,Boolean isLast
    *   @Return      :  HttpResponse
    *   @Description :  Api to call multi callout in single transaction                        
    ***************************************************************************************************************/
    public override HttpResponse doMultiCallOut(String  intefaceName,string methodType,
                                            string reqBody,map<string,string> headerMap,string PostRetryProcess,
                                            string messageId,Boolean isLast){
        interface_settings__c objIntSetting;
        IntegrationLogstructure objIntegrationLogStr = new IntegrationLogstructure();
        HttpResponse hrs;
        boolean isError = false;
        boolean isErrorCodeMatch = false;
        DateTime requestTime;
        DateTime responseTime;
        if(!commulticalloutlog){
            try{
                InterfaceHandler.preCalloutLogUpdates(objIntegrationLogStr, reqBody, headerMap, methodType, PostRetryProcess,messageId);
                Boolean doRetry = false;            
                Integer loopCount = 0;                
                if(interfaceCSstaticMap <>null){
                    objIntSetting = interfaceCSstaticMap.get(intefaceName.trim().toLowerCase());
                }
                if(staticMapInterfaceFramSt <> null && staticMapInterfaceFramSt.values().size()>0 && 
                                         !staticMapInterfaceFramSt.values()[0].Stop_All_CallOuts__c){
                    if(objIntSetting <> null){
                        if(!objIntSetting.No_Callout__c ){
                            doRetry = objIntSetting.Immediate_Retry__c;
                            objIntegrationLogStr.errorDescription = '';
                            Http htp = new Http();
                            do{ 
                                
                                loopCount++;
                                requestTime = system.now();
                                if(!objIntSetting.Request_Body_As_Blob__c){
                                    hrs = htp.send(InterfaceHandler.generateHttpRequest(methodType,reqBody,objIntSetting,headerMap));
                                }else{
                                    hrs = htp.send(InterfaceHandler.generateHttpRequest(methodType,EncodingUtil.base64Decode(reqBody),objIntSetting,headerMap));
                                }
                                system.debug('hrs===='+hrs);
                                responseTime = system.now();
                                objIntegrationLogStr.strRresponse=hrs.getBody();
                                
                                objIntegrationLogStr.statusCode=string.valueof(hrs.getStatusCode());
                                objIntegrationLogStr.noOfRetry=loopCount;
                                system.debug('hrs.getStatusCode()'+hrs.getStatusCode());
                                if(!InterfaceHandler.interfaceErrorCodes(objIntSetting ).contains(string.valueOf(hrs.getStatusCode()).trim())){ 
                                    isErrorCodeMatch =true;
                                }
                            }while(doRetry && isErrorCodeMatch  && loopCount < 2 && 
                                    InterfaceHandler.objIntegrationMsgLog == null &&
                                         objIntSetting.Number_of_Retry__c > 1);
                        }else{
                            isError =true; 
                            errorInCalloutProcess = true;
                            InterfaceHandler.logForRespectiveIntLocked(objIntegrationLogStr);                        
                        }
                    }else{
                        isError =true;
                        errorInCalloutProcess = true;
                        InterfaceHandler.logForInvalidInterface(objIntegrationLogStr);                      
                    }
                }else{
                    errorInCalloutProcess = true;
                    InterfaceHandler.logForAllInterfacesLocked(objIntegrationLogStr);
                }
            }catch(CalloutException ex){
                isError =true;
                errorInCalloutProcess = true;
                InterfaceHandler.logForCalloutException(objIntegrationLogStr,ex);            
            }catch(exception ex){
                isError =true;
                errorInCalloutProcess = true;
                InterfaceHandler.logForApexErrorInCallout(objIntegrationLogStr,ex);            
            }
            
            // Immediate Email incase callout exceeds threshold time
            InterfaceHandler.updateCalloutTime(objIntSetting,objIntegrationLogStr,requestTime,responseTime,isError);
            
            
            //
            Integration_Message_Log__c objIntMsgLog = InterfaceHandler.createInterfaceLogObject(objIntSetting, objIntegrationLogStr);
            
            if(!multiIntegrationLogMap.containsKey(objIntSetting)){
                multiIntegrationLogMap.put(objIntSetting,new List<Integration_Message_Log__c>{objIntMsgLog});
            }else{
                List<Integration_Message_Log__c> loglist = multiIntegrationLogMap.get(objIntSetting);
                //multiIntegrationLogMap.get(objIntSetting.name).add(objIntMsgLog);
                loglist.add(objIntMsgLog);
                multiIntegrationLogMap.put(objIntSetting,loglist);
            }
        }
        if(isLast){
            if(InterfaceCallOutProcess.multiIntegrationLogMap != null &&
                                 !InterfaceCallOutProcess.multiIntegrationLogMap.values().isEmpty()){
                List<Integration_Message_Log__c> intLogInsertList = new List<Integration_Message_Log__c>();
                for(List<Integration_Message_Log__c> intListObj : InterfaceCallOutProcess.multiIntegrationLogMap.values()){
                   
                    intLogInsertList.addAll(intListObj);
                }
                if(!intLogInsertList.isEmpty()){
                    database.insert(intLogInsertList);
                }
                
                if(!intLogInsertList.isEmpty()){
                    // Response history versioning
                    InterfaceHandler.createMultiCalloutLogHistory(intLogInsertList);
                    // Immidiate Failure mail
                    if(InterfaceHandler.objIntegrationMsgLog == null && (isError || isErrorCodeMatch)){
                        System.enqueueJob(new IT_immediate_email_enqueue(intLogInsertList));
                    }
                    
                }
            }
        }
        if(hrs != null){
            return hrs;
        }else{
            return null;
        }
    }
    
}