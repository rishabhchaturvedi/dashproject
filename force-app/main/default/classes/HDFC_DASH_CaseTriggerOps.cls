/**
className: HDFC_DASH_CaseTriggerOps
DevelopedBy: Deepali Gupta
Date: 30 Sept 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on Case object.
**/
public with sharing class HDFC_DASH_CaseTriggerOps {
    
    /**
className: EmailToCaseListener
DevelopedBy: Deepali Gupta
Date: 30 Sept 2021
Company: Accenture
Class Description: This Class contains the trigger operation to Case.
**/    
    public with sharing class EmailToCaseListener implements HDFC_DASH_TriggerOps{
        
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        /* 
Method Name :filter
Description :This method would filter the records.
*/  
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForEmailToCase();
            return caseRecList;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeCaseRecForListener(caseRecs); 
        }
    }
 
    
    //Case Manual Owner Change   
    public with sharing class ManualOwnerAssignmentListener implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        /* 
Method Name :filter
Description :This method would filter the records.
*/  
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForManualAssignment();
            return caseRecList;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeCaseRecForOwnerChangeListener(caseRecs); 
        }
    }
    
    //Intent Type Combination Validation - commented 13th feb
   /* public with sharing class IntentTypeValidation implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
         public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForIntentType();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeCaseRecForIntentListener(caseRecs); 
        }
        
    }*/
    
    //Case Status Change Count
     public with sharing class StatusChangeCounts implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
         public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForStatusChange();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeCaseRecForStatusChange(caseRecs); 
        }
        
    }
    
    
    //Case Prior value set
  public with sharing class CasePriorValueSet implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
         public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForPriorValues();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeCaseForPriorValueListener(caseRecs); 
        }
        
    } 
    
    //ILPS/LAC Integration
     public with sharing class postCasesToILPSLAC implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
         public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecToPost();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executePostCaseRecListener(caseRecs); 
        }
        
    } 
    
    //Email trigger communication
     public with sharing class uponcasecreationOps implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            system.debug('@enabled '+ HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun);
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
         public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
             system.debug('filtercall@');
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForEmailCom();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            system.debug('executecall@');
            HDFC_DASH_CaseTriggerOpsHelper.uponcasecreation(caseRecs); 
          //  HDFC_DASH_CaseTriggerOpsHelper.uponcaseOwnerChange(caseRecs);
            HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun = false;
        }
        
    } 
    
   
    //Case Workflow
    public with sharing class CaseWorkflow implements HDFC_DASH_TriggerOps{    
    /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        /* 
Method Name :filter
Description :This method would filter the records.
*/  
        public SObject[] filter(){
            system.debug('existingMdtListFilter : ');
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForWorkflow();
            return caseRecList;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeCaseRecForWorkflow(caseRecs); 
           // HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun = false;
        }
}
    public with sharing class FeeTransnAssignment implements HDFC_DASH_TriggerOps{    
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        /* 
Method Name :filter
Description :This method would filter the records.
*/  
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterFeeTransnAssignment();
            return caseRecList;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeFeeTransnAssignment(caseRecs); 
            HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun = false;
        }
    }
    
    //Resolved cases
     //Read Only on Resolved Cases
    public with sharing class caseReadOnlyOnResolve implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();             
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterReadOnlyCase();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            system.debug('executePostCaseRecListener...');
            HDFC_DASH_CaseTriggerOpsHelper.executeReadOnlyListener(caseRecs); 
        }
        
    }
    
     //Update Status to Replied
    public with sharing class UpdateStatusReplied implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();             
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterRepliedCase();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            system.debug('executePostCaseRecListener...');
            HDFC_DASH_CaseTriggerOpsHelper.executeRepliedCaseListener(caseRecs); 
        }
        
    }
    
     //Social Media Error 
    /*public with sharing class SocialMediaErrorScenario implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();             
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterCaseRecForSocialMediaException();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            system.debug('executePostCaseRecListener...');
            //HDFC_DASH_CaseTriggerOpsHelper.executeCaseSocialMediaExcep(caseRecs); 
        }
        
    } */
    
    //Deposit
    public with sharing class DepositEmailToCase implements HDFC_DASH_TriggerOps{    
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        /* 
Method Name :filter
Description :This method would filter the records.
*/  
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterDepositCases();
            return caseRecList;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeDepositCases(caseRecs); 
        }
    }
    
    //Before Insert/update for all cases
   
     public with sharing class beforecreateorupdate implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
         public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
             system.debug('@@@@ before all case filtercall@');
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterallCaseRec();
            return caseRecList;
        }
        
        public void execute(Case[] caseRecs){
            system.debug('@@@@ before all case executecall@');
            HDFC_DASH_CaseTriggerOpsHelper.executeallCaseRec(caseRecs); 
           // HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun = false;
        }
        
    } 
    
    //Case Prior value set
  /*public with sharing class CaseInternalEscalation implements HDFC_DASH_TriggerOps{        
      
        public Integer count = 0;
  
          public Boolean isEnabled() {
            system.debug('@@@@ CaseInternalEscalation caseIsFirstRun '+HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun);
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
         public SObject[] filter(){
             if(count < 1){
                 List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterEscalatedCase();
              system.debug('@@@@ CaseInternalEscalation Return List Size '+caseRecList.size());
            return caseRecList;                 
             }else{
                 return null;
             }
            
        }
        
        public void execute(Case[] caseRecs){//executeEscalatedCase
            if(count < 1){
                system.debug('@@@@ CaseInternalEscalation Return List Size '+caseRecs.size());
            HDFC_DASH_CaseTriggerOpsHelper.executeEscalatedCase(caseRecs); 
                count = 1;
            }
            
            
            
        }
          
          
     
        
        
    } */
    
    //Social Media File number validation
     public with sharing class FileNumberValidation implements HDFC_DASH_TriggerOps{
        
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
        }
        
        /* 
Method Name :filter
Description :This method would filter the records.
*/  
        public SObject[] filter(){
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterFileForSocialMedia();
            return caseRecList;
            
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Case[] caseRecs){
            HDFC_DASH_CaseTriggerOpsHelper.executeFileForSocialMedia(caseRecs); 
        }
    }
    
    
    //Level 0 Website Communication
    public with sharing class websiteCommunication implements HDFC_DASH_TriggerOps{
        
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
           // return HDFC_DASH_TriggerOpsRecursionFlags.caseIsFirstRun;
           return true;
        }
        
        /* 
Method Name :filter
Description :This method would filter the records.
*/  
        public SObject[] filter(){
            system.debug('inside filter websiteCommunication');
            List<Case> caseRecList = new List<Case>();
            caseRecList = HDFC_DASH_CaseTriggerOpsHelper.filterLevelWebsite();
            return caseRecList;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Case[] caseRecs){
             system.debug('inside execute websiteCommunication');
            HDFC_DASH_CaseTriggerOpsHelper.executeLevelWebsite(caseRecs); 
        }
    }
    
    
    
}