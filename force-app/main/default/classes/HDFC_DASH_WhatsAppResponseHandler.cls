/**
className: HDFC_DASH_WhatsAppResponseHandler
DevelopedBy: Tejeswari 
Date: 10 January 2022
Company: Accenture 
Class Description: This Class contains the update operation on Task object based on the WhatsApp Integration Response.
**/
public with sharing class HDFC_DASH_WhatsAppResponseHandler extends processResponseHandler{
       /*      
Method Name :doRetryProcess
Description :This method would Update the Task Description, status.
*/
    public Override void doRetryProcess(HttpResponse response, String processDataStr) 
    {
        List<Task> listUpdateTask = new List<Task>();
        try
        {
            if(response != null && response?.getBody() != null)
            {               
                string responseBody = response?.getBody();

                HDFC_DASH_ParseResponseWAIntegration result = HDFC_DASH_ParseResponseWAIntegration.parse(responseBody);
                HDFC_DASH_CommunicationWrapper.processData objProcessData = (HDFC_DASH_CommunicationWrapper.processData)json.deserialize(processDataStr, HDFC_DASH_CommunicationWrapper.processData.class);
                
                if(objProcessData?.taskId != null)
                {            
                    Task objTask = new Task();
                    objTask.id = objProcessData.taskId;
                   
                    if(result.status == HDFC_DASH_Constants.STRING_SUCCESS)
                    {
                       
                        objTask.Status = HDFC_DASH_Constants.STRING_CLOSED;
                    }
                    else
                    {
                        objTask.Status = HDFC_DASH_Constants.STRING_FAILED;
                    }
                    
                    if(objProcessData?.messageBody != null)
                    {
                        objTask.Description = objProcessData?.messageBody;
                    }
                    
                    if(objProcessData?.msgLogId != null)
                    {
                        objTask.HDFC_DASH_Message_Log__c = objProcessData?.msgLogId;
                    }
                    listUpdateTask.add(objTask);

                    if(Schema.sObjectType.Task.isUpdateable()){
                        Database.update(listUpdateTask); 
                    }
                }
            }   
        }
        catch(Exception e)
        {     
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            //system.debug('expDetails => ' + expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_WHATSRESPONSEHANDLER, HDFC_DASH_Constants.HANDLE_DORETRYPROCESS, processDataStr, expDetails);
        }
    }
}