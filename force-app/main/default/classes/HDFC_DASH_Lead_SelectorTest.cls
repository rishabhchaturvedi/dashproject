/**
className: HDFC_DASH_Lead_SelectorTest
DevelopedBy: Punam Marbate
Date: 05 July 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Lead_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_Lead_SelectorTest {
    private static final string STRING_ID ='Id';
    private static final string STRING_FIRSTNAME='FirstName'; 
    private static final string STRING_LASTNAME = 'LastName';
    private static final string STRING_ISCONVERTED ='IsConverted';
    private static final string STRING_MASTERLABEL ='MasterLabel';

    /* Method Name:testGetLeadsBasedOnIds
    Description:This is the testmethod for getLeadsBasedOnIds
    */
    static testmethod void testGetLeadsBasedOnIds(){
        List<Lead> resultLead = null ;
        List<Lead>  leadRecords=null;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        System.runAs(sysAdmin){
            leadRecords = HDFC_DASH_TestDataFactory_Leads.getBasicLeadlist(2);
            Set<Id> resultIds =(new Map<Id,SObject>(leadRecords)).keySet();
            List<Id> leadIds = new List<Id>(resultIds);
            List<String> fieldList = new List<string>{STRING_ID,STRING_FIRSTNAME,STRING_LASTNAME};
            Test.startTest();
            resultLead =  HDFC_DASH_Lead_Selector.getLeadsBasedOnIds(fieldList, leadIds);
            Test.stopTest();
        }
        system.assertNotEquals( null, resultLead);
        system.assertNotEquals( null, leadRecords);
        system.assertEquals(2,resultLead.size()); 
    }
    
    /*Method Name:testGetLeadsBasedOnId
    Description:This is the testmethod for getLeadsBasedOnId
 	*/
    static testmethod void testGetLeadsBasedOnId(){
        Lead resultLead = null ;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Lead leadRecord;
        System.runAs(sysAdmin){
            leadRecord = HDFC_DASH_TestDataFactory_Leads.getBasicLead();      
            List<String> fieldList = new List<string>{STRING_ID,STRING_FIRSTNAME,STRING_LASTNAME};
        Test.startTest();
            resultLead =  HDFC_DASH_Lead_Selector.getLeadsBasedOnId(fieldList, leadRecord.Id);
        Test.stopTest();
        }
        system.assertNotEquals( null, resultLead);
        system.assertNotEquals( null, leadRecord);
        system.assertEquals(leadRecord.Id,resultLead.Id); 
    }

    /*Method Name: testGetLeadsBasedOnQuery
    Description: This is the testmethod for getLeadsBasedOnQuery
    */
    static testmethod void testGetLeadsBasedOnQuery(){       
        List<Lead> resultLeadList = null ;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Lead leadRecord;
        System.runAs(sysAdmin){
            leadRecord = HDFC_DASH_TestDataFactory_Leads.getBasicLead();      
            List<String> fieldList = new List<string>{STRING_ID,STRING_FIRSTNAME,STRING_LASTNAME};
            Map<String,String> condition= new Map<string,String>{'HDFC_DASH_PAN__c' => '='+'\''+leadRecord.HDFC_DASH_PAN__c+'\''};       
    	Test.startTest();
            resultLeadList =  HDFC_DASH_Lead_Selector.getLeadsBasedOnQuery(fieldList, condition);
        Test.stopTest();
        }
        system.assertNotEquals( null, resultLeadList);
        system.assertNotEquals( null, leadRecord);
        system.assertEquals(leadRecord.Id,resultLeadList[0].Id);      
    }
    /*Method Name: testGetLeadStatus
    Description: This is the testmethod for getLeadStatus
    */
    static testmethod void testGetLeadStatus(){     
        LeadStatus convertStatus;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Lead leadRecord;
        System.runAs(sysAdmin){
            leadRecord = HDFC_DASH_TestDataFactory_Leads.getBasicLead();   
            Database.LeadConvert leadConv = new database.LeadConvert();
            leadConv.setLeadId(leadRecord.id);
            List<String> fieldListLeadConStatus = new List<string>{STRING_ID,STRING_MASTERLABEL};
            Map<String,String> conditionLeadConStatus= new Map<string,String>{STRING_ISCONVERTED => HDFC_DASH_Constants.STRING_EQUALTO+ true};    
    	Test.startTest();
            convertStatus = HDFC_DASH_Lead_Selector.getLeadStatus(fieldListLeadConStatus,conditionLeadConStatus);
            leadConv.setConvertedStatus(convertStatus.MasterLabel);
        Test.stopTest();
        }
        system.assertNotEquals( null, convertStatus);              
    }
}