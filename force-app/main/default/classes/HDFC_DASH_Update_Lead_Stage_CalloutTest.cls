/*
Class: HDFC_DASH_Update_Lead_Stage_CalloutTest
Author: Ayesha Gavandi
Date: 25 Nov 2021
Company: Accenture
Description:This is the test class for Queueable Class for Update Lead Stage.
*/

@isTest(SeeAllData=false)
public class HDFC_DASH_Update_Lead_Stage_CalloutTest {
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
  	private static Lead leadRec = HDFC_DASH_TestDataFactory_Leads.getBasicLead();
 /* 
  Method Name: testUpdateLeadStageCallout
  parameter:none
  Method Description: tests the functionality of lead stage callout
 */ 
    static testmethod void testUpdateLeadStageCallout(){
        System.runAs(sysAdmin)
        { 
        app.HDFC_DASH_LMS_Lead_ID__c = '80624449';
        app.HDFC_DASH_Stage_Number__c = HDFC_DASH_Constants.STRING_50;
        update app;
            leadRec.id = app.HDFC_DASH_Lead__c;
           leadRec.HDFC_DASH_LMS_Lead_ID__c = app.HDFC_DASH_LMS_Lead_ID__c;
            insert leadRec;
        ID jobID=null;
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        HDFC_DASH_TestDataFactory_API.createInterfaceSettingUpdateLeadStage();
        Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockLeadStageResGen());
        HDFC_DASH_Update_Lead_Stage_Callout leadStageCallOut  = new HDFC_DASH_Update_Lead_Stage_Callout(app.Id);
		Test.startTest();
        jobID = System.enqueueJob(leadStageCallOut);
        Test.stopTest();  
        System.assertNotEquals(null,jobID);
        }
    }
/*
  Method Name:testUpdateLeadStageCalloutException
  Method Description: This method will test the exception scenario for Probe Search callout
*/
    static testmethod void testUpdateLeadStageCalloutException (){
        System.runAs(sysAdmin)
        {
        ID jobID=null;
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        HDFC_DASH_TestDataFactory_API.createInterfaceSettingPollingPaymentStatus();
        Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockLeadStageResGen());
        HDFC_DASH_Update_Lead_Stage_Callout LeadStageCallOut  = new HDFC_DASH_Update_Lead_Stage_Callout(app.Id);
		Test.startTest();
        jobID = System.enqueueJob(LeadStageCallOut);
        Test.stopTest();  
        System.assertNotEquals(null,jobID);
        }
    }
}