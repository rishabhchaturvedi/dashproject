/* ================================================
 * @Class Name :  InterfaceCallOutProcess_Test
 * @author : Accenture
 * @Purpose: This class is used to test the functionality of class InterfaceCallOutProcess
 * @created date:
 * @Last modified date:19/1/2016
 * @Last modified by : Accenture
================================================*/
@istest(SeeAllData = false)
public class InterfaceCallOutProcess_Test{

    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);

    /***************************************************************************************************************
    *   @Name        :  testMethodFirst                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doMultiCallOut of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testMethodFirst(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            //interfaceSettingObj.Email_Template_Id__c = utility.createEmailTemplate();
            Database.update(interfaceSettingObj);    
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            HttpResponse res = null;
            test.starttest(); 
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            res = classObj.doCallOut('GETCALL','GET','testurl',null,'IfTestRetryProcess','TestMessageId');
            test.stoptest();
            system.assertNotEquals(null, res);
            system.assertEquals(200, res.getStatusCode(),'status code matching');
        }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodNew                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doMultiCallOut of InterfaceCallOutProcess.                 
    ***************************************************************************************************************/
    public static testMethod void testMethodNew(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            //interfaceSettingObj.Email_Template_Id__c = utility.createEmailTemplate();
            
            Database.update(interfaceSettingObj);
            
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            HttpResponse res = null;
            test.starttest();
            
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            res =classObj.doMultiCallOut('GETCALL','GET','testurl',null,'IfTestRetryProcess','TestMessageId',true);
            test.stoptest();
            system.assertNotEquals(null, res);
            system.assertEquals(200, res.getStatusCode(),'status code matching');
        }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodTwo                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doMultiCallOut of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testMethodTwo(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            //interfaceSettingObj.Email_Template_Id__c = utility.createEmailTemplate();
            
            Database.update(interfaceSettingObj);
            
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            HttpResponse res = null;
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            res =classObj.doCallOut('GETCALL','GET','testurl',null,'IfTestRetryProcess','TestMessageId');
            test.stoptest();
            system.assertNotEquals(null, res);
            system.assertEquals(200, res.getStatusCode(),'status code matching');
        }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodThree                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doMultiCallOut of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
     public static testMethod void testMethodThree(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(true);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            //interfaceSettingObj.Email_Template_Id__c = utility.createEmailTemplate();
            
            Database.update(interfaceSettingObj);
            
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            HttpResponse res = null;
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            res = classObj.doCallOut('GETCALL','GET','testurl',null,'IfTestRetryProcess','TestMessageId');
            test.stoptest();
            system.assertEquals(null, res);
        }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodFour                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doMultiCallOut of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testMethodFour(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            interfaceSettingObj.No_Callout__c = true;
            Database.update(interfaceSettingObj);
            //HttpResponse res =null;
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            classObj.doCallOut('GETCALL','GET','testurl',null,'IfTestRetryProcess','TestMessageId');
            test.stoptest();
            //system.assertNotEquals(null, res);
            //system.assertEquals(200, res.getStatusCode(),'status code matching');

        }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodFive                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doMultiCallOut of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testMethodFive(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            interfaceSettingObj.No_Callout__c = true;
            interfaceSettingObj.Immediate_email__c = false;
            Database.update(interfaceSettingObj);
            HttpResponse res=null;
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            classObj.doCallOut('GETCALL','GET','testurl',null,'IfTestRetryProcess','TestMessageId');
            test.stoptest();
            //system.assertNotEquals(null, res);
            //system.assertEquals(200, res.getStatusCode(),'status code matching');

       }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodSix                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doManualRetry of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testMethodSix(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            // insert Integration Message Log record
            Integration_Message_Log__c intLogObj = new Integration_Message_Log__c();
            utility.createInterationLog(intLogObj);
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            Database.update(interfaceSettingObj);
            
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            classObj.doManualRetry(intLogObj);
            test.stoptest();
        }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodSeven                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doCallOutLogging of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testMethodSeven(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    blob.valueOf('[{"Name": "sForceTest1"}]'),
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord(true);
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            //interfaceSettingObj.Email_Template_Id__c = utility.createEmailTemplate();
            
            Database.update(interfaceSettingObj);
            Blob bval = blob.valueOf('test_data');
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            HttpRequest req = new HttpRequest();
            
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            req.setHeader('test','headerMap.get(objstr)');
            req.setMethod('GET');
            req.setEndpoint('objIntSetting.End_point__c');
            req.setTimeout(1);
            HttpResponse  response = classObj.doCallOut('GETCALL','POST',bval,null,'IfTestRetryProcess','TestMessageId');
            InterfaceCallOutProcess.doCallOutLogging('GETCALL',req,response,'testurl','TestMessageId', system.now(),system.now());
            test.stoptest();
       }
    }
    /***************************************************************************************************************
    *   @Name        :  testMethodEight                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for createInboundIntegrationLog of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
     public static testMethod void testMethodEight(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            //interfaceSettingObj.Email_Template_Id__c = utility.createEmailTemplate();
            
            Database.update(interfaceSettingObj);
            Exception excObj;
            try{
                integer intg = 0/0;
            }catch(Exception ex){
                excObj = ex;
            }
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            map<string,string> header = new map<string,string>();
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            
           
            InterfaceCallOutProcess.createInboundIntegrationLog('GETCALL','GET','testurl','GET','TestMessageId',excObj,header);
            
            test.stoptest();
        }
     }
    /***************************************************************************************************************
    *   @Name        :  testMethodNine                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for createInboundIntegrationLog of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
     public static testMethod void testMethodNine(){
        System.runAs(sysAdmin){
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            utility.insertFireAndForgetIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            interfaceSettingObj.Fire_And_Forget__c = true;
            //interfaceSettingObj.Email_Template_Id__c = utility.createEmailTemplate();
            
            Database.update(interfaceSettingObj);
            Exception excObj = null;
            try{
                integer intg= 0/0;
            }catch(Exception ex){
                excObj = ex;
            }
            // insert parent interface record;
            Integration_Message_Log__c intMsgLog = new Integration_Message_Log__c();
            intMsgLog.Interface_Name__c = 'GETCALL';
            intMsgLog.Message_Id__c  = 'FireNForgetTestMessageId';
            Database.insert(intMsgLog);
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            map<string,string> header = new map<string,string>();
            test.starttest();
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            InterfaceCallOutProcess.createInboundIntegrationLog('ParentRecord','GET','testurl',
                                    'GET','FireNForgetTestMessageId',excObj,header);
            test.stoptest();
        }
    }
    /***************************************************************************************************************
    *   @Name        :  testlogContinuationCallout                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for logContinuationCallout of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testlogContinuationCallout(){
        System.runAs(sysAdmin){
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            map<string,string> header = new map<string,string>();
            // insert interface setting record
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            test.starttest();  
            interfaceSettingObj = utility.insertIntefaceSettingsRecord(true);
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            Database.update(interfaceSettingObj);
            
            classObj.logContinuationCallout('Test Interface','POST','Test Request Body',
                            header,'Test PostRetryProcess','continuationId1','MessageId 1');
            test.stoptest();
        }
    }
    /***************************************************************************************************************
    *   @Name        :  testdoMultiCalloutsWithNoCallOutCheckBox                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for doMultiCallouts WithNoCallOutCheckBox of InterfaceCallOutProcess.                     
    ***************************************************************************************************************/
    public static testMethod void testdoMultiCalloutsWithNoCallOutCheckBox(){
        System.runAs(sysAdmin){
          
            IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                    'Complete',
                                                    '[{"Name": "sForceTest1"}]',
                                                    null);
            // get utility class object
            IT_TestUtility utility = new IT_TestUtility();
            Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
            InterfaceCallOutProcess classObj = new InterfaceCallOutProcess();
            Test.starttest();
            
            
            //insert framework setting record
            utility.insertFrameworkSettingsRecord(false);
            
            // insert interface setting record
            
            interfaceSettingObj = utility.insertIntefaceSettingsRecord();
            interfaceSettingObj.User_Contact_ID_for_Email__c = utility.insertContact();
            interfaceSettingObj.No_Callout__c = true;
            Database.update(interfaceSettingObj);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            classObj.doMultiCallOut('GETCALL','GET','testurl',null,'IfTestRetryProcess','TestMessageId',true);
            Test.stoptest();
        }
    }
    
}