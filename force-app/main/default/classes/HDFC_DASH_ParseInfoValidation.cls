/*
Author: Anisha Arumugam
Class: HDFC_DASH_ParseInfoValidation
Description: Parser Class for Store Info Validation API
*/
public without sharing class HDFC_DASH_ParseInfoValidation {
    
    /*
Class: PrePopulatedPassportInformation
Description: Class to parse PrePopulated Passport Information
*/
    public without sharing class PrePopulatedPassportInformation {
        public String passportNumber;
        public String nationality;
    }
    
    /*
Class: PrePopulatedPermanentResidency
Description: Class to parse PrePopulated Permanent Residency
*/
    public without sharing class PrePopulatedPermanentResidency {
        public String prNumber;
        public String prCountry;
        public String prCountryCode;
        public Date prValidityDate;
    }
    
    /*
Class: PrePopulatedAddressDetails
Description: Class to parse PrePopulated Address Details
*/
    public without sharing class PrePopulatedAddressDetails {
        public String addressLine1;
        public String addressLine2;
        public String addressLine3;
        public String addressLine4;
        public String landmark;
        public String city;
        public String cityCode;
        public String taluka;
        public String district;
        public String state;
        public String stateCode;
        public String country;
        public String countryCode;
        public String pinCode;
        public String postOfficeName;
    }
    
    public String sfApplicationId;
    public String sfApplicationNumber;
    public String sfApplicantId;
    public String sfCustomerId;
    public String sfCustomerNumber;
    public String fileNumber;
    public String sfCaseId;
    public String sfCaseNumber;
    public String documentStatus;
    //public String customerNumber;
    public String customerCapacity;
    public Date uploadedDate;
    public String infoValidationRecordType;
    public ApplicantInfoValidation applicantInfoValidation;
    
    /*
Class: ApplicantInfoValidation
Description: Class to parse ApplicantInfo Validation
*/
    public without sharing class ApplicantInfoValidation {
        public String validationType;
        public String source;
        public String docType;
        public String docNumber;
        public Date dateOfIssue;
        public Date dateOfExpiry;
        public String salutation;
        public String firstName;
        public String middleName;
        public String lastName;
        public Date dateOfBirth;
        public String genderCode;
        public String addressType;
        
        public Boolean documentFileUploaded;
        public Boolean loanOutstandingLetterUpload;
        public Boolean propertyCostSheetUpload;
        public String dmsDocumentId;
        public Boolean financialInfoDocumentUpload;
       
        
        public ApplicantImage applicantImage;
        public PrePopulatedSalaryInformation prePopulatedSalaryInformation;
        public PrePopulatedBankDetails prePopulatedBankDetails;
        public PrePopulatedEmployerInformation prePopulatedEmployerInformation;
        public PrePopulatedBusinessInformation prePopulatedBusinessInformation;
        public PrePopulatedPassportInformation prePopulatedPassportInformation;
        public PrePopulatedPermanentResidency prePopulatedPermanentResidency;
        public PrePopulatedAddressDetails prePopulatedAddressDetails;
        public PrePopulatedEmpGstBusinessDetails prePopulatedEmpGstBusinessDetails;
        public PrePopulatedFinancialInformation prePopulatedFinancialInformation;
        
        public String docExtractable;
        public String docImageOrigin;
        public String docScannedBy;
        public String docTypeCode;
        public String monthOfGstReturn;
        public String assessmentYear;
        public String financialYear;
        public Decimal totalAmountPaid;
        public Decimal noOfQuartersPaid;
        public String proofOfCommunication;
        public String proofOfCommunicationCode;
        public String bcpDocType;
        public String bcpDocTypeCode;
        public Decimal latitude;
        public Decimal longitude;
        public String kycDocType;
        public String kycDocTypeCode;
        public String storageIdentifier;
        public String storageType;
        
        
    }
    /*
Class: applicantImage
Description: Class to parse applicantImage
*/
    public without sharing class ApplicantImage {
        public String imageSource;
        public String validImage; 
        public String fileName;
        public String fileExtension; 
        public String contentDocumentId;
        public String base64File; 
    }
    
    /*
Class: prePopulatedFinancialInformation
Description: Class to parse Pre Populated Financial Information
*/
    public without sharing class prePopulatedFinancialInformation {
        public String accountHolderName;
        public String bankName;
        public String bankCode;
        public String loanType;
        public String loanTypeCode;
        public Decimal outstandingAmount;
        public Decimal installmentAmount;
        public Integer balanceTermOfLoanInMonths;
    }
    /*
Class: prePopulatedSalaryInformation
Description: Class to parse Pre Populated Salary Information
*/
    public without sharing class prePopulatedSalaryInformation {
        public String incomeProof;
        public String salaryMonth;
        public Integer salaryYear;
        public String additionalIncomeDocumentType;
        public Boolean bankStatementUpload;
        public Boolean salarySlipUploadMonth;
        public Boolean salarySlipUploadWeek1;
        public Boolean salarySlipUploadWeek2;
        public Boolean salarySlipUploadWeek3;
        public Boolean salarySlipUploadWeek4;
    }
    
    /*
Class: PrePopulatedBankDetails
Description: Class to parse Bank Details
*/
    public without sharing class PrePopulatedBankDetails {
        public String accountType;
        public Date fromDate;
        public Date toDate;
        public String accountHolderName;
        public String bankCode;
        public String bankName;
        public String accountNumber;
        public String ifscCode;
        public String swiftCode;
        public String isSalaryAccount;
    }
    
    /*
Class: PrePopulatedEmployerInformation
Description: Class to parse Pre Populated Employer Information
*/
    public without sharing class PrePopulatedEmployerInformation {
        public String employerName;
        public String employerCode;
        public String employeeNumber;
        public String designation;
        public String department;
        public Integer jobDurationInMonths;
        
    }
    /*
Class: PrePopulatedBusinessInformation
Description: Class to parse Pre Populated Business Information
*/
    public without sharing class PrePopulatedBusinessInformation {
        public String designationAndEntityType;
        public String businessName;
        public Integer yearOfIncorporation;
        public String annualTurnover;
        public String natureOfBusiness;
        public String industryType;
        public String businessPAN;
        public String udyamRegistrationNumber;
        public String cinNumber;
    }
    
    /*
Method: parse
Description: Class to parse Pre Populated GST Information
*/
    public without sharing class PrePopulatedEmpGstBusinessDetails {
        public String gstNumber;
        public String empGstStateCode;
        public String empGstState;
    }
    
    /*
Method: parse
Description: Method to Deserialize the StoreInfoValidation API's request body
*/
    public static HDFC_DASH_ParseInfoValidation parse(RestRequest request) {
        string json = request.requestBody.toString();
        return (HDFC_DASH_ParseInfoValidation) System.JSON.deserialize(json, HDFC_DASH_ParseInfoValidation.class);
    }
}