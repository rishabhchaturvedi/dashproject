/**
className: HDFC_DASH_Applicant_BankDet_SelectorTest
DevelopedBy: Sindu Priya
Date: 28 Sep 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Applicant_BankDetail_Selector class.
**/
@isTest(SeeAllData = false)
Public with sharing class HDFC_DASH_Applicant_BankDet_SelectorTest {
    
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                    HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    //private static HDFC_DASH_Applicant_Bank_Detail__c AppBankDetail = 
     //               HDFC_DASH_TestDataFactory_BankDetails.getBasicApplBankDetail(appDetails.Id);
    //private static HDFC_DASH_Applicant_Details__c appDetails2 = 
      //              HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Bank_Detail__c appBankDetail = 
                    HDFC_DASH_TestDataFactory_BankDetails.getBasicApplBankDetail(appDetails.Id,con.id);
    /* 
    Method Name :testgetBankDetailsBasedOnId
    Description :This method will test the getBankDetailsBasedOnId.
    */
   /* static testmethod void testgetBankDetailsBasedOnId(){
        System.runAs(sysAdmin){
            HDFC_DASH_Applicant_Bank_Detail__c resApplBankDetail;   
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            resApplBankDetail = HDFC_DASH_Applicant_BankDetail_Selector.getBankDetailsBasedOnId(fieldList,appBankDetail.Id);       
            Test.stopTest();
            system.assertNotEquals( null, resApplBankDetail);
            system.assertEquals(appBankDetail.Id,resApplBankDetail.Id,'SUCCESS'); 
        }
    }  */
    /* 
    Method Name :testgetBankDetailBasedOnIds
    Description :This method will test the method getBankDetailBasedOnIds().
   */
  /*  static testmethod void testgetBankDetailBasedOnIds(){
        List<HDFC_DASH_Applicant_Bank_Detail__c>  listBankDetails = null;
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            List<Id> listBankDetailIds = new List<Id>{appBankDetail.Id};
			Test.startTest();
            listBankDetails = HDFC_DASH_Applicant_BankDetail_Selector.getBankDetailBasedOnIds(fieldList ,listBankDetailIds);
            Test.stopTest();
        }
        system.assertNotEquals( null, listBankDetails);
        system.assertEquals(1,listBankDetails.size(),'SUCCESS');
    } */
      /* 
    Method Name :testgetBankDetailsBasedOnQuery
    Description :This method will test the getBankDetailsBasedOnQuery.
    */
    static testmethod void testgetBankDetailsBasedOnQuery(){
        List<HDFC_DASH_Applicant_Bank_Detail__c>  listBankDetailRec = null;
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.HDFC_DASH_APPDETAILS};
            Test.startTest();
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_APPDETAILS =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                appDetails.Id+HDFC_DASH_Constants.STRING_QUOTE};
            listBankDetailRec = HDFC_DASH_Applicant_BankDetail_Selector.getBankDetailsBasedOnQuery(fieldList,condition);
        	Test.stopTest();
        }
        system.assertNotEquals( null, listBankDetailRec);
    }
    /* 
    Method Name :testgetBankDetailsBasedOnContactIds
    Description :This method will test the method getBankDetailsBasedOnContactIds().
    */
   /* static testmethod void testgetBankDetailsBasedOnContactIds(){   
        List<HDFC_DASH_Applicant_Bank_Detail__c> resBankDetails = null; 
        List<Id> objDetailIdList = new List<Id>{appDetails.id} ; 
        System.runAs(sysAdmin){   
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                                            HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            resBankDetails = HDFC_DASH_Applicant_BankDetail_Selector.getBankDetailsBasedOnContactIds(fieldList,objDetailIdList);
        	Test.stopTest();
        }
        system.assertNotEquals( null, resBankDetails);
    }*/
    /* 
    Method Name :testgetBankDetails
    Description :This method will test the method getBankDetails().
    */
    static testmethod void testgetBankDetails(){
        List<HDFC_DASH_Applicant_Bank_Detail__c>  listBankDetailRec = null;
        System.runAs(sysAdmin){      
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                                                HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listBankDetailRec = HDFC_DASH_Applicant_BankDetail_Selector.getBankDetails(
                                    new List<String> {appBankDetail.Id}  ,
                                    new List<String>{HDFC_DASH_Constants.STRING_ID},
                                    HDFC_DASH_Constants.ID_STRING);          
            Test.stopTest();
        }
        system.assertNotEquals( null, listBankDetailRec);
        system.assertEquals(1,listBankDetailRec.size(),'SUCCESS'); 
    }
}