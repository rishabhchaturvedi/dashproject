/*
	Author: Anisha Arumugam
	Class: HDFC_DASH_APIResponse_InfoValidation
	Description: Apex Class for handling the response for Info Store Info Validation API
*/
public inherited sharing class HDFC_DASH_APIResponse_InfoValidation 
{
	public String successMessage;
    public String sfInfoValidationId;
}