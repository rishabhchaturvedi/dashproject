/* ================================================
 * @Class Name :  IT_immediate_email_enqueue
 * @author : Accenture
 * @Purpose: The purpose of this class is to send Immediate Emails for Integration message logs.
 * @created date:
 ================================================*/
public class IT_immediate_email_enqueue implements Queueable {
    
    private Integration_Message_Log__c objIntMessage;
    private interface_settings__c objIntSetting;
    private Boolean multiCallouts;
    private List<Integration_Message_Log__c> intLogInsertList;
    /***************************************************************************************************************
    *   @Name        :  IT_immediate_email_enqueue                                                              
    *   @Parameters  :  Integration_Message_Log__c objIntMessage ,interface_settings__c objIntSetting                                                                                
    *   @Description :  It is a constructor for class IT_immediate_email_enqueue                             
    ***************************************************************************************************************/
    public IT_immediate_email_enqueue(Integration_Message_Log__c objIntMessage ,interface_settings__c objIntSetting){
        this.objIntMessage = objIntMessage;
        this.objIntSetting = objIntSetting;
        this.multiCallouts = false;
    }
    /***************************************************************************************************************
    *   @Name        :  IT_immediate_email_enqueue                                                              
    *   @Return      :  List<Integration_Message_Log__c> intLogInsertList                                                                                    
    *   @Description :  It is a constructor for class IT_immediate_email_enqueue                             
    ***************************************************************************************************************/
    public IT_immediate_email_enqueue(List<Integration_Message_Log__c> intLogInsertList){
        this.intLogInsertList = intLogInsertList;
        this.multiCallouts = true;
    }
    /***************************************************************************************************************
    *   @Name        :  Execute                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  sendImmediateEmail method of InterfaceHandler to be called in execute method.                              
    ***************************************************************************************************************/
    public void execute(QueueableContext context) {
        if(!multiCallouts){
            InterfaceHandler.sendImmediateEmail(objIntMessage, objIntSetting);   
        }else{
            InterfaceHandler.sendMultiImmediateEmail(intLogInsertList);
        }
    }
}