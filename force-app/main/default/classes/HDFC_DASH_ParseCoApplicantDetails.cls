/**
* 	className: HDFC_DASH_ParseCoApplicantDetails
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class is a wrapper class for HDFC_DASH_RestResource_CoApplicant for receiving the API request .
**/
public inherited sharing class HDFC_DASH_ParseCoApplicantDetails{
    /*
Class: EmploymentDetails
Description: Class to get EmploymentDetails
*/
    public without sharing class EmploymentDetails {
        public String occupation;
        public String workEmail;
        public String employerCode;
        public String employerName;
    }
    
    public CoApplicant coApplicant;
    
    
    /*
Class: CoApplicant
Description: Class to get CoApplicant
*/
    public without sharing class CoApplicant {
        public String applicantType;
        public String sfApplicationId;
        public String sfApplicationNumber;
        public Integer leadStageNumber;
        public String leadStageDescription;
        public String experianCibilID;
        public Boolean experianCibilTechnicalFailure;
        public PersonalDetails personalDetails;
        public RiskScore riskScore;
        public Boolean riskScoreTechnicalFailure;
        public String skipEmploymentDetails;
        public EmploymentDetails employmentDetails;
        public FinancialInfo financialInfo;
        public Integer coAppSequenceNumber;
    }
    /*
Class: RiskScore
Description: Class to get RiskScore
*/
    public without sharing class RiskScore {
        public String riskScoreCustomerstatus;
        public String riskScoreCustomerflags;
        public String riskScoreRtrexists;
        public String riskScoreCustomergrade;
        public String riskScoreRiskevent;
        public String riskScoreAvailedMoratorium;
        public String riskScoreAvailedRestruct;
        public String riskScoreHdfcStaff;
        public String riskScoreConfidenceScore;
        public String riskScoreExistInNegList;
    }
    
    /*
Class: FinancialInfo
Description: Class to get FinancialInfo
*/
    public without sharing class FinancialInfo { 
        public Decimal fixedMonthlyIncome;
        public Decimal additionalIncome;
        public String noAdditionalIncome;
        public String salarySlipAttached;
    }
    
    /*
Class: PersonalDetails
Description: Class to get PersonalDetails
*/
    public without sharing class PersonalDetails { 
        public String outSystemsID;
        public String salutation;
        public String firstName;
        public String middleName;
        public String lastName;
        public String pan; 
        public String panStatus;
        public String panApplied;
        public Date dateOfBirth;
        public String genderCode;
        public String cityCode;
        public String stateCode;
        public String CountryCode;
        public String cityName;
        public String stateName;
        public String countryName;
        public String residentType;
        public String relationWithPrimaryApplicant;
        public String fatherFirstName;
        public String fatherMiddleName;
        public String fatherLastName;
        public String maritalStatus;
        public String highestQualification;
        public String highestQualificationCode;
        public String socialCategory;
    }
    
    /* 
Method Name :parse
Parameters  :RestRequest request
Description :This method would convert the JSON string to HDFC_DASH_ParseCoApplicantDetails class.
*/
    public static HDFC_DASH_ParseCoApplicantDetails parse(RestRequest request) {
        String requestBody = request.requestBody.toString();
        return (HDFC_DASH_ParseCoApplicantDetails) System.JSON.deserialize(requestBody,HDFC_DASH_ParseCoApplicantDetails.class);
    }
}