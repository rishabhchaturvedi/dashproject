/*
	Author: Sai Shruthi Akkireddy
	Class: HDFC_DASH_APIResponse_LMS
	Description: Apex Class for handling LMS API Response
*/
public inherited sharing class HDFC_DASH_APIResponse_LMS { 
	public Integer return_cd;
	public String return_msg;
	public String lead_no;
	public Lead_Details lead_Details;
	public Appln_form appln_form;

	/* Lead Node */
	public class Lead_Details {
		public String lead_no;
		public String lead_type;
		public String lead_branch_cd;
        public String lead_branch;
		public String lead_name;
		public String resident_type;
		public String employment_type;
		public String lead_city;
		public String lead_country;
		public String lead_status;
		public Date lead_dt;
		public String source;
		public String sub_source;
		public String term_source;
		public String lead_distribution;
		public String lead_owner;
	}
	/*Appln_form Generation Node*/
	public class Appln_form {
	}

/*
	Method: HDFC_DASH_APIResponse_LMS
	Description: Method for Response Parsing
*/	
	public static HDFC_DASH_APIResponse_LMS parse(HttpResponse response) {
          string json = response.getBody();
		return (HDFC_DASH_APIResponse_LMS) System.JSON.deserialize(json, HDFC_DASH_APIResponse_LMS.class);
	}
}