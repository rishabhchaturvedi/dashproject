/**
className: HDFC_DASH_RestResource_DocReviewTest
DevelopedBy: Janani Mohankumar
Date: 27 Oct 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestService_DocReview and HDFC_DASH_RestResource_DocReview.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_RestResource_DocReviewTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string BASEURL = URL.getOrgDomainUrl().toExternalForm();
    private static final string STRING_URL = BASEURL + '/hdfc/documentreview/V1.0';
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Info_Validation__c infoValApp = HDFC_DASH_TestDataFactory_AppInfoValid.getBasicInfoValWithApplication(app.Id);
    
    /* 
Method Name : testGetDocReview()
Parameters  : No
Description :This method will call HDFC_DASH_InboundRestService_DocReview.getDocReview() and test the DocReview API.
*/
    static testmethod void testGetDocReview()
    {
        System.runAs(sysAdmin)
        {
            
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_SF_APPLICATIONID,app.id);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockDocumentReviewResGenerator());
            HDFC_DASH_TestDataFactory_API.createInterfaceSetting_DocReview();
            Test.startTest();
            HDFC_DASH_RestResource_DocReview.getDocReview();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* Method Name: testExceptionAPP_ID_MISSING
parameters: NONE
Method Description: This method is to test Exception scenario which call HDFC_DASH_InboundRestService_DocReview.getDocReview() when sfApplicationId is missing in the request params
*/
    static TestMethod void testExceptionAPP_ID_MISSING() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        Exception testException;
        
        //Request Header and parameter are not added to check whether the exception is throw       
        RestRequest request = new RestRequest();
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
        request.requestUri = STRING_URL;      
        RestContext.request = request;
        
        test.startTest();
        System.runAs(sysAdmin)
        {
            try
            {
                HDFC_DASH_RestResource_DocReview.getDocReview();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    } 
    
}