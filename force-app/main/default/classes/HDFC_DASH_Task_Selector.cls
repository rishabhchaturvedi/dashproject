/**
className: HDFC_DASH_Task_Selector
DevelopedBy:Janani Mohankumar
Date: 27 NOV 2021
Company: Accenture
Class Description: This class would create the select queries for Task object.
**/
public with sharing class HDFC_DASH_Task_Selector {
 private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from Task ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    public static final string  QUERY_LIMIT = ' Limit';
    /* 
Method Name :getTask
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get a list of Task based on one condition.
*/   
    public static List<Task> getTask(List<String> queryParameters,List<string> fieldList, String conditionOn)
    {     
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<Task> listTask = database.query(querystring); 
        return listTask;
    }
    
    /* 
Method Name :getTaskBasedOnQuery
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
Description :This method would get the List of Task based on fieldsAndparameters sent to it.
*/
    public static List<Task> getTaskBasedOnQuery(List<String> fieldList, Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_CONSTANTS.COMMA) + FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters);
       
        List<Task> taskList = database.query(querystring); 
       
        return taskList;   
    }
    
    /* 
Method Name :getTaskBasedOnLimit
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters,Integer queryLimit
Description :This method would get the List of Task based on fieldsAndparametersAndquerylimit sent to it.
*/
    public static List<Task> getTaskBasedOnLimit(List<String> fieldList, Map<String,String> fieldsAndparameters, Integer queryLimit)
    {
        
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_CONSTANTS.COMMA) + FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) + QUERY_LIMIT + HDFC_DASH_CONSTANTS.STRING_SPACE + queryLimit;
        List<Task> taskList = database.query(querystring); 
       
        return taskList;
    }
    
    /* 
Method Name :getCondition
Parameters  :Map<String,String> fieldsAndparameters
Description :This method would be called from getTaskBasedOnQuery.
*/
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }   
    
}