/*
Class: HDFC_DASH_DocDetails_ILPSCallout
Author: Tejeswari
Date: 25 Oct 2021
Company: Accenture
Description: Queueable Class to make Info validation Details Callout to ILPS 
*/
public class HDFC_DASH_DocDetails_ILPSCallout implements Queueable, Database.AllowsCallouts {
    Id recId;
    Boolean sendSingleDoc;
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    RestRequest request;
    Exception exp;
    
    //Add infoValId and sendSingleDoc flag
    public HDFC_DASH_DocDetails_ILPSCallout(Id recId,Boolean sendSingleDoc)
    {
        this.recId = recId;
        this.sendSingleDoc = sendSingleDoc;
    }
    /*
Method: execute
Description: This method will Post the Document Details to ILPS.
*/
    public void execute(QueueableContext context) 
    {
        try
        {             
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();     
            
            //Get the Document details request body
            HDFC_DASH_DocDetailsReq_ILPSCallout reqBody = getRequestBody();    
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_DD);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_DD, HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            processResponse(response);    
           
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_DD, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
        
    }
    /*
Method: getRequestBody
Parameters:None
Description: This method will create the requestBody from the request.
*/ 
    public HDFC_DASH_DocDetailsReq_ILPSCallout getRequestBody(){
        HDFC_DASH_DocDetailsReq_ILPSCallout docDetailsReq = new HDFC_DASH_DocDetailsReq_ILPSCallout();
        List<HDFC_DASH_DocDetailsReq_ILPSCallout.documentDetails> listDocumentDetails = new List<HDFC_DASH_DocDetailsReq_ILPSCallout.documentDetails>();
        Map<string,string> mapCondition = new Map<string,string>();
        List<Opportunity> listApp = new List<Opportunity>();
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>(); 
        List<HDFC_DASH_Info_Validation__c > listinfoval = new List<HDFC_DASH_Info_Validation__c >();
        List<HDFC_DASH_Info_Validation__c > listinfoval_App = new List<HDFC_DASH_Info_Validation__c >();
        List<HDFC_DASH_Info_Validation__c > listinfoval_AppDetail = new List<HDFC_DASH_Info_Validation__c >();
        List<HDFC_DASH_Info_Validation__c > listinfoval_AppDetail_Borrower = new List<HDFC_DASH_Info_Validation__c >();
        List<HDFC_DASH_Info_Validation__c > listinfoval_AppDetail_CoBorrower = new List<HDFC_DASH_Info_Validation__c >();       
        List<Id> listAppId = new List<Id>{recId};   
            List<Id> listAppDetailsID = new List<Id>();
        List<Id> listContactID = new List<Id>();
        
        List<String> listOfFields_App = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber,
            HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch,HDFC_DASH_Constants.SPOT_OFFER_ACCEPTED,
            HDFC_DASH_Constants.SO_REQUIRED_LOAN_AMOUNT,HDFC_DASH_Constants.CS_LOAN_AMOUNT,HDFC_DASH_Constants.EA_LOAN_TERM_MONTHS,
            HDFC_DASH_Constants.CS_TENURE_MONTHS,HDFC_DASH_Constants.CS_INTEREST_RATE,HDFC_DASH_Constants.CS_EMI_AMOUNT,HDFC_DASH_Constants.LOAN_TYPE,
            HDFC_DASH_Constants.CS_INTEREST_TYPE,HDFC_DASH_Constants.FINANCING_FOR,HDFC_DASH_Constants.EA_CURRENT_ROI,HDFC_DASH_Constants.EA_POSSIBLE_TERM_MONTHS,
            HDFC_DASH_Constants.EA_STRETCHED_TERM_MONTHS,HDFC_DASH_Constants.EA_LOAN_POSSIBLE,HDFC_DASH_Constants.EA_STRETCHED_AMOUNT,
            HDFC_DASH_Constants.EA_POSSIBLE_EMI,HDFC_DASH_Constants.EA_STRETCHED_EMI,HDFC_DASH_Constants.EA_NORMAL_IIR,HDFC_DASH_Constants.EA_NORMAL_FOIR,
            HDFC_DASH_Constants.EA_MAX_IIR,HDFC_DASH_Constants.EA_MAX_FOIR,HDFC_DASH_Constants.EA_CAMPAIGN_CODE,HDFC_DASH_Constants.EA_NIIR,HDFC_DASH_Constants.EA_NFOIR,HDFC_DASH_Constants.EA_NLCR,
            HDFC_DASH_Constants.EA_NLVR,HDFC_DASH_Constants.EA_COMB_LCR_LTV,HDFC_DASH_Constants.EA_LTV_VAL,HDFC_DASH_Constants.SO_APP_PROCESSING_FEE, 
            HDFC_DASH_Constants.HDFC_DASH_Property_Locality,HDFC_DASH_Constants.HDFC_DASH_Property_Cost,
            HDFC_DASH_Constants.HDFC_DASH_Name_of_Project_Code,HDFC_DASH_Constants.HDFC_DASH_Name_Of_Project,HDFC_DASH_Constants.PROPERTY_DECIDED, HDFC_DASH_Constants.LOAN_TYPE_CODE};
                
                List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.RECORDTYPE_NAME,
                    HDFC_DASH_Constants.HDFC_DASH_Salutation,HDFC_DASH_Constants.HDFC_DASH_FirstName,HDFC_DASH_Constants.HDFC_DASH_MiddleName,
                    HDFC_DASH_Constants.HDFC_DASH_LastName,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.HDFC_DASH_PAN,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,
                    HDFC_DASH_Constants.HDFC_DASH_Resident_Type,HDFC_DASH_Constants.HDFC_DASH_Mobile,HDFC_DASH_Constants.HDFC_DASH_Email,HDFC_DASH_Constants.RELATION_WITH_PRIMARYAPPLICANT,
                    HDFC_DASH_Constants.FIXED_MONTHLY_INCOME,HDFC_DASH_Constants.ADDITIONAL_INCOME, HDFC_DASH_Constants.APPDETAIL_CONTACT_R_CONTACT_NUMBER};
                        
                        list<string> listOfFields_InfoVal = new list<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, HDFC_DASH_Constants.DOCTYPE, HDFC_DASH_Constants.HDFC_DASH_Doc_Type_Code, HDFC_DASH_Constants.FILENO,
                            HDFC_DASH_Constants.MONTHOFPAYSLIP,HDFC_DASH_Constants.HDFC_DASH_Salary_Year, HDFC_DASH_Constants.MONTHOFGSTRETURN, HDFC_DASH_Constants.ASSESSMENTYEAR, HDFC_DASH_Constants.FINANCIALYEAR, HDFC_DASH_Constants.ACCOUNTTYPE, HDFC_DASH_Constants.HDFC_DASH_Account_Number, HDFC_DASH_Constants.FROMDATE,
                            HDFC_DASH_Constants.TODATE, HDFC_DASH_Constants.BANKCODE, HDFC_DASH_Constants.HDFC_DASH_Employer_Name, HDFC_DASH_Constants.TOTALAMOUNTPAID, HDFC_DASH_Constants.NOOFQUARTERSPAID, HDFC_DASH_Constants.PROOFOFCOMMUNICATION, HDFC_DASH_Constants.KYCDOCTYPE, HDFC_DASH_Constants.HDFC_DASH_KYC_Doc_Code,
                            HDFC_DASH_Constants.BCPDOCTYPE,HDFC_DASH_Constants.HDFC_DASH_BCP_Doc_Code, HDFC_DASH_Constants.DOCEXTRACTABLE, HDFC_DASH_Constants.DOCSCANNEDBY, HDFC_DASH_Constants.DOCIMAGEORIGIN, HDFC_DASH_Constants.HDFC_DASH_Location_Latitude, HDFC_DASH_Constants.HDFC_DASH_Location_Longitude, HDFC_DASH_Constants.STORAGE_IDENTIFIER,
                            HDFC_DASH_Constants.STORAGE_TYPE, HDFC_DASH_Constants.APPDETAILS_R_APPLICATION, HDFC_DASH_Constants.APPDETAILS_R_APPLICANTCAPACITY, HDFC_DASH_Constants.APPDETAILS_R_CONTACT,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.HDFC_DASH_ILPS_Cust_Number,
                            HDFC_DASH_Constants.HDFC_DASH_Customer_Number_Formula, HDFC_DASH_Constants.HDFC_DASH_Info_Validation_Record_Type,HDFC_DASH_Constants.HDFC_DASH_SF_Triggered,HDFC_DASH_Constants.HDFC_DASH_Is_Salary_Account};
                                
                                List<string> listOfFields_Con = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Contact_Number,HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number};
                                    Opportunity app = new Opportunity();
        
        if(!sendSingleDoc)
        {
            listApp = HDFC_DASH_Application_Selector.getApplication(listAppId, listOfFields_App, HDFC_DASH_Constants.ID_STRING); 
            app = listApp[HDFC_DASH_Constants.INT_ZERO];
            
            listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppId, listOfFields_AppDetails, HDFC_DASH_Constants.HDFC_DASH_Application);
            for(HDFC_DASH_Applicant_Details__c appDetail : listAppDetails)
            {
                listAppDetailsID.add(appDetail.Id);                      
            }   
            
            if(!listAppDetails.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAppId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
                mapCondition.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAppDetailsID, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE + HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
            else{
                mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAppId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
        }
        else{
            mapCondition.put(HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + recId +HDFC_DASH_Constants.STRING_QUOTE);
        }
        mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Info_Validation_Record_Type, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_DOCUMENT + HDFC_DASH_Constants.STRING_QUOTE);
        mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_DOCUMENT_RECORD_SERIAL_No, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_NULL);
        mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_SF_Triggered, HDFC_DASH_Constants.STRING_NOTEQUALTO + HDFC_DASH_Constants.BOOLEAN_TRUE);
        
        listinfoval = HDFC_DASH_AppInfoValidation_Selector.getappInfoValBasedOnQuery(listOfFields_InfoVal, mapCondition);
        //system.debug('listinfoval '+listinfoval.size());
        for(HDFC_DASH_Info_Validation__c  infoVal : listinfoval)
        {
            HDFC_DASH_DocDetailsReq_ILPSCallout.documentDetails documentDetailsnode = new HDFC_DASH_DocDetailsReq_ILPSCallout.documentDetails();
            
            documentDetailsnode.sfCustNo = infoVal.HDFC_DASH_Customer_Number_Formula__c;
            documentDetailsnode.sfApplId = app?.HDFC_DASH_ApplicationNumber__c;            
            
            documentDetailsnode = populateData(documentDetailsnode, infoVal);
            
            listDocumentDetails.add(documentDetailsnode);			
        }
        
        docDetailsReq.documentDetails = listDocumentDetails;
        return docDetailsReq; 
        
    }
    
    
    /*
Method: populateData
Description: This method will populate data from document object to the request body.
*/
    public HDFC_DASH_DocDetailsReq_ILPSCallout.documentDetails populateData(HDFC_DASH_DocDetailsReq_ILPSCallout.documentDetails documentDetailsnode, HDFC_DASH_Info_Validation__c infoVal){
        
        documentDetailsnode.fileNo = infoVal?.HDFC_DASH_File_Number__c;
        documentDetailsnode.docType = infoVal?.HDFC_DASH_Doc_Type_Code__c;
        documentDetailsnode.custNo = infoVal?.HDFC_DASH_ILPS_Cust_Number__c;
        documentDetailsnode.capacity = infoVal?.HDFC_DASH_Applicant_Capacity__c;
        if(infoVal?.HDFC_DASH_Salary_Year__c!=null && infoVal?.HDFC_DASH_Salary_Month__c!=null)
        {
            String salaryYear=String.valueOf(infoVal?.HDFC_DASH_Salary_Year__c);
            if(salaryYear.length()== HDFC_DASH_Constants.INT_FOUR && infoVal?.HDFC_DASH_Salary_Month__c.length()>HDFC_DASH_Constants.INT_TWO){
                documentDetailsnode.monthOfPayslip = infoVal?.HDFC_DASH_Salary_Month__c?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) + HDFC_DASH_Constants.STRING_DASH + salaryYear?.substring(HDFC_DASH_Constants.INT_TWO,HDFC_DASH_Constants.INT_FOUR) ;
            }
            
        }
        documentDetailsnode.monthOfGstReturn = infoVal?.HDFC_DASH_Mon_Of_GST_Return__c;
        documentDetailsnode.assessmentYear = infoVal?.HDFC_DASH_Assessment_Year__c;
        documentDetailsnode.financialYear = infoVal?.HDFC_DASH_Financial_Year__c;
        documentDetailsnode.accountType = infoVal?.HDFC_DASH_Account_Type__c;
        documentDetailsnode.bkAccNo = infoVal?.HDFC_DASH_Account_Number__c;
        documentDetailsnode.fromDate = infoVal?.HDFC_DASH_From_Date__c;
        documentDetailsnode.toDate = infoVal?.HDFC_DASH_To_Date__c;
        documentDetailsnode.bankCode= infoVal?.HDFC_DASH_Bank_Code__c;
        documentDetailsnode.currentEmployer = infoVal?.HDFC_DASH_Employer_Name__c;
        documentDetailsnode.totalAmountPaid = infoVal?.HDFC_DASH_Total_Amount_Paid__c;
        documentDetailsnode.noOfQuartersPaid =Integer.valueOf( infoVal?.HDFC_DASH_No_Of_Quarters_Paid__c);
        documentDetailsnode.proofOfCommunication = infoVal?.HDFC_DASH_Proof_Of_Communication__c;
        documentDetailsnode.kycDocType = infoVal?.HDFC_DASH_KYC_Doc_Code__c;
        documentDetailsnode.bcpDocType = infoVal?.HDFC_DASH_BCP_Doc_Code__c;
        documentDetailsnode.docExtractable = infoVal?.HDFC_DASH_Document_Extractable__c;
        documentDetailsnode.docScannedBy = infoVal?.HDFC_DASH_Document_Scanned_By__c;
        documentDetailsnode.docImageOrigin = infoVal?.HDFC_DASH_Document_Image_Origin__c;
        documentDetailsnode.latitute = infoVal?.HDFC_DASH_Location__Latitude__s;
        documentDetailsnode.longitute = infoVal?.HDFC_DASH_Location__Longitude__s;
        documentDetailsnode.storageIdentifier = infoVal?.HDFC_DASH_Storage_Identifier__c;
        documentDetailsnode.storageType = infoVal?.HDFC_DASH_Storage_Type__c;
        documentDetailsnode.isSalaryAccount = infoVal?.HDFC_DASH_Is_Salary_Account__c;
        return documentDetailsnode;
    }
    /*
Method: processResponse
Parameters: Response
Description: This method will process the response and store Document Serial number into document object.
*/     
    public void processResponse(HttpResponse response)
    {
        Map<string,string> resMap = new Map<string,string>();
        List<string> listStorageIdentifier= new List<string>();
        
        if(response != null && response.getStatusCode() == HDFC_DASH_Constants.INT_TWO_HUNDRED)
        {
            HDFC_DASH_ParseResponseDocDetailsILPS calloutResponse = HDFC_DASH_ParseResponseDocDetailsILPS.parse(response.getBody());
            
            for(HDFC_DASH_ParseResponseDocDetailsILPS.DocumentDetail doc : calloutResponse?.document_details )
            {
                resMap.put(doc?.storage_identifier, doc?.doc_rec_srno);
                listStorageIdentifier.add(doc?.storage_identifier);
            }
            
            List<HDFC_DASH_Info_Validation__c > listDocs = HDFC_DASH_AppInfoValidation_Selector.getAppInfoVal(listStorageIdentifier,new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STORAGE_IDENTIFIER,HDFC_DASH_Constants.HDFC_DASH_DOCUMENT_RECORD_SERIAL_No} ,HDFC_DASH_Constants.STORAGE_IDENTIFIER);
            for(HDFC_DASH_Info_Validation__c  docToUpdate : listDocs)
            {
                if(resMap.get(docToUpdate.HDFC_DASH_Storage_Identifier__c) != null){
                    docToUpdate.HDFC_DASH_Document_Record_Serial_No__c = resMap.get(docToUpdate.HDFC_DASH_Storage_Identifier__c);
                }
            }
            if(Schema.sObjectType.HDFC_DASH_Info_Validation__c.isUpdateable()){
                Database.Update(listDocs);
            }
        }  
    }
}