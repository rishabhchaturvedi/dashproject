/*
Class: HDFC_DASH_RiskScore_CalloutTest
Author: Punam Marbate
Date: 5 Aug 2021
Company: Accenture
Description:This is the test class for Queueable Class for RiskScore_Callout.
*/
@isTest(SeeAllData=false)
public with sharing class HDFC_DASH_RiskScore_CalloutTest {
   
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    
     
    /* Method Name: testRiskScoreCallout
    parameter:none
    Method Description: This method will enqueue and test the Risk Score callout.
    */ 
    static testmethod void testRiskScoreCallout(){
        System.runAs(sysAdmin)
        {
            
            acc.HDFC_DASH_Customer_Number__c='8013821';
            Database.update(acc);
            appDetails.HDFC_DASH_Contact__c=con.Id;
           //app.ContactID__c=con.Id;
            Database.update(app);
            ID jobID=null;
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockRiskScoreResGenerator());
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_TestDataFactory_API.createInterfaceSetting_RiskScore();
            HDFC_DASH_RiskScore_Callout riskScoreCallOut  = new HDFC_DASH_RiskScore_Callout(app.Id);
            Test.startTest();
            jobID= System.enqueueJob(riskScoreCallOut);
            Test.stopTest();   
            System.assertNotEquals(null,jobID);          
        }        
    }
    /* Method Name: testRiskScoreCalloutException
    Method Description: This method will test the exception scenario for Risk Score CAllout...
    */
    static testmethod void testRiskScoreCalloutException(){
        System.runAs(sysAdmin){
          
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockRiskScoreResGenerator());
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_RiskScore_Callout riskScoreCallOut  = new HDFC_DASH_RiskScore_Callout(app.Id);
            ID jobID=null;
            Test.startTest();
            jobID = System.enqueueJob(riskScoreCallOut);
            Test.stopTest();          
            System.assertNotEquals(null,jobID);
        }
    }
}