/**
* 	className: HDFC_DASH_BatchNotification
DevelopedBy: Anisha Arumugam
Date: 25 January 2022
Company: Accenture
Class Description: This class would create platform events.
**/
global class HDFC_DASH_BatchNotification implements Database.Batchable<AggregateResult> 
{
    // The batch job starts
    /*  
Method Name :start
Parameters  :Database.BatchableContext BC 
Description :This method would query the list of Events where Missedtype is true and startDateTime istoday and activity type.
*/
    global Iterable<AggregateResult> start(Database.BatchableContext bc)
    {
        //constants - All values, Event obj, noOfMissedActivities, actorExecutiveCode, 
        //String query = 'Select count(Id) ' + HDFC_DASH_Constants.NOOFMISSEDACTIVITIES +' , HDFC_DASH_Actor_Executive_Code_Text__c ' + HDFC_DASH_Constants.ACTOREXECUTIVECODE + ' from ' + HDFC_DASH_Constants.EVENT_OBJECT + ' where Type = ' + HDFC_DASH_Constants.STRING_SINGLE_QUOTE + HDFC_DASH_Constants.STRING_ACTIVITY + HDFC_DASH_Constants.STRING_SINGLE_QUOTE + ' and StartDateTime = Today and HDFC_DASH_Missed__c = ' + HDFC_DASH_Constants.BOOLEAN_TRUE + ' and HDFC_DASH_Actor_Executive_Code_Text__c !=' + HDFC_DASH_Constants.STRING_NULL + ' group by HDFC_DASH_Actor_Executive_Code_Text__c';
        //
        String query = 'Select count(Id) ' + HDFC_DASH_Constants.NOOFMISSEDACTIVITIES +' , HDFC_DASH_Actor_Executive_Code_Text__c ' + HDFC_DASH_Constants.ACTOREXECUTIVECODE + ' from ' + HDFC_DASH_Constants.EVENT_OBJECT + ' where Type = ' + HDFC_DASH_Constants.STRING_SINGLE_QUOTE + HDFC_DASH_Constants.STRING_ACTIVITY + HDFC_DASH_Constants.STRING_SINGLE_QUOTE + ' and HDFC_DASH_User_Type__c = ' + HDFC_DASH_Constants.STRING_SINGLE_QUOTE + HDFC_DASH_Constants.STRING_HSALES + HDFC_DASH_Constants.STRING_SINGLE_QUOTE + ' and StartDateTime = Today and HDFC_DASH_Missed__c = ' + HDFC_DASH_Constants.BOOLEAN_TRUE + ' and HDFC_DASH_Actor_Executive_Code_Text__c !=' + HDFC_DASH_Constants.STRING_NULL + ' group by HDFC_DASH_Actor_Executive_Code_Text__c';
        System.debug('***Batch Started... ' + query);
        
        return new HDFC_DASH_AggregateResultIterable(query);
    } 
    
    // The batch job executes and operates on one batch of records
    /* 
Method Name :execute
Parameters  :Database.BatchableContext BC, List<sObject> scope
Description :This method would create platform event notifications.
*/ 
    global void execute(Database.BatchableContext bc, List<sObject> scope)
    { 
        List<HDFC_DASH_Notifications__e> listNotificationPE = new List<HDFC_DASH_Notifications__e>();
        List<string> listActorExecutiveCode = new List<string>();
        List<Event> listEvents = new List<Event>();
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Actor_Executive_Code_Text,  HDFC_DASH_Constants.HDFC_DASH_User_Type}; //
            Map<String, Event> mapEvent = new Map<String, Event>();
        
        try
        {
            for(sObject sObj : scope) 
            {
                AggregateResult agrResult = (AggregateResult)sObj;
                listActorExecutiveCode.add(String.ValueOf(agrResult.get(HDFC_DASH_Constants.ACTOREXECUTIVECODE))); //
            }
            System.debug('***listActorExecutiveCode Size : ' + listActorExecutiveCode.size());
            listEvents = HDFC_DASH_Event_Selector.getEvent(listActorExecutiveCode, fieldList, HDFC_DASH_Constants.HDFC_DASH_Actor_Executive_Code_Text); //
            
            for(Event e : listEvents)
            {
                mapEvent.put(e.HDFC_DASH_Actor_Executive_Code_Text__c, e);
            }
            
            HDFC_DASH_Notification_Config__mdt mdt_NotificationConfig = HDFC_DASH_Notification_Config__mdt.getInstance(HDFC_DASH_Constants.MISSED_EVENTNOTIFICATION); //
                
            for(sObject sObj : scope) 
            {
                AggregateResult agrResult = (AggregateResult)sObj;
                System.debug('***COUNT Missed Activities: ' + agrResult.get('noOfMissedActivities'));
                System.debug('***actorExecutiveCode: ' + agrResult.get('actorExecutiveCode'));
                
                if(mdt_NotificationConfig != null)
                {
                    HDFC_DASH_Notifications__e pe = new HDFC_DASH_Notifications__e();
                    
                    pe.HDFC_DASH_Msg_Title__c = mdt_NotificationConfig.HDFC_DASH_Message_Title_Template__c;
                    
                    //if(String.ValueOf(agrResult.get(HDFC_DASH_Constants.ACTOREXECUTIVECODE)) =='12345')
                    pe.HDFC_DASH_Msg_Display__c = mdt_NotificationConfig.HDFC_DASH_Message_Display_Template__c.replace(HDFC_DASH_Constants.HDFC_DASH_NOOFMISSEDACTIVITIES, String.ValueOf(agrResult.get(HDFC_DASH_Constants.NOOFMISSEDACTIVITIES))); //
                    
                    pe.HDFC_DASH_Msg_Actual__c = mdt_NotificationConfig.HDFC_DASH_Message_Actual_Template__c;
                    
                    pe.HDFC_DASH_Expiry_DateTime__c = DateTime.Now() + (mdt_NotificationConfig.HDFC_DASH_Validity__c/1440); 
                    
                    pe.HDFC_DASH_Message_Type__c = mdt_NotificationConfig.HDFC_DASH_MessageType__c;
                    
                    pe.HDFC_DASH_User_Type__c = mapEvent.get(String.ValueOf(agrResult.get(HDFC_DASH_Constants.ACTOREXECUTIVECODE))).HDFC_DASH_User_Type__c; //
                    
                    pe.HDFC_DASH_User_Id__c = String.ValueOf(agrResult.get(HDFC_DASH_Constants.ACTOREXECUTIVECODE)); //
                    
                    listNotificationPE.add(pe);
                }
            }
            
            List<Database.SaveResult> results = EventBus.publish(listNotificationPE);
            
            // Inspect publishing result for each event
           /* for (Database.SaveResult sr : results) 
            {
                if (sr.isSuccess()) {
                    System.debug('***Result - Successfully published event.');
                } 
                else {
                    for(Database.Error e : sr.getErrors()) {
                        System.debug('***Result - Error returned: ' + e.getStatusCode() + ' - ' + e.getMessage());
                        
                        String expDetails = e.getStatusCode() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e;
                        system.debug('expDetails => '+expDetails);
                        
                        HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_NOTIFICATIONMESSAGE, HDFC_DASH_Constants.GETMESSAGEBODY, string.ValueOf(scope.size()), expDetails); //               
                    }
                }            
            }*/
        }
        catch(exception e)
        {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();
            system.debug('expDetails => '+expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_NOTIFICATIONMESSAGE, HDFC_DASH_Constants.GETMESSAGEBODY, string.ValueOf(scope.size()), expDetails); 
        }
    }
    
    
    // The batch job finishes
    /* 
Method Name :finish
Parameters  :Database.BatchableContext BC
Description :
*/
    global void finish(Database.BatchableContext bc)
    { }
}