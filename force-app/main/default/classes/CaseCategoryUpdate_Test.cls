@isTest
public class CaseCategoryUpdate_Test {
	
    @isTest
    public static void updateMethod1(){
        List<Case> caseList = new List<Case>();
        Case c1 = new Case(
        	HDFC_DASH_Category__c = 'Website / App Services',
            HDFC_DASH_Sub_Category__c = 'Enquiry for Portal / Mobile Access',
            HDFC_DASH_Case_Type__c = 'Query',
            HDFC_DASH_Mode__c = 'Email',
            HDFC_DASH_Sub_mode__c = 'Email'
        );
        caseList.add(c1);
        
        Case c2 = new Case(
        	HDFC_DASH_Category__c = 'Accounts - Transaction Related',
            HDFC_DASH_Sub_Category__c = 'EMI / Pre-emi Refund',
            HDFC_DASH_Case_Type__c = 'Query',
            HDFC_DASH_Mode__c = 'Email',
            HDFC_DASH_Sub_mode__c = 'Email'
        );
        caseList.add(c2);
        
        Test.startTest();
        	database.insert(caseList);
        	id jobId = database.executeBatch(new CaseCategoryUpdate());
        Test.stopTest();
               
    }
}