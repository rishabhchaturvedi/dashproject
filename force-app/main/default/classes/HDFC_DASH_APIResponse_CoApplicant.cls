/**
* 	className: HDFC_DASH_APIResponse_CoApplicant
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class is a wrapper class for HDFC_DASH_RestResource_CoApplicant for sending the API response . 
**/
public  inherited sharing class HDFC_DASH_APIResponse_CoApplicant {  
     
   
	public String doesCoApplicantExist;
	//public Boolean isCustomer;
	public Boolean isExistingCustomer;
	public Application application;
	/*CoApplicant Node*/
	public class CoApplicant {
		public String sfApplicantId;
        public String sfCustomerId;
        public String sfCustomerNumber;
	}
	/*Application Node*/
	public class Application {
		public String sfApplicationId;
		public String sfApplicationNumber;
		public CoApplicant coApplicant;
	}
}