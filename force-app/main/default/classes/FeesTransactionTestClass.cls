@isTest
public class FeesTransactionTestClass {

    @isTest     
    public static void feeTransactionTest(){
        Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
       	acc.PersonEmail = 'test123@salesforce.com';
        update acc;
        Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
        app.HDFC_DASH_File_Number__c = app.HDFC_DASH_File_Number__c.left(9);
        app.StageName = 'Onboarding';
        app.HDFC_DASH_Credit_Appraiser__c = UserInfo.getUserId();
        update app;
        
        Test.startTest();
        Fees_Transaction__x f = new Fees_Transaction__x(
        	FILE_NO__c = '123456789',
            TRANS_CD__c = 'PF',
            AMT_RECD__c = '2',
            AMT_RECBLE__c = '0'            
        );
        
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,app.Id);		        
        insert caseRec2;
        HDFC_DASH_FeeTransactionAssignment.mockedRequests.add(f);
        HDFC_DASH_FeeTransactionAssignment.setCaseOwner(caseRec2.Id, app.HDFC_DASH_File_Number__c);
        Test.stopTest();
        
    }
    
     @isTest     
    public static void feeTransactionTest2(){
        Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
       	acc.PersonEmail = 'test123@salesforce.com';
        update acc;
        Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
        app.HDFC_DASH_File_Number__c = app.HDFC_DASH_File_Number__c.left(9);
        app.StageName = 'Onboarding';
        app.HDFC_DASH_Credit_Appraiser__c = UserInfo.getUserId();
        update app;
        
        Test.startTest();
        Fees_Transaction__x f = new Fees_Transaction__x(
        	FILE_NO__c = '123456789',
            TRANS_CD__c = 'PF',
            AMT_RECD__c = '0',
            AMT_RECBLE__c = '2'            
        );
        
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,app.Id);		        
        insert caseRec2;
        HDFC_DASH_FeeTransactionAssignment.mockedRequests.add(f);
        HDFC_DASH_FeeTransactionAssignment.setCaseOwner(caseRec2.Id, app.HDFC_DASH_File_Number__c);
        Test.stopTest();
        
    }
}