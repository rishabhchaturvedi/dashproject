/**
className: HDFC_DASH_MockPollingPaymentResGenerator
Author: Ashita Jain
Date: 29 OCT 2021
Company: Accenture
Class Description: This is mock class used for generating response for Polling Payment Status Batch Class.
**/ 
@isTest(SeeAllData=false)
global with sharing class HDFC_DASH_MockPollingPaymentResGenerator implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req) {
        
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        HttpResponse res = new HttpResponse();
      
            // Create a fake response
            res.setHeader('Content-Type', 'application/json'); 
            res.setBody('{"msg":"Success","transactionId":"3534534","transactionStatus":"0002"}');
            res.setStatusCode(200);
                
        
        return res;
    }
}