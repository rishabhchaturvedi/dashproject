/*
Class: HDFC_DASH_APIResponse_ConfidenceRuleEng
Author:Sai Suman
Date: 03 December 2021
Company: Accenture
Description:This class will define the response body of confidence rule engine callout.
*/
public class HDFC_DASH_APIResponse_ConfidenceRuleEng {


	public String status;
	public String message;
	public List<Response> response;


	public class Response{
		public String status;
		public ResponseClass response;
	}

	public class ResponseClass{
		public BRE bre;
	}

	public class BRE{
		public SUMMARY summary;
		public List<Applicant_Result> APPLICANT_RESULT;
	}

	public class Summary{
		public Integer APPLICATIONAPPROVEDAMOUNT;
		public String Status;
	}

	public class Applicant_Result{
		public String STATUS;
		public List<Rules> RULES;
		public String APPLICANT_ID;
	}

	public class Rules{
		//public Integer CriteriaID;
		public String RuleName;
	}
	/*	
		public String status;	//SUCCESS
		public String message;	//Successfully Executed!
		public list<response> response;
	public class response {
			public response response;
			public BRE BRE;
			public String acknowledgmentId;	//61aa04c6a06b9f66b4ca8ebc
		}
	public class BRE {
			public SUMMARY SUMMARY;
			public list<APPLICANT_RESULT> APPLICANT_RESULT;
		}
	public class SUMMARY {
			public Integer APPLICATION-APPROVED-AMOUNT;	//0
			public String APPLICATION-DECISION;	//Declined
			public String APPLICATION-SCORE;	//
			public String STATUS;	//SUCCESS
		}
	public class APPLICANT-RESULT {
			public String STATUS;	//SUCCESS
			public DERIVED_FIELDS DERIVED_FIELDS;
			public String SCORING-REF-ID;	//163853230518421
			public list<RULES> RULES;
			public String ELIGIBILITY-AMOUNT;	//0.0
			public String ELIGIBILITY-APPROVED-AMOUNT;	//0.0
			public String ELIGIBILITY-DECISION;	//
			public String DECISION;	//Declined
			public Integer POLICY_ID;	//4
			public String POLICY_NAME;	//CONFSCORE POSTFEE
			public String APPLICANT-ID;	//1
		}
	public class DERIVED_FIELDS {
			public String POLICY_NAME;	//CONFSCORE POSTFEE
			public Integer POLICY_ID;	//4
		}
	public class RULES {
			public Integer CriteriaID;	//1
			public String RuleName;	//216
			public Values Values;
			public String Outcome;	//Declined
			public String Remark;	//Update Property Details
			public String Exp;	// ( ( LOAN_PRODUCT_TYPE is not HOU ) && ( LOAN_PRODUCT_TYPE is not LND ) &&( LOAN_PRODUCT_TYPE is not LNC ) &&( LOAN_PRODUCT_TYPE is not NRP )  ) 
		}
	public class Values {
			public String LOAN_PRODUCT_TYPE;	//HO

	/*
	Method: parse
	Parameters:String json
	Description: This method will parse the Response.
	*/
		public static HDFC_DASH_APIResponse_ConfidenceRuleEng parse(String response) {
			string json = response;
			//system.debug('In parse ' + json);
			return (HDFC_DASH_APIResponse_ConfidenceRuleEng) System.JSON.deserialize(json, HDFC_DASH_APIResponse_ConfidenceRuleEng.class);
		}
	}