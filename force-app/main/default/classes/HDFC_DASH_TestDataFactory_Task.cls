/*   
ClassName: HDFC_DASH_TestDataFactory_Task
DevelopedBy: Pratheeksha Bayari
Date: 21 Nov 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
                      used throughout HDFC DASH.
                      In this class we will be creating data for Task object 
*/

public class HDFC_DASH_TestDataFactory_Task {

    public static final String OwnerIdStr = UserInfo.getUserId();
    public static final String STRING_TYPE = 'Activity';
    public static final String STRING_STATUS = 'Open';
             
    /* Method Name: createBasicTask
    parameters: None
    Class Description: This method is to generate test data for Task.
    */
    public static Task createBasicTask()
    {
        Task taskRec = new Task();
        taskRec.OwnerId = OwnerIdStr;        
        return taskRec;
    }
    
    /* Method Name: createBasicTask
parameters:Id AppId,Id ConId,Id UserId 
Method Description: This method is to create test data for Task.
Pass fields of Task Object
*/
    public static Task createBasicTask(Id AppId,Id ConId,Id UserId)
    {
        Task objTask = new Task();
        
        objTask.Type=STRING_TYPE;
        objTask.Status=STRING_STATUS;
        objTask.OwnerId = UserId;
        objTask.WhatId = AppId;
        objTask.WhoId = ConId;
        objTask.HDFC_DASH_BSA__c = ConId;
        return objTask;        
    }
    
    /* Method Name: getBasicTask
parameters: Id AppId,Id ConId,Id UserId
Method Description: This method is to generate test data for Task.
*/
    public static Task getBasicTask(Id AppId,Id ConId,Id UserId)
    {
        Task taskDetails = HDFC_DASH_TestDataFactory_Task.createBasicTask(AppId, ConId, UserId);
        if(Schema.sObjectType.Task.isCreateable()){
            Database.insert(taskDetails);
        }
        return taskDetails;
    }
    
    
}