/*
	Author: Anisha Arumugam
	Class: HDFC_DASH_Applicant_Detail_Selector
	Description: Apex Class for applicant detail selector
*/
public with sharing class HDFC_DASH_Applicant_Detail_Selector {
    
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Applicant_Details__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    private static final string QUERY_PARAMETER1 = '=:queryParameters1';
    private static final string AND_STRING = ' AND ';
    private static final string QUERY_RECORDTYPE = 'recordTypeId=:';
    private static final string RECORDTYPEIDVAL = 'recordTypeId';    
    private static final string QUERY_PARAMETER2 = '=:queryParameters2';
    private static final string QUERY_PARAMETER3 = ' = :queryParameter';
        
/*
	Method: getApplicationDetails
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
	Description: Fetch application Details based on only one condition.
*/
    public static List<HDFC_DASH_Applicant_Details__c> getApplicationDetails( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;  
        
        List<HDFC_DASH_Applicant_Details__c> resultList = database.query(querystring);        
        return resultList;
    }         
    
    /*
      Name: getApplicationDetailsByRecordType
      Parameters  :List<String> queryParameters, String conditionOn, string recordType
      Description: Method to fetch Application Details based on the Record type
    */
    public static List<HDFC_DASH_Applicant_Details__c> getApplicationDetailsByRecordType(List<String> queryParameters, List<string> fieldList, String conditionOn, string recordType)
    {   
        Id recordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();

        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(queryParameters, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE + 
            + AND_STRING + QUERY_RECORDTYPE + RECORDTYPEIDVAL ;  

        List<HDFC_DASH_Applicant_Details__c> resultList = database.query(querystring);        
        return resultList;
    } 
    
        /* 
Method Name :getApplicantDetailBasedOnId
Parameters  :List<string> fieldList, Id ApplicantDetailsId
Description :This method would get a single Applicant details based on the ID.
*/   
    public static HDFC_DASH_Applicant_Details__c getApplicantDetailBasedOnId(List<string> fieldList, Id ApplicantDetailsId){
        
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT+ WHERE_STRING+ ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ApplicantDetailsId+ HDFC_DASH_Constants.STRING_QUOTE;
        HDFC_DASH_Applicant_Details__c applicantDetailRec = database.query(querystring); 
        return applicantDetailRec;
    }

     /* 
    Method Name :getApplicantDetailBasedOnIds
    Parameters  :List<string> fieldList,List<ID> ApplicantDetailsIds
    Description :This method would get the Applicant details based on the IDs.
    */
        public static List<HDFC_DASH_Applicant_Details__c> getApplicantDetailBasedOnIds(List<string> fieldList,List<ID> ApplicantDetailsIds){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT+ WHERE_STRING+ HDFC_DASH_CONSTANTS.STRING_ID + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(ApplicantDetailsIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
            List<HDFC_DASH_Applicant_Details__c> appDetList = database.query(querystring); 
            return appDetList;
        }
    
     /* 
    Method Name :getAppDetailsBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Applicant Details based on fieldsAndparameters sent to it.
    */
        public static List<HDFC_DASH_Applicant_Details__c> getAppDetailsBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters);
            system.debug('AD querystring => ' + querystring);
            List<HDFC_DASH_Applicant_Details__c> appDetList = database.query(querystring); 
            return appDetList;   
        }
        
        /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getAppDetailsBasedOnQuery .
    */
        public static String getCondition(Map<String,String> fieldsAndparameters){
            String condition ='';
            for(String fieldName : fieldsAndparameters.keySet()){
                condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
            }
            return condition;
        }
    
    /* 
    Method Name :getappDetailRecsBasedOnString
    Parameters  :List<string> fieldList,String queryParameter,String conditionField
    Description :This method would get the List of Applicant Details based on single string condition sent to it.
    */
    public static List<HDFC_DASH_Applicant_Details__c> getappDetailRecsBasedOnString(List<string> fieldList,String queryParameter,String conditionField){
		string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
            				 +FROM_OBJECT + WHERE_STRING
            				 +conditionField +QUERY_PARAMETER3;
        List<HDFC_DASH_Applicant_Details__c> appDetailRecsList = database.query(querystring);
        return appDetailRecsList;
    }
    
}