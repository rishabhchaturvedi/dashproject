/*
Class: HDFC_DASH_ParseResponseRiskScore
Author: Punam Marbate
Date: 5 Aug 2021
Company: Accenture
Description:This class will parse the response body of Risk Score callout.
*/
public with sharing class HDFC_DASH_ParseResponseRiskScore {
	/*
	Class:CustRiskDetail
	Description: class to parse CustRiskDetail.
	*/
    public class CustRiskDetail{
        public String customerStatus;
		public String customerFlags;
		public String rtrExists;
		public String customerGrade;
		public String riskEvent;
		public String riskScoreAvailedMoratorium;
		public String riskScoreAvailedRestruct;
		public String riskScoreHdfcStaff;
		public String riskScoreConfidenceScore;
		public String riskScoreExistInNegList; 
    }
	public List<CustRiskDetail> custRiskDetail;
	public string errorMessage;
	/*
	Method: parse
	Parameters:String json
	Description: This method will parse the Response.
	*/ 
    public static HDFC_DASH_ParseResponseRiskScore parse(String json) {
		return (HDFC_DASH_ParseResponseRiskScore) System.JSON.deserialize(json, HDFC_DASH_ParseResponseRiskScore.class);
	}

}