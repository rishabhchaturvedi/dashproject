/*
Class: HDFC_DASH_RuleEngCapture_ILPSCalloutTest
Author: Ashita
Date: 15 Dec 2021
Company: Accenture
Description:This is the test class for Queueable Class for HDFC_DASH_RuleEngineCapture_ILPSCallout.
*/
@isTest(SeeAllData=false)
public with sharing class HDFC_DASH_RuleEngCapture_ILPSCalloutTest {
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDet = 
        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c applEmpDet =
        HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDet.Id,con.id);
    private static HDFC_DASH_Rule_Engine_Capture__c ruleEngine =
        HDFC_DASH_TestDataFactory_RuleEngine.getBasicRuleEngineCapture(app.id,appDet.Id);
    
    
    /* Method Name: testRuleEngineCaptureILPSCallout
	parameter:none
	Method Description: This method will enqueue and test the Rule Engine Capture ILPS Callout.
	*/ 
    static testmethod void testRuleEngineCaptureILPSCallout(){
        System.runAs(sysAdmin)
        { 			
            ID jobID=null;
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_TestDataFactory_API.createInterfaceSetting_RuleEng_ILPSCallout();
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockRuleEngCaptureILPSResGen());
            HDFC_DASH_RuleEngineCapture_ILPSCallout ruleEngCaptureILPSCallOut  = new HDFC_DASH_RuleEngineCapture_ILPSCallout(app.Id);
            Test.startTest();
            jobID = System.enqueueJob(ruleEngCaptureILPSCallOut);
            Test.stopTest();  
            System.assertNotEquals(null,jobID);
        }
    }
    /*
	Method Name: testRuleEngILPSCalloutException
	Method Description: This method will test the exception scenario for Rule Engine ILPS callout
	*/
    static testmethod void testRuleEngILPSCalloutException(){
        System.runAs(sysAdmin)
        {
            ID jobID=null;
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_TestDataFactory_API.createInterfaceSettingPollingPaymentStatus();
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockRuleEngCaptureILPSResGen());
            HDFC_DASH_RuleEngineCapture_ILPSCallout ruleEngCaptureILPSCallOut  = new HDFC_DASH_RuleEngineCapture_ILPSCallout(app.Id);
            Test.startTest();
            jobID = System.enqueueJob(ruleEngCaptureILPSCallOut);
            Test.stopTest();  
            System.assertNotEquals(null,jobID);
        }
    }
}