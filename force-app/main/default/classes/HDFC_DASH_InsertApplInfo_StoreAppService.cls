/**
className: HDFC_DASH_InsertApplInfo_StoreAppService
DevelopedBy: Punam Marbate
Date: 31 Aug 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the Applicant details.
**/
public inherited sharing class HDFC_DASH_InsertApplInfo_StoreAppService {
    /* 
    Method Name :mapApplicantDetail
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the applicant details node for storeApp API and sends us the applicant Detail record. 
    */
    public static HDFC_DASH_Applicant_Details__c mapApplicantDetail(HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo)
    { 
        
        appDetail.HDFC_DASH_Alternate_Customer_Number__c = parseApplInfo?.applicantdetail?.alternateSfCustomerNumber;//added for ILPS Applications
        appDetail.HDFC_DASH_CoAppSeqNumber__c = parseApplInfo?.applicantdetail?.coAppSequenceNumber;
        //appDetail.HDFC_DASH_Contact__c = null;
        appDetail.HDFC_DASH_cKYC_Number__c =  parseApplInfo?.applicantdetail?.ckycNumber;
        appDetail.HDFC_DASH_Confidence_Score__c = parseApplInfo?.applicantdetail?.confidenceScore;
        appDetail.HDFC_DASH_Experian_CIBIL_ID__c = parseApplInfo?.applicantdetail?.experianId;
        appDetail.HDFC_DASH_No_Additional_Income__c = parseApplInfo?.applicantdetail?.noAdditionalIncome;
        appDetail.HDFC_DASH_Relation_with_PrimaryApplicant__c = parseApplInfo?.applicantdetail?.relationWithPrimApplicant;
        appDetail.HDFC_DASH_Resident_Type__c  = parseApplInfo?.applicantdetail?.residentType;
        appDetail.HDFC_DASH_Retirement_Age__c = parseApplInfo?.applicantdetail?.retirementAge;
        appDetail.HDFC_DASH_Obligation__c = parseApplInfo?.applicantdetail?.obligation;
        appDetail.HDFC_DASH_PropOwner__c = parseApplInfo?.applicantdetail?.propOwner;
        appDetail.HDFC_DASH_SO_Additional_Income__c = parseApplInfo?.applicantdetail?.additionalIncome;
        appDetail.HDFC_DASH_SO_Fixed_Monthly_Income__c = parseApplInfo?.applicantdetail?.fixedMonthlyIncome;  
        appDetail.HDFC_DASH_Transunion_CIBIL_ID__c = parseApplInfo?.applicantdetail?.transunionCibilId;
        appDetail.HDFC_DASH_GrossBusinessIncome__c = parseApplInfo?.applicantdetail?.grossBusinessIncome;
        appDetail.HDFC_DASH_Salary_Interval__c = parseApplInfo?.applicantdetail?.salaryInterval;
        appDetail.HDFC_DASH_Member_Employment_status__c = parseApplInfo?.applicantdetail?.memberEmploymentStatus;
        appDetail.HDFC_DASH_Current_Net_Income__c  = parseApplInfo?.applicantdetail?.currentNetIncome;
        appDetail.HDFC_DASH_IsResidence_ChngdFrmIndiaToNRI__c  = parseApplInfo?.applicantdetail?.isResidenceChangedFromIndiaToNRI;
        appDetail.HDFC_DASH_Mobile_Number_Verified_Flag__c  = parseApplInfo?.applicantdetail?.mobileNumberVerifiedFlag;
        appDetail.HDFC_DASH_Email_Verified_Flag__c  = parseApplInfo?.applicantdetail?.emailVerifiedFlag;
        appDetail.HDFC_DASH_Name_Match_Percentage__c  = parseApplInfo?.applicantdetail?.nameMatchPercentage;
        appDetail.HDFC_DASH_Address_Match_Percentage__c  = parseApplInfo?.applicantdetail?.addressMatchPercentage;
        appDetail.HDFC_DASH_Address_Changed__c = parseApplInfo?.applicantdetail?.addressChanged;
        appDetail.HDFC_DASH_Employment_Changed__c = parseApplInfo?.applicantdetail?.employmentChanged;
        appDetail.HDFC_DASH_Customer_Profile__c = parseApplInfo?.applicantdetail?.customerProfile;
        appDetail.HDFC_DASH_NSDL_Name_Match_Percentage__c = parseApplInfo?.applicantdetail.nsdlNameMatchPercentage;
        appDetail.HDFC_DASH_Reach_Customer__c = parseApplInfo?.applicantdetail.reachCustomer;
        appDetail.HDFC_DASH_Customer_Has_Application__c = parseApplInfo?.applicantdetail.customerHasApplication == null ? false : parseApplInfo?.applicantdetail.customerHasApplication;
        appDetail.HDFC_DASH_Is_First_Application_For_Cust__c = parseApplInfo?.applicantdetail.stopCustomerDataEdit == null ? false : parseApplInfo?.applicantdetail.stopCustomerDataEdit;
        appDetail.HDFC_DASH_Stop_Customer_Data_Edit__c = parseApplInfo?.applicantdetail.isFirstApplicationForCustomer == null ? false : parseApplInfo?.applicantdetail.isFirstApplicationForCustomer;
        appDetail.HDFC_DASH_Karza_Photo_Match__c = parseApplInfo?.applicantdetail.karzaPhotoMatch;
        appDetail.HDFC_DASH_Form16_Traces_Format__c = parseApplInfo?.applicantdetail.form16TracesFormat;    
        appDetail.HDFC_DASH_Form16_Id_Match__c = parseApplInfo?.applicantdetail.form16IdMatchPercentage;
        appDetail.HDFC_DASH_Sal_Slip_Id_Match__c = parseApplInfo?.applicantdetail.salSlipIdMatchPercentage;
        appDetail.HDFC_DASH_Bank_stmnt_Id_Match__c = parseApplInfo?.applicantdetail.bkstmntIdMatchPercentage;
        appDetail.HDFC_DASH_Bank_Stmnt_Extractable__c = parseApplInfo?.applicantdetail.bkstmntExtractable;
        appDetail.HDFC_DASH_Current_Employment_Years__c = parseApplInfo?.applicantdetail.curEmpYears;
        appDetail.HDFC_DASH_Total_Experience_Years__c = parseApplInfo?.applicantdetail.totalExpYears;
        appDetail.HDFC_DASH_Form16_Latest_Year__c = parseApplInfo?.applicantdetail.form16LatestYear;
        appDetail.HDFC_DASH_Form16_Assessment_Year__c = parseApplInfo?.applicantdetail.form16AssessmentYear;
        appDetail.HDFC_DASH_Appraised_Fixed_Amt__c = parseApplInfo?.applicantdetail.appraisedFixedAmount;
        appDetail.HDFC_DASH_Appraised_Total_Amt__c = parseApplInfo?.applicantdetail.appraisedTotalAmount;
        appDetail.HDFC_DASH_Self_Registered__c =  parseApplInfo?.applicantdetail.selfRegistered == null? false : parseApplInfo?.applicantdetail.selfRegistered;
        //This node is added to store the natureOfEmp on applicant detail for the applications coming from ILPS.
        appDetail.HDFC_DASH_Nat_Of_Emp__c = parseApplInfo?.applicantdetail?.natureOfEmp;
        //appDetail.HDFC_DASH_Business_Entity_Name__c = parseApplInfo?.applicantdetail?.businessEntityName;
        
        //appDetail.HDFC_DASH_ILPS_Customer_Id__c = parseApplInfo?.applicantdetail.ILPSCustomerID;
        if(parseApplInfo?.personalApplicantInfo?.applicantCapacity == HDFC_DASH_Constants.STRING_B){
            appDetail.HDFC_DASH_Any_Existing_Obligation__c = parseApplInfo?.applicantdetail?.doHaveAnyExistingObligation;
            appDetail.HDFC_DASH_Children_Dependents__c = parseApplInfo?.applicantdetail?.childrenDependents;
            appDetail.HDFC_DASH_Other_Dependents__c = parseApplInfo?.applicantdetail?.otherDependents;
        }
        appDetail.HDFC_DASH_Politically_Involved_Person__c = parseApplInfo?.applicantdetail?.politicallyInvolvedPerson;
		appDetail.HDFC_DASH_DoYouHaveAnyDependents__c = parseApplInfo?.applicantdetail?.doYouHaveDependents;
        if(parseApplInfo?.applicantdetail?.sfCustomerId != null &&
        parseApplInfo?.personalApplicantInfo?.applicantCapacity != HDFC_DASH_Constants.STRING_R){
            appDetail.HDFC_DASH_Contact__c =  parseApplInfo?.applicantdetail?.sfCustomerId;
        }
        if(appDetail.HDFC_DASH_Resident_Type__c == HDFC_DASH_Constants.HDFC_DASH_Resident_Type_NRI)
        {
            appDetail.HDFC_DASH_NRI_Gross_MonthlyIncome__c = parseApplInfo?.applicantdetail.nriGrossMonthlyIncome;
            appDetail.HDFC_DASH_NRI_Grs_MnthlyIncome_CurencyCD__c = parseApplInfo?.applicantdetail.nriGrossMonthlyIncomeCurrencyCode;
            
        }

        return appDetail;
    }
    /* 
    Method Name :mapConsent
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of Consent node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapConsent(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
        
        appDetail.HDFC_DASH_Consent_Content__c=parseApplInfo?.consent?.consentContent;
        appDetail.HDFC_DASH_SMS_Consent__c=parseApplInfo?.consent?.smsConsent;
        appDetail.HDFC_DASH_SMS_Consent_Date__c=parseApplInfo?.consent?.smsConsentDate;
        appDetail.HDFC_DASH_Voice_Consent__c=parseApplInfo?.consent?.voiceConsent;
        appDetail.HDFC_DASH_Voice_Consent_Date__c=parseApplInfo?.consent?.voiceConsentDate;
        appDetail.HDFC_DASH_WhatsApp_Consent__c=parseApplInfo?.consent?.whatsappConsent;
        appDetail.HDFC_DASH_WhatsApp_Consent_Date__c=parseApplInfo?.consent?.whatsappConsentDate;
        appDetail.HDFC_DASH_User_Consent_For_CIBIL__c=parseApplInfo?.consent?.userConsentForCibil;
        appDetail.HDFC_DASH_SelfDeclarationTermsConditions__c =  parseApplInfo?.consent?.selfDeclarationConsentForTermsAndCondition == null? false : parseApplInfo?.consent?.selfDeclarationConsentForTermsAndCondition;
        appDetail.HDFC_DASH_SelfDeclarationContentForTerms__c = parseApplInfo?.consent?.selfDeclarationContentForTermsAndCondition;
        appDetail.HDFC_DASH_SelfDeclarationConsentAccepted__c = parseApplInfo?.consent?.selfDeclarationConsentAcceptedDateTime;
       // appDetail.HDFC_DASH_Terms_And_Condition_Consent__c=parseApplInfo?.consent?.termsAndConditionConsentFlag;
        //appDetail.HDFC_DASH_TermsAndConditionVersion__c=parseApplInfo?.consent?.termsAndConditionConsentVersion;
       // appDetail.HDFC_DASH_TermsAndConditionDateTime__c=parseApplInfo?.consent?.termsAndConditionConsentAcceptedDateTime;
       
        return appDetail;
    }
    /* 
    Method Name :mapResidenceLocation
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of Residence Location node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapResidenceLocation(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
        
        //map cityOfResidence,cityOfResidenceCode,stateOfResidence,stateOfResidenceCode,countryOfResidence,countryOfResidenceCode
        appDetail.HDFC_DASH_City__c = parseApplInfo?.residenceLocation?.cityOfResidence;
        appDetail.HDFC_DASH_City_Code__c = parseApplInfo?.residenceLocation?.cityOfResidenceCode;
        appDetail.HDFC_DASH_State__c = parseApplInfo?.residenceLocation?.stateOfResidence;
        appDetail.HDFC_DASH_State_Code__c = parseApplInfo?.residenceLocation?.stateOfResidenceCode;
        appDetail.HDFC_DASH_Country__c = parseApplInfo?.residenceLocation?.countryOfResidence;
        appDetail.HDFC_DASH_Country_Code__c = parseApplInfo?.residenceLocation?.countryOfResidenceCode;
        //map cityOfResidence,stateOfResidence,countryOfResidence RecordId
        //appDetail = (HDFC_DASH_Applicant_Details__c)HDFC_DASH_UtilityClass.generateMasterTabelFields(appDetail);
       
        return appDetail;
    }
    /* 
    Method Name :mapRiskScore
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of Risk Score node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapRiskScore(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
        
        appDetail.HDFC_DASH_RiskScore_Availed_moratorium__c = parseApplInfo?.riskScore?.riskscoreAvailedMoratorium;
        appDetail.HDFC_DASH_RiskScore_Availed_restruct__c = parseApplInfo?.riskScore?.riskscoreAvailedRestruct;
        appDetail.HDFC_DASH_RiskScore_Confidence_score__c = parseApplInfo?.riskScore?.riskscoreConfidenceScore;
        appDetail.HDFC_DASH_RiskScore_Customer_flags__c = parseApplInfo?.riskScore?.riskscoreCustomerFlags;
        appDetail.HDFC_DASH_RiskScore_Customer_grade__c = parseApplInfo?.riskScore?.riskscoreCustomerGrade;
        appDetail.HDFC_DASH_RiskScore_Customer_Status__c = parseApplInfo?.riskScore?.riskscoreCustomerStatus;
        appDetail.HDFC_DASH_RiskScore_Hdfc_Staff__c = parseApplInfo?.riskScore?.riskscoreHdfcStaff;
        appDetail.HDFC_DASH_RiskScore_Risk_event__c = parseApplInfo?.riskScore?.riskscoreRiskEvent;
        appDetail.HDFC_DASH_RiskScore_Rtr_Exists__c = parseApplInfo?.riskScore?.riskscoreRtrExists;
        appDetail.HDFC_DASH_RiskSore_Exist_in_neg_list__c = parseApplInfo?.riskScore?.risksoreExistInNegList;
        //appDetail.HDFC_DASH_TF_RiskScore__c = parseApplInfo?.riskScore?.riskScoreTechnicalFailure;
        
        return appDetail;
    }
    /* 
    Method Name :mapPersonalApplInfo
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of PersonalApplInfo node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapPersonalApplInfo(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
       
        appDetail.HDFC_DASH_Salutation__c  = parseApplInfo?.personalApplicantInfo?.salutation;
        appDetail.HDFC_DASH_Business_Entity_Name__c = parseApplInfo?.personalApplicantInfo?.businessEntityName;
        appDetail.Name = parseApplInfo?.personalApplicantInfo?.name;
        appDetail.HDFC_DASH_Mobile_Country_Code__c = parseApplInfo?.personalApplicantInfo?.mobileCountryCode;
        appDetail.HDFC_DASH_Mobile__c = parseApplInfo?.personalApplicantInfo?.mobile;
        appDetail.HDFC_DASH_MiddleName__c = parseApplInfo?.personalApplicantInfo?.middleName;
        appDetail.HDFC_DASH_LastName__c  = parseApplInfo?.personalApplicantInfo?.lastName;
        appDetail.HDFC_DASH_Gender__c = parseApplInfo?.personalApplicantInfo?.genderCode;
        appDetail.HDFC_DASH_FirstName__c  = parseApplInfo?.personalApplicantInfo?.firstName;
        appDetail.HDFC_DASH_Email__c = parseApplInfo?.personalApplicantInfo?.email;
        appDetail.HDFC_DASH_Email_verify_Later__c = parseApplInfo?.personalApplicantInfo?.emailVerifyLater;
        appDetail.HDFC_DASH_Date_Of_Birth__c = parseApplInfo?.personalApplicantInfo?.dateOfBirth;
        appDetail.HDFC_DASH_PAN__c  = parseApplInfo?.personalApplicantInfo?.pan;
        appDetail.HDFC_DASH_Pan_Applied__c  = parseApplInfo?.personalApplicantInfo?.panApplied;
        appDetail.HDFC_DASH_Pan_Status_From_NSDL__c  = parseApplInfo?.personalApplicantInfo?.panStatusFromNsdl;
        appDetail.HDFC_DASH_Applicant_Capacity__c = parseApplInfo?.personalApplicantInfo?.applicantCapacity;
        
        
        return appDetail;
    }
    /* 
    Method Name :mapAddPersonalInfo
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of additionalPersonalInfo node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapAddPersonalInfo(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
       
        appDetail.HDFC_DASH_Father_First_Name__c  = parseApplInfo?.additionalPersonalInformation?.fatherFirstName;
        appDetail.HDFC_DASH_Father_Middle_Name__c = parseApplInfo?.additionalPersonalInformation?.fatherMiddleName;
        appDetail.HDFC_DASH_Father_Last_Name__c = parseApplInfo?.additionalPersonalInformation?.fatherLastName;
        appDetail.HDFC_DASH_Marital_Status__c = parseApplInfo?.additionalPersonalInformation?.maritalStatus;
        //to do
        appDetail.HDFC_DASH_Highest_Qualification_Code__c = parseApplInfo?.additionalPersonalInformation?.highestQualificationCode;
        appDetail.HDFC_DASH_Highest_Qualification__c  = parseApplInfo?.additionalPersonalInformation?.highestQualification;
        appDetail.HDFC_DASH_Social_Category__c = parseApplInfo?.additionalPersonalInformation?.socialCategory;
              
        return appDetail;
    }
    /* 
    Method Name :mapPrePopApplDetails
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of PrePopApplDetails node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapPrePopApplDetails(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
        
        appDetail.HDFC_DASH_NameEdit__c  = parseApplInfo?.prePopulatedApplicantDetails?.nameEdit;
        appDetail.HDFC_DASH_PrePopulatedFirstName__c = parseApplInfo?.prePopulatedApplicantDetails?.prePopulatedFirstName;
        appDetail.HDFC_DASH_PrePopulatedMiddleName__c = parseApplInfo?.prePopulatedApplicantDetails?.prePopulatedMiddleName;
        appDetail.HDFC_DASH_PrePopulatedLastName__c = parseApplInfo?.prePopulatedApplicantDetails?.prePopulatedLastName;
        appDetail.HDFC_DASH_PrePopulatedGender__c  = parseApplInfo?.prePopulatedApplicantDetails?.prePopulatedGender;
        appDetail.HDFC_DASH_PrePopulatedDate_Of_Birth__c = parseApplInfo?.prePopulatedApplicantDetails?.prePopulatedDOB;
        appDetail.HDFC_DASH_PrePopulatedFatherFirstName__c = parseApplInfo?.prePopulatedApplicantDetails?.prePopFatherFirstName;
        appDetail.HDFC_DASH_PrePopulatedFatherMiddleName__c = parseApplInfo?.prePopulatedApplicantDetails?.prePopFatherMiddleName;
        appDetail.HDFC_DASH_PrePopulatedFatherLastName__c = parseApplInfo?.prePopulatedApplicantDetails?.prePopFatherLastName;
        appDetail.HDFC_DASH_Identity_Source__c = parseApplInfo?.prePopulatedApplicantDetails?.identitySource;
        appDetail.HDFC_DASH_Identity_From__c = parseApplInfo?.prePopulatedApplicantDetails?.identityFrom;
        
        return appDetail;
    }
    /* 
    Method Name :mapNriDetails
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of nriDetails node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapNriDetails(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo)
    {
        appDetail.HDFC_DASH_NRI_Visa_Status__c = parseApplInfo?.nriDetails?.visaStatus;
        appDetail.HDFC_DASH_NRI_Visa_Type__c = parseApplInfo?.nriDetails?.visaType;
        appDetail.HDFC_DASH_NRI_Visa_Issued_By__c =parseApplInfo?.nriDetails?.visaIssuedBy;
        appDetail.HDFC_DASH_NRI_Visa_Validity__c =parseApplInfo?.nriDetails?.visaValidity;
        appDetail.HDFC_DASH_NRI_PR_Number__c =parseApplInfo?.nriDetails?.prNumber;
        appDetail.HDFC_DASH_NRI_PR_Validity_Date__c = parseApplInfo?.nriDetails?.prValidity;
        //map citizenship,citizenship code,prCountry,prCountry code
        appDetail.HDFC_DASH_NRI_Citizenship__c = parseApplInfo?.nriDetails?.citizenship;
        appDetail.HDFC_DASH_NRI_Citizenship_Code__c = parseApplInfo?.nriDetails?.citizenshipCode;
        appDetail.HDFC_DASH_NRI_PR_Country__c =parseApplInfo?.nriDetails?.prCountryName;
        appDetail.HDFC_DASH_NRI_PR_Country_Code__c = parseApplInfo?.nriDetails?.prCountryCode;
        //map citizenship, prCountry RecordId...
        //appDetail = (HDFC_DASH_Applicant_Details__c)HDFC_DASH_UtilityClass.generateMasterTabelFields(appDetail);
        
        return appDetail;
    }
    /* 
    Method Name :mapCSPassportInfo
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of CustomerselectedPassportInfo node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapCSPassportInfo(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
       
        appDetail.HDFC_DASH_Passport_Details_Edit_Flag__c = parseApplInfo?.customerselectedPassportInfo?.passportDetailsEditFlag;
        appDetail.HDFC_DASH_Passport_Number__c = parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportNumber;
        appDetail.HDFC_DASH_Passport_Salutation__c = parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportSalutation;
        appDetail.HDFC_DASH_Passport_FirstName__c = parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportFirstName;
        appDetail.HDFC_DASH_Passport_MiddleName__c = parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportMiddleName;
        appDetail.HDFC_DASH_Passport_LastName__c = parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportLastName;
        appDetail.HDFC_DASH_Passport_Nationality__c = parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportNationality;
        appDetail.HDFC_DASH_Passport_Date_Of_Expiry__c =parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportDateOfExpiry;
        appDetail.HDFC_DASH_Passport_Date_Of_Issue__c = parseApplInfo?.customerselectedPassportInfo?.customerselectedPassportDateOfIssue;
        
        return appDetail;
    }
    /* 
    Method Name :mapPrePopPassportInfo
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map all the fields of  PrePopulatedPassportInfo node for storeApp API and sends us the applicant Detail record.
    */
    public static HDFC_DASH_Applicant_Details__c mapPrePopPassportInfo(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
       
        appDetail.HDFC_DASH_PP_Passport_Number__c = parseApplInfo?.prePopulatedPassportInfo?.passportNumber;
        appDetail.HDFC_DASH_PP_Passport_Salutation__c = parseApplInfo?.prePopulatedPassportInfo?.passportSalutation;
        appDetail.HDFC_DASH_PP_Passport_FirstName__c = parseApplInfo?.prePopulatedPassportInfo?.passportFirstName;
        appDetail.HDFC_DASH_PP_Passport_MiddleName__c = parseApplInfo?.prePopulatedPassportInfo?.passportMiddleName;
        appDetail.HDFC_DASH_PP_Passport_LastName__c = parseApplInfo?.prePopulatedPassportInfo?.passportLastName;
        appDetail.HDFC_DASH_PP_Passport_Nationality__c = parseApplInfo?.prePopulatedPassportInfo?.passportNationality;
        appDetail.HDFC_DASH_PP_Passport_Date_Of_Expiry__c = parseApplInfo?.prePopulatedPassportInfo?.passportDateOfExpiry;
        appDetail.HDFC_DASH_PP_Passport_Date_Of_Issue__c = parseApplInfo?.prePopulatedPassportInfo?.passportDateOfIssue;
        
        return appDetail;
    }

    /* 
    Method Name :updateApplicantName
    Parameters  :HDFC_DASH_Applicant_Details__c appDetail, HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo
    Description :This method would map 
    */
    public static HDFC_DASH_Applicant_Details__c updateApplicantName(HDFC_DASH_Applicant_Details__c appDetail,HDFC_DASH_ParseApplication.ApplicantInfo parseApplInfo){
        
        appDetail.HDFC_DASH_Salutation__c  = parseApplInfo?.personalApplicantInfo?.salutation;
        appDetail.Name = parseApplInfo?.personalApplicantInfo?.name;
        appDetail.HDFC_DASH_MiddleName__c = parseApplInfo?.personalApplicantInfo?.middleName;
        appDetail.HDFC_DASH_LastName__c  = parseApplInfo?.personalApplicantInfo?.lastName;
        appDetail.HDFC_DASH_FirstName__c  = parseApplInfo?.personalApplicantInfo?.firstName;
        //appDetail.HDFC_DASH_Email__c = parseApplInfo?.personalApplicantInfo?.email;

        
        return appDetail;
    }
    
}