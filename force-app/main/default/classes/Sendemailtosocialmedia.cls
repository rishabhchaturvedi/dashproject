public class Sendemailtosocialmedia {
    
    public static void sendemailtosocialmedia(list<case> caselist)
    {
              OrgWideEmailAddress[] owea;
              String owdLabel;
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
     //   List<OrgWideEmailAddress> owea = new List<OrgWideEmailAddress>();
        Id emailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Service_request_query_Creation_via_Social_media_channel_3'].Id;
        Id resolvedemailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Service_request_query_resolution_which_was_raised_via_Social_media_channel'].Id;
        Id reopenedemailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Social_Media_Case_Re_Opened'].Id;

                 Id loancomplaintRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Complaint').getRecordTypeId();
                 Id depositcomRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Deposit - Complaint').getRecordTypeId();
                Id loanqueryTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Query').getRecordTypeId();
                Id loanRequestTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Request').getRecordTypeId();
                
        

        for(case c:caselist)
        {
        if(c.RecordTypeId==loancomplaintRecId ){
                    owdLabel = Label.LoanComplaint;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    else if(c.RecordTypeId==depositcomRecId){
                    owdLabel = Label.DepositComplaint;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    
                    else if(c.RecordTypeId==loanqueryTypeId){
                    owdLabel = Label.LoanQuery;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    
                else{
                    owdLabel = Label.LoanRequest;
                    owea= [select Id from OrgWideEmailAddress where Address =: owdLabel];}
        
      
        if(c.status=='Re-Opened')
        {
         mail.setTemplateId(reopenedemailTemp);
        }
        else if (c.status=='Resolved')
        {
         mail.setTemplateId(resolvedemailTemp);
        }
        else if(c.status !='Escalated L1' && c.status !='Escalated L2' && c.status !='Not Registered' && c.status !='Closed')
        {
         mail.setTemplateId(emailTemp);
        }
            if(c.HDFC_DASH_Social_media_link__c !=null && c.HDFC_DASH_Social_media_link__c !='')
            {
                 List<String> tosimplify360 = new List<String>();
                  String tosimplify = Label.simplify_360;
                                        tosimplify360=tosimplify.split(';');
                   
                    mail.setOrgWideEmailAddressId(owea[0].Id);
                   
                    mail.toaddresses=tosimplify360;
                    mail.setTargetObjectId(c.contactid);
                    mail.setWhatId(c.Id);
                    mail.setSaveAsActivity(false);
                    mail.setTreatTargetObjectAsRecipient(false);
            }
        }
        system.debug('Social Media Email body'+mail.htmlbody);
        if(!Test.isRunningTest()) // 16th feb
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      }

    Public static void sendemailtocaseowner(list<case> newcases)
    {
        OrgWideEmailAddress[] owea;
        String owdLabel;
        User u;
       
        list<string> toaddresses = new list<string>();
         for (case c1:newcases)
        {
                  
       if(string.valueOf(c1.ownerid).startsWith('005') && c1.contactId != null)
       {
            u=[select id,email from User where id =:c1.OwnerId];
        if(u.email !=null)
        {
            toaddresses.add(u.email);
        }
            system.debug('set email address'+u.email);
        }
        }
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            Id loancomplaintRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Complaint').getRecordTypeId();
            Id depositcomRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Deposit - Complaint').getRecordTypeId();
            Id loanqueryTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Query').getRecordTypeId();
           Id loanRequestTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Request').getRecordTypeId();
            list<string> ccAddresses = new list<string>();
        
                                        
         Id emailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Case_Creation_Social_media_channel_influencer'].Id;
        Id caseassignment = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Case_creation_email_via_social_media_for_employee'].Id;
        Id reopenedemailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Social_Media_complaint_followup_Comment'].Id;
        for(case c:newcases)
        {
             if(c.contactId != null){
                 
             String searchString = '%' + c.HDFC_DASH_Home_Branch__c + '%';
                                if(searchString!=''){
                                    List<User> GROorOpsHead=new List<User>([select id,Name,UserRole.name from user where UserRole.name like :searchString]);
                                   integer i=0;
                                    for(User usd:GROorOpsHead){
                                        String UserroleGROorOpsHead =usd.userRole.Name;
                                        if(UserroleGROorOpsHead!='' &&(UserroleGROorOpsHead.contains('Branch GRO')||UserroleGROorOpsHead.contains('Operations') )){
                                            system.debug('UserGRO@'+usd.Name);
                                            i++; 
                                            ccAddresses.add(usd.Id);
                                        }
                                    }
                                }
            
             if(c.RecordTypeId==loancomplaintRecId ){
                     owdLabel = Label.LoanComplaint;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    else if(c.RecordTypeId==depositcomRecId){
                    owdLabel = Label.DepositComplaint;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    
                    else if(c.RecordTypeId==loanqueryTypeId){
                    owdLabel = Label.LoanQuery;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    
                else{
                    owdLabel = Label.LoanRequest;
                    owea= [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                if(c.Priority=='Influencer' || c.Priority=='Critical')
                {
                    mail.setTemplateId(emailTemp); 
                    mail.setCcAddresses(ccAddresses);
                }
                else  //if(c.Priority=='High' || c.Priority=='Medium')
                {
                    mail.setTemplateId(caseassignment); 
                    
                }
                              
                    mail.setOrgWideEmailAddressId(owea[0].Id);
                    mail.toaddresses=toaddresses;
                    mail.setTargetObjectId(c.contactid);
            
                    mail.setWhatId(c.Id);
                    mail.setSaveAsActivity(false);
                    mail.setTreatTargetObjectAsRecipient(false);
             }
    }
       system.debug('send email to case owner'+mail.htmlbody);
        if(toaddresses.size()>0)
        {
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        }
    }
    
    public static void sendescalationemails(list<case> newcases ){
        OrgWideEmailAddress[] owea;
        String owdLabel;
        User u;
       try
       {
        list<string> toaddresses = new list<string>();
         for (case c1:newcases)
        {
       if(string.valueOf(c1.ownerid).startsWith('005'))
         
            u=[select id,email from User where id =:c1.OwnerId];
        if(u.id !=c1.previous_owner__r.id)
            toaddresses.add(u.email);
            system.debug('set email address'+u.email);
        }
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            Id loancomplaintRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Complaint').getRecordTypeId();
            Id depositcomRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Deposit - Complaint').getRecordTypeId();
            Id loanqueryTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Query').getRecordTypeId();
           Id loanRequestTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Request').getRecordTypeId();
            
         Id emailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Complaint_SR_escalation_triggersL12'].Id;
       
        for(case c:newcases)
        {
            
             if(c.RecordTypeId==loancomplaintRecId ){
                     owdLabel = Label.LoanComplaint;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    else if(c.RecordTypeId==depositcomRecId){
                    owdLabel = Label.DepositComplaint;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    
                    else if(c.RecordTypeId==loanqueryTypeId){
                    owdLabel = Label.LoanQuery;
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                    
                else{
                    owdLabel = Label.LoanRequest;
                    owea= [select Id from OrgWideEmailAddress where Address =: owdLabel];}
                
          
                   mail.setTemplateId(emailTemp);
                    mail.setOrgWideEmailAddressId(owea[0].Id);
                    mail.toaddresses=toaddresses;
                    mail.setTargetObjectId(c.contactid);
                    mail.setWhatId(c.Id);
                    mail.setSaveAsActivity(false);
                    mail.setTreatTargetObjectAsRecipient(false);
    }
       system.debug('send email to case owner'+mail.htmlbody);
           if(toaddresses.size()>0)
           {
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
           }
    }
    
    catch(exception e)
    {
        system.debug('exception'+e.getMessage());
    }
    }
    
     //Social Media Emails - 15th Feb
    public static void caseUpdatesFromSocialMedia(List<Case> newcases){
                OrgWideEmailAddress[] owea;
                User u;
                try
                {
                list<string> toaddresses = new list<string>();
                for (case c1:newcases)
                {
                if(string.valueOf(c1.ownerid).startsWith('005'))
                
                u=[select id,email from User where id =:c1.OwnerId];
                toaddresses.add(u.email);
                system.debug('set email address'+u.email);
                }
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String owdLabel;
                Id loancomplaintRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Complaint').getRecordTypeId();
                Id depositcomRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Deposit - Complaint').getRecordTypeId();
                Id loanqueryTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Query').getRecordTypeId();
                Id loanRequestTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Request').getRecordTypeId();
                
                Id emailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Social_Media_complaint_followup_Comment'].Id;
                
                for(case c:newcases)
                {
                
                if(c.RecordTypeId==loancomplaintRecId ){
                owdLabel = Label.LoanComplaint;
                owea = [select Id from OrgWideEmailAddress where Address =:owdLabel ];}
                
                else if(c.RecordTypeId==depositcomRecId){
                owdLabel = Label.DepositComplaint;
                owea = [select Id from OrgWideEmailAddress where Address =:owdLabel ];}                
                
                else if(c.RecordTypeId==loanqueryTypeId){
                 owdLabel = Label.LoanQuery;
                owea = [select Id from OrgWideEmailAddress where Address =:owdLabel ];}
                
                else{
                 owdLabel = Label.LoanRequest;
                owea= [select Id from OrgWideEmailAddress where Address =:owdLabel ];}
                
                
                mail.setTemplateId(emailTemp);
                mail.setOrgWideEmailAddressId(owea[0].Id);
                mail.toaddresses=toaddresses;
                mail.setTargetObjectId(c.contactid);
                mail.setWhatId(c.Id);
                mail.setSaveAsActivity(false);
                mail.setTreatTargetObjectAsRecipient(false);
                }
                system.debug('send email to case owner'+mail.htmlbody);
                if(toaddresses.size()>0)
                {
                if(!Test.isRunningTest())
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
                }
                }
                
                catch(exception e)
                {
                system.debug('exception'+e.getMessage());
                }
       
    
    }
  
}