/**
className: HDFC_DASH_MasterTables_SelectorTest
DevelopedBy: Punam Marbate
Date: 26 July 2021
Company: Accenture
Class Description: Test class for  MasterTables_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_MasterTables_SelectorTest {
     //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static List <HDFC_DASH_Master_Table__c> masterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicMasterTable(); 
    private static HDFC_DASH_City_Master__c cityMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicCityMaster(); 
    private static HDFC_DASH_Pincode_Master__c pinCodeMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicPincodeMaster(); 
    private static HDFC_DASH_Project_Master__c projectMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicProjectMaster(); 
    private static HDFC_DASH_KYC_Document_Master__c kycDocMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicKYCDocMaster(); 
    private static HDFC_DASH_Image_Document_Master__c imageMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicIMGDocMaster(); 
    private static HDFC_DASH_Property_Loan_Master__c loanAvailedMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicLoanAvailMaster();
     private static HDFC_DASH_Property_Loan_Master__c institutionMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicInstitutionMaster();
    private static HDFC_DASH_Property_Loan_Master__c reraMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicRERAMaster();
    private static HDFC_DASH_Property_Loan_Master__c bankDetailsMasterData = 
                      HDFC_DASH_TestDataFactory_MasterTables.getBasicBankDetailsMaster();
    /* 
    Method Name: testGetMasterTable
    Description: This is the testmethod for getMasterTable().
    */
    static testmethod void testGetMasterTable(){    
        List<HDFC_DASH_Master_Table__c>  listMasterDataRec = null;
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listMasterDataRec = HDFC_DASH_MasterTables_Selector.getMasterTable(new List<String> {masterData[0].HDFC_DASH_CD_Val__c} );
            Test.stopTest();
        }
        system.assertNotEquals( null, listMasterDataRec);
        system.assertEquals(1,listMasterDataRec.size()); 
     }
     /* 
    Method Name: testGetMasterTableWithRecordType
    Description: This is the testmethod for getMasterTableWithRecordType().
    */
    static testmethod void testGetMasterTableWithRecordType(){    
      List<HDFC_DASH_Master_Table__c>  listMasterDataRec = null;
      System.runAs(sysAdmin){
          
          Test.startTest();
          listMasterDataRec = HDFC_DASH_MasterTables_Selector.getMasterTableWithRecordType(new List<String> {masterData[HDFC_DASH_Constants.INT_ZERO].HDFC_DASH_CD_Val__c},HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_STATECODE );
          Test.stopTest();
      }
      system.assertNotEquals( null, listMasterDataRec);
      system.assertEquals(1,listMasterDataRec.size()); 
   }
   

    /* 
    Method Name: testGetCityMaster
    Description: This is the testmethod for getCityMaster().
    */
    static testmethod void testGetCityMaster(){      
        List<HDFC_DASH_City_Master__c>  listCityMasterRec = null;   
        System.runAs(sysAdmin){
          
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listCityMasterRec = HDFC_DASH_MasterTables_Selector.getCityMaster(new List<String> {cityMasterData.HDFC_DASH_City_Code__c} );
                 
            Test.stopTest();
        }
        system.assertNotEquals( null, listCityMasterRec);
        system.assertEquals(1,listCityMasterRec.size()); 
      }
    /* 
    Method Name: testGetPinCodeMaster
    Description: This is the testmethod for getPinCodeMaster().
    */
      static testmethod void testGetPinCodeMaster(){      
        List<HDFC_DASH_Pincode_Master__c>  listPinCodeMasterRec = null;   
        System.runAs(sysAdmin){
          
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listPinCodeMasterRec = HDFC_DASH_MasterTables_Selector.getPinCodeMaster(new List<String> {pinCodeMasterData.HDFC_DASH_Pincode__c} );
                 
            Test.stopTest();
        }
        system.assertNotEquals( null, listPinCodeMasterRec);
        system.assertEquals(1,listPinCodeMasterRec.size()); 
      }
    /* 
    Method Name: testGetProjectMaster
    Description: This is the testmethod for getProjectMaster().
    */
    static testmethod void testGetProjectMaster(){

      
        List<HDFC_DASH_Project_Master__c>  listProjectMasterRec = null;
        
        System.runAs(sysAdmin){
           
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listProjectMasterRec = HDFC_DASH_MasterTables_Selector.getProjectMaster(new List<String> {projectMasterData.HDFC_DASH_Project_UID__c} );
                 
            Test.stopTest();
        }
        system.assertNotEquals( null, listProjectMasterRec);
        system.assertEquals(1,listProjectMasterRec.size()); 
      }
       /* 
    Method Name: testGetEmpMaster
    Description: This is the testmethod for getEmpMaster().
    */
      static testmethod void testGetEmpMaster(){
        List<Account>  listEmpMasterRec = null;
        System.runAs(sysAdmin){   
            Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.HDFC_EMPLOYER_RECORD_TYPE).getRecordTypeId();
            acc.RecordTypeId = recordTypeId;
            Database.update(acc); 
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listEmpMasterRec = HDFC_DASH_MasterTables_Selector.getEmpMaster(new List<String> {acc.HDFC_DASH_Corp_Cust_No__c} );         
            Test.stopTest();
        }
        system.assertNotEquals( null, listEmpMasterRec);
        system.assertEquals(1,listEmpMasterRec.size()); 
      }
    /* 
    Method Name: testgetKYCDocMasterWithRecordType
    Description: This is the testmethod for getKYCDocMasterWithRecordType().
    */
    static testmethod void testgetKYCDocMasterWithRecordType(){
        List<HDFC_DASH_KYC_Document_Master__c>  listKycMasterRec = null;
        System.runAs(sysAdmin){   
            Test.startTest();
            listKycMasterRec = HDFC_DASH_MasterTables_Selector.getKYCDocMasterWithRecordType(new List<String> {kycDocMasterData.HDFC_DASH_Doc_Code__c},HDFC_DASH_Constants.KYCMASTERTABLE_RECORDTYPE_KYCDOC);         
            Test.stopTest();
        }
        system.assertNotEquals( null, listKycMasterRec);
        system.assertEquals(1,listKycMasterRec.size()); 
    }
    /* 
    Method Name: testGetImageDocMaster
    Description: This is the testmethod for getImageDocMaster().
    */
      static testmethod void testGetImageDocMaster(){
        List<HDFC_DASH_Image_Document_Master__c>  listImageMasterRec = null;
        System.runAs(sysAdmin){   
            Test.startTest();
            listImageMasterRec = HDFC_DASH_MasterTables_Selector.getImageDocMaster(new List<String> {imageMasterData.HDFC_DASH_Doc_Type__c} );         
            Test.stopTest();
        }
        system.assertNotEquals( null, listImageMasterRec);
        system.assertEquals(1,listImageMasterRec.size()); 
      }
      /* 
    Method Name: testGetLoanAvailedMaster
    Description: This is the testmethod for getLoanAvailedMaster().
    */
     /*static testmethod void testGetLoanAvailedMaster(){
        List<HDFC_DASH_Property_Loan_Master__c>  listAvailLoanRec = new List<HDFC_DASH_Property_Loan_Master__c>();
        System.runAs(sysAdmin)
        {   
            Test.startTest();
            listAvailLoanRec = HDFC_DASH_MasterTables_Selector.getLoanAvailedMaster(new List<String> {loanAvailedMasterData.HDFC_DASH_Loan_Purpose_Desc__c});         
            Test.stopTest();
        }
        system.assertNotEquals( null, listAvailLoanRec); 
        system.assertEquals(1,listAvailLoanRec.size()); 
      }*/
    
     /* 
    Method Name: testGetInstitutionMaster
    Description: This is the testmethod for getInstitutionMaster().
    */
     /*static testmethod void testGetInstitutionMaster(){
        List<HDFC_DASH_Property_Loan_Master__c>  listInstitutionRec = new List<HDFC_DASH_Property_Loan_Master__c>();
        System.runAs(sysAdmin){   
            Id recordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.INSTITUTION_Master).getRecordTypeId();
            institutionMasterData.RecordTypeId = recordTypeId;
            Database.update(institutionMasterData); 

            Test.startTest();
            listInstitutionRec = HDFC_DASH_MasterTables_Selector.getInstitutionMaster(new List<String> {institutionMasterData.HDFC_DASH_Institution_Code__c});         
            Test.stopTest();
        }
        system.assertNotEquals( null, listInstitutionRec); 
        system.assertEquals(1,listInstitutionRec.size()); 
      }*/
    
     /* 
    Method Name: testGetRERAMaster
    Description: This is the testmethod for getRERAMaster().
    */
     /*static testmethod void testGetRERAMaster(){
        List<HDFC_DASH_Property_Loan_Master__c>  listRERARec = new List<HDFC_DASH_Property_Loan_Master__c>();
        System.runAs(sysAdmin){   
            Id recordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.RERA_MASTER).getRecordTypeId();
            reraMasterData.RecordTypeId = recordTypeId;
            Database.update(reraMasterData); 

            Test.startTest();
            listRERARec = HDFC_DASH_MasterTables_Selector.getRERAMaster(new List<String> {reraMasterData.HDFC_DASH_RERA_Id__c});         
            Test.stopTest();
        }
        system.assertNotEquals( null, listRERARec); 
        system.assertEquals(1,listRERARec.size()); 
      }*/
    
    /* 
    Method Name: testGetBankDetailMaster
    Description: This is the testmethod for getBankDetailMaster().
    */
     /*static testmethod void testGetBankDetailMaster(){
        List<HDFC_DASH_Property_Loan_Master__c>  listBankDetailsRec = new List<HDFC_DASH_Property_Loan_Master__c>();
        System.runAs(sysAdmin)
        {   
            Test.startTest();
            listBankDetailsRec = HDFC_DASH_MasterTables_Selector.getBankDetailMaster(new List<String> {bankDetailsMasterData.HDFC_DASH_Bank_Cd__c});         
            Test.stopTest();
        }
        system.assertNotEquals( null, listBankDetailsRec); 
        system.assertEquals(1,listBankDetailsRec.size()); 
      }*/
}