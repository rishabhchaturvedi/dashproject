/**
className: SendemailforILPSLAC
Class Description : This class send an email .
**/
public class SendemailforILPSLAC {
    /**
Method Name: sendemailforILPSLAC
Method Description : This Method send an email .
**/
    @InvocableMethod
    public static void sendemailforILPSLAC(list<id> caseids)
    {
              OrgWideEmailAddress[] owea;
               String owdLabel;
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
       
                 Id loancomplaintRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Complaint').getRecordTypeId();
                 Id depositcomRecId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Deposit - Complaint').getRecordTypeId();
                Id loanqueryTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Query').getRecordTypeId();
                Id loanRequestTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Request').getRecordTypeId();
                
        

        for(case c:[select id,RecordTypeId,HDFC_DASH_ILPS_LAC__c,status,cc_Notification__c,Subject,HDFC_DASH_Attachment_Associated__c,HDFC_DASH_Description__c,To_Email_Address__c,HDFC_DASH_CC_Notification__c,contactid from case where id=:caseids[0] ])
        {
        if(c.RecordTypeId==loancomplaintRecId ){
                    owdLabel = Label.LoanComplaint;
                    if(Schema.sObjectType.OrgWideEmailAddress.IsAccessible()) {
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];}
                    }
                    else if(c.RecordTypeId==depositcomRecId){
                    owdLabel = Label.DepositComplaint;
                        if(Schema.sObjectType.OrgWideEmailAddress.IsAccessible()) {
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];}
                    }
                    else if(c.RecordTypeId==loanqueryTypeId){
                     owdLabel = Label.LoanQuery;
                        if(Schema.sObjectType.OrgWideEmailAddress.IsAccessible()) {
                    owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];}
                    }
                else{
                    owdLabel = Label.LoanRequest;
                    if(Schema.sObjectType.OrgWideEmailAddress.IsAccessible()) {
                    owea= [select Id from OrgWideEmailAddress where Address =: owdLabel ];}
                    }
      
        
            if(c.HDFC_DASH_ILPS_LAC__c ==true && c.status =='Closed' && c.HDFC_DASH_Attachment_Associated__c==false)
            {   
                Id tempId = [select Id,DeveloperName from EmailTemplate where DeveloperName =: 'ILPS_template' limit 1].Id;
                    mail.setOrgWideEmailAddressId(owea[0].Id);
                    mail.setHtmlBody(c.HDFC_DASH_Description__c);
                    mail.setTemplateId(tempId);
                    mail.setSubject(c.Subject);
                   list<string> emailids= new list<string>();
                list<string> toaddresses = new list<string>();
                toaddresses=c.To_Email_Address__c.split(',');
                    emailids.add(c.To_Email_Address__c);
                    mail.setToAddresses(toaddresses);
                list<string> ccaddresses = new list<string>();
               
                if(c.cc_Notification__c !='' && c.cc_Notification__c !=null)
                {
                    ccaddresses=c.cc_Notification__c.split(',');
                    mail.setCcAddresses(ccaddresses);
                }
                    mail.setTargetObjectId(c.contactid);
                    mail.setWhatId(c.Id);
                    mail.setSaveAsActivity(true);
                    mail.setTreatTargetObjectAsRecipient(false);
            }
        }
        system.debug('send email for ILPSLAC'+mail.htmlbody);
        try{
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception e){
            system.debug('exception SendemailforILPSLAC...'+e);
        }
       
      
    }


}