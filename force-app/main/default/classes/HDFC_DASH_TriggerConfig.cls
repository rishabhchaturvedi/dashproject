/**
*  className: HDFC_DASH_TriggerConfig
DevelopedBy: Anmol
Date: 25 May 2021
Company: Accenture
Class Description: A singleton class that presents the configuration properties of the individual triggers.
* Instances are created dynamically from a JSON static resource.
*/
public with sharing class HDFC_DASH_TriggerConfig {
    public Boolean isEnabled {get; set;}
    public HDFC_DASH_TriggerOps[] beforeOps {get; private set;}
    public HDFC_DASH_TriggerOps[] afterOps {get; private set;}
    public Boolean isBeforeInsertEnabled {get; set;}
    public Boolean isBeforeUpdateEnabled {get; set;}
    public Boolean isBeforeDeleteEnabled {get; set;}
    public Boolean isAfterInsertEnabled {get; set;}
    public Boolean isAfterUpdateEnabled {get; set;}
    public Boolean isAfterDeleteEnabled {get; set;}
    public Boolean isAfterUndeleteEnabled {get; set;}
    
    public HDFC_DASH_TriggerOps[] beforeInsertOps {get; set;}
    public HDFC_DASH_TriggerOps[] beforeUpdateOps {get; set;}
    public HDFC_DASH_TriggerOps[] beforeDeleteOps {get;  set;}
    public HDFC_DASH_TriggerOps[] afterInsertOps {get;  set;}
    public HDFC_DASH_TriggerOps[] afterUpdateOps {get;  set;}
    public HDFC_DASH_TriggerOps[] afterDeleteOps {get;  set;}
    public HDFC_DASH_TriggerOps[] afterUndeleteOps {get; set;}
    
    /* Method Name: HDFC_DASH_TriggerConfig
parameters: NONE
Class Description: This is an empty constructor.
*/
    public HDFC_DASH_TriggerConfig(){
        
    }
    /* Method Name: HDFC_DASH_TriggerConfig
parameters: NONE
Class Description: This is a constructor.
*/
    private HDFC_DASH_TriggerConfig(Boolean isEnabled, HDFC_DASH_TriggerOps[] beforeOps, HDFC_DASH_TriggerOps[] afterOps) {
        this.isEnabled = isEnabled;
        this.beforeOps = beforeOps;
        this.afterOps = afterOps;
    }
    private static final String CONFIG_RESOURCE_NAME = HDFC_DASH_Constants.TRIGGER_CONFIG;
   public static final HDFC_DASH_TriggerConfig ACCOUNT_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_ACC_CONFIG);
    public static final HDFC_DASH_TriggerConfig PROCESS_EXCEP_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRI_CON_PROC_EXC_CON);
    public static final HDFC_DASH_TriggerConfig APPLICANT_DETAILS_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRI_APP_DETAIL_CON);
   // public static final HDFC_DASH_TriggerConfig ACCOUNT_CHANGE_EVENT_CONFIG = triggerConfigMap.get(
        //HDFC_DASH_Constants.TRIG_CONF_ACC_CHANGE_EVENT_CONFIG);
    public static final HDFC_DASH_TriggerConfig CONTACT_ADDRESS_DETAILS = triggerConfigMap.get(
    	HDFC_DASH_Constants.TRIG_CONF_CONTACT_ADDRESS_DETAILS_CONFIG);
    public static final HDFC_DASH_TriggerConfig CASE_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_CASE_CONFIG);
    public static final HDFC_DASH_TriggerConfig APPLICANT_BANK_DETAILS = triggerConfigMap.get(
    	HDFC_DASH_Constants.TRIG_CONF_APPLICANT_BANK_DETAILS_CONFIG);
    public static final HDFC_DASH_TriggerConfig APPLICANT_EMPLOYMENT_DETAILS =  triggerConfigMap.get(
    	HDFC_DASH_Constants.TRIG_CONF_APPLICANT_EMPLOYMENT_DETAILS_CONFIG);
   public static final HDFC_DASH_TriggerConfig LEAD_CONFIG =  triggerConfigMap.get(
    	HDFC_DASH_Constants.TRIG_CONF_LEAD_CONFIG);
    public static final HDFC_DASH_TriggerConfig INFOVAL_CONFIG =  triggerConfigMap.get(
    	HDFC_DASH_Constants.TRIG_CONF_INFOVALIDATION_CONFIG);
   public static final HDFC_DASH_TriggerConfig CONTENT_VERSION_CONFIG = triggerConfigMap.get(
    	HDFC_DASH_Constants.TRIG_CONF_CONTENT_VERSION_CONFIG);
    public static final HDFC_DASH_TriggerConfig APPLICATION_CONFIG = triggerConfigMap.get(
    	HDFC_DASH_Constants.TRIG_CONF_APPLICATION_CONFIG);
    public static final HDFC_DASH_TriggerConfig USER_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_USER_CONFIG);
     public static final HDFC_DASH_TriggerConfig CONTACT_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_CONTACT_CONFIG);
     public static final HDFC_DASH_TriggerConfig SO_BSA_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_SO_BSA_CONFIG);
     public static final HDFC_DASH_TriggerConfig APP_PAYDET_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_APP_PAYDET_CONFIG);
    public static final HDFC_DASH_TriggerConfig TASK_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_TASK_CONFIG);
    public static final HDFC_DASH_TriggerConfig CASEUPDATE_CONGIF = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_CASEUPDATE_CONFIG); 
    public static final HDFC_DASH_TriggerConfig EVENT_CONFIG = triggerConfigMap.get(
        HDFC_DASH_Constants.TRIG_CONF_EVENT_CONFIG);
    
    
    public static Map<String, HDFC_DASH_TriggerConfig> triggerConfigMap {
        get {
            if (triggerConfigMap == null) {
                triggerConfigMap = new Map<String, HDFC_DASH_TriggerConfig>();
                //list<HDFC_DASH_Trigger_Config__mdt> triggerConfigList =  HDFC_DASH_Trigger_Config__mdt.getall().values();
                 list<HDFC_DASH_Trigger_Config__mdt> triggerConfigList =  [SELECT Id, HDFC_DASH_Before_Insert_Operations__c, HDFC_DASH_After_Undelete_Operations__c, HDFC_DASH_Before_Update_Operations__c, HDFC_DASH_After_Undelete__c, HDFC_DASH_After_Delete_Operations__c, HDFC_DASH_After_Update_Operations__c, HDFC_DASH_After_Insert_Operations__c, NamespacePrefix, Label, QualifiedApiName, HDFC_DASH_isEnabled__c, HDFC_DASH_Before_Insert__c, HDFC_DASH_After_Insert__c, HDFC_DASH_Before_Update__c, HDFC_DASH_After_Update__c, HDFC_DASH_Before_Delete__c, HDFC_DASH_After_Delete__c, DeveloperName, MasterLabel, Language, HDFC_DASH_Before_Delete_Operations__c, HDFC_DASH_Before_Undelete_Operations__c, HDFC_DASH_Before_Undelete__c FROM HDFC_DASH_Trigger_Config__mdt]; 
                //system.debug('triggerConfigList'+triggerConfigList);
                if (!triggerConfigList.isEmpty()) {
                    for(HDFC_DASH_Trigger_Config__mdt triggerConfigObj :triggerConfigList){
                        HDFC_DASH_TriggerConfig triggerCon = new HDFC_DASH_TriggerConfig();
                        triggerCon.isEnabled = triggerConfigObj?.HDFC_DASH_isEnabled__c;
                        triggerCon.isBeforeInsertEnabled = triggerConfigObj?.HDFC_DASH_Before_Insert__c;
                        triggerCon.isBeforeUpdateEnabled = triggerConfigObj?.HDFC_DASH_Before_Update__c;
                        triggerCon.isBeforeDeleteEnabled = triggerConfigObj?.HDFC_DASH_Before_Delete__c;
                        triggerCon.isAfterInsertEnabled = triggerConfigObj?.HDFC_DASH_After_Insert__c;
                        triggerCon.isAfterUpdateEnabled = triggerConfigObj?.HDFC_DASH_After_Update__c;
                        triggerCon.isAfterDeleteEnabled = triggerConfigObj?.HDFC_DASH_After_Delete__c;
                        triggerCon.isAfterUndeleteEnabled = triggerConfigObj?.HDFC_DASH_After_Undelete__c;
                        if(triggerCon.isBeforeInsertEnabled){
                            triggerCon.beforeInsertOps = newInstancesFrom(triggerConfigObj?.HDFC_DASH_Before_Insert_Operations__c);}
                        if(triggerCon.isBeforeUpdateEnabled){
                            triggerCon.beforeUpdateOps = newInstancesFrom(triggerConfigObj?.HDFC_DASH_Before_Update_Operations__c);}
                        if(triggerCon.isBeforeDeleteEnabled){
                            triggerCon.beforeDeleteOps = newInstancesFrom(triggerConfigObj?.HDFC_DASH_Before_Delete_Operations__c);}
                        if(triggerCon.isAfterInsertEnabled){
                            triggerCon.afterInsertOps = newInstancesFrom(triggerConfigObj?.HDFC_DASH_After_Insert_Operations__c);}
                        if(triggerCon.isAfterUpdateEnabled){
                            triggerCon.afterUpdateOps = newInstancesFrom(triggerConfigObj?.HDFC_DASH_After_Update_Operations__c);}
                        if(triggerCon.isAfterDeleteEnabled){
                            triggerCon.afterDeleteOps = newInstancesFrom(triggerConfigObj?.HDFC_DASH_After_Delete_Operations__c);}
                        if(triggerCon.isAfterUndeleteEnabled){
                            triggerCon.afterUndeleteOps = newInstancesFrom(triggerConfigObj?.HDFC_DASH_After_Undelete_Operations__c);}
                        
                        triggerConfigMap.put(triggerConfigObj?.DeveloperName, triggerCon);
                        
                    }
                }
            }
            return triggerConfigMap;
        }
        set;
    }
    
    
    
    /*
methodName:newInstancesFrom
parameters:String[]
methodDescription: Creates a new instance from the Custom Metadata
*/
    private static HDFC_DASH_TriggerOps[] newInstancesFrom(String classNames) {
        HDFC_DASH_TriggerOps[] result = new HDFC_DASH_TriggerOps[] {};
            for (String className : classNames.split(HDFC_DASH_Constants.COMMA)) {
                Type type = Type.forName(className);
                system.debug('className==='+className);
                result.add((HDFC_DASH_TriggerOps) type.newInstance());
            }
        return result;
    }
}