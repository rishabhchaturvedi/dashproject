/**
className: HDFC_DASH_ApplicantDetailsTriggerOps
DevelopedBy: Anmol
Date: 15 July 2021
Company: Accenture
Class Description: This Class contains the trigger operation on Applicant Details Object.
**/
public with sharing class HDFC_DASH_ApplicantDetailsTriggerOps {
    /**
className: Validation
DevelopedBy: Anmol
Date: 15 July 2021
Company: Accenture
Class Description: This Class contains the trigger operation to HDFC_DASH_Applicant_Details__c.
**/
    public with sharing class PANValidationInsert implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return Trigger.isInsert ;
        }
        /* 
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<HDFC_DASH_Applicant_Details__c> appDetList = Trigger.New;
            List<HDFC_DASH_Applicant_Details__c> filterAppDetList = new List<HDFC_DASH_Applicant_Details__c>();
            for(HDFC_DASH_Applicant_Details__c appDet: appDetList) {
                if(!appDet.HDFC_DASH_Application_Created_From_ILPS__c){
                    filterAppDetList.add(appDet);
                }
                
            }
            return filterAppDetList; 
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(HDFC_DASH_Applicant_Details__c[] appDetailRecs){
            
            HDFC_DASH_ApplicantTriggerOpsHelper.executeOnInsert(appDetailRecs);

        }     
    }
    
        /**
className: updateContactIsCust
DevelopedBy: Sai Shruthi
Date: 15 July 2021
Company: Accenture
Class Description: This Class contains the after Update trigger operation to HDFC_DASH_Applicant_Details__c.
**/
    public with sharing class updateContactIsCust implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.appDetailsIsFirstRun; 
        }
        /* 
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
           List<HDFC_DASH_Applicant_Details__c> appDetRecs = HDFC_DASH_ApplicantTriggerOpsHelper.fetchContacts(); 
            return appDetRecs;  
        } 
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(HDFC_DASH_Applicant_Details__c[] appDetailRecs){
            
            HDFC_DASH_ApplicantTriggerOpsHelper.updateConRecBasedOnAppRec(appDetailRecs);
            HDFC_DASH_TriggerOpsRecursionFlags.appDetailsIsFirstRun = false;

        }     
    }
    /* 
Method Name :PANValidationUpdate
Description :This class will do the PAN validation
*/
    public with sharing class PANValidationUpdate implements HDFC_DASH_TriggerOps
    {
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return Trigger.isUpdate ;
            
        }
        /* 
Method Name :filter
Description :This method will filter the records in which PAN is updated.
*/
        public SObject[] filter()
        {
            /*
            List<SObject> listPANUpdatedRecords = new List<SObject>();
            for(SObject objAppDetail : Trigger.new)
            {  
                HDFC_DASH_Applicant_Details__c newMapData = (HDFC_DASH_Applicant_Details__c) Trigger.newMap.get(objAppDetail.Id);
            	HDFC_DASH_Applicant_Details__c oldMapData = (HDFC_DASH_Applicant_Details__c) Trigger.oldMap.get(objAppDetail.Id);
                
                if(newMapData.HDFC_DASH_PAN__c != oldMapData.HDFC_DASH_PAN__c && (!newMapData.HDFC_DASH_Application_Created_From_ILPS__c))
   
                {  
                    listPANUpdatedRecords.add(objAppDetail);
                }
            }
            return listPANUpdatedRecords; */
             List<HDFC_DASH_Applicant_Details__c> appDetRecList =
                HDFC_DASH_ApplicantTriggerOpsHelper.filterforPanIsUpdated();
            return appDetRecList;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(HDFC_DASH_Applicant_Details__c[] appDetailRecs){
            HDFC_DASH_ApplicantTriggerOpsHelper.executeOnUpdate(appDetailRecs);
        }
    }
 
        
}