/**
className: HDFC_DASH_SO_BSA_Selector
DevelopedBy: Utkarsh Patidar
Date: 08 Nov 2021
Company: Accenture
Class Description: This class would create the select queries for SO BSA Association object .
**/
public with sharing class HDFC_DASH_SO_BSA_Selector {
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_SO_BSA_Association__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    
 
  /* 
    Method Name :getBSA
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get a list of BSA based on one condition.
    */   
    public static List<HDFC_DASH_SO_BSA_Association__c> getBSA(List<String> queryParameters,List<string> fieldList, String conditionOn){
            
       string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        List<HDFC_DASH_SO_BSA_Association__c> listBSA = database.query(querystring); 
        return listBSA;
    }
    
    /* 
    Method Name :getBSABasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of BSA based on fieldsAndparameters sent to it.
    */
    public static List<HDFC_DASH_SO_BSA_Association__c> getBSABasedOnQuery(List<String> fieldList,
                                                            Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +
                                FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<HDFC_DASH_SO_BSA_Association__c> bsaList = database.query(querystring); 
        return bsaList;   
    }
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getBSABasedOnQuery.
    */
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }
    
   

}