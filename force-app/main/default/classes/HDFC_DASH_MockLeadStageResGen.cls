/**
className: HDFC_DASH_MockLeadStageResGen
Date: 24Nov 2021
Company: Accenture
Class Description: This is mock class used for generating response for Lead Stage API callout.
**/ 
@isTest(SeeAllData=false)
global with sharing class HDFC_DASH_MockLeadStageResGen implements HttpCalloutMock{
    /* 
Method Name :respond
Parameters  :HTTPRequest req
Description :This method would create a fake response for Confidence Rule Engine callout.
*/
    global HTTPResponse respond(HTTPRequest req) {
        
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        HttpResponse res = new HttpResponse();
        
        // Create a fake response
        
            res.setHeader('Content-Type', 'application/json'); 
            res.setBody('{"return_cd":0,"return_msg":"SUCCESS","lead_no":80624449,"lead_Details":null,"appln_form":null,"new_lead_no":"101800912"}');
            res.setStatusCode(200);
        
        return res;
    }
}