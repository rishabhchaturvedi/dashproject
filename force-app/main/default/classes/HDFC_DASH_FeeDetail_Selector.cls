/**
className: HDFC_DASH_FeeDetail_Selector
DevelopedBy: Punam marbate
Date: 26 Aug 2021
Company: Accenture
Class Description: This class would create the select queries for FeeDetail(HDFC_DASH_Fee_Detail__c) object .
**/
public inherited sharing class HDFC_DASH_FeeDetail_Selector {

    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Fee_Detail__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';

    /* 
    Method Name :getFeeDetailsBasedOnId
    Parameters  :List<string> fieldList, Id feeDetailId
    Description :This method would get a single Fee Detail based on the ID.
    */   
   /* public static HDFC_DASH_Fee_Detail__c getFeeDetailsBasedOnId(List<string> fieldList, Id feeDetailId){
            
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT+
                            WHERE_STRING+ ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+feeDetailId+ 
                            HDFC_DASH_Constants.STRING_QUOTE;
        HDFC_DASH_Fee_Detail__c feeDetailRec = database.query(querystring); 
        return feeDetailRec;
    }*/
    /* 
    Method Name :getFeeDetailsBasedOnIds
    Parameters  :List<string> fieldList, Id feeDetailIds
    Description :This method would get a single Fee Detail based on the IDs.
    */   
    public static List<HDFC_DASH_Fee_Detail__c> getFeeDetailsBasedOnIds(List<string> fieldList, List<Id>feeDetailIds){
            
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA)
        +FROM_OBJECT+WHERE_STRING+HDFC_DASH_CONSTANTS.STRING_ID
        +HDFC_DASH_CONSTANTS.STRING_IN
        +HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE
        +String.join(feeDetailIds,HDFC_DASH_Constants.STRING_QUOTE
        +HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)
        +HDFC_DASH_Constants.STRING_QUOTE
        +HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<HDFC_DASH_Fee_Detail__c> feeDetailList = database.query(querystring); 
        return feeDetailList;
    }
    /* 
    Method Name :getFeeDetailsBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Fee Details based on fieldsAndparameters sent to it.
    */
    public static List<HDFC_DASH_Fee_Detail__c> getFeeDetailsBasedOnQuery(List<String> fieldList,
                                                            Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +
                                FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<HDFC_DASH_Fee_Detail__c> feeDetList = database.query(querystring); 
        return feeDetList;   
    }
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparamethttps://hdfclimited--hdfcdev.my.salesforce.com/_ui/common/apex/debug/ApexCSIPage#ers
    Description :This method would be called from getFeeDetailsBasedOnQuery .
    */
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }   
    
        /* 
Method Name :getFeeDetails
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of Contacts based on the condition.
*/
    public static List<HDFC_DASH_Fee_Detail__c> getFeeDetails(List<string> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT+ WHERE_STRING + conditionOn + QUERY_PARAMETER; 
        List<HDFC_DASH_Fee_Detail__c> resultList = database.query(querystring);        
        
        return resultList;
    } 
}