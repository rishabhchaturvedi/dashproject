/*
Class: HDFC_DASH_CommunicationWrapper
Author: Anisha Arumugam
Date: 17 Dec 2021
Company: Accenture
Description: This class will handle the Communication callouts.
*/
public class HDFC_DASH_CommunicationWrapper 
{
    public Map<String,String> requestHeader = new Map<String,String>();
    HttpResponse response;
    RestRequest request; 
    Exception exp;
    string mobilenumber;
    string template;
    string taskId;
    Task objTask = new Task();
    
    /**
ConstructorName : HDFC_DASH_CommunicationWrapper
* Parameters : Task objTask
* Description : It is a constructor for class HDFC_DASH_CommunicationWrapper.
*/
    public HDFC_DASH_CommunicationWrapper(Task objTask)
    {
        this.objTask = objTask;
    }
    
    /*
Class: processData
Description: This method will create a json for Response Handler.
*/
    public with sharing class ProcessData
    {
        public string taskId;
        public string msgLogId;
        public string messageBody;
        
        /**
ConstructorName : processData
* Parameters : string messageBody, string taskId
* Description : It is a constructor for class processData.
*/
        public ProcessData(string taskId, string messageBody, string msgLogId){
            this.taskId = taskId;
            this.msgLogId = msgLogId;
            this.messageBody = messageBody;
        }
    }
    
    /*Method: processCommunication
Description: This method will process the communication data.
*/
    public void processCommunication()
    {
        try
        {
            //Checking the communication types
            if(objTask.HDFC_DASH_Target_Number__c != null && (objTask.HDFC_DASH_Communication_Type__c == HDFC_DASH_Constants.STRING_SMS || objTask.HDFC_DASH_Communication_Type__c == HDFC_DASH_Constants.STRING_WA))
            {
                string messageBody = callGenerateContent(objTask);
                //messageBody should not contain "hdfc_dash_" substring
                if(messageBody != null && (!messageBody.contains(HDFC_DASH_Constants.STRING_HDFC_DASH))) 
                {
                    if(objTask.HDFC_DASH_Communication_Type__c == HDFC_DASH_Constants.STRING_SMS)
                    {
                        HDFC_DASH_SMSIntegration smsCallout = new HDFC_DASH_SMSIntegration(objTask, messageBody); 
                        system.enqueueJob(smsCallout);
                    }
                    
                    if(objTask.HDFC_DASH_Communication_Type__c == HDFC_DASH_Constants.STRING_WA)
                    {
                        HDFC_DASH_WhatsappIntegration whatsAppCallout = new HDFC_DASH_WhatsappIntegration(objTask, messageBody); 
                        system.enqueueJob(whatsAppCallout);
                    }
                }
            }
        }
        catch(exception e)
        {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();
            //system.debug('expDetails => '+expDetails);
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_CommunicationWrapper, HDFC_DASH_Constants.ProcessCommunication, string.valueof(objTask), expDetails);
        }
    }   
    
    
    /*Method: callGenerateContent
Description: This method would call the generatecontent method.
*/
    public String callGenerateContent(Task task)
    {
        string messageBody;
        
        if(task.HDFC_DASH_Communication_ID__c != null && task.HDFC_DASH_Related_Record__c != null)
        {
            HDFC_DASH_Communication__mdt mdt_Communication = HDFC_DASH_Communication__mdt.getInstance(task.HDFC_DASH_Communication_ID__c);   
            if(mdt_Communication != null)
            {
                messageBody = generatecontent(mdt_Communication, task.HDFC_DASH_Related_Record__c);
            }
        }
        return messageBody;
    }
    
    /*Method: generatecontent
Description: This method would construct message body.
*/
    public static string generatecontent(HDFC_DASH_Communication__mdt mdt_Comm, string recordid)
    {
        String messagetemplate = mdt_Comm.HDFC_DASH_Template__c;
        List<String> fieldList = new List<String>();
        list<string> maplist = new list<String>();
        List<sObject> sobjList = new list<sObject>();
        map<string,string> mapping = new map<string,string>();
        if(String.isNotBlank(mdt_Comm.HDFC_DASH_Mapping__c))
        {
            maplist = mdt_Comm.HDFC_DASH_Mapping__c.split(HDFC_DASH_Constants.PAIR_SEPERATOR);
        }
        for(string mappingrecord : maplist)
        {
            list<string> splitmappingstr = mappingrecord.split(HDFC_DASH_Constants.VALUE_SEPERATOR);
            
            if(!splitmappingstr.isEmpty())
            {
                mapping.put(splitmappingstr[HDFC_DASH_Constants.INT_ZERO],splitmappingstr[HDFC_DASH_Constants.INT_ONE]);
                fieldList.add(splitmappingstr[HDFC_DASH_Constants.INT_ONE]);
            }
        }
        if(!fieldlist.isEmpty())
        {
            sobjList = HDFC_DASH_SObject_Selector.getSObjects(new List<String>{recordid}, fieldlist, mdt_Comm.HDFC_DASH_Object__c, HDFC_DASH_Constants.ID_STRING);	
        }
        if(!sobjList.isEmpty())
        {
            for(string mapkey : mapping.keySet())
            {
                string fieldvalue = (string)sobjList[HDFC_DASH_Constants.INT_ZERO]?.get(mapping.get(mapkey));
                if(String.isNotBlank(fieldvalue))
                {
                    messagetemplate = messagetemplate.replace(mapkey,fieldvalue);
                }
            }
            if(String.isNotBlank(Label.HDFC_DASH_Portal_Link) && messagetemplate.contains(HDFC_DASH_Constants.HDFC_DASH_LINK)){
                messagetemplate = messagetemplate.replace(HDFC_DASH_Constants.HDFC_DASH_LINK, Label.HDFC_DASH_Portal_Link);
            }
        }
        return messagetemplate;
    }
}