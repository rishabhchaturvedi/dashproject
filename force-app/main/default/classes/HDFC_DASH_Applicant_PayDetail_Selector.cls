/**
className: HDFC_DASH_Applicant_PayDetail_Selector
DevelopedBy: Anisha Arumugam
Date: 21 July 2021
Company: Accenture
Class Description: This class would create the select queries for Applicant Payment Detail object .
**/
public class HDFC_DASH_Applicant_PayDetail_Selector {
    
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Application_Payment_Details__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    
        /* 
Method Name :getApplicationPaymentDetailOnId
Parameters  :List<string> fieldList, Id ApplicationId
Description :This method would get a single Application Payment Detail based on the ID.
*/   
    /*public static HDFC_DASH_Application_Payment_Details__c getApplicationPaymentDetailOnId(List<string> fieldList, Id ApplicationId){
        
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT+ WHERE_STRING+ ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ApplicationId+ HDFC_DASH_Constants.STRING_QUOTE;
        HDFC_DASH_Application_Payment_Details__c applicationPaymentDetail = database.query(querystring); 
        return applicationPaymentDetail;
    }*/
/*
/* 
Method Name :getApplicationPaymentDetailOnIds
Parameters  :List<string> fieldList, List<Id> applicationPaymentDetailId
Description :This method would get a single Application Payment Detail based on the IDs.
*/   
    public static List<HDFC_DASH_Application_Payment_Details__c> getApplicationPaymentDetailOnIds(List<string> fieldList,List<Id> applicationPaymentDetailId){
        
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA)
        +FROM_OBJECT+WHERE_STRING+HDFC_DASH_CONSTANTS.STRING_ID
        +HDFC_DASH_CONSTANTS.STRING_IN
        +HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE
        +String.join(applicationPaymentDetailId,HDFC_DASH_Constants.STRING_QUOTE
        +HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)
        +HDFC_DASH_Constants.STRING_QUOTE
        +HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<HDFC_DASH_Application_Payment_Details__c> applicationPaymentDetail = database.query(querystring); 
        return applicationPaymentDetail;
    }
/*
Method Name :getApplicationPaymentDetail
Parameters  :List<string> fieldList, Id ApplicationId
Description :This method would get a single Application Payment Detail based on only one condition.
*/   
    public static List<HDFC_DASH_Application_Payment_Details__c> getApplicationPaymentDetail(List<String> queryParameters, List<string> fieldList, String conditionOn){
        
         string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER; 
        List<HDFC_DASH_Application_Payment_Details__c> applicationPaymentDetail = database.query(querystring); 
        return applicationPaymentDetail;
    }    
    
    
       /* 
    Method Name :getApplicationPaymentDetailBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Application Payment Detail based on fieldsAndparameters sent to it.
    */
        public static List<HDFC_DASH_Application_Payment_Details__c> getApplicationPaymentDetailBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
            List<HDFC_DASH_Application_Payment_Details__c> applicationPaymentDetail = database.query(querystring); 
            return applicationPaymentDetail;   
        }
        
        /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getApplicationPaymentDetailBasedOnQuery .
    */
        public static String getCondition(Map<String,String> fieldsAndparameters){
            String condition ='';
            for(String fieldName : fieldsAndparameters.keySet()){
                condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
            }
            return condition;
        }

}