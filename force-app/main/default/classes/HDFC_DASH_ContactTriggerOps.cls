/**
className: HDFC_DASH_ContactTriggerOps
DevelopedBy: Tejeswari
Date: 18 November 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on Contact object.
**/
public with sharing class HDFC_DASH_ContactTriggerOps {
    /**
className: populateContactFields
DevelopedBy: Tejeswari
Date: 18 November 2021
Company: Accenture 
Class Description: This Class update the lookup fields of Contact.
*/
    public with sharing class populateContactFields implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.Contact_Api_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Contact> listContactRecs = HDFC_DASH_ContactTriggerOpsHelper.filterContactsWithReportsToIdField();
            return listContactRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfContacts){
            HDFC_DASH_ContactTriggerOpsHelper.updateReportsToIdLookupField(listOfContacts); 
            HDFC_DASH_TriggerOpsRecursionFlags.Contact_Api_FirstRun = false;
            
        }  
    }
    
   
    /**
className: createUser
DevelopedBy: Tejeswari
Date: 18 November 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on Contact object.
*/
    public with sharing class createUser implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.Contact_UserCreation_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            system.debug('inside user create filter');
            List<Contact> listContactRecs = HDFC_DASH_ContactTriggerOpsHelper.filterContactsForCreatingUser();
            return listContactRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfContacts){
            system.debug('inside user execution');
            System.enqueueJob(new HDFC_DASH_UserCreation(listOfContacts));
           	HDFC_DASH_TriggerOpsRecursionFlags.Contact_UserCreation_FirstRun = false;
        }  
    }
}