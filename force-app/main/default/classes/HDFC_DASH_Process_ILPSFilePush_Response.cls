/**
className: processResponseHandler
DevelopedBy: Koora Raghavendra
Date: 14 Dec 2021
Company: Accenture
Class Description: This class will retry the Spot Offer ILPS File Push Callout
**/
public class HDFC_DASH_Process_ILPSFilePush_Response extends processResponseHandler {
       /*
Method: doRetryProcess
Description: This method will retry the ILPS Callout
*/
    public override void doRetryProcess(HttpResponse response,  String ProcessData )
    {
        //HDFC_DASH_ILPS_FilePush_Callout ilpsFilePushCallout = new HDFC_DASH_ILPS_FilePush_Callout();
        HDFC_DASH_ILPS_FilePush_Callout.processResponse(response);       
    }
}