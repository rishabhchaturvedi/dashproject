/**
className: HDFC_DASH_RestResource_FeeDetails
DevelopedBy: Sai Shruthi
Date: 19 July 2021
Company: Accenture
Class Description: This is a webservice class for feeDetails .
**/
@RestResource(urlMapping='/hdfc/feeDetails/V1.0') 
global with sharing class HDFC_DASH_RestResource_FeeDetails{
    @HttpPatch
    /*  
Method Name :handleFeeDetailsRequest
Parameters  :
Description :This method would get the request and pass it to HDFC_DASH_RestResource_FeeDetails.processFeeDetails.
*/
    global static void handleFeeDetailsRequest() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        map<string,string> responseheaders;
        HDFC_DASH_APIResponse_FeeDetails apiResponse;
        exception exp;
        try{
            responseheaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_FeeDetails.processFeeDetails(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse)); 
        }
        catch(Exception e){
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON +e.getMessage()+ HDFC_DASH_Constants.SEMICOLON +e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON +e.getTypeName() ;  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_FEE_DETAILS,HDFC_DASH_Constants.HANDLE_FEE_DETAILS_REQUEST , HDFC_DASH_Constants.METHOD_TYPE_PATCH, expDetails);
            // Throw Exception
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e,res);
        }
        finally{	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_FEE_DETAILS, request.requestBody.tostring(),
                                                                (res.responseBody)?.toString(), HDFC_DASH_Constants.METHOD_TYPE_PATCH,
                                                                HDFC_DASH_Constants.HANDLE_FEE_DETAILS_REQUEST, exp, request.headers);
            
            
            if(responseheaders!= NULL){
                for(string responseheaderstr : responseheaders.keySet()){
                    res.addHeader(responseheaderstr, responseheaders.get(responseheaderstr));
                }
            }
        }
    }
}