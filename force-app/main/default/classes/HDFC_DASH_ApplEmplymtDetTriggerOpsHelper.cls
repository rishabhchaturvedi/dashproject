/*
className: HDFC_DASH_ApplEmplymtDetTriggerOpsHelper (ApplicantEmploymentDetails)
DevelopedBy: Sai Suman
Date: 04 October 2021
Company: Accenture
Class Description: This class would acts as helper class for Applicant Employment Details Trigger and contains the methods for before and after insert Scenarios.
*/
public class HDFC_DASH_ApplEmplymtDetTriggerOpsHelper {
     /* 
Method Name :filterEDWhereConIsPopulated
Description :This method would filter ApplicantEmploymentDetails where contact is populated.
*/
    public static List<HDFC_DASH_Applicant_Employment_Details__c> filterEDWhereConIsPopulated(){
         List<HDFC_DASH_Applicant_Employment_Details__c> empDetList = new List<HDFC_DASH_Applicant_Employment_Details__c>();
            List<HDFC_DASH_Applicant_Employment_Details__c> newEmpDetails = Trigger.new;
           
            for(HDFC_DASH_Applicant_Employment_Details__c empdetail: newEmpDetails){
               
               HDFC_DASH_Applicant_Employment_Details__c oldMapDetail = (HDFC_DASH_Applicant_Employment_Details__c) Trigger.oldMap?.get(empdetail.id);
                if(empdetail.HDFC_DASH_Contact__c != oldMapDetail?.HDFC_DASH_Contact__c && 
                   empdetail.HDFC_DASH_Contact__c != NULL){
                    empDetList.add(empdetail);
                }
            }
        

        //system.debug('bkDetList'+empDetList);
        return empDetList; 
    }
    /*
Method Name :filterEDforLatestFlagScenario
Description :This method would filter Applicant Employment Details for latest flag.
*/
    public static List<HDFC_DASH_Applicant_Employment_Details__c> filterEDforLatestFlagScenario(){
         Map<id,HDFC_DASH_Applicant_Employment_Details__c> mapEmpDetails;
        String queryCondition = HDFC_DASH_Constants.HDFC_DASH_Contact;
            List<HDFC_DASH_Applicant_Employment_Details__c> newEmpDetails = Trigger.new;
            List<String> conList = new List<String>();
            for(HDFC_DASH_Applicant_Employment_Details__c empdetail: newEmpDetails){
                
               HDFC_DASH_Applicant_Employment_Details__c oldMapDetail = (HDFC_DASH_Applicant_Employment_Details__c) Trigger.oldMap?.get(empdetail.id);
                if(empdetail.HDFC_DASH_Contact__c != oldMapDetail?.HDFC_DASH_Contact__c && 
                   empdetail.HDFC_DASH_Contact__c != NULL){
                    conList.add(empdetail.HDFC_DASH_Contact__c);
                       //system.debug('conList'+conList);
                }
            }
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_LATEST};
                mapEmpDetails =  new Map<id,HDFC_DASH_Applicant_Employment_Details__c>
                (HDFC_DASH_Applicant_Employment_Selector.getEmploymentDetails(conList,fieldList,queryCondition));
            for(HDFC_DASH_Applicant_Employment_Details__c empDet :newEmpDetails){
                if(mapEmpDetails.get(empDet.id) !=NULL){
                    mapEmpDetails.remove(empDet.id);
                }
            }
       
        return mapEmpDetails.values();
 
    }
    /*
Method Name :updateEmpDetRec
Description :This method would update the Latest Flag for Applicant Employment detail records.
*/
    public static void updateEmpDetRec(List<HDFC_DASH_Applicant_Employment_Details__c> empDetRecList){
        List<HDFC_DASH_Applicant_Employment_Details__c> updateEmpDetail = new List<HDFC_DASH_Applicant_Employment_Details__c>();
        try{
            for(HDFC_DASH_Applicant_Employment_Details__c EmpDetail : empDetRecList){
                if(EmpDetail.HDFC_DASH_Latest__c == HDFC_DASH_Constants.STRING_YES){
                    EmpDetail.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_NO;
                }
                updateEmpDetail.add(EmpDetail);
            }
            if(Schema.sObjectType.HDFC_DASH_Applicant_Employment_Details__c.isUpdateable()){
                Database.update(updateEmpDetail,true);
            }
        }
        catch(Exception e){
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON 
                + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Applicant_Employment_Details,
                                                        HDFC_DASH_Constants.UPDATELATESTFIELD,NULL,expDetails);
        }
    }
    /*
Method Name :changeEmpDetRecBeforeInsert
Description :This method would change the Latest field of Applicant Employment Details records before insert.
*/
    public static void changeEmpDetRecBeforeUpdate(List<HDFC_DASH_Applicant_Employment_Details__c> empDetRecList){
       try{
            for(HDFC_DASH_Applicant_Employment_Details__c empDet :empDetRecList){
                
                empDet.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_YES;
               
            }
        }catch(exception e)
        {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON 
                + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_Applicant_Employment_Details,
                                                        HDFC_DASH_Constants.UPDATELATESTFIELD,NULL,expDetails);
        }
    }
    
    /*
    Method Name :filterEDforNatOfEmpScenario
    Description :This method would filter Applicant Employment Details for NatOfEmp field.
    */
    public static List<HDFC_DASH_Applicant_Employment_Details__c> filterEDforNatOfEmpScenario(){
        List<HDFC_DASH_Applicant_Employment_Details__c> listNewEmpDetails =Trigger.new;
        //List<HDFC_DASH_Applicant_Employment_Details__c> listNewEmpDetailsOldData =Trigger.old;
        List<HDFC_DASH_Applicant_Employment_Details__c> listUpdateEmpDetails = new List<HDFC_DASH_Applicant_Employment_Details__c>();
       // if(listNewEmpDetailsOldData!=null){
        for(HDFC_DASH_Applicant_Employment_Details__c empDetRec: listNewEmpDetails){
         //HDFC_DASH_Applicant_Employment_Details__c newMapData = (HDFC_DASH_Applicant_Employment_Details__c) Trigger.newMap.get(empDetRec.Id);
         HDFC_DASH_Applicant_Employment_Details__c oldMapData = (HDFC_DASH_Applicant_Employment_Details__c) Trigger.oldMap?.get(empDetRec.Id);
            if((empDetRec.HDFC_DASH_Nat_Of_Emp__c!= oldMapData?.HDFC_DASH_Nat_Of_Emp__c) &&(String.isNotBlank(empDetRec.HDFC_DASH_Nat_Of_Emp__c) &&(!empDetRec.HDFC_DASH_Application_Created_From_ILPS__c))){
              // )){
                //&& (!empDetRec.HDFC_DASH_Application_Created_From_ILPS__c))){
                listUpdateEmpDetails.add(empDetRec);
            }
        }
   /* }
        else{
          for(HDFC_DASH_Applicant_Employment_Details__c empDetRec: listNewEmpDetails){
            if((String.isNotBlank(empDetRec.HDFC_DASH_Nat_Of_Emp__c)) && (!empDetRec.HDFC_DASH_Application_Created_From_ILPS__c)){
                listUpdateEmpDetails.add(empDetRec);
            }
        }  
        }*/
        return listUpdateEmpDetails;
    }
    
    /*
    Method Name :updateApplDetRec
    Description :This method would update the Nature of Emp for Applicant detail record.
    */
    public static void updateApplDetRec(List<HDFC_DASH_Applicant_Employment_Details__c> listEmpDetRecs){
        List<Id> listADIds = new List<Id>();
        Map<Id,HDFC_DASH_Applicant_Details__c> mapIdtoApplDet = new Map<Id,HDFC_DASH_Applicant_Details__c>();
        List<string> listOfFields_AppDetails = new List<string>{HDFC_DASH_Constants.ID_STRING};
            for(HDFC_DASH_Applicant_Employment_Details__c empDetRec: listEmpDetRecs){
                listADIds.add(empDetRec.HDFC_DASH_Applicant_Details__c);
            }
        Map<ID, HDFC_DASH_Applicant_Details__c> mapADRecs = new Map<ID, HDFC_DASH_Applicant_Details__c>( HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listADIds, listOfFields_AppDetails, HDFC_DASH_Constants.ID_STRING));
        for(HDFC_DASH_Applicant_Employment_Details__c empDetRec : listEmpDetRecs){
            HDFC_DASH_Applicant_Details__c objApplDet = mapADRecs.get(empDetRec.HDFC_DASH_Applicant_Details__c);
            objApplDet.HDFC_DASH_Nat_Of_Emp__c = empDetRec.HDFC_DASH_Nat_Of_Emp__c;
            mapIdtoApplDet.put(objApplDet.Id,objApplDet);
        }
        if(Schema.sObjectType.HDFC_DASH_Applicant_Details__c.isUpdateable()){
            update mapIdtoApplDet.values();
        }
    }

    
}