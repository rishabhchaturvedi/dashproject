/*
Author: Kumar Gourav
Class: HDFC_DASH_InboundRestService_DocReview
Date: 27 Oct 2021
Company: Accenture
Description: Service Class for getCustDetails Info API
*/
public with sharing class HDFC_DASH_InboundRestService_DocReview{
    /* 
Method:getDocReview
Description: Method to get the Mandatory and Received Documents.
*/
    public static HDFC_DASH_APIResponse_DocReview getDocReview(RestRequest request)
    {      
        String sfApplicationId = RestContext.request.params.get(HDFC_DASH_Constants.STRING_SF_APPLICATIONID);
        
        //get Customer details
        HDFC_DASH_APIResponse_DocReview apiRes = getDocReviewResponse(sfApplicationId);
        return apiRes;  
    }
    /* 
Method: getDocReviewResponse
Description: Method to get the list of Mandatory and Received Documents.
*/
    public static HDFC_DASH_APIResponse_DocReview getDocReviewResponse(String sfAppId){        
        HDFC_DASH_APIResponse_DocReview apiRes;
        
        if(String.isNotBlank(sfAppId))
        {
            List<Id> listAppId = new List<Id>{sfAppId};
                List<String> listOfFields_App = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber,
                    HDFC_DASH_Constants.HDFC_DASH_ADDLPRODUCTTYPE,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Mode,
                    HDFC_DASH_Constants.LOAN_TYPE_CODE,HDFC_DASH_Constants.HDFC_DASH_Emp_Class, HDFC_DASH_Constants.HDFC_DASH_LMS_Agency_Code,HDFC_DASH_Constants.RECORD_TYPE_ID,HDFC_DASH_Constants.RECORDTYPE_NAME,
                    HDFC_DASH_Constants.HDFC_DASH_Refinance_Flag,HDFC_DASH_Constants.HDFC_DASH_FastTrack,HDFC_DASH_Constants.HDFC_DASH_Is_SimultaneousLoan,HDFC_DASH_Constants.HDFC_DASH_Is_PrimeFlag,HDFC_DASH_Constants.HDFC_DASH_Prime_Application,HDFC_DASH_Constants.HDFC_DASH_Preferred_Branch_Code};
                        
                        List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.APPLICANT_CAPACITY,
                            HDFC_DASH_Constants.HDFC_DASH_Salutation,HDFC_DASH_Constants.HDFC_DASH_FirstName,HDFC_DASH_Constants.HDFC_DASH_MiddleName,HDFC_DASH_Constants.HDFC_DASH_LastName,
                            HDFC_DASH_Constants.HDFC_DASH_Resident_Type,HDFC_DASH_Constants.HDFC_DASH_PAN,HDFC_DASH_Constants.Contact_Account_HDFC_DASH_Customer_Number,HDFC_DASH_Constants.HDFC_DASH_Nat_Of_Emp,HDFC_DASH_Constants.HDFC_DASH_Income_Considered};
                                
                                List<Opportunity> listApp = HDFC_DASH_Application_Selector.getApplication(listAppId, listOfFields_App, HDFC_DASH_Constants.ID_STRING); 
            
            if(!listApp.isEmpty())
            {
                List<HDFC_DASH_Applicant_Details__c> listAppDetails= HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppId, listOfFields_AppDetails, HDFC_DASH_Constants.HDFC_DASH_Application);
                Opportunity app = listApp[HDFC_DASH_Constants.INT_ZERO];
                
                HttpResponse minMandResponse = doMinMandIlpsCallout(app);
                
                if(minMandResponse !=null){
                    apiRes =  getDocReviewResponse_MinMandDocs(minMandResponse);
                    storeMinMandResponse(minMandResponse, app);
                }
                apiRes = getDocReviewResponse_InfoVal(sfAppId, listAppDetails, apiRes);
            }
            else{
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ApplicationId_Exp); 
            }
        }
        else{
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_PARAMS_EXP); 
        }
        return apiRes;   
    }
    
    /*
Method: doMinMandIlpsCallout
Description: This method will make a callout to ILPS Min Mand Docs.
*/
    public static HttpResponse doMinMandIlpsCallout(Opportunity app) {
        
        HttpResponse response;
        Map<String,String> requestHeader = new Map<String,String>();
        RestRequest request;
        Exception exp;
        List<Opportunity> primeAppList = new List<Opportunity>();
        
        List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.APPDETAIL_CONTACT_R_ACCOUNT_CUSTOMER_NUMBER,
            HDFC_DASH_Constants.HDFC_DASH_Salutation,HDFC_DASH_Constants.HDFC_DASH_FirstName,HDFC_DASH_Constants.HDFC_DASH_MiddleName,HDFC_DASH_Constants.HDFC_DASH_LastName,HDFC_DASH_Constants.HDFC_DASH_NRI_Visa_Type,
            HDFC_DASH_Constants.HDFC_DASH_Resident_Type,HDFC_DASH_Constants.HDFC_DASH_PAN,HDFC_DASH_Constants.APPDETAIL_CONTACT_R_CONTACT_NUMBER,HDFC_DASH_Constants.HDFC_DASH_Nat_Of_Emp,HDFC_DASH_Constants.HDFC_DASH_Income_Considered};
        
        HFDC_DASH_MinMandReq_ILPSCallout minMandReqBody = new HFDC_DASH_MinMandReq_ILPSCallout();
        HFDC_DASH_MinMandReq_ILPSCallout.ApplicationParams appParams= new HFDC_DASH_MinMandReq_ILPSCallout.ApplicationParams();
        List<HFDC_DASH_MinMandReq_ILPSCallout.ApplicantParams> listapplEmpDet = new List<HFDC_DASH_MinMandReq_ILPSCallout.ApplicantParams>();
        
        Opportunity appRec;
        List<String> listAppRecId =new List<String>();
        
        //Checking for Additional Loan
        if(app.RecordType.name == HDFC_DASH_Constants.ADDITIONAL_LOAN)
        {  
            if(app.HDFC_DASH_Prime_Application__c != null){
            List<String> listPrimeApplId = new List<String>{app.HDFC_DASH_Prime_Application__c};
                List<String> primeAppFieldList = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber,
                    HDFC_DASH_Constants.HDFC_DASH_ADDLPRODUCTTYPE,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Mode,
                    HDFC_DASH_Constants.LOAN_TYPE_CODE,HDFC_DASH_Constants.HDFC_DASH_Emp_Class, HDFC_DASH_Constants.HDFC_DASH_LMS_Agency_Code,HDFC_DASH_Constants.RECORD_TYPE_ID,HDFC_DASH_Constants.RECORDTYPE_NAME,
                    HDFC_DASH_Constants.HDFC_DASH_Refinance_Flag,HDFC_DASH_Constants.HDFC_DASH_FastTrack,HDFC_DASH_Constants.HDFC_DASH_Is_SimultaneousLoan,HDFC_DASH_Constants.HDFC_DASH_Is_PrimeFlag,HDFC_DASH_Constants.HDFC_DASH_Prime_Application,HDFC_DASH_Constants.HDFC_DASH_Preferred_Branch_Code};
                            
             primeAppList = HDFC_DASH_Application_Selector.getApplication(listPrimeApplId, primeAppFieldList, HDFC_DASH_Constants.ID_STRING);
            appRec = primeAppList[HDFC_DASH_Constants.INT_ZERO];
            listAppRecId.add(appRec.id);
            }
            else{
               HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_Error_PrimeApplication_Exp);  
            }
        }
        else{
            appRec = app;
            listAppRecId.add(app.id);
        } 
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppRecId, listOfFields_AppDetails, HDFC_DASH_Constants.HDFC_DASH_Application);
        
        appParams.isSimultaneousLoan = app.HDFC_DASH_Is_SimultaneousLoan__c ; 
        appParams.isPrimeFlag = app.HDFC_DASH_Is_PrimeFlag__c ; 
        appParams.requiredProduct = app.HDFC_DASH_Loan_Type_Code__c ; 
       
        appParams.sfApplicationNumber = appRec.HDFC_DASH_ApplicationNumber__c;
        appParams.addlProdType = appRec.HDFC_DASH_AddlProductType__c;
        if(String.isNotBlank(appRec.HDFC_DASH_Preferred_Branch_Code__c))
        {
            appParams.originBranch = appRec.HDFC_DASH_Preferred_Branch_Code__c;
        }
        else{
            appParams.originBranch = appRec.HDFC_DASH_LMS_Origin_Branch_Id__c;
        }
        appParams.originMode = appRec.HDFC_DASH_LMS_Origin_Mode__c;
        appParams.refinanceFlag = appRec.HDFC_DASH_Refinance_Flag__c;
        appParams.fastTrack = appRec.HDFC_DASH_FastTrack__c ;
        appParams.empClass = appRec.HDFC_DASH_Emp_Class__c ;
        appParams.agencyCode = appRec.HDFC_DASH_LMS_Agency_Code__c ;
        
        for(HDFC_DASH_Applicant_Details__c appDet : listAppDetails)
        {
            HFDC_DASH_MinMandReq_ILPSCallout.ApplicantParams applicantParams= new HFDC_DASH_MinMandReq_ILPSCallout.ApplicantParams(); 
            applicantParams.sfCustomerNumber = appDet.HDFC_DASH_Contact__r.Account.HDFC_DASH_Customer_Number__c;
            applicantParams.capacity = appDet.HDFC_DASH_Applicant_Capacity__c;
            applicantParams.custTitle = appDet.HDFC_DASH_Salutation__c ;
            applicantParams.firstName = appDet.HDFC_DASH_FirstName__c ;
            applicantParams.middleName = appDet.HDFC_DASH_MiddleName__c  ;
            applicantParams.lastName = appDet.HDFC_DASH_LastName__c ;
            if(appDet.HDFC_DASH_Resident_Type__c == HDFC_DASH_Constants.STRING_NRI){
                applicantParams.nriFlag = HDFC_DASH_Constants.STRING_Y;
            }
            
            applicantParams.panNo = appDet.HDFC_DASH_PAN__c  ;
            applicantParams.employNat = appDet.HDFC_DASH_Nat_Of_Emp__c ;
            applicantParams.incomeCons = appDet.HDFC_DASH_Income_Considered__c;
            applicantParams.addressChanged = HDFC_DASH_Constants.STRING_N  ;
            applicantParams.employmentChanged = HDFC_DASH_Constants.STRING_N;
            applicantParams.visaType = appDet.HDFC_DASH_NRI_Visa_Type__c;
            listapplEmpDet.add(applicantParams);
        }
        appParams.applicantParams = listapplEmpDet;
        minMandReqBody.applicationParams = appParams;
        InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess(); 
        
        requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_MM);
        response = interfaceObj.doCallOut(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_MM, HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(minMandReqBody)), requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
        return response;       
    }
    
    /*
Method: getDocReviewResponse_MinMandDocs
Description: This method will populate the Min Mand Docs Response into the DocReview Response
*/
    public static HDFC_DASH_APIResponse_DocReview getDocReviewResponse_MinMandDocs(HttpResponse response){
        HDFC_DASH_APIResponse_DocReview docReviewRes = new HDFC_DASH_APIResponse_DocReview();
        List<HDFC_DASH_APIResponse_DocReview.MandatoryDocs> listMinMandRes = new List<HDFC_DASH_APIResponse_DocReview.mandatoryDocs>();
        
        if(response.getStatusCode() == HDFC_DASH_Constants.INT_TWO_HUNDRED)
        { 
            HDFC_DASH_ParseResponseMinMandDocs calloutResponse = HDFC_DASH_ParseResponseMinMandDocs.parse(response.getBody());
            for(HDFC_DASH_ParseResponseMinMandDocs.ListOfDocs doc : calloutResponse?.listOfDocs){
                HDFC_DASH_APIResponse_DocReview.MandatoryDocs minMandNode = new HDFC_DASH_APIResponse_DocReview.MandatoryDocs();
                
                minMandNode.sfapplicantno = doc?.sfapplicantno;
                minMandNode.doccode = doc?.doccode;
                minMandNode.dockey4val = doc?.dockey4val;
                minMandNode.dockey5val = doc?.dockey5val;
                minMandNode.docfromdate = doc?.docfromdate;
                minMandNode.doctodate = doc?.doctodate;
                minMandNode.mindoc = doc?.mindoc;
                minMandNode.manddoc = doc?.manddoc;
                listMinMandRes.add(minMandNode);
            }
            docReviewRes.mandatoryDocs = listMinMandRes;
        }
        return docReviewRes;
    }
    
    /*
Method: storeMinMandResponse
Description: This method will store Min Mand Docs Response in Info Validation Object.
*/    
    public static Void storeMinMandResponse(HttpResponse response, Opportunity app){
        
        HDFC_DASH_Info_Validation__c infoValRec = new HDFC_DASH_Info_Validation__c();
        
        infoValRec.HDFC_DASH_Validation_Response__c = response?.getBody();
        infoValRec.HDFC_DASH_SF_Triggered__c = HDFC_DASH_Constants.BOOLEAN_TRUE;
        infoValRec.HDFC_DASH_SF_Triggered_Integration_Name__c  = HDFC_DASH_Constants.MANDATORYDOCSFROM_ILPS;
        infoValRec.HDFC_DASH_Application__c = app.Id;
        infoValRec.HDFC_DASH_Application_Number__c = app.HDFC_DASH_ApplicationNumber__c;
        
        Insert infoValRec;       
    }
    
    /*
Method: getDocReviewResponse_InfoVal
Description: This method will map the info validation records to DocReview response
*/
    public static HDFC_DASH_APIResponse_DocReview getDocReviewResponse_InfoVal(Id sfAppId, List<HDFC_DASH_Applicant_Details__c>listAppDetails, HDFC_DASH_APIResponse_DocReview docReviewRes)
    {
        
        List<HDFC_DASH_APIResponse_DocReview.ReceivedDocs> listInfoValRes = new List<HDFC_DASH_APIResponse_DocReview.ReceivedDocs>();
        List<Id> listAppDetailsID = new List<Id>();
        
        for(HDFC_DASH_Applicant_Details__c appDetail : listAppDetails)
        {
            listAppDetailsID.add(appDetail.Id);                      
        }   
        
        Map<string,string> mapCondition = new Map<string,string>();
        if(!listAppDetails.isEmpty()){
            mapCondition.put(HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + sfAppId + HDFC_DASH_Constants.STRING_QUOTE);
            mapCondition.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAppDetailsID, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE + HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        }
        else{
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + sfAppId + HDFC_DASH_Constants.STRING_QUOTE);
        }
        mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_SF_Triggered, HDFC_DASH_Constants.STRING_NOTEQUALTO + HDFC_DASH_Constants.BOOLEAN_TRUE);
        mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_LATEST, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_YES + HDFC_DASH_Constants.STRING_QUOTE);
        mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Info_Validation_Record_Type, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_DOCUMENT + HDFC_DASH_Constants.STRING_QUOTE);
        
        list<string> listOfFields_InfoVal = new list<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, HDFC_DASH_Constants.DOCTYPE, HDFC_DASH_Constants.HDFC_DASH_Doc_Type_Code, HDFC_DASH_Constants.FILENO,
            HDFC_DASH_Constants.MONTHOFPAYSLIP,HDFC_DASH_Constants.HDFC_DASH_Salary_Year, HDFC_DASH_Constants.MONTHOFGSTRETURN, HDFC_DASH_Constants.ASSESSMENTYEAR, HDFC_DASH_Constants.FINANCIALYEAR, HDFC_DASH_Constants.ACCOUNTTYPE, HDFC_DASH_Constants.HDFC_DASH_Account_Number, HDFC_DASH_Constants.FROMDATE,
            HDFC_DASH_Constants.TODATE, HDFC_DASH_Constants.BANKCODE, HDFC_DASH_Constants.HDFC_DASH_Employer_Name, HDFC_DASH_Constants.TOTALAMOUNTPAID, HDFC_DASH_Constants.NOOFQUARTERSPAID, HDFC_DASH_Constants.PROOFOFCOMMUNICATION, HDFC_DASH_Constants.KYCDOCTYPE, HDFC_DASH_Constants.HDFC_DASH_KYC_Doc_Code,
            HDFC_DASH_Constants.BCPDOCTYPE,HDFC_DASH_Constants.HDFC_DASH_BCP_Doc_Code, HDFC_DASH_Constants.DOCEXTRACTABLE, HDFC_DASH_Constants.DOCSCANNEDBY, HDFC_DASH_Constants.DOCIMAGEORIGIN, HDFC_DASH_Constants.HDFC_DASH_Location_Latitude, HDFC_DASH_Constants.HDFC_DASH_Location_Longitude, HDFC_DASH_Constants.STORAGE_IDENTIFIER,
            HDFC_DASH_Constants.STORAGE_TYPE,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.HDFC_DASH_ILPS_Cust_Number,
            HDFC_DASH_Constants.HDFC_DASH_Application_Number,HDFC_DASH_Constants.HDFC_DASH_Contact,HDFC_DASH_Constants.HDFC_DASH_Document_Status,HDFC_DASH_Constants.HDFC_DASH_Uploaded_Date,
            HDFC_DASH_Constants.HDFC_DASH_Validation_Type,HDFC_DASH_Constants.HDFC_DASH_Validated_With,HDFC_DASH_Constants.HDFC_DASH_User_Input_Value,HDFC_DASH_Constants.HDFC_DASH_Salutation,
            HDFC_DASH_Constants.HDFC_DASH_FirstName,HDFC_DASH_Constants.HDFC_DASH_MiddleName,HDFC_DASH_Constants.HDFC_DASH_LastName,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,
            HDFC_DASH_Constants.HDFC_DASH_Gender,HDFC_DASH_Constants.ADDRESS_TYPE,HDFC_DASH_Constants.HDFC_DASH_DMS_Document_Id,HDFC_DASH_Constants.HDFC_DASH_Loan_Outstanding_Letter_Upload,
            HDFC_DASH_Constants.HDFC_DASH_Property_Cost_Sheet_Upload,HDFC_DASH_Constants.HDFC_DASH_Financial_Info_Document_Upload,HDFC_DASH_Constants.HDFC_DASH_Info_Validation_Record_Type,HDFC_DASH_Constants.HDFC_DASH_Income_Proof,
            HDFC_DASH_Constants.HDFC_DASH_Bank_Statement_Upload,HDFC_DASH_Constants.HDFC_DASH_Addl_Income_Document_Type,HDFC_DASH_Constants.HDFC_DASH_Salary_Slip_Upload_Month,HDFC_DASH_Constants.HDFC_DASH_Salary_Slip_Upload_Week1,
            HDFC_DASH_Constants.HDFC_DASH_Salary_Slip_Upload_Week2,HDFC_DASH_Constants.HDFC_DASH_Salary_Slip_Upload_Week3,HDFC_DASH_Constants.HDFC_DASH_Salary_Slip_Upload_Week4,HDFC_DASH_Constants.HDFC_DASH_Account_Holder_Name,
            HDFC_DASH_Constants.HDFC_DASH_Bank_Name,HDFC_DASH_Constants.LOAN_TYPE,HDFC_DASH_Constants.LOAN_TYPE_CODE,HDFC_DASH_Constants.HDFC_DASH_Installment_Amount,
            HDFC_DASH_Constants.HDFC_DASH_Outstanding_Amount,HDFC_DASH_Constants.HDFC_DASH_BalanceTermOfLoanInMonths,HDFC_DASH_Constants.HDFC_DASH_IfscCode,HDFC_DASH_Constants.HDFC_DASH_Employee_Number,
            HDFC_DASH_Constants.HDFC_DASH_Employer_Code,HDFC_DASH_Constants.HDFC_DASH_Department,HDFC_DASH_Constants.HDFC_DASH_Designation,HDFC_DASH_Constants.HDFC_DASH_Job_Duration_In_Months,
            HDFC_DASH_Constants.HDFC_DASH_Passport_Number,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Expiry,HDFC_DASH_Constants.HDFC_DASH_Nationality,HDFC_DASH_Constants.HDFC_DASH_PR_Number,
            HDFC_DASH_Constants.HDFC_DASH_PR_Country,HDFC_DASH_Constants.HDFC_DASH_PR_Country_Code,HDFC_DASH_Constants.HDFC_DASH_PR_Validity_Date,HDFC_DASH_Constants.HDFC_DASH_Address_Line1,
            HDFC_DASH_Constants.HDFC_DASH_Address_Line2,HDFC_DASH_Constants.HDFC_DASH_Address_Line3,HDFC_DASH_Constants.HDFC_DASH_Address_Line4,HDFC_DASH_Constants.HDFC_DASH_Landmark,
            HDFC_DASH_Constants.HDFC_DASH_City,HDFC_DASH_Constants.HDFC_DASH_City_Code,HDFC_DASH_Constants.HDFC_DASH_Taluka,HDFC_DASH_Constants.HDFC_DASH_District,
            HDFC_DASH_Constants.HDFC_DASH_State,HDFC_DASH_Constants.HDFC_DASH_State_Code,HDFC_DASH_Constants.HDFC_DASH_Country,HDFC_DASH_Constants.HDFC_DASH_Country_Code,
            HDFC_DASH_Constants.HDFC_DASH_Pincode,HDFC_DASH_Constants.HDFC_DASH_Post_Office_Name,HDFC_DASH_Constants.HDFC_DASH_Validation_Response,HDFC_DASH_Constants.HDFC_DASH_Designation_Entity_Type,
            HDFC_DASH_Constants.HDFC_DASH_Business_Name,HDFC_DASH_Constants.HDFC_DASH_Year_Of_Incorporation,HDFC_DASH_Constants.HDFC_DASH_Annual_Turnover,HDFC_DASH_Constants.HDFC_DASH_Nature_Of_Business,
            HDFC_DASH_Constants.HDFC_DASH_Industry_Type,HDFC_DASH_Constants.HDFC_DASH_Business_PAN,HDFC_DASH_Constants.HDFC_DASH_Udyam_Registration_number,HDFC_DASH_Constants.HDFC_DASH_CIN_Number,HDFC_DASH_Constants.HDFC_DASH_Document_File_Uploaded,
            HDFC_DASH_Constants.HDFC_DASH_GST_Number,HDFC_DASH_Constants.HDFC_DASH_Emp_Business_GST_State_Code,HDFC_DASH_Constants.HDFC_DASH_Emp_Business_GST_State,HDFC_DASH_Constants.HDFC_DASH_Proof_Of_Communication_Code,
            HDFC_DASH_Constants.HDFC_DASH_OCR_Response,HDFC_DASH_Constants.HDFC_DASH_SF_Triggered,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Issue,HDFC_DASH_Constants.HDFC_DASH_Customer_Number,HDFC_DASH_Constants.HDFC_DASH_LATEST,HDFC_DASH_Constants.HDFC_DASH_Is_Salary_Account
            };
                
                
                List<HDFC_DASH_Info_Validation__c> listinfoval = HDFC_DASH_AppInfoValidation_Selector.getappInfoValBasedOnQuery(listOfFields_InfoVal, mapCondition);
       
        for(HDFC_DASH_Info_Validation__c objInfoVal : listinfoval)
        {
            HDFC_DASH_APIResponse_DocReview.ReceivedDocs infoValNode = new HDFC_DASH_APIResponse_DocReview.ReceivedDocs();
            HDFC_DASH_APIResponse_DocReview.ApplicantInfoValidation applinfoValNode =new  HDFC_DASH_APIResponse_DocReview.ApplicantInfoValidation();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedFinancialInformation prePopulatedFinancialInfo = new  HDFC_DASH_APIResponse_DocReview.PrePopulatedFinancialInformation();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedSalaryInformation prePopulatedSalInfo = new HDFC_DASH_APIResponse_DocReview.PrePopulatedSalaryInformation();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedBankDetails prepopulatedBD = new HDFC_DASH_APIResponse_DocReview.PrePopulatedBankDetails();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedEmployerInformation prepopulatedEmpInfo= new HDFC_DASH_APIResponse_DocReview.PrePopulatedEmployerInformation();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedEmpGstBusinessDetails prepopulatedEmpGst= new  HDFC_DASH_APIResponse_DocReview.PrePopulatedEmpGstBusinessDetails();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedBusinessInformation prepopulatedBusinessInfo= new HDFC_DASH_APIResponse_DocReview.PrePopulatedBusinessInformation();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedPassportInformation prepopulatedPassportInfo= new HDFC_DASH_APIResponse_DocReview.PrePopulatedPassportInformation();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedPermanentResidency prepopulatedPermanentResidency= new HDFC_DASH_APIResponse_DocReview.PrePopulatedPermanentResidency();
            HDFC_DASH_APIResponse_DocReview.PrePopulatedAddressDetails prepopulatedAddressDetails= new HDFC_DASH_APIResponse_DocReview.PrePopulatedAddressDetails();
            
            infoValNode.sfInfoValidationId = objInfoVal?.Id ;
            infoValNode.sfApplicationID = objInfoVal?.HDFC_DASH_Application__c ;
            infoValNode.sfApplicantId = objInfoVal?.HDFC_DASH_Applicant_Details__c ;
            infoValNode.sfApplicationNumber = objInfoVal?.HDFC_DASH_Application_Number__c ;
            infoValNode.sfCustomerId =  objInfoVal?.HDFC_DASH_Contact__c ;
            infoValNode.sfCustomerNumber =  objInfoVal?.HDFC_DASH_Customer_Number__c ;
            infoValNode.fileNumber =  objInfoVal?.HDFC_DASH_File_Number__c ;
            infoValNode.documentStatus  =  objInfoVal?.HDFC_DASH_Document_Status__c ;
            infoValNode.customerNumber =  objInfoVal?.HDFC_DASH_ILPS_Cust_Number__c;
            infoValNode.customerCapacity =  objInfoVal?.HDFC_DASH_Applicant_Capacity__c;
            infoValNode.uploadedDate =  objInfoVal?.HDFC_DASH_Uploaded_Date__c;
            infoValNode.infoValidationRecordType =  objInfoVal?.HDFC_DASH_Info_Validation_Record_Type__c;
            infoValNode.isLatest =  objInfoVal?.HDFC_DASH_Latest__c;
            
            
            applinfoValNode.validationType =  objInfoVal?.HDFC_DASH_Validation_Type__c ;
            applinfoValNode.source =  objInfoVal?.HDFC_DASH_Validated_With__c ;
            applinfoValNode.docTypeCode =  objInfoVal?.HDFC_DASH_Doc_Type_Code__c;
            applinfoValNode.docType =  objInfoVal?.HDFC_DASH_Doc_Type__c ;
            applinfoValNode.docNumber =  objInfoVal?.HDFC_DASH_User_Input_Value__c ;
            applinfoValNode.dateOfIssue =  objInfoVal?.HDFC_DASH_Date_Of_Issue__c ;
            applinfoValNode.dateOfExpiry =  objInfoVal?.HDFC_DASH_Date_Of_Expiry__c ;
            applinfoValNode.salutation =  objInfoVal?.HDFC_DASH_Salutation__c ;
            applinfoValNode.firstName =  objInfoVal?.HDFC_DASH_FirstName__c ;
            applinfoValNode.middleName  =  objInfoVal?.HDFC_DASH_MiddleName__c ;
            applinfoValNode.lastName  =  objInfoVal?.HDFC_DASH_LastName__c ;
            applinfoValNode.dateOfBirth  =  objInfoVal?.HDFC_DASH_Date_Of_Birth__c ;
            applinfoValNode.genderCode =  objInfoVal?.HDFC_DASH_Gender__c ;
            applinfoValNode.addressType =  objInfoVal?.HDFC_DASH_Address_Type__c ;
            applinfoValNode.documentFileUploaded  = objInfoVal?.HDFC_DASH_Document_File_Uploaded__c ;
            applinfoValNode.loanOutstandingLetterUpload  =  objInfoVal?.HDFC_DASH_Loan_Outstanding_Letter_Upload__c ;
            applinfoValNode.propertyCostSheetUpload  =  objInfoVal?.HDFC_DASH_Property_Cost_Sheet_Upload__c ;
            applinfoValNode.dmsDocumentId =  objInfoVal?.HDFC_DASH_DMS_Document_Id__c ;
            applinfoValNode.financialInfoDocumentUpload  =  objInfoVal?.HDFC_DASH_Financial_Info_Document_Upload__c ;
            applinfoValNode.docExtractable =  objInfoVal?.HDFC_DASH_Document_Extractable__c;
            applinfoValNode.docImageOrigin = objInfoVal?.HDFC_DASH_Document_Image_Origin__c;
            applinfoValNode.docScannedBy = objInfoVal?.HDFC_DASH_Document_Scanned_By__c;
            applinfoValNode.monthOfGstReturn = objInfoVal?.HDFC_DASH_Mon_Of_GST_Return__c;
            applinfoValNode.assessmentYear = objInfoVal?.HDFC_DASH_Assessment_Year__c;
            applinfoValNode.financialYear = objInfoVal?.HDFC_DASH_Financial_Year__c;
            applinfoValNode.totalAmountPaid = objInfoVal?.HDFC_DASH_Total_Amount_Paid__c ;
            applinfoValNode.noOfQuartersPaid = objInfoVal?.HDFC_DASH_No_Of_Quarters_Paid__c;
            applinfoValNode.proofOfCommunication = objInfoVal?.HDFC_DASH_Proof_Of_Communication__c;
            applinfoValNode.bcpDocType = objInfoVal?.HDFC_DASH_BCP_Doc_Type__c;
            applinfoValNode.bcpDocTypeCode = objInfoVal?.HDFC_DASH_BCP_Doc_Code__c;
            applinfoValNode.latitude = objInfoVal?.HDFC_DASH_Location__Latitude__s ;
            applinfoValNode.longitude = objInfoVal?.HDFC_DASH_Location__Longitude__s ;
            applinfoValNode.kycDocType = objInfoVal?.HDFC_DASH_KYC_Doc_Type__c;
            applinfoValNode.kycDocTypeCode = objInfoVal?.HDFC_DASH_KYC_Doc_Code__c;
            applinfoValNode.storageIdentifier = objInfoVal?.HDFC_DASH_Storage_Identifier__c;
            applinfoValNode.storageType = objInfoVal?.HDFC_DASH_Storage_Type__c;
            
            prePopulatedFinancialInfo.accountHolderName =  objInfoVal?.HDFC_DASH_Account_Holder_Name__c ;
            prePopulatedFinancialInfo.bankName = objInfoVal?.HDFC_DASH_Bank_Name__c ;
            prePopulatedFinancialInfo.bankCode = objInfoVal?.HDFC_DASH_Bank_Code__c ;
            prePopulatedFinancialInfo.loanType = objInfoVal?.HDFC_DASH_Loan_Type__c ;
            prePopulatedFinancialInfo.loanTypeCode = objInfoVal?.HDFC_DASH_Loan_Type_Code__c;
            prePopulatedFinancialInfo.outstandingAmount = objInfoVal?.HDFC_DASH_Outstanding_Amount__c ;
            prePopulatedFinancialInfo.installmentAmount = objInfoVal?.HDFC_DASH_Installment_Amount__c ;
            prePopulatedFinancialInfo.balanceTermOfLoanInMonths = objInfoVal?.HDFC_DASH_Balance_Term_Of_Loan_In_Months__c ;
            
            prePopulatedSalInfo.incomeProof  = objInfoVal?.HDFC_DASH_Income_Proof__c ;
            prePopulatedSalInfo.salaryMonth  = objInfoVal?.HDFC_DASH_Salary_Month__c ;
            prePopulatedSalInfo.salaryYear  = objInfoVal?.HDFC_DASH_Salary_Year__c ;
            prePopulatedSalInfo.additionalIncomeDocumentType  = objInfoVal?.HDFC_DASH_Addl_Income_Document_Type__c ;
            prePopulatedSalInfo.bankStatementUpload  = objInfoVal?.HDFC_DASH_Bank_Statement_Upload__c ;
            prePopulatedSalInfo.salarySlipUploadMonth  = objInfoVal?.HDFC_DASH_Salary_Slip_Upload_Month__c ;
            prePopulatedSalInfo.salarySlipUploadWeek1  = objInfoVal?.HDFC_DASH_Salary_Slip_Upload_Week1__c ;
            prePopulatedSalInfo.salarySlipUploadWeek2  = objInfoVal?.HDFC_DASH_Salary_Slip_Upload_Week2__c ;
            prePopulatedSalInfo.salarySlipUploadWeek3  = objInfoVal?.HDFC_DASH_Salary_Slip_Upload_Week3__c ;
            prePopulatedSalInfo.salarySlipUploadWeek4  = objInfoVal?.HDFC_DASH_Salary_Slip_Upload_Week4__c ;
            
            prepopulatedBD.accountType = objInfoVal?.HDFC_DASH_Account_Type__c ;
            prepopulatedBD.fromDate = objInfoVal?.HDFC_DASH_From_Date__c ;
            prepopulatedBD.toDate = objInfoVal?.HDFC_DASH_To_Date__c ;
            prepopulatedBD.accountHolderName = objInfoVal?.HDFC_DASH_Account_Holder_Name__c ;
            prepopulatedBD.bankName = objInfoVal?.HDFC_DASH_Bank_Name__c ;
            prepopulatedBD.bankCode = objInfoVal?.HDFC_DASH_Bank_Code__c ;
            prepopulatedBD.accountNumber = objInfoVal?.HDFC_DASH_Account_Number__c ;
            prepopulatedBD.ifscCode = objInfoVal?.HDFC_DASH_Ifsc_Code__c ;
            prepopulatedBD.isSalaryAccount = objInfoVal?.HDFC_DASH_Is_Salary_Account__c ;
            
            prepopulatedEmpInfo.employerName = objInfoVal?.HDFC_DASH_Employer_Name__c ;
            prepopulatedEmpInfo.employerCode = objInfoVal?.HDFC_DASH_Employer_Code__c ;
            prepopulatedEmpInfo.employeeNumber = objInfoVal?.HDFC_DASH_Employee_Number__c ;
            prepopulatedEmpInfo.designation = objInfoVal?.HDFC_DASH_Designation__c ;
            prepopulatedEmpInfo.department = objInfoVal?.HDFC_DASH_Department__c ;
            prepopulatedEmpInfo.jobDurationInMonths = objInfoVal?.HDFC_DASH_Job_Duration_In_Months__c ;
            
            prepopulatedEmpGst.gstNumber = objInfoVal?.HDFC_DASH_GST_Number__c ;
            prepopulatedEmpGst.empGstState = objInfoVal?.HDFC_DASH_Emp_Business_GST_State__c ;
            prepopulatedEmpGst.empGstStateCode = objInfoVal?.HDFC_DASH_Emp_Business_GST_State_Code__c ;
            
            prepopulatedBusinessInfo.designationAndEntityType = objInfoVal?.HDFC_DASH_Designation_Entity_Type__c ;
            prepopulatedBusinessInfo.businessName = objInfoVal?.HDFC_DASH_Business_Name__c ;
            prepopulatedBusinessInfo.yearOfIncorporation = objInfoVal?.HDFC_DASH_Year_Of_Incorporation__c ;
            prepopulatedBusinessInfo.annualTurnover = objInfoVal?.HDFC_DASH_Annual_Turnover__c ;
            prepopulatedBusinessInfo.natureOfBusiness = objInfoVal?.HDFC_DASH_Nature_Of_Business__c ;
            prepopulatedBusinessInfo.industryType = objInfoVal?.HDFC_DASH_Industry_Type__c ;
            prepopulatedBusinessInfo.businessPAN = objInfoVal?.HDFC_DASH_Business_PAN__c ;
            prepopulatedBusinessInfo.udyamRegistrationNumber = objInfoVal?.HDFC_DASH_Udyam_Registration_number__c ;
            prepopulatedBusinessInfo.cinNumber = objInfoVal?.HDFC_DASH_CIN_Number__c ;
            
            prepopulatedPassportInfo.passportNumber = objInfoVal?.HDFC_DASH_Passport_Number__c ;
            prepopulatedPassportInfo.nationality  = objInfoVal?.HDFC_DASH_Nationality__c ;
            
            prepopulatedPermanentResidency.prNumber = objInfoVal?.HDFC_DASH_PR_Number__c ;
            prepopulatedPermanentResidency.prValidityDate = objInfoVal?.HDFC_DASH_PR_Validity_Date__c ;
            prepopulatedPermanentResidency.prCountry = objInfoVal?.HDFC_DASH_PR_Country__c ;
            prepopulatedPermanentResidency.prCountryCode = objInfoVal?.HDFC_DASH_PR_Country_Code__c ;
            
            prepopulatedAddressDetails.addressLine1 = objInfoVal?.HDFC_DASH_Address_Line1__c ;
            prepopulatedAddressDetails.addressLine2 = objInfoVal?.HDFC_DASH_Address_Line2__c ;
            prepopulatedAddressDetails.addressLine3 = objInfoVal?.HDFC_DASH_Address_Line3__c ;
            prepopulatedAddressDetails.addressLine4 = objInfoVal?.HDFC_DASH_Address_Line4__c ;
            prepopulatedAddressDetails.landmark = objInfoVal?.HDFC_DASH_Landmark__c ;
            prepopulatedAddressDetails.taluka = objInfoVal?.HDFC_DASH_Taluka__c ;
            prepopulatedAddressDetails.district = objInfoVal?.HDFC_DASH_District__c ;
            prepopulatedAddressDetails.city = objInfoVal?.HDFC_DASH_City__c ;
            prepopulatedAddressDetails.cityCode = objInfoVal?.HDFC_DASH_City_Code__c ;
            prepopulatedAddressDetails.state = objInfoVal?.HDFC_DASH_State__c ;
            prepopulatedAddressDetails.stateCode = objInfoVal?.HDFC_DASH_State_Code__c ;
            prepopulatedAddressDetails.country = objInfoVal?.HDFC_DASH_Country__c ;
            prepopulatedAddressDetails.countryCode = objInfoVal?.HDFC_DASH_Country_Code__c ;
            prepopulatedAddressDetails.pinCode = objInfoVal?.HDFC_DASH_Pincode__c ;
            prepopulatedAddressDetails.postOfficeName = objInfoVal?.HDFC_DASH_Post_Office_Name__c ;
            
            applinfoValNode.prePopulatedFinancialInformation = prepopulatedfinancialInfo;
            applinfoValNode.prePopulatedSalaryInformation = prePopulatedSalInfo;
            applinfoValNode.prePopulatedBankDetails = prepopulatedBD;
            applinfoValNode.prePopulatedEmployerInformation = prepopulatedEmpInfo;
            applinfoValNode.prePopulatedEmpGstBusinessDetails = prepopulatedEmpGst;
            applinfoValNode.prePopulatedBusinessInformation = prepopulatedBusinessInfo;
            applinfoValNode.prePopulatedPassportInformation = prepopulatedPassportInfo;
            applinfoValNode.prePopulatedPermanentResidency = prepopulatedPermanentResidency;
            applinfoValNode.prePopulatedAddressDetails = prepopulatedAddressDetails;
            
            infoValNode.applicantInfoValidation =applinfoValNode;
            listInfoValRes.add(infoValNode);
        }
        docReviewRes.receivedDocs = listInfoValRes;
        return docReviewRes;
    }
}