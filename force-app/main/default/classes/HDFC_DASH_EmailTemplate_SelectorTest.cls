/**
className: HDFC_DASH_EmailTemplate_SelectorTest
DevelopedBy: Sai Suman
Date: 22 Dec 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_EmailTemplate_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_EmailTemplate_SelectorTest {
    
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static EmailTemplate emailTemplate = HDFC_DASH_TestDataFactory_EmailTemplate.createEmailTemplate();
    //private static EmailMessage emailMessage = HDFC_DASH_TestDataFactory_EmailMessage.getBasicEmailMes(app.id);
    private static final string SUCCESS ='Success';
    
    /* 
Method Name :testGetEmailTemplate
Description :This method will test the method getEmailTemplate.
*/
    static testmethod void testGetEmailTemplate()
    {
        List<EmailTemplate> emailTempRec = new List<EmailTemplate>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.DEVELOPERNAME};
                Test.startTest();   
            emailTempRec = HDFC_DASH_EmailTemplate_Selector.getEmailTemplate(new List<String> {emailTemplate.Id},fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, emailTempRec);
            //system.assertEquals(1,emailTempRec.size());
        }
    }
        /* 
Method Name :testGetEmailTemplateBasedOnQuery
Description :This method will test the method getEmailTemplateBasedOnQuery().
*/
    static testmethod void testGetEmailTemplateBasedOnQuery()
    {
        List<EmailTemplate> listEmailTemplateRec  = new List<EmailTemplate>();
        System.runAs(sysAdmin)
        {     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.DEVELOPERNAME};
                Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.FOLDER_DEVNAME =>
                    HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                    emailTemplate+HDFC_DASH_Constants.STRING_QUOTE};
                        Test.startTest();
            listEmailTemplateRec = HDFC_DASH_EmailTemplate_Selector.getEmailTemplateBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listEmailTemplateRec);
        //system.assertEquals(1, listEmailTemplateRec.size(),SUCCESS);       
    }

}