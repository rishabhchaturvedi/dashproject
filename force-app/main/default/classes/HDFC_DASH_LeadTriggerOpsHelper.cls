/**
className: HDFC_DASH_LeadTriggerOpsHelper
DevelopedBy: Sai Shruthi
Date: 06 October 2021
Company: Accenture
Class Description: Helper class for HDFC_DASH_LeadTriggerOps.
**/
public with sharing class HDFC_DASH_LeadTriggerOpsHelper {
    
    /* 
Method Name :filterAccountRecForPubPlatformEvent
Description :This method would be called to filter the list of accounts for which the platform event needs to be triggered. 
*/  
    /*public static List<Lead> filterLeadsWithSameLMSLeadID(){
List<Lead> duplicateLeadRecs = new List<Lead>();
try{

Map<Id,String> maplmsLeadId = new Map<Id,String>();
List<Lead> leads = new List<Lead>();
leads = trigger.new;
for(Lead leadRec:leads){
maplmsLeadId.put(leadRec.id,leadRec.HDFC_DASH_LMS_Lead_ID__c);
}
//List<String> lmsLeaIds = new List<String>(maplmsLeadId.keySet());
List<String> leadFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID};
duplicateLeadRecs = HDFC_DASH_Lead_Selector.getLeadsBasedOnCondition(maplmsLeadId.values(),leadFieldList,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID);
}catch(Exception e){
HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.STRING_LEAD,
HDFC_DASH_Constants.FETCH_DUPLICATE_LMS_IDS,NULL,e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON
+ e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
+ HDFC_DASH_Constants.SEMICOLON + e.getTypeName());
}

return duplicateLeadRecs;
}*/
    
    /* 
Method Name :throwExcForDupLeads
Description :This method would throw an exception if a lead exists with the same lead id.
*/
    /*public static void throwExcForDupLeads(List<Lead> dupLeads){
//HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_DUP_LMSID);
List<String> dupLmsLeadIds = new List<String>();
for(Lead leadRec:dupLeads){
if(leadRec.HDFC_DASH_LMS_Lead_ID__c !=NULL)
dupLmsLeadIds.add(leadRec.HDFC_DASH_LMS_Lead_ID__c);
}
if(dupLmsLeadIds != NULL && dupLmsLeadIds?.size()>HDFC_DASH_Constants.INT_ZERO){
trigger.new[HDFC_DASH_Constants.INT_ZERO].adderror(HDFC_DASH_Constants.HDFC_DASH_ERROR_DUP_LMSID +dupLmsLeadIds);
}
}*/
    
    /* 
Method Name :filterPopulateLeadFields_Insert
Description :This method would be called to filter the list of Leads for which have the related fields.
*/ 
    public static List<Lead> filterPopulateLeadFields_Insert(){
        List<Lead> listLeads = new List<Lead>();
        List<Lead> listUpdateLeads = new List<Lead>();
        listLeads = Trigger.new;  
        for(Lead leadRec:listLeads)
        {
            if((String.isNotBlank(leadRec.HDFC_DASH_Source_Id__c) && String.isBlank(leadRec.HDFC_DASH_Source__c)) || (String.isNotBlank(leadRec.HDFC_DASH_Source_Executive_ID__c) && String.isBlank(leadRec.HDFC_DASH_Source_Executive__c)) || (String.isNotBlank(leadRec.HDFC_DASH_Owner_Id__c)))
            {
                listUpdateLeads.add(leadRec);
            }
        }
        return listUpdateLeads;
    }  
    
    /* 
Method Name :populateLeadFields_Insert
Description :This method would be to Update the lookup fields of the leads.
*/ 
    public static void populateLeadFields_Insert(List<Lead> listLeads)
    {
        List<String> listAgencyIds = new List<String>();
        List<String> listSourceIds = new List<String>();
        List<String> listExecutiveIds = new List<String>();
        List<String> listOwnerIds = new List<String>();
        List<Contact> listCon = new List<Contact>();
        List<User> listUsers = new List<User>();
        List<Account> listAcc = new List<Account>();
        Map<String,Id> mapCon = new Map<String,Id>();
        Map<String,String> mapCondition = new Map<String,String>();
        Map<String,String> mapConditionForUsers = new Map<String,String>();
        Map<String,User> mapUsersForExecCode = new Map<String,User>();
        Map<String,User> mapUsersForFederation = new Map<String,User>();
        Map<String,Id> mapAcc = new Map<String,Id>(); 
        
        List<string> listConFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Executive_Code};
            List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Executive_Code,HDFC_DASH_Constants.HDFC_DASH_Federation_Id,HDFC_DASH_Constants.STRING_ACCOUNTID,HDFC_DASH_Constants.ACCOUNT_AGENCYCODE};
                List<String> listAccFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Agency_Code,HDFC_DASH_Constants.HDFC_DASH_Source_Id};
                    Map<String,Id> mapSourceAcc = new Map<String,Id>();
        
        for(Lead leadRec : listLeads)
        { 
            //Processing Source Agency fields
            if(String.isNotBlank(leadRec.HDFC_DASH_Source_Id__c) && String.isBlank(leadRec.HDFC_DASH_Source__c))
            {
                if(leadRec.HDFC_DASH_Source_Reference__c == null || leadRec.HDFC_DASH_Source_Reference__c == HDFC_DASH_Constants.STRING_ARCHIVO)
                {
                    if(String.isBlank(leadRec.HDFC_DASH_Source_Reference__c))
                    {
                        listAgencyIds.add(leadRec.HDFC_DASH_Source_Id__c);
                    }
                    else if(leadRec.HDFC_DASH_Source_Reference__c == HDFC_DASH_Constants.STRING_ARCHIVO){
                        listSourceIds.add(leadRec.HDFC_DASH_Source_Id__c);
                    } 
                }
                else
                {
                    HDFC_DASH_Error_Code__c errorCode_SourceRef = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_SourceReference_Exp);
                    leadRec.adderror(errorCode_SourceRef.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Reference + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Reference__c);
                }
            }
            
            //Processing Source Executive fields
            if(String.isNotBlank(leadRec.HDFC_DASH_Source_Executive_Id__c) && String.isBlank(leadRec.HDFC_DASH_Source_Executive__c))
            {
                listExecutiveIds.add(leadRec.HDFC_DASH_Source_Executive_Id__c);
            }
            
            //Processing Owner Id fields
            if(String.isNotBlank(leadRec.HDFC_DASH_Owner_Id__c))
            {
                listOwnerIds.add(leadRec.HDFC_DASH_Owner_Id__c);
            }
        }
        
        if(!listAgencyIds.isEmpty()){
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Agency_Code, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAgencyIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        }
        else if(!listSourceIds.isEmpty()){
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Source_Id, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listSourceIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);       
        }
        
        if(!mapCondition.isEmpty())
        {
            listAcc = HDFC_DASH_Account_Selector.getAccountsBasedOnQuery( listAccFields, mapCondition); 
        }
        if(!listAcc.isEmpty())
        {
            for(Account accRec:listAcc)
            {
                if(String.isNotBlank(accRec.HDFC_DASH_Agency_Code__c)){
                    mapAcc.put(accRec.HDFC_DASH_Agency_Code__c,accRec.Id);
                }
                if(String.isNotBlank(accRec.HDFC_DASH_Source_Id__c)){
                    mapSourceAcc.put(accRec.HDFC_DASH_Source_Id__c,accRec.Id);
                }
            }
        }
        
        if(!listExecutiveIds.isEmpty()){
            listCon = HDFC_DASH_Contact_Selector.getContacts(listExecutiveIds, listConFields, HDFC_DASH_Constants.HDFC_DASH_Executive_Code);
        }
        if(!listCon.isEmpty()){
            for(Contact conRec:listCon){
                mapCon.put(conRec.HDFC_DASH_Executive_Code__c,conRec.Id);
            }
        }
        
        if(!listOwnerIds.isEmpty())
        {  
            mapConditionForUsers.put(HDFC_DASH_Constants.HDFC_DASH_Executive_Code, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listOwnerIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            mapConditionForUsers.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.HDFC_DASH_Federation_Id, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listOwnerIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        }
        if(!mapConditionForUsers.isEmpty())
        {
            listUsers = HDFC_DASH_User_Selector.getUserBasedOnQuery(listUserFields, mapConditionForUsers);
        }
        if(!listUsers.isEmpty())
        {
            for(User userRec:listUsers)
            {
                if(String.isNotBlank(userRec.HDFC_DASH_Executive_Code__c))
                {
                    mapUsersForExecCode.put(userRec.HDFC_DASH_Executive_Code__c,userRec);
                }
                if(String.isNotBlank(userRec.FederationIdentifier))
                {
                    mapUsersForFederation.put(userRec.FederationIdentifier,userRec);
                }
            }
        }
        
        for(Lead leadRec : listLeads)
        {
            //Populate Source lookup field
            if(String.isNotBlank(leadRec.HDFC_DASH_Source_Id__c) && String.isBlank(leadRec.HDFC_DASH_Source__c))
            {
                if(String.isBlank(leadRec.HDFC_DASH_Source_Reference__c))
                {
                    if(!mapAcc.isEmpty() && mapAcc.containsKey(leadRec.HDFC_DASH_Source_Id__c))
                    {
                        leadRec.HDFC_DASH_Source__c = mapAcc.get(leadRec.HDFC_DASH_Source_Id__c);
                    }
                    else
                    {
                        HDFC_DASH_Error_Code__c errorCode_SourceId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_SOURCEID_EXP);
                        leadRec.adderror(errorCode_SourceId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Id__c);
                    }
                }
                else if(leadRec.HDFC_DASH_Source_Reference__c == HDFC_DASH_Constants.STRING_ARCHIVO)
                {
                    if(!mapSourceAcc.isEmpty() && mapSourceAcc.containsKey(leadRec.HDFC_DASH_Source_Id__c))
                    {
                        leadRec.HDFC_DASH_Source__c = mapSourceAcc.get(leadRec.HDFC_DASH_Source_Id__c);
                    }
                    else
                    {
                        HDFC_DASH_Error_Code__c errorCode_SourceId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_SOURCEID_EXP);
                        leadRec.adderror(errorCode_SourceId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Id__c);
                    }
                }
            }
            
            //Populate Source Executive lookup field
            if(String.isNotBlank(leadRec.HDFC_DASH_Source_Executive_Id__c) && String.isBlank(leadRec.HDFC_DASH_Source_Executive__c))
            {
                if(!mapCon.isEmpty() && mapCon.containsKey(leadRec.HDFC_DASH_Source_Executive_Id__c)){
                    leadRec.HDFC_DASH_Source_Executive__c = mapCon.get(leadRec.HDFC_DASH_Source_Executive_Id__c);
                }
                else{
                    HDFC_DASH_Error_Code__c errorCode_SourceExecutiveId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_SOURCEEXECUTIVEID_EXP);
                    leadRec.adderror(errorCode_SourceExecutiveId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Executive_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Executive_Id__c);            
                }
            }
            
            //Populate Owner lookup field
            if(String.isNotBlank(leadRec.HDFC_DASH_Owner_Id__c))
            {
                if(!mapUsersForExecCode.isEmpty() && mapUsersForExecCode.containsKey(leadRec.HDFC_DASH_Owner_Id__c)){
                    leadRec.OwnerId = mapUsersForExecCode.get(leadRec.HDFC_DASH_Owner_Id__c).Id; 
                    leadRec.HDFC_DASH_Owner_Agency_Id__c = mapUsersForExecCode.get(leadRec.HDFC_DASH_Owner_Id__c)?.account?.HDFC_DASH_Agency_Code__c; 
                    leadRec.HDFC_DASH_Owner_Agency__c = mapUsersForExecCode.get(leadRec.HDFC_DASH_Owner_Id__c)?.AccountId; 
                }
                
                else if(!mapUsersForFederation.isEmpty() && mapUsersForFederation.containsKey(leadRec.HDFC_DASH_Owner_Id__c))
                {
                    leadRec.OwnerId = mapUsersForFederation.get(leadRec.HDFC_DASH_Owner_Id__c).Id; 
                    leadRec.HDFC_DASH_Owner_Agency_Id__c = null; 
                    leadRec.HDFC_DASH_Owner_Agency__c = null;   
                }
                else{
                    HDFC_DASH_Error_Code__c errorCode_OwnerId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_OWNERID_EXP);
                    leadRec.adderror(errorCode_OwnerId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Owner_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Owner_Id__c);
                }
            }
        }
    }
    
    /* 
Method Name :filterPopulateLeadFields_Update
Description :This method would be called to filter the list of Leads for which have the related fields.
*/ 
    public static List<Lead> filterPopulateLeadFields_Update(){
        List<Lead> listLeads = new List<Lead>();
        List<Lead> listUpdateLeads = new List<Lead>();
        listLeads = Trigger.new;
        for(Lead leadRec:listLeads)
        {
            Lead oldLeadRec = (Lead)Trigger.oldMap?.get(leadRec.id); 
            Lead newLeadRec = (Lead)Trigger.newMap?.get(leadRec.id); 
            
            if((newLeadRec.HDFC_DASH_Source_Id__c != oldLeadRec.HDFC_DASH_Source_Id__c) || (newLeadRec.HDFC_DASH_Owner_Id__c != oldLeadRec.HDFC_DASH_Owner_Id__c) || (newLeadRec.HDFC_DASH_Source_Executive_ID__c != oldLeadRec.HDFC_DASH_Source_Executive_ID__c)) 
            {
                system.debug('in update');
                listUpdateLeads.add(leadRec);
            }  
        }
        return listUpdateLeads;
    }
    
    /* 
Method Name :populateLeadFields_Update
Description :This method would be to Update the lookup fields of the leads.
*/ 
    public static void populateLeadFields_Update(List<Lead> listLeads)
    {
        List<String> listAgencyIds = new List<String>();
        List<String> listSourceIds = new List<String>();
        List<String> listExecutiveIds = new List<String>();
        List<String> listOwnerIds = new List<String>();
        List<Contact> listCon = new List<Contact>();
        List<User> listUsers = new List<User>();
        List<Account> listAcc = new List<Account>();
        Map<String,Id> mapCon = new Map<String,Id>();
        Map<String,String> mapCondition = new Map<String,String>();
        Map<String,String> mapConditionForUsers = new Map<String,String>();
        Map<String,User> mapUsersForExecCode = new Map<String,User>();
        Map<String,User> mapUsersForFederation = new Map<String,User>();
        Map<String,Id> mapAcc = new Map<String,Id>();
        
        List<string> listConFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Executive_Code};
            List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Executive_Code,HDFC_DASH_Constants.HDFC_DASH_Federation_Id,HDFC_DASH_Constants.STRING_ACCOUNTID,HDFC_DASH_Constants.ACCOUNT_AGENCYCODE};
                List<String> listAccFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Agency_Code,HDFC_DASH_Constants.HDFC_DASH_Source_Id};
                    Map<String,Id> mapSourceAcc = new Map<String,Id>();
        
        for(Lead leadRec : listLeads)
        {
            Lead oldLeadRec = (Lead)Trigger.oldMap?.get(leadRec.id); 
            Lead newLeadRec = (Lead)Trigger.newMap?.get(leadRec.id); 
            system.debug('inside update in 1st trigger');
            //Processing Source Id fields 
            if(newLeadRec?.HDFC_DASH_Source_Id__c != oldLeadRec?.HDFC_DASH_Source_Id__c)
            {
                if(String.isNotBlank(leadRec.HDFC_DASH_Source_Id__c))
                {
                    if(leadRec.HDFC_DASH_Source_Reference__c == null || leadRec.HDFC_DASH_Source_Reference__c == HDFC_DASH_Constants.STRING_ARCHIVO)
                    {
                        if(String.isBlank(leadRec.HDFC_DASH_Source_Reference__c))
                        {
                            listAgencyIds.add(leadRec.HDFC_DASH_Source_Id__c);
                        }
                        else if(leadRec.HDFC_DASH_Source_Reference__c == HDFC_DASH_Constants.STRING_ARCHIVO){
                            listSourceIds.add(leadRec.HDFC_DASH_Source_Id__c);
                        } 
                    }
                    else
                    {
                        HDFC_DASH_Error_Code__c errorCode_SourceRef = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_SourceReference_Exp);
                        leadRec.adderror(errorCode_SourceRef.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Reference + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Reference__c);
                    }
                }
                else
                {
                    leadRec.HDFC_DASH_Source__c = null;
                }
            }
            
            //Processing Source Executive fields 
            if(newLeadRec?.HDFC_DASH_Source_Executive_ID__c != oldLeadRec?.HDFC_DASH_Source_Executive_ID__c)
            {
                if(String.isNotBlank(leadRec.HDFC_DASH_Source_Executive_Id__c))
                {
                    listExecutiveIds.add(leadRec.HDFC_DASH_Source_Executive_Id__c);
                }
                else
                {
                    leadRec.HDFC_DASH_Source_Executive__c = null;
                }
            }
            
            //Processing Owner Id fields 
            if(newLeadRec?.HDFC_DASH_Owner_Id__c != oldLeadRec?.HDFC_DASH_Owner_Id__c)
            {
                if(String.isNotBlank(leadRec.HDFC_DASH_Owner_Id__c))
                {
                    listOwnerIds.add(leadRec.HDFC_DASH_Owner_Id__c);
                }
                else
                {
                    
                    leadRec.HDFC_DASH_Owner_Agency_Id__c = null;
                    leadRec.HDFC_DASH_Owner_Agency__c = null;
                }
            }
        }
        
        if(!listAgencyIds.isEmpty()){
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Agency_Code, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAgencyIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        }
        else if(!listSourceIds.isEmpty()){
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Source_Id, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listSourceIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);       
        }
        
        if(!mapCondition.isEmpty())
        {
            listAcc = HDFC_DASH_Account_Selector.getAccountsBasedOnQuery( listAccFields, mapCondition); 
        }
        if(!listAcc.isEmpty())
        {
            for(Account accRec:listAcc)
            {
                if(String.isNotBlank(accRec.HDFC_DASH_Agency_Code__c)){
                    mapAcc.put(accRec.HDFC_DASH_Agency_Code__c,accRec.Id);
                }
                if(String.isNotBlank(accRec.HDFC_DASH_Source_Id__c)){
                    mapSourceAcc.put(accRec.HDFC_DASH_Source_Id__c,accRec.Id);
                }
            }
        }
        
        if(!listExecutiveIds.isEmpty()){
            listCon = HDFC_DASH_Contact_Selector.getContacts(listExecutiveIds, listConFields, HDFC_DASH_Constants.HDFC_DASH_Executive_Code);
        }
        if(!listCon.isEmpty()){
            for(Contact conRec:listCon){
                mapCon.put(conRec.HDFC_DASH_Executive_Code__c,conRec.Id);
            }
        }
        
        if(!listOwnerIds.isEmpty())
        {  
            mapConditionForUsers.put(HDFC_DASH_Constants.HDFC_DASH_Executive_Code, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listOwnerIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            mapConditionForUsers.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.HDFC_DASH_Federation_Id, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listOwnerIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        }
        if(!mapConditionForUsers.isEmpty())
        {
            listUsers = HDFC_DASH_User_Selector.getUserBasedOnQuery(listUserFields, mapConditionForUsers);
        }
        if(!listUsers.isEmpty())
        {
            for(User userRec:listUsers)
            {
                if(String.isNotBlank(userRec.HDFC_DASH_Executive_Code__c))
                {
                    mapUsersForExecCode.put(userRec.HDFC_DASH_Executive_Code__c,userRec);
                }
                if(String.isNotBlank(userRec.FederationIdentifier))
                {
                    mapUsersForFederation.put(userRec.FederationIdentifier,userRec);
                }
            }
        }
        
        for(Lead leadRec : listLeads)
        {
            Lead oldLeadRec = (Lead)Trigger.oldMap?.get(leadRec.id); 
            Lead newLeadRec = (Lead)Trigger.newMap?.get(leadRec.id); 
            
            //Populate Source lookup field
            if(newLeadRec.HDFC_DASH_Source_Id__c != oldLeadRec.HDFC_DASH_Source_Id__c)
            {
                if(String.isNotBlank(leadRec.HDFC_DASH_Source_Id__c))
                {
                    if(String.isBlank(leadRec.HDFC_DASH_Source_Reference__c))
                    {
                        if(!mapAcc.isEmpty() && mapAcc.containsKey(leadRec.HDFC_DASH_Source_Id__c))
                        {
                            leadRec.HDFC_DASH_Source__c = mapAcc.get(leadRec.HDFC_DASH_Source_Id__c);
                        }
                        else
                        {
                            HDFC_DASH_Error_Code__c errorCode_SourceId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_SOURCEID_EXP);
                            leadRec.adderror(errorCode_SourceId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Id__c);
                        }
                    }
                    else if(leadRec.HDFC_DASH_Source_Reference__c == HDFC_DASH_Constants.STRING_ARCHIVO)
                    {
                        if(!mapSourceAcc.isEmpty() && mapSourceAcc.containsKey(leadRec.HDFC_DASH_Source_Id__c))
                        {
                            leadRec.HDFC_DASH_Source__c = mapSourceAcc.get(leadRec.HDFC_DASH_Source_Id__c);
                        }
                        else
                        {
                            HDFC_DASH_Error_Code__c errorCode_SourceId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_SOURCEID_EXP);
                            leadRec.adderror(errorCode_SourceId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Id__c);
                        }
                    }
                }
            }
            
            //Populate Source Executive lookup field
            if(newLeadRec.HDFC_DASH_Source_Executive_ID__c != oldLeadRec.HDFC_DASH_Source_Executive_ID__c)
            {
                if(String.isNotBlank(leadRec.HDFC_DASH_Source_Executive_Id__c))
                {
                    if(!mapCon.isEmpty() && mapCon.containsKey(leadRec.HDFC_DASH_Source_Executive_Id__c)){
                        leadRec.HDFC_DASH_Source_Executive__c = mapCon.get(leadRec.HDFC_DASH_Source_Executive_Id__c);
                    }
                    else{
                        HDFC_DASH_Error_Code__c errorCode_SourceExecutiveId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_SOURCEEXECUTIVEID_EXP);
                        leadRec.adderror(errorCode_SourceExecutiveId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Source_Executive_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Source_Executive_Id__c);            
                    }
                }
            }
            
            //Populate Owner lookup field
            if(newLeadRec.HDFC_DASH_Owner_Id__c != oldLeadRec.HDFC_DASH_Owner_Id__c)
            {
                if(String.isNotBlank(leadRec.HDFC_DASH_Owner_Id__c))
                {
                    if(!mapUsersForExecCode.isEmpty() && mapUsersForExecCode.containsKey(leadRec.HDFC_DASH_Owner_Id__c)){
                        leadRec.OwnerId = mapUsersForExecCode.get(leadRec.HDFC_DASH_Owner_Id__c).Id; 
                        leadRec.HDFC_DASH_Owner_Agency_Id__c = mapUsersForExecCode.get(leadRec.HDFC_DASH_Owner_Id__c)?.account?.HDFC_DASH_Agency_Code__c; 
                        leadRec.HDFC_DASH_Owner_Agency__c = mapUsersForExecCode.get(leadRec.HDFC_DASH_Owner_Id__c)?.AccountId; 
                    }
                    else if(!mapUsersForFederation.isEmpty() && mapUsersForFederation.containsKey(leadRec.HDFC_DASH_Owner_Id__c))
                    {
                        leadRec.OwnerId = mapUsersForFederation.get(leadRec.HDFC_DASH_Owner_Id__c).Id; 
                        leadRec.HDFC_DASH_Owner_Agency_Id__c = null; 
                        leadRec.HDFC_DASH_Owner_Agency__c = null; 
                    }
                    else{
                        HDFC_DASH_Error_Code__c errorCode_OwnerId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_OWNERID_EXP);
                        leadRec.adderror(errorCode_OwnerId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Owner_Id + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Owner_Id__c);
                    }
                }
            }     
        }
    }
    
    /* 
Method Name :filterLeadOwnerAgencyFields
Description :This method would be called to filter the list of Leads for which have the related fields.
*/ 
    public static List<Lead> filterLeadOwnerAgencyFields(){
        List<Lead> listLeads = new List<Lead>();
        List<Lead> listUpdateLeads = new List<Lead>();
        listLeads = Trigger.new;  
        for(Lead leadRec:listLeads)
        {
            if(Trigger.isInsert)
            {
                if((String.isNotBlank(leadRec.HDFC_DASH_Owner_Agency_Id__c) && String.isBlank(leadRec.HDFC_DASH_Owner_Agency__c) && String.isBlank(leadRec.HDFC_DASH_Owner_Id__c)))
                {
                    listUpdateLeads.add(leadRec);
                }
            }
            else if(Trigger.isUpdate)
            {
                Lead oldLeadRec = (Lead)Trigger.oldMap.get(leadRec.id); 
                Lead newLeadRec = (Lead)Trigger.newMap.get(leadRec.id); 
                
                if((newLeadRec?.HDFC_DASH_Owner_Agency_Id__c != oldLeadRec?.HDFC_DASH_Owner_Agency_Id__c) && (newLeadRec?.HDFC_DASH_Owner_Id__c == oldLeadRec?.HDFC_DASH_Owner_Id__c))
                {
                    system.debug('in update');
                    listUpdateLeads.add(leadRec);
                }
            }
        }
        return listUpdateLeads;
    } 
    
    /* 
Method Name :updateLeadOwnerAgencyFields
Description :This method would be to Update the lookup fields of the leads.
*/ 
    public static void updateLeadOwnerAgencyFields(List<Lead> listLeads)
    {
        List<String> listOwnerAgency = new List<String>();
        List<Account> listAcc = new List<Account>();
        List<String> listAccFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Agency_Code,HDFC_DASH_Constants.HDFC_DASH_Source_Id};
            Map<String,Id> mapAcc = new Map<String,Id>();  
        
        for(Lead leadRec : listLeads)
        {
            if(Trigger.isInsert)
            {
                system.debug('Inside insert owner agency');
                if(String.isNotBlank(leadRec.HDFC_DASH_Owner_Agency_Id__c))
                {
                    listOwnerAgency.add(leadRec.HDFC_DASH_Owner_Agency_Id__c);
                }
            }
            else if(Trigger.isUpdate)
            {
                Lead oldLeadRec = (Lead)Trigger.oldMap?.get(leadRec.id); 
                Lead newLeadRec = (Lead)Trigger.newMap?.get(leadRec.id); 
                system.debug('Inside update owner agency');
                if(String.isBlank(leadRec.HDFC_DASH_Owner_Agency_Id__c))
                {  
                    leadRec.HDFC_DASH_Owner_Agency__c = null;
                }
                else
                {
                    listOwnerAgency.add(leadRec.HDFC_DASH_Owner_Agency_Id__c);
                }
            }
        }
        
        if(!listOwnerAgency.isEmpty())
        {
            listAcc = HDFC_DASH_Account_Selector.getAccounts(listOwnerAgency, listAccFields, HDFC_DASH_Constants.HDFC_DASH_Agency_Code); 
        }
        if(!listAcc.isEmpty())
        {
            for(Account accRec:listAcc)
            {
                if(String.isNotBlank(accRec.HDFC_DASH_Agency_Code__c)){
                    mapAcc.put(accRec.HDFC_DASH_Agency_Code__c,accRec.Id);
                }
            }
        }
        for(Lead leadRec : listLeads)
        {
            //Populate Owner Agency lookup field
            if(String.isNotBlank(leadRec.HDFC_DASH_Owner_Agency_Id__c))
            {
                if(!mapAcc.isEmpty() && mapAcc.containsKey(leadRec.HDFC_DASH_Owner_Agency_Id__c))
                {
                    leadRec.HDFC_DASH_Owner_Agency__c = mapAcc.get(leadRec.HDFC_DASH_Owner_Agency_Id__c);
                }                
                else
                {
                    //Update the error code
                    HDFC_DASH_Error_Code__c errorCode_OwnerAgencyId = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_OWNERAGENCYID_Exp);
                    leadRec.adderror(errorCode_OwnerAgencyId.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Owner_Agency_Id  + HDFC_DASH_Constants.COLON +leadRec.HDFC_DASH_Owner_Agency_Id__c);
                }
            }
        }
    }
}