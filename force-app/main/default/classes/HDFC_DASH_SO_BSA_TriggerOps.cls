/**
className: HDFC_DASH_SO_BSA_TriggerOps
DevelopedBy: Tejeswari
Date: 19 November 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on HDFC_DASH_SO_BSA_Association__c object.
**/
public class HDFC_DASH_SO_BSA_TriggerOps {
 /**
className: populateSoBsaFields
DevelopedBy: Tejeswari
Date: 19 November 2021
Company: Accenture 
Class Description: This Class update the lookup fields of HDFC_DASH_SO_BSA_Association__c.
*/
  public with sharing class populateSoBsaFields implements HDFC_DASH_TriggerOps{
/*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
          return HDFC_DASH_TriggerOpsRecursionFlags.SO_BSA_Api_FirstRun;
        }
/*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<HDFC_DASH_SO_BSA_Association__c> listSoBsa = HDFC_DASH_SO_BSA_TriggerOpsHelper.filterSoBsaWithAgencyAndFederation();
           	return listSoBsa;
        }
/*      
Method Name :execute
Description :This method would execute the logic.
*/
    public void execute(SObject[] listSoBsa){
            HDFC_DASH_SO_BSA_TriggerOpsHelper.updateSoBsaFields(listSoBsa); 
         	HDFC_DASH_TriggerOpsRecursionFlags.SO_BSA_Api_FirstRun = false;
         
        }  
    }
}