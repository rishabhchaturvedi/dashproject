/**
className: HDFC_DASH_InboundRestService_CoApplicantTest
DevelopedBy: Punam Marbate
Date: 14 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestService_CoApplicant
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_InboundRest_CoApplicantTest {

    private static final string SUCCESS ='Success';
    private static final string STATUS ='Complete';
    private static final string CIBILID = '100032503832';
    private static final Date DATE_OF_BIRTH =Date.valueOf('1998-08-24');
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                                                            HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    
    private static Interface_Framework_Settings__c ifSetting = new Interface_Framework_Settings__c();
    private static Interface_Settings__c interfaceSetting = new Interface_Settings__c();
    private static final string WORKEMAIL = 'abdcoapp@xyz.com';
    private static final string requestBody ='{"coApplicant":{"applicantType":"HDFC_DASH_Co_Applicant","sfApplicationId":"'+ app.id +'","sfApplicationNumber":"10000063","leadStageNumber":"6","leadStageDescription":""," experianCibilID":"12890","personalDetails":{"outSystemsID":"12345","salutation":"MR","firstName":"SHIVAM","middleName":"","lastName":"MITTAL","pan":"BNIPA8045N","panStatus":"ExistingandValid.AadhaarSeedingisSuccessful.","panApplied":"No","dateOfBirth":"2001-10-30","cityCode":"HARAPANAHALLI","stateCode":"KA","countryCode":"INDIA","cityName":"","stateName":"","countryName":"","residentType":"Res"},"riskScore":{"riskScoreCustomerstatus":"EXISTING","riskScoreCustomerflags":"","riskScoreRtrexists":"N","riskScoreCustomergrade":"","riskScoreRiskevent":"","riskScoreAvailedMoratorium":"N","riskScoreAvailedRestruct":"N","riskScoreHdfcStaff":"","riskScoreConfidenceScore":"","riskScoreExistInNegList":"N"},"skipEmploymentDetails":"No","employmentDetails":{"occupation":"E","workEmail":"'+ WORKEMAIL +'","employerCode":"1076600","employerName":"ZSASSOCIATESINDIAPRIVATELIMITED"},"financialInfo":{"fixedMonthlyIncome":"10001.00","additionalIncome":"10001.00","noAdditionalIncome":"Yes","salarySlipAttached":"Yes"}},"validationOutcome":[{"user_Input_Type":"A1 PAN","user_Input_Value":"ASD123DSedf","validated_With":"NSDL","validation_Response":{}}]}';
    
    /* 
    Method Name : testProcessCoApplicantDetails
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_CoApplicant.processCoApplicantDetails().
    */
    static testmethod void testProcessCoApplicantDetails(){   
      
        System.runAs(sysAdmin){
            
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            Test.startTest();
            HDFC_DASH_APIResponse_CoApplicant coApplicantResponse =  
                HDFC_DASH_InboundRestService_CoApplicant.processCoApplicantDetails(request);
            Test.stopTest();
            System.assertNotEquals(null,coApplicantResponse);
        } 
    }
    /* 
    Method Name : testRunSalesforceDedupe
    Parameters  : No 
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_CoApplicant.runSalesforceDedupe().
    */
    static testmethod void testRunSalesforceDedupe(){
        
        System.runAs(sysAdmin){    
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            Contact conRec = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
            conRec.HDFC_DASH_Date_Of_Birth__c = DATE_OF_BIRTH;
            conRec.FirstName ='SHIVAM'; 
            conRec.LastName = 'MITTAL';   
            conRec.HDFC_DASH_PAN__c = 'BNIPA8045N';
            upsert conRec;
            HDFC_DASH_ParseCoApplicantDetails coApplicantDetails = HDFC_DASH_ParseCoApplicantDetails.parse(request);
            Contact result = new Contact();
            Test.startTest();
            result = HDFC_DASH_InboundRestService_CoApplicant.runSalesforceDedupe(coApplicantDetails);           
            Test.stopTest();
            system.assertNotEquals(null, result);
        }
    }   
    /* 
    Method Name : testRunSalesforceDedupe
    Parameters  : No
    Description : This method is for success scenario which call  HDFC_DASH_InboundRestService_CoApplicant.insertCoApplicantDetails().
    */
    static testmethod void testInsertCoApplicantDetails(){
        
      
        System.runAs(sysAdmin){
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
            HDFC_DASH_ParseCoApplicantDetails coapplicant =  HDFC_DASH_ParseCoApplicantDetails.parse(request);
            Id result = null;
            Test.startTest();
            result = HDFC_DASH_InboundRestService_CoApplicant.insertCoApplicantDetails(coapplicant,con);
            Test.stopTest();
            system.assertNotEquals(null,result);        
        }      
    } 
    /* 
    Method Name :testInsertCoApplicantDetailsConpassingDedupeCheckIsNull
    Parameters  :NO
    Description :This method is for success scenario which call HDFC_DASH_InboundRestService_CoApplicant.insertCoApplicantDetails() when ConpassingDedupeCheck is Null.
    */
    static testmethod void testInsertCoApplicantDetailsConpassingDedupeCheckIsNull(){
        
      
        System.runAs(sysAdmin){
            RestRequest request = new RestRequest();    
            request.requestBody = Blob.valueOf(requestBody);
            HDFC_DASH_ParseCoApplicantDetails coapplicant =  HDFC_DASH_ParseCoApplicantDetails.parse(request);
            Id result=null;
            Test.startTest();
            result = HDFC_DASH_InboundRestService_CoApplicant.insertCoApplicantDetails(coapplicant,con);
            Test.stopTest();
            system.assertNotEquals(null,result);        
        }     
    } 
    /* 
    Method Name :testInsertEmployementDetailsRec
    Parameters  :
    Description :This method is for success scenario which call  HDFC_DASH_InboundRestService_CoApplicant.insertEmployementDetailsRec().
    */
    static testmethod void testInsertEmployementDetailsRec(){
        
        
        System.runAs(sysAdmin){
            RestRequest request = new RestRequest();        
            request.requestBody = Blob.valueOf(requestBody);
            HDFC_DASH_ParseCoApplicantDetails coApplicantDetails = HDFC_DASH_ParseCoApplicantDetails.parse(request);
            Test.startTest();
            HDFC_DASH_InboundRestService_CoApplicant.insertEmployementDetailsRec(appDetails.Id , coApplicantDetails);
        }
        Test.stopTest();
        system.assertEquals(1, [select count() from HDFC_DASH_Applicant_Employment_Details__c 
                        where HDFC_DASH_Work_Email__c =:WORKEMAIL], SUCCESS);      
    }
    
}