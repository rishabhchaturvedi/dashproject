/* 
ClassName: HDFC_DASH_TestDataFactory_RuleEngine
DevelopedBy: Kumar Gourav
Date: 4 Dec 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Rule Engine Capture object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_RuleEngine {
    public static final string RULEID = '12';
    public static final string STAGE = 'PRE_FEE';
    public static final Boolean DOCRULEENGOUTCOME = true;
    public static final Boolean CONFIDENCERULEENGOUTCOME = false;
    public static final Boolean CUSTOMERRESOLVED = true;
    public static final string CUSTOMERACTION = 'Data Updated';
    public static final Boolean CUSTOMERSKIPPED = false;
    public static final string DOCCODE = 'SALSLP';
    public static final string DOCNAME = 'SALARY SLIP';
    public static final string SUBDETAILS = 'MAY-21';
    
        
    /* Method Name: createBasicRuleEngineCapture
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Rule_Engine_Capture__c.
    Pass fields of HDFC_DASH_Rule_Engine_Capture__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Rule_Engine_Capture__c or inserted HDFC_DASH_Rule_Engine_Capture__c id. 
    */
    public static HDFC_DASH_Rule_Engine_Capture__c createBasicRuleEngineCapture(ID appID,ID applId)
    {
        HDFC_DASH_Rule_Engine_Capture__c ruleEng = new HDFC_DASH_Rule_Engine_Capture__c();
            
       // doc.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        ruleEng.HDFC_DASH_Application__c = appID;     
        ruleEng.HDFC_DASH_Applicant_Detail__c = applId;
        ruleEng.HDFC_DASH_Rule_Id__c= RULEID;
        ruleEng.HDFC_DASH_Stage__c =STAGE;
        ruleEng.HDFC_DASH_Doc_Rule_Engine_Outcome__c =DOCRULEENGOUTCOME;
        ruleEng.HDFC_DASH_Confidence_Rule_Engine_Outcome__c =CONFIDENCERULEENGOUTCOME;
        ruleEng.HDFC_DASH_Customer_Resolved__c =CUSTOMERRESOLVED;
        ruleEng.HDFC_DASH_Customer_Action__c =CUSTOMERACTION;
        ruleEng.HDFC_DASH_Customer_Skipped__c =CUSTOMERSKIPPED;
        ruleEng.HDFC_DASH_Doc_Code__c =DOCCODE;
        ruleEng.HDFC_DASH_Doc_Name__c =DOCNAME;
        ruleEng.HDFC_DASH_Sub_Details__c =SUBDETAILS;
        return ruleEng;        
    }
    
    /* Method Name: getBasicRuleEngineCapture
    parameters: NONE
    Method Description: This method is to generate test data for RuleEngineCapture.
    */
    public static HDFC_DASH_Rule_Engine_Capture__c getBasicRuleEngineCapture(ID appID,ID applId)
    {
        HDFC_DASH_Rule_Engine_Capture__c ruleEng = HDFC_DASH_TestDataFactory_RuleEngine.createBasicRuleEngineCapture(appID,applId);
        if(Schema.sObjectType.HDFC_DASH_Rule_Engine_Capture__c.isCreateable()){
            Database.insert(ruleEng);
        }
        return ruleEng;
    }
       
}