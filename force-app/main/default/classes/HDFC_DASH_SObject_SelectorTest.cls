/**
className: HDFC_DASH_SObject_SelectorTest
DevelopedBy: Tejeswari
Date: 26 November 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_SObject_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_SObject_SelectorTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    /* 
Method Name :testGetSObjects
Description :This method will test the method getSObjects().
*/
    static testmethod void testGetSObjects(){
        List<sObject> sobjectRec = new List<sObject>(); 
         System.runAs(sysAdmin){
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING};
            Test.startTest();   
        sobjectRec = HDFC_DASH_SObject_Selector.getSObjects(new List<String>{appDetails.Id}, fieldList,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, HDFC_DASH_Constants.ID_STRING);       
        Test.stopTest();
        system.assertNotEquals( null, sobjectRec);
        system.assertEquals(1,sobjectRec.size());  
         }
    }
}