/*
    Author: Deepali Gupta
    Class: HDFC_DASH_Metadata_Selector
    Description: Apex Class for metadata selector
*/
public with sharing class HDFC_DASH_Metadata_Selector {

    private static final string SELECT_STRING = 'SELECT ';
    private static final string FROM_METADATANAME = ' FROM  HDFC_DASH_Email_To_Case_Field_Population__mdt ';
    private static final string FROM_METADATANAME2 = 'FROM Intent_Type_Combinations__mdt';
    private static final string FROM_METADATANAME_Escalation = ' FROM Case_Default_Owner__mdt ';
    private static final string FROM_CaseType_Alerts = ' FROM Emails_to_Case_Type__mdt ';
    private static final string WHERE_STRING = 'WHERE ';
    private static final string QUERY_PARAMETER = ' = :queryParameter';
    private static final string CASE_TYPE =' Case_Type__c =\'';
    private static final string Mode =' Mode__c=\'';
    private static final string SubMode =' Sub_Mode__c =\'';
    private static final string Category =' Category__c =\'';
    private static final string SubCategory =' Sub_Category__c =\'';
    private static final string And_STRING = '\' AND ';
    private static final string And_STRIN = ' AND ';
    private static final string End_STRING = '\'';
    private static final string EndLikeCondition = '%\'';
	private static final string status = 'Status__c=\'';
    private static final string status1 =  'Status__c Like \'%';
	private static final string CaseType = 'Case_Type__c Like \'%';
    private static final string CaseOrigins = ' Case_Origin__c Like \'%';
    
    /* 
    Method Name :getMetadataRecsBasedOnString      
    Parameters  :List<string> fieldList,String queryParameter,String conditionField
    Description :This method would get the List of Email Metadata recs based on single string condition sent to it.
    */
    public static List<HDFC_DASH_Email_To_Case_Field_Population__mdt> 
        getEmailMetadataRecsBasedOnString(List<string> fieldList,String queryParameter,String conditionField){
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
                             +FROM_METADATANAME + WHERE_STRING
                             + conditionField +QUERY_PARAMETER;
        //system.debug('querystring1 : ' + querystring);
        List<HDFC_DASH_Email_To_Case_Field_Population__mdt>emMetRecsList= database.query(querystring);  
        return emMetRecsList;
    }
    
    //Intent Type Combinations
  /* public static List<Intent_Type_Combinations__mdt> 
        getIntentMetadataRecs(List<string> fieldList){
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + fieldList
                             +FROM_METADATANAME2;
        system.debug('querystring : ' + querystring);
        List<Intent_Type_Combinations__mdt>emMetRecsList= database.query(querystring);  
        return emMetRecsList;
    } */
    
    /* 
    Method Name :getEscalationMtdRecsBasedOnString
    Parameters  :List<string> fieldList,String queryParameter,String conditionField
    Description :This method would get the List of Email Metadata recs based on single string condition sent to it.
    */
    public static List<Case_Default_Owner__mdt> getEscalationMtdRecs(List<string> fieldList,Case c){
        //string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
                            // +FROM_METADATANAME_Escalation + WHERE_STRING
                          //  + CASE_TYPE + c.HDFC_DASH_Case_Type__c+ And_STRING + Mode + c.HDFC_DASH_Mode__c 
                           //+ And_STRING + SubMode + c.HDFC_DASH_Sub_mode__c + And_STRING + Category + c.HDFC_DASH_Category__c
                            //+ And_STRING +  SubCategory + c.HDFC_DASH_Sub_Category__c+End_STRING;
        //querystring = querystring.replace('\'null\'', 'null');
        //system.debug('querystring1 : ' + querystring);
        //List<Case_Default_Owner__mdt>cDOMetRecsList= database.query(querystring);  
        //system.debug('querystring1 : ' + cDOMetRecsList.ToString());
        
        //system.debug('Owner: '+c.Owner.name);
        //system.debug('CaseNumber2 -'+c.casenumber);
        List<Case_Default_Owner__mdt>cDOMetRecsList = null;
        if(c.HDFC_DASH_Case_Type__c=='Complaint'){
            if(c.HDFC_DASH_Mode__c =='Email' && c.HDFC_DASH_Sub_Mode__c=='Email' && c.HDFC_DASH_Category__c=='' && C.HDFC_DASH_Sub_Category__c=='')
            {
            cDOMetRecsList = [Select AssignTo__c,Escalation_Type__c,Escalate_L1__c,Escalate_L2__c,Case_Type__c,Mode__c,Sub_Mode__c,Category__c,Sub_Category__c,Type__c FROM Case_Default_Owner__mdt WHERE  Case_Type__c =:c.HDFC_DASH_Case_Type__c  AND  Category__c =:c.HDFC_DASH_Category__c AND  Sub_Category__c =:c.HDFC_DASH_Sub_Category__c ];
              }
            else
            {
                cDOMetRecsList = [Select AssignTo__c,Escalation_Type__c,Escalate_L1__c,Escalate_L2__c,Case_Type__c,Mode__c,Sub_Mode__c,Category__c,Sub_Category__c,Type__c FROM Case_Default_Owner__mdt WHERE  Case_Type__c =:c.HDFC_DASH_Case_Type__c  AND Mode__c=:c.HDFC_DASH_Mode__c AND Sub_Mode__c=:c.HDFC_DASH_Sub_mode__c  AND  Category__c =:c.HDFC_DASH_Category__c AND  Sub_Category__c =:c.HDFC_DASH_Sub_Category__c ]; 
            }
        
            } else if (c.HDFC_DASH_Case_Type__c=='Request' || c.HDFC_DASH_Case_Type__c=='Query'){
        cDOMetRecsList = [Select AssignTo__c,Escalation_Type__c,Escalate_L1__c,Escalate_L2__c,Case_Type__c,Mode__c,Sub_Mode__c,Category__c,Sub_Category__c,Type__c FROM Case_Default_Owner__mdt WHERE  Case_Type__c =:c.HDFC_DASH_Case_Type__c  AND  Category__c =:c.HDFC_DASH_Category__c AND  Sub_Category__c =:c.HDFC_DASH_Sub_Category__c ];
  
        }
        system.debug('cDOMetRecsList: '+cDOMetRecsList);
        return cDOMetRecsList;
    }
       /* 
    Method Name :getMetadataBasedOnCaseType
    Parameters  :List<string> fieldList,String queryParameter,String conditionField
    Description :
    */  //LikeCondition   EndLikeCondition 
       public static List<Emails_to_Case_Type__mdt> getMetadataBasedOnCaseType(List<string> fieldList,Case c){
        
        List<Emails_to_Case_Type__mdt> emailMetRecsList= new List<Emails_to_Case_Type__mdt>();
        List<Emails_to_Case_Type__mdt> finalresult= new List<Emails_to_Case_Type__mdt>();
        List<Emails_to_Case_Type__mdt> result2= new List <Emails_to_Case_Type__mdt>();
           
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
            				 +FROM_CaseType_Alerts + WHERE_STRING + CaseType+c.HDFC_DASH_Case_Type__c+ EndLikeCondition +And_STRIN+status1+c.Status+EndLikeCondition+And_STRIN+CaseOrigins+c.Origin+EndLikeCondition;
            system.debug('querystring'+querystring);
         emailMetRecsList= database.query(querystring);
          
          /*  for(Emails_to_Case_Type__mdt ets : emailMetRecsList){
               
                if(ets.Category__c !='' && ets.Category__c==c.HDFC_DASH_Category__c){
                    finalresult.addAll(emailMetRecsList);
                }else if(ets.Category__c==''){
                    finalresult.addAll(emailMetRecsList);
                }
           }*/
           
           if(emailMetRecsList.size()>0){
           finalresult.addAll(emailMetRecsList);
            }
           
        return finalresult;
        }
    
}