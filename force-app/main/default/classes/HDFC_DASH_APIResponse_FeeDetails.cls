/**
className: HDFC_DASH_ParseLeadDetails
DevelopedBy: Sai Shruthi
Date: 19 July 2021
Company: Accenture
Class Description: This is a response wrapper class for Feedetails API.
**/
public class HDFC_DASH_APIResponse_FeeDetails {
    public String successMessage;
    public String sfApplicationId;
    public String sfApplicationNumber;
    public String fileNo;
    public List<CustomerDetails> customerDetails;
    /*
Class: CustomerDetails
Description: Class to get CustomerDetails
*/
    public without sharing class CustomerDetails {
        public String sfCustNo;
        public String custNo;
        public String unqCustNo;        
    }
}