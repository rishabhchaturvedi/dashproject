/* Class: HDFC_DASH_APIRequest_LMS
   Author: Sai Shruthi Akkireddy
   Description: Class for LMS Request structure
*/
public inherited sharing class HDFC_DASH_APIRequest_LMS { 
    public String ref_id;
	public String ref_type;
	public String lead_title;
	public String first_name;
	public String middle_name;
	public String last_name;
	public String isd_code;
	public Long mobile_no;
	public String email_id;
	public String dob;
	public String pan_no;
	public String resident_type;
	public String employement_type;
	public String employer_name;
	public String customer_income;
	public String city_name;
	public String country_name;
	public String lead_source;
	public String lead_sub_source;
	public String lead_term_Source;
	public String campaign_code;
	public String loan_amount;
	public String loan_product;
	public String source_agency_cd;
	public String source_agency_executive;
	public String voice_consent_flag;
	public String Voice_consent_dt;
	public String sms_consent_flag;
	public String sms_consent_dt;
	public String whatsapp_consent_flag;
	public String whatsapp_consent_dt;
	public String consent_content;
	public String remarks;
	public Appln_form appln_form;

	/* appln_form request node */
	public class appln_form {
	}
}