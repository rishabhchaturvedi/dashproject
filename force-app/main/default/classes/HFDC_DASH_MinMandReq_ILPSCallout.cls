/*
Class:HFDC_DASH_MinMandReq_ILPSCallout
Author: Tejeswari
Description: This class will generate the request body for MinMand Callout to ILPS 
*/
public without sharing class HFDC_DASH_MinMandReq_ILPSCallout {
     /*
Class: ApplicantParams
Description: Class to parse Applicant Params 
*/
public  without sharing class ApplicantParams {
		public String sfCustomerNumber;
		public String capacity;
		public String custTitle;
		public String firstName;
    	public String middleName;
    	public String lastName;
		public String nriFlag;
		public String panNo;
		public String employNat;
		public String incomeCons;
    	public String addressChanged;
		public String employmentChanged;
    	public String visaType;
		
	}

	public ApplicationParams applicationParams;
 /*
Class: ApplicationParams
Description: Class to parse Application Params 
*/
	public without sharing class ApplicationParams {
		public String sfApplicationNumber;
		public String addlProdType;
		public String originBranch;
		public String originMode;
        public String isSimultaneousLoan;
        public String isPrimeFlag;
		public String requiredProduct;
		public String refinanceFlag;
		public String fastTrack;
		public String empClass;
		public String agencyCode;
        public List<ApplicantParams> applicantParams;
	}

}