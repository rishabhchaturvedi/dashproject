/*
className: HDFC_DASH_OrgWideEmailAddress_Selector
DevelopedBy: Sai Suman
Date: 22 December 2021
Company: Accenture
Class Description: This class would create the select queries for OrgWideEmailAddress object.
*/
public class HDFC_DASH_OrgWideEmailAddress_Selector {
    
     private static final string FROM_ORGWIDEADDRESS = ' FROM  OrgWideEmailAddress ';

/* 
Method Name :getOrgWideEmailAddress
Parameters  :List<string> fieldList,String queryParameter,String conditionField
Description :This method would get the List of OrgWideEmailAddress based on single string condition sent to it.
*/
    public static List<OrgWideEmailAddress> getOrgWideEmailAddress(List<string> fieldList){
		string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_ORGWIDEADDRESS;
        List<OrgWideEmailAddress>orgWideEmailAddress= database.query(querystring);  
        return orgWideEmailAddress;
    }
}