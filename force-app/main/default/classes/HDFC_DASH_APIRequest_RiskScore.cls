/*
Class: HDFC_DASH_APIRequest_RiskScore
Author: Punam Marbate
Date: 5 Aug 2021
Company: Accenture
Description:This class will define the request body of Risk Score callout 
*/
public with sharing class HDFC_DASH_APIRequest_RiskScore {

    public String sfApplId;
    public String sfCustNo;
    public String custNo;
    public String unqCustNo;
}