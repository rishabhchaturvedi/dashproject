/**
className: HDFC_DASH_InboundRestSer_GetLeapDash
DevelopedBy:  Tejeswari 
Date: 8 December 2021
Company: Accenture
Class Description: Service Class for getLeapDashboard API
**/
public with sharing class HDFC_DASH_InboundRestSer_GetLeapDash 
{
    public static String param_lrNumber, param_FileNumber, param_FromDate, param_ToDate, param_Status;
    public static String param_Branch, param_ServiceCenter, param_Agency, param_SalesExecutive, param_DocReceived;
    public static String param_leapFlag, param_QuickLoginSource, param_SpotOfferRequest, param_EmploymentType;
    public static String param_DigitalVerification, param_Product, param_Campaign, param_ExistingCustomer;
    public static String param_OnlineStage, param_FileCompleteness, param_DSF;
    public static List<String> listLrOrFileNoWithCustomer = new List<String>();
    public static List<String> listLrOrFileNo = new List<String>();
    
    /* 
Method:getLeapDashboardDetails
Description: Method to get a list of Applications.
*/
    public static HDFC_DASH_APIResponse_GetleapDashboard getLeapDashboardDetails(RestRequest request)
    {   
        HDFC_DASH_APIResponse_GetleapDashboard apiRes = getLeapDashboardResponse(request);
        return apiRes;  
    }
    /* 
Method: getLeapDashboardResponse
Description: Method to get the list of Applications.
*/
    public static HDFC_DASH_APIResponse_GetleapDashboard getLeapDashboardResponse(RestRequest request)
    {     
        HDFC_DASH_APIResponse_GetleapDashboard apiRes = new HDFC_DASH_APIResponse_GetleapDashboard();
        List<Opportunity> listApp = new List<Opportunity>();
        List<String> listAppId = new List<String>();
        String flag_LrOrFileNo;
        List<HDFC_DASH_Applicant_Details__c> applDetailsList = new List<HDFC_DASH_Applicant_Details__c>();
        List<HDFC_DASH_APIResponse_GetleapDashboard.leapdashboard> sfLeapDashboard = new  List<HDFC_DASH_APIResponse_GetleapDashboard.leapdashboard>();
        List<String> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_File_Number, HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID,
            HDFC_DASH_Constants.HDFC_DASH_Leap_LR_Number,HDFC_DASH_Constants.STRING_name,HDFC_DASH_Constants.CREATEDDATE,HDFC_DASH_Constants.HDFC_DASH_Status,
            HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id,HDFC_DASH_Constants.HDFC_DASH_Preferred_Branch_Code,
            HDFC_DASH_Constants.OWNER_EXECUTIVE_CODE,HDFC_DASH_Constants.HDFC_DASH_Spot_Offer_Started,HDFC_DASH_Constants.HDFC_DASH_Emp_Class,HDFC_DASH_Constants.LOAN_TYPE_CODE,HDFC_DASH_Constants.HDFC_DASH_LMS_Agency_Code,
            HDFC_DASH_Constants.HDFC_DASH_Campaign_Code,HDFC_DASH_Constants.HDFC_DASH_DSF_Flag,HDFC_DASH_Constants.HDFC_DASH_File_First_Push_To_ILPS,HDFC_DASH_Constants.SO_REQUIRED_LOAN_AMOUNT,HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber,
            HDFC_DASH_Constants.Stage,HDFC_DASH_Constants.HDFC_DASH_Leap_LR_Date,HDFC_DASH_Constants.HDFC_DASH_LMSLeadSource,HDFC_DASH_Constants.HDFC_DASH_Leap_Flag,HDFC_DASH_Constants.HDFC_DASH_DSF_Executive,HDFC_DASH_Constants.HDFC_DASH_File_Completeness_Flag};  
                List<String> fieldsListapplDetail = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_name,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.USER_CONSENT_FOR_CIBIL,HDFC_DASH_Constants.HDFC_DASH_APPLICATION_FILENUMBER,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.HDFC_DASH_APPLICATION_LRNUMBER,HDFC_DASH_Constants.APPDETAIL_CONTACT_R_IS_CUSTOMER};
                    list<string> listOfFields_InfoVal = new list<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, HDFC_DASH_Constants.DOCTYPE, HDFC_DASH_Constants.HDFC_DASH_Doc_Type_Code, HDFC_DASH_Constants.FILENO,
                        HDFC_DASH_Constants.MONTHOFPAYSLIP,HDFC_DASH_Constants.HDFC_DASH_Salary_Year, HDFC_DASH_Constants.MONTHOFGSTRETURN, HDFC_DASH_Constants.ASSESSMENTYEAR, HDFC_DASH_Constants.FINANCIALYEAR, HDFC_DASH_Constants.ACCOUNTTYPE, HDFC_DASH_Constants.HDFC_DASH_Account_Number, HDFC_DASH_Constants.FROMDATE,
                        HDFC_DASH_Constants.TODATE, HDFC_DASH_Constants.BANKCODE, HDFC_DASH_Constants.HDFC_DASH_Employer_Name, HDFC_DASH_Constants.TOTALAMOUNTPAID, HDFC_DASH_Constants.NOOFQUARTERSPAID, HDFC_DASH_Constants.PROOFOFCOMMUNICATION, HDFC_DASH_Constants.KYCDOCTYPE, HDFC_DASH_Constants.HDFC_DASH_KYC_Doc_Code,
                        HDFC_DASH_Constants.BCPDOCTYPE,HDFC_DASH_Constants.HDFC_DASH_BCP_Doc_Code, HDFC_DASH_Constants.DOCEXTRACTABLE, HDFC_DASH_Constants.DOCSCANNEDBY, HDFC_DASH_Constants.DOCIMAGEORIGIN, HDFC_DASH_Constants.HDFC_DASH_Location_Latitude, HDFC_DASH_Constants.HDFC_DASH_Location_Longitude, HDFC_DASH_Constants.STORAGE_IDENTIFIER,
                        HDFC_DASH_Constants.STORAGE_TYPE, HDFC_DASH_Constants.APPDETAILS_R_APPLICATION, HDFC_DASH_Constants.APPDETAILS_R_APPLICANTCAPACITY, HDFC_DASH_Constants.APPDETAILS_R_CONTACT,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.HDFC_DASH_ILPS_Cust_Number,HDFC_DASH_Constants.HDFC_DASH_DOCUMENT_RECORD_SERIAL_No,
                        HDFC_DASH_Constants.HDFC_DASH_Customer_Number_Formula,HDFC_DASH_Constants.HDFC_DASH_Info_Validation_Record_Type,HDFC_DASH_Constants.HDFC_DASH_SF_Triggered};     
                            Map<string,string> mapCondition = new Map<string,string>();
        Map<string,string> mapApplDetCondition = new Map<string,string>();
        Map<string,string> mapApplDet = new Map<string,string>();
        Map<string,HDFC_DASH_Applicant_Details__c> mapLrOrFileNo = new Map<string,HDFC_DASH_Applicant_Details__c>();
        Map<string,string> mapInfoVal = new Map<string,string>();
        Map<string,List<HDFC_DASH_Info_Validation__c>> mapAppl_InfoVal = new Map<string,List<HDFC_DASH_Info_Validation__c>>();
        
        param_lrNumber = RestContext.request.params.get(HDFC_DASH_Constants.STRING_LRNUMBER);
        param_FileNumber = RestContext.request.params.get(HDFC_DASH_Constants.STRING_FILENUMBER);
        param_FromDate = RestContext.request.params.get(HDFC_DASH_Constants.STRING_FROMDATE);
        param_ToDate = RestContext.request.params.get(HDFC_DASH_Constants.STRING_TODATE);
        param_Status = RestContext.request.params.get(HDFC_DASH_Constants.STRING_PARAM_STATUS);
        param_Branch = RestContext.request.params.get(HDFC_DASH_Constants.STRING_BRANCH);
        param_ServiceCenter= RestContext.request.params.get(HDFC_DASH_Constants.STRING_SERVICECENTER);
        param_Agency = RestContext.request.params.get(HDFC_DASH_Constants.STRING_AGENCY);
        param_SalesExecutive= RestContext.request.params.get(HDFC_DASH_Constants.STRING_SALESEXECUTIVE);
        param_DocReceived= RestContext.request.params.get(HDFC_DASH_Constants.STRING_DOC_RECEIVED);
        param_leapFlag = RestContext.request.params.get(HDFC_DASH_Constants.STRING_LEAP_FLAG);
        param_QuickLoginSource = RestContext.request.params.get(HDFC_DASH_Constants.STRING_QUICK_LOGIN_SOURCE);
        param_SpotOfferRequest = RestContext.request.params.get(HDFC_DASH_Constants.STRING_SPOT_OFFER_REQ);
        param_EmploymentType = RestContext.request.params.get(HDFC_DASH_Constants.STRING_EMPLOYMENT_TYPE);
        param_DigitalVerification = RestContext.request.params.get(HDFC_DASH_Constants.STRING_DIGITAL_VERIFICATION);
        param_Product= RestContext.request.params.get(HDFC_DASH_Constants.STRING_PRODUCT);
        param_Campaign = RestContext.request.params.get(HDFC_DASH_Constants.STRING_CAMPAIGN);
        param_ExistingCustomer = RestContext.request.params.get(HDFC_DASH_Constants.STRING_EXISTING_CUST);
        param_OnlineStage = RestContext.request.params.get(HDFC_DASH_Constants.STRING_ONLINE_STAGE);
        param_FileCompleteness = RestContext.request.params.get(HDFC_DASH_Constants.STRING_FILE_COMPLETENESS);
        param_DSF = RestContext.request.params.get(HDFC_DASH_Constants.STRING_DSF);
        
        if(String.isNotBlank(param_lrNumber) || String.isNotBlank(param_FileNumber))
        {
            if((String.isNotBlank(param_lrNumber) && String.isBlank(param_FileNumber)) || (String.isNotBlank(param_FileNumber) && String.isBlank(param_lrNumber))) 
            {
                if(String.isNotBlank(param_lrNumber))
                {
                    flag_LrOrFileNo = HDFC_DASH_Constants.STRING_LRNUMBER;
                    listLrOrFileNo = param_lrNumber.split(HDFC_DASH_Constants.COMMA);
                    
                    if(!listLrOrFileNo.isEmpty()){
                        mapApplDetCondition.put(HDFC_DASH_Constants.HDFC_DASH_APPLICATION_LRNUMBER, HDFC_DASH_Constants.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listLrOrFileNo, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);	
                    }
                }
                else if(String.isNotBlank(param_FileNumber)){
                    flag_LrOrFileNo = HDFC_DASH_Constants.STRING_FILENUMBER;
                    listLrOrFileNo = param_FileNumber.split(HDFC_DASH_Constants.COMMA);
                    if(!listLrOrFileNo.isEmpty()){
                        mapApplDetCondition.put(HDFC_DASH_Constants.HDFC_DASH_APPLICATION_FILENUMBER, HDFC_DASH_Constants.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listLrOrFileNo, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);	
                    }
                }
            }
            else
            {
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_Error_Multiple_Params_Exp); 
            }
        }
        else{
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_REQUIREDFIELD_MISSING);
        }
        
        if(!mapApplDetCondition.isEmpty())
        {
            mapApplDetCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.APPLICANT_CAPACITY, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_B + HDFC_DASH_Constants.STRING_QUOTE);
            applDetailsList = HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(fieldsListapplDetail, mapApplDetCondition);
        }
        
        
        for(HDFC_DASH_Applicant_Details__c applDet : applDetailsList)
        {
            mapApplDet.put(applDet.HDFC_DASH_Application__c,applDet.Name);
            if(flag_LrOrFileNo == HDFC_DASH_Constants.STRING_LRNUMBER)
            {
                mapLrOrFileNo.put(applDet.HDFC_DASH_Application__r.HDFC_DASH_Leap_LR_Number__c,applDet); 
            }
            else if(flag_LrOrFileNo == HDFC_DASH_Constants.STRING_FILENUMBER)
            {
                mapLrOrFileNo.put(applDet.HDFC_DASH_Application__r.HDFC_DASH_File_Number__c,applDet); 
            }
        }
        
        if(String.isNotBlank(param_ExistingCustomer))
        {
            Boolean isCustomer = false;
            for(String lrOrFileNumber : listLrOrFileNo)
            {
                isCustomer = mapLrOrFileNo.get(lrOrFileNumber)?.HDFC_DASH_Contact__r?.HDFC_DASH_Is_Customer__c == null? false : mapLrOrFileNo.get(lrOrFileNumber)?.HDFC_DASH_Contact__r?.HDFC_DASH_Is_Customer__c;
                if(String.valueOf(isCustomer) == param_ExistingCustomer)
                {
                    listLrOrFileNoWithCustomer.add(lrOrFileNumber);
                    mapCondition = getQueryCondition();
                }
            }
        }
        else{
            mapCondition = getQueryCondition();
        }
        
        
        if(!mapCondition.isEmpty()){
            listApp = HDFC_DASH_Application_Selector.getApplicationBasedOnQuery(listOfFields_App, mapCondition);
        }
        
        for(Opportunity appRec : listApp)
        {
            listAppId.add(appRec.id);
        }
        
        mapInfoVal.put(HDFC_DASH_Constants.HDFC_DASH_Application, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAppId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        mapInfoVal.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Info_Validation_Record_Type, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_DOCUMENT + HDFC_DASH_Constants.STRING_QUOTE);
        mapInfoVal.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_SF_Triggered, HDFC_DASH_Constants.STRING_NOTEQUALTO + HDFC_DASH_Constants.BOOLEAN_TRUE);
        
        
        List<HDFC_DASH_Info_Validation__c> listinfoval = HDFC_DASH_AppInfoValidation_Selector.getappInfoValBasedOnQuery(listOfFields_InfoVal, mapInfoVal);
        
        for(HDFC_DASH_Info_Validation__c infoValRec : listinfoval)
        {
            List<HDFC_DASH_Info_Validation__c> listInfoRec =new  List<HDFC_DASH_Info_Validation__c>();
            
            if(mapAppl_InfoVal.containsKey(infoValRec.HDFC_DASH_Application__c))
            {
                List<HDFC_DASH_Info_Validation__c> listInfoExisting = mapAppl_InfoVal.get(infoValRec.HDFC_DASH_Application__c);
                listInfoExisting.add(infoValRec);
                mapAppl_InfoVal.put(infoValRec.HDFC_DASH_Application__c,listInfoExisting);
            }
            else
            {
                listInfoRec.add(infoValRec);
                mapAppl_InfoVal.put(infoValRec.HDFC_DASH_Application__c,listInfoRec);
            }
        }
        
        for(Opportunity appRec : listApp)
        {
            HDFC_DASH_APIResponse_GetleapDashboard.Leapdashboard leapDet = new HDFC_DASH_APIResponse_GetleapDashboard.Leapdashboard();
            List<HDFC_DASH_APIResponse_GetleapDashboard.AllDocsReceived> listDocDetails= new  List<HDFC_DASH_APIResponse_GetleapDashboard.AllDocsReceived>();
            List<HDFC_DASH_Info_Validation__c> listInfoRec =new List<HDFC_DASH_Info_Validation__c>();
            
            leapDet.serialNumber = appRec?.Name;
            leapDet.sfApplicationID = appRec?.Id;
            leapDet.lrNumber = appRec?.HDFC_DASH_Leap_LR_Number__c;
            leapDet.leadNumber = appRec?.HDFC_DASH_LMS_Lead_ID__c;
            leapDet.borrowerName = mapApplDet.get(appRec?.id);
            if(String.isNotBlank(appRec?.HDFC_DASH_Preferred_Branch_Code__c))
            {
                leapDet.serviceCenter = appRec?.HDFC_DASH_Preferred_Branch_Code__c;
            }
            else if(String.isNotBlank(appRec?.HDFC_DASH_LMS_Origin_Branch_Id__c))
            {
                leapDet.serviceCenter = appRec?.HDFC_DASH_LMS_Origin_Branch_Id__c; 
            }
            leapDet.leapFlag = appRec?.HDFC_DASH_Leap_Flag__c;
            leapDet.loanAmount = appRec?.HDFC_DASH_SO_Required_Loan_Amount__c;
            leapDet.empClass = appRec?.HDFC_DASH_Emp_Class__c;
            leapDet.status = appRec?.HDFC_DASH_Status__c;
            leapDet.onlineStage = appRec?.StageName;
            leapDet.fileCompleteness = appRec?.HDFC_DASH_File_Completeness_Flag__c;
            leapDet.digitalBankSt = HDFC_DASH_Constants.STRING_N;
            leapDet.salesExecutive = appRec?.Owner.HDFC_DASH_Executive_Code__c;
            leapDet.lrDate = appRec?.HDFC_DASH_Leap_LR_Date__c;
            leapDet.dsfExecutive = appRec?.HDFC_DASH_DSF_Executive__c;
            leapDet.spotLetterRequest = appRec?.HDFC_DASH_Spot_Offer_Started__c;
            leapDet.source = appRec?.HDFC_DASH_LMSLeadSource__c;
            
            if(mapAppl_InfoVal.get(appRec?.id)!= null)
            {
                listInfoRec = mapAppl_InfoVal.get(appRec?.id);
            }
            if(param_DocReceived == HDFC_DASH_Constants.STRING_Y && !listInfoRec.isEmpty())
            {
                leapDet.docReceived = HDFC_DASH_Constants.STRING_Y;
                
                for(HDFC_DASH_Info_Validation__c infoVal:listInfoRec)
                {
                    if(infoVal.HDFC_DASH_Doc_Type_Code__c == HDFC_DASH_Constants.STRING_BKST && infoVal.HDFC_DASH_Document_Extractable__c ==  HDFC_DASH_Constants.STRING_YES){
                        leapDet.digitalBankSt = HDFC_DASH_Constants.STRING_Y;
                    }
                    HDFC_DASH_APIResponse_GetleapDashboard.AllDocsReceived docDetails = new HDFC_DASH_APIResponse_GetleapDashboard.AllDocsReceived();
                    docDetails.sfCustNo = infoVal.HDFC_DASH_Customer_Number_Formula__c;
                    docDetails.sfApplId = appRec?.HDFC_DASH_ApplicationNumber__c; 
                    docDetails.fileNo = infoVal?.HDFC_DASH_File_Number__c;
                    docDetails.docType = infoVal?.HDFC_DASH_Doc_Type_Code__c;
                    docDetails.custNo = infoVal?.HDFC_DASH_ILPS_Cust_Number__c;
                    docDetails.capacity = infoVal?.HDFC_DASH_Applicant_Capacity__c;
                    if(infoVal?.HDFC_DASH_Salary_Year__c!=null && infoVal?.HDFC_DASH_Salary_Month__c!=null)
                    {
                        String salaryYear=String.valueOf(infoVal?.HDFC_DASH_Salary_Year__c);
                        if(salaryYear.length()== HDFC_DASH_Constants.INT_FOUR && infoVal?.HDFC_DASH_Salary_Month__c.length()>HDFC_DASH_Constants.INT_TWO){
                            docDetails.monthOfPayslip = infoVal?.HDFC_DASH_Salary_Month__c?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) + HDFC_DASH_Constants.STRING_DASH + salaryYear?.substring(HDFC_DASH_Constants.INT_TWO,HDFC_DASH_Constants.INT_FOUR) ;
                        }
                        
                    }
                    docDetails.monthOfGstReturn = infoVal?.HDFC_DASH_Mon_Of_GST_Return__c;
                    docDetails.assessmentYear = infoVal?.HDFC_DASH_Assessment_Year__c;
                    docDetails.financialYear = infoVal?.HDFC_DASH_Financial_Year__c;
                    docDetails.accountType = infoVal?.HDFC_DASH_Account_Type__c;
                    docDetails.bkAccNo = infoVal?.HDFC_DASH_Account_Number__c;
                    docDetails.fromDate = infoVal?.HDFC_DASH_From_Date__c;
                    docDetails.toDate = infoVal?.HDFC_DASH_To_Date__c;
                    docDetails.bankCode= infoVal?.HDFC_DASH_Bank_Code__c;
                    docDetails.currentEmployer = infoVal?.HDFC_DASH_Employer_Name__c;
                    docDetails.totalAmountPaid = infoVal?.HDFC_DASH_Total_Amount_Paid__c;
                    docDetails.noOfQuartersPaid =Integer.valueOf( infoVal?.HDFC_DASH_No_Of_Quarters_Paid__c);
                    docDetails.proofOfCommunication = infoVal?.HDFC_DASH_Proof_Of_Communication__c;
                    docDetails.kycDocType = infoVal?.HDFC_DASH_KYC_Doc_Code__c;
                    docDetails.bcpDocType = infoVal?.HDFC_DASH_BCP_Doc_Code__c;
                    docDetails.docExtractable = infoVal?.HDFC_DASH_Document_Extractable__c;
                    docDetails.docScannedBy = infoVal?.HDFC_DASH_Document_Scanned_By__c;
                    docDetails.docImageOrigin = infoVal?.HDFC_DASH_Document_Image_Origin__c;
                    docDetails.latitute = infoVal?.HDFC_DASH_Location__Latitude__s;
                    docDetails.longitute = infoVal?.HDFC_DASH_Location__Longitude__s;
                    docDetails.storageIdentifier = infoVal?.HDFC_DASH_Storage_Identifier__c;
                    docDetails.storageType = infoVal?.HDFC_DASH_Storage_Type__c;
                    docDetails.docRecSrno = infoVal?.HDFC_DASH_Document_Record_Serial_No__c ;
                    listDocDetails.add(docDetails);
                }
            }
            else{
                leapDet.docReceived = HDFC_DASH_Constants.STRING_N;
            }
            leapDet.allDocsReceived = listDocDetails;
            
            sfLeapDashboard.add(leapDet);
        }
        apiRes.leapdashboard = sfLeapDashboard;
        return apiRes;  
    }
    
    /* 
Method: getQueryCondition
Description: Method to get the query conditions for Application selector.
*/
    Public Static Map<string,string> getQueryCondition()
    {
        
        Map<string,string> mapCondition = new Map<string,string>();
        
        if(!listLrOrFileNoWithCustomer.isEmpty() && String.isNotBlank(param_ExistingCustomer))
        {
            if(String.isNotBlank(param_lrNumber))
            {
                mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Leap_LR_Number, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listLrOrFileNoWithCustomer, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
            else if(String.isNotBlank(param_FileNumber))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_name, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listLrOrFileNoWithCustomer, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
        }
        else if(!listLrOrFileNo.isEmpty())
        {
            if(String.isNotBlank(param_lrNumber))
            {
                mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_Leap_LR_Number, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listLrOrFileNo, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
            else if(String.isNotBlank(param_FileNumber))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_name, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listLrOrFileNo, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
        }
        
        if(!mapCondition.isEmpty()){
            if(String.isNotBlank(param_FromDate))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.CREATEDDATE, HDFC_DASH_Constants.STRING_GRETHERTHAN_EQUALTO + param_FromDate); 
            }
            if(String.isNotBlank(param_ToDate))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.CREATEDDATE, HDFC_DASH_Constants.STRING_LESSTHAN_EQUALTO + param_ToDate);
            }
            if(String.isNotBlank(param_Status))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Status, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_Status + HDFC_DASH_Constants.STRING_QUOTE);
            }
            if(String.isNotBlank(param_Branch))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_Branch + HDFC_DASH_Constants.STRING_QUOTE);
            }
            if(String.isNotBlank(param_ServiceCenter))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.HDFC_DASH_Preferred_Branch_Code, HDFC_DASH_Constants.STRING_NOTEQUALTO + HDFC_DASH_Constants.STRING_NULL );
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Preferred_Branch_Code, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_ServiceCenter + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_CLOSING_BRACE);
                mapCondition.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.HDFC_DASH_Preferred_Branch_Code, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_NULL);
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_ServiceCenter + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_CLOSING_BRACE + HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
            if(String.isNotBlank(param_Agency))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_LMS_Agency_Code, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_Agency + HDFC_DASH_Constants.STRING_QUOTE);
            }
            if(String.isNotBlank(param_SalesExecutive))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.OWNER_EXECUTIVE_CODE, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_SalesExecutive + HDFC_DASH_Constants.STRING_QUOTE);
            }
            if(String.isNotBlank(param_SpotOfferRequest))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Spot_Offer_Started, HDFC_DASH_Constants.STRING_EQUALTO + param_SpotOfferRequest);
            }
            
            if(String.isNotBlank(param_EmploymentType))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Emp_Class, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_EmploymentType + HDFC_DASH_Constants.STRING_QUOTE);
            }
            if(String.isNotBlank(param_Product))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.LOAN_TYPE_CODE, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_Product + HDFC_DASH_Constants.STRING_QUOTE);
            }
            if(String.isNotBlank(param_OnlineStage))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.Stage, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_OnlineStage + HDFC_DASH_Constants.STRING_QUOTE);
            }
            
            if(String.isNotBlank(param_Campaign))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Campaign_Code, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_Campaign + HDFC_DASH_Constants.STRING_QUOTE);
            }
            
            if(String.isNotBlank(param_DSF))
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_DSF_Flag, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_DSF + HDFC_DASH_Constants.STRING_QUOTE);
            }
            mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_File_First_Push_To_ILPS, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.BOOLEAN_FALSE );
        }
        return mapCondition;
    }
    
}