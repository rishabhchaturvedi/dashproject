@RestResource(urlMapping='/hdfc/quoteinfomation/V1.0')
/*
Author: Anisha Arumugam
Class Name: HDFC_DASH_RestResource_QuoteInfo
*/
global with sharing class HDFC_DASH_RestResource_QuoteInfo {
    @HttpPatch
    /*
Method:storeQuoteInfo()
Description:This method will store the quote info data coming from OS.
*/
    global static void storeQuoteInfo() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders;
        String response;
        Exception exp;
        HDFC_DASH_APIResponse_QuoteInfo apiResponse = new HDFC_DASH_APIResponse_QuoteInfo();  
        try
        { 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_QuoteInfo.processQuoteDetails(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_QUOTE_INFO, HDFC_DASH_Constants.HANDLE_QUOTEINFO_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_PATCH, expDetails); 
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);            
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_QUOTEINFO, request.requestBody.tostring(), (res.responseBody)?.ToString(), HDFC_DASH_Constants.METHOD_TYPE_PATCH, HDFC_DASH_Constants.HANDLE_QUOTEINFO_REQUEST, exp, request.headers);        
            
            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                    res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
}