/*
	Author: Anisha Arumugam
	Class: HDFC_DASH_APIResponse_FinancialInfo
	Description: Apex Class for handling success message for Financial Info API
*/
public inherited sharing class HDFC_DASH_APIResponse_FinancialInfo {
    public String successMessage;          
}