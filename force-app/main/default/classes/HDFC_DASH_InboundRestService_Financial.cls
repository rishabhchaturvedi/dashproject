/*
Author: Anisha Arumugam
Class:HDFC_DASH_InboundRestService_Financial
Description: Service Class for Financial Info API
*/
public inherited sharing class HDFC_DASH_InboundRestService_Financial {
    
    private static ID appDetailID;
    /* 
    Method:processFinancialDetails
    Description: Method to process Financial information
    */
    public static HDFC_DASH_APIResponse_FinancialInfo processFinancialDetails(RestRequest request)
    {      
        //Calling JSON Parser Class
        HDFC_DASH_ParseFinancialDetails result = HDFC_DASH_ParseFinancialDetails.parse(request);
        
        //Update Application, Applicant Details and Employment Details Objects
        HDFC_DASH_APIResponse_FinancialInfo apiRes = updateFinancialDetails(result);
        
        //Store Validation Outcome in Applicant Info Validation Object
        HDFC_DASH_UtilityClass.storeValidationOutcome(request, appDetailID);
        
        return apiRes;  
    }
    
    /*   Method:updateFinancialDetails
    Description:This method will update the Application, Applicant Details and Employment Details Objects
    */
    private static HDFC_DASH_APIResponse_FinancialInfo updateFinancialDetails(HDFC_DASH_ParseFinancialDetails result)
    {             
        List<string> listAppID = new List<string>();
        List<string> listAppDetailID = new List<string>();       
        List<Opportunity> listApp = new List<Opportunity>();
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>();
        HDFC_DASH_APIResponse_FinancialInfo apiRes =  new HDFC_DASH_APIResponse_FinancialInfo();  
        List<string> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING};  
            List<string> listOfFields_AppDetails = new List<string>{HDFC_DASH_Constants.ID_STRING};  
                
                List<HDFC_DASH_Master_Table__c> listMasterTable = new List<HDFC_DASH_Master_Table__c>();
        List<HDFC_DASH_City_Master__c> listCityMaster = new List<HDFC_DASH_City_Master__c>();
        List<HDFC_DASH_Project_Master__c> listProjectMaster = new List<HDFC_DASH_Project_Master__c>();
        List<Account> listEmpMaster = new List<Account>();
        List<HDFC_DASH_Pincode_Master__c> listPinCodeMaster = new List<HDFC_DASH_Pincode_Master__c>();
        
        List<string> listMasterTableIDs = new List<string>();
        List<string> listCityMasterIDs = new List<string>();
        List<string> listProjectMasterIDs = new List<string>();
        List<string> listEmpMasterIDs = new List<string>();
        List<string> listPinCodeMasterIDs = new List<string>();
        
        map<string, HDFC_DASH_Master_Table__c> mapMasterTable = new map<string, HDFC_DASH_Master_Table__c>();
        map<string, HDFC_DASH_Pincode_Master__c> mapPinCodeMaster = new map<string, HDFC_DASH_Pincode_Master__c>();
        
        if(!String.isBlank(result.ApplicationDetails.sfApplicationID) && !String.isBlank(result.ApplicationDetails.sfApplicantID))
        {
            listAppID.add(result.ApplicationDetails.sfApplicationID);                
            
            listAppDetailID.add(result.ApplicationDetails.sfApplicantID); 

            //Adding the list of IDs to be queried
            listMasterTableIDs.add(result?.ApplicationDetails?.LoanType);
            listMasterTableIDs.add(result?.ApplicationDetails?.PropertyStateCode);
            listMasterTableIDs.add(result?.ApplicationDetails?.PropertyCountryCode);                        
            listCityMasterIDs.add(result?.ApplicationDetails?.PropertyCityCode);
            listProjectMasterIDs.add(result?.ApplicationDetails?.PropertyDetails?.ProjectCode);
            listEmpMasterIDs.add(result?.ApplicationDetails?.ApplicantEmploymentDetails?.EmployerCode);
            listPinCodeMasterIDs.add(result?.ApplicationDetails?.PropertyDetails?.PropertyPostalCode);
            
            //Calling Application and Applicant Detail Selector Classes
            listApp = HDFC_DASH_Application_Selector.getApplication(listAppID, listOfFields_App, HDFC_DASH_Constants.ID_STRING);
            listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppDetailID, listOfFields_AppDetails, HDFC_DASH_Constants.ID_STRING);
            
            if(!listApp.isEmpty() && !listAppDetails.isEmpty())
            {
                Opportunity objApplication = listApp[HDFC_DASH_Constants.INT_ZERO];                
                appDetailID = result.ApplicationDetails.sfApplicantID;
                
                //Calling Selector to query Master Tables     
                listMasterTable = HDFC_DASH_MasterTables_Selector.getMasterTable(listMasterTableIDs);                
                listCityMaster = HDFC_DASH_MasterTables_Selector.getCityMaster(listCityMasterIDs);               
                listProjectMaster = HDFC_DASH_MasterTables_Selector.getProjectMaster(listProjectMasterIDs);
                listEmpMaster = HDFC_DASH_MasterTables_Selector.getEmpMaster(listEmpMasterIDs);
                listPinCodeMaster = HDFC_DASH_MasterTables_Selector.getPinCodeMaster(listPinCodeMasterIDs);
                
                for(HDFC_DASH_Master_Table__c objMT : listMasterTable)
                    mapMasterTable.put(objMT.HDFC_DASH_CD_Val__c, objMT);
                
                for(HDFC_DASH_Pincode_Master__c objPM : listPinCodeMaster)
                    mapPinCodeMaster.put(objPM.HDFC_DASH_Pincode__c, objPM);
                
                //Set loan type name and record id from Master Table using loan type Code    
                if(result?.ApplicationDetails?.LoanType != null && mapMasterTable.get(result?.ApplicationDetails?.LoanType) != null)
                {
                    objApplication.HDFC_DASH_Loan_Type_Code__c = result?.ApplicationDetails?.LoanType;
                    objApplication.HDFC_DASH_Loan_Type__c = mapMasterTable.get(result?.ApplicationDetails?.LoanType)?.HDFC_DASH_CD_Desc__c;
                    objApplication.HDFC_DASH_Loan_Type_RecordId__c = mapMasterTable.get(result?.ApplicationDetails?.LoanType)?.id ;
                }
                objApplication.HDFC_DASH_Financing_For__c=result?.ApplicationDetails?.financingFor;
                //objApplication.HDFC_DASH_Stage_Number__c = result?.ApplicationDetails?.leadStageNumber;    
                //objApplication.HDFC_DASH_Stage_Description__c = result?.ApplicationDetails?.leadStageDescription;

                objApplication.HDFC_DASH_CS_Interest_Type__c = result?.ApplicationDetails?.preferredInterestRate;
                objApplication.HDFC_DASH_Balanced_transfer_loan__c = result?.ApplicationDetails?.balanceTransferLoan;
                
                objApplication.HDFC_DASH_Property_Decided__c = result?.ApplicationDetails?.PropertyDecided;     
                
                //If property not decided yet, then set the property indicative cost
                if(result.ApplicationDetails.PropertyDecided != HDFC_DASH_Constants.STRING_YES)
                    objApplication.HDFC_DASH_Property_Indicative_Cost__c = result?.ApplicationDetails?.PropertyIndicativeCost;
                
                //Update Property Details             
                objApplication.HDFC_DASH_Property_Cost__c = result?.ApplicationDetails?.PropertyDetails?.PropertyCost;
                objApplication.HDFC_DASH_Property_House_Identifier__c = result?.ApplicationDetails?.PropertyDetails?.PropertyHouseIdentifier;
                objApplication.HDFC_DASH_Property_StreetName_Identifier__c = result?.ApplicationDetails?.PropertyDetails?.PropertyStreetNameIdentifier;
                objApplication.HDFC_DASH_Property_Landmark__c = result?.ApplicationDetails?.PropertyDetails?.PropertyLandmark;
                objApplication.HDFC_DASH_Property_Locality__c = result?.ApplicationDetails?.PropertyDetails?.PropertyLocality;
                objApplication.HDFC_DASH_Property_Village_Town_City__c = result?.ApplicationDetails?.PropertyDetails?.PropertyVillageTownCity;
                objApplication.HDFC_DASH_Property_Sub_District__c = result?.ApplicationDetails?.PropertyDetails?.PropertySubDistrict;
                objApplication.HDFC_DASH_Property_District__c = result?.ApplicationDetails?.PropertyDetails?.PropertyDistrict;
                objApplication.HDFC_DASH_Property_State__c = result?.ApplicationDetails?.PropertyDetails?.PropertyState;
                objApplication.HDFC_DASH_Property_Country__c = result?.ApplicationDetails?.PropertyDetails?.PropertyCountry;                
                objApplication.HDFC_DASH_Property_Post_Office_Name__c = result?.ApplicationDetails?.PropertyDetails?.PropertyPostOfficeName;
                objApplication.HDFC_DASH_Property_Status__c = result?.ApplicationDetails?.PropertyDetails?.PropertyStatus;   
                
                //Set city name and city record id from City Master using City Code
                if(!String.isBlank(result?.ApplicationDetails?.PropertyCityCode) && !listCityMaster.isEmpty())
                {
                    objApplication.HDFC_DASH_Property_City_Code__c = result?.ApplicationDetails?.PropertyCityCode;  
                    objApplication.HDFC_DASH_Property_City__c = listCityMaster[HDFC_DASH_Constants.INT_ZERO]?.HDFC_DASH_City_Name__c ;
                    objApplication.HDFC_DASH_Property_City_RecordID__c = listCityMaster[HDFC_DASH_Constants.INT_ZERO]?.id;
                }
                
                //Set State name and State record id from Master Table using State code                
                if(!String.isBlank(result?.ApplicationDetails?.PropertyStateCode) && mapMasterTable.get(result?.ApplicationDetails?.PropertyStateCode) != null)
                {
                    objApplication.HDFC_DASH_Property_State_Code__c = result?.ApplicationDetails?.PropertyStateCode;
                    objApplication.HDFC_DASH_Property_State__c = mapMasterTable.get(result?.ApplicationDetails?.PropertyStateCode)?.HDFC_DASH_CD_Desc__c ;
                    objApplication.HDFC_DASH_Property_State_RecordID__c = mapMasterTable.get(result?.ApplicationDetails?.PropertyStateCode)?.id;
                }
                
                //Set Country name and Country record id from Master Table using Country code                
                if(!String.isBlank(result?.ApplicationDetails?.PropertyCountryCode) && mapMasterTable.get(result?.ApplicationDetails?.PropertyCountryCode) != null)
                {
                    objApplication.HDFC_DASH_Property_Country_Code__c = result?.ApplicationDetails?.PropertyCountryCode;
                    objApplication.HDFC_DASH_Property_Country__c = mapMasterTable.get(result?.ApplicationDetails?.PropertyCountryCode)?.HDFC_DASH_CD_Desc__c ;
                    objApplication.HDFC_DASH_Property_Country_RecordID__c = mapMasterTable.get(result?.ApplicationDetails?.PropertyCountryCode)?.id ;
                }                
                
                //Set Postal code record id from Pin code Master using Postal code
                objApplication.HDFC_DASH_Property_Postal_Code__c = result?.ApplicationDetails?.PropertyDetails?.PropertyPostalCode;
                if(!String.isBlank(result?.ApplicationDetails?.PropertyDetails?.PropertyPostalCode) && mapPinCodeMaster.get(result?.ApplicationDetails?.PropertyDetails?.PropertyPostalCode) != null)
                {                   
                    objApplication.HDFC_DASH_Property_Postal_RecordId__c = mapPinCodeMaster.get(result?.ApplicationDetails?.PropertyDetails?.PropertyPostalCode)?.id ;
                    objApplication.HDFC_DASH_Property_Post_Office_Name__c = mapPinCodeMaster.get(result?.ApplicationDetails?.PropertyDetails?.PropertyPostalCode)?.HDFC_DASH_Place__c;
                }
                
                //Set Project record id from Project Master using Project code
                objApplication.HDFC_DASH_Name_Of_Project__c = result?.ApplicationDetails?.PropertyDetails?.ProjectName; 
                objApplication.HDFC_DASH_Name_of_Project_Code__c = result?.ApplicationDetails?.PropertyDetails?.ProjectCode;
                if(!String.isBlank(result?.ApplicationDetails?.PropertyDetails?.ProjectCode) && !listProjectMaster.isEmpty())
                { 
                    objApplication.HDFC_DASH_Name_Of_Project__c = listProjectMaster[HDFC_DASH_Constants.INT_ZERO]?.HDFC_DASH_Project_Name__c ;
                    objApplication.HDFC_DASH_Name_Of_Project_RecordID__c = listProjectMaster[HDFC_DASH_Constants.INT_ZERO]?.id ;
                }
                
                Database.update(objApplication);  
                
                //Update Financial Info
                HDFC_DASH_Applicant_Details__c objAppDetail = listAppDetails[HDFC_DASH_Constants.INT_ZERO];
                objAppDetail.HDFC_DASH_Skip_Employment_Details__c = result?.ApplicationDetails?.SkipEmploymentDetails;
                objAppDetail.HDFC_DASH_SO_Fixed_Monthly_Income__c = result?.ApplicationDetails?.FinancialInfo?.FixedMonthlyIncome;
                objAppDetail.HDFC_DASH_SO_Additional_Income__c = result?.ApplicationDetails?.FinancialInfo?.AdditionalIncome;
                objAppDetail.HDFC_DASH_No_Additional_Income__c = result?.ApplicationDetails?.FinancialInfo?.NoAdditionalIncome;
                objAppDetail.HDFC_DASH_Salary_Slip_Attached__c = result?.ApplicationDetails?.FinancialInfo?.SalarySlipAttached;
                objAppDetail.HDFC_DASH_Confidence_Score__c = result?.ApplicationDetails?.ConfidenceScore;
                objAppDetail.HDFC_DASH_TF_Confidence_Score__c=  result?.ApplicationDetails?.confidenceScoreTechnicalFailure==null?false:result?.ApplicationDetails?.confidenceScoreTechnicalFailure;
                objAppDetail.HDFC_DASH_Experian_CIBIL_ID__c = result?.ApplicationDetails?.ExperianCibilID;
                objAppDetail.HDFC_DASH_TF_Experian_CIBILID__c=result?.ApplicationDetails?.experianCibilTechnicalFailure==null?false:result?.ApplicationDetails?.experianCibilTechnicalFailure;
                
                //Update Risk Score
                objAppDetail.HDFC_DASH_RiskScore_Customer_Status__c = result?.ApplicationDetails?.riskScore?.riskScoreCustomerstatus;
                objAppDetail.HDFC_DASH_RiskScore_Customer_flags__c = result?.ApplicationDetails?.riskScore?.riskScoreCustomerflags;
                objAppDetail.HDFC_DASH_RiskScore_Rtr_Exists__c = result?.ApplicationDetails?.riskScore?.riskScoreRtrexists;
                objAppDetail.HDFC_DASH_RiskScore_Customer_grade__c = result?.ApplicationDetails?.riskScore?.riskScoreCustomergrade;
                objAppDetail.HDFC_DASH_RiskScore_Risk_event__c = result?.ApplicationDetails?.riskScore?.riskScoreRiskevent;
                objAppDetail.HDFC_DASH_RiskScore_Availed_moratorium__c = result?.ApplicationDetails?.riskScore?.riskScoreAvailedMoratorium;
                objAppDetail.HDFC_DASH_RiskScore_Availed_restruct__c = result?.ApplicationDetails?.riskScore?.riskScoreAvailedRestruct;
                objAppDetail.HDFC_DASH_RiskScore_Hdfc_Staff__c = result?.ApplicationDetails?.riskScore?.riskScoreHdfcStaff;
                objAppDetail.HDFC_DASH_RiskScore_Confidence_score__c = result?.ApplicationDetails?.riskScore?.riskScoreConfidenceScore;
                objAppDetail.HDFC_DASH_RiskSore_Exist_in_neg_list__c = result?.ApplicationDetails?.riskScore?.riskScoreExistInNegList;
                objAppDetail.HDFC_DASH_TF_RiskScore__c = result?.ApplicationDetails?.riskScoreTechnicalFailure==null?false:result?.ApplicationDetails?.riskScoreTechnicalFailure;
                
                Database.Update(objAppDetail);
                
                //Insert Employment details
                HDFC_DASH_Applicant_Employment_Details__c objEmpDetail = new HDFC_DASH_Applicant_Employment_Details__c();
                objEmpDetail.RecordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Employment_Details__c.getRecordTypeInfosByDeveloperName().get(HDFC_DASH_Constants.Dev_NAME_RECORDTYPE_SALARIED).getRecordTypeId();

                //objEmpDetail.name = result.ApplicationDetails?.ApplicantEmploymentDetails?.EmployerName; 
                objEmpDetail.HDFC_DASH_Applicant_Details__c = objAppDetail.id;
                objEmpDetail.HDFC_DASH_Nat_Of_Emp__c = result.ApplicationDetails?.ApplicantEmploymentDetails?.Occupation;//as per the final master sheet we decided to map occupation with nature of employment
                objEmpDetail.HDFC_DASH_Work_Email__c = result.ApplicationDetails?.ApplicantEmploymentDetails?.WorkEmail;
                objEmpDetail.HDFC_DASH_Employer_Name__c = result.ApplicationDetails?.ApplicantEmploymentDetails?.EmployerName;  
                if(result.ApplicationDetails?.ApplicantEmploymentDetails?.Occupation == HDFC_DASH_Constants.Occupation_Salaried)
                {
                    objEmpDetail.HDFC_DASH_Nat_Of_Emp__c = HDFC_DASH_Constants.NAT_OF_EMP;
                    objEmpDetail.HDFC_DASH_Self_Emp_Type__c = HDFC_DASH_Constants.SELF_EMP_TYPE;
                }
                //Set Employer record id from Employer Master using Employer code
                objEmpDetail.HDFC_DASH_Employer_Code__c = result?.ApplicationDetails?.ApplicantEmploymentDetails?.EmployerCode; 
                if(!String.isBlank(result?.ApplicationDetails?.ApplicantEmploymentDetails?.EmployerCode) && !listEmpMaster.isEmpty())
                {           
                    objEmpDetail.HDFC_DASH_Employer_Name__c = listEmpMaster[HDFC_DASH_Constants.INT_ZERO]?.Name;
                    objEmpDetail.HDFC_DASH_Employer_Name_RecordID__C = listEmpMaster[HDFC_DASH_Constants.INT_ZERO]?.id;
                }         
                Database.insert(objEmpDetail);
                
                apiRes.SuccessMessage = HDFC_DASH_Constants.FINANCIALINFO_SUCCESSMESSAGE;
            }   
            else
            {
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_NOTEXIST); 
            }
        } 
        else
        {
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_MISSING); 
        }
        return apiRes; 
    }    
}