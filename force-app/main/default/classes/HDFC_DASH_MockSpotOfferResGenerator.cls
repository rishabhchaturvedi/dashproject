/**
className: HDFC_DASH_MockSpotOfferResGenerator
Date: 22 July 2021
Company: Accenture
Class Description: This is mock class used for generating response for SpotOffer_ILPScallout.
**/ 
@isTest(SeeAllData=false)
global with sharing class HDFC_DASH_MockSpotOfferResGenerator implements HttpCalloutMock{
    /* 
Method Name :respond
Parameters  :HTTPRequest req
Description :This method would create a fake response for SpotOffer_ILPScallout.
*/
    global HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        // Create a fake response
        res.setHeader('Content-Type', 'application/json');  
        res.setBody('{ "sf_appl_id" : 123456789, "file_no": 610123456, "custDetails": [ { "sf_cust_no": 10000138, "cust_no": 123454321, "unq_cust_no": 678901234 }, { "sf_cust_no": 10000139, "cust_no": 213432504, "unq_cust_no": 345632504 } ], "errorMessage":"" }');
        res.setStatusCode(200);
        system.assertNotEquals(null, req);
        return res;
    }
}