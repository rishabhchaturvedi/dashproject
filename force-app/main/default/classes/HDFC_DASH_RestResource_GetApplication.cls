/**
* 	className: HDFC_DASH_RestResource_GetApplication
DevelopedBy: Janani Mohankumar
Date: 06 July 2021
Company: Accenture
Class Description: This is a webservice class for getting the application and related child objects .
**/
@RestResource(urlMapping='/hdfc/getApplication/V1.0/*')
global with sharing class HDFC_DASH_RestResource_GetApplication {
    
    @Httpget
    /* 
Method Name :getApplication
Parameters  :
Description :This method would get the application and related child objects .
*/
    global static void getApplication() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders = new Map<string,string>();
        Exception exp = NULL;
        HDFC_DASH_ParseApplication apiResponse = new HDFC_DASH_ParseApplication();  
        try{ 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_getApp.getApplication(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse)); 
        }
        catch(Exception e){ 
            // Throw Exception
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() 
                + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_GET_APPLICATION,
                                                        HDFC_DASH_Constants.HANDLE_GET_APPLICATION_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_GET, expDetails);
            
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally{	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_GETAPP,
                                                                request.requestBody.tostring(), 
                                                                (res.responseBody)?.toString(), HDFC_DASH_Constants.METHOD_TYPE_GET, 
                                                                HDFC_DASH_Constants.HANDLE_GET_APPLICATION_REQUEST, exp, request.headers);        
            
            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                    res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
    
    
}