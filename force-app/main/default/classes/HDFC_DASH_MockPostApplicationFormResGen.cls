/**className: HDFC_DASH_MockPostApplicationFormResGen
Author: Sai Suman
Date: 11 Dec 2021
Company: Accenture
Class Description: This is mock class used for generating response for PostApplicationForm API callout.
**/ 
@isTest(SeeAllData=false)

global with sharing class HDFC_DASH_MockPostApplicationFormResGen implements HttpCalloutMock{
    /* 
Method Name :response
Parameters  :HTTPRequest req
Description :This method would create a fake response for PostApplicationForm callout.
*/
    global HTTPResponse respond(HTTPRequest req) {
        
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        HttpResponse res = new HttpResponse();
        
        // Create a fake response
        
        res.setHeader('Content-Type', 'application/json'); 
        res.setBody('{"message":"Success","StatusCode":"200"}');
        res.setStatusCode(200);
        
        return res;
    }
}