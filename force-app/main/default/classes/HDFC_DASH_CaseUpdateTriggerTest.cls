@isTest
public class HDFC_DASH_CaseUpdateTriggerTest {
    
    Public static final string MODE_STRING = 'Government of India';
    public static final string SUB_MODE_STRING = 'MOF';
    
    
    @isTest
    public static void testCaseUpdate(){
        List<Case_Update__e> cuList = HDFC_DASH_TestDataFactory.createCaseUpdatePlatformEvent(1);
        Case caseRec = HDFC_DASH_TestDataFactory_Case.createBasicEmailCase();
        insert caseRec;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        for(Case_Update__e c : cuList){
            c.Existing_CaseId__c = caseRec.Id;
            c.Mode__c = MODE_STRING;
            c.Sub_Mode__c = SUB_MODE_STRING;
            c.Reason__c = HDFC_DASH_Constants.HIGHERPRIORITYREOPEN;
        }
    	Test.startTest();
        System.runAs(sysAdmin){
       eventBus.publish(cuList);
       Test.stopTest();
          
 
        }
    }
    
      @isTest
    public static void testCaseUpdate2(){
        List<Case_Update__e> cuList = HDFC_DASH_TestDataFactory.createCaseUpdatePlatformEvent(1);
        Case caseRec = HDFC_DASH_TestDataFactory_Case.createBasicEmailCase();
        caseRec.HDFC_DASH_Case_Type__c = 'Complaint';
        caseRec.HDFC_DASH_Skip_AutoAssignment__c = true;
        insert caseRec;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        for(Case_Update__e c : cuList){
            c.Existing_CaseId__c = caseRec.Id;
            c.Mode__c = 'Website';
            c.Sub_Mode__c = 'CGRO';
            c.Category__c = 'CLSS';
            c.Sub_Category__c = 'Property ownership issue';
            c.Reason__c = HDFC_DASH_Constants.HIGHERPRIORITYREOPEN;
        }
    	Test.startTest();
        System.runAs(sysAdmin){
       eventBus.publish(cuList);
       Test.stopTest();
          
 
        }
    }
    
     @isTest
    public static void testCaseUpdate3(){
        List<Case_Update__e> cuList = HDFC_DASH_TestDataFactory.createCaseUpdatePlatformEvent(1);
        Case caseRec = HDFC_DASH_TestDataFactory_Case.createBasicEmailCase();
        caseRec.HDFC_DASH_Case_Type__c = 'Complaint';
        insert caseRec;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        for(Case_Update__e c : cuList){            
            c.Existing_CaseId__c = caseRec.Id;
            c.Mode__c = 'Website';
            c.Sub_Mode__c = 'MD Level 2- Website';
            c.Category__c = 'CLSS';
            c.Sub_Category__c = 'Property ownership issue';
            c.Reason__c = HDFC_DASH_Constants.HIGHERPRIORITYREOPEN;
        }
    	Test.startTest();
        System.runAs(sysAdmin){
       eventBus.publish(cuList);
       Test.stopTest();
          
 
        }
    }
	
     @isTest
    public static void testCaseUpdate4(){
        List<Case_Update__e> cuList = HDFC_DASH_TestDataFactory.createCaseUpdatePlatformEvent(1);
        Case caseRec = HDFC_DASH_TestDataFactory_Case.createBasicEmailCase();
        insert caseRec;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        for(Case_Update__e c : cuList){
            c.Existing_CaseId__c = caseRec.Id;
            c.Mode__c = 'Social Media';
            c.Sub_Mode__c = 'YouTube';
            c.Reason__c = HDFC_DASH_Constants.LOWEREQUALPRIORITY;
        }
    	Test.startTest();
        System.runAs(sysAdmin){
       eventBus.publish(cuList);
       Test.stopTest();
          
 
        }
    }
    
     @isTest
    public static void testCaseUpdate5(){
        List<Case_Update__e> cuList = HDFC_DASH_TestDataFactory.createCaseUpdatePlatformEvent(1);
        Case caseRec = HDFC_DASH_TestDataFactory_Case.createBasicEmailCase();
        caseRec.HDFC_DASH_Skip_AutoAssignment__c = true;
        caseRec.HDFC_DASH_Case_Type__c = 'Complaint';
        insert caseRec;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        for(Case_Update__e c : cuList){
            c.Existing_CaseId__c = caseRec.Id;
            c.Mode__c = 'Social Media';
            c.Sub_Mode__c = 'YouTube';
            c.Category__c = 'CLSS';
            c.Sub_Category__c = 'Property ownership issue';
            c.Reason__c = HDFC_DASH_Constants.HIGHERPRIORITY;
        }
    	Test.startTest();
        System.runAs(sysAdmin){
       eventBus.publish(cuList);
       Test.stopTest();
          
 
        }
    }
    
    
}