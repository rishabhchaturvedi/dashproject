/**
className: HDFC_DASH_MockRiskScoreResGenerator
Date: 5 Aug 2021
Company: Accenture
Class Description: This is mock class used for generating response for RiskSCore API callout.
**/ 
@isTest(SeeAllData=false)
global with sharing class HDFC_DASH_MockRiskScoreResGenerator implements HttpCalloutMock{
   
    /* 
    Method Name :respond
    Parameters  :HTTPRequest req
    Description :This method would create a fake response for Risk Score callout.
    */
    global HTTPResponse respond(HTTPRequest req) {
        
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        HttpResponse res = new HttpResponse();
        
            // Create a fake response
            res.setHeader('Content-Type', 'application/json');  
            res.setBody('{"custRiskDetail":[{"customerstatus":"EXISTING","customerflags":null,'+
            '"rtrexists":null,"customergrade":"NEVER IN DEFAULT","riskevent":null,"availedmoratorium":null,'+
            '"availedrestruct":"Y","hdfcstaff":null,"confidencescore":null,"existinneglist":null}],"errorMessage":"SUCCESS"}');
            res.setStatusCode(200);
            system.assertNotEquals(null, req);
        
        return res;
    }
}