/**
* 	className: HDFC_DASH_RestResource_CoApplicant
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This is a webservice class for DedupeCheck .
**/
@RestResource(urlMapping='/hdfc/coApplicant/V1.0')
global with sharing class HDFC_DASH_RestResource_CoApplicant {
    
    
    @HttpPatch
    /* 
Method Name :storeCoApplicantInfo
Parameters  :
Description :This method would be called when CoApplicant  API is called .
*/
    global static void storeCoApplicantInfo() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders = new Map<string,string>();
        Exception exp;
        HDFC_DASH_APIResponse_CoApplicant apiResponse = new HDFC_DASH_APIResponse_CoApplicant();   
        
        try{ 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_CoApplicant.processCoApplicantDetails(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e){     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() 
                + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_COAPPLICATION_INFO,
                                                        HDFC_DASH_Constants.HANDLE_CO_APPLICATION_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_PATCH, expDetails);
            // Throw Exception
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally{	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_COAPP,
                                                                request.requestBody.tostring(), 
                                                                (res.responseBody)?.toString(), HDFC_DASH_Constants.METHOD_TYPE_PATCH, 
                                                                HDFC_DASH_Constants.HANDLE_CO_APPLICATION_REQUEST, exp, request.headers);        
            
            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet())
                {
                    res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
    
    
}