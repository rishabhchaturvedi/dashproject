/**
className: HDFC_DASH_AccountTriggerOpsHelper
DevelopedBy: Tejeswari
Date: 20 January 2022
Company: Accenture 
Class Description:  Helper class for HDFC_DASH_AccountTriggerOps.
**/
public class HDFC_DASH_AccountTriggerOpsHelper {
    
    /* 
Method Name :filterInsertAccForOptinCallout
Description :This method would filter inserting Account for OptinCallout.
*/
    public static List<Account> filterInsertAccForOptinCallout(){
        List<Account> listAccounts = new List<Account>();
        List<Account> listUpdateAccount = new List<Account>();
        listAccounts = trigger.new;  
        for(Account accRec:listAccounts)
        {
            
            if(accRec.HDFC_DASH_WhatsApp_Consent__pc == HDFC_DASH_Constants.STRING_Y && String.isNotBlank(accRec.PersonMobilePhone) && String.isNotBlank(accRec.HDFC_DASH_Mobile_Country_Code__pc)){
                listUpdateAccount.add(accRec);
            }
        }
        
        return listUpdateAccount;
    } 
    
    /* 
Method Name :filterUpdateAccForOptinCallout
Description :This method would filter update Account for OptinCallout.
*/
    public static List<Account> filterUpdateAccForOptinCallout(){
        List<Account> listAccounts = new List<Account>();
        List<Account> listUpdateAccount = new List<Account>();
        listAccounts = trigger.new;  
        for(Account accRec:listAccounts)
        {
            Account oldAccountRec = (Account)Trigger.oldMap?.get(accRec.id); 
            Account newAccountRec = (Account)Trigger.newMap?.get(accRec.id); 
            
            if((oldAccountRec.HDFC_DASH_WhatsApp_Consent__pc != newAccountRec.HDFC_DASH_WhatsApp_Consent__pc) || (oldAccountRec.PersonMobilePhone != newAccountRec.PersonMobilePhone) || (oldAccountRec.HDFC_DASH_Mobile_Country_Code__pc != newAccountRec.HDFC_DASH_Mobile_Country_Code__pc))
            {
                if(String.isNotBlank(newAccountRec.PersonMobilePhone) && String.isNotBlank(newAccountRec.HDFC_DASH_Mobile_Country_Code__pc) && (newAccountRec.HDFC_DASH_WhatsApp_Consent__pc == HDFC_DASH_Constants.STRING_Y))
                {
                    listUpdateAccount.add(accRec);
                }
            }
        }
        return listUpdateAccount;
    } 
    /* 
Method Name :optinCallout
Description :This method would call Optin Callout.
*/
    public static void optinCallout(List<Account> listAccounts){
        for(Account accRec:listAccounts)
        {       
            String mobileNumber = accRec.HDFC_DASH_Mobile_Country_Code__pc + accRec.PersonMobilePhone;
            HDFC_DASH_OptinCallout optinCallout = new HDFC_DASH_OptinCallout(mobileNumber);
            system.enqueueJob(optinCallout);
        }
    }
    
}