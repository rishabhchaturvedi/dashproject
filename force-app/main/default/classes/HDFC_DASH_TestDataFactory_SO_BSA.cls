/* 
ClassName: HDFC_DASH_TestDataFactory_SO_BSA
DevelopedBy: Utkarsh Patidar
Date: 08 Nov 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for SO BSA Association object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_SO_BSA {
   /* Method Name: createBasicSoBsa
    parameters: ID applID
    Method Description: This method is to create test data for HDFC_DASH_SO_BSA_Selector. 
    */
    public static HDFC_DASH_SO_BSA_Association__c createBasicSoBsa(ID accID)
    {
        HDFC_DASH_SO_BSA_Association__c BSA = new HDFC_DASH_SO_BSA_Association__c();
        BSA.HDFC_DASH_BSA__c =  accID  ;    
        BSA.HDFC_DASH_Sales_Officer__c = UserInfo.getUserId();
        return BSA;        
    }
    
    /* Method Name: getBasicSoBsa
    parameters: ID applID
    Method Description: This method is to generate test data for BSA.
    */
    public static HDFC_DASH_SO_BSA_Association__c getBasicSoBsa(ID accID)
    {
        HDFC_DASH_SO_BSA_Association__c bsa = HDFC_DASH_TestDataFactory_SO_BSA.createBasicSoBsa(accID);
        Database.insert(BSA);
        return bsa;
    } 
}