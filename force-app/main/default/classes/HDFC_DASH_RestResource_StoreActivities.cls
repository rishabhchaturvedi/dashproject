@RestResource(urlMapping='/hdfc/storeactivities/V1.0')
/*
Author: Utkarsh Patidar
Date: 27 Sep 2021
Company: Accenture
Class Name: HDFC_DASH_RestResource_StoreActivities
*/
global with sharing class HDFC_DASH_RestResource_StoreActivities {

/*
Method:storeActivities()
Description: This method will store the list of Activities
*/
    @HttpPost
    global static void storeActivities() 
    {        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders = new Map<string,String>();
        Exception exp;
        HDFC_DASH_APIResponse_StoreActivities apiResponse = new HDFC_DASH_APIResponse_StoreActivities();
        
        try
        { 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestSer_StoreActivities.processActivities(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                            HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                            + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_STOREACTIVITIES, 
                                                        HDFC_DASH_Constants.HANDLE_STOREACTIVITIES, 
                                                        HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_STOREACTIVITIES, 
                                            request.requestBody.tostring(), (res.responseBody)?.tostring(), 
                                            HDFC_DASH_Constants.METHOD_TYPE_POST, HDFC_DASH_Constants.HANDLE_STOREACTIVITIES,
                                            exp, request.headers);        

            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                        res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
}