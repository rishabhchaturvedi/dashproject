/**
className: HDFC_DASH_ApplFinanlInfo_SelectorTest
DevelopedBy: Sai Suman
Date: 28 Sep 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_ApplFinancialInfo_Selector class.
**/
@isTest(SeeAllData = false) 
public with sharing class HDFC_DASH_ApplFinanlInfo_SelectorTest {
      //Getting the object records
      private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
      private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
      private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
      private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
      private static HDFC_DASH_Applicant_Details__c appDetails =
                      HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
      private static HDFC_DASH_Applicant_Financial_Info__c applicantFinancialInfo =
      HDFC_DASH_TestDataFactory_ApplFinanInfo.getBasicApplicantFinancialInfo(appDetails.Id);
      /*
      Method Name :testgetApplicantFinancialInfoBasedOnId
      Description :This method will test the method getAppFinInfoBasedOnId().
      */
      /*static testmethod void testgetApplicantFinancialInfoBasedOnId(){
          System.runAs(sysAdmin){
              HDFC_DASH_Applicant_Financial_Info__c resApplicantFinancialInfoRec = null;
              List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
              Test.startTest();
              resApplicantFinancialInfoRec = HDFC_DASH_ApplFinancialInfo_Selector.getAppFinInfoBasedOnId(fieldList,applicantFinancialInfo.Id);
              Test.stopTest();
              system.assertNotEquals( null, resApplicantFinancialInfoRec);
              system.assertEquals(applicantFinancialInfo.Id,resApplicantFinancialInfoRec.Id,'SUCCESS');
          }
      }*/
  
      /*
      Method Name :testgetApplicantFinancialInfoBasedOnIds
      Description :This method will test the method getAppFinInfoBasedOnIds().
     */
      /*static testmethod void testgetApplicantFinancialInfoBasedOnIds(){
          List<HDFC_DASH_Applicant_Financial_Info__c>  listApplicantFinancialInfo = null;
          System.runAs(sysAdmin){
              List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
              List<Id> listApplicantFinancialInfoIds = new List<Id>{applicantFinancialInfo.Id};
              Test.startTest();
              listApplicantFinancialInfo = HDFC_DASH_ApplFinancialInfo_Selector.getAppFinInfoBasedOnIds(fieldList ,listApplicantFinancialInfoIds);
              Test.stopTest();
          }
          system.assertNotEquals( null, listApplicantFinancialInfo);
          system.assertEquals(1,listApplicantFinancialInfo.size(),'SUCCESS');
      }*/
       /*
      Method Name :testgetApplFinanInfoBasedOnAppDetailIds
      Description :This method will test the method getApplFinInfoBasedOnAppDetailIds().
      */
     /*  static testmethod void testgetApplFinanInfoBasedOnAppDetailIds(){
          List<HDFC_DASH_Applicant_Financial_Info__c> resApplFinInfoDetails = null;
          List<Id> ApplicantFinancialInfoList = new List<Id>{appDetails.id} ;
          System.runAs(sysAdmin){
              List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                                              HDFC_DASH_Constants.STRING_Name};
              Test.startTest();
              resApplFinInfoDetails = HDFC_DASH_ApplFinancialInfo_Selector.getApplFinInfoBasedOnAppDetailIds(fieldList,ApplicantFinancialInfoList);
              Test.stopTest();
          }
          system.assertNotEquals( null, resApplFinInfoDetails);
           system.assertEquals(1, resApplFinInfoDetails.size(),'SUCCESS');
      }*/
       /*
      Method Name :testgetApplicantFinancialInfoDetails
      Description :This method will test the method get Applicant Financial Detail().
      */
      static testmethod void testgetApplicantFinancialInfoDetails(){
          List<HDFC_DASH_Applicant_Financial_Info__c>  listApplicantFinancialInfoDetailRec = null;
          System.runAs(sysAdmin){
              List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
              Test.startTest();
              listApplicantFinancialInfoDetailRec = HDFC_DASH_ApplFinancialInfo_Selector.getApplicantFinancialInfo(
                                  new List<String> {applicantFinancialInfo.Id},
                                  new List<String>{HDFC_DASH_Constants.STRING_ID},
                                  HDFC_DASH_Constants.ID_STRING);
              Test.stopTest();
          }
          system.assertNotEquals( null, listApplicantFinancialInfoDetailRec);
          system.assertEquals(1,listApplicantFinancialInfoDetailRec.size(),'SUCCESS');
      }
}