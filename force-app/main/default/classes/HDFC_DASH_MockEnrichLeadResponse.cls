/**
className: HDFC_DASH_MockEnrichLeadResponse
Author: Punam Marbate
Date: 14 July 2021
Company: Accenture
Class Description: This is mock class used for creating MockEnrichLeadResponse
**/ 
@isTest(SeeAllData = false)
global with sharing class HDFC_DASH_MockEnrichLeadResponse implements HttpCalloutMock{
    
    /* 
    Method Name :respond
    Description :This method would create a fake response for EnrichLeadLMS callout.
    */
    global HTTPResponse respond(HTTPRequest req) 
    {
        HttpResponse res = new HttpResponse();
        // User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        // System.runAs(sysAdmin)
        // {
            // Create a mock response            
            res.setHeader('Content-Type', 'application/json');            
            res.setBody('{"return_cd":0,"return_msg":"Application Information has been Updated","lead_no":101788353,"lead_Details":null,"appln_form":null}');
            res.setStatusCode(200);
            System.assertNotEquals(null,req);           
        //}
        return res;
    }
}