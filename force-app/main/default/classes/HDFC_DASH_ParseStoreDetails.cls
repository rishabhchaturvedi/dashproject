/*
Author: Anmol Srivastava
Class: HDFC_DASH_ParseDMSDocument
Description: Parser Class for Store Details API
*/
public inherited sharing class HDFC_DASH_ParseStoreDetails {

	public StoreDetails  storeDetails ;
    /*
Class: StoreDetails
Description: Class to parse Details from request
*/  
	public without sharing class StoreDetails  {
		public String sfApplicationId;
		public String sfApplicationNo;
		public String cibilTransunion;
        public boolean cibilTransunionTechnicalFailure;
		public Decimal leadStageNumber;
		public String leadStageDescription;
		public String postPaymentspotOfferEligible;          
        public boolean postPaymentSpotOfferEligibilityTechnicalFailure;
	}
	/*
        Method: parse
        Description: Method to Deserialize store details API request body
    */
    public static HDFC_DASH_ParseStoreDetails parse(RestRequest request) {
        String requestBody = request.requestBody.toString();
        return (HDFC_DASH_ParseStoreDetails) System.JSON.deserialize(requestBody, HDFC_DASH_ParseStoreDetails.class);
    }
}