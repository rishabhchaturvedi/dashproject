/*
	Author: Anisha Arumugam
	Class: HDFC_DASH_APIResponse_QuoteInfo
	Description: Apex Class for handling success message for Quote Info API
*/
public inherited sharing class HDFC_DASH_APIResponse_QuoteInfo {
    public String successMessage;          
}