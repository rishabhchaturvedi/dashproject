/*
Author: Anisha Arumugam
Class: HDFC_DASH_InboundRestService_GetAppList
Description: Service Class for getAppList Info API
*/
public with sharing class HDFC_DASH_InboundRestService_GetAppList
{
    Public static string multiConditions = '';
    /* 
Method:getApplicationList
Description: Method to get the list of Applications
*/
    public static HDFC_DASH_APIResponse_GetAppList getApplicationList(RestRequest request)
    {      
        String customerId = RestContext.request.params.get(HDFC_DASH_Constants.CUSTOMER_ID);
        String param_TLID = RestContext.request.params.get(HDFC_DASH_Constants.STRING_TLID);
        String param_SOID = RestContext.request.params.get(HDFC_DASH_Constants.STRING_SOID);
        String param_BSAID = RestContext.request.params.get(HDFC_DASH_Constants.STRING_BSAID);
        String param_CustomerNumber = RestContext.request.params.get(HDFC_DASH_Constants.CUSTOMER_NUMBER);

        //get Application List
        HDFC_DASH_APIResponse_GetAppList apiRes = getApplications(customerId,param_CustomerNumber,param_TLID,param_SOID,param_BSAID);
        return apiRes;  
    }
    /* 
Method: getApplications
Description: Method to get the list of Applications associated with a customer
*/
    public static HDFC_DASH_APIResponse_GetAppList getApplications(String CustId,String param_CustomerNumber, String param_TLID, String param_SOID, String param_BSAID){        
        
        HDFC_DASH_APIResponse_GetAppList apiRes = new HDFC_DASH_APIResponse_GetAppList();
        List<HDFC_DASH_APIResponse_GetAppList.SfApplications> sfAppList =
            new List<HDFC_DASH_APIResponse_GetAppList.SfApplications>();
        List<string> listAppID = new List<string>();
        List<string> listSOId = new List<string>();
        String contactId;
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>(); 
        List<string> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber, HDFC_DASH_Constants.HDFC_DASH_File_Number, HDFC_DASH_Constants.Stage, 
            HDFC_DASH_Constants.SO_REQUIRED_LOAN_AMOUNT, HDFC_DASH_Constants.LOAN_TYPE_CODE, HDFC_DASH_Constants.STATUS, HDFC_DASH_Constants.CREATEDDATE, HDFC_DASH_Constants.OwnerId,HDFC_DASH_Constants.HDFC_DASH_RERA_Code,
            HDFC_DASH_Constants.HDFC_DASH_RERA_Name,HDFC_DASH_Constants.HDFC_DASH_Prop_Tower_Wing_Name,HDFC_DASH_Constants.HDFC_DASH_Property_Village_Town_City,HDFC_DASH_Constants.HDFC_DASH_Property_Sub_District,
            HDFC_DASH_Constants.HDFC_DASH_Property_State_Code,HDFC_DASH_Constants.HDFC_DASH_Property_State,HDFC_DASH_Constants.HDFC_DASH_Property_Post_Office_Name,HDFC_DASH_Constants.HDFC_DASH_Property_Postal_Code,
            HDFC_DASH_Constants.HDFC_DASH_Prop_Planned_City_Code,HDFC_DASH_Constants.HDFC_DASH_Prop_Planned_City,HDFC_DASH_Constants.HDFC_DASH_Prop_Location_Decided,HDFC_DASH_Constants.HDFC_DASH_Property_Locality,
            HDFC_DASH_Constants.HDFC_DASH_Property_Landmark,HDFC_DASH_Constants.HDFC_DASH_Property_House_Identifier,HDFC_DASH_Constants.HDFC_DASH_Property_District,HDFC_DASH_Constants.HDFC_DASH_Property_Country_Code,
            HDFC_DASH_Constants.HDFC_DASH_Property_Country,HDFC_DASH_Constants.HDFC_DASH_Property_City_Code,HDFC_DASH_Constants.HDFC_DASH_Property_City,HDFC_DASH_Constants.HDFC_DASH_Loan_Account_Number,
            HDFC_DASH_Constants.HDFC_DASH_Remaining_Term,HDFC_DASH_Constants.HDFC_DASH_Receipt_Date,HDFC_DASH_Constants.HDFC_DASH_Loan_EMI,HDFC_DASH_Constants.HDFC_DASH_LMS_Agency_Code};  
                List<string> listOfFields_AppDet = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.USER_CONSENT_FOR_CIBIL, HDFC_DASH_Constants.HDFC_DASH_Application};  
                    // Map<String,String> fields_Param_AppDet = new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Contact =>
                    //   HDFC_DASH_Constants.STRING_EQUALTO +HDFC_DASH_Constants.STRING_QUOTE + CustId +
                    //   HDFC_DASH_Constants.STRING_QUOTE,HDFC_DASH_Constants.STRING_AND +
                    //  HDFC_DASH_Constants.USER_CONSENT_FOR_CIBIL => HDFC_DASH_Constants.STRING_EQUALTO +
                    //  HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_YES+HDFC_DASH_Constants.STRING_QUOTE};
                    List<Opportunity> listAppBasedonCustId = new List<Opportunity>();
        Map<string,string> mapCondition = new Map<string,string>();	
        
        if(String.isNotBlank(param_TLID) || String.isNotBlank(param_SOID) || String.isNotBlank(param_BSAID) || String.isNotBlank(CustId) || String.isNotBlank(param_CustomerNumber))
        {   
            if(String.isNotBlank(CustId) || String.isNotBlank(param_CustomerNumber))
            {
                if((String.isNotBlank(CustId) && String.isBlank(param_CustomerNumber)) || (String.isNotBlank(param_CustomerNumber) && String.isBlank(CustId)))
                {
                    if(String.isNotBlank(CustId))
                    {
                        contactId = CustId;
                    }
                    else if(String.isNotBlank(param_CustomerNumber))
                    {
                        List<String> listCustNumber = new List<String>{param_CustomerNumber};
                            List<Contact> listCon = HDFC_DASH_Contact_Selector.getContacts(listCustNumber, new List<String>{HDFC_DASH_Constants.STRING_ID,HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number}, HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number);
                        
                        if(!listCon.isEmpty())
                        {
                            contactId = listCon[HDFC_DASH_Constants.INT_ZERO].Id;   
                        }
                        else
                        {
                            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_Error_Customer_NotExist); 
                        }
                    }
                    
                    Map<String,String> fields_Param_AppDet = new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Contact =>
                        HDFC_DASH_Constants.STRING_EQUALTO +HDFC_DASH_Constants.STRING_QUOTE + contactId +
                        HDFC_DASH_Constants.STRING_QUOTE,HDFC_DASH_Constants.STRING_AND +
                        HDFC_DASH_Constants.USER_CONSENT_FOR_CIBIL => HDFC_DASH_Constants.STRING_EQUALTO +
                        HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_YES+HDFC_DASH_Constants.STRING_QUOTE};
                            
                            listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(listOfFields_AppDet,fields_Param_AppDet);
                    
                    for(HDFC_DASH_Applicant_Details__c appDetail : listAppDetails){
                        listAppID.add(appDetail.HDFC_DASH_Application__c);
                    }
                    
                    if(!listAppID.isEmpty()){
                        multiConditions = HDFC_DASH_Constants.STRING_OPEN_BRACE;
                    }
                    apiRes.sfCustomerId = contactId;
                }
                else
                {
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_Error_Multiple_Params_Exp); 
                }
            }
            if(String.isNotBlank(param_TLID) || String.isNotBlank(param_SOID) || String.isNotBlank(param_BSAID))
            {
                //Any one of the 3 new params were there in the request
                if((String.isNotBlank(param_TLID) && String.isBlank(param_SOID) && String.isBlank(param_BSAID)) || (String.isNotBlank(param_SOID) && String.isBlank(param_TLID) && String.isBlank(param_BSAID)) ||(String.isNotBlank(param_BSAID) && String.isBlank(param_TLID) && String.isBlank(param_SOID)))
                {
                    if(String.isNotBlank(param_TLID))
                    {
                        List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_MANAGERID_FEDERATIONID};
                            //Condition - managerId.FederationIdentifier
                            List<User> listSOUsers = HDFC_DASH_User_Selector.getUser(new List<string>{param_TLID}, listUserFields, HDFC_DASH_Constants.STRING_MANAGERID_FEDERATIONID);
                        for(User userRec:listSOUsers){
                            listSOId.add(userRec.Id);
                        }
                        
                        //if listSOId not empty then 2 conditions else one condition
                        if(!listSOId.isEmpty()){
                            mapCondition.put(multiConditions + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.OwnerId, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listSOId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
                            //Condition - HDFC_DASH_Owner_Id__c
                            mapCondition.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.HDFC_DASH_Owner_Id, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_TLID + HDFC_DASH_Constants.STRING_QUOTE +HDFC_DASH_Constants.STRING_CLOSING_BRACE);
                        }
                        else{
                            //Condition - HDFC_DASH_Owner_Id__c
                            mapCondition.put(multiConditions + HDFC_DASH_Constants.HDFC_DASH_Owner_Id, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_TLID + HDFC_DASH_Constants.STRING_QUOTE);
                        }
                    }
                    else if(String.isNotBlank(param_SOID))
                    {
                        //Condition - HDFC_DASH_Owner_Id__c
                        mapCondition.put(multiConditions + HDFC_DASH_Constants.HDFC_DASH_Owner_Id, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_SOID + HDFC_DASH_Constants.STRING_QUOTE);
                    }
                    else if(String.isNotBlank(param_BSAID))
                    {
                        mapCondition.put(multiConditions + HDFC_DASH_Constants.HDFC_DASH_LMS_Agency_Code, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_BSAID + HDFC_DASH_Constants.STRING_QUOTE);
                    }
                }
                else
                {
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_Error_Multiple_Params_Exp); 
                }
            }
            
            //Adding list of Applications associated with CustomerID/CustomerNumber
            if(!listAppID.isEmpty())
            {
                if(!mapCondition.isEmpty())
                {
                    mapCondition.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAppID, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE + HDFC_DASH_Constants.STRING_CLOSING_BRACE);	
                }
                else
                {
                    mapCondition.put(HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listAppID, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);	
                }
            }
            
            if(!mapCondition.isEmpty())
            {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_File_First_Push_To_ILPS, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.BOOLEAN_FALSE );
                List<Opportunity> listApp = HDFC_DASH_Application_Selector.getApplicationBasedOnQuery(listOfFields_App, mapCondition);
                sfAppList = getApplicationsList(listApp,sfAppList);
                apiRes.sfApplications = sfAppList;
            }
        }
        else{
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_PARAMS_EXP); 
        }
        return apiRes;     
    }
    /* 
Method: getApplicationsList
Description: Method to get the list of Applications Based on Params
*/
    public static List<HDFC_DASH_APIResponse_GetAppList.SfApplications> getApplicationsList(List<Opportunity> listApp,List<HDFC_DASH_APIResponse_GetAppList.SfApplications> sfAppList){
        
        for(Opportunity objApplication : listApp){ 
            HDFC_DASH_APIResponse_GetAppList.SfApplications sfApp = new HDFC_DASH_APIResponse_GetAppList.SfApplications();
            HDFC_DASH_APIResponse_GetAppList.PropertyDetails propertyDet =  new HDFC_DASH_APIResponse_GetAppList.PropertyDetails();
            sfApp.sfApplicationID = objApplication?.Id;
            sfApp.sfApplicationNumber = objApplication?.HDFC_DASH_ApplicationNumber__c;
            sfApp.ilpsFileNo = objApplication?.HDFC_DASH_File_Number__c;                       
            sfApp.requiredLoanAmount = objApplication?.HDFC_DASH_SO_Required_Loan_Amount__c;  
            sfApp.stage = objApplication?.StageName;  
            sfApp.loanType = objApplication?.HDFC_DASH_Loan_Type_Code__c;  
            sfApp.status = objApplication?.HDFC_DASH_Status__c ;  
            sfApp.applicationDate = objApplication?.CreatedDate.Date();
            sfApp.loanAccountNo = objApplication?.HDFC_DASH_Loan_Account_Number__c;
            sfApp.remainingTerm = objApplication?.HDFC_DASH_Remaining_Term__c;
            sfApp.receiptDate = objApplication?.HDFC_DASH_Receipt_Date__c;
            sfApp.loanEMI = objApplication?.HDFC_DASH_Loan_EMI__c;  
            propertyDet.reraCode  = objApplication?.HDFC_DASH_RERA_Code__c;
            propertyDet.reraName  = objApplication?.HDFC_DASH_RERA_Name__c;
            propertyDet.towerOrWingName  = objApplication?.HDFC_DASH_Prop_Tower_Wing_Name__c;
            propertyDet.propertyVillageTownCity = objApplication?.HDFC_DASH_Property_Village_Town_City__c;
            propertyDet.propertySubDistrict = objApplication?.HDFC_DASH_Property_Sub_District__c;
            propertyDet.propertyStateCode = objApplication?.HDFC_DASH_Property_State_Code__c;
            propertyDet.propertyState   = objApplication?.HDFC_DASH_Property_State__c;
            propertyDet.propertyPostOfficeName  = objApplication?.HDFC_DASH_Property_Post_Office_Name__c;
            propertyDet.propertyPostalCode  = objApplication?.HDFC_DASH_Property_Postal_Code__c;
            propertyDet.propertyPlannedCityCode  = objApplication?.HDFC_DASH_Prop_Planned_City_Code__c;
            propertyDet.propertyPlannedCity  = objApplication?.HDFC_DASH_Prop_Planned_City__c;
            propertyDet.propertyLocationDecided  = objApplication?.HDFC_DASH_Prop_Location_Decided__c;
            propertyDet.propertyLocality = objApplication?.HDFC_DASH_Property_Locality__c;
            propertyDet.propertyLandmark = objApplication?.HDFC_DASH_Property_Landmark__c;
            propertyDet.propertyHouseIdentifier = objApplication?.HDFC_DASH_Property_House_Identifier__c;
            propertyDet.propertyDistrict    = objApplication?.HDFC_DASH_Property_District__c;
            propertyDet.propertyCountryCode = objApplication?.HDFC_DASH_Property_Country_Code__c;
            propertyDet.propertyCountry = objApplication?.HDFC_DASH_Property_Country__c;
            propertyDet.propertyCityCode = objApplication?.HDFC_DASH_Property_City_Code__c;
            propertyDet.propertyCity = objApplication?.HDFC_DASH_Property_City__c;
            sfApp.PropertyDetails = propertyDet;
            sfAppList.add(sfApp);
        } 
        return sfAppList;
    }
}