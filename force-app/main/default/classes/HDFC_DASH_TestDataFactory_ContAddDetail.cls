/* 
ClassName: HDFC_DASH_TestDataFactory_ContAddDetail
DevelopedBy: Punam Marbate
Date: 13 Aug 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Contact Address Detail object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_ContAddDetail {
    public static final string NAME = 'Name';
    public static final string ADDRESS_CHANGED = 'Yes';
    /* Method Name: createBasicContAddressDetail
    parameters: ID contactID
    Method Description: This method is to create test data for HDFC_DASH_Contact_Address_Details__c.
    Pass fields of HDFC_DASH_Contact_Address_Details__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Contact_Address_Details__c or inserted HDFC_DASH_Contact_Address_Details__c id. 
    */
    public static HDFC_DASH_Contact_Address_Details__c createBasicContAddressDetail(ID contactID)
    {
        HDFC_DASH_Contact_Address_Details__c conAddressDetail = new HDFC_DASH_Contact_Address_Details__c();
            
        //conAddressDetail.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        conAddressDetail.HDFC_DASH_Contact__c =  contactID  ; 
        conAddressDetail.HDFC_DASH_Address_Changed__c = ADDRESS_CHANGED; 
        return conAddressDetail;        
    }
    
    /*Method Name: getBasicContAddressDetail
    parameters: ID aplDetailID
    Method Description: This method is to generate test data for Contact Address Detail.
    */
    public static HDFC_DASH_Contact_Address_Details__c getBasicContAddressDetail(ID contactID)
    {
        HDFC_DASH_Contact_Address_Details__c conAddressDetail = HDFC_DASH_TestDataFactory_ContAddDetail.createBasicContAddressDetail(contactID);
        if(Schema.sObjectType.HDFC_DASH_Contact_Address_Details__c.isCreateable()){
            Database.insert(conAddressDetail);
        }
        return conAddressDetail;
    }
    
    /* Method Name: createCadDetailList
    parameters: Integer numberofCadDet
    Class Description: This method is to create a list of test data for Applicant bank detail
                        by calling a createCadDetailList method.
    */
    public static List<HDFC_DASH_Contact_Address_Details__c> createCadDetailList(Integer numberofCadDet,ID conID)
    {
        list<HDFC_DASH_Contact_Address_Details__c> cadDetlist = new list<HDFC_DASH_Contact_Address_Details__c>();
        for(integer i=0;i<numberofCadDet; i++){       
            cadDetlist.add(HDFC_DASH_TestDataFactory_ContAddDetail.createBasicContAddressDetail(conID));
        }
        return cadDetlist;
    }
    
    /* Method Name: getBasicCadDetail
    parameters: Integer numberofCadRec
    Class Description: This method is to generate a list of test data for  Contact Address detail.
    */
    public static List<HDFC_DASH_Contact_Address_Details__c> getBasicCadDetail(Integer numberofCadRec,ID conID) 
    {
        list<HDFC_DASH_Contact_Address_Details__c> cadlist = HDFC_DASH_TestDataFactory_ContAddDetail.createCadDetailList(numberofCadRec,conID);
        if(Schema.sObjectType.HDFC_DASH_Contact_Address_Details__c.isCreateable()){
            Database.insert(cadlist);
        }
        return cadlist;
    }
    
    
}