/**
* 	className: HDFC_DASH_InfoValTriggerOpsHelper (InfoValidation)
DevelopedBy: Tejeswari
Date: 10 November 2021
Company: Accenture
Class Description: This class would acts as helper class for Info Validation Trigger and contains the methods for before and after insert Scenarios.
**/
public with sharing class HDFC_DASH_InfoValTriggerOpsHelper {
    /* 
Method Name :changeInfoValBeforeInsert
Description :This method would change the Latest field of Info Validation records before insert.
*/ 
    public static void changeInfoValBeforeInsert(List<HDFC_DASH_Info_Validation__c > infoValList)
    {
        for(HDFC_DASH_Info_Validation__c infoValRec :infoValList)
        {
            system.debug('***Info Val Before Execute 1');
            infoValRec.HDFC_DASH_Latest__c  = HDFC_DASH_Constants.STRING_YES;
        }
    }
    
    /* 
Method Name :filterInfoValforLatestFlagSenario
Description :This method would filter Info Validation for latest flag.
*/
    public static List<HDFC_DASH_Info_Validation__c> filterInfoValforLatestFlagSenario()
    {
        List<HDFC_DASH_Info_Validation__c> listInfoVal = Trigger.new;
        List<String> listKey = new List<String>();
        
        for(HDFC_DASH_Info_Validation__c infoValRec: listInfoVal)
        { 
            system.debug('***Info Val After Filter 1');
            if(infoValRec.HDFC_DASH_Latest_Key__c != NULL && infoValRec.HDFC_DASH_SF_Triggered__c == HDFC_DASH_Constants.BOOLEAN_FALSE){
                system.debug('***Info Val After Filter 2');
                listKey.add(infoValRec.HDFC_DASH_Latest_Key__c);
            }
        }
        
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_LATEST, HDFC_DASH_Constants.HDFC_DASH_Latest_Key, HDFC_DASH_Constants.HDFC_DASH_Account_Holder_Name};
            List<HDFC_DASH_Info_Validation__c> listOfInfoVal = HDFC_DASH_AppInfoValidation_Selector.getAppInfoVal(listKey,fieldList,HDFC_DASH_Constants.HDFC_DASH_Latest_Key);
        system.debug('***Info Val After Filter 3 Size=> ' + listOfInfoVal.size());
        return listOfInfoVal;
    }
    
    /* 
Method Name :updateInfoValRec
Description :This method would update the Latest Flag for Info Validation records.
*/
    public static void updateInfoValRec(List<HDFC_DASH_Info_Validation__c> listInfoVal)
    {
        system.debug('***Info Val After Execute 1');
        map<id, HDFC_DASH_Info_Validation__c> mapUpdateInfoVal = new map<id, HDFC_DASH_Info_Validation__c>();
        
        for(HDFC_DASH_Info_Validation__c objInfoVal : listInfoVal)
        {
            system.debug('***Info Val After Execute 2');
            if(!Trigger.Newmap.keyset().contains(objInfoVal.id))
            {
                system.debug('***Info Val After Execute 3');
                objInfoVal.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_NO;	
                mapUpdateInfoVal.put(objInfoVal.id,objInfoVal);
            }
        }
        
        if(!mapUpdateInfoVal.isEmpty())
        {
            if(Schema.sObjectType.HDFC_DASH_Info_Validation__c.isUpdateable()){
                Database.update(mapUpdateInfoVal.values());
            }
        }
    }
    /* 
Method Name :filterInfoValforConDocDelAndDDCallOut
Description :This method would filter Info Validation for Content Document Deletion and for Doc Details ILPS Callout.
*/
    public static List<HDFC_DASH_Info_Validation__c> filterInfoValforConDocDelAndDDCallOut()
    {
        List<HDFC_DASH_Info_Validation__c> listInfoVal = Trigger.new;
        List<HDFC_DASH_Info_Validation__c> listUpdateInfoVal = new List<HDFC_DASH_Info_Validation__c>();
        
        for(HDFC_DASH_Info_Validation__c infoValRec: listInfoVal){
            HDFC_DASH_Info_Validation__c oldInfoval = (HDFC_DASH_Info_Validation__c)Trigger.oldMap.get(infoValRec.Id);
            if(String.isBlank(oldInfoval.HDFC_DASH_Storage_Identifier__c) && String.isNotBlank(infoValRec.HDFC_DASH_Storage_Identifier__c))
            {
                listUpdateInfoVal.add(infoValRec);
            }
        }
        return listUpdateInfoVal;
    }
    /* 
Method Name :delOfconDocument
Description :This method would delete Content Document. 
*/
    public static void delOfconDocument(List<HDFC_DASH_Info_Validation__c> listInfoVal)
    {
        List<String> listConDocId = new List<String>();
        List<String> listInfoValId = new List<String>();
         List<ContentDocumentLink> listConDocLink = new List<ContentDocumentLink>();
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.ContentDocumentId,HDFC_DASH_Constants.LinkedEntityId};
            
            for(HDFC_DASH_Info_Validation__c infoValRec: listInfoVal){
                listInfoValId.add(infoValRec.id);
            }
        
        if(!listInfoValId.isEmpty()){
             listConDocLink = HDFC_DASH_ContentDocument_Selector.getContentDocumentLink(listInfoValId, fieldList, HDFC_DASH_Constants.LinkedEntityId);   
        }
        for(ContentDocumentLink conDocLink: listConDocLink){
            listConDocId.add(conDocLink.ContentDocumentId);
        }
        
        
        if(!listConDocId.isEmpty()){
            if(Schema.sObjectType.ContentDocument.isDeletable()){
                Database.delete(listConDocId);
            }
        }
    }
    
    /* 
Method Name :docDetailCallout
Description :This method would call Document Details Callout.
*/
    public static void docDetailCallout(List<HDFC_DASH_Info_Validation__c> listInfoVal){
        for(HDFC_DASH_Info_Validation__c infoValRec: listInfoVal){
            if(String.isNotBlank(infoValRec.HDFC_DASH_File_Number__c) && infoValRec.HDFC_DASH_Sync_Document_To_ILPS__c == HDFC_DASH_Constants.BOOLEAN_TRUE)
            {
                HDFC_DASH_DocDetails_ILPSCallout ddILPSCallout = new HDFC_DASH_DocDetails_ILPSCallout(infoValRec.id,HDFC_DASH_Constants.BOOLEAN_TRUE);
                system.enqueueJob(ddILPSCallout);
            }
        }
    }
}