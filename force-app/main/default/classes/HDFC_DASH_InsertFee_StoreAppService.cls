/**
className: HDFC_DASH_InsertFee_StoreAppService
DevelopedBy: Janani Mohankumar
Date: 30 Aug 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the Fee details.
**/
public class HDFC_DASH_InsertFee_StoreAppService {
    /*
    Method Name :mapFeeDetails
    Parameters  :Id applicationID, HDFC_DASH_ParseApplication.fee parseFee
    Description :This method would map all the fee details node for storeApp API and sends us the fee detail record.
    */
    public static HDFC_DASH_Fee_Detail__c mapFeeDetails(Id applicationID, HDFC_DASH_ParseApplication.fee parseFee)
    {
       
            //system.debug('Inside feeDetailObj--');
        	HDFC_DASH_Fee_Detail__c feeDetailObj = new HDFC_DASH_Fee_Detail__c();
        	feeDetailObj.Id = parseFee?.sfFeeDetailId;
            if(applicationId != null)
            {
                feeDetailObj.HDFC_DASH_Application__c = applicationId;
            }
            else if(parseFee?.sfApporAddLoanId != null){
                feeDetailObj.HDFC_DASH_Application__c = parseFee?.sfApporAddLoanId;
            }
            feeDetailObj.HDFC_DASH_Fee_Construct_feeType__c = parseFee?.feeType;
            feeDetailObj.HDFC_DASH_Fee_Construct_Detail_Type__c = parseFee?.feeDetailType;
            feeDetailObj.HDFC_DASH_Fee_Construct_pfExclTax__c = parseFee?.PfExclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_pfInclTax__c = parseFee?.feeosInclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_stampDuty__c = parseFee?.stampDuty;
            feeDetailObj.HDFC_DASH_Fee_Construct_sfExclTax__c = parseFee?.sfExclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_sfInclTax__c = parseFee?.sfInclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_minFeesExclTax__c = parseFee?.minFeesExclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_minFeesInclTax__c = parseFee?.minFeesInclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_feeosExclTax__c = parseFee?.feeosExclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_feeosInclTax__c = parseFee?.feeosInclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_subventAmt__c = parseFee?.subventAmt;
            feeDetailObj.HDFC_DASH_Fee_Construct_payableLater__c = parseFee?.payableLater;
            feeDetailObj.HDFC_DASH_Fee_Construct_totFeeExclTax__c = parseFee?.totFeeExclTax;
            feeDetailObj.HDFC_DASH_Fee_Construct_totFeeInclTax__c = parseFee?.totFeeInclTax;
            feeDetailObj.HDFC_DASH_ILPS_Fee_Details_Id__c = parseFee?.ILPSFeeDetailsId;
            //feeDetailObj.HDFC_DASH_TF_Fee_Construct__c = parseFee?.feeConstructTechnicalFailure;
            //system.debug('feeDetailObj--'+feeDetailObj);
        return feeDetailObj;
    }
}