/**
className: HDFC_DASH_AggregateResultIterator
DevelopedBy: Anisha Arumugam
Date: 27 January 2022
Company: Accenture 
Class Description: This class would construct the Query for Batch notification.
**/
global class HDFC_DASH_AggregateResultIterator implements Iterator<AggregateResult> {
  AggregateResult [] results {get;set;}
  // tracks which result item is returned
  Integer index {get; set;} 
      
     /**
ConstructorName : HDFC_DASH_AggregateResultIterator
* Parameters : String query
* Description : It is a constructor for class HDFC_DASH_AggregateResultIterator.
*/ 
  global HDFC_DASH_AggregateResultIterator(String query) {
    index = 0;
    results = Database.query(query);            
  } 
  /* 
Method Name :hasNext
Description :This method will help for generating the Query
*/ 
  global boolean hasNext(){ 
    return results != null && !results.isEmpty() && index < results.size(); 
  }    

      /* 
Method Name :next
Description :This method will help for generating the Query
*/ 
  global AggregateResult next(){        
    return results[index++];            
  }       
}