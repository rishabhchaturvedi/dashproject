public class HDFC_DASH_CaseUpdateTriggerOps {
    
    public class caseUpdateMode implements HDFC_DASH_TriggerOps{
        
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.proc_CaseUpdateIsFirstRun;   
        }
        /* 
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            return Trigger.new; 
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Case_Update__e [] pexcList){
            system.debug('@@@@ pexcList '+pexcList.size());
            String oldRemarks = '';
            Boolean sendEmailMsg = true;
            List<Case> eceList = new List<Case>();
            Map<Id,Case> caseMap = new Map<Id,Case>();
            List<FeedItem> fdList = new List<FeedItem>();
            List<OrgWideEmailAddress> owea = new List<OrgWideEmailAddress>();
             String owdLabel;
            List<EmailMessage> emailMsgList = new List<EmailMessage>();
            List<Case> existCaseList = new List<Case>();
            
            try{
                existCaseList = [SELECT Id,ContactId,HDFC_DASH_Category__c,HDFC_DASH_Sub_Category__c, Contact.Email,Status,HDFC_DASH_Case_Type__c,OwnerId,
                                 HDFC_DASH_Skip_AutoAssignment__c,HDFC_DASH_HDB_Number__c,HDFC_DASH_Deposit_Number__c,RecordType.Name,HDFC_DASH_ILPS_LAC__c,SuppliedEmail FROM 
                                 Case WHERE Id = :pexcList[0].Existing_CaseId__c];
                for(Case_Update__e ce :pexcList){
                    Case caseRec = new Case();
                    caseRec.Apex_Context__c = true; //Status Update validation
                    system.debug('@@@@ Sub_Mode__c '+ce.Sub_Mode__c+'  exist status '+existCaseList[0].Status);
                    if(ce.Sub_Mode__c =='CGRO'){                    
                        caseRec.Status =  'Statuary Escalation L1';
                        caseRec.Subject = ce.Subject__c;
                        String postBody = 'Mode and Sub-Mode were updated to higher priority '+ ce.Mode__c+'/'+ ce.Sub_Mode__c;
                        caseRec.HDFC_DASH_Mode__c = ce.Mode__c;
                        caseRec.HDFC_DASH_Sub_mode__c = ce.Sub_Mode__c;
                        caseRec.Statuary_Escalation_L1_Date__c =date.today();
                        if(caseRec.HDFC_DASH_Case_Type__c == 'Complaint'){
                            if(ce.Category__c != '') caseRec.HDFC_DASH_Category__c = ce.Category__c;
                            if(ce.Sub_Category__c != '') caseRec.HDFC_DASH_Sub_Category__c = ce.Sub_Category__c; 
                        }
                        system.debug('existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c..'+existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c+'...'+caseRec.HDFC_DASH_Skip_AutoAssignment__c +'existCaseList[0].HDFC_DASH_Deposit_Number__c '+existCaseList[0].HDFC_DASH_Deposit_Number__c);
                       system.debug('@@@@Label.Statuary_Escalation_L1_Owner '+Label.Statuary_Escalation_L1_Owner);
                        if(existCaseList[0].HDFC_DASH_Deposit_Number__c!=null) 
                           { 
                                system.debug('@@@@Label.Statuary_Escalation_L1_Owner '+Label.Statuary_Escalation_L1_Owner);
                              caseRec.OwnerId=Label.Statuary_Escalation_L1_Owner; 
                           }
                        if(existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c == true){
                            caseRec.HDFC_DASH_Skip_AutoAssignment__c = false;
                        }
                        FeedItem fd = new FeedItem(
                            Body = postBody,
                            ParentId = ce.Existing_CaseId__c    
                        );
                        fdList.add(fd);
                        sendEmailMsg = false;
                        system.debug('sendEmailMsg...'+sendEmailMsg);
                        //send email
                        
                    }
                    else if(ce.Sub_Mode__c =='MD Level 2- Website'&& ce.Mode__c == 'Website'){ //(existCaseList[0].Status=='Statuary Escalation L1' ) 
                        caseRec.Status = 'Statuary Escalation L2';
                        caseRec.Subject = ce.Subject__c;
                        String postBody = 'Mode and Sub-Mode were updated to higher priority '+ ce.Mode__c+'/'+ ce.Sub_Mode__c;
                        caseRec.HDFC_DASH_Mode__c = ce.Mode__c;
                        caseRec.HDFC_DASH_Sub_mode__c = ce.Sub_Mode__c;
                        if(caseRec.HDFC_DASH_Case_Type__c == 'Complaint'){
                            if(ce.Category__c != '') caseRec.HDFC_DASH_Category__c = ce.Category__c;
                            if(ce.Sub_Category__c != '') caseRec.HDFC_DASH_Sub_Category__c = ce.Sub_Category__c;
                        }
                        if(existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c == true){
                            caseRec.HDFC_DASH_Skip_AutoAssignment__c = false;
                        }
                        if(existCaseList[0].HDFC_DASH_Deposit_Number__c!=null) 
                           { 
                              caseRec.OwnerId= Label.Deposit_Statuary_Escalation_L2_Owner; 
                           }
                        system.debug('existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c..'+existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c+'...'+caseRec.HDFC_DASH_Skip_AutoAssignment__c);
                        FeedItem fd = new FeedItem(
                            Body = postBody,
                            ParentId = ce.Existing_CaseId__c    
                        );
                        fdList.add(fd);
                        
                         sendEmailMsg = false;
                        system.debug('sendEmailMsg...'+sendEmailMsg);
                    }
                    else if(HDFC_DASH_Constants.HIGHERPRIORITY.equals(ce.Reason__c) || HDFC_DASH_Constants.HIGHERPRIORITYREOPEN.equals(ce.Reason__c)){
                        if(HDFC_DASH_Constants.HIGHERPRIORITY.equals(ce.Reason__c)){ //&& !existCaseList[0].Status.contains(HDFC_DASH_Constants.ESCALATED) && !existCaseList[0].Status.contains('Escalation')){
                            caseRec.Status = HDFC_DASH_Constants.PENDING;
                            system.debug('existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c..'+existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c+'...'+caseRec.HDFC_DASH_Skip_AutoAssignment__c);
                            if(existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c == true){
                                caseRec.HDFC_DASH_Skip_AutoAssignment__c = false;
                            }
                        }
                        if(ce.Mode__c == 'Social Media'){
                            caseRec.Priority = ce.Social_Media_Priority__c;
                            CaseRec.HDFC_DASH_Social_media_link__c=ce.Social_Media_Link__c;
                        }
                        String postBody = 'Mode and Sub-Mode were updated to higher priority '+ ce.Mode__c+'/'+ ce.Sub_Mode__c;
                        caseRec.HDFC_DASH_Mode__c = ce.Mode__c;
                        caseRec.HDFC_DASH_Sub_mode__c = ce.Sub_Mode__c;
                        if(caseRec.HDFC_DASH_Case_Type__c == 'Complaint'){
                            if(ce.Category__c != '') caseRec.HDFC_DASH_Category__c = ce.Category__c;
                            if(ce.Sub_Category__c != '') caseRec.HDFC_DASH_Sub_Category__c = ce.Sub_Category__c;
                        }
                        caseRec.Subject = ce.Subject__c;
                        //caseRec.Subject = 
                        FeedItem fd = new FeedItem(
                            Body = postBody,
                            ParentId = ce.Existing_CaseId__c    
                        );
                        fdList.add(fd);
                    }
                    if((HDFC_DASH_Constants.LOWEREQUALPRIORITY.equals(ce.Reason__c) || HDFC_DASH_Constants.EQUALPRIORITY.equals(ce.Reason__c)) 
                       && !existCaseList[0].Status.contains(HDFC_DASH_Constants.ESCALATED) && !existCaseList[0].Status.contains('Escalation')){
                           IF(existCaseList[0].HDFC_DASH_ILPS_LAC__c==true && existCaseList[0].status=='Closed')
                           {
                               caseRec.Status='Re-Opened';
                           }
                           else
                           {
                           caseRec.Status = HDFC_DASH_Constants.PENDING;
                           }
                           if(ce.Mode__c == 'Social Media'){
                               caseRec.Priority = ce.Social_Media_Priority__c;
                               caseRec.HDFC_DASH_Social_media_link__c=ce.Social_Media_Link__c;
                           }
                           system.debug('existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c..'+existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c+'...'+caseRec.HDFC_DASH_Skip_AutoAssignment__c);
                           if(existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c == true){
                               caseRec.HDFC_DASH_Skip_AutoAssignment__c = false;
                           }
                           if(caseRec.Priority !='Influencer')
                           {
                               CaseRec.Priority=ce.Social_Media_Priority__c;
                           }
                           if(caseRec.Priority !='Critical')
                           {
                               CaseRec.Priority=ce.Social_Media_Priority__c;
                           }
                           String postBody = 'Interaction was tagged '+ ce.Category__c+'/'+ ce.Sub_Category__c;
                           FeedItem fd = new FeedItem(
                               Body = postBody,
                               ParentId = ce.Existing_CaseId__c    
                           );
                           fdList.add(fd);
                       }
                    if(HDFC_DASH_Constants.REOPENCASE.equals(ce.Reason__c) || HDFC_DASH_Constants.HIGHERPRIORITYREOPEN.equals(ce.Reason__c)
                       || HDFC_DASH_Constants.LOWEREQUALPRIORITYREOPEN.equals(ce.Reason__c) || HDFC_DASH_Constants.EQUALPRIORITYREOPEN.equals(ce.Reason__c)){
                           caseRec.Status = HDFC_DASH_Constants.REOPENED_STATUS;
                           
                           if(ce.Mode__c == 'Social Media'){
                               
                               caseRec.Priority = ce.Social_Media_Priority__c;
                               caseRec.HDFC_DASH_Social_media_link__c=ce.Social_Media_Link__c;
                           }
                           //oldRemarks = caseRec.HDFC_DASH_Remarks__c;
                           caseRec.HDFC_DASH_Remarks__c = '';
                           caseRec.HDFC_DASH_Social_media_link__c=ce.Social_Media_Link__c;
                           //caseRec.HDFC_DASH_Mode__c = ce.Mode__c;
                           //caseRec.HDFC_DASH_Sub_mode__c = ce.Sub_Mode__c;
                           String postBody = 'Interaction was tagged '+ ce.Category__c+'/'+ ce.Sub_Category__c;
                           FeedItem fd = new FeedItem(
                               Body = postBody,
                               ParentId = ce.Existing_CaseId__c    
                           );
                           fdList.add(fd);
                       }
                       
                        if(existCaseList[0].hdfc_dash_case_type__c != 'Complaint'){
                    if(ce.Category__c != '' && caseRec.HDFC_DASH_Category__c==null) 
                        caseRec.HDFC_DASH_Category__c = ce.Category__c;
                    if(ce.Sub_Category__c != '' && caseRec.HDFC_DASH_Sub_Category__c==null) 
                        caseRec.HDFC_DASH_Sub_Category__c = ce.Sub_Category__c;
                        }
                    
                    system.debug('skip auto assignment old... '+existCaseList[0].HDFC_DASH_Skip_AutoAssignment__c+'..new '+caseRec.HDFC_DASH_Skip_AutoAssignment__c);
                    
                    if(sendEmailMsg){
                    Messaging.SingleEmailMessage mail;
                    if(existCaseList[0].HDFC_DASH_Deposit_Number__c != null){
                        owdLabel = Label.DepositComplaint;
                         owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];
                    }else if(existCaseList[0].hdfc_dash_case_type__c == 'Complaint'){
                        owdLabel = Label.LoanComplaint;
                        owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];
                    }else if(existCaseList[0].hdfc_dash_case_type__c == 'Query'){
                        owdLabel = Label.LoanQuery;
                        owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];
                    }else if(existCaseList[0].hdfc_dash_case_type__c == 'Request'){
                        owdLabel = Label.LoanRequest;
                        owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];
                    }
                   // OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: 'uatcustomer.service@hdfc.com'];
                    Id emailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Validation_of_Unique_Case_Creation_1'].Id;
                    mail = new Messaging.SingleEmailMessage();
                    mail.setOrgWideEmailAddressId(owea[0].Id);
                    mail.setTemplateId(emailTemp);
                    mail.toaddresses = new String[] {existCaseList[0].Contact.Email};
                        mail.setTargetObjectId(existCaseList[0].ContactId);
                    mail.setWhatId(existCaseList[0].Id);
                    mail.setSaveAsActivity(false);
                    mail.setTreatTargetObjectAsRecipient(false);
                    try{
                        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {mail};
                           Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);  
                            }catch(Exception Ex){
                                system.debug('@@@@Error msg:- '+Ex.getMessage());
                            }
                    }
                    else if(sendEmailMsg == false){ //CGRO and MD Level 2- emails
                        Case c1 = new Case();
                        //c1.ContactEmail = existCaseList[0].Contact.Email;
                        c1.ContactId = existCaseList[0].ContactId;
                        c1.SuppliedEmail = existCaseList[0].SuppliedEmail;
                        c1.HDFC_DASH_Sub_mode__c = caseRec.HDFC_DASH_Sub_mode__c ;
                        c1.Id = existCaseList[0].Id;
                        sendemailforstatutoryescalation.sendemailtocustomer(new List<Case>{c1});                    
                    }
                    
                    
                    caseRec.Id = ce.Existing_CaseId__c ;
                    caseMap.put(caseRec.Id, caseRec);
                    eceList.add(caseRec);
                    
                    
                    //Deposits - Regulator and GOI assignment 
                    if(existCaseList[0].HDFC_DASH_Deposit_Number__c!=null){
                        if(caseRec.HDFC_DASH_Mode__c == 'Regulator' || caseRec.HDFC_DASH_Mode__c == 'Government of India'){
                            User u = [Select Id from User where UserRole.Name = 'Regulator/DC Role' limit 1];
                            if(u != null){
                                caseRec.OwnerId = U.Id;
                            }
                        }
                    } // Deposits- end
                    
                    
                   // if(existCaseList[0].RecordType.Name != 'Deposit - Complaint'){
                    EmailMessage emsg = new EmailMessage();try{
                        emsg.Bypass_Validation__c = true;
                        if(ce.FromAddress__c == '' && UserInfo.getUserEmail() != ''){
                            emsg.FromAddress = UserInfo.getUserEmail();  
                            //emsg.Bypass_Validation__c = true;
                            system.debug('emsg.FromAddress...');
                        }else{
                            emsg.FromAddress = ce.FromAddress__c;
                            //emsg.Bypass_Validation__c = true;
                            system.debug('emsg.FromAddress...');
                        }                          
                        
                        emsg.ToAddress = ce.ToAddress__c;
                        system.debug('emsg.ToAddress...');
                        emsg.Incoming = HDFC_DASH_Constants.BOOLEAN_TRUE;
                        if(ce.Mode__c != 'Email'){
                            emsg.Subject = '['+ce.Sub_Mode__c+']'+ce.Subject__c;
                        }else{
                            emsg.Subject = ce.Subject__c;
                        }
                        if(ce.Mode__c == 'Social Media'){                                                       
                            String str = 'Social Media Link: ';
                            emsg.TextBody = ''; //ce.Social_Media_Link__c;
                            if(caseRec.HDFC_DASH_Social_media_link__c=='' && ce.Social_Media_Link__c!='')
                            {
                                caseRec.HDFC_DASH_Social_media_link__c=ce.Social_Media_Link__c;
                            }
                            CaseRec.HDFC_DASH_Category__c=ce.Category__c;
                            CaseRec.HDFC_DASH_Sub_Category__c=ce.Sub_Category__c;
                            
                            system.debug('email body'+emsg.TextBody);  
                            
                        }else{
                            caseRec.HDFC_DASH_Social_media_link__c=caseRec.HDFC_DASH_Social_media_link__c;
                            if(ce.TextBody__c !=null)
                                //emsg.TextBody = caseRec.Description;
                                emsg.TextBody = ce.TextBody__c;
                                if(existCaseList[0].hdfc_dash_case_type__c != 'Complaint'){
                                    if(ce.Category__c !='' && CaseRec.HDFC_DASH_Category__c=='')
                            {
                                CaseRec.HDFC_DASH_Category__c=ce.Category__c; 
                            }
                            if(ce.Sub_Category__c !='' && CaseRec.HDFC_DASH_Sub_Category__c=='')
                            {
                                CaseRec.HDFC_DASH_Sub_Category__c=ce.Sub_Category__c;
                            }
                            if(ce.Category__c !='' && CaseRec.HDFC_DASH_Category__c!='')
                            {
                                //CaseRec.HDFC_DASH_Category__c=CaseRec.HDFC_DASH_Category__c; 
                                CaseRec.HDFC_DASH_Category__c=ce.Category__c;
                            }
                                }
                            
                            
                        } 
                        system.debug('Caserec'+CaseRec);
                        system.debug('old category'+CaseRec.HDFC_DASH_Category__c);
                        system.debug('new category'+ce.Category__c);
                        system.debug('old sub category'+CaseRec.HDFC_DASH_Sub_Category__c);
                        if(existCaseList[0].hdfc_dash_case_type__c != 'Complaint'){   
                            CaseRec.HDFC_DASH_Mode__c=ce.Mode__c;
                            CaseRec.HDFC_DASH_Sub_mode__c=ce.Sub_Mode__c;
                        } 
                        
                        system.debug('emsg.TextBody...'); 
                        emsg.ParentId = ce.Existing_CaseId__c;
                        
                        emailMsgList.add(emsg);
                    }
                    catch(Exception Ex){
                            system.debug('@@@@Error msg2:- '+Ex.getMessage());
                        }
                   // }
                }
                
                                
                Database.update(caseMap.values());
                if(emailMsgList.size()>0)
                {
                    Database.insert(emailMsgList);
                }
                if(fdList.size() > 0)
                    Database.insert(fdList); 
            }
            catch(Exception e){
                String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON 
                    + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                    + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
                HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.CASE_UPDATE_TRIGGER_OPS,
                                                            HDFC_DASH_Constants.CASE_EVENT,NULL,expDetails);
            }
            HDFC_DASH_TriggerOpsRecursionFlags.proc_CaseUpdateIsFirstRun = false;
        }
    }     
    
}