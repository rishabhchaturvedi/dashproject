/**className: HDFC_DASH_Account_SelectorTest
DevelopedBy: Punam Marbate
Date: 05 July 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Account_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_Account_SelectorTest {
    private static final string STRING_ID ='Id';
    private static final string STRING_RECORDTYPE='RecordTypeId';
    private static final string STRING_NAME='Name';
    private static final string SUCCESS='Success';

    /* Method Name:testGetAccountsBasedOnIds
    Description:This is the testmethod for getAccountsBasedOnIds
    */
    static testmethod void testGetAccountsBasedOnIds(){
        List<Account> resultAccount = new List<Account>() ;
        List<Account>  accountRecords = new List<Account>();
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        System.runAs(sysAdmin){
            accountRecords = HDFC_DASH_TestDataFactory_Accounts.getBasicAccountlist(2);
           
            Set<Id> resultIds =(new Map<Id,SObject>(accountRecords)).keySet();
            List<Id> accountIds = new List<Id>(resultIds);
            List<String> fieldList = new List<string>{STRING_ID,STRING_RECORDTYPE,STRING_NAME};
            Test.startTest();
            resultAccount =  HDFC_DASH_Account_Selector.getAccountsBasedOnIds(fieldList, accountIds);
            Test.stopTest();
        }
        system.assertNotEquals( null, resultAccount);
        system.assertNotEquals( null, accountRecords);
        system.assertEquals(2,resultAccount.size(),SUCCESS); 
    }
    
    
    /*Method Name:testGetAccountsBasedOnId
    Description:This is the testmethod for getAccountsBasedOnId
 	*/
    static testmethod void testGetAccountsBasedOnId(){
        Account resultAccount = null ;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Account accountRecord = new Account();
        System.runAs(sysAdmin){
            accountRecord = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
            
            List<String> fieldList = new List<string>{STRING_ID,STRING_RECORDTYPE,STRING_NAME};
        Test.startTest();
            resultAccount =  HDFC_DASH_Account_Selector.getAccountsBasedOnId(fieldList, accountRecord.Id);
        Test.stopTest();
        }
        system.assertNotEquals( null, resultAccount);
        system.assertNotEquals( null, accountRecord);
        system.assertEquals(accountRecord.Id,resultAccount.Id,SUCCESS); 
    }
    
    
     /*Method Name: testGetAccountsBasedOnQuery
    Description: This is the testmethod for getAccountsBasedOnQuery
    */
    static testmethod void testGetAccountsBasedOnQuery(){       
        List<Account> resultAccountList = null ;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Account accountRecord = new Account();
        System.runAs(sysAdmin){
            accountRecord = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
           
            List<String> fieldList = new List<string>{STRING_ID,STRING_RECORDTYPE,STRING_NAME};
             
            
              Map<String,String> condition= new Map<string,String>{STRING_ID =>
            HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
            accountRecord.Id+HDFC_DASH_Constants.STRING_QUOTE};
                
    	Test.startTest();
            resultAccountList =  HDFC_DASH_Account_Selector.getAccountsBasedOnQuery(fieldList, condition);
        Test.stopTest();
        }
        system.assertNotEquals( null, resultAccountList);
        system.assertNotEquals( null, accountRecord);
        system.assertEquals(accountRecord.Id,resultAccountList[0].Id,SUCCESS);      
    }
    

}