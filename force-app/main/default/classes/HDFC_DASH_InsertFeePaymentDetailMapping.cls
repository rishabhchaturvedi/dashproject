/**
className: HDFC_DASH_InsertFeePaymentDetailMapping
DevelopedBy: Ashita Jain
Date: 19 Nov 2021
Company: Accenture
Class Description: This class is has a methods for mapping Junction Object Fee Payment Allocation.
**/
public inherited sharing class HDFC_DASH_InsertFeePaymentDetailMapping {
    /*
Method Name :mapFeePaymentDetail
Parameters  :Id applicationID, HDFC_DASH_ParseApplication.applicationPaymentDetails parsePaymentDetails
Description :This method would map all the Fee Payment Allocation node for storeApp API and sends us the Fee Payment Allocation record.
*/
    public static List<HDFC_DASH_Fee_Payment_Allocation__c> mapFeePaymentDetail(Map<Id,HDFC_DASH_Application_Payment_Details__c> mapApplFeeIds)
    {	system.debug('mapApplFeeIds=='+mapApplFeeIds);
        List<HDFC_DASH_Fee_Payment_Allocation__c> listFeePaymentDet = new List<HDFC_DASH_Fee_Payment_Allocation__c>();
        for(Id FeeId : mapApplFeeIds.keyset()){
            HDFC_DASH_Fee_Payment_Allocation__c feePaymentDet = new HDFC_DASH_Fee_Payment_Allocation__c();
            feePaymentDet.HDFC_DASH_Fee_Detail__c = FeeId;
            system.debug('mapApplFeeIds.get(FeeId).Id==='+mapApplFeeIds.get(FeeId).Id);
            feePaymentDet.HDFC_DASH_Application_Payment_Detail__c = mapApplFeeIds.get(FeeId).Id;
            listFeePaymentDet.add(feePaymentDet);  
        } 
        
        
        //database.insert(listFeePaymentDet);
        
        return listFeePaymentDet;       
    }
    
    /*
Method Name :populateFeeOnPaymentRec
Parameters  :Id feeId, HDFC_DASH_ParseApplication.applicationPaymentDetails parsePaymentDetails
Description :This method would upsert applicant detail recods with fee ids.
*/
    public static HDFC_DASH_StoreAppWrapper populateFeeOnPaymentRec(Map<HDFC_DASH_Application_Payment_Details__c,List<Id>> mapAppPayDetFeeId)
    {	
        system.debug('mapAppPayDetFeeId==='+mapAppPayDetFeeId);
        List<HDFC_DASH_Application_Payment_Details__c> listPayDet = new List<HDFC_DASH_Application_Payment_Details__c>();
        List<Id>  feeIdsToBeQueried = new List<Id>();
        Map<Id,HDFC_DASH_Application_Payment_Details__c> mapApplFeeIds= new Map<Id,HDFC_DASH_Application_Payment_Details__c>();
        list<HDFC_DASH_Application_Payment_Details__c> payDetToBeUpserted = new  list<HDFC_DASH_Application_Payment_Details__c>();
        
        for(HDFC_DASH_Application_Payment_Details__c appPaymetRec:mapAppPayDetFeeId.keyset()){
            system.debug('mapAppPayDetFeeId.get(appPaymetRec)==='+mapAppPayDetFeeId.get(appPaymetRec));
            List<Id> feeIds = mapAppPayDetFeeId.get(appPaymetRec);
            system.debug('feeIds==='+feeIds);
            if(feeIds!=NULL && feeIds.size()==HDFC_DASH_Constants.INT_ONE){
                appPaymetRec.HDFC_DASH_Fee_Detail__c = feeIds[HDFC_DASH_Constants.INT_ZERO];
                payDetToBeUpserted.add(appPaymetRec);
            }else if(feeIds!=NULL && feeIds.size()>HDFC_DASH_Constants.INT_ONE){
                feeIdsToBeQueried.addAll(feeIds);
            }
            system.debug('feeIdsToBeQueried==='+feeIdsToBeQueried);
        }
        
        
        List<HDFC_DASH_Application_Payment_Details__c> payDetToBeUpdForMulFee = new  List<HDFC_DASH_Application_Payment_Details__c>();
        List<string> fieldListFeeDetails = new List<string>{HDFC_DASH_Constants.STRING_ID,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.HDFC_DASH_APPLICATION_ISPRIME};
            Map<Id,HDFC_DASH_Fee_Detail__c> mapFeeDet= new Map<Id,HDFC_DASH_Fee_Detail__c>(HDFC_DASH_FeeDetail_Selector.getFeeDetailsBasedOnIds(fieldListFeeDetails,feeIdsToBeQueried));
        for(HDFC_DASH_Application_Payment_Details__c appPaymetRec:mapAppPayDetFeeId.keyset()){
            List<Id> feeIds = mapAppPayDetFeeId.get(appPaymetRec);
            system.debug('feeIds==='+feeIds);
            if(feeIds !=NULL && feeIds.size()>1){
                for(Id feeId:feeIds){
                    system.debug('mapFeeDet.get(feeId).HDFC_DASH_Application__r.HDFC_DASH_Is_PrimeFlag__c=='+mapFeeDet.get(feeId).HDFC_DASH_Application__r.HDFC_DASH_Is_PrimeFlag__c);
                    if(mapFeeDet.get(feeId).HDFC_DASH_Application__r.HDFC_DASH_Is_PrimeFlag__c == HDFC_DASH_Constants.STRING_Y){
                        appPaymetRec.HDFC_DASH_Fee_Detail__c = feeId;
                         mapApplFeeIds.put(feeId,appPaymetRec); 
                       
                    }else{
                         mapApplFeeIds.put(feeId,appPaymetRec); 
                    } 
                }
            }
        }
        Set<HDFC_DASH_Application_Payment_Details__c> appPayDetSet = new Set<HDFC_DASH_Application_Payment_Details__c>();
        appPayDetSet.addAll(mapApplFeeIds.values());
        payDetToBeUpserted.addAll(appPayDetSet);
        HDFC_DASH_StoreAppWrapper storeAppWrapper = new HDFC_DASH_StoreAppWrapper();
        storeAppWrapper.payDetToBeUpserted = payDetToBeUpserted;
        storeAppWrapper.mapApplFeeIds = mapApplFeeIds;
		system.debug('storeAppWrapper=='+storeAppWrapper);
        return storeAppWrapper;
    }
}