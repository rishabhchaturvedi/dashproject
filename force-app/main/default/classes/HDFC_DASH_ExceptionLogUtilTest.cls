/**
* 	className: HDFC_DASH_ExceptionLogUtil
DevelopedBy: Janani Mohankumar
Date: 27 May 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_ExceptionLogUtil class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_ExceptionLogUtilTest {
private static final string EXCEPTION_OPERATION = 'Testing Log Exception';

/* 
Method Name :testExceptionLog
Description :This method will publish the logException Record.
*/
static testmethod void testpublishExceptionWithASingleRecord(){
	
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    List<Account> accList=  HDFC_DASH_TestDataFactory_Accounts.createBasicAccountlist(1);
    System.runAs(sysAdmin) {
    Test.startTest();
    try{
       
			Database.update(accList) ;
          
    }catch(Exception e){
        
        HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.ACCOUNT_OBJECT,EXCEPTION_OPERATION,
                                                     accList[0].id,(e.getCause()+HDFC_DASH_Constants.SEMICOLON+e.getLineNumber()+HDFC_DASH_Constants.SEMICOLON
                                                     +e.getMessage()+HDFC_DASH_Constants.SEMICOLON+e.getStackTraceString()
                                                                     +HDFC_DASH_Constants.SEMICOLON+e.getTypeName()) );
    }
    }  
	Test.stopTest(); 
    if(HDFC_DASH_Constants.TRUESTR.equalsIgnoreCase(System.Label.HDFC_DASH_ProcessExcepFlag)){
		system.assertEquals(1,[select COUNT() from HDFC_DASH_Exception_Log__c 
                                WHERE HDFC_DASH_Operation__c=:EXCEPTION_OPERATION],EXCEPTION_OPERATION);
     }else{
     System.assertEquals(0, [select COUNT() from HDFC_DASH_Exception_Log__c 
                                         WHERE HDFC_DASH_Operation__c=:EXCEPTION_OPERATION], EXCEPTION_OPERATION);   
     }
}
/* 
Method Name :testExceptionLog
Description :This method will publish the logException With Multiple Records.
*/
static testmethod void testPublishExceptionWithMultipleRecords(){
	
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    List<Account> accList= new List<Account>();
	Database.SaveResult[] sList=null;
    System.runAs(sysAdmin) {
    Test.startTest();
    try{
        accList=  HDFC_DASH_TestDataFactory_Accounts.createBasicAccountlist(2);
		accList[0].name ='';
     	Database.insert(accList);
	}catch(Exception e){
        sList = Database.Insert(accList,false);
		HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.ACCOUNT_OBJECT,EXCEPTION_OPERATION,sList);
 	}
    }
    Test.stopTest();
    if(HDFC_DASH_Constants.truestr.equalsignorecase(System.Label.HDFC_DASH_ProcessExcepFlag)){
		system.assertEquals(1,[select COUNT() from HDFC_DASH_Exception_Log__c 
                                              WHERE HDFC_DASH_Operation__c=:EXCEPTION_OPERATION],EXCEPTION_OPERATION);
    }else{
        system.assertEquals(0,[select COUNT() from HDFC_DASH_Exception_Log__c 
                                        WHERE HDFC_DASH_Operation__c=:EXCEPTION_OPERATION],EXCEPTION_OPERATION);
    }
}
}