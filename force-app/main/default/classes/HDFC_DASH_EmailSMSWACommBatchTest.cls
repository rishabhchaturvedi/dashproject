/**
className: HDFC_DASH_EmailSMSWACommBatchTest
DevelopedBy: Raghavendra Koora
Date: 29 Dec 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_EmailSMSWACommunicationBatch
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_EmailSMSWACommBatchTest {
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);

    /*  
    Method Name :testEmailCommunicationBatchTest
    Description :This method would test the email communication batch.
    */
    static testmethod void testEmailCommunicationBatchTest(){
        
        Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
        Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
        Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
        HDFC_DASH_Applicant_Details__c appDet = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id,app.id);
        ID batchJobId=null;
        System.runAs(sysAdmin){
        DateTime twentyFourHours = System.now().addHours(-24);
        
        con.HDFC_DASH_SMS_Consent__c = 'Y';
        con.HDFC_DASH_WhatsApp_Consent__c = 'Y';
        update con;

        app.stageName = HDFC_DASH_Constants.ONBOARDING;
        //app.HDFC_DASH_Latest_Status_Change_Date_Time__c = System.now().addHours(-25);
        app.HDFC_DASH_Status__c = '75';
        app.HDFC_DASH_Pre_Approved_Loan_Flag__c = false;
        Database.update(app);
        //System.debug('application1 ' + app);
            
        app.HDFC_DASH_Latest_Status_Change_Date_Time__c = System.now().addHours(-50);
        Database.update(app);
        
        List<Opportunity> oppList = [SELECT ID,HDFC_DASH_Status__c,HDFC_DASH_Latest_Status_Change_Date_Time__c,stageName,HDFC_DASH_Pre_Approved_Loan_Flag__c,
        HDFC_DASH_Consent_For_Term_And_Condition__c,HDFC_DASH_Second_File_Push_To_ILPS__c 
        FROM Opportunity WHERE stageName = :HDFC_DASH_Constants.ONBOARDING AND HDFC_DASH_Latest_Status_Change_Date_Time__c <= :twentyFourHours];
		//System.debug('xxx batchquery ' + oppList.size());
        
        // ID batchJobId=null;
        //HDFC_DASH_EmailCommunicationBatch emailCommunicationBatch = new HDFC_DASH_EmailCommunicationBatch();
        Test.startTest();
        batchJobId = Database.executeBatch(new HDFC_DASH_EmailSMSWACommunicationBatch(), 1);
        Test.stopTest();
        }
        System.assertNotEquals(null, batchJobId);
    }
    
    /*  
    Method Name :testPreApprovedEmailCommunicationBatchTest
    Description :This method would test the email communication batch.
    */
    static testMethod void testPreApprovedEmailCommunicationBatchTest(){

        DateTime twentyFourHours = System.now().addHours(-24);
        ID batchJobId2=null;

        Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
        Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
        Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
        HDFC_DASH_Applicant_Details__c appDet = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id,app.id);
        
        System.runAs(sysAdmin){
        
        
        con.HDFC_DASH_SMS_Consent__c = 'Y';
        con.HDFC_DASH_WhatsApp_Consent__c = 'Y';
        update con;

        app.stageName = HDFC_DASH_Constants.ONBOARDING;
        app.HDFC_DASH_Status__c = '75';
        app.HDFC_DASH_Pre_Approved_Loan_Flag__c = false;
        Database.update(app);
        //System.debug('application1 ' + app);
        
        Account acc2 = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
        Contact con2 = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
        Opportunity app2 = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc2.id);
        HDFC_DASH_Applicant_Details__c appDet2 = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con2.id,app2.id);
        
        con2.HDFC_DASH_SMS_Consent__c = 'Y';
        con2.HDFC_DASH_WhatsApp_Consent__c = 'Y';
        update con2;
        
        app2.stageName = HDFC_DASH_Constants.ONBOARDING;
        //app2.HDFC_DASH_Latest_Status_Change_Date_Time__c = System.now().addHours(-50);
        //app2.HDFC_DASH_Status__c = '75';
        app2.HDFC_DASH_Pre_Approved_Loan_Flag__c = true;
        //app2.HDFC_DASH_Consent_For_Term_And_Condition__c = false;
        Database.update(app2);
        
        app2.HDFC_DASH_Latest_Status_Change_Date_Time__c = System.now().addHours(-50);
        Database.update(app2);
        
        List<Opportunity> oppList = [SELECT ID,HDFC_DASH_Status__c,HDFC_DASH_Latest_Status_Change_Date_Time__c,stageName,HDFC_DASH_Pre_Approved_Loan_Flag__c,
        HDFC_DASH_Consent_For_Term_And_Condition__c,HDFC_DASH_Second_File_Push_To_ILPS__c 
        FROM Opportunity WHERE stageName = :HDFC_DASH_Constants.ONBOARDING AND HDFC_DASH_Latest_Status_Change_Date_Time__c = null];
        //System.debug('xxx batchquery2 ' + oppList.size());
        // ID batchJobId2=null;
        //HDFC_DASH_EmailCommunicationBatch emailCommunicationBatch2 = new HDFC_DASH_EmailCommunicationBatch();
        Test.startTest();
        batchJobId2 = Database.executeBatch(new HDFC_DASH_EmailSMSWACommunicationBatch(), 1);
        Test.stopTest();
       }
       System.assertNotEquals(null, batchJobId2);
    }

    /*  
    Method Name :testScheduleCommunicationBatch
    Description :This method would test the scheduleBatchCommunication class.
    */
    static testmethod void testScheduleCommunicationBatch(){
        System.runAs(sysAdmin)
        { 
        HDFC_DASH_ScheduleBatchCommunication  scheduleBatchCommunication = new HDFC_DASH_ScheduleBatchCommunication();
        String schCronExp = '0 0 23 * * ?'; 
        Test.starttest();
        system.schedule('Schedule Communication Batch', schCronExp, scheduleBatchCommunication); 
        Test.stopTest(); 
        }
        System.assertNotEquals(null, 'test');
    }

}