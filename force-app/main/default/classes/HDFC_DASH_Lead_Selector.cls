/**
* 	className: HDFC_DASH_Lead_Selector
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class would create the select queries for Lead object .
**/
public inherited sharing class HDFC_DASH_Lead_Selector {
    private static final string QUERY_PARAMETER = '=:queryParameters';
        
    /* 
    Method Name :getLeadsBasedOnIds
    Parameters  :List<string> fieldList,List<ID> leadIds
    Description :This method would get the leads based on the IDs.
    */
        public static List<Lead> getLeadsBasedOnIds(List<string> fieldList,List<ID> leadIds)
        {
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROMLEADWHERE+ HDFC_DASH_CONSTANTS.STRING_ID + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(leadIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
            List<Lead> leadList = database.query(querystring); 
            return leadList;
        }
    /* 
    Method Name :getLeadsBasedOnId
    Parameters  :List<string> fieldList, Id leadId
    Description :This method would get a single Lead based on the ID.
    */
        public static Lead getLeadsBasedOnId(List<string> fieldList, Id leadId)
        {
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROMLEADWHERE+ HDFC_DASH_CONSTANTS.STRING_ID+ HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+leadId+HDFC_DASH_Constants.STRING_QUOTE;
            Lead leadRec = database.query(querystring); 
            return leadRec;
        }
        
    /* 
    Method Name :getLeadsBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of leads based on fieldsAndparameters sent to it.
    */
        public static List<Lead> getLeadsBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters)
        {
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROMLEADWHERE +getCondition(fieldsAndparameters) ;
            List<Lead> leadList = database.query(querystring); 
            return leadList;   
        }
        
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getLeadsBasedOnQuery .
    */
        public static String getCondition(Map<String,String> fieldsAndparameters){
            String condition ='';
            for(String fieldName : fieldsAndparameters.keySet()){
                
                condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
            }
            return condition;
        }

    /* 
    Method Name :getLeadStatus
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get a single LeadStatus based on the status.
    */
        public static LeadStatus getLeadStatus(List<String> fieldList,Map<String,String> fieldsAndparameters){ 
            string querystring =HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_LEAD_STATUS_WHERE +getCondition(fieldsAndparameters) +HDFC_DASH_CONSTANTS.STRING_LIMIT_1;
            LeadStatus  convertStatus = database.query(querystring); 
            return convertStatus;
        } 

                /* 
    Method Name :getLeadsBasedOnCondition
    Parameters  :String queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get the List of Leads based on the condition.
    */
   /* public static List<Lead> getLeadsBasedOnCondition(string queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + HDFC_DASH_Constants.ID_STRING+ HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_FirstName+ HDFC_DASH_Constants.COMMA+ HDFC_DASH_Constants.STRING_LastName+HDFC_DASH_Constants.COMMA + HDFC_DASH_Constants.STRING_CONTACT + HDFC_DASH_Constants.COMMA+ HDFC_DASH_Constants.STRING_STATUS  +HDFC_DASH_CONSTANTS.STRING_FROMLEADWHERE + conditionOn + HDFC_DASH_CONSTANTS.STRING_EQUALTO +HDFC_DASH_CONSTANTS.STRING_QUOTE + queryParameters+ HDFC_DASH_CONSTANTS.STRING_QUOTE;  
        List<Lead> resultList = database.query(querystring);        
        
        return resultList;
    } */
    
     /* 
Method Name :getLeadsBasedOnCondition
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of Leads based on the condition.
*/
    public static List<Lead> getLeadsBasedOnCondition(List<string> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + HDFC_DASH_CONSTANTS.STRING_FROMLEADWHERE + conditionOn + QUERY_PARAMETER;  
        List<Lead> resultList = database.query(querystring);        
        
        return resultList;
    } 
    }