/**
className: HDFC_DASH_AppInfoValid_SelectorTest
DevelopedBy: Punam Marbate
Date: 26 July 2021
Company: Accenture
Class Description: Test class for Applicant Info Validation class.
**/
@isTest(SeeAllData = false)
public with sharing class HDFC_DASH_AppInfoValid_SelectorTest {

    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Info_Validation__c applInfoValidation = 
                        HDFC_DASH_TestDataFactory_AppInfoValid.getBasicApplInfoValidation(appDetails.Id,HDFC_DASH_Constants.STRING_B);
    private static HDFC_DASH_Applicant_Details__c appDetails2 = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Info_Validation__c applInfoValidation2 = 
                        HDFC_DASH_TestDataFactory_AppInfoValid.getBasicApplInfoValidation(appDetails.Id,HDFC_DASH_Constants.STRING_B);
    /* 
    Method Name :testGetappInfoValBasedOnId
    Description :This method will test the method getappInfoValBasedOnId().
    */
   /* static testmethod void testGetappInfoValBasedOnId(){
        System.runAs(sysAdmin){
            HDFC_DASH_Info_Validation__c resApplInfoValidRec;
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            resApplInfoValidRec = HDFC_DASH_AppInfoValidation_Selector.getappInfoValBasedOnId(fieldList, applInfoValidation.Id);
            Test.stopTest();
            system.assertNotEquals( null, resApplInfoValidRec);
            system.assertEquals(applInfoValidation.Id,resApplInfoValidRec.Id); 
        }
      }*/
     /* 
    Method Name :testGetAppInfoValidationBasedOnIds
    Description :This method will test the method getAppInfoValidationBasedOnIds().
    */
  /*  static testmethod void testGetAppInfoValidationBasedOnIds(){
        List<HDFC_DASH_Info_Validation__c>  listApplInfoValidRec = null;
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                                            HDFC_DASH_Constants.STRING_Name};
            List<Id> listApplInfoValidationIds = new List<Id>{applInfoValidation.Id,applInfoValidation2.Id};
            Test.startTest();
            listApplInfoValidRec = HDFC_DASH_AppInfoValidation_Selector.getAppInfoValidationBasedOnIds(
                    fieldList ,listApplInfoValidationIds);
            Test.stopTest();
        }
        system.assertNotEquals( null, listApplInfoValidRec);
        system.assertEquals(2,listApplInfoValidRec.size()); 
      } */
    /* 
    Method Name :testGetappInfoValBasedOnQuery
    Description :This method will test the method getappInfoValBasedOnQuery().
    */
      static testmethod void testGetappInfoValBasedOnQuery(){
        List<HDFC_DASH_Info_Validation__c> listApplInfoValRec = null;
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_APPDETAILS=>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                appDetails.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
                listApplInfoValRec = HDFC_DASH_AppInfoValidation_Selector.getappInfoValBasedOnQuery(new List<String>{HDFC_DASH_Constants.STRING_ID},condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listApplInfoValRec);
        system.assertEquals(2,listApplInfoValRec.size()); 
      }
}