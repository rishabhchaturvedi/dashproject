/**
className: HDFC_DASH_Appl_Emp_SelectorTest
DevelopedBy: Punam Marbate
Date: 22 July 2021
Company: Accenture
Class Description: Test class for  Applicant_Employment_Selector class.
**/
@isTest(SeeAllData = false)
public with sharing class HDFC_DASH_Appl_Emp_SelectorTest {
    private static final string STRING_ID='Id';
    private static final string STRING_NAME='Name'; 
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c aplEmpDetail = HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.Id,con.id);
      /* 
    Method Name :testGetEmploymentDetailsBasedOnId
    Description :This method will test the GetEmploymentDetailsBasedOnId.
    */
    static testmethod void testGetEmploymentDetailsBasedOnId(){  
        HDFC_DASH_Applicant_Employment_Details__c resEmpDetailRec = null;     
        System.runAs(sysAdmin){   
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME};
            Test.startTest();
             resEmpDetailRec = HDFC_DASH_Applicant_Employment_Selector.getEmploymentDetailsBasedOnId(fieldList, aplEmpDetail.Id);
        	Test.stopTest();
        }
        system.assertNotEquals( null, resEmpDetailRec);
        system.assertNotEquals( null, aplEmpDetail);
        system.assertEquals(aplEmpDetail.Id,resEmpDetailRec.Id); 
    }
    /* 
    Method Name :testGetEmploymentDetails
    Description :This method will test the GetEmploymentDetails.
    */
    static testmethod void testGetEmploymentDetails(){  
        List<HDFC_DASH_Applicant_Employment_Details__c>  listEmpDetailRec = null;
        HDFC_DASH_Applicant_Employment_Details__c empDetailRec = new HDFC_DASH_Applicant_Employment_Details__c();
        System.runAs(sysAdmin){ 
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME};
            Test.startTest();
            listEmpDetailRec = HDFC_DASH_Applicant_Employment_Selector.getEmploymentDetails(new List<String> {aplEmpDetail.Id}  ,new List<String>{HDFC_DASH_Constants.STRING_ID},HDFC_DASH_Constants.ID_STRING);          
        	Test.stopTest();
        }
        system.assertNotEquals( null, listEmpDetailRec);
        system.assertEquals(1,listEmpDetailRec.size()); 
    }

    /* 
    Method Name :testGetEmploymentDetails
    Description :This method will test the GetEmploymentDetails.
    */
    static testmethod void testgetAppEmpDetailsBasedOnQuery(){  
        List<HDFC_DASH_Applicant_Employment_Details__c>  listEmpDetailRec = null;
        HDFC_DASH_Applicant_Employment_Details__c empDetailRec = new HDFC_DASH_Applicant_Employment_Details__c();
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME};
            Test.startTest();
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_APPDETAILS =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                appDetails.Id+HDFC_DASH_Constants.STRING_QUOTE};
            listEmpDetailRec = HDFC_DASH_Applicant_Employment_Selector.getAppEmpDetailsBasedOnQuery(new List<String>{HDFC_DASH_Constants.STRING_ID},condition);      
        	Test.stopTest();
        }
        system.assertNotEquals( null, listEmpDetailRec);
        system.assertEquals(1,listEmpDetailRec.size()); 
    }
}