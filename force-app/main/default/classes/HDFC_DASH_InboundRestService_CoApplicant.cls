/**
* 	className: HDFC_DASH_InboundRestService_CoApplicant
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class would check dedupe logic for co-applicants.
**/
public inherited sharing class HDFC_DASH_InboundRestService_CoApplicant {
    
    
    //variables used throught the class
    private static Boolean isexistingCustomer = HDFC_DASH_Constants.BOOLEAN_FALSE; 
    //private static Boolean isexistingCustomer = HDFC_DASH_Constants.BOOLEAN_FALSE; 
    /* 
Method Name :processCoApplicantDetails
Parameters  :RestRequest request
Description :This method return the response for the dedupe request from HDFC_DASH_RestResource_CoApplicant.
*/
    public static HDFC_DASH_APIResponse_CoApplicant processCoApplicantDetails(RestRequest request){
        
        //parsing json  
        HDFC_DASH_ParseCoApplicantDetails coApplicantDetails = HDFC_DASH_ParseCoApplicantDetails.parse(request);
        
        //run salesforce logic for dedupe
        Contact conpassingDedupeCheck =runSalesforceDedupe(coApplicantDetails); 
        if(conpassingDedupeCheck == NULL){
            isexistingCustomer = HDFC_DASH_Constants.BOOLEAN_FALSE;
            // isexistingCustomer = HDFC_DASH_Constants.BOOLEAN_TRUE;
        }
        Id appDetailRecId = insertCoApplicantDetails(coApplicantDetails,conpassingDedupeCheck);
        
        HDFC_DASH_UtilityClass.storeValidationOutcome(request, appDetailRecId);
        HDFC_DASH_APIResponse_CoApplicant apiResponse = createResponseStructure(appDetailRecId); 
        return apiResponse;
    }
    /* 
Method Name :runSalesforceDedupe
Parameters  :HDFC_DASH_ParseCoApplicantDetails coApplicantDetails
Description :This method would check if a contact is already present in Salesforce.
*/
    public static Contact runSalesforceDedupe(HDFC_DASH_ParseCoApplicantDetails coApplicantDetails){
        Contact conpassingDedupeCheck =NULL;
        if(String.isNotBlank(coApplicantDetails?.coApplicant?.personalDetails?.pan)){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,HDFC_DASH_Constants.STRING_FirstName,HDFC_DASH_Constants.STRING_LastName,HDFC_DASH_Constants.HDFC_DASH_PAN};
                Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_PAN => HDFC_DASH_Constants.STRING_EQUALTO
                    +HDFC_DASH_Constants.STRING_QUOTE+coApplicantDetails?.coApplicant?.personalDetails?.pan
                    +HDFC_DASH_Constants.STRING_QUOTE};
                        
                        for(Contact con:HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(fieldList, condition)){
                            if((con.HDFC_DASH_Date_Of_Birth__c == coApplicantDetails?.coApplicant?.personalDetails?.dateOfBirth) ||HDFC_DASH_UtilityClass.doesNameMatch(coApplicantDetails?.coApplicant?.personalDetails?.firstName,coApplicantDetails?.coApplicant?.personalDetails?.lastName,con?.FirstName,con?.LastName)){
                                
                                conpassingDedupeCheck =con;
                                isexistingCustomer =HDFC_DASH_Constants.BOOLEAN_TRUE;
                                //isexistingCustomer = HDFC_DASH_Constants.BOOLEAN_TRUE;
                                break;
                            } 
                        }
        }
        return conpassingDedupeCheck;    
    }
    
    /* 
Method Name :insertCoApplicantDetails
Parameters  :HDFC_DASH_ParseCoApplicantDetails coApplicantDetails,contact conpassingDedupeCheck
Description :This method would insert the co-Applicant details.
*/      
    public static Id insertCoApplicantDetails(HDFC_DASH_ParseCoApplicantDetails coApplicantDetails,contact conpassingDedupeCheck){
        HDFC_DASH_Applicant_Details__c appDetailRec = new HDFC_DASH_Applicant_Details__c();
        Sobject aggregateAppDetailRec;
        
        appDetailRec.HDFC_DASH_Applicant_Capacity__c = HDFC_DASH_Constants.STRING_C;
        if(!String.isBlank(coApplicantDetails?.coApplicant?.sfApplicationId)){
            appDetailRec.HDFC_DASH_Application__c = coApplicantDetails?.coApplicant?.sfApplicationId;
        }else{
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_MISSING); 
        }
        
        if(!String.isBlank(coApplicantDetails?.coApplicant?.applicantType)){
            appDetailRec.RecordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Details__c
                .getRecordTypeInfosByDeveloperName().get(coApplicantDetails?.coApplicant?.applicantType).getRecordTypeId();
            //appDetailRec.HDFC_DASH_User_Consent_For_CIBIL__c = HDFC_DASH_Constants.STRING_NO;
            appDetailRec.HDFC_DASH_Skip_Employment_Details__c = coApplicantDetails?.coApplicant?.skipEmploymentDetails;
            appDetailRec.HDFC_DASH_Experian_CIBIL_ID__c = coApplicantDetails?.coApplicant?.experianCibilID;
            appDetailRec.HDFC_DASH_TF_Experian_CIBILID__c = coApplicantDetails?.coApplicant?.experianCibilTechnicalFailure==null?false:coApplicantDetails?.coApplicant?.experianCibilTechnicalFailure;
            appDetailRec.HDFC_DASH_Relation_with_PrimaryApplicant__c = coApplicantDetails?.coApplicant?.personalDetails?.relationWithPrimaryApplicant;
            
            appDetailRec.HDFC_DASH_CoAppSeqNumber__c = coApplicantDetails?.coApplicant?.coAppSequenceNumber;
            
            appDetailRec.HDFC_DASH_Father_First_Name__c = coApplicantDetails?.coApplicant?.personalDetails?.fatherFirstName;
            appDetailRec.HDFC_DASH_Father_Middle_Name__c = coApplicantDetails?.coApplicant?.personalDetails?.fatherMiddleName;
            appDetailRec.HDFC_DASH_Father_Last_Name__c = coApplicantDetails?.coApplicant?.personalDetails?.fatherLastName;
            appDetailRec.HDFC_DASH_Marital_Status__c = coApplicantDetails?.coApplicant?.personalDetails?.maritalStatus;
            appDetailRec.HDFC_DASH_Highest_Qualification__c = coApplicantDetails?.coApplicant?.personalDetails?.highestQualification;
            appDetailRec.HDFC_DASH_Highest_Qualification_Code__c = coApplicantDetails?.coApplicant?.personalDetails?.highestQualificationCode;
            appDetailRec.HDFC_DASH_Social_Category__c = coApplicantDetails?.coApplicant?.personalDetails?.socialCategory;
            
            if(conpassingDedupeCheck !=NULL){
                appDetailRec.HDFC_DASH_Contact__c = conpassingDedupeCheck.id;
            }else{
                contact conRec = insertpersonAccountRec(coApplicantDetails);
                appDetailRec.HDFC_DASH_Contact__c = conRec.id;
            }
            
            appDetailRec.Name = (coApplicantDetails?.coApplicant?.personalDetails?.salutation==null?'':coApplicantDetails?.coApplicant?.personalDetails?.salutation)+' '+(coApplicantDetails?.coApplicant?.personalDetails?.firstName==null?'':coApplicantDetails?.coApplicant?.personalDetails?.firstName)+' ' +
                (coApplicantDetails?.coApplicant?.personalDetails?.middleName==null?'':coApplicantDetails?.coApplicant?.personalDetails?.middleName+ ' ')+(coApplicantDetails?.coApplicant?.personalDetails?.lastName==null?'':coApplicantDetails?.coApplicant?.personalDetails?.lastName);
            
            appDetailRec.HDFC_DASH_Relation_with_PrimaryApplicant__c = coApplicantDetails?.coApplicant?.personalDetails?.relationWithPrimaryApplicant;
            
            
            //riskscore feilds
            appDetailRec.HDFC_DASH_RiskScore_Customer_Status__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreCustomerstatus;
            appDetailRec.HDFC_DASH_RiskScore_Customer_flags__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreCustomerflags;
            appDetailRec.HDFC_DASH_RiskScore_Rtr_Exists__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreRtrexists;
            appDetailRec.HDFC_DASH_RiskScore_Customer_grade__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreCustomergrade;
            appDetailRec.HDFC_DASH_RiskScore_Risk_event__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreRiskevent;
            appDetailRec.HDFC_DASH_RiskScore_Availed_moratorium__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreAvailedMoratorium;
            appDetailRec.HDFC_DASH_RiskScore_Availed_restruct__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreAvailedRestruct;
            appDetailRec.HDFC_DASH_RiskScore_Hdfc_Staff__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreHdfcStaff;
            appDetailRec.HDFC_DASH_RiskScore_Confidence_score__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreConfidenceScore;
            appDetailRec.HDFC_DASH_RiskSore_Exist_in_neg_list__c = coApplicantDetails?.coApplicant?.riskScore?.riskScoreExistInNegList;
            appDetailRec.HDFC_DASH_TF_RiskScore__c = coApplicantDetails?.coApplicant?.riskScoreTechnicalFailure == null?false : coApplicantDetails?.coApplicant?.riskScoreTechnicalFailure;
            
            //Financial Info feilds
            appDetailRec.HDFC_DASH_SO_Fixed_Monthly_Income__c = coApplicantDetails?.coApplicant?.financialInfo?.fixedMonthlyIncome;
            appDetailRec.HDFC_DASH_SO_Additional_Income__c = coApplicantDetails?.coApplicant?.financialInfo?.additionalIncome;
            appDetailRec.HDFC_DASH_NO_Additional_Income__c = coApplicantDetails?.coApplicant?.financialInfo?.noAdditionalIncome;
            appDetailRec.HDFC_DASH_Salary_Slip_Attached__c = coApplicantDetails?.coApplicant?.financialInfo?.salarySlipAttached;
            
            //populate personal details
            appDetailRec.HDFC_DASH_Salutation__c = coApplicantDetails?.coApplicant?.personalDetails?.salutation;
            appDetailRec.HDFC_DASH_FirstName__c = coApplicantDetails?.coApplicant?.personalDetails?.firstName;
            appDetailRec.HDFC_DASH_MiddleName__c = coApplicantDetails?.coApplicant?.personalDetails?.middleName;
            appDetailRec.HDFC_DASH_LastName__c = coApplicantDetails?.coApplicant?.personalDetails?.lastName;
            appDetailRec.HDFC_DASH_PAN__c = coApplicantDetails?.coApplicant?.personalDetails?.pan;
            appDetailRec.HDFC_DASH_Pan_Applied__c = coApplicantDetails?.coApplicant?.personalDetails?.panApplied;
            appDetailRec.HDFC_DASH_Pan_Status_From_NSDL__c = coApplicantDetails?.coApplicant?.personalDetails?.panStatus;
            appDetailRec.HDFC_DASH_Date_Of_Birth__c = coApplicantDetails?.coApplicant?.personalDetails?.dateOfBirth;
            appDetailRec.HDFC_DASH_Gender__c = coApplicantDetails?.coApplicant?.personalDetails?.genderCode;
            appDetailRec.HDFC_DASH_City_Code__c = coApplicantDetails?.coApplicant?.personalDetails?.cityCode;
            appDetailRec.HDFC_DASH_State_Code__c = coApplicantDetails?.coApplicant?.personalDetails?.stateCode;
            appDetailRec.HDFC_DASH_Country_Code__c = coApplicantDetails?.coApplicant?.personalDetails?.CountryCode;
            appDetailRec.HDFC_DASH_City__c =  coApplicantDetails?.coApplicant?.personalDetails?.cityName;
            appDetailRec.HDFC_DASH_State__c = coApplicantDetails?.coApplicant?.personalDetails?.stateName;
            appDetailRec.HDFC_DASH_Country__c = coApplicantDetails?.coApplicant?.personalDetails?.countryName;
            appDetailRec = (HDFC_DASH_Applicant_Details__c)HDFC_DASH_UtilityClass.generateMasterTabelFields(appDetailRec);
            appDetailRec.HDFC_DASH_Resident_Type__c = coApplicantDetails?.coApplicant?.personalDetails?.residentType;
            appDetailRec.HDFC_DASH_Outsystems_ID__c = coApplicantDetails?.coApplicant?.personalDetails?.outSystemsID;
            
            //aggregateAppDetailRec = generateCommonFields(coApplicantDetails,appDetailRec);
            
            Database.SaveResult insertedAppDetailsRec = Database.insert(appDetailRec); 
            
            //update application   
            updateApplication(coApplicantDetails);
            insertEmployementDetailsRec(appDetailRec.id,coApplicantDetails);
            
        }
        else{
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_RECORDTYPE_EXP); 
        }
        return appDetailRec.id;
    }
    /* 
Method Name :insertcontactRec
Parameters  :HDFC_DASH_ParseCoApplicantDetails coApplicantDetails
Description :This method would insert the contact record.
*/      
    public static Contact insertpersonAccountRec(HDFC_DASH_ParseCoApplicantDetails coApplicantDetails){
       // RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = HDFC_DASH_Constants.STRING_PERSONACCOUNT and SObjectType = HDFC_DASH_Constants.ACCOUNT_OBJECT];
        Account newPersonAcc = new Account(); 
        
        newPersonAcc.Salutation = coApplicantDetails?.coApplicant?.personalDetails?.salutation;
        newPersonAcc.FirstName = coApplicantDetails?.coApplicant?.personalDetails?.firstName;
        newPersonAcc.MiddleName = coApplicantDetails?.coApplicant?.personalDetails?.middleName;
        newPersonAcc.LastName = coApplicantDetails?.coApplicant?.personalDetails?.lastName;
        newPersonAcc.HDFC_DASH_PAN__pc = coApplicantDetails?.coApplicant?.personalDetails?.pan;
        newPersonAcc.HDFC_DASH_Pan_Applied__pc = coApplicantDetails?.coApplicant?.personalDetails?.panApplied;
        newPersonAcc.HDFC_DASH_Pan_Status_From_NSDL__pc = coApplicantDetails?.coApplicant?.personalDetails?.panStatus;
        newPersonAcc.HDFC_DASH_Date_Of_Birth__pc = coApplicantDetails?.coApplicant?.personalDetails?.dateOfBirth;
        newPersonAcc.HDFC_DASH_Gender__pc = coApplicantDetails?.coApplicant?.personalDetails?.genderCode;
        newPersonAcc.HDFC_DASH_City_Code__pc = coApplicantDetails?.coApplicant?.personalDetails?.cityCode;
        newPersonAcc.HDFC_DASH_State_Code__pc = coApplicantDetails?.coApplicant?.personalDetails?.stateCode;
        newPersonAcc.HDFC_DASH_Country_Code__pc = coApplicantDetails?.coApplicant?.personalDetails?.CountryCode;
        newPersonAcc.HDFC_DASH_City__pc = coApplicantDetails?.coApplicant?.personalDetails?.cityName;
        newPersonAcc.HDFC_DASH_State__pc = coApplicantDetails?.coApplicant?.personalDetails?.stateName;
        newPersonAcc.HDFC_DASH_Country__pc = coApplicantDetails?.coApplicant?.personalDetails?.countryName;
        newPersonAcc = (Account)HDFC_DASH_UtilityClass.generateMasterTabelFields(newPersonAcc);
        newPersonAcc.HDFC_DASH_Resident_Type__pc = coApplicantDetails?.coApplicant?.personalDetails?.residentType;
        newPersonAcc.HDFC_DASH_Outsystems_ID__pc = coApplicantDetails?.coApplicant?.personalDetails?.outSystemsID;
        //newPersonAcc = (Account)generateCommonFields(coApplicantDetails,newPersonAcc);
        newPersonAcc.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.STRING_PERSONACCOUNT).getRecordTypeId();
        insert newPersonAcc;
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_ACCOUNTID};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.STRING_ACCOUNTID => HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+newPersonAcc.id+HDFC_DASH_Constants.STRING_QUOTE};
                system.debug('contact condition'+condition);
        List<Contact> conRec = HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(fieldList, condition);
        system.debug('conRec'+conRec);
        return conRec[HDFC_DASH_Constants.INT_ZERO];
    }
    
    /* 
Method Name :updateApplication
Parameters  :HDFC_DASH_ParseCoApplicantDetails coApplicantDetails
Description :This method would update Application record.
*/
    public static void updateApplication(HDFC_DASH_ParseCoApplicantDetails coApplicantDetails){
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Stage_Number,HDFC_DASH_Constants.HDFC_DASH_Stage_Description};
            Opportunity application = HDFC_DASH_Application_Selector.getApplicationBasedOnId(fieldList, coApplicantDetails?.coApplicant?.sfApplicationId);
        //application.HDFC_DASH_Stage_Number__c = coApplicantDetails?.coApplicant?.leadStageNumber;
        //application.HDFC_DASH_Stage_Description__c = coApplicantDetails?.coApplicant?.leadStageDescription;
        //we are setting it here only in code but we need to get this field from parser
        application.HDFC_DASH_Co_Applicant_Exist__c = HDFC_DASH_Constants.STRING_YES; 
        Database.update(application);
        
    }
    /* 
Method Name :insertEmployementDetailsRec
Parameters  :Id appDetailRecId,HDFC_DASH_ParseCoApplicantDetails coApplicantDetails
Description :This method would insert Employement Details of a particular applicant details record.
*/
    public static void insertEmployementDetailsRec(Id appDetailRecId,HDFC_DASH_ParseCoApplicantDetails coApplicantDetails){
        HDFC_DASH_Applicant_Employment_Details__c appEmpDetRec = new HDFC_DASH_Applicant_Employment_Details__c();
        appEmpDetRec.HDFC_DASH_Applicant_Details__c = appDetailRecId;
        appEmpDetRec.HDFC_DASH_Nat_Of_Emp__c = coApplicantDetails?.coApplicant?.employmentDetails?.occupation;//as per the final master sheet we decided to map occupation with nature of employment
        appEmpDetRec.HDFC_DASH_Work_Email__c = coApplicantDetails?.coApplicant?.employmentDetails?.workEmail;
        appEmpDetRec.HDFC_DASH_Employer_Code__c = coApplicantDetails?.coApplicant?.employmentDetails?.employerCode;
        appEmpDetRec.HDFC_DASH_Employer_Name__c = coApplicantDetails?.coApplicant?.employmentDetails?.employerName;
        appEmpDetRec =  (HDFC_DASH_Applicant_Employment_Details__c)HDFC_DASH_UtilityClass.generateEmployerTabelFields((Sobject)appEmpDetRec);
        Database.insert(appEmpDetRec);
    }
    /* 
Method Name :createResponseStructure
Parameters  :Id appDetailsRecId
Description :This method would generate the response for coapplicantdetails API.
*/
    private static HDFC_DASH_APIResponse_CoApplicant createResponseStructure(Id appDetailsRecId){ 
        
        HDFC_DASH_APIResponse_CoApplicant apiResponse = new HDFC_DASH_APIResponse_CoApplicant();
        
        HDFC_DASH_APIResponse_CoApplicant.Application applicationResponse = new HDFC_DASH_APIResponse_CoApplicant.Application();
        HDFC_DASH_APIResponse_CoApplicant.CoApplicant coApplicantResponse = new HDFC_DASH_APIResponse_CoApplicant.CoApplicant();
        //Fetch Applicant Details Rec
        List<String> appDetailsFeildList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.HDFC_DASH_Contact,HDFC_DASH_Constants.Contact_Account_HDFC_DASH_Customer_Number};
            HDFC_DASH_Applicant_Details__c appDetailsRec = HDFC_DASH_Applicant_Detail_Selector.getApplicantDetailBasedOnId(appDetailsFeildList,appDetailsRecId);
        
        List<String> oppFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber,HDFC_DASH_Constants.HDFC_DASH_Co_Applicant_Exist};
            Opportunity application = HDFC_DASH_Application_Selector.getApplicationBasedOnId(oppFieldList, appDetailsRec?.HDFC_DASH_Application__c);
        
        
        apiResponse.doesCoApplicantExist = application?.HDFC_DASH_Co_Applicant_Exist__c ;
        apiResponse.isexistingCustomer = isexistingCustomer;
        //apiResponse.isExistingCustomer =isexistingCustomer;
        applicationResponse.sfApplicationId = application?.Id;
        applicationResponse.sfApplicationNumber = application?.HDFC_DASH_ApplicationNumber__c;
        if(appDetailsRec !=NULL){
            coApplicantResponse.sfApplicantId = appDetailsRec?.id;
            coApplicantResponse.sfCustomerId = appDetailsRec?.HDFC_DASH_Contact__c;
            coApplicantResponse.sfCustomerNumber = appDetailsRec?.HDFC_DASH_Contact__r?.Account.HDFC_DASH_Customer_Number__c;
                       
        } 
        apiResponse.Application = applicationResponse;
        apiResponse.Application.CoApplicant = coApplicantResponse;
        return apiResponse;
    }
    
    
}