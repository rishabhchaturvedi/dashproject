/**
className: HDFC_DASH_ScheduleBatchNotification
DevelopedBy: Anisha Arumugam
Date: 25 Jan 2022
Company: Accenture
Class Description: This class would schedule the HDFC_DASH_BatchNotification.
**/
global class HDFC_DASH_ScheduleBatchNotification Implements Schedulable  
{
    /* 
Method Name :execute
Parameters  :SchedulableContext sc
Description :This method will schedule the HDFC_DASH_BatchNotification
*/ 
    global void execute(SchedulableContext sc) 
    {
        HDFC_DASH_BatchNotification notificationBatch = new HDFC_DASH_BatchNotification(); 
        database.executebatch(notificationBatch, 200);
    }
}