/* ================================================
 * @Class Name :  IT_TestUtility
 * @author : Accenture
 * @created date:
 ================================================*/
public class IT_TestUtility{
    private static final string NAME='GETCALL';
    private static final string FRAMEWORK_NAME = 'test';
    private static final string LAST_NAME='testuser';
    private static final string EMAIL='testuser@abc.com';
    private static final string END_POINT='testUrl';
    private static final string RETRY_PROCESS_CLASS='processResponseHandler';
    private static final string SUCESS_RESPONSE_CODES = '200;202';
    /***************************************************************************************************************
    *   @Name        :  insertContact                                                              
    *   @Return      :  string                                                                                       
    *   @Description :  This method in testUtility insert Contact into database.                              
    ***************************************************************************************************************/
    public string insertContact(){
        Contact con = new contact();
        con.lastname = LAST_NAME;
        con.email = EMAIL;
        insert con;
        return con.id;
    }
    /***************************************************************************************************************
    *   @Name        :  insertIntefaceSettingsRecord                                                              
    *   @Return      :  Interface_Settings__c    
    *   @Parameters  :  None                                                                               
    *   @Description :  This method in testUtility insert IntefaceSettingsRecord into database.                              
    ***************************************************************************************************************/
    public Interface_Settings__c insertIntefaceSettingsRecord(){
        Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
        interfaceSettingObj.name = NAME;
        interfaceSettingObj.Consolidated_Email__c = true;
        //interfaceSettingObj.Email_Template_Id__c = true;
        interfaceSettingObj.End_point__c = END_POINT;
        interfaceSettingObj.Expiry_Time__c = 60;
        interfaceSettingObj.Immediate_email__c = true;
        interfaceSettingObj.Immediate_Retry__c = true;
        interfaceSettingObj.No_Callout__c = false;
        interfaceSettingObj.Number_of_Retry__c = 3;
        interfaceSettingObj.Retry_Interval__c = 30;
        interfaceSettingObj.Retry_Process_Class__c = RETRY_PROCESS_CLASS;
        interfaceSettingObj.Success_Response_Codes__c = SUCESS_RESPONSE_CODES;
        //interfaceSettingObj.User_Contact_ID_for_Email__c = true;
        insert interfaceSettingObj;
        return interfaceSettingObj;
    }
    /***************************************************************************************************************
    *   @Name        :  insertIntefaceSettingsRecord                                                              
    *   @Return      :  Interface_Settings__c     
    *   @Parameters  :  Boolean blobBody                                                                               
    *   @Description :  This method in testUtility insert IntefaceSettingsRecord into database.                              
    ***************************************************************************************************************/
    public Interface_Settings__c insertIntefaceSettingsRecord(Boolean blobBody){
        Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
        interfaceSettingObj.name = NAME;
        interfaceSettingObj.Consolidated_Email__c = true;
        //interfaceSettingObj.Email_Template_Id__c = true;
        interfaceSettingObj.End_point__c = END_POINT;
        interfaceSettingObj.Expiry_Time__c = 60;
        interfaceSettingObj.Immediate_email__c = true;
        interfaceSettingObj.Immediate_Retry__c = true;
        interfaceSettingObj.No_Callout__c = false;
        interfaceSettingObj.Number_of_Retry__c = 3;
        interfaceSettingObj.Retry_Interval__c = 30;
        interfaceSettingObj.Retry_Process_Class__c = RETRY_PROCESS_CLASS;
        interfaceSettingObj.Success_Response_Codes__c = SUCESS_RESPONSE_CODES;
        interfaceSettingObj.Request_Body_As_Blob__c = true;
        //interfaceSettingObj.User_Contact_ID_for_Email__c = true;
        insert interfaceSettingObj;
        return interfaceSettingObj;
    }
    /***************************************************************************************************************
    *   @Name        :  insertFrameworkSettingsRecord                                                              
    *   @Return      :  void     
    *   @Parameters  :  Boolean stopCallouts                                                                             
    *   @Description :  This method in testUtility insert FrameworkSettingsRecord into database.                              
    ***************************************************************************************************************/
    public void insertFrameworkSettingsRecord(Boolean stopCallouts){
        interface_Framework_Settings__c frameWorkCustomSetting = new interface_Framework_Settings__c();
        frameWorkCustomSetting.name = FRAMEWORK_NAME;
        frameWorkCustomSetting.Stop_All_CallOuts__c = stopCallouts;
        insert frameWorkCustomSetting;
    }
    /***************************************************************************************************************
    *   @Name        :  createEmailTemplate                                                              
    *   @Return      :  string     
    *   @Parameters  :  None                                                                             
    *   @Description :  This method in testUtility insert/create EmailTemplate into database.                              
    ***************************************************************************************************************/
    public string createEmailTemplate(){
        EmailTemplate templateObject = new EmailTemplate();
        templateObject.isActive = true;
        templateObject.Name = 'immediate email';
        templateObject.DeveloperName = 'testuser';
        templateObject.TemplateType = 'text';
        insert templateObject;
        return templateObject.id;
    }
    /***************************************************************************************************************
    *   @Name        :  createInterationLog                                                              
    *   @Return      :  void     
    *   @Parameters  :  Integration_Message_Log__c intLogObj                                                                            
    *   @Description :  This method in testUtility insert/create InterationLog into database.                              
    ***************************************************************************************************************/
    public void createInterationLog(Integration_Message_Log__c intLogObj){
        
        intLogObj.Interface_Name__c = 'GETCALL';
        intLogObj.Method_Type__c = 'GET';
        intLogObj.Request_Body__c = 'testUrl';
        intLogObj.Header_Map__c = null;
        intLogObj.Post_Retry_Process__c = 'processResponseHandler';
        intLogObj.Number_Of_Retries__c = 0;
        insert intLogObj;
    }
    /***************************************************************************************************************
    *   @Name        :  insertFireAndForgetIntefaceSettingsRecord                                                              
    *   @Return      :  Interface_Settings__c     
    *   @Parameters  :  None                                                                            
    *   @Description :  This method in testUtility insert/create FireAndForgetIntefaceSettingsRecord into database.                              
    ***************************************************************************************************************/
    public Interface_Settings__c insertFireAndForgetIntefaceSettingsRecord(){
        Interface_Settings__c interfaceSettingObj = new Interface_Settings__c();
        interfaceSettingObj.name = 'ParentRecord';
        interfaceSettingObj.Parent_Interface_Name__c = 'GETCALL';
        interfaceSettingObj.Consolidated_Email__c = true;
        //interfaceSettingObj.Email_Template_Id__c = true;
        interfaceSettingObj.End_point__c = END_POINT;
        interfaceSettingObj.Expiry_Time__c = 60;
        interfaceSettingObj.Immediate_email__c = true;
        interfaceSettingObj.Immediate_Retry__c = true;
        interfaceSettingObj.No_Callout__c = false;
        interfaceSettingObj.Number_of_Retry__c = 3;
        interfaceSettingObj.Retry_Interval__c = 30;
        interfaceSettingObj.Retry_Process_Class__c = RETRY_PROCESS_CLASS;
        interfaceSettingObj.Success_Response_Codes__c = SUCESS_RESPONSE_CODES;
        //interfaceSettingObj.User_Contact_ID_for_Email__c = true;
        insert interfaceSettingObj;
        return interfaceSettingObj;
    }
}