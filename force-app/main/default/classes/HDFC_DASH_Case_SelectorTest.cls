/**
className: HDFC_DASH_Case_SelectorTest
DevelopedBy: Sakshi Shetty
**/


@IsTest
private class HDFC_DASH_Case_SelectorTest {
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicPersonAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(con.id, app.id);
    private static Case caseobject=HDFC_DASH_TestDataFactory_Case.getBasicCase(acc.Id,app.Id);
    private static List<String> userIds = new List<String>();
    
        /* 
  Method Name :getCaseRecsBasedOnStringTest
  Parameters  :List<string> fieldList,String queryParameter,String conditionField
  Description :This method would test the List of Cases based on single string condition sent to it.
  */   
    static testmethod void getCaseRecsBasedOnStringTest(){

        List<Case> resultCaseDetails = null;
        System.runAs(sysAdmin){
            
            List<String> fieldList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_HDB_Number,
            HDFC_DASH_Constants.Case_Status,HDFC_DASH_Constants.CREATEDDATE,HDFC_DASH_Constants.IsClosed,HDFC_DASH_Constants.HDFC_DASH_File_Number};
            
            
            Test.startTest();
              resultCaseDetails = HDFC_DASH_Case_Selector.getCaseRecsBasedOnString(fieldList,caseobject.Id, HDFC_DASH_Constants.ID_STRING);
            Test.stopTest();
        }
            system.assertNotEquals( null, resultCaseDetails);
            system.assertEquals(1,resultCaseDetails.size()); 
     }   
    
        /* 
    Method Name :getCaseRecsBasedOnListTest
    Parameters  :List<string> fieldList,List<String> queryParameter,String conditionField
    Description :This method would test the List of Cases based on single string condition sent to it.
    */
     
    static testmethod void getCaseRecsBasedOnListTest(){

        List<Case> resultCaseList = null;
        System.runAs(sysAdmin){
               
            List<String> fieldList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_HDB_Number,
            HDFC_DASH_Constants.Case_Status,HDFC_DASH_Constants.CREATEDDATE,HDFC_DASH_Constants.IsClosed,HDFC_DASH_Constants.HDFC_DASH_File_Number};
            
      Test.startTest();
            resultCaseList = HDFC_DASH_Case_Selector.getCaseRecsBasedOnList(fieldList,new list<String> {caseobject.Id},HDFC_DASH_Constants.ID_STRING);
            Test.stopTest();
            }
       system.assertEquals(1,resultCaseList.size()); 
   
         }
}