/*
Class: HDFC_DASH_NotificationMessage
Author: Anisha Arumugam
Date: 23 January 2022
Company: Accenture
Description: This class will will handle the Notifications callouts.
*/
global class HDFC_DASH_NotificationMessage 
{   
    
    /*Method: getMessageBody
Description: This method would call the generateMessageBody method.
*/
    @InvocableMethod(label = 'Notification - Get Message Body')
    global static List<OutputMessageBody> getMessageBody(List<Event> listInputEvents)
    { 
        List<OutputMessageBody> listMessagBody = new List<OutputMessageBody>();
        system.debug('***Invocable Method - getMessageBody() Input => ' + listInputEvents);
        system.debug('Input listEventId.size() => ' + listInputEvents.size());
        
        try
        {
            Map<String, HDFC_DASH_Notification_Config__mdt> mapAllNotificationConfig = HDFC_DASH_Notification_Config__mdt.getAll();
            
            for(Event e : listInputEvents)
            {
                string messageBodyTitle, messageBodyDisplay, messageBodyActual;
               String metadataRecordName = e.HDFC_DASH_User_Type__c + HDFC_DASH_Constants.UNDERSCORE + e.HDFC_DASH_Meeting_Type__c.replace(HDFC_DASH_Constants.STRING_DASH,HDFC_DASH_Constants.STRING_BLANK) + HDFC_DASH_Constants.STRING_EVENTNOTIFICATION;
                system.debug('***metadataRecordName => '+metadataRecordName);
                
                if(mapAllNotificationConfig?.containsKey(metadataRecordName))
                {
                    HDFC_DASH_Notification_Config__mdt mdt_NotificationConfig = mapAllNotificationConfig.get(metadataRecordName);  
                    
                    messageBodyTitle = HDFC_DASH_UtilityClass.generateMessageBody(mdt_NotificationConfig.HDFC_DASH_Message_Title_Template__c, mdt_NotificationConfig.HDFC_DASH_Message_Title_Mapping__c, mdt_NotificationConfig.HDFC_DASH_Message_Title_Object__c, e.id);
                    system.debug('***messageBodyTitle => '+messageBodyTitle);
                    
                    messageBodyDisplay = HDFC_DASH_UtilityClass.generateMessageBody(mdt_NotificationConfig.HDFC_DASH_Message_Display_Template__c, mdt_NotificationConfig.HDFC_DASH_Message_Display_Mapping__c, mdt_NotificationConfig.HDFC_DASH_Message_Display_Object__c, e.id);
                    system.debug('***messageBodyDisplay => '+messageBodyDisplay);
                    
                    messageBodyActual = HDFC_DASH_UtilityClass.generateMessageBody(mdt_NotificationConfig.HDFC_DASH_Message_Actual_Template__c, mdt_NotificationConfig.HDFC_DASH_Message_Actual_Mapping__c, mdt_NotificationConfig.HDFC_DASH_Message_Actual_Object__c, e.id);
                    system.debug('***messageBodyActual => '+messageBodyActual);
                    
                    OutputMessageBody output = new OutputMessageBody();
                    output.messageTitle = messageBodyTitle;
                    output.messageDisplay = messageBodyDisplay;
                    output.messageActual = messageBodyActual;
                    listMessagBody.add(output);
                }
            }
            system.debug('***listMessagBody => '+listMessagBody.size());
        }
        catch(exception e)
        {
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();
            system.debug('expDetails => '+expDetails);
         	 HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_NOTIFICATIONMESSAGE, HDFC_DASH_Constants.GETMESSAGEBODY, string.ValueOf(listInputEvents.size()), expDetails);
        }
        return listMessagBody;
    }
    
    /*Method: OutputMessageBody
Description: This method would construct OutputMessageBody.
*/
    global class OutputMessageBody
    {
        @InvocableVariable
        public String messageTitle;
        
        @InvocableVariable
        public String messageDisplay;
        
        @InvocableVariable
        public String messageActual;
    }
}