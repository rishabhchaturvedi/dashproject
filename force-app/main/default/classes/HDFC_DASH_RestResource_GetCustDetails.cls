@RestResource(urlMapping='/hdfc/getcustomerdetails/V1.0/*')
/*
Author: Kumar Gourav
Class Name:HDFC_DASH_RestResource_GetCustDetails
Date: 20 Oct 2021
Company: Accenture
Class Description: This class is to implement the Get Customer Details API
*/
global with sharing class HDFC_DASH_RestResource_GetCustDetails {
    
    /*
    Method:getCustomerDetails()
    Description:This method will get the details of customer
    */
    @HttpGet
    global static void getCustomerDetails() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders  = new Map<string,String>();
        String response;
        Exception exp;
        HDFC_DASH_APIResponse_GetCustDetails apiResponse = new HDFC_DASH_APIResponse_GetCustDetails();  
        try
        { 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestSer_GetCustDetails.getCustomerDetails(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                                            HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                                            + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            system.debug('expDetails => ' + expDetails);
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_GETCUSTDETAILS, HDFC_DASH_Constants.HANDLE_GETCUSTDETAILS_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_GET, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_GETCUSTDETAILS, request.requestBody.tostring(), (res.responseBody)?.tostring(), 
                                                                HDFC_DASH_Constants.METHOD_TYPE_GET, HDFC_DASH_Constants.HANDLE_GETCUSTDETAILS_REQUEST, exp, request.headers);        

            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                        res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
}