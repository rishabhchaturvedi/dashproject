Public class sendemailforstatutoryescalation{

public static void sendemailtocustomer(list<case> caselist)
{
 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<OrgWideEmailAddress> owea = new List<OrgWideEmailAddress>();
        Id emailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Complaint_EscalationSubmission'].Id;
        Id resolvedemailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =:'Complaint_EscalationSubmission_2'].Id;

         String owdLabel = Label.LoanComplaint;
         owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];
        for(case c:caselist)
        {
      
       
        if(c.HDFC_DASH_Sub_mode__c=='CGRO')
        {
         mail.setTemplateId(emailTemp);
         }
         else if(c.HDFC_DASH_Sub_mode__c=='MD Level 2- Website')
         {
         mail.setTemplateId(resolvedemailTemp);
         }
         
      
       list<string> emailaddresses= new list<string>();
       
                 
                    mail.setOrgWideEmailAddressId(owea[0].Id);
                   if(c.ContactEmail !=null)
                   {
                   emailaddresses.add(c.ContactEmail);  
                    mail.toaddresses=emailaddresses;
                    }
                    else
                    {
                      emailaddresses.add(c.suppliedemail); 
                    mail.toaddresses=emailaddresses;
                    }
                    mail.setTargetObjectId(c.contactid);
                    mail.setWhatId(c.Id);
                    mail.setSaveAsActivity(false);
                    mail.setTreatTargetObjectAsRecipient(false);
       /*    //create a post
         try{
         FeedItem cs = new FeedItem(
             Body = mail.plainTextBody,
             ParentId = c.Id         
         );
         insert cs;
         }Catch(Exception e){
             system.debug('Exception inside feed item...'+e);
         }*/
        
     //   system.debug('stat escalation email body'+mail.htmlbody);
       try{
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         
     }Catch(Exception e){
             system.debug('Exception inside feed item...'+e);
         }
    }


}
}