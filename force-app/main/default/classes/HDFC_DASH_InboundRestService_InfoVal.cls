/*
Author: Anisha Arumugam
Class:HDFC_DASH_InboundRestService_InfoVal
Description: Service Class for Store Info Validation API
*/
public inherited sharing class HDFC_DASH_InboundRestService_InfoVal 
{
    /* 
Method:processInfoValidation
Description: Method to process Applicant Info Validation
*/
    public static HDFC_DASH_APIResponse_InfoValidation processInfoValidation(RestRequest request)
    {      
        //Calling JSON Parser Class
        HDFC_DASH_ParseInfoValidation result = HDFC_DASH_ParseInfoValidation.parse(request);
        
        //Save Applicant Info Validation
        HDFC_DASH_APIResponse_InfoValidation apiRes = saveInfoValidation(result, request);
        
        return apiRes;  
    }
    
    /*   
Method: saveInfoValidation
Description: This method will save the data in Applicant Info Validation Object
*/
    private static HDFC_DASH_APIResponse_InfoValidation saveInfoValidation(HDFC_DASH_ParseInfoValidation result, RestRequest request)
    {             
        HDFC_DASH_APIResponse_InfoValidation apiRes =  new HDFC_DASH_APIResponse_InfoValidation();  
        system.debug('**** result '+result);
        
        List<string> listStateMasterIDs = new List<string>();
        List<string> listCountryMasterIDs = new List<string>();
        List<string> listCityMasterIDs = new List<string>();
        List<string> listPinCodeMasterIDs = new List<string>();
        List<string> listEmpMasterIDs = new List<string>();
        
        List<HDFC_DASH_Master_Table__c> listStateMaster = new List<HDFC_DASH_Master_Table__c>();
        List<HDFC_DASH_Master_Table__c> listCountryMaster = new List<HDFC_DASH_Master_Table__c>();
        List<HDFC_DASH_City_Master__c> listCityMaster = new List<HDFC_DASH_City_Master__c>();
        List<HDFC_DASH_Pincode_Master__c> listPinCodeMaster = new List<HDFC_DASH_Pincode_Master__c>();
        List<Account> listEmpMaster = new List<Account>(); 
        
        map<string, HDFC_DASH_Master_Table__c> mapStateMaster = new map<string, HDFC_DASH_Master_Table__c>();
        map<string, HDFC_DASH_Master_Table__c> mapCountryMaster = new map<string, HDFC_DASH_Master_Table__c>();
        map<string, HDFC_DASH_City_Master__c> mapCityMaster = new map<string, HDFC_DASH_City_Master__c>();
        map<string, HDFC_DASH_Pincode_Master__c> mapPinCodeMaster = new map<string, HDFC_DASH_Pincode_Master__c>();
        map<string, Account> mapEmpMaster = new map<string, Account>();
        Map<String, Object> mapInfoValResponse = new Map<String, Object>();
        
        listStateMasterIDs.add(result?.applicantInfoValidation?.prePopulatedAddressDetails?.stateCode);
        listCountryMasterIDs.add(result?.applicantInfoValidation?.prePopulatedAddressDetails?.countryCode);
        listCountryMasterIDs.add(result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prCountryCode);
        listCityMasterIDs.add(result?.applicantInfoValidation?.prePopulatedAddressDetails?.cityCode);
        listPinCodeMasterIDs.add(result?.applicantInfoValidation?.prePopulatedAddressDetails?.pinCode);
        listEmpMasterIDs.add(result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employerCode);
        
        //Calling Selector to query Master Tables     
        listStateMaster = HDFC_DASH_MasterTables_Selector.getMasterTableWithRecordType(listStateMasterIDs, HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_STATECODE);    
        listCountryMaster = HDFC_DASH_MasterTables_Selector.getMasterTableWithRecordType(listCountryMasterIDs, HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_COUNTRYCODE);          
        listCityMaster = HDFC_DASH_MasterTables_Selector.getCityMaster(listCityMasterIDs);               
        listPinCodeMaster = HDFC_DASH_MasterTables_Selector.getPinCodeMaster(listPinCodeMasterIDs);
        listEmpMaster = HDFC_DASH_MasterTables_Selector.getEmpMaster(listEmpMasterIDs);
        
        for(HDFC_DASH_Master_Table__c objMT : listStateMaster)
            mapStateMaster.put(objMT.HDFC_DASH_CD_Val__c, objMT);
        
        for(HDFC_DASH_Master_Table__c objMT : listCountryMaster)
            mapCountryMaster.put(objMT.HDFC_DASH_CD_Val__c, objMT);
        
        for(HDFC_DASH_City_Master__c objCM : listCityMaster)
            mapCityMaster.put(objCM.HDFC_DASH_City_Code__c, objCM);
        
        for(HDFC_DASH_Pincode_Master__c objPM : listPinCodeMaster)
            mapPinCodeMaster.put(objPM.HDFC_DASH_Pincode__c, objPM);
        
        for(Account objEM : listEmpMaster)
            mapEmpMaster.put(objEM.HDFC_DASH_Corp_Cust_No__c, objEM);
        
        
        if(!String.isBlank(result?.sfApplicationId) || !String.isBlank(result?.sfApplicantId))
        {
            HDFC_DASH_Info_Validation__c objInfoVal = new HDFC_DASH_Info_Validation__c();
            
            objInfoVal.HDFC_DASH_Application__c=result?.sfApplicationId;
            objInfoVal.HDFC_DASH_Application_Number__c = result?.sfApplicationNumber;
            objInfoVal.HDFC_DASH_Applicant_Details__c = result?.sfApplicantId;
            objInfoVal.HDFC_DASH_Contact__c = result?.sfCustomerId;
            system.debug('objInfoVal.HDFC_DASH_Contact__c ' +objInfoVal.HDFC_DASH_Contact__c);
            objInfoVal.HDFC_DASH_Customer_Number__c = result?.sfCustomerNumber;
            objInfoVal.HDFC_DASH_File_Number__c = result?.fileNumber;
            system.debug('case '+result?.sfCaseId);
            objInfoVal.HDFC_DASH_Case__c = result?.sfCaseId;
            system.debug('case '+objInfoVal.HDFC_DASH_Case__c);
            objInfoVal.HDFC_DASH_Case_Number__c = result?.sfCaseNumber;
            objInfoVal.HDFC_DASH_Document_Status__c = result?.documentStatus; 
            //objInfoVal.HDFC_DASH_ILPS_Cust_Number__c= result?.customerNumber;
            objInfoVal.HDFC_DASH_Applicant_Capacity__c= result?.customerCapacity;
            objInfoVal.HDFC_DASH_Uploaded_Date__c= result?.uploadedDate;
            objInfoVal.HDFC_DASH_Info_Validation_Record_Type__c= result?.infoValidationRecordType;
            objInfoVal.HDFC_DASH_Validation_Type__c = result?.applicantInfoValidation?.validationType;
            objInfoVal.HDFC_DASH_Validated_With__c = result?.applicantInfoValidation?.source;
            objInfoVal.HDFC_DASH_Doc_Type_Code__c=result?.applicantInfoValidation?.docTypeCode;
            objInfoVal.HDFC_DASH_Doc_Type__c = result?.applicantInfoValidation?.docType;
            objInfoVal.HDFC_DASH_User_Input_Value__c = result?.applicantInfoValidation?.docNumber;
            objInfoVal.HDFC_DASH_Date_Of_Issue__c =  result?.applicantInfoValidation?.dateOfIssue;
            objInfoVal.HDFC_DASH_Date_Of_Expiry__c = result?.applicantInfoValidation?.dateOfExpiry;
            objInfoVal.HDFC_DASH_Salutation__c = result?.applicantInfoValidation?.salutation;
            objInfoVal.HDFC_DASH_FirstName__c = result?.applicantInfoValidation?.firstName;
            objInfoVal.HDFC_DASH_MiddleName__c = result?.applicantInfoValidation?.middleName; 
            objInfoVal.HDFC_DASH_LastName__c = result?.applicantInfoValidation?.lastName; 
            objInfoVal.HDFC_DASH_Date_Of_Birth__c = result?.applicantInfoValidation?.dateOfBirth; 
            system.debug('***gender result '+result?.applicantInfoValidation?.genderCode);
            objInfoVal.HDFC_DASH_Gender__c = result?.applicantInfoValidation?.genderCode;
            objInfoVal.HDFC_DASH_Address_Type__c = result?.applicantInfoValidation?.addressType;
            
            
            
            objInfoVal.HDFC_DASH_Document_File_Uploaded__c = result?.applicantInfoValidation?.documentFileUploaded == null? false : result?.applicantInfoValidation?.documentFileUploaded; 
            objInfoVal.HDFC_DASH_Loan_Outstanding_Letter_Upload__c = result?.applicantInfoValidation?.loanOutstandingLetterUpload == null? false : result?.applicantInfoValidation?.loanOutstandingLetterUpload;
            objInfoVal.HDFC_DASH_Property_Cost_Sheet_Upload__c = result?.applicantInfoValidation?.propertyCostSheetUpload == null? false : result?.applicantInfoValidation?.propertyCostSheetUpload;
            objInfoVal.HDFC_DASH_DMS_Document_Id__c = result?.applicantInfoValidation?.dmsDocumentId;
            objInfoVal.HDFC_DASH_Financial_Info_Document_Upload__c = result?.applicantInfoValidation?.financialInfoDocumentUpload == null? false : result?.applicantInfoValidation?.financialInfoDocumentUpload;
           
            objInfoVal.HDFC_DASH_Account_Holder_Name__c = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.accountHolderName;
            objInfoVal.HDFC_DASH_Bank_Name__c = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.bankName;
            objInfoVal.HDFC_DASH_Bank_Code__c = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.bankCode;
            objInfoVal.HDFC_DASH_Loan_Type__c = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.loanType;
            objInfoVal.HDFC_DASH_Loan_Type_Code__c   = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.loanTypeCode;
            objInfoVal.HDFC_DASH_Outstanding_Amount__c = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.outstandingAmount;
            objInfoVal.HDFC_DASH_Installment_Amount__c = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.installmentAmount;
            objInfoVal.HDFC_DASH_Balance_Term_Of_Loan_In_Months__c = result?.applicantInfoValidation?.prePopulatedFinancialInformation?.balanceTermOfLoanInMonths;
            
            objInfoVal.HDFC_DASH_Content_Document_Id__c = result?.applicantInfoValidation?.applicantImage?.contentDocumentId;
            objInfoVal.HDFC_DASH_Image_Source__c = result?.applicantInfoValidation?.applicantImage?.imageSource; 
            objInfoVal.HDFC_DASH_Valid_Image__c = result?.applicantInfoValidation?.applicantImage?.validImage;
            
            objInfoVal.HDFC_DASH_Income_Proof__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.incomeProof; 
            objInfoVal.HDFC_DASH_Salary_Month__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salaryMonth; 
            objInfoVal.HDFC_DASH_Salary_Year__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salaryYear; 
            objInfoVal.HDFC_DASH_Addl_Income_Document_Type__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.additionalIncomeDocumentType; 
            objInfoVal.HDFC_DASH_Bank_Statement_Upload__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.bankStatementUpload == null? false : result?.applicantInfoValidation?.prePopulatedSalaryInformation?.bankStatementUpload; 
            objInfoVal.HDFC_DASH_Salary_Slip_Upload_Month__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadMonth == null? false : result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadMonth;
            objInfoVal.HDFC_DASH_Salary_Slip_Upload_Week1__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek1 == null? false : result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek1;
            objInfoVal.HDFC_DASH_Salary_Slip_Upload_Week2__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek2 == null? false : result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek2;
            objInfoVal.HDFC_DASH_Salary_Slip_Upload_Week3__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek3 == null? false : result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek3;
            objInfoVal.HDFC_DASH_Salary_Slip_Upload_Week4__c = result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek4 == null? false : result?.applicantInfoValidation?.prePopulatedSalaryInformation?.salarySlipUploadWeek4;
            
            objInfoVal.HDFC_DASH_Account_Type__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.accountType;
            objInfoVal.HDFC_DASH_From_Date__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.fromDate;
            objInfoVal.HDFC_DASH_To_Date__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.toDate;
            objInfoVal.HDFC_DASH_Account_Holder_Name__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.accountHolderName;
            objInfoVal.HDFC_DASH_Bank_Name__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.bankName;
            objInfoVal.HDFC_DASH_Bank_Code__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.bankCode;    
            objInfoVal.HDFC_DASH_Account_Number__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.accountNumber;
            objInfoVal.HDFC_DASH_Ifsc_Code__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.ifscCode;
            objInfoVal.HDFC_DASH_Is_Salary_Account__c = result?.applicantInfoValidation?.PrePopulatedBankDetails?.isSalaryAccount;
            
            objInfoVal.HDFC_DASH_Employer_Name__c = result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employerName; 
            objInfoVal.HDFC_DASH_Employer_Code__c = result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employerCode; 
            if(!String.isBlank(result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employerCode) && mapEmpMaster.get(result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employerCode) != null)
            {
                objInfoVal.HDFC_DASH_Employer_Name__c = mapEmpMaster.get(result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employerCode)?.Name;
                objInfoVal.HDFC_DASH_Employer_RecordID__c = mapEmpMaster.get(result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employerCode)?.id; 
            }
            
            objInfoVal.HDFC_DASH_Employee_Number__c = result?.applicantInfoValidation?.prePopulatedEmployerInformation?.employeeNumber; 
            objInfoVal.HDFC_DASH_Designation__c = result?.applicantInfoValidation?.prePopulatedEmployerInformation?.designation; 
            objInfoVal.HDFC_DASH_Department__c = result?.applicantInfoValidation?.prePopulatedEmployerInformation?.department; 
            objInfoVal.HDFC_DASH_Job_Duration_In_Months__c =  result?.applicantInfoValidation?.prePopulatedEmployerInformation?.jobDurationInMonths;
            
            objInfoVal.HDFC_DASH_Passport_Number__c = result?.applicantInfoValidation?.prePopulatedPassportInformation?.passportNumber;
            
            objInfoVal.HDFC_DASH_Nationality__c = result?.applicantInfoValidation?.prePopulatedPassportInformation?.nationality; 
            
            objInfoVal.HDFC_DASH_PR_Number__c = result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prNumber; 
            objInfoVal.HDFC_DASH_PR_Validity_Date__c = result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prValidityDate; 
            
            objInfoVal.HDFC_DASH_PR_Country__c = result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prCountry;
            objInfoVal.HDFC_DASH_PR_Country_Code__c = result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prCountryCode; 
            if(!String.isBlank(result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prCountryCode) && mapCountryMaster.get(result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prCountryCode) != null)
            {
                objInfoVal.HDFC_DASH_PR_Country__c = mapCountryMaster.get(result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prCountryCode)?.HDFC_DASH_CD_Desc__c;
                objInfoVal.HDFC_DASH_PR_Country_RecordId__c = mapCountryMaster.get(result?.applicantInfoValidation?.prePopulatedPermanentResidency?.prCountryCode)?.id; 
            }
            
            objInfoVal.HDFC_DASH_Address_Line1__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.addressLine1; 
            objInfoVal.HDFC_DASH_Address_Line2__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.addressLine2; 
            objInfoVal.HDFC_DASH_Address_Line3__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.addressLine3; 
            objInfoVal.HDFC_DASH_Address_Line4__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.addressLine4; 
            objInfoVal.HDFC_DASH_Landmark__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.landmark; 
            objInfoVal.HDFC_DASH_Taluka__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.taluka; 
            objInfoVal.HDFC_DASH_District__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.district; 
            
            objInfoVal.HDFC_DASH_City__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.city; 
            objInfoVal.HDFC_DASH_City_Code__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.cityCode; 
            if(!String.isBlank(result?.applicantInfoValidation?.prePopulatedAddressDetails?.cityCode) && mapCityMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.cityCode) != null)
            {
                objInfoVal.HDFC_DASH_City__c = mapCityMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.cityCode)?.HDFC_DASH_City_Name__c;
                objInfoVal.HDFC_DASH_City_RecordID__c = mapCityMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.cityCode)?.id; 
            }
            
            objInfoVal.HDFC_DASH_State__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.state; 
            objInfoVal.HDFC_DASH_State_Code__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.stateCode; 
            if(!String.isBlank(result?.applicantInfoValidation?.prePopulatedAddressDetails?.stateCode) && mapStateMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.stateCode) != null)
            {
                objInfoVal.HDFC_DASH_State__c = mapStateMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.stateCode)?.HDFC_DASH_CD_Desc__c;
                objInfoVal.HDFC_DASH_State_RecordID__c = mapStateMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.stateCode)?.id; 
            }
            
            objInfoVal.HDFC_DASH_Country__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.country; 
            objInfoVal.HDFC_DASH_Country_Code__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.countryCode; 
            if(!String.isBlank(result?.applicantInfoValidation?.prePopulatedAddressDetails?.countryCode) && mapCountryMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.countryCode) != null)
            {
                objInfoVal.HDFC_DASH_Country__c = mapCountryMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.countryCode)?.HDFC_DASH_CD_Desc__c;
                objInfoVal.HDFC_DASH_Country_RecordID__c = mapCountryMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.countryCode)?.id; 
            }
            
            objInfoVal.HDFC_DASH_Pincode__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.pinCode; 
            objInfoVal.HDFC_DASH_Post_Office_Name__c = result?.applicantInfoValidation?.prePopulatedAddressDetails?.postOfficeName; 
            if(!String.isBlank(result?.applicantInfoValidation?.prePopulatedAddressDetails?.pinCode) && mapPinCodeMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.pinCode) != null)
            {
                objInfoVal.HDFC_DASH_Pincode_RecordID__c = mapPinCodeMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.pinCode)?.id; 
                objInfoVal.HDFC_DASH_Post_Office_Name__c = mapPinCodeMaster.get(result?.applicantInfoValidation?.prePopulatedAddressDetails?.pinCode)?.HDFC_DASH_Place__c; 
            }
            
            objInfoVal.HDFC_DASH_Designation_Entity_Type__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.designationAndEntityType; 
            objInfoVal.HDFC_DASH_Business_Name__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.businessName; 
            objInfoVal.HDFC_DASH_Year_Of_Incorporation__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.yearOfIncorporation; 
            objInfoVal.HDFC_DASH_Annual_Turnover__c  = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.annualTurnover; 
            objInfoVal.HDFC_DASH_Nature_Of_Business__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.natureOfBusiness; 
            objInfoVal.HDFC_DASH_Industry_Type__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.industryType; 
            objInfoVal.HDFC_DASH_Business_PAN__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.businessPAN; 
            objInfoVal.HDFC_DASH_Udyam_Registration_number__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.udyamRegistrationNumber;
            objInfoVal.HDFC_DASH_CIN_Number__c = result?.applicantInfoValidation?.prePopulatedBusinessInformation?.cinNumber; 
            
            objInfoVal.HDFC_DASH_GST_Number__c = result?.applicantInfoValidation?.prePopulatedEmpGstBusinessDetails?.gstNumber; 
            objInfoVal.HDFC_DASH_Emp_Business_GST_State__c = result?.applicantInfoValidation?.prePopulatedEmpGstBusinessDetails?.empGstState; 
            objInfoVal.HDFC_DASH_Emp_Business_GST_State_Code__c = result?.applicantInfoValidation?.prePopulatedEmpGstBusinessDetails?.empGstStateCode; 
            
            objInfoVal.HDFC_DASH_Document_Extractable__c=result?.applicantInfoValidation?.docExtractable;
            objInfoVal.HDFC_DASH_Document_Image_Origin__c=result?.applicantInfoValidation?.docImageOrigin;
            objInfoVal.HDFC_DASH_Document_Scanned_By__c=result?.applicantInfoValidation?.docScannedBy;
            objInfoVal.HDFC_DASH_Mon_Of_GST_Return__c=result?.applicantInfoValidation?.monthOfGstReturn;
            objInfoVal.HDFC_DASH_Assessment_Year__c=result?.applicantInfoValidation?.assessmentYear;
            objInfoVal.HDFC_DASH_Financial_Year__c=result?.applicantInfoValidation?.financialYear;
            objInfoVal.HDFC_DASH_Total_Amount_Paid__c = result?.applicantInfoValidation?.totalAmountPaid;
            objInfoVal.HDFC_DASH_No_Of_Quarters_Paid__c=result?.applicantInfoValidation?.noOfQuartersPaid;
            objInfoVal.HDFC_DASH_Proof_Of_Communication__c=result?.applicantInfoValidation?.proofOfCommunication;
            objInfoVal.HDFC_DASH_BCP_Doc_Type__c=result?.applicantInfoValidation?.bcpDocType;
            objInfoVal.HDFC_DASH_BCP_Doc_Code__c=result?.applicantInfoValidation?.bcpDocTypeCode;
            objInfoVal.HDFC_DASH_Location__Latitude__s = result?.applicantInfoValidation?.latitude;
            objInfoVal.HDFC_DASH_Location__Longitude__s = result?.applicantInfoValidation?.longitude;            
            objInfoVal.HDFC_DASH_KYC_Doc_Type__c=result?.applicantInfoValidation?.kycDocType;
            objInfoVal.HDFC_DASH_KYC_Doc_Code__c=result?.applicantInfoValidation?.kycDocTypeCode;
            objInfoVal.HDFC_DASH_Storage_Identifier__c=result?.applicantInfoValidation?.storageIdentifier;
            objInfoVal.HDFC_DASH_Storage_Type__c=result?.applicantInfoValidation?.storageType;
            objInfoVal.HDFC_DASH_Proof_Of_Communication_Code__c=result?.applicantInfoValidation?.proofOfCommunicationCode;
            
            //Deserialize and store Validation Response and OCR Response
            Map<String, Object> mapRequestBody = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
            string serializedAppInfoVal = JSON.serialize(mapRequestBody.get(HDFC_DASH_Constants.APPLICANT_INFO_VALIDATION));
            Map<String, Object> mapAppInfoVal = (Map<String, Object>)JSON.deserializeUntyped(serializedAppInfoVal);  
            
            if(mapAppInfoVal.get(HDFC_DASH_Constants.OCR_RESPONSE) != null)
                objInfoVal.HDFC_DASH_OCR_Response__c = JSON.serialize(mapAppInfoVal.get(HDFC_DASH_Constants.OCR_RESPONSE));  
            if(mapAppInfoVal.get(HDFC_DASH_Constants.VALIDATIONRESPONSE) != null)
                objInfoVal.HDFC_DASH_Validation_Response__c = JSON.serialize(mapAppInfoVal.get(HDFC_DASH_Constants.VALIDATIONRESPONSE));
            
            
            
            system.debug('** fileName***'+result?.applicantInfoValidation?.applicantImage?.fileName);
            system.debug('** fileExtension***'+result?.applicantInfoValidation?.applicantImage?.fileExtension);
            system.debug('** base64File***'+result?.applicantInfoValidation?.applicantImage?.base64File);
            
                if(String.isNotBlank(result?.applicantInfoValidation?.applicantImage?.fileName) && String.isNotBlank(result?.applicantInfoValidation?.applicantImage?.fileExtension) && String.isNotBlank(result?.applicantInfoValidation?.applicantImage?.base64File))
                {
                    ContentVersion contentVer = new ContentVersion();
                    contentVer.Title = result?.applicantInfoValidation?.applicantImage?.fileName;
                    contentVer.PathOnClient = ContentVer.Title + HDFC_DASH_Constants.STRING_DOT + result?.applicantInfoValidation?.applicantImage?.fileExtension; 
                    contentVer.VersionData = EncodingUtil.base64Decode(result?.applicantInfoValidation?.applicantImage?.base64File);
                    if(String.isNotBlank(result?.applicantInfoValidation?.applicantImage?.contentDocumentId)){
                        contentVer.contentDocumentId = result?.applicantInfoValidation?.applicantImage?.contentDocumentId;
                    }
                    Insert contentVer;
                    
                    if(contentVer?.id != null)
                    {
                        List<String> listConverId = new List<String>();
                        listConverId.add(contentVer.id);
                        
                        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.ContentDocumentId};
                            List<ContentVersion> listConVer = HDFC_DASH_ContentDocument_Selector.getContentVersion(listConverId, fieldList, HDFC_DASH_Constants.ID_STRING);
                        objInfoVal.HDFC_DASH_Content_Document_Id__c = listConVer[HDFC_DASH_Constants.INT_ZERO]?.ContentDocumentId;
                        
                        if(String.isBlank(result?.applicantInfoValidation?.applicantImage?.contentDocumentId) && String.isNotBlank(result?.sfCustomerId) && !listConVer.isEmpty())
                        {   
                            List<String> listConId = new List<String>{result?.sfCustomerId};
                                List<Contact> listCon = HDFC_DASH_Contact_Selector.getContacts(listConId, new List<String>{HDFC_DASH_Constants.STRING_ID,HDFC_DASH_Constants.ACCOUNT_RECORDTYPE_NAME,HDFC_DASH_Constants.STRING_ACCOUNTID}, HDFC_DASH_Constants.STRING_ID);
                            system.debug('cont list'+listCon);
                            if(!listCon.isEmpty())
                            {
                                system.debug('inside if block');
                                ContentDocumentLink contentDocLink = new ContentDocumentLink();
                                contentDocLink.LinkedEntityId = result?.sfCustomerId;
                                contentDocLink.ContentDocumentId = listConVer[HDFC_DASH_Constants.INT_ZERO]?.ContentDocumentId;
                                Insert contentDocLink;
                                
                                if(listCon[HDFC_DASH_Constants.INT_ZERO].Account.RecordType.name != HDFC_DASH_Constants.PERSON_ACCOUNT){
                                    ContentDocumentLink contentDocLinkForAcc = new ContentDocumentLink();
                                    contentDocLinkForAcc.LinkedEntityId = listCon[HDFC_DASH_Constants.INT_ZERO]?.AccountId;
                                    contentDocLinkForAcc.ContentDocumentId = listConVer[HDFC_DASH_Constants.INT_ZERO]?.ContentDocumentId;
                                    Insert contentDocLinkForAcc;
                                }
                            }
                            else
                            {
                                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_Error_Customer_NotExist); 
                            }
                        } 
                    }
                } 
            
            Insert objInfoVal;
            apiRes.SuccessMessage = HDFC_DASH_Constants.INFOVALIDATION_SUCCESSMESSAGE;
            apiRes.sfInfoValidationId = objInfoVal.id;
        } 
        else
        {
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_MISSING); 
        }
        return apiRes;
    }  
}