/*
Class: HDFC_DASH_SMSIntegration
Author: Santhi Gudala
Date: 26 Oct 2021
Company: Accenture
Description: Queueable Class to make SMS Details Callout to ILPS 
*/
public inherited sharing class HDFC_DASH_SMSIntegration implements Queueable, Database.AllowsCallouts
{
    public Map<String,String> requestHeader = new Map<String,String>();
    HttpResponse response;
    RestRequest request; 
    Exception exp;
    string mobilenumber;
    string template;
    string taskId;
    Task objTask = new Task();
    String messageBody;
    Task whatsAppTask;
    String waMessageBody;
    
    /**
ConstructorName : HDFC_DASH_SMSIntegration
* Parameters : Task objTask
* Description : It is a constructor for class HDFC_DASH_SMSIntegration.
*/
    public HDFC_DASH_SMSIntegration(Task objTask, String messageBody)
    {
        this.objTask = objTask;
        this.messageBody = messageBody;
    }
    /**
ConstructorName : HDFC_DASH_SMSIntegration
* Parameters : Task objTask, String messageBody,Task whatsAppTask,String waMessageBody
* Description : It is a constructor for class HDFC_DASH_SMSIntegration.
*/
    public HDFC_DASH_SMSIntegration(Task objTask, String messageBody,Task whatsAppTask,String waMessageBody)
    {
        
        this.objTask = objTask;
        this.messageBody = messageBody;
        this.whatsAppTask = whatsAppTask;
        this.waMessageBody = waMessageBody;
        
    }
    
    /*
Method: execute
Description: This method will make the SMS callout to ILPS.
*/
    public void execute(QueueableContext context) 
    {
        
        try
        {       
            doSMSCallout();
            if(waMessageBody!=NULL && whatsAppTask !=NULL){
                HDFC_DASH_WhatsappIntegration whatsAppCallout = new HDFC_DASH_WhatsappIntegration(whatsAppTask,waMessageBody); 
                //whatsAppCallout.doWhatsappCallout();
                system.enqueueJob(whatsAppCallout);
                //String jobId = System.enqueueJob( new HDFC_DASH_WhatsappIntegration(whatsAppTask,waMessageBody)); 
            }
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            //system.debug('expDetails => ' + expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_SMS_ILPSCALLOUT, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }
    
    /*
Method: doSMSCallout
Description: This method will make the SMS callout to ILPS.
*/
    public void doSMSCallout()
    {
        string postProcessDataStr;
        string reqBody = HDFC_DASH_Constants.STRING_BLANK;
        HDFC_DASH_CommunicationWrapper.ProcessData objProcessData;
        if(messageBody != null && (!messageBody.contains(HDFC_DASH_Constants.STRING_HDFC_DASH))) 
        {
            Interface_Settings__c is_Sms_Integration = Interface_Settings__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_SMS_ILPSCALLOUT);
            String baseURL = is_Sms_Integration.End_point__c;
            String encodedMessgeBody = EncodingUtil.URLENCODE(messageBody, HDFC_DASH_Constants.STRING_UTF);
            String params = HDFC_DASH_Constants.STRING_SMOBILE +  objTask.HDFC_DASH_Target_Number__c  + HDFC_DASH_Constants.STRING_SMSG + encodedMessgeBody +
                HDFC_DASH_Constants.AND_OPERATOR + is_Sms_Integration.HDFC_DASH_Process_Name__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ZERO] + HDFC_DASH_Constants.STRING_EQUALTO + is_Sms_Integration.HDFC_DASH_Process_Name__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ONE] +
                HDFC_DASH_Constants.AND_OPERATOR + is_Sms_Integration.HDFC_DASH_Request_No__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ZERO] + HDFC_DASH_Constants.STRING_EQUALTO + is_Sms_Integration.HDFC_DASH_Request_No__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ONE ]+
                HDFC_DASH_Constants.AND_OPERATOR + is_Sms_Integration.HDFC_DASH_UID__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ZERO] + HDFC_DASH_Constants.STRING_EQUALTO + is_Sms_Integration.HDFC_DASH_UID__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ONE] + 
                HDFC_DASH_Constants.AND_OPERATOR + is_Sms_Integration.HDFC_DASH_PWD__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ZERO] + HDFC_DASH_Constants.STRING_EQUALTO + is_Sms_Integration.HDFC_DASH_PWD__c.split(HDFC_DASH_Constants.COMMA)[HDFC_DASH_Constants.INT_ONE];
            String endpoint = baseURL + params;
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();     
            interfaceObj.skipCustomSettingEndPoint = true;
            interfaceObj.overriddenEndPoint = endpoint;
            
            //Populate headers
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.HDFC_DASH_SMS_ILPSCALLOUT);
            objProcessData = new HDFC_DASH_CommunicationWrapper.ProcessData(objTask.id, messageBody, null);   
            postProcessDataStr = json.serialize(objProcessData);
            //Make the SMS Callout
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.HDFC_DASH_SMS_ILPSCALLOUT, HDFC_DASH_Constants.METHOD_TYPE_POST, reqBody, requestHeader, postProcessDataStr, requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));  
            Integration_Message_Log__c objLog = interfaceObj.objIntMsgLog;
            objProcessData = new HDFC_DASH_CommunicationWrapper.ProcessData(objTask.id, messageBody, objLog?.id);   
            postProcessDataStr = json.serialize(objProcessData);
            //Post response processing          
            HDFC_DASH_SMSResponseHandler responseHandler = new HDFC_DASH_SMSResponseHandler();
            responseHandler.doRetryProcess(response, postProcessDataStr);
        }
    }
}