/**
className: HDFC_DASH_ContactTriggerOpsTest 
DevelopedBy: Tejeswari
Date: 20 November 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_ContactTriggerOps class.
*/
@isTest(SeeAllData = false) 
private with sharing class HDFC_DASH_ContactTriggerOpsTest {
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static User userRec = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string REPORTSTO_ID= '123456';
    private static final string ROLE_NAME= 'Sales Officer';
    private static final string USER_NAME= 'usertestclass@testorg.com';
    private static final string FEDERATION_IDENTIFIER= 'Johnrocky@abc.com';
    private static final string Email= 'Johnrock@abc.com';
    private static final string AGENCY_CODE= '12345';
    private static final string DSA_GROUP = 'HDFC_SALES';
    private static final string DSA_GROUP_NONSALES = 'CCST';
    private static final string EXEC_CODE= '123456';
    private static final string MOBILE_PHONE= '914567678822';
    /* 
Method Name :testFilterConWithRepId_EmpNo
Description :This method would test the method in trigger helper for the list of Contact based on User Id field.
*/
    @isTest 
    public static void testFilterConWithRepId_EmpNo(){
        Exception testException;
        acc.HDFC_DASH_DSA_Group__c = DSA_GROUP;
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.AGENCY_CODE).getRecordTypeId();
        insert acc;
        Contact contactRec = HDFC_DASH_TestDataFactory_Contacts.createBasicContactsWithAccountID(acc.id);
        contactRec.HDFC_DASH_Emp_Number__c = REPORTSTO_ID;
        insert contactRec;
        Contact conRec = HDFC_DASH_TestDataFactory_Contacts.createBasicContacts();
        conRec.HDFC_DASH_Agency_Code__c = AGENCY_CODE;
        conRec.HDFC_DASH_ReportsTo_External_Id__c = REPORTSTO_ID;
        System.runAs(sysAdmin) {
            Test.startTest();
            
            try{
                Database.insert(conRec);   
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            Contact objContact = [Select id, ReportsToId from Contact where id=:conRec.id];
            System.assertNotEquals(null, Contact.ReportsToId);
            system.assertEquals(null, testException);
        }
    }
    /* 
Method Name :testFilterContactsWithReportsToIdField
Description :This method would test the method in trigger helper for the list of Contact based on Executive code field.
*/
    @isTest 
    public static void testFilterConWithRepId_ExeCd(){
        Exception testException;
        acc.HDFC_DASH_DSA_Group__c = DSA_GROUP_NONSALES;
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.AGENCY_CODE).getRecordTypeId();
        insert acc;
        Contact contactRec = HDFC_DASH_TestDataFactory_Contacts.createBasicContactsWithAccountID(acc.id);
        contactRec.HDFC_DASH_Executive_Code__c = EXEC_CODE;
        insert contactRec;
        Contact conRec = HDFC_DASH_TestDataFactory_Contacts.createBasicContacts();
        conRec.HDFC_DASH_Agency_Code__c = AGENCY_CODE;
        conRec.HDFC_DASH_ReportsTo_External_Id__c = REPORTSTO_ID;
        System.runAs(sysAdmin) {
            Test.startTest();
            
            try{
                Database.insert(conRec);   
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            Contact objContact = [Select id, ReportsToId from Contact where id=:conRec.id];
            System.assertNotEquals(null, Contact.ReportsToId);
            system.assertEquals(null, testException);
        }
    }
    /* 
Method Name :testFilterContactsForCreatingUser
Description :This method would test the method in trigger helper for the list of Contact.
*/ 
    @isTest 
    public static void testFilterContactsForCreatingUser(){
        Exception testException;
        Account accRec = HDFC_DASH_TestDataFactory_Accounts.createBasicAccount();
        accRec.HDFC_DASH_DSA_Group__c = DSA_GROUP;
        User userRec = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        UserRole role = [SELECT Id FROM UserRole WHERE Name=:ROLE_NAME];
       userRec.UserRoleId=role.id;
        userRec.UserName = USER_NAME+string.valueOf(datetime.now().getTime());
        userRec.FederationIdentifier = FEDERATION_IDENTIFIER;
        
        System.runAs(sysAdmin) {
            insert userRec;
            accrec.OwnerId=userRec.Id;
            insert accRec;
            Contact conRec = HDFC_DASH_TestDataFactory_Contacts.createBasicContactsWithAccountID(accRec.id);
            conRec.HDFC_DASH_Function_Role_Code__c =  HDFC_DASH_Constants.STRING_TL;
            conRec.Email = Email;
            conRec.HDFC_DASH_Federation_Identifier__c = Email;
            
            Test.startTest();
            
            try{
                Database.insert(conRec);   
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            System.assertNotEquals(null, conRec.HDFC_DASH_Function_Role_Code__c);
            system.assertEquals(null, testException);
        }
    }
  
}