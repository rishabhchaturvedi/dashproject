/**
className: HDFC_DASH_InboundRest_StoreDetailsTest
DevelopedBy: Anmol Srivastava
Date: 20 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestService_StoreDetail
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_InboundRest_StoreDetailsTest {
    private static final string SUCCESS ='Success';
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(con.id, app.id);
    private static Interface_Framework_Settings__c ifSetting = new Interface_Framework_Settings__c();
    private static Interface_Settings__c interfaceSetting = new Interface_Settings__c();
    private static final string requestBody ='{"storeDetails":{"sfApplicationId":"'+app.id+'","sfApplicationNo":"'+app.HDFC_DASH_ApplicationNumber__c+'","cibilTransunion":"100032566162","cibilTransunionTechnicalFailure":false,"leadStageNumber":6,"leadStageDescription":"leadStageDescription","postPaymentspotOfferEligible":"Y","postPaymentSpotOfferEligibilityTechnicalFailure":false}}';
    private static final string reqBodywithouApp ='{"storeDetails":{"sfApplicationId":"","sfApplicationNo":"10000113","cibilTransunion":"100032566162","cibilTransunionTechnicalFailure":false,"leadStageNumber":6,"leadStageDescription":"leadStageDescription","postPaymentspotOfferEligible":"Y","postPaymentSpotOfferEligibilityTechnicalFailure":false}}';
    private static final string reqBodyWrongApp ='{"storeDetails":{"sfApplicationId":"0067D00000B2iN5QAJ","sfApplicationNo":"10000113","cibilTransunion":"100032566162","cibilTransunionTechnicalFailure":false,"leadStageNumber":6,"leadStageDescription":"leadStageDescription","postPaymentspotOfferEligible":"Y","postPaymentSpotOfferEligibilityTechnicalFailure":false}}';
    /* 
    Method Name : testprocessStoreDetails
    Parameters  : 
    Description : This method is will call  HDFC_DASH_InboundRestService_StoreDetail.processFeeDetails().
    */
    static testmethod void testprocessStoreDetails(){   
        System.runAs(sysAdmin){
            Id Record = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByDeveloperName().get(HDFC_DASH_Constants.PRIMARY_APPLICANT).getRecordTypeId();
            appDetails.RecordTypeId=Record;
            Test.startTest();
            database.insert(appDetails);
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            HDFC_DASH_APIResponse_StoreDetail StoreDetRes =  HDFC_DASH_InboundRestService_StoreDetail.processStoreDetails(request);
            Test.stopTest();
            System.assertNotEquals(null,StoreDetRes,SUCCESS);
        }   
    }
    /* 
    Method Name : testSaveDetails
    Parameters  : 
    Description : This method is will call  HDFC_DASH_InboundRestService_StoreDetail.saveDetails().
    */
    static testmethod void testSaveDetails(){
        System.runAs(sysAdmin){    
            Id Record = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByDeveloperName().get(HDFC_DASH_Constants.PRIMARY_APPLICANT).getRecordTypeId();
            appDetails.RecordTypeId=Record;
            Test.startTest();
            database.insert(appDetails);
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            HDFC_DASH_ParseStoreDetails Details = HDFC_DASH_ParseStoreDetails.parse(request);
            HDFC_DASH_APIResponse_StoreDetail apiRes = HDFC_DASH_InboundRestService_StoreDetail.saveDetails(Details);           
            Test.stopTest();
            system.assertEquals(HDFC_DASH_Constants.STORE_DETAIL_SUCCESSMESSAGE, apiRes.successMessage,SUCCESS);
        }
    }   
    /* 
    Method Name : testBlankApplicationIdException
    Parameters  : 
    Description : This method will check if exception is generated if Application Id is Blank.
    */
    static testmethod void testBlankApplicationIdException(){
        System.runAs(sysAdmin){
            Exception testException;     
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(reqBodywithouApp);
            Test.startTest();
            try{  
                HDFC_DASH_ParseStoreDetails Details = HDFC_DASH_ParseStoreDetails.parse(request);
                HDFC_DASH_APIResponse_StoreDetail apiRes = HDFC_DASH_InboundRestService_StoreDetail.saveDetails(Details);
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);       
        }      
    }
    /* 
    Method Name :testInvalidApplicationIdException
    Parameters  :
    Description :This method will check if exception is generated if Application Id is Invalid.
    */
    static testmethod void testinvalidApplicationIdException(){
        
        System.runAs(sysAdmin){
            Exception testException;                
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(reqBodyWrongApp);
            Test.startTest();
            try{
                database.insert(appDetails);
                HDFC_DASH_ParseStoreDetails Details = HDFC_DASH_ParseStoreDetails.parse(request);
                HDFC_DASH_APIResponse_StoreDetail apiRes = HDFC_DASH_InboundRestService_StoreDetail.saveDetails(Details);
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);         
        }      
    } 
        
}