/*
Class: HDFC_DASH_EmailToActivtiy_ListenerTest
Author: Kumar Gourav
Date: 14 Dec 2021
Company: Accenture
Description:Test class for HDFC_DASH_EmailToActivtiy_Listener class.
*/
@istest(SeeAllData = false)
private with sharing class HDFC_DASH_EmailToActivtiy_ListenerTest 
{  
    private static final String SUBJECT = 'File Number 123456789';
    private static final String FROMNAME = 'Client';
    private static final String TOADDRESS = 'name@hdfc.com';
    private static final String TOADDRESS1 = 'name1@gmail.com';
    private static final String FROMADDRESS = 'name2@gmail.com';
    private static final String EMAILBODY = 'The File Number 123456789 is added to the email body';
    private static final String ATTACHMENTBODY = 'Attachment Body';
    private static final String FILENAME = 'textFile.txt';
    private static final String MIMETYPE = 'text/plain';
    private static final String FILENUMBER = '123456789';
    private static final String SUCCESS = 'Success';
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicPersonAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id); 
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    
    /* Method Name: testInBoundEmail
    parameter:none
    Method Description: This method will enqueue and test the InboundEmail.
    */ 
    static testMethod void testInBoundEmail()
    {
        //System.runAs(sysAdmin)
       // {
        app.HDFC_DASH_File_Number__c = FILENUMBER;
        app.Name = FILENUMBER;
        if(Schema.sObjectType.Opportunity.isUpdateable()){
            Database.update(app);
        }
           appDetails.HDFC_DASH_Email__c = acc.PersonEmail;
           Database.update(appDetails);
     
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf(ATTACHMENTBODY);
        attachment.fileName = FILENAME;
        attachment.mimeTypeSubType = MIMETYPE;
        
        email.subject = SUBJECT;
        
        email.fromAddress = FROMADDRESS;
        email.toAddresses = new List<string>{TOADDRESS};
            email.plainTextBody = EMAILBODY;
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        HDFC_DASH_EmailToActivtiy_Listener  testInbound = new HDFC_DASH_EmailToActivtiy_Listener();
            Test.startTest();
        testInbound.handleInboundEmail(email, env);
        Test.stopTest();
      
    }
    
    /* Method Name: testInBoundEmail_WithOutMatchingContact
    parameter:none
    Method Description: This method will enqueue and test the InboundEmail without matching contact.
    */ 
    static testMethod void testInBoundEmail_WithOutMatchingContact()
    {
       
        app.HDFC_DASH_File_Number__c = FILENUMBER;
        app.Name = FILENUMBER;
        if(Schema.sObjectType.Opportunity.isUpdateable()){
            Database.update(app);
        }
        appDetails.HDFC_DASH_Email__c = acc.PersonEmail;
           Database.update(appDetails);
        //System.runAs(sysAdmin)
       // {
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = blob.valueOf(ATTACHMENTBODY);
            attachment.fileName = FILENAME;
            attachment.mimeTypeSubType = MIMETYPE;
            
            email.subject = SUBJECT;
            email.fromAddress = FROMADDRESS;
            email.toAddresses = new List<string>{acc.PersonEmail};
                
                email.plainTextBody = EMAILBODY;
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            HDFC_DASH_EmailToActivtiy_Listener  testInbound = new HDFC_DASH_EmailToActivtiy_Listener();
            Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
            
                Test.startTest();
            result = testInbound.handleInboundEmail(email, env);
            Test.stopTest();
            
            //system.assertEquals(false, result.success,SUCCESS);
       // }
    }
    
    @isTest
    static void emailMessageCheck(){
        
       // System.runAs(sysAdmin)
        //{
           	app.HDFC_DASH_File_Number__c = FILENUMBER;
            app.Name = FILENUMBER;
            Database.update(app);
            appDetails.HDFC_DASH_Email__c = acc.PersonEmail;
           Database.update(appDetails);
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = blob.valueOf(ATTACHMENTBODY);
            attachment.fileName = FILENAME;
            attachment.mimeTypeSubType = MIMETYPE;
            
            email.subject = 'no file number';
            email.fromAddress = FROMADDRESS;
            email.toAddresses = new List<string>{TOADDRESS1};
                email.plainTextBody = EMAILBODY;
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            HDFC_DASH_EmailToActivtiy_Listener  testInbound = new HDFC_DASH_EmailToActivtiy_Listener();
            Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
            
                Test.startTest();
            result = testInbound.handleInboundEmail(email, env);
            Test.stopTest();
            
            //system.assertEquals(false, result.success,SUCCESS);
       // }
        
    }
}