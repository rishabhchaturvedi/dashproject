/**
className: HDFC_DASH_MockLMSResponseGenerator
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestService_DedupeCheck
**/ 
@isTest
global class HDFC_DASH_MockLMSResponseGenerator implements HttpCalloutMock {
     /* 
Method Name :respond
Parameters  :
Description :This method would create a fake response for LMS callout.
*/
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        res.setBody('{"return_cd":0,"return_msg":"SUCCESS","lead_no":"101788909","lead_Details":{"LEAD_NO":101788909,"LEAD_TYPE":"FRESH","LEAD_BRANCH_CD":"508","LEAD_BRANCH":"NEW DELHI","LEAD_NAME":"ROBERT MIDDLE P","RESIDENT_TYPE":"RES","EMPLOYMENT_TYPE":null,"LEAD_CITY":"ACHABAL","LEAD_COUNTRY":"INDIA","LEAD_STATUS":"NEW","LEAD_DT":"2021-12-23","SOURCE":"INTERNET","SUB_SOURCE":"HDFC_DIGITAL","TERM_SOURCE":"HDFC_DASH_SPOT_OFFER","LEAD_DISTRIBUTION":"DSA","LEAD_OWNER":"HDFC"},"appln_form":null}');
        res.setStatusCode(200);
        System.assertNotEquals(null,req);
        return res;
    }
}