/*
  Author: Sakshi Bhadange
  Class: HDFC_DASH_Metadata_Selector_Test
  Description: Apex Class for testdata metadata selector
*/

@isTest(SeeAllData = false)
public class HDFC_DASH_Metadata_Selector_Test {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
   private static final string TOADDRESS = 'customer.service@hdfc.com';
      private static List<String> mdtFieldsList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_EmailId};
          private static List<String> mdtFieldsList2 = new List<String>{HDFC_DASH_Constants.ID_STRING};

/*
 method : test metadatarecs based on string
*/
 static testmethod void testmetadatarecsbasedonstring(){
      List<HDFC_DASH_Email_To_Case_Field_Population__mdt> emailMetadataRecsList = null;
     System.runAs(sysAdmin){
Test.startTest();
 emailMetadataRecsList =  HDFC_DASH_Metadata_Selector.getEmailMetadataRecsBasedOnString(mdtFieldsList,TOADDRESS,HDFC_DASH_Constants.HDFC_DASH_EmailId);
Test.stopTest();
   }
 
    //system.assertNotEquals( null, emailMetadataRecsList);
    //system.assertEquals(1,emailMetadataRecsList.size()); 
    }
    
     static testmethod void testmetadatarecsbasedonstring1(){
	Case c = new Case(
    	HDFC_DASH_Case_Type__c = 'Complaint',
        HDFC_DASH_Mode__c = 'Email',
        HDFC_DASH_Sub_Mode__c = 'Email'
        //HDFC_DASH_Category__c = 'CLSS',
        //HDFC_DASH_Sub_Category__c = 'Property ownership issue'
    );
      List<Case_Default_Owner__mdt> emailMetadataRecsList = null;
     System.runAs(sysAdmin){
Test.startTest();
 emailMetadataRecsList =  HDFC_DASH_Metadata_Selector.getEscalationMtdRecs(mdtFieldsList2,c);
Test.stopTest();
   }
 
    //system.assertNotEquals( null, emailMetadataRecsList);
    //system.assertEquals(1,emailMetadataRecsList.size()); 
    }
    
     static testmethod void testmetadatarecsbasedonstring2(){
	Case c = new Case(
    	HDFC_DASH_Case_Type__c = 'Request',
        HDFC_DASH_Mode__c = 'Email',
        HDFC_DASH_Sub_Mode__c = 'Email',
        HDFC_DASH_Category__c = 'Loan Application',
        HDFC_DASH_Sub_Category__c = 'CIBIL Related'
    );
      List<Case_Default_Owner__mdt> emailMetadataRecsList = null;
     System.runAs(sysAdmin){
Test.startTest();
 emailMetadataRecsList =  HDFC_DASH_Metadata_Selector.getEscalationMtdRecs(mdtFieldsList2,c);
Test.stopTest();
   }
 
    //system.assertNotEquals( null, emailMetadataRecsList);
    //system.assertEquals(1,emailMetadataRecsList.size()); 
    }
    
    static testmethod void testmetadatarecsbasedonstring3(){
	Case c = new Case(
    	HDFC_DASH_Case_Type__c = 'Request',
        HDFC_DASH_Mode__c = 'Email',
        HDFC_DASH_Sub_Mode__c = 'Email',
        HDFC_DASH_Category__c = 'Loan Application',
        HDFC_DASH_Sub_Category__c = 'CIBIL Related'
    );
      List<Emails_to_Case_Type__mdt> emailMetadataRecsList = null;
     System.runAs(sysAdmin){
Test.startTest();
 emailMetadataRecsList =  HDFC_DASH_Metadata_Selector.getMetadataBasedOnCaseType(mdtFieldsList2,c);
Test.stopTest();
   }
 
    //system.assertNotEquals( null, emailMetadataRecsList);
    //system.assertEquals(1,emailMetadataRecsList.size()); 
    }
    
    
}