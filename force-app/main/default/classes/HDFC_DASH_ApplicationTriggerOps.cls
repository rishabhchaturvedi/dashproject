/**
className: HDFC_DASH_ApplicationTriggerOps
DevelopedBy: Sai Suman
Date: 10 Nov 2021
Company: Accenture
Class Description: This Class contains the trigger operation on Opportunity Object.
**/
public class HDFC_DASH_ApplicationTriggerOps {
    /**
className: ChangeStageOnBeforeInsert
Class Description: This Class contains the trigger operation on Opportunity before insert.
**/
    public with sharing class ChangeStageOnBeforeInsert implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.appChangeStageOnBeforeIns;
        }
        /* 
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Opportunity> filteredAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterForSettingDefaultStatusAndStage();
            return filteredAppRecs; 
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Opportunity[] appRecs){ 
            HDFC_DASH_ApplicationTriggerOpsHelper.setAppStartedFlag(appRecs);
            HDFC_DASH_TriggerOpsRecursionFlags.appChangeStageOnBeforeIns =false;
        }     
    }
    /* 
Method Name :ChangeStatusStage
Class Description: This Class contains the trigger operation to change the status field on Opportunity before update.
*/
    public with sharing class ChangeStatusStage implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.appChangeStageOnBeforeUpdate;
        }
        /* 
Method Name :filter
Description :This method will filter the records.
*/
        public SObject[] filter() {
            List<Opportunity> filteredAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterForupdatingStatusAndStage();
            return filteredAppRecs;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Opportunity[] appRecs){
            HDFC_DASH_ApplicationTriggerOpsHelper.updateForStatusAndStage(appRecs); 
            HDFC_DASH_TriggerOpsRecursionFlags.appChangeStageOnBeforeUpdate = false;
        }
    }
    /* 
Method Name :ChangeLMSStageNumber
Class Description: This Class contains the trigger operation to change the StageNumber field on Opportunity before update.
*/
    public with sharing class ChangeLMSStageNumber implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.appChangeStageNumberOnBeforeUpdate;
        }
        /* 
Method Name :filter
Description :This method will filter the records.
*/
        public SObject[] filter() {
            List<Opportunity> filteredAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterForupdatingLMS_StageNumber();
            return filteredAppRecs;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Opportunity[] appRecs){
            HDFC_DASH_ApplicationTriggerOpsHelper.updateForLMS_StageNumber(appRecs); 
            HDFC_DASH_TriggerOpsRecursionFlags.appChangeStageNumberOnBeforeUpdate = false;
        }
    }  
    /* 
Method Name :postFeeProcessing
Class Description: This Class contains the trigger operation to for post fee processing.
*/
    public with sharing class postFeeProcessing implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applPostFeeProcessing;
        }
        /* 
Method Name :filter
Description :This method will filter the records.
*/
        public SObject[] filter() {
            List<Opportunity> filteredAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterPostFeeProcessingRecords();
            return filteredAppRecs;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/ 
        public void execute(Opportunity[] appRecs){
            HDFC_DASH_ApplicationTriggerOpsHelper.executePostFeeProcessing(appRecs); 
            HDFC_DASH_TriggerOpsRecursionFlags.applPostFeeProcessing = false;
        }
    }
    
    /* 
Method Name :WhenFileFirstPushedToILPS
Class Description: This Class contains the trigger operation to for post fee processing.
*/
    public with sharing class WhenFileFirstPushedToILPS  implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applWhenFileFirstPushedToILPS;
        }
        /* 
Method Name :filter
Description :This method will filter the records.
*/
        public SObject[] filter() {
            List<Opportunity> filteredAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterFileFirstPushedToILPSRecords();
            return filteredAppRecs;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/ 
        public void execute(Opportunity[] appRecs){
            HDFC_DASH_ApplicationTriggerOpsHelper.executeAferFileFirstPushedToILPSRecs(appRecs); 
            HDFC_DASH_TriggerOpsRecursionFlags.applWhenFileFirstPushedToILPS = false; 
        }
    }
    /* 
Method Name :WhenFileSecondPushedToILPS
Class Description: This Class contains the trigger operation to for post fee processing.
*/
    public with sharing class WhenFileSecondPushedToILPS  implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applWhenFileSecondPushedToILPS;
        }
        /* 
Method Name :filter
Description :This method will filter the records.
*/
        public SObject[] filter() {
            List<Opportunity> filteredAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterFileSecondPushedToILPSRecords();
            return filteredAppRecs;
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/ 
        public void execute(Opportunity[] appRecs){
            HDFC_DASH_ApplicationTriggerOpsHelper.executeSecondPushToILPSRec(appRecs); 
            HDFC_DASH_TriggerOpsRecursionFlags.applWhenFileSecondPushedToILPS = false; 
        }
    }  
    /* 
Method Name :UpdatePrimeApplicationNos
Class Description: This Class contains the trigger operation to change the status field on Opportunity before update.
*/
    public with sharing class UpdatePrimeApplicationNos implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            //system.debug('inside is enabled of UpdatePrimeApplicationNos');
            return HDFC_DASH_TriggerOpsRecursionFlags.updatePrimeApplicationNosOnApp;
        }
        /* 
Method Name :filter
Description :This method will filter the records.
*/
        public SObject[] filter(){
            List<Opportunity> filAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterSimultaneouLoanApplications();
            return filAppRecs; 
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Opportunity[] appRecs){
            //system.debug('inside is execute of UpdatePrimeApplicationNos');
            HDFC_DASH_ApplicationTriggerOpsHelper.setPrimeApplication(appRecs); 
            HDFC_DASH_TriggerOpsRecursionFlags.updatePrimeApplicationNosOnApp = false;
        }
    }
    
    /**
className: CopyAppInfoToCustomerBasedOnFlags
Class Description: This Class contains the trigger operation on Opportunity before insert.
**/
    public with sharing class CopyAppInfoToCustomerBasedOnFlags implements HDFC_DASH_TriggerOps{  
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.copyAppInfoToCustomerBasedOnFlags;
        }
        /* 
Method Name :filter
Description :This method would filter for the records. 
*/
        public SObject[] filter(){
            //system.debug('xxx In Filter AppPayDetTrigger');
            List<Opportunity> filAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterAppBasedOnPayment(); 
            return filAppRecs; 
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Opportunity[] appRecs){ 
            //system.debug('inside execute method');
            HDFC_DASH_ApplicationTriggerOpsHelper.copyAppDetInfoToCust(appRecs);
            HDFC_DASH_TriggerOpsRecursionFlags.copyAppInfoToCustomerBasedOnFlags =false;
        }     
    }
    
    /**
className: IlpsFilePushCallout
Class Description: This Class contains the trigger operation on Opportunity before insert.
**/
    public with sharing class IlpsFilePushCallout implements HDFC_DASH_TriggerOps{  
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.ilpsFilePushCalloutFlags;
        }
        /* 
Method Name :filter
Description :This method would filter for the records. 
*/
        public SObject[] filter(){
            List<Opportunity> filAppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterilpsFilePushCallout(); 
            return filAppRecs; 
            //return Trigger.new; 
        }
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Opportunity[] appRecs){ 
            HDFC_DASH_ApplicationTriggerOpsHelper.updateFilePushToILPS(appRecs);
            HDFC_DASH_TriggerOpsRecursionFlags.ilpsFilePushCalloutFlags =false;
        }     
    }
    /**
className: PopulateAppFieldsInsert
DevelopedBy: Tejeswari
Date: 27 November 2021
Company: Accenture
Class Description: This Class update the lookup fields of Opportunity for insert Operation.
*/ 
    public with sharing class PopulateAppFieldsInsert implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.OppPopulateFieldsFirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Opportunity> listOppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterPopulateAppFields_Insert();
            return listOppRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] Opps){
            HDFC_DASH_ApplicationTriggerOpsHelper.populateAppFields_Insert(Opps);  
            HDFC_DASH_TriggerOpsRecursionFlags.OppPopulateFieldsFirstRun = false;
            
        }  
    }
    /**
className: PopulateAppFieldsUpdate
DevelopedBy: Tejeswari
Date: 30 November 2021
Company: Accenture
Class Description: This Class update the lookup fields of Opportunity for update operation.
*/ 
    public with sharing class PopulateAppFieldsUpdate implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.appPopulateFields_Update_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Opportunity> listOppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterPopulateOppFields_Update();
            return listOppRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] Opps){
            HDFC_DASH_ApplicationTriggerOpsHelper.populateAppFields_Update(Opps);  
            HDFC_DASH_TriggerOpsRecursionFlags.appPopulateFields_Update_FirstRun = false;
        }  
    }
    /**
className: PopulateAppFieldsUpdate
DevelopedBy: Tejeswari
Date: 30 November 2021
Company: Accenture
Class Description: This Class update the ownerid field of Opportunity for insert.
*/ 
    public with sharing class PopulateAppOwnerAgencyFields implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.appPopulateOwnerAgency_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Opportunity> listOppRecs = HDFC_DASH_ApplicationTriggerOpsHelper.filterAppOwnerAgencyFields();
            return listOppRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] Opps){
            HDFC_DASH_ApplicationTriggerOpsHelper.updateAppOwnerAgencyFields(Opps);  
            HDFC_DASH_TriggerOpsRecursionFlags.appPopulateOwnerAgency_FirstRun = false;
            
        }  
    }
}