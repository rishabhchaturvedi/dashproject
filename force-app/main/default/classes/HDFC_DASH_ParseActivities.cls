/**
className: HDFC_DASH_ParseActivities
DevelopedBy: Anisha Arumugam
Date: 24 Aug 2021
Company: Accenture
Class Description: This is a wrapper class for HDFC_DASH_RestResource_GetActivities to send the API response and 
also for HDFC_DASH_RestResource_PostActivities to store the data into salesforce.
**/
public without sharing class HDFC_DASH_ParseActivities {
    
    /*
        Class: Activities
        Description: Class to parse the Activities
    */
    public without sharing class Activities 
    {
        public String title;
        public String type;
        public String sfActivityId;
        public String sfActivityNumber;
        public String status;
        public String description;
        public DateTime startDateTime;
        public DateTime endDateTime;
        public DateTime actualEndDateTime;
        public String meetingType;
        public Boolean isMissed;
        public String assignedToName;
        public String assignedToSfId;
        public String executiveCode;
        public String bsa;
        public String fileNumber;
        public String leadID; 
        public String subjectName;
        public String applicationStage;
        public String applicationStatus;
        public Double latitude;
        public Double longitude;
    }
    
    public List<Activities> activities;
    
     /* 
    Method Name :parse
    Description :This method would convert the request JSON string to HDFC_DASH_ParseActivities class.
    */
    public static HDFC_DASH_ParseActivities parse(RestRequest request) {
        string json = request.requestBody.toString();
        return (HDFC_DASH_ParseActivities) System.JSON.deserialize(json, HDFC_DASH_ParseActivities.class);
    }
}