/*
Author: Kumar Gourav
Class: HDFC_DASH_APIResponse_DocReview
Description: Apex Class for handling the API Response of GetDocReview
*/
public without sharing class HDFC_DASH_APIResponse_DocReview 
{
       /*
class name: ReceivedDocs
Description: This class will parse the response body of Info Validation
*/
    public without sharing class ReceivedDocs {
        public String sfInfoValidationId;
        public String sfApplicationID;
        public String sfApplicationNumber;
        public String sfApplicantID;
        public String sfCustomerId;
        public String sfCustomerNumber;
        public String fileNumber;
        public String documentStatus;
        public String customerNumber;
        public String customerCapacity;
        public Date uploadedDate;
        public String infoValidationRecordType;
        public String isLatest;
        public ApplicantInfoValidation applicantInfoValidation;
    }
    /*
        Class: PrePopulatedSalaryInformation
        Description: Class to parse PrePopulated Salary Information
    */
    public without sharing class PrePopulatedSalaryInformation {
        public String incomeProof;
        public String salaryMonth;
        public Decimal salaryYear;
        public String additionalIncomeDocumentType;
        public Boolean bankStatementUpload;
        public Boolean salarySlipUploadMonth;
        public Boolean salarySlipUploadWeek1;
        public Boolean salarySlipUploadWeek2;
        public Boolean salarySlipUploadWeek3;
        public Boolean salarySlipUploadWeek4;
    }
    /*
        Class: PrePopulatedFinancialInformation
        Description: Class to parse PrePopulated Financial Information
    */
    public without sharing class PrePopulatedFinancialInformation {
        public String accountHolderName;
        public String bankName;
        public String bankCode;
        public String loanType;
        public String loanTypeCode;
        public Double outstandingAmount;
        public Decimal installmentAmount;
        public Decimal balanceTermOfLoanInMonths;
    }
    /*
        Class: PrePopulatedPassportInformation
        Description: Class to parse PrePopulated Passport Information
    */
    public without sharing class PrePopulatedPassportInformation {
        public String passportNumber;
       	public String nationality;
    }
    
    public List<MandatoryDocs> mandatoryDocs;
    public List<ReceivedDocs> receivedDocs;
     /*
        Class: PrePopulatedBusinessInformation
        Description: Class to parse PrePopulated Business Information
    */
    public without sharing class PrePopulatedBusinessInformation {
        public String designationAndEntityType;
        public String businessName;
        public Decimal yearOfIncorporation;
        public String annualTurnover;
        public String natureOfBusiness;
        public String industryType;
        public String businessPAN;
        public String udyamRegistrationNumber;
        public String cinNumber;
    }
     /*
        Class: PrePopulatedBusinessInformation
        Description: Class to parse Applicant Info Validation
    */
    public without sharing class ApplicantInfoValidation {
        public String validationType;
        public String source;
        public String docTypeCode;
        public String docType;
        public String docNumber;
        public Date dateOfIssue;
        public Date dateOfExpiry;
        public String salutation;
        public String firstName;
        public String middleName;
        public String lastName;
        public Date dateOfBirth;
        public String genderCode;
        public String addressType;
        public String dmsDocumentId;
        public String storageIdentifier;
        public String storageType;
        public String monthOfGstReturn;
        public String assessmentYear;
        public String financialYear;
        public Double totalAmountPaid;
        public Decimal noOfQuartersPaid;
        public String proofOfCommunication;
        public String proofOfCommunicationCode;
        public String kycDocType;
        public String kycDocTypeCode;
        public String bcpDocType;
        public String bcpDocTypeCode;
        public String docExtractable;
        public String docScannedBy;
        public String docImageOrigin;
        public Double latitude;
        public Double longitude;
       	public Boolean documentFileUploaded;
        public Boolean loanOutstandingLetterUpload;
        public Boolean propertyCostSheetUpload;
        public Boolean financialInfoDocumentUpload;
        public PrePopulatedFinancialInformation prePopulatedFinancialInformation;
        public PrePopulatedSalaryInformation prePopulatedSalaryInformation;
        public PrePopulatedBankDetails prePopulatedBankDetails;
        public PrePopulatedEmployerInformation prePopulatedEmployerInformation;
        public PrePopulatedEmpGstBusinessDetails prePopulatedEmpGstBusinessDetails;
        public PrePopulatedBusinessInformation prePopulatedBusinessInformation;
        public PrePopulatedPassportInformation prePopulatedPassportInformation;
        public PrePopulatedPermanentResidency prePopulatedPermanentResidency;
        public PrePopulatedAddressDetails prePopulatedAddressDetails;
       
    }
    /*
        Class: PrePopulatedAddressDetails
        Description: Class to parse PrePopulated Address Details
    */
    public without sharing class PrePopulatedAddressDetails {
        public String addressLine1;
        public String addressLine2;
        public String addressLine3;
        public String addressLine4;
        public String landmark;
        public String city;
        public String cityCode;
        public String taluka;
        public String district;
        public String state;
        public String stateCode;
        public String country;
        public String countryCode;
        public String pinCode;
        public String postOfficeName;
    }
    /*
        Class: PrePopulatedEmpGstBusinessDetails
        Description: Class to parse PrePopulated EmpGst Business Details
    */
    public without sharing class PrePopulatedEmpGstBusinessDetails {
        public String gstNumber;
        public String empGstStateCode;
        public String empGstState;
    }
    /*
        Class: PrePopulatedEmployerInformation
        Description: Class to parse PrePopulated Employer Information
    */
    public without sharing class PrePopulatedEmployerInformation {
        public String employerName;
        public String employerCode;
        public String employeeNumber;
        public String designation;
        public String department;
        public Decimal jobDurationInMonths;
    }
     /*
        Class: PrePopulatedPermanentResidency
        Description: Class to parse PrePopulated Permanent Residency
    */
    public without sharing class PrePopulatedPermanentResidency {
        public String prNumber;
        public String prCountry;
        public String prCountryCode;
        public Date prValidityDate;
    }
    
   /*
        Class: MandatoryDocs
        Description: Class to parse the response body of Min Mand Doc callout to ILPS
    */
    
    public without sharing class MandatoryDocs {
        public Integer sfapplicantno;
        public String doccode;
        public String dockey4val;
        public String dockey5val;
        public String docfromdate;
        public String doctodate;
        public String mindoc;
        public String manddoc;
    }
     /*
        Class: PrePopulatedBankDetails
        Description: Class to parse PrePopulated Bank Details
    */
    public without sharing class PrePopulatedBankDetails {
        public String accountType;
        public Date fromDate;
        public Date toDate;
        public String accountHolderName;
        public String bankCode;
        public String bankName;
        public String accountNumber;
        public String ifscCode;
        public String isSalaryAccount;
    }
    
}