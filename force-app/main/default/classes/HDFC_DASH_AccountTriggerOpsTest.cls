/**
className: HDFC_DASH_AccountTriggerOpsTest 
DevelopedBy: Anmol
Date: 27 May 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_AccountTriggerOps class.

**/
@isTest(SeeAllData = false) 
public class HDFC_DASH_AccountTriggerOpsTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string VALIDATION_METHOD = 'validation method';
    private static final string MOBILE_PHONE= '4567678822';
    private static final string MOBILE_CODE= '91';
    
    /* 
Method Name :testValidationMethod
Description :This method would test the trigger for inserting one Platform Event.
*/
    static testmethod void testValidationMethod() { 
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        test.startTest();
        system.runAs(sysAdmin){
            
        }
        test.stopTest();
        system.assert(true, VALIDATION_METHOD);
    }    
    /* 
Method Name :testFilterInsertAccForOptinCallout
Description :This method would test the method in insert trigger helper for Optin callout.
*/ 
    @isTest 
    public static void testFilterInsertAccForOptinCallout(){
        Exception testException;
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OptinCallout();
        Account accountRec = HDFC_DASH_TestDataFactory_Accounts.createBasicPersonAccount();
        accountRec.HDFC_DASH_WhatsApp_Consent__pc = HDFC_DASH_Constants.STRING_Y;
        accountRec.PersonMobilePhone = MOBILE_PHONE;
         accountRec.HDFC_DASH_Mobile_Country_Code__pc = MOBILE_CODE;
        System.runAs(sysAdmin) {
            Test.startTest();
            
            try{
                Database.insert(accountRec);   
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            system.assertEquals(null, testException);
        }
    }
    
    /* 
Method Name :testFilterUpdateAccForOptinCallout
Description :This method would test the method in update trigger helper for Optin callout.
*/ 
    @isTest 
    public static void testFilterUpdateAccForOptinCallout(){
        Exception testException;
        Account accountRec = HDFC_DASH_TestDataFactory_Accounts.createBasicPersonAccount();
        accountRec.HDFC_DASH_WhatsApp_Consent__pc = HDFC_DASH_Constants.STRING_Y;
        accountRec.HDFC_DASH_Mobile_Country_Code__pc = MOBILE_CODE;
        Database.insert(accountRec);   
        System.runAs(sysAdmin) {
            
            Test.startTest();
            Account objAccount = [Select id, PersonMobilePhone from Account where id=:accountRec.id];
            objAccount.PersonMobilePhone = MOBILE_PHONE;
            try{
                Database.update(objAccount);   
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            system.assertEquals(null, testException);
        }
    }
}