/**
className: HDFC_DASH_Cont_Address_SelectorTest
DevelopedBy: Punam Marbate
Date: 13 Aug 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Contact_Address_Selector class.
**/
@isTest(SeeAllData = false)
public with sharing class HDFC_DASH_Cont_Address_SelectorTest {

    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Contact_Address_Details__c conAddressDetail = 
            HDFC_DASH_TestDataFactory_ContAddDetail.getBasicContAddressDetail(con.Id);
    /* 
    Method Name :testGetContactAddressBasedOnId
    Description :This method will test the method getContactAddressBasedOnId().
    */
   /* static testmethod void testGetContactAddressBasedOnId(){
        
        HDFC_DASH_Contact_Address_Details__c resConAddDetailRec = null;    
        System.runAs(sysAdmin){      
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                                                    HDFC_DASH_Constants.STRING_Name};
            Test.startTest();  
            resConAddDetailRec = HDFC_DASH_Contact_Address_Selector.getContactAddressBasedOnId(fieldList, 
                                                                                conAddressDetail.Id);        
        	Test.stopTest();
        }
        system.assertNotEquals( null, resConAddDetailRec);
        system.assertNotEquals( null, conAddressDetail);
        system.assertEquals(conAddressDetail.Id,resConAddDetailRec.Id); 
        
    }*/
    
    /* 
    Method Name :testGetContactAddressBasedOnIds
    Description :This method will test the method getContactAddressBasedOnId().
    */
    static testmethod void testGetContactAddressBasedOnIds(){
        
        List<HDFC_DASH_Contact_Address_Details__c> resConAddDetailRec = null;    
        System.runAs(sysAdmin){      
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                                                    HDFC_DASH_Constants.STRING_Name};
             List<Id> listContactAddressDetailIds = new List<Id>{conAddressDetail.Id};                                           
            Test.startTest();  
            resConAddDetailRec = HDFC_DASH_Contact_Address_Selector.getContactAddressBasedOnIds(fieldList, 
                                                                                listContactAddressDetailIds);        
        	Test.stopTest();
        
        system.assertNotEquals( null, resConAddDetailRec);
        system.assertNotEquals( null, conAddressDetail);
        system.assertEquals(listContactAddressDetailIds.size(),resConAddDetailRec.size()); 
      }
        
    }
    
    /* 
    Method Name :testGetContactAddress
    Description :This method will test the method getContactAddress().
    */
    /*static testmethod void testGetContactAddress(){

        List<HDFC_DASH_Contact_Address_Details__c>  listConAddrDetailRec = null;
        System.runAs(sysAdmin){      
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                                                HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listConAddrDetailRec = HDFC_DASH_Contact_Address_Selector.getContactAddress(
                                    new List<String> {conAddressDetail.Id}  ,
                                    new List<String>{HDFC_DASH_Constants.STRING_ID},
                                    HDFC_DASH_Constants.ID_STRING);          
            Test.stopTest();
        }
        system.assertNotEquals( null, listConAddrDetailRec);
        system.assertEquals(1,listConAddrDetailRec.size()); 
      }*/
    /* 
    Method Name :testGetContactAddressBasedOnQuery
    Description :This method will test the method getContactAddressBasedOnQuery().
    */
    static testmethod void testGetContactAddressBasedOnQuery(){
        List<HDFC_DASH_Contact_Address_Details__c>  listConAddrDetailRec = null;
        System.runAs(sysAdmin){       
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,
                HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Contact =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                con.Id+HDFC_DASH_Constants.STRING_QUOTE};
                listConAddrDetailRec = HDFC_DASH_Contact_Address_Selector.getContactAddressBasedOnQuery(
                new List<String>{HDFC_DASH_Constants.STRING_ID},condition);             
            Test.stopTest();
        }
        system.assertNotEquals( null, listConAddrDetailRec);
        system.assertEquals(1,listConAddrDetailRec.size()); 
    }
     
}