/*
Class: HDFC_DASH_Process_ILPS_FilePush_Test
Author: Koora Raghavendra
Date: 15 December 2021
Company: Accenture
Description:This is the test class for HDFC_DASH_Process_SpotOfferILPS_Response.
*/
@isTest
public with sharing class HDFC_DASH_Process_ILPS_FilePush_Test {
    
    private static final string SUCCESS ='Success';
    private static final string CUSTNO ='123454321';
     //Getting the object records
     private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
     private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
     private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
     private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
     private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
     private static HDFC_DASH_Applicant_Employment_Details__c empAppDetail = HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.id,con.id);
    

     /*
    Method: testdoRetryProcess
    Description: This method will test the doRetryProcess() method in the class HDFC_DASH_Process_SpotOfferILPS_Response.
    */
    static testmethod void  testdoRetryProcess()
    {    
        HttpResponse res = new HttpResponse();
        System.runAs(sysAdmin)
        {
            //Contact newCon = [Select id, name, HDFC_DASH_Contact_Number__c from contact where id =: con.id];
            Account newAcc = [Select id, name, HDFC_DASH_Customer_Number__c from Account where id =: acc.id];
            Opportunity newApplication=[Select id, name, HDFC_DASH_ApplicationNumber__c from opportunity where id =: app.id];

            res.setStatusCode(HDFC_DASH_Constants.INT_TWO_HUNDRED);
            //res.setBody('{ "sf_appl_id" :' + newApplication.HDFC_DASH_ApplicationNumber__c + ', "file_no": 610123456, "custDetails": [ { "sf_cust_no": ' + newAcc.HDFC_DASH_Customer_Number__c + ', "cust_no": 123454321, "unq_cust_no": 678901234 } ], "errorMessage":"" }');
            //res.setBody('{"serialDetails":[{"sfapplicationid":' + newApplication.id + ',"fileno":658116280,"filecompletenessflag":"N"}],"applicantDetails":[{"sfcustno":'+ newAcc.HDFC_DASH_Customer_Number__c +',"customername":"Mr. Ram L Shah","capacity":"B","ilpscustnumber":123454321}]}');
            res.setBody('{ "serialDetails": [ { "sfapplicationid": "' + newApplication.id + '", "fileno": 658033240, "filecompletenessflag": "N" } ], "applicantDetails": [ { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015076 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015077 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015078 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015231 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015232 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015233 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015441 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015442 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015443 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015444 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015445 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015446 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015447 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015448 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015449 }, { "sfcustno": 10000113, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015450 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015451 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015452 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015453 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015454 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015455 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015456 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015457 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015458 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015459 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015460 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015461 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015462 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015463 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015464 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015465 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015466 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015467 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015468 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015469 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015470 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015471 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015472 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015473 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015474 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015475 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015476 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015477 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015478 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015479 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015480 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015481 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015482 }, { "sfcustno": 10000113, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015483 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015484 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015485 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015486 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015487 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015488 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "B", "ilpscustnumber": 8015489 }, { "sfcustno": null, "customername": "Mr. Manoj6 L Shah", "capacity": "R", "ilpscustnumber": 8015490 }, { "sfcustno": null, "customername": "Mr. Manoj L Shah", "capacity": "R", "ilpscustnumber": 8015491 }, { "sfcustno": 10000934, "customername": "Mr. Manoj L Devraj", "capacity": "C", "ilpscustnumber": 8015558 } ], "errorCode": 0, "errorMessage": "Success" }');
            Test.startTest();
            HDFC_DASH_Process_ILPSFilePush_Response spotOfferResp  = new HDFC_DASH_Process_ILPSFilePush_Response();  
            spotOfferResp.doRetryProcess(res,null);
            Test.stopTest();
            //system.assertEquals(1, [select count() from HDFC_DASH_Applicant_Details__c where HDFC_DASH_ILPS_Customer_Number__c =:CUSTNO], SUCCESS);  
            //system.assertEquals(1, [select count() from Account where HDFC_DASH_ILPS_Unique_Cust_Number__c =:UNIQ_CUSTNO], SUCCESS);
        }        
    } 
}