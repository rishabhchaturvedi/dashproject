public class HDFC_DASH_GetUserForCaseEscalationNew {
    
    public static Id GetUSerForEscalation(Case c){
        
        Id escalatedToID ;
        string currentOwnerRole;
        boolean isCustomerEnagemet=false;
        boolean isQueue=true;
        String CustomerEngTeam='Customer Engagement Team';
        escalatedToID=c.OwnerId;
        String currentOwnerName;
        
        if(string.valueOf(escalatedToID).startsWith('005'))
            currentOwnerName=[SELECT Id,Owner.Name,Owner.UserRole.Name  from case where  id = :c.Id].Owner.UserRole.Name;
        else
             currentOwnerName = 'queue';
        
        if(!c.HDFC_DASH_Skip_AutoAssignment__c){
        
            system.debug('@@@@ currentOwnerName '+ currentOwnerName+' Status '+c.Status);
            
            if(!currentOwnerName.contains(CustomerEngTeam))
            {
                if(string.valueOf(c.OwnerId).startsWith('005')){
                    
                    List<User> currentOwner=[select id,Name,UserRole.Name from user where id= :c.OwnerId];
                    // List<User> currentOwner=[SELECT Id,Name,UserRoleId,  UserRole.Name,ManagerId    from user where id= :c.OwnerId];
                    system.debug('@@@@ currentOwner '+ currentOwner);
                    if(currentOwner!=null && currentOwner.size()>0)
                    {
                        currentOwnerRole=currentOwner[0].UserRole.Name;
                        isQueue=false;
                    }
                    system.debug('@@@@ currentOwnerRole '+ currentOwnerRole);
                }
            }else
            {
                isCustomerEnagemet=true;
            }
            if(currentOwnerRole!=null)
            {
                if(currentOwnerRole.contains(CustomerEngTeam)){
                    isCustomerEnagemet=true;
                }
                else{
                    isQueue=false;
                }
                
            }
            if(isCustomerEnagemet){
                
                if(c.Status==HDFC_DASH_Constants.ESCALATED_L1){
                  // escalatedToID=[SELECT Id   from Group where Type ='Queue' and Name ='Customer Engagement Team - TL'].Id;
                    // escalatedToID=[select id,Name from UserRole where Name ='Customer Engagement Team - TL'].id;
                    escalatedToID = [select id from User where userrole.name =: 'Customer Engagement Team - TL' limit 1].Id;
                }
                else if(c.Status==HDFC_DASH_Constants.ESCALATED_L2){
                    //Id groupId = [SELECT Id   from Group where Type ='Queue' and Name  ='HO CE HEAD/CGRO'].Id;
                    escalatedToID = [select id from User where userrole.name =: 'HO CE HEAD/CGRO' limit 1].Id;
                    
                    //escalatedToID=[SELECT Id   from Group where Type ='Queue' and Name  ='HO CE HEAD/CGRO'].Id;
                    // escalatedToID=[select id,Name from UserRole where Name ='HO CE HEAD/CGRO'].id;
                }
                
            }else if(isQueue==false){
                escalatedToID=getUserManager(c);
                system.debug('@@@@ getUserManager '+ escalatedToID);
            }
        }
        return escalatedToID;
    }
    
    public static Id getUserManager(Case c){
        Id escalatedToID ;
        List<User> userList= [select managerId from User where id=:c.OwnerId];
        system.debug('@@@@ userList Size '+ userList.size() +' List '+userList);
        for (User usLst: userList){
            escalatedToID=usLst.ManagerId;
            system.debug('@@@@ ManagerId '+ escalatedToID);
        }
        return escalatedToID;
    }
    
    
}