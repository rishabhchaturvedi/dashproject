global class MilestoneTimeCalculator implements Support.MilestoneTriggerTimeCalculator {
    
    global integer calculateMilestoneTriggerTime (String caseId, String milestoneTypeId){
        //Get Specific milestone to set time trigger
        //system.debug('caseId...'+caseId);
        //system.debug('milestoneTypeId...'+milestoneTypeId);
        MilestoneType mt = [SELECT Name FROM MilestoneType WHERE Id=:milestoneTypeId];
        Integer tatdays = 0;
        
        
        if (mt.Name != null && mt.Name.equals('TAT 5 days')) 
        { 
            tatdays = 5;
            
        }
        if (mt.Name != null && mt.Name.equals('TAT 7 days')) 
        { 
            tatdays = 7;
            
        }
        if (mt.Name != null && mt.Name.equals('TAT 10 days')) 
        { 
            tatdays = 10;
            
        }
        if (mt.Name != null && mt.Name.equals('TAT 15 days')) 
        { 
            tatdays = 15;
            
        }
        //Get the Case record
        Case c = [SELECT Id,CreatedDate,
                  Status,
                  Priority,HDFC_DASH_Home_Branch__c
                  FROM Case WHERE Id=:caseId];
        
        String Homebranch = c.HDFC_DASH_Home_Branch__c;
        Datetime st = c.CreatedDate;
        DateTime et;
        if (tatdays!=0){
            et = c.CreatedDate + tatdays;
        }
        
        Date stDate =Date.newInstance(st.year(),st.month(),st.day());
        Date edDate = Date.newInstance(et.year(),et.month(),et.day());
        //system.debug('edDate...'+edDate);
        List<HDFC_DASH_Branch_Wise_Holiday__c> holiday = [Select HDFC_DASH_Holiday_Date__c,Name 
                                                          FROM HDFC_DASH_Branch_Wise_Holiday__c 
                                                          WHERE Branch_Name__c =:HomeBranch
                                                          AND HDFC_DASH_Holiday_Date__c >=:stDate 
                                                          AND HDFC_DASH_Holiday_Date__c <=:edDate ];
        if (holiday.size() >0)
        {
            edDate = edDate + holiday.size();
            System.debug('final endate:'+edDate);
            
        } else {
            edDate = edDate;
            System.debug('final endate:'+edDate);
        }
        Double noOfmin;
        if (tatdays!=0){
            noOfmin = (stDate.daysBetween(edDate)*480);
        } 
        
        system.debug('1111');
        return integer.valueof(noofMin);
    }
    
    
    
}