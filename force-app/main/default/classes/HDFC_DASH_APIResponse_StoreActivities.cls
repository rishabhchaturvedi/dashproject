/*
Author: Kumar Gourav
Date: 28 Sep 2021
Company: Accenture
Class: HDFC_DASH_APIResponse_StoreActivities
Description: Apex Class for handling the response for Store Activity Validation API
*/
public inherited sharing class HDFC_DASH_APIResponse_StoreActivities 
{
    public String successMessage;
    /*
Class: activity
Description: Class for handling the response for Store activity
*/
    Public without sharing Class activity{
        public String sfActivityId;
        public String sfActivityNumber;
    }
    public List<activity> activities;
}