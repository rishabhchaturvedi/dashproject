/* 
ClassName: HDFC_DASH_TestDataFactory_Accounts
DevelopedBy: Sushant
Date: 25 May 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Account object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_Accounts {

    public static final string PHONENUMBER = '123456';
    public static final string NAME = 'Name';
    public static final string CORP_CUST_NO = '567';
    public static Id recordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.PERSON_ACCOUNT).getRecordTypeId(); 
  	private static final string BSA_ID = '12345';
    private static final string EMAIL='robin2134@gmail.com';
    private static final string CUSTOMER_NUMBER='772663';
    private static final string SOURCE_RCSRNO = '56979';
    
   
/* Method Name: createBasicAccount
parameters: NONE
Class Description: This method is to create test data for Account.
					Pass fields of Account, and based on doInsert it will either
					pass an instance of uninserted account or inserted account id. 
*/
public static Account createBasicAccount()
{
    Account acc = new Account();
    acc.Name = NAME+string.valueOf(datetime.now().getTime());
    acc.phone= PHONENUMBER+Integer.valueof((Math.random() * 1000));
    acc.HDFC_DASH_Corp_Cust_No__c=CORP_CUST_NO+Integer.valueof((Math.random() * 1000));
    acc.HDFC_DASH_Agency_Code__c = BSA_ID;
    acc.HDFC_DASH_Customer_Number__c  = CUSTOMER_NUMBER + Integer.valueof((Math.random() * 1000));
    acc.HDFC_DASH_Source_Rec_SrNo__c = SOURCE_RCSRNO + Integer.valueof((Math.random() * 1000));
   
	return acc;

}

/* Method Name: getBasicAccount
parameters: NONE
Class Description: This method is to generate test data for Account.
*/
public static Account getBasicAccount()
{
    Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicAccount();
    if(Schema.sObjectType.Account.isCreateable()){
        Database.insert(acc,false);
    }
    return acc;
}

/* Method Name: createBasicAccountlist
parameters: Integer numberofaccounts
Class Description: This method is to create a list of test data for Account
                    by calling a createBasicAccount method.
*/
public static List<Account> createBasicAccountlist(Integer numberofaccounts)
{
	list<Account> accountlist = new list<Account>();
	for(integer i=0;i<numberofaccounts; i++){
    accountlist.add(HDFC_DASH_TestDataFactory_Accounts.createBasicAccount());
	}
	return accountlist;
}

    /* Method Name: getBasicAccountlist
parameters: Integer numberofaccounts
Class Description: This method is to generate a list of test data for Account.
*/
public static List<Account> getBasicAccountlist(Integer numberofaccounts)
{
list<Account> accountlist = HDFC_DASH_TestDataFactory_Accounts.createBasicAccountlist(numberofaccounts);
if(Schema.sObjectType.Account.isCreateable()){
    Database.insert(accountlist);
}
return accountlist;
}

    /* Method Name: createBasicPersonAccount
parameters: none
Class Description: This method is to create test data for Person Account
*/
public static Account createBasicPersonAccount()
{
    Account acc1 = new Account();
    acc1.RecordTypeId=recordTypeId;
    acc1.LastName = NAME+string.valueOf(datetime.now().getTime());
    acc1.PersonMobilePhone= PHONENUMBER+Integer.valueof((Math.random() * 1000));
    acc1.PersonEmail=EMAIL;
	return acc1;

}

    /* Method Name: getBasicPersonAccount
parameters: none
Class Description: This method is to create test data for Person Account 
					by calling createBasicPersonAccount					
*/
public static Account getBasicPersonAccount()
{
    Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicPersonAccount();
    if(Schema.sObjectType.Account.isCreateable()){
        Database.insert(acc,false);
    }
    return acc;

}

}