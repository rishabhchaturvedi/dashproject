/**
* 	className: HDFC_DASH_AppInfoValidation_Selector
DevelopedBy: Sai Shruthi
Date: 21 July 2021
Company: Accenture
Class Description: This class would create the select queries for HDFC_DASH_Info_Validation__c object .
**/
public inherited sharing class HDFC_DASH_AppInfoValidation_Selector {
    private static final string QUERY_PARAMETER = '=:queryParameters';
   
    /* 
    Method Name :getAppInfoValidationBasedOnIds
    Parameters  :List<string> fieldList,List<ID> appInfoValIds
    Description :This method would get the HDFC_DASH_Info_Validation__c based on the IDs.
    */
     /*  public static List<HDFC_DASH_Info_Validation__c> getAppInfoValidationBasedOnIds(List<string> fieldList,List<ID> appInfoValIds){
            
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_APPINFOVALIDATION_WHERE + HDFC_DASH_CONSTANTS.STRING_ID + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(appInfoValIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
            List<HDFC_DASH_Info_Validation__c> appInfoValList = database.query(querystring); 
            return appInfoValList;
        } */
        
    /* 
    Method Name :getappInfoValBasedOnId
    Parameters  :List<string> fieldList, Id appInfoValId
    Description :This method would get a single HDFC_DASH_Info_Validation__c based on the ID.
    */
     /*   public static HDFC_DASH_Info_Validation__c getappInfoValBasedOnId(List<string> fieldList, Id appInfoValId)
        {
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + HDFC_DASH_CONSTANTS.STRING_FROM_APPINFOVALIDATION_WHERE + HDFC_DASH_CONSTANTS.STRING_ID+ HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ appInfoValId +HDFC_DASH_Constants.STRING_QUOTE;
            HDFC_DASH_Info_Validation__c appInfoValRec = database.query(querystring); 
            return appInfoValRec;
        } */
        
         /* 
    Method Name :getappInfoValBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of HDFC_DASH_Info_Validation__c based on fieldsAndparameters sent to it.
    */
        public static List<HDFC_DASH_Info_Validation__c> getappInfoValBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +HDFC_DASH_CONSTANTS.STRING_FROM_APPINFOVALIDATION_WHERE +getCondition(fieldsAndparameters) ;
				//system.debug('***querystring '+querystring);
            List<HDFC_DASH_Info_Validation__c> appInfoValList = database.query(querystring); 
            return appInfoValList;   
        }
        
        /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getappInfoValBasedOnQuery .
    */
        public static String getCondition(Map<String,String> fieldsAndparameters){
            String condition ='';
            for(String fieldName : fieldsAndparameters.keySet()){
                condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
            }
            return condition;
        }
   /* 
    Method Name :getAppInfoVal
    Parameters  :List<String> queryParameters,List<string> fieldList, String conditionOn
    Description :This method would get the List of HDFC_DASH_Info_Validation__c.
    */   
    public static List<HDFC_DASH_Info_Validation__c> getAppInfoVal(List<String> queryParameters,List<string> fieldList, String conditionOn)
    {      
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + HDFC_DASH_CONSTANTS.STRING_FROM_APPINFOVALIDATION_WHERE + conditionOn + QUERY_PARAMETER;
         List<HDFC_DASH_Info_Validation__c> appInfoValList = database.query(querystring); 
         return appInfoValList;
     }
}