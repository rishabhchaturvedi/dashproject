/**
className: HDFC_DASH_MockDocDetailsResGenerator
Author: Punam Marbate
Date: 20 July 2021
Company: Accenture
Class Description: Thi class will generate a mock response for DocumentInfo_ILPScallout.
**/ 
@isTest(SeeAllData=false)
global with sharing class HDFC_DASH_MockDocDetailsResGenerator implements HttpCalloutMock
{    
    private string storageIdentifier_App,storageIdentifier_AppDetail;  
    /* 
Method Name :HDFC_DASH_MockDocDetailsResGenerator
Parameters  :string dmsId_App,string dmsId_AppDetail
Description :This constructor for class HDFC_DASH_MockDocDetailsResGenerator.
*/
    public HDFC_DASH_MockDocDetailsResGenerator(string storageIdentifier_App, string storageIdentifier_AppDetail)
    {
        this.storageIdentifier_App = storageIdentifier_App;
        this.storageIdentifier_AppDetail = storageIdentifier_AppDetail;  
    }
    /* 
Method Name :respond
Parameters  :HTTPRequest req
Description :This method would create a fake response for Document details ILPS callout.
*/
    global HTTPResponse respond(HTTPRequest req) 
    {     
         //system.debug('inside mockclass');
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');  
        res.setBody('{"document_details":[{"storage_identifier":"'+storageIdentifier_App+'","doc_rec_srno":null}],"error_message":"Success"}');
        //res.setBody('{"errormessage":"","documentdetails":[{"storageIdentifier":"' + storageIdentifier_App + '","docrecsrno":"343243232"},{"storageIdentifier":"' + storageIdentifier_AppDetail + '","docrecsrno":"3432434544"}]}');
        res.setStatusCode(200);
        system.assertNotEquals(null, req);
        return res;
    }
}