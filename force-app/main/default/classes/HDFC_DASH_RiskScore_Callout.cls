/*
Class: HDFC_DASH_RiskScore_Callout
Author: Punam Marbate
Date: 5 Aug 2021
Company: Accenture
Description: Queueable Class to make Risk Score Callout
*/
public inherited sharing class HDFC_DASH_RiskScore_Callout implements Queueable, Database.AllowsCallouts{
    Id appId;
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    RestRequest request;
    Exception exp;
    public HDFC_DASH_RiskScore_Callout(Id appId)
    {
        this.appId = appId;
    }
    
    /*
    Method: execute
    Description: This method will Post the Document Details to ILPS.
    */
    public void execute(QueueableContext context) 
    {
        //system.debug('This is Risk Score Detail Callout');
        try
        {             
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();     
            
            //Get the Document details request body
            HDFC_DASH_APIRequest_RiskScore reqBody = getRequestBody(); 
            //system.debug('HDFC_DASH_RiskScore_Callout Request '+ reqBody);    
            
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_RISKSCORE);
            //system.debug('Request Generated'+ requestHeader);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_RISKSCORE, 
                                            HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), 
                                            requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,
                                            requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            //system.debug('Response Generated'+response);

            processResponse(response);     

            HDFC_DASH_ConfidenceRuleEngineCallout confidenceRuleCallout = new HDFC_DASH_ConfidenceRuleEngineCallout(appId); 
            system.enqueueJob(confidenceRuleCallout);
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_RISKSCORE, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }       
    /*
Method: getRequestBody
Parameters:None
Description: This method will create the requestBody from the request.
*/ 
    public HDFC_DASH_APIRequest_RiskScore getRequestBody() 
    {
        HDFC_DASH_APIRequest_RiskScore riskScoreReq = new HDFC_DASH_APIRequest_RiskScore();

        
        List<String> oppFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber };
        Opportunity app = HDFC_DASH_Application_Selector.getApplicationBasedOnId(oppFieldList,appId); 
        List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING,
                                HDFC_DASH_Constants.Contact_Account_HDFC_DASH_Customer_Number,
                                HDFC_DASH_Constants.HDFC_DASH_ILPS_Customer_Number
                                /*HDFC_DASH_Constants.APPDETAIL_CONTACT_R_ILPS_UNI_CUST_NUMBER*/
                            };
        Id recordTypeId =  Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByDeveloperName().get(HDFC_DASH_Constants.PRIMARY_APPLICANT).getRecordTypeId();
        Map<String,String> condition=new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Application => HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + appId + HDFC_DASH_Constants.STRING_QUOTE,
                                HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.RECORD_TYPE_ID => HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + recordTypeId + HDFC_DASH_Constants.STRING_QUOTE };
                                    
          //select applicant detail based on application and record type
        List<HDFC_DASH_Applicant_Details__c> primaryApplicantList=HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(listOfFields_AppDetails,condition);
        riskScoreReq.sfApplId= app?.HDFC_DASH_ApplicationNumber__c;
         if(!primaryApplicantList.isEmpty()){
            for(HDFC_DASH_Applicant_Details__c applDetailRec:primaryApplicantList){
                riskScoreReq.sfCustNo=applDetailRec?.HDFC_DASH_Contact__r?.Account.HDFC_DASH_Customer_Number__c;
                riskScoreReq.custNo=applDetailRec?.HDFC_DASH_ILPS_Customer_Number__c;//new Customer Number Change
                riskScoreReq.unqCustNo=applDetailRec?.HDFC_DASH_Contact__r?.Account.HDFC_DASH_Customer_Number__c;
                break;
            }
        }
        //system.debug('Risk Score Details '+ riskScoreReq);
        return riskScoreReq;
               
    }
    
    /*
    Method: processResponse
    Parameters: Response
    Description: This method will process the response and store Document Serial number into document object.
    */
    public void processResponse(HttpResponse response)
    {
        List<HDFC_DASH_Applicant_Details__c> AppDetails = new List<HDFC_DASH_Applicant_Details__c>(); 
        List<HDFC_DASH_Applicant_Details__c> listUpdateApplicantRikScore=new List<HDFC_DASH_Applicant_Details__c>();
        HDFC_DASH_ParseResponseRiskScore calloutResponse = HDFC_DASH_ParseResponseRiskScore.parse(response.getBody());
        
        if(response != null && response.getStatusCode() == HDFC_DASH_Constants.INT_TWO_HUNDRED)
        {
            //system.debug('inside 1st if');
            
            //system.debug('HDFC_DASH_ParseResponseRiskScore => '+ calloutResponse);
            List<Id> listAppID = new List<Id>{appId};  
            List<Opportunity> listApp = new List<Opportunity>(); 
            List<string> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber};
            List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING,
                                                HDFC_DASH_Constants.APPDETAIL_CONTACT_R_CONTACT_NUMBER};
            listApp = HDFC_DASH_Application_Selector.getApplication(listAppID, listOfFields_App, HDFC_DASH_Constants.ID_STRING);
            if(calloutResponse<>null && calloutResponse.custRiskDetail<> null &&  calloutResponse.custRiskDetail.size() >0){ 
                HDFC_DASH_ParseResponseRiskScore.CustRiskDetail riskDetail= calloutResponse.custRiskDetail[0];
                for(HDFC_DASH_Applicant_Details__c appDetail:HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppID, 
                                                                        listOfFields_AppDetails, 
                                                                        HDFC_DASH_Constants.HDFC_DASH_Application)){
                    appDetail.HDFC_DASH_RiskScore_Customer_Status__c=riskDetail.customerStatus;
                    appDetail.HDFC_DASH_RiskScore_Customer_flags__c=riskDetail.customerFlags;
                    appDetail.HDFC_DASH_RiskScore_Rtr_Exists__c=riskDetail.rtrExists;
                    appDetail.HDFC_DASH_RiskScore_Customer_grade__c=riskDetail.customerGrade;
                    appDetail.HDFC_DASH_RiskScore_Risk_event__c=riskDetail.riskEvent;
                    appDetail.HDFC_DASH_RiskScore_Availed_moratorium__c=riskDetail.riskScoreAvailedMoratorium;
                    appDetail.HDFC_DASH_RiskScore_Availed_restruct__c=riskDetail.riskScoreAvailedRestruct;
                    appDetail.HDFC_DASH_RiskScore_Hdfc_Staff__c=riskDetail.riskScoreHdfcStaff;
                    appDetail.HDFC_DASH_RiskScore_Confidence_score__c=riskDetail.riskScoreConfidenceScore;
                    appDetail.HDFC_DASH_RiskSore_Exist_in_neg_list__c=riskDetail.riskScoreExistInNegList;
                    appDetail.HDFC_DASH_EM_RiskScore__c=calloutResponse.errorMessage;
                    listUpdateApplicantRikScore.add(appDetail);
                }
            }
            //system.debug('listUpdateApplicantRikScore '+ listUpdateApplicantRikScore.size());    
            Database.Update(listUpdateApplicantRikScore);
            //system.debug('appId==='+appId);
            
        }
    } 
}