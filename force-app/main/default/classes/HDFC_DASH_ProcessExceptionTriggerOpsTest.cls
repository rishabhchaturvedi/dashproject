/**
className: HDFC_DASH_ProcessExceptionTriggerOpsTest 
DevelopedBy: Anmol
Date: 27 May 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_ProcessExceptionTriggerOps class.

**/
@isTest(SeeAllData = false)
private class HDFC_DASH_ProcessExceptionTriggerOpsTest {
private static final string ASSERT_MSG = 'Assert Message';    
    
 /* 
Method Name :testProcessExcepWithOne
Description :This method would test the trigger for inserting one Platform Event.
*/
@isTest public static void testProcessExcepWithOnePE() {  
    List<HDFC_DASH_Exception_Logs__e> peList = HDFC_DASH_TestDataFactory.createBasicPlatformEventlist(1);
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    Test.startTest();
    System.runAs(sysAdmin){
       eventBus.publish(peList);
       Test.stopTest();
          if(HDFC_DASH_Constants.TRUESTR.equalsignorecase(System.Label.HDFC_DASH_ProcessExcepFlag)){
              system.assertEquals(1,[select count() from HDFC_DASH_Exception_Log__c 
                                  	 where HDFC_DASH_Record_ID__c =:peList[0].HDFC_DASH_Record_ID__c ],ASSERT_MSG);
            }
            else{
                system.assertEquals(0,[select count() from HDFC_DASH_Exception_Log__c 
                                       where HDFC_DASH_Record_ID__c =:peList[0].HDFC_DASH_Record_ID__c ],ASSERT_MSG);
            }
 
        }
    }
/* 
Method Name :testProcessExcepWithZero
Description :This method would test the trigger for inserting no Platform Event.
*/
@isTest public static void testProcessExcepWithZeroPE() {
      User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
      List<HDFC_DASH_Exception_Logs__e> pelist = new List<HDFC_DASH_Exception_Logs__e>();
      Test.startTest();
      System.runAs(sysAdmin){
      eventBus.publish(pelist);
       Test.stopTest();
          if(HDFC_DASH_Constants.TRUESTR.equalsignorecase(System.Label.HDFC_DASH_ProcessExcepFlag)){
               system.assertEquals(0,[select count() from HDFC_DASH_Exception_Log__c limit 1],ASSERT_MSG);
            }
            else{
               system.assertEquals(0,[select count() from HDFC_DASH_Exception_Log__c limit 1],ASSERT_MSG);
            }
        }
    } 
}