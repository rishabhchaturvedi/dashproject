/*  
Class Name: HDFC_DASH_SalesforceDedupe
Author: Sai Shruthi Akkireddy
Description: Utility class for performing common actions
*/
public class HDFC_DASH_SalesforceDedupe {
    //Public static Map<id,Id> mapAppDetConIds = new Map<id,Id>();

    Public static List<HDFC_DASH_Applicant_Details__c> matchingApplicants = new List<HDFC_DASH_Applicant_Details__c>();
    Public static List<HDFC_DASH_Applicant_Details__c> nonMatchingApplicants = new List<HDFC_DASH_Applicant_Details__c>();
    Public static List<HDFC_DASH_Applicant_Details__c> newApplicants = new List<HDFC_DASH_Applicant_Details__c>();
    /*  
    Method Name: runSalesforceDedupe
    Description: This method would run salesforce dedupe on Applicant details parser
    */
    public static map<string, List<HDFC_DASH_Applicant_Details__c>> runSalesforceDedupeonAppDet(List<HDFC_DASH_Applicant_Details__c> listApplDetails){
        Map<string, List<HDFC_DASH_Applicant_Details__c>> applicantDetailMap= new  Map<string, List<HDFC_DASH_Applicant_Details__c>> ();
        Map<Id,Id> conAccountMap = new Map<Id,Id>();
        applicantDetailMap.put(HDFC_DASH_Constants.HDFC_DASH_Matching,matchingApplicants);
        applicantDetailMap.put(HDFC_DASH_Constants.HDFC_DASH_NonMatching,nonMatchingApplicants);
        applicantDetailMap.put(HDFC_DASH_Constants.HDFC_DASH_NewApplicant,newApplicants);

        List<HDFC_DASH_Applicant_Details__c> appDetToReturn = new List<HDFC_DASH_Applicant_Details__c>();
        List<HDFC_DASH_Applicant_Details__c> appDetWithoutPan = new List<HDFC_DASH_Applicant_Details__c>();
        Map<String,HDFC_DASH_Applicant_Details__c> appDetPanMap = new Map<String,HDFC_DASH_Applicant_Details__c>();
        Map<String,List<String>> mapConIdToCondition = new Map<String,List<String>>();
        List<String> panNos = new  List<String>();
        for(HDFC_DASH_Applicant_Details__c appDetRec : listApplDetails){
            if(appDetRec?.HDFC_DASH_PAN__c != NULL && appDetRec?.HDFC_DASH_Applicant_Capacity__c == HDFC_DASH_Constants.STRING_C)
            {
                panNos.add(appDetRec?.HDFC_DASH_PAN__c);
                appDetPanMap.put(appDetRec?.HDFC_DASH_PAN__c,appDetRec);
            }
            else if(appDetRec?.HDFC_DASH_Applicant_Capacity__c == HDFC_DASH_Constants.STRING_C){
                appDetWithoutPan.add(appDetRec);
            }
        } 
        List<String> panNosMatchingConFound = new List<String>();
        List<Contact> conRecs = new List<Contact>();
        if(appDetPanMap.size() > HDFC_DASH_Constants.INT_ZERO)
        {
            List<String> fields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,HDFC_DASH_Constants.STRING_FirstName,HDFC_DASH_Constants.STRING_LastName,
                                                    HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.STRING_MobilePhone,HDFC_DASH_Constants.STRING_name,HDFC_DASH_Constants.HDFC_DASH_PAN};
            conRecs = HDFC_DASH_Contact_Selector.getContacts(panNos, fields, HDFC_DASH_Constants.HDFC_DASH_PAN);
            for(Contact conRec : conRecs)
            {
                HDFC_DASH_Applicant_Details__c appDetRec = appDetPanMap.get(conRec?.HDFC_DASH_PAN__c);   
                panNosMatchingConFound.add(conRec.HDFC_DASH_PAN__c);  
                if((conRec?.HDFC_DASH_Date_Of_Birth__c == appDetRec?.HDFC_DASH_Date_Of_Birth__c) ||
                    (conRec?.Email == appDetRec?.HDFC_DASH_Email__c) || (conRec?.MobilePhone == appDetRec?.HDFC_DASH_Mobile__c)||
                    HDFC_DASH_UtilityClass.doesNameMatch(appDetRec?.HDFC_DASH_FirstName__c,appDetRec?.HDFC_DASH_LastName__c,conRec?.FirstName,conRec?.LastName))
                {
                    appDetRec.HDFC_DASH_Contact__c = conRec.id;
                  

                    if((conRec?.Email == appDetRec?.HDFC_DASH_Email__c) || (conRec?.MobilePhone == appDetRec?.HDFC_DASH_Mobile__c)){
                        matchingApplicants.add(appDetRec);
                        //system.debug('adding to matching--'+matchingApplicants);
                    }
                    else if((conRec?.Email != appDetRec?.HDFC_DASH_Email__c) && (conRec?.MobilePhone != appDetRec?.HDFC_DASH_Mobile__c)){
                        nonMatchingApplicants.add(appDetRec);
                        //system.debug('adding to Non matching--'+nonMatchingApplicants);
                    }
                }
                else{
                    dedupeWithoutPan(appDetRec,conRec);
                }
                
            }
            if(conRecs.size() == HDFC_DASH_Constants.INT_ZERO || panNosMatchingConFound.size() < panNos.size())
            {
                for(string panNo : panNos){
                    if(!panNosMatchingConFound.contains(panNo))
                    {
                        HDFC_DASH_Applicant_Details__c appDetRec = appDetPanMap.get(panNo);
                        appDetWithoutPan.add(appDetRec);
                    }
                }
            }
           
            List<HDFC_DASH_Applicant_Details__c> appDetRecs = new List<HDFC_DASH_Applicant_Details__c>(appDetPanMap.values());
            appDetToReturn.addAll(appDetRecs);
        }
        //Pan Null 
        if(appDetWithoutPan.size() > HDFC_DASH_Constants.INT_ZERO )
        {
            List<String> dateOfBirthList = new List<String>();
            List<String> firstNameList = new List<String>();
            List<String> lastNameList = new List<String>();
            List<String> emailList = new List<String>();
            List<String> mobileList = new List<String>();
            for(HDFC_DASH_Applicant_Details__c appDetRec : appDetWithoutPan){
                dateOfBirthList.add(String.valueOf(appDetRec.HDFC_DASH_Date_Of_Birth__c).removeEnd(HDFC_DASH_Constants.STRING_TIME));
                firstNameList.add(appDetRec.HDFC_DASH_FirstName__c);
                lastNameList.add(appDetRec.HDFC_DASH_LastName__c);
                emailList.add(appDetRec.HDFC_DASH_Email__c); 
                mobileList.add(appDetRec.HDFC_DASH_Mobile__c);  
            }
            List<String> conFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,HDFC_DASH_Constants.STRING_FirstName,HDFC_DASH_Constants.STRING_LastName,
             									   HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.STRING_MobilePhone,HDFC_DASH_Constants.STRING_name,HDFC_DASH_Constants.HDFC_DASH_PAN,HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number};
            List<Contact> conPanNullRecs = HDFC_DASH_Contact_Selector.runConQueryForDedupePanNull(conFields,dateOfBirthList,emailList,firstNameList,lastNameList,mobileList);
            //List<Contact> conPanNullRecs = HDFC_DASH_Contact_Selector.getContactsWithMultipleList(conFields,mapConIdToCondition, orAnd);

            for(Contact conRec : conPanNullRecs)
            {
                for(HDFC_DASH_Applicant_Details__c appDetRec : appDetWithoutPan)
                {
				    dedupeWithoutPan(appDetRec,conRec);
                }     
            }
        	appDetToReturn.addAll(appDetWithoutPan);
        }
        //create a new Contact
        Map<HDFC_DASH_Applicant_Details__c,Account> personAccToInsert = new Map<HDFC_DASH_Applicant_Details__c,Account>();
        List<HDFC_DASH_Applicant_Details__c> appDetWithoutConPop = new List<HDFC_DASH_Applicant_Details__c>();
        for(HDFC_DASH_Applicant_Details__c appDetRec : appDetToReturn)
        {
            if(String.isBlank(appDetRec.HDFC_DASH_Contact__c)){
                Account personAccRec = new Account();
                //conRec = createContactRecord(appDetRec);
				appDetWithoutConPop.add(appDetRec);               
                personAccRec = createPersonAccount(appDetRec);
                appDetRec.HDFC_DASH_Customer_Has_Application__c = false;
                appDetRec.HDFC_DASH_Stop_Customer_Data_Edit__c = false;
                appDetRec.HDFC_DASH_Is_First_Application_For_Cust__c = true;
                personAccToInsert.put(appDetRec,personAccRec);
            }
        }
        if(personAccToInsert.values() !=NULL){
            insert personAccToInsert.values();
            List<Id> listAccountIds = new List<Id>();
            for(Account acc : personAccToInsert.values())
            {
                listAccountIds.add(acc.Id);
            }
            List<String> fieldConList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_ACCOUNTID};
            conRecs = HDFC_DASH_Contact_Selector.getContacts(listAccountIds,fieldConList, HDFC_DASH_Constants.STRING_ACCOUNTID);
            //system.debug('conRec'+conRecs);
            
            for(Contact con : conRecs){
                conAccountMap.put(con.AccountId,con.Id);
            }
        }
        //Connect the contact with Applicant detail
        for(HDFC_DASH_Applicant_Details__c appDetRec : appDetWithoutConPop)
        {
            //system.debug('conAccountMap' + conAccountMap);
            //system.debug('personAccToInsert.get(appDetRec): '+personAccToInsert.get(appDetRec));
             if(conAccountMap.get(personAccToInsert.get(appDetRec)?.id)!=NULL)
             {
				appDetRec.HDFC_DASH_Contact__c = conAccountMap.get(personAccToInsert.get(appDetRec)?.id);
                newApplicants.add(appDetRec);
                //system.debug('adding to New Applicant--'+newApplicants);
             }
        } 
        return applicantDetailMap;
    }

    /*  
    Method Name: runSalesforceDedupeOnRef
    Description: This method would run salesforce dedupe on Applicant details parser
    */
    public static List<HDFC_DASH_Applicant_Details__c> runSalesforceDedupeOnRef(List<HDFC_DASH_Applicant_Details__c> listApplDetails){
        Map<String,List<String>> conIdToConditionMap = new Map<String,List<String>>();
        Map<String,HDFC_DASH_Applicant_Details__c> emailToapplMap = new Map<String,HDFC_DASH_Applicant_Details__c>();
        Map<HDFC_DASH_Applicant_Details__c,Account> personAccToInsert = new Map<HDFC_DASH_Applicant_Details__c,Account>();
        List<HDFC_DASH_Applicant_Details__c> appDetToReturn = new List<HDFC_DASH_Applicant_Details__c>();
        List<Contact> conRecs = new List<Contact>();
        List<String> conditionList = new List<String>();
        List<String> firstNameList = new List<String>();
        List<String> lastNameList = new List<String>();
        List<String> emailList = new List<String>();
        List<String> mobileNumberList = new List<String>();
        Map<Id,Id> conAccountMap = new Map<Id,Id>();

        List<String> fieldList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_FirstName,HDFC_DASH_Constants.STRING_LastName,
            HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.STRING_MobilePhone,HDFC_DASH_Constants.STRING_name};
        
        //Map created to access appldetails in contacts list
        for(HDFC_DASH_Applicant_Details__c appldetail: listApplDetails){
            if(appldetail?.HDFC_DASH_Applicant_Capacity__c == HDFC_DASH_Constants.STRING_R){
                //system.debug('xxx Referecne + ' + appldetail);
                firstNameList.add(appldetail.HDFC_DASH_FirstName__c);
                lastNameList.add(appldetail.HDFC_DASH_LastName__c);
                emailList.add(appldetail.HDFC_DASH_Email__c);
                mobileNumberList.add(appldetail.HDFC_DASH_Mobile__c);
            }
        }

        conIdToConditionMap.put(HDFC_DASH_Constants.STRING_FirstName, firstNameList);
        conIdToConditionMap.put(HDFC_DASH_Constants.STRING_LastName, lastNameList);
        conIdToConditionMap.put(HDFC_DASH_Constants.STRING_Email, emailList);
        conIdToConditionMap.put(HDFC_DASH_Constants.STRING_MobilePhone, mobileNumberList);
        if(conIdToConditionMap.size() > HDFC_DASH_Constants.INT_ZERO ){
            conRecs = HDFC_DASH_Contact_Selector.getContactsWithMultipleList(fieldList, conIdToConditionMap, HDFC_DASH_Constants.STRING_AND);
         }

        if(conRecs != null && conRecs.size() > HDFC_DASH_Constants.INT_ZERO){
            for(Contact con: conRecs){

                //HDFC_DASH_Applicant_Details__c appldetail = emailToapplMap.get(con?.Email);
                for(HDFC_DASH_Applicant_Details__c appldetail: listApplDetails){

                    if((con?.Email == appldetail?.HDFC_DASH_Email__c) && (con?.MobilePhone == appldetail?.HDFC_DASH_Mobile__c)
                    && HDFC_DASH_UtilityClass.doesNameMatch(appldetail.HDFC_DASH_FirstName__c, appldetail.HDFC_DASH_LastName__c, con.FirstName, con.LastName)){
                        appldetail.HDFC_DASH_Contact__c = con.id;
                        appDetToReturn.add(appldetail);
                    }
                }
            }
        }
        else{
            for(HDFC_DASH_Applicant_Details__c appldetail: listApplDetails){
                if(appldetail.HDFC_DASH_Contact__c == null && appldetail?.HDFC_DASH_Applicant_Capacity__c == HDFC_DASH_Constants.STRING_R){
                    //Contact newContact = createContactRecord(appldetail);
                    Account newPersonAccount = createPersonAccount(appldetail);
                    //system.debug('xxx Reference Check ' + newPersonAccount); 
                    personAccToInsert.put(appldetail, newPersonAccount);
                }
            }
            
            if(personAccToInsert.values() != NULL && personAccToInsert.values().size() > HDFC_DASH_Constants.INT_ZERO){
                //system.debug('xxx Reference Check before insert' + personAccToInsert.values());
                insert personAccToInsert.values();
                
            
                List<Id> listAccountIds = new List<Id>();
                for(Account acc : personAccToInsert.values())
                {
                    listAccountIds.add(acc.Id);
                }
                List<String> fieldConList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_ACCOUNTID};
                conRecs = HDFC_DASH_Contact_Selector.getContacts(listAccountIds,fieldConList, HDFC_DASH_Constants.STRING_ACCOUNTID);
                //system.debug('conRec'+conRecs);
              
                for(Contact con : conRecs){
                    conAccountMap.put(con.AccountId,con.Id);
                }
                
                if(conAccountMap != NULL && conAccountMap.size() > HDFC_DASH_Constants.INT_ZERO){
                    for(HDFC_DASH_Applicant_Details__c appldetail: listApplDetails){
                        if(appldetail.HDFC_DASH_Contact__c == null){
                            appldetail.HDFC_DASH_Contact__c = conAccountMap.get(personAccToInsert.get(appldetail)?.id);
                        }
                        appDetToReturn.add(appldetail);
                    }
                }
            }
        }
        
        return appDetToReturn;
    }

    /*  
    Method Name: dedupeWithoutPan
    Description: This method would run salesforce dedupe without pan.
    */
    public static void dedupeWithoutPan(HDFC_DASH_Applicant_Details__c appDetRec,Contact conRec)
    {
        if(String.isNotBlank(appDetRec.HDFC_DASH_FirstName__c) && String.isNotBlank(appDetRec.HDFC_DASH_LastName__c) &&
            String.isNotBlank(conRec.FirstName) && String.isNotBlank(conRec.LastName))
        {
            if(HDFC_DASH_UtilityClass.doesNameMatch(appDetRec.HDFC_DASH_FirstName__c ,appDetRec.HDFC_DASH_LastName__c,conRec.FirstName,conRec.LastName) && 
                ((conRec.Email == appDetRec.HDFC_DASH_Email__c && conRec.MobilePhone == appDetRec.HDFC_DASH_Mobile__c) ||
                (conRec.Email == appDetRec.HDFC_DASH_Email__c && conRec.HDFC_DASH_Date_Of_Birth__c == appDetRec.HDFC_DASH_Date_Of_Birth__c) ||
                (conRec.MobilePhone == appDetRec.HDFC_DASH_Mobile__c && conRec.HDFC_DASH_Date_Of_Birth__c == appDetRec.HDFC_DASH_Date_Of_Birth__c)))
            {
                appDetRec.HDFC_DASH_Contact__c = conRec.id; 
                
                if((conRec?.Email == appDetRec?.HDFC_DASH_Email__c) || (conRec?.MobilePhone == appDetRec?.HDFC_DASH_Mobile__c)){
                    if(!matchingApplicants.contains(appDetRec)){
                    matchingApplicants.add(appDetRec);
                    //system.debug('adding to matching--'+matchingApplicants);
                    }
                }
                
            }
        }
    } 
    /* 
    Method Name :createPersonAccount
    Parameters  :HDFC_DASH_ParseLeadDetails LeadDetails
    Description :This method would create a contact record based on the request.
    */
    public static Account createPersonAccount(HDFC_DASH_Applicant_Details__c appDetRec)
    {
        //Contact conRec = New Contact();
        Account newPersonAcc = new Account(); 
        newPersonAcc.HDFC_DASH_Is_Customer__pc = true;
        newPersonAcc.Salutation = appDetRec.HDFC_DASH_Salutation__c;
        newPersonAcc.FirstName = appDetRec.HDFC_DASH_FirstName__c;
        newPersonAcc.MiddleName = appDetRec.HDFC_DASH_MiddleName__c;
        newPersonAcc.LastName = appDetRec.HDFC_DASH_LastName__c;
        newPersonAcc.PersonEmail = appDetRec.HDFC_DASH_Email__c;
        newPersonAcc.PersonMobilePhone = appDetRec.HDFC_DASH_Mobile__c;
        newPersonAcc.HDFC_DASH_Mobile_Country_Code__pc = appDetRec.HDFC_DASH_Mobile_Country_Code__c;
        newPersonAcc.HDFC_DASH_PAN__pc = appDetRec.HDFC_DASH_PAN__c;
        newPersonAcc.HDFC_DASH_Pan_Applied__pc = appDetRec.HDFC_DASH_Pan_Applied__c;
        newPersonAcc.HDFC_DASH_Pan_Status_From_NSDL__pc = appDetRec.HDFC_DASH_Pan_Status_From_NSDL__c;
        newPersonAcc.HDFC_DASH_Date_Of_Birth__pc = appDetRec.HDFC_DASH_Date_Of_Birth__c;
        newPersonAcc.HDFC_DASH_Gender__pc = appDetRec.HDFC_DASH_Gender__c;
        newPersonAcc.HDFC_DASH_City_Code__pc = appDetRec.HDFC_DASH_City_Code__c;
        newPersonAcc.HDFC_DASH_State_Code__pc = appDetRec.HDFC_DASH_State_Code__c;
        newPersonAcc.HDFC_DASH_Country_Code__pc = appDetRec.HDFC_DASH_Country_Code__c;
        newPersonAcc.HDFC_DASH_City__pc = appDetRec.HDFC_DASH_City__c;
        newPersonAcc.HDFC_DASH_State__pc = appDetRec.HDFC_DASH_State__c; 
        newPersonAcc.HDFC_DASH_Country__pc = appDetRec.HDFC_DASH_Country__c;
        newPersonAcc.HDFC_DASH_Consent_Content__pc = appDetRec.HDFC_DASH_Consent_Content__c;
        newPersonAcc.HDFC_DASH_SMS_Consent__pc = appDetRec.HDFC_DASH_SMS_Consent__c;
        newPersonAcc.HDFC_DASH_SMS_Consent_Date__pc = appDetRec.HDFC_DASH_SMS_Consent_Date__c;
        newPersonAcc.HDFC_DASH_Voice_Consent__pc = appDetRec.HDFC_DASH_Voice_Consent__c;
        newPersonAcc.HDFC_DASH_Voice_Consent_Date__pc = appDetRec.HDFC_DASH_Voice_Consent_Date__c;
        newPersonAcc.HDFC_DASH_WhatsApp_Consent__pc = appDetRec.HDFC_DASH_WhatsApp_Consent__c;
        newPersonAcc.HDFC_DASH_WhatsApp_Consent_Date__pc = appDetRec.HDFC_DASH_WhatsApp_Consent_Date__c;
        //conRec.HDFC_DASH_City_RecordID__c = appDetRec.HDFC_DASH_City_RecordID__c;
        //conRec.HDFC_DASH_Country_RecordID__c = appDetRec.HDFC_DASH_Country_RecordID__c;
        //conRec.HDFC_DASH_State_RecordID__c = appDetRec.HDFC_DASH_State_RecordID__c;
        //conRec = (Contact)HDFC_DASH_UtilityClass.generateMasterTabelFields(conRec);
        newPersonAcc.HDFC_DASH_Resident_Type__pc = appDetRec.HDFC_DASH_Resident_Type__c;
        newPersonAcc.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.STRING_PERSONACCOUNT).getRecordTypeId();
        //system.debug('newPersonAcc--'+newPersonAcc);
        return newPersonAcc;
        //return conRec;
    }    	
}