/**
className: HDFC_DASH_ActiveCases 
DevelopedBy: Deepali Gupta
Date: 19 Nov 2021
Company: Accenture
Class Description: To count active cases for particular role
**/
public with sharing class HDFC_DASH_ActiveCases {
    
/* 
Method Name :countActiveCases
Description :This method would be called to from flow to count active cases for assignment
*/
    @InvocableMethod
    public static List<Integer> countActiveCases(){
        List<Integer> activeCases = new List<Integer>();
        List<User> userList = new List<User>();
        List<Case> caseList = new List<Case>();
        List<String> userIds = new List<String>();
        List<String> userFieldsList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.USERROLE_NAME};
        List<String> caseFieldsList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.IsClosed,
            HDFC_DASH_Constants.OwnerId};    
        try{
            userList = HDFC_DASH_User_Selector.getUserRecBasedOnString(userFieldsList,HDFC_DASH_Constants.CENTRALTEAM,
                                                                       HDFC_DASH_Constants.USERROLE_NAME);
            if(userList.size()>0){
                for(User userRec : userList){
                    userIds.add(userRec.Id);
                }
            }
            caseList = HDFC_DASH_Case_Selector.getCaseRecsBasedOnList(caseFieldsList,userIds,HDFC_DASH_Constants.OwnerId);
            activeCases.add(caseList.size());
        }
        catch(Exception e){
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString() + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.ACTIVE_CASES, HDFC_DASH_Constants.COUNT_ACTIVE_CASES,NULL,expDetails);
        }
        return activeCases; 
    }
}