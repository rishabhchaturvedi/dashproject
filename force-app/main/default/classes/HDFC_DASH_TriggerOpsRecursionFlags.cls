/**
*   className:HDFC_DASH_TriggerOpsRecursionFlags 
DevelopedBy: Anmol
Date: 25 May 2021
Company: Accenture
Class Description: This class checks for the First Run of the Operations.
* 
*/
public with sharing class HDFC_DASH_TriggerOpsRecursionFlags {
    public static boolean validationIsFirstRun = true;
    public static boolean cont_Desc_IsFirstRun = true;
    public static boolean proc_ExcepIsFirstRun = true;
    public static boolean proc_CaseUpdateIsFirstRun = true;
    public static boolean accChangeEvent_IsFirstRun = true;
    public static boolean contactAddressDetailsIsFirstRun = true;
    public static boolean appDetailsIsFirstRun = true;
    public static boolean caseIsFirstRun = true; 
    public static boolean applicantBankDetailsFirstRun = true;
    public static boolean applicantEmpDetailsFirstRun = true;
    public static boolean leadDupFieldFirstRun = true;
    public static boolean lead_PopulateFields_Insert_FirstRun = true;
    public static boolean OppPopulateFieldsFirstRun = true;
    public static boolean lead_PopulateOwnerAgency_FirstRun = true;
    public static boolean lead_PopulateFields_Update_FirstRun = true;
    public static boolean appPopulateOwnerAgency_FirstRun = true;
    public static boolean appPopulateFields_Update_FirstRun = true;
    public static boolean contentVersionFirstRun = true;
    public static boolean applicantEmpDetails_AD_NatureOfEmp_FirstRun = true;
    public static boolean infoValRecordsFirstRun = true;
    public static boolean appChangeStageOnBeforeIns = true;
    public static boolean appChangeStageOnBeforeUpdate = true;
    public static boolean appChangeStageNumberOnBeforeUpdate = true;
    public static boolean appChangeFlagsOnBeforeUpdate = true;
    public static boolean userIsFirstRun = true;
    public static boolean infoValRecAfterIns = true;
    public static boolean Contact_Api_FirstRun = true;
    public static boolean Contact_UserCreation_FirstRun = true;
    public static boolean SO_BSA_Api_FirstRun = true;
    public static boolean updatePrimeApplicationNosOnApp = true;
    public static boolean copyAppInfoToCustomerBasedOnFlags = true;
    public static boolean applicationFlagUpdateContacts = true;
    public static boolean copyAppInfoToCustforIsFirstAppCust = true;
    public static boolean taskSendMessage_FirstRun = true;
    public static boolean infoVal_DeleteConDoc_FirstRun = true;
    public static boolean infoVal_DDCALLOUT_FirstRun = true;
    public static boolean applPostFeeProcessing = true;
    public static boolean applWhenFileFirstPushedToILPS = true;
    public static boolean applWhenFileSecondPushedToILPS = true;
    public static boolean attachmentFirstRun = true;
    public static boolean ilpsFilePushCalloutFlags = true;
    public static boolean currentOutstandingFlag = true;
    public static boolean contact_OptinCalloutInsert_FirstRun = true;
    public static boolean PersonAcc_OptinCalloutInsert_FirstRun = true;
    public static boolean PersonAcc_OptinCalloutUpdate_FirstRun = true;
    public static boolean Event_PopulateSubNameInsert_FirstRun = true;
    public static boolean Event_PopulateSubNameUpdate_FirstRun = true;
    public static boolean Event_PopulateActorExeCodeInsert_FirstRun = true;
    public static boolean Event_PopulateActorExeCodeUpdate_FirstRun = true;
    public static boolean Deposit_Thread_FirstRun = true;
    public static boolean Case_Escalation_Update = true;
    public static boolean Case_Deposit_Assignment = true;
    public static boolean Case_Exceution = true; 
}