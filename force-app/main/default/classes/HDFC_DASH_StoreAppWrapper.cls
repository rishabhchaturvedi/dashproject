public with sharing class HDFC_DASH_StoreAppWrapper {
    
    public Map<HDFC_DASH_ParseApplication.ApplicantInfo,HDFC_DASH_Applicant_Details__c> mapApplicantInfoDetail;
    public List<HDFC_DASH_Applicant_Details__c> listApplicantDetails;
    public List<HDFC_DASH_Applicant_Details__c> insertedApplicantDetailsList;
    public Map<String,HDFC_DASH_Applicant_Details__c> customerNoApplicantMap;
    
    //Payment Details insertion/upsertion
    public Map<Id,HDFC_DASH_Application_Payment_Details__c> mapApplFeeIds;
    public List<HDFC_DASH_Application_Payment_Details__c> payDetToBeUpserted;
}