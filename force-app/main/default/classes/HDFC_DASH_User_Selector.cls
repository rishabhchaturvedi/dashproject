/**
className: HDFC_DASH_User_Selector
DevelopedBy: Utkarsh Patidar
Date: 28 Sep 2021
Company: Accenture
Class Description: This class would create the select queries for User object .
**/
public with sharing class HDFC_DASH_User_Selector {
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from User ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    private static final string QUERY_PARAMETER1 = ' =:queryParameter LIMIT 1';
 
  /* 
    Method Name :getUser
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get a list of Users based on one condition.
    */   
    public static List<User> getUser(List<String> queryParameters,List<string> fieldList, String conditionOn)
    {
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;
       //List<User> listUser = [Select ID  from User where userroleid=:queryParameters LIMIT 1];
        List<User> listUser = database.query(querystring); 
        return listUser;

    }
    
    /* 
    Method Name :getUserBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of User based on fieldsAndparameters sent to it.
    */
    public static List<User> getUserBasedOnQuery(List<String> fieldList,
                                                            Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +
                                FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<User> userList = database.query(querystring); 
        return userList;   
    }
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getUserBasedOnQuery.
    */
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }
    
    /* 
    Method Name :getUserRecBasedOnString
    Parameters  :List<string> fieldList, String queryParameter, String conditionField
    Description :This method would get the User based on field sent to it.
    */
    public static List<User> getUserRecBasedOnString(List<string> fieldList, String queryParameter, String conditionField){
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
                             +HDFC_DASH_CONSTANTS.STRING_FROMUSER + conditionField + QUERY_PARAMETER1;
        system.debug('querystring :-'+querystring);
        List<User> userList = database.query(querystring); 
       
        return userList;
    }

}