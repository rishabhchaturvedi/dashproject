/**
className: HDFC_DASH_BkDetailTriggerOpsTest
DevelopedBy: Sai Suman
Date: 01 October 2021
Company: Accenture
Class Description: test class for HDFC_DASH_BkDetailTriggerOps.
**/
@isTest(SeeAllData = false)
public class HDFC_DASH_BkDetailTriggerOpsTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id); 
    private static HDFC_DASH_Applicant_Details__c appDetails = 
    HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Bank_Detail__c bkDetails =
    HDFC_DASH_TestDataFactory_BankDetails.getBasicApplBankDetail(appDetails.id,NULL);
    private static List<HDFC_DASH_Applicant_Bank_Detail__c> bkDetailsList =
    HDFC_DASH_TestDataFactory_BankDetails.getBasicApplBankDetailList(3,appDetails.id,con.id);
    /*  
Method Name :testInsertbankDetail
Description :This method would test the trigger for inserting Applicant Bank Details.
*/
    @isTest public static void testupdateBKDetRec() 
    { 
        Exception testException ;
        System.runAs(sysAdmin)
        {
            test.startTest();
           // HDFC_DASH_Applicant_Bank_Detail__c bkdet = new HDFC_DASH_Applicant_Bank_Detail__c();
            try
            {
                //bkdet.HDFC_DASH_Contact__c = con.Id;
                //insert bkDetails;
                List<HDFC_DASH_Applicant_Bank_Detail__c> bkDetList = new List<HDFC_DASH_Applicant_Bank_Detail__c>(); 
                for(HDFC_DASH_Applicant_Bank_Detail__c bankDet :bkDetailsList){
                    bankDet.HDFC_DASH_Bank_Detail_Latest__c = HDFC_DASH_Constants.STRING_YES;
                    bkDetList.add(bankDet);
                }
                update bkDetList;
                bkDetails.HDFC_DASH_Contact__c = con.Id;
                update bkDetails;
                
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            HDFC_DASH_Applicant_Bank_Detail__c bankDetailRec = [select id,HDFC_DASH_Bank_Detail_Latest__c from HDFC_DASH_Applicant_Bank_Detail__c where id =:bkDetails.id];
            system.assertEquals(HDFC_DASH_Constants.STRING_YES,bankDetailRec.HDFC_DASH_Bank_Detail_Latest__c);
            system.assertEquals(null, testException);
        }
    }
  
    /* 
Method Name :testForException
Description :This method would test the trigger for exception.
*/
    @isTest public static void testForException()
    {
        Exception testException ;
        System.runAs(sysAdmin)
        {
            try{
                HDFC_DASH_Applicant_Bank_Detail__c excep = new HDFC_DASH_Applicant_Bank_Detail__c();
                excep.HDFC_DASH_Contact__c = con.id;
                //excep.HDFC_DASH_Address_From__c='18 characters';
                //excep.HDFC_DASH_Pincode__c='123456789A';
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_NULLPOINTER_EXP);
                database.insert(excep);
            }
            catch (Exception e) { 
                testException=e;
            }
            system.assertNotEquals(null, testException);
        }
    }
}