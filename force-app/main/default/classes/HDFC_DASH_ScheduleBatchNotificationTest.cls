/**
className: HDFC_DASH_ScheduleBatchNotificationTest
DevelopedBy: Tejeswari
Date: 27 January 2022
Company: Accenture 
Class Description: Test class for  HDFC_DASH_ScheduleBatchNotification class.
**/
@isTest(SeeAllData = false)
public class HDFC_DASH_ScheduleBatchNotificationTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string SCHEDULE_EXPRESSION = '0 0 23 * * ?' ;
    private static final string TEST_NOTIFICATION_CHECK = 'Test Notification Check';
    /* 
Method Name :testschedule
Description :This method would test HDFC_DASH_ScheduleBatchNotification class.
*/
    public static testMethod void testschedule() {
        Exception testException;
        System.runAs(sysAdmin) {
            try{
                Test.StartTest();
                HDFC_DASH_ScheduleBatchNotification testsche = new HDFC_DASH_ScheduleBatchNotification();
                system.schedule(TEST_NOTIFICATION_CHECK, SCHEDULE_EXPRESSION, testsche);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            system.assertEquals(null, testException);
        }
    }
}