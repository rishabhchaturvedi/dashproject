/**
className: HDFC_DASH_SObject_Selector
DevelopedBy: Tejeswari
Date: 26 November 2021
Company: Accenture
Class Description: This class would create the select queries for setup objects.
**/
public with sharing class HDFC_DASH_SObject_Selector {
    private static final string FROM_STRING = ' from ';
    private static final string SELECT_STRING = 'Select ';
    private static final string WHERE_STRING = ' where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    
    /* 
Method Name :getSObjects
Parameters  :List<String> queryParameters, List<string> fieldList, String ObjectName String conditionOn
Description :This method would get the List of Sobjects based on the condition.
*/
    public static List<Sobject> getSObjects(List<string> queryParameters,List<String> fieldList, String objectName, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + objectName + WHERE_STRING + conditionOn + QUERY_PARAMETER;  
        //system.debug('querystring setup' +querystring);
        List<Sobject> resultList = database.query(querystring);        
        
        return resultList;
    } 
}