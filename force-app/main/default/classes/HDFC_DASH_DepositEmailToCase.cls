public class HDFC_DASH_DepositEmailToCase {
    
    @TestVisible private static List<Deposit__x> mockedRequests = new List<Deposit__x>();
    
    @future(callout=true)
    public static void emailToCaseDepositListener(Id caseRecId){
        system.debug('inside deposit email to case');
        List<Case> caseRecs = new List<Case>();
        String EMAILSUBJECTBODY;
        List<String> forwardedDomains = new List<String>();
        Boolean fwdFromHdfcDomain = HDFC_DASH_Constants.BOOLEAN_FALSE;
        String ownerNameQueue;
        List<Group> queueList = new List<Group>();
        List<Account> accList = new List<Account>();
        List<Deposit__x> depList = new List<Deposit__x>();
        Deposit__x depRec = new Deposit__x();
        Boolean containsDepositNumber;
        List<User> userList = new List<User>(); 
        List<User> userListOwner = new List<User>(); 
        List<String> depositNumbersInMail = new List<String>();
        List<String> userFieldsList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.USEREMAIL};
            try{
                caseRecs = [SELECT Id,ownerId,Subject,HDFC_DASH_Home_Branch__c,HDFC_DASH_Service_Centre__c,HDFC_DASH_Deposit_Number__c,
                            ContactId,AccountId,Status,Origin,SuppliedEmail,IsClosed,HDFC_DASH_Skip_AutoAssignment__c,
                            HDFC_DASH_No_Further_Response__c,HDFC_DASH_Mode__c,Deposit_Number_Text__c,Description FROM Case WHERE Id = :caseRecId FOR UPDATE];
                
                system.debug('caseRecs[0].Deposit_Number_Text__c...'+caseRecs[0].Deposit_Number_Text__c);
                
                if(caseRecs[0].Origin == HDFC_DASH_Constants.CASE_ORIGIN_EMAIL){
                    EMAILSUBJECTBODY = caseRecs[0].Subject + HDFC_DASH_Constants.STRING_SPACE + caseRecs[0].Description;
                    forwardedDomains = Label.HDFC_DASH_Forwarding_Email_Domain.split(HDFC_DASH_Constants.COMMA);
                    for(String strDomain : forwardedDomains){
                        if(caseRecs[0].SuppliedEmail.containsIgnoreCase(strDomain)){
                            fwdFromHdfcDomain = HDFC_DASH_Constants.BOOLEAN_TRUE;
                        }
                    }
                    Matcher m = Pattern.compile(Label.HDFC_DASH_Deposit_Number_Pattern).matcher(EMAILSUBJECTBODY);
                    while (m.find()) {
                        depositNumbersInMail.add(m.group());
                        System.debug('deposit number@@ : ' + m.group());
                        System.debug('depNuminmail : ' + depositNumbersInMail[0]);
                    }
                    if(depositNumbersInMail.size() > 0){
                    
                    if(!Test.isRunningTest()){
                    depList = [SELECT Id,BORR_CUST_NO__c,CUST_NAME__c,DEPOSIT_AMT__c,DEPOSIT_NO__c,MATURITY_DATE__c,HOME_BRANCH__c,SERVICE_CENTER__c
                               FROM Deposit__x WHERE DEPOSIT_NO__c = :depositNumbersInMail[0] LIMIT 1];
                               }else{
                                   depList = new List<Deposit__x>(mockedRequests);
                               }
                    }
                    System.debug('depListSize ' + depList.size());
                       // depList = [SELECT Id,BORR_CUST_NO__c,CUST_NAME__c,DEPOSIT_AMT__c,DEPOSIT_NO__c,MATURITY_DATE__c,HOME_BRANCH__c,SERVICE_CENTER__c
                              // FROM Deposit__x WHERE DEPOSIT_NO__c = :caseRecs[0].Deposit_Number_Text__c LIMIT 1];
                    if(depList.size()>0){
                        depRec = depList[0];
                        system.debug('depRec...'+depRec);
                        containsDepositNumber = HDFC_DASH_Constants.BOOLEAN_TRUE;
                        caseRecs[0].HDFC_DASH_Home_Branch__c = depRec.HOME_BRANCH__c;
                        caseRecs[0].HDFC_DASH_Service_Centre__c = depRec.SERVICE_CENTER__c;
                        accList = [SELECT Id,Name,Recordtype.Name,PersonContactId,HDFC_DASH_Customer_Number__c 
                                   FROM Account WHERE HDFC_DASH_Customer_Number__c = :depRec.BORR_CUST_NO__c AND Recordtype.Name = 'Person Account' LIMIT 1];
                    }
                    
                    //fwd from hdfc employee
                    if(caseRecs[0].Subject.startsWithIgnoreCase(Label.HDFC_DASH_Forwarded_Email_String) && fwdFromHdfcDomain){
                        userList = HDFC_DASH_User_Selector.getUserRecBasedOnString(userFieldsList,caseRecs[0].SuppliedEmail,HDFC_DASH_Constants.USEREMAIL);
                        if(containsDepositNumber){
                            if(accList.size() > 0){
                                caseRecs[0].accountId = accList[0].Id;
                                caseRecs[0].ContactId = accList[0].PersonContactId;
                            }
                            caseRecs[0].HDFC_DASH_Deposit_Number__c = depRec.DEPOSIT_NO__c;
                            if(userList.size() > 0){
                                caseRecs[0].OwnerId = userList[0].Id;
                            }
                        }
                        //deposit number not found in database
                        else{
                            if(caseRecs[0].ContactId != null){
                                caseRecs[0].ContactId = null;
                            }
                            caseRecs[0].Status = Label.HDFC_DASH_Unregistered_Mail_Status;
                        }
                    }
                    //from register mail id
                    else if(caseRecs[0].ContactId != null && containsDepositNumber){
                        caseRecs[0].HDFC_DASH_Deposit_Number__c = depRec.DEPOSIT_NO__c;
                    }
                    //from unregister mail id
                    else if(caseRecs[0].ContactId == null && containsDepositNumber){
                        if(accList.size() > 0){
                            caseRecs[0].accountId = accList[0].Id;
                            caseRecs[0].ContactId = accList[0].PersonContactId;
                        }
                        caseRecs[0].HDFC_DASH_Deposit_Number__c = depRec.DEPOSIT_NO__c;
                        caseRecs[0].Status = Label.HDFC_DASH_Unregistered_Mail_Status;
                    }
                    //no deposit number and from unregister mail
                    else{
                        caseRecs[0].Status = Label.HDFC_DASH_Unregistered_Mail_Status;
                    }
                }
                //assign home branch and service centre for all other origin than email
                else if(caseRecs[0].HDFC_DASH_Deposit_Number__c != null){
                    depList = [SELECT Id,BORR_CUST_NO__c,CUST_NAME__c,DEPOSIT_AMT__c,DEPOSIT_NO__c,MATURITY_DATE__c,HOME_BRANCH__c,SERVICE_CENTER__c
                               FROM Deposit__x WHERE DEPOSIT_NO__c = :caseRecs[0].HDFC_DASH_Deposit_Number__c LIMIT 1];
                    if(depList.size()>0){
                        depRec = depList[0];
                        caseRecs[0].HDFC_DASH_Home_Branch__c = depRec.HOME_BRANCH__c;
                        caseRecs[0].HDFC_DASH_Service_Centre__c = depRec.SERVICE_CENTER__c;
                    }
                    caseRecs[0].HDFC_DASH_File_Number__c = '';
                }
                if(!caseRecs[0].isClosed && !caseRecs[0].HDFC_DASH_Skip_AutoAssignment__c && 
                   !caseRecs[0].HDFC_DASH_No_Further_Response__c){
                       if(caseRecs[0].HDFC_DASH_Mode__c == 'Government of India' || caseRecs[0].HDFC_DASH_Mode__c == 'Regulator'){
                           userListOwner = [SELECT Id,Name,UserRole.Name FROM User WHERE UserRole.Name = 'Regulator/DC Role'];
                           if(userListOwner.size() > 0){
                               caseRecs[0].ownerId = userListOwner[0].Id;
                           }
                       }
                       else if(!String.isBlank(caseRecs[0].HDFC_DASH_Home_Branch__c) && !caseRecs[0].Status.containsIgnoreCase('Statuary Escalation')){
                           ownerNameQueue = caseRecs[0].HDFC_DASH_Home_Branch__c + ' Deposit Head';
                           userListOwner = [SELECT Id,Name,UserRole.Name FROM User WHERE UserRole.Name = :ownerNameQueue];
                           if(userListOwner.size() > 0){
                               caseRecs[0].ownerId = userListOwner[0].Id;
                           }
                           else{
                               queueList = [SELECT Id,Name FROM GROUP WHERE Name = :ownerNameQueue];
                               if(queueList.size() > 0){
                                   caseRecs[0].ownerId = queueList[0].Id;
                               }
                           }
                       }
                }
                Database.update(caseRecs);
                //Notification to Case Owner
                if(caseRecs[0].Status != Label.HDFC_DASH_Unregistered_Mail_Status){
                    HDFC_DASH_CaseTriggerOpsHelper.caseAssignmentOmniChannel(caseRecs);
                 }else if(caseRecs[0].Status == Label.HDFC_DASH_Unregistered_Mail_Status){
                          
                          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                          String owdLabel = Label.DepositComplaint;
                          OrgWideEmailAddress owea = [select Id from OrgWideEmailAddress where Address =: owdLabel ];
                          
                          Id emailTemp = [select Id,DeveloperName from EmailTemplate where DeveloperName =: 'Case_Creation'].Id;       
                          
                            mail.setTemplateId(emailTemp);
                            mail.setOrgWideEmailAddressId(owea.Id);
                           
                            mail.toaddresses=new List<String>{caseRecs[0].SuppliedEmail};
                            mail.setTargetObjectId(caseRecs[0].contactid);
                            mail.setWhatId(caseRecs[0].Id);
                            mail.setSaveAsActivity(false);
                            mail.setTreatTargetObjectAsRecipient(false);
            
                            system.debug('Deposit email '+mail.htmlbody);
                            system.debug('plain text body...'+mail.plainTextBody);
                          
                                try{
                                    system.debug('try block...'+mail);
                                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                                }catch(exception e){
                                    system.debug('exception e..'+e);
                                }
                 }
                    
                
            }
        catch(Exception e){
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON 
                + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.CASE_REC_FOR_DEPOSIT,
                                                        HDFC_DASH_Constants.CASE_EVENT,NULL,expDetails);
        }
    }
}