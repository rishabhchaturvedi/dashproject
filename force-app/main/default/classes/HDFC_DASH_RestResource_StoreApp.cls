@RestResource(urlMapping='/hdfc/storeApplication/V1.0')
/*
Author: Anmol Srivastava
Class Name:HDFC_DASH_RestResource_StoreApp
Date: 24 Aug 2021
Company: Accenture
*/
global with sharing class HDFC_DASH_RestResource_StoreApp {
    /*
    Method:storeApplication()
    Description:This method will store the Application data from OS
    */
    @HttpPatch
    global static void storeApplication() {
        RestRequest request = RestContext.request;
        RestResponse res=RestContext.response;  
        
        map<string,string> responseheaders = new Map<string,string>();
        
        exception exp;
        //Savepoint sp = Database.setSavepoint();
        try 
        { 
            responseheaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            Boolean param_StoreFullApplicationFromILPS;
            if(request.params.get(HDFC_DASH_Constants.STRING_StoreFullILPSApplication)!=null){
                param_StoreFullApplicationFromILPS = Boolean.valueOf(request.params.get(HDFC_DASH_Constants.STRING_StoreFullILPSApplication));
            }else{
                param_StoreFullApplicationFromILPS = false;
            }
            system.debug('param_StoreFullApplicationFromILPS--'+param_StoreFullApplicationFromILPS);
            
            if(param_StoreFullApplicationFromILPS)
            {
                system.debug('Inside ILPS storeApp:');
                HDFC_DASH_ParseApplication apiResponseILPS = new HDFC_DASH_ParseApplication();
                apiResponseILPS = HDFC_DASH_InboundRestServiceStoreAppILPS.processApplication(request,param_StoreFullApplicationFromILPS);
                res.responseBody = Blob.valueOf(JSON.serialize(apiResponseILPS)); 
            }
            else 
            {
               
                HDFC_DASH_APIResponse_StoreApp apiResponse = new HDFC_DASH_APIResponse_StoreApp(); 
                apiResponse = HDFC_DASH_InboundRestService_StoreApp.processApplication(request);
                res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));  
            }          
             
        }
        catch(Exception e){
            // Throw Exception
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() 
                            + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_STOREAPP,
                                                        HDFC_DASH_Constants.HANDLE_STORE_APPLICATION, HDFC_DASH_Constants.METHOD_TYPE_PATCH, expDetails);
            
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
            //Database.rollback(sp);
        }
        finally{	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_STOREAPP,
                                                                request.requestBody.tostring(), 
                                                                (res.responseBody)?.toString(), HDFC_DASH_Constants.METHOD_TYPE_PATCH, 
                                                                HDFC_DASH_Constants.HANDLE_STORE_APPLICATION, exp, request.headers);       
            
            if(responseheaders!= NULL){
                for(string responseheaderstr : responseheaders.keySet()){
                    res.addHeader(responseheaderstr, responseheaders.get(responseheaderstr));
                }
            }
        }        
        
    }
    
}