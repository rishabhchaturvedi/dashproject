/*
Author: Anisha Arumugam
Class:HDFC_DASH_ParseResponseSpotOfferFileILPS
Description: This class will parse the response body of Spot Offer File ILPS Callout
*/
public class HDFC_DASH_ParseResponseSpotOfferFileILPS {
    public String sf_appl_id;
    public String file_no;
    public String errorMessage;
    public List<CustDetails> custDetails;
    
    /*
Method: CustDetails
Description: This class will parse the customer details
*/
    public class CustDetails {
        public string sf_cust_no;
        public string cust_no;
        public string unq_cust_no;
    }
    
    /*
Method: parse
Description: This method will parse the response body of Spot Offer File ILPS Callout
*/
    public static HDFC_DASH_ParseResponseSpotOfferFileILPS parse(String json) {
        return (HDFC_DASH_ParseResponseSpotOfferFileILPS) System.JSON.deserialize(json, HDFC_DASH_ParseResponseSpotOfferFileILPS.class);
    }
    
}