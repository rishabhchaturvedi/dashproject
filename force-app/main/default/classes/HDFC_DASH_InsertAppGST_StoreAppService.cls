/**
className: HDFC_DASH_InsertAppGST_StoreAppService
DevelopedBy: Raghavendra Koora
Date: 08 OCT 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the applicant GST details.
**/
public inherited sharing class HDFC_DASH_InsertAppGST_StoreAppService {
    
    /*
    Method Name :mapEmpGSTDetails
    Parameters  :Id applsfEmpIdcantID,HDFC_DASH_ParseApplication.empGstBusinessDetails parseEmp,HDFC_DASH_GST_Business_Details__c gstEmpDetails
    Description :This method would map empGstBusinessDetails node for storeApp API and sends us the applicant GST record.
    */
    public static HDFC_DASH_GST_Business_Details__c mapEmpGSTDetails(Id sfEmpDetId, HDFC_DASH_ParseApplication.empGstBusinessDetails parseEmp, HDFC_DASH_GST_Business_Details__c gstEmpDetails){

        gstEmpDetails.HDFC_DASH_Applicant_Employment_Details__c =  sfEmpDetId;
        gstEmpDetails.HDFC_DASH_GST_Number__c = parseEmp?.gstNumber;
        gstEmpDetails.HDFC_DASH_State_Code__c = parseEmp?.stateCode; 
        gstEmpDetails.HDFC_DASH_State__c = parseEmp?.state;
        gstEmpDetails.HDFC_DASH_ILPS_EmpGst_Business_Detail_Id__c = parseEmp?.ILPSEmpGstBusinessDetId;
        return gstEmpDetails;

    }
}