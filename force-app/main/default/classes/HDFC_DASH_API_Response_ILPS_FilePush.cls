public with sharing class HDFC_DASH_API_Response_ILPS_FilePush {
    
    public List<SerialDetails> serialDetails;
    public List<ApplicantDetails> applicantDetails;

    public class SerialDetails {
        public String sfapplicationid;
        public Integer fileno;
        public String filecompletenessflag;
    }

    public class ApplicantDetails{
        public String sfcustno;
        public String customername;
        public String capacity;
        public Integer ilpscustnumber;
    }

    /*
    Method: HDFC_DASH_API_Response_ILPS_FilePush
    Description: Method for Response Parsing
    */	
		public static HDFC_DASH_API_Response_ILPS_FilePush parse(HttpResponse response) {
            string json = response.getBody();
            return (HDFC_DASH_API_Response_ILPS_FilePush) System.JSON.deserialize(json, HDFC_DASH_API_Response_ILPS_FilePush.class);
        }
}