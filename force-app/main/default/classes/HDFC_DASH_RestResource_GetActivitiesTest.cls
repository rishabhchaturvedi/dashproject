/**
className: HDFC_DASH_RestResource_GetActivitiesTest
DevelopedBy: Punam Marbate
Date: 17 Sep 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_GetActivities
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_RestResource_GetActivitiesTest {
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR); 
    private static Id sysAdminId = UserInfo.getUserId();
    private static final string BASEURL = URL.getOrgDomainUrl().toExternalForm();
    private static final string STRING_URL = BASEURL + '/hdfc/getactivities/V1.0';
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Contact conWithoutApp = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static Event event= HDFC_DASH_TestDataFactory_Event.getBasicEvent(app.id,con.id,sysAdminId);
	private static Lead leadRec = HDFC_DASH_TestDataFactory_Leads.getBasicLead();
    private static HDFC_DASH_SO_BSA_Association__c SO_BSA_Association = HDFC_DASH_TestDataFactory_SO_BSA.getBasicSoBsa(acc.id);
        
    private static final string ACTIVITY ='Activity';
    private static final string HUDDLE = 'Huddle';
    private static final string OPEN ='Open';
    private static final string STARTDATETIME ='2021-09-15T11:00:00.000Z';
    private static final string ENDDATETIME ='2021-09-15T12:00:00.000Z';
   
     /* 
Method Name : testGetActivitiesTypeHuddle()
Parameters  : No
Description :This method is for Huddle scenario which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/
    static testmethod void testGetActivitiesTypeHuddle()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,HUDDLE);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_TL,sysAdminId);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
     /* 
Method Name : testGetActivitiesTypeHuddleWithSO()
Parameters  : No
Description :This method is for Huddle scenario which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/
  static testmethod void testGetActivitiesTypeHuddleWithSO()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,HUDDLE);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_SO,sysAdminId);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    /* 
Method Name : testGetActivitiesTypeTL()
Parameters  : No
Description :This method is for success scenario 1 which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/
    static testmethod void testGetActivitiesTypeTL()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_TL,sysAdminId);
            request.addParameter(HDFC_DASH_Constants.STRING_SHOWALL,HDFC_DASH_Constants.TRUESTR);
            request.addParameter(HDFC_DASH_Constants.STRING_FILENUMBER, app.HDFC_DASH_File_Number__c);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
 
    /* 
Method Name : testGetActivitiesTypeSO()
Parameters  : No
Description :This method is for success scenario 2 which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/  
    static testmethod void testGetActivitiesTypeSO()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_FILENUMBER, app.HDFC_DASH_File_Number__c);
            request.addParameter(HDFC_DASH_Constants.STRING_SHOWALL,HDFC_DASH_Constants.TRUESTR);
            request.addParameter(HDFC_DASH_Constants.STRING_SO,sysAdminId);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* 
Method Name : testGetActivitiesTypeBSA()
Parameters  : No
Description :This method is for success scenario 3 which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/ 
    static testmethod void testGetActivitiesTypeBSA()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_FILENUMBER, app.HDFC_DASH_File_Number__c);
            request.addParameter(HDFC_DASH_Constants.STRING_BSAOWNER,con.HDFC_DASH_Executive_Code__c);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* 
Method Name : testGetActivitiesTypeFileNumber()
Parameters  : No
Description :This method is for success scenario 4 which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/  
    static testmethod void testGetActivitiesTypeFileNumber()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_FILENUMBER, app.HDFC_DASH_File_Number__c);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* 
Method Name : testGetActivitiesTypeLeadId()
Parameters  : No
Description :This method is for success scenario 5 which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/
    static testmethod void testGetActivitiesTypeLeadId()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_LEADID,leadRec.HDFC_DASH_LMS_Lead_ID__c);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* 
Method Name : testGetActivitiesTypeTLBSA()
Parameters  : No
Description :This method is for success scenario 6 which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/
    static testmethod void testGetActivitiesTypeTLBSA()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_TL,sysAdminId);
            request.addParameter(HDFC_DASH_Constants.STRING_SHOWALL,HDFC_DASH_Constants.TRUESTR);
            request.addParameter(HDFC_DASH_Constants.STRING_BSA,acc.HDFC_DASH_Source_Rec_SrNo__c);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* 
Method Name : testGetActivitiesTypeSOBSA()
Parameters  : No
Description :This method is for success scenario 7 which calls HDFC_DASH_RestResource_GetActivities.getActivities().
*/
    static testmethod void testGetActivitiesTypeSOBSA()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            
            request.addParameter(HDFC_DASH_Constants.STRING_SO,sysAdminId);
            request.addParameter(HDFC_DASH_Constants.STRING_BSA,acc.HDFC_DASH_Source_Rec_SrNo__c);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    
    /* Method Name: testException
parameters: NONE
Method Description: This method is to test Exception scenario.
*/
    static TestMethod void testException() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        Exception testException;
        
        //Request Header is not added to check whether the exception is throw       
        RestRequest request = new RestRequest();
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;   
        RestContext.request = request;
        
        test.startTest();
        System.runAs(sysAdmin) 
        {
            try
            {
                HDFC_DASH_RestResource_GetActivities.getActivities();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    } 
    
    /* 
Method Name : testGetActivitiesTypeTLWithILPSFileNumber()
Parameters  : No
Description :This method is for success scenario 1 which call HDFC_DASH_RestResource_GetActivities.getActivities().

    static testmethod void testGetActivitiesTypeTLWithILPSFileNumber()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_TYPE,ACTIVITY);
            request.addParameter(HDFC_DASH_Constants.STRING_EVENTSTATUS,OPEN);
            request.addParameter(HDFC_DASH_Constants.STRING_STARTDATETIME,STARTDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_ENDDATETIME,ENDDATETIME);
            request.addParameter(HDFC_DASH_Constants.STRING_TL,sysAdminId);
            request.addParameter(HDFC_DASH_Constants.STRING_FILENUMBER, ilpsApp.HDFC_DASH_File_Number__c);
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetActivities.getActivities();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }*/
    
}