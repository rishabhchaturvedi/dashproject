/* 
ClassName: HDFC_DASH_TestDataFactory
DevelopedBy: Sushant
Date: 26 May 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
*/
public with sharing class HDFC_DASH_TestDataFactory {
    public static final string EMAIL = 'testuserintestclass@testorg.com';
    public static final string NEW_USER = 'newUser';
    public static final string UTF = 'UTF-8';
    public static final string LAST_NAME = 'Testing';
    public static final string LOCALE ='en_US';
    public static final string TIME_ZONE ='America/Los_Angeles';
    public static final string USER_NAME ='testuserintestclass@testorg.com';
    public static final string EXCEPTION_OPERATION = 'Testing Log Exception';
    public static final string EXCEPTION_DETAILS = 'This is a Test Exception';
    private static final string SO_ID = '1357989';
    private static final string ROLE_NAME= 'Sales Officer';
    private static final string ROLE_NAME_CentralTeam = 'CENTRAL TEAM';
    
    /* 
MethodName: createBasicPlatformEventlist
MethodDescription: creates a list of Platform events.
*/
    public static list<HDFC_DASH_Exception_Logs__e> createBasicPlatformEventlist(Integer numberofplatformevents){
        list<HDFC_DASH_Exception_Logs__e> pelist = new list<HDFC_DASH_Exception_Logs__e>();
        for(integer i=0;i<numberofplatformevents; i++){
            HDFC_DASH_Exception_Logs__e pEvent = new HDFC_DASH_Exception_Logs__e();
            pEvent.HDFC_DASH_Record_ID__c= HDFC_DASH_Constants.ACCOUNT_OBJECT + i;
            pEvent.HDFC_DASH_Object__c= HDFC_DASH_Constants.ACCOUNT_OBJECT;
            pEvent.HDFC_DASH_Operation__c= EXCEPTION_OPERATION;
            pEvent.HDFC_DASH_Exception_Details__c= EXCEPTION_DETAILS;
            pelist.add(pEvent);
        }
        return pelist;
    }
    
        /* 
MethodName: createCaseUpdatePlatformEvent
MethodDescription: creates a list of Platform events.
*/
    public static list<Case_Update__e> createCaseUpdatePlatformEvent(Integer numberofplatformevents){
        list<Case_Update__e> pelist = new list<Case_Update__e>();
        for(integer i=0;i<numberofplatformevents; i++){
            Case_Update__e pEvent = new Case_Update__e();
            pEvent.Existing_CaseId__c= HDFC_DASH_Constants.ACCOUNT_OBJECT + i;
            pEvent.Mode__c= HDFC_DASH_Constants.ACCOUNT_OBJECT;
            pEvent.Sub_Mode__c= EXCEPTION_OPERATION;
            pEvent.Reason__c= EXCEPTION_DETAILS;
            pelist.add(pEvent);
        }
        return pelist;
    }
    
    /* 
MethodName: createUser
MethodDescription: creates a user for running the test class .
*/
    public static User createUser(String profilename){
        Profile profile = new Profile();
        UserRole role = new UserRole();
        if(Schema.sObjectType.Profile.IsAccessible()){
            profile = [SELECT Id FROM Profile WHERE Name=:profilename];
        }
        if(Schema.sObjectType.UserRole.IsAccessible()){
            role = [SELECT Id FROM UserRole WHERE Name=:ROLE_NAME];
        }
        User user = new User(Alias = LAST_NAME, Email= EMAIL,
                             EmailEncodingKey= UTF,LastName= LAST_NAME, LanguageLocaleKey= LOCALE,
                             LocaleSidKey= LOCALE, ProfileId = profile.Id,
                             TimeZoneSidKey= TIME_ZONE, UserName= USER_NAME, FederationIdentifier = SO_ID);
        return user;
    }
    /* 
MethodName: createUserCentralTeam
MethodDescription: creates a user for running the test class with Role as Central Team.
*/
    public static User createUserCentralTeam(String profilename){
        Profile profile;
        UserRole role;
        if(Schema.sObjectType.Profile.IsAccessible()){
            profile = [SELECT Id FROM Profile WHERE Name=:profilename];
        }
        if(Schema.sObjectType.UserRole.IsAccessible()){
            role = [SELECT Id FROM UserRole WHERE Name=:ROLE_NAME_CentralTeam];
        }
        User user = new User(Alias = LAST_NAME, Email= EMAIL,
                             EmailEncodingKey= UTF,LastName= LAST_NAME, LanguageLocaleKey= LOCALE,
                             LocaleSidKey= LOCALE, ProfileId = profile.Id,
                             TimeZoneSidKey= TIME_ZONE, UserName= USER_NAME, FederationIdentifier = SO_ID);
        return user;
    }
}