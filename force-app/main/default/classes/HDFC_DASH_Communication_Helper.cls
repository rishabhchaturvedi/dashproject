/**
* 	className: HDFC_DASH_Communication_Helper
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This is a helper class which would get all the Applications for which Email,SMS,WhatsApp communications need to be sent.
**/
public  with sharing class HDFC_DASH_Communication_Helper {
    /*
Method Name :sendCommunication
Description :This method would send the respective email,SMS,WhatsApp for the filtered applications whose status is not chages above 24 hours.
*/    
    public  static void sendCommunication(Map<String, HDFC_DASH_Communication__mdt> communicationMetadata,Map<String,EmailTemplate> emailTemplateMap,Map<String,Id> orgWideEmailAddressMap,Map<String,List<Opportunity>> oppListForEmailcomm){
        List<Id> applicationIds = new  List<Id>();
        Map<Id,Opportunity> mapAppRec = new  Map<Id,Opportunity>();
        List<Opportunity> oppListToUpdate = new List<Opportunity>();
        for(String appStatus:oppListForEmailcomm.keySet()){
            List<Opportunity> appRecs = oppListForEmailcomm.get(appStatus);
            for(Opportunity appRec:appRecs){
                applicationIds.add(appRec.id);
                mapAppRec.put(appRec.id,appRec);
                apprec.HDFC_DASH_Latest_Status_Change_Date_Time__c = System.now();
                oppListToUpdate.add(apprec);
            }
        }
        
        //get all the applicant details based on application id
        List<string> appDetFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Contact,HDFC_DASH_Constants.HDFC_DASH_Application,
            HDFC_DASH_Constants.HDFC_DASH_SMS_Consent,HDFC_DASH_Constants.HDFC_DASH_WhatsApp_Consent,HDFC_DASH_Constants.CONTACT_SMS_CONSENT,HDFC_DASH_Constants.CONTACT_WA_CONSENT,
            HDFC_DASH_Constants.CONTACT_MOBILEPHONE, HDFC_DASH_Constants.CONTACT_MOBILEPHONE_COUNTRYCODE, HDFC_DASH_Constants.APPDETAIL_CONTACT_R_EMAIL};
                
        Map<Id,Opportunity> mapAppDetIdAppRec = new  Map<Id,Opportunity>();
        Map<Id,Id> mapAppDetIdAppId= new  Map<Id,Id>();
        Map<Id,HDFC_DASH_Applicant_Details__c> mapAppDetRec = new  Map<Id,HDFC_DASH_Applicant_Details__c>();
        
        for(HDFC_DASH_Applicant_Details__c appDetRec:HDFC_DASH_Applicant_Detail_Selector.getApplicationDetailsByRecordType(
            applicationIds,appDetFields,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.RECORDTYPE_PRIMARYAPP)){
                mapAppDetIdAppRec.put(appDetRec.id,mapAppRec.get(appDetRec.HDFC_DASH_Application__c));
                //system.debug('appDetRec==='+appDetRec);
                mapAppDetRec.put(appDetRec.id,appDetRec);
            } 
        //system.debug('mapAppDetRec==='+mapAppDetRec);
        //system.debug('mapAppDetIdAppRec==='+mapAppDetIdAppRec);
        Map<String,List<HDFC_DASH_Applicant_Details__c>> mapAppDetRecSatus = new Map<String,List<HDFC_DASH_Applicant_Details__c>>();
        List<Id> oppRecIdsOfStatus250 = new List<id>();
        
        for(String appStatus:oppListForEmailcomm.keySet()){
            if(String.isNotBlank(appStatus)){
                List<Opportunity> appRecs = oppListForEmailcomm.get(appStatus);
                if(appStatus.equals(HDFC_DASH_Constants.STRING_250)){
                    for(Opportunity appRec: appRecs){
                        oppRecIdsOfStatus250.add(appRec.Id);
                    }
                }
                for(Id appDetId:mapAppDetIdAppRec.KeySet()){
                    if(appRecs.contains(mapAppDetIdAppRec.get(appDetId))){
                        if(mapAppDetRecSatus.containsKey(appStatus) && mapAppDetRecSatus.get(appStatus) != NULL){
                            List<HDFC_DASH_Applicant_Details__c> appDetRecList = mapAppDetRecSatus.get(appStatus);
                            appDetRecList.add(mapAppDetRec.get(appDetId));
                            mapAppDetRecSatus.put(appStatus,appDetRecList); 
                        }   
                        else {
                            mapAppDetRecSatus.put(appStatus,new List<HDFC_DASH_Applicant_Details__c> {mapAppDetRec.get(appDetId)});
                        } 
                    }
                }
            }
            
        }
        //system.debug('mapAppDetRecSatus===='+mapAppDetRecSatus);
        
        List<string> payDetFieldList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.HDFC_DASH_Fee_Detail,HDFC_DASH_Constants.HDFC_DASH_Fee_Detail_FeeConstruct,HDFC_DASH_Constants.HDFC_DASH_TxnDate,HDFC_DASH_Constants.HDFC_DASH_AuthStatus};
            Map<String,HDFC_DASH_Application_Payment_Details__c> appToPaymentDetail = new Map<String,HDFC_DASH_Application_Payment_Details__c>();
        
        for(HDFC_DASH_Application_Payment_Details__c applPaymentDetail: HDFC_DASH_Applicant_PayDetail_Selector.getApplicationPaymentDetail(oppRecIdsOfStatus250,payDetFieldList,HDFC_DASH_Constants.HDFC_DASH_Application)){
            if(applPaymentDetail.HDFC_DASH_Fee_Detail__r.HDFC_DASH_Fee_Construct_feeType__c != NULL && 
               applPaymentDetail.HDFC_DASH_Fee_Detail__r.HDFC_DASH_Fee_Construct_feeType__c == HDFC_DASH_Constants.HDFC_DASH_FEE_TYPE_PF){
                   //System.debug('PPP In populating appToPaymentDetail' + applPaymentDetail);
                   appToPaymentDetail.put(applPaymentDetail.HDFC_DASH_Application__c,applPaymentDetail);
               }
        }
        
        //Loop Over to create email records
        List<Messaging.SingleEmailMessage> singleEmailMsgList = new List<Messaging.SingleEmailMessage>();
        List<Task> tasksToInsert = new List<Task>();
        for(String appStatus:mapAppDetRecSatus.keySet()){
            String emailcommMatadataKey = HDFC_DASH_Constants.Email_UNDERSCORE+appStatus;
            String smsCommMatadataKey = HDFC_DASH_Constants.SMS_UNDERSCORE+appStatus;
            String whatsAppCommMatadataKey = HDFC_DASH_Constants.WA_UNDERSCORE+appStatus;
            //System.debug('ttt ' + emailcommMatadataKey);
            //if(appStatus != 'Email_200'){
            HDFC_DASH_Communication__mdt commMetaDataRec = communicationMetadata.get(emailcommMatadataKey);
            if(commMetaDataRec != NULL){
                //system.debug('commMetaDataRec==='+commMetaDataRec);
                List<HDFC_DASH_Applicant_Details__c> appDetRecs = mapAppDetRecSatus.get(appStatus);
                for(HDFC_DASH_Applicant_Details__c appDetRec :appDetRecs){
                    //System.debug('ppp emai test from send email ' + appDetRec.HDFC_DASH_Application__c );
                    //if(String.isNotBlank(appDetRec.HDFC_DASH_Contact__r.Email)){
                        //system.debug('EMailID'+appDetRec.HDFC_DASH_Contact__r.Email);
                        Messaging.SingleEmailMessage SingleEmailMessageobj = new Messaging.SingleEmailMessage();
                        SingleEmailMessageobj.setTargetObjectId(appDetRec.HDFC_DASH_Contact__c); //contact iD
                        //system.debug('emailTemplateMap.get(commMetaDataRec.HDFC_DASH_TemplateID__c)'+emailTemplateMap.get(commMetaDataRec.HDFC_DASH_TemplateID__c));
                        SingleEmailMessageobj.setTemplateId(emailTemplateMap.get(commMetaDataRec.HDFC_DASH_TemplateID__c).Id); // ID of the template
                        SingleEmailMessageobj.setOrgWideEmailAddressId(orgWideEmailAddressMap.get(commMetaDataRec.HDFC_DASH_FromEmailAddress__c)); // Set from address 
                        SingleEmailMessageobj.setSaveAsActivity(HDFC_DASH_Constants.BOOLEAN_TRUE);
                        
                        if(String.isNotBlank(appDetRec.HDFC_DASH_Contact__r.Email) && appStatus == HDFC_DASH_Constants.STRING_250 && appToPaymentDetail.get(appDetRec.HDFC_DASH_Application__c) !=NULL){
                            //System.debug('ppp payment' + appDetRec.HDFC_DASH_Application__c);
                           // System.debug('ppp payment map ' + appToPaymentDetail);
                            SingleEmailMessageobj.setWhatId(appToPaymentDetail.get(appDetRec.HDFC_DASH_Application__c).Id);
                            singleEmailMsgList.add(SingleEmailMessageobj);
                        }
                        else if(String.isNotBlank(appDetRec.HDFC_DASH_Contact__r.Email) && appStatus != HDFC_DASH_Constants.STRING_250){
                            SingleEmailMessageobj.setWhatId(appDetRec.HDFC_DASH_Application__c); //ID of the record to use the merge field
                            singleEmailMsgList.add(SingleEmailMessageobj); 
                        }
                        
                        if(String.isNotBlank(appDetRec.HDFC_DASH_Contact__r.Email) && appDetRec.HDFC_DASH_Contact__r.HDFC_DASH_SMS_Consent__c == HDFC_DASH_Constants.STRING_Y){
                            Task smsTask = sendSmsOrWhatsapp(emailTemplateMap,commMetaDataRec,appDetRec,smsCommMatadataKey,HDFC_DASH_Constants.STRING_SMS, appToPaymentDetail);
                            tasksToInsert.add(smsTask);
                        }
                        //system.debug('whatsAppCommMatadataKey'+whatsAppCommMatadataKey);
                        if(String.isNotBlank(appDetRec.HDFC_DASH_Contact__r.Email) && appDetRec.HDFC_DASH_Contact__r.HDFC_DASH_WhatsApp_Consent__c == HDFC_DASH_Constants.STRING_Y && !whatsAppCommMatadataKey.equals(HDFC_DASH_Constants.WA_UNDERSCORE+ HDFC_DASH_Constants.STRING_50)){
                            Task watSappTask = sendSmsOrWhatsapp(emailTemplateMap,commMetaDataRec,appDetRec,whatsAppCommMatadataKey,HDFC_DASH_Constants.STRING_WA, appToPaymentDetail);
                            tasksToInsert.add(watSappTask);
                        }
                        
                    //}
                }
            }
        }
        if(oppListToUpdate.size()> HDFC_DASH_Constants.INT_ZERO){
            Database.update(oppListToUpdate); //To update Latest Date Change Time
        }
        Database.insert(tasksToInsert);
        callSMSWAEmailCommunications(tasksToInsert,singleEmailMsgList);
        
    }
    
    /*
Method Name :sendCommunicationForPreApproved
Description :This method would send the respective email,sms,whatsApp for the applications where preapproved loan flag is true.
*/    
    public static void sendCommunicationForPreApproved(Map<String, HDFC_DASH_Communication__mdt> communicationMetadata,List<Id> appRecIds,Map<String,EmailTemplate> emailTemplateMap,Map<String,Id> orgWideEmailAddressMap){
        //system.debug('inside sendCommunicationForPreApproved');
        if(appRecIds !=null && appRecIds.size() > HDFC_DASH_Constants.INT_ZERO){
            
            List<Messaging.SingleEmailMessage> singleEmailMsgList = new List<Messaging.SingleEmailMessage>();
            //List<string> appDetFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Contact,HDFC_DASH_Constants.HDFC_DASH_Application};
            List<string> appDetFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Contact,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.HDFC_DASH_SMS_Consent,HDFC_DASH_Constants.HDFC_DASH_WhatsApp_Consent,
                HDFC_DASH_Constants.CONTACT_SMS_CONSENT,HDFC_DASH_Constants.CONTACT_WA_CONSENT,HDFC_DASH_Constants.CONTACT_MOBILEPHONE, HDFC_DASH_Constants.CONTACT_MOBILEPHONE_COUNTRYCODE, HDFC_DASH_Constants.APPDETAIL_CONTACT_R_EMAIL};
                    List<Task> tasksToInsert = new List<Task>();
            HDFC_DASH_Communication__mdt commMetaDataRec = communicationMetadata.get(HDFC_DASH_Constants.HDFC_DASH_EMAIL_PreApprovedLoan);
            String smsCommMatadataKey = HDFC_DASH_Constants.SMS_PreApproved_Loan_Offer;
            String whatsAppCommMatadataKey = HDFC_DASH_Constants.WA_PreApproved_Loan_Offer;
            
            List<Opportunity> oppRecList = new List<Opportunity>();
            //System.debug('email test orgwide ' + orgWideEmailAddressMap.get(HDFC_DASH_Constants.CUSTOMERSERVICEMAIL));
            for(HDFC_DASH_Applicant_Details__c appDetRec:HDFC_DASH_Applicant_Detail_Selector.getApplicationDetailsByRecordType(
                appRecIds,appDetFields,HDFC_DASH_Constants.HDFC_DASH_Application,HDFC_DASH_Constants.RECORDTYPE_PRIMARYAPP)){
                    
                    Opportunity oppRec = new Opportunity();
                    oppRec.id = appDetRec.HDFC_DASH_Application__c;
                    oppRec.HDFC_DASH_Latest_Status_Change_Date_Time__c = System.now();
                    oppRecList.add(oppRec);
                    
                    //System.debug('Email Test ' + appDetRec.HDFC_DASH_Contact__c);
                    Messaging.SingleEmailMessage SingleEmailMessageobj = new Messaging.SingleEmailMessage();
                    SingleEmailMessageobj.setTargetObjectId(appDetRec.HDFC_DASH_Contact__c); //contact iD
                    SingleEmailMessageobj.setTemplateId(emailTemplateMap.get(commMetaDataRec.HDFC_DASH_TemplateID__c).Id); // ID of the template
                    SingleEmailMessageobj.setWhatId(appDetRec.HDFC_DASH_Application__c); //ID of the record to use the merge field 
                    SingleEmailMessageobj.setSaveAsActivity(HDFC_DASH_Constants.BOOLEAN_TRUE);
                    SingleEmailMessageobj.setOrgWideEmailAddressId(orgWideEmailAddressMap.get(HDFC_DASH_Constants.CUSTOMERSERVICEMAIL)); // Set from address 
                    singleEmailMsgList.add(SingleEmailMessageobj);
                    
                    if(appDetRec.HDFC_DASH_Contact__r.HDFC_DASH_SMS_Consent__c == HDFC_DASH_Constants.STRING_Y){
                        Task smsTask = sendSmsOrWhatsapp(emailTemplateMap,commMetaDataRec,appDetRec,smsCommMatadataKey,HDFC_DASH_Constants.STRING_SMS, NULL);
                        tasksToInsert.add(smsTask);
                    }
                   /* if(appDetRec.HDFC_DASH_Contact__r.HDFC_DASH_WhatsApp_Consent__c == HDFC_DASH_Constants.STRING_Y){
                        Task watSappTask = sendSmsOrWhatsapp(emailTemplateMap,commMetaDataRec,appDetRec,whatsAppCommMatadataKey,HDFC_DASH_Constants.STRING_WA, NULL);
                        tasksToInsert.add(watSappTask);
                    } */
                }
            
            if(oppRecList.size()> HDFC_DASH_Constants.INT_ZERO){
                //system.debug('oppRecList==='+oppRecList);
                Database.update(oppRecList); //To update Latest Date Change Time
            }
            Database.insert(tasksToInsert);
            //system.debug('singleEmailMsgList===='+singleEmailMsgList);
            //Messaging.sendEmail(singleEmailMsgList);
            callSMSWAEmailCommunications(tasksToInsert,singleEmailMsgList);
        }
    }
    
    /*
Method Name :sendEmail
Description :This method would send the respective email for the filtered applications whose status is not chages above 24 hours.
*/    
    public  static Task sendSmsOrWhatsapp(Map<String,EmailTemplate> emailTemplateMap, HDFC_DASH_Communication__mdt commMetaDataRec,HDFC_DASH_Applicant_Details__c appDetRec,String appStatus,String commType, Map<String,HDFC_DASH_Application_Payment_Details__c> appToPaymentDetail){
        //system.debug('inside sendSmsOrWhatsapp===');
        Task task = new Task();
        //system.debug('commMetaDataRec.HDFC_DASH_TemplateID__c'+commMetaDataRec);
        task.Subject = emailTemplateMap.get(commMetaDataRec.HDFC_DASH_TemplateID__c).Subject;
        task.WhatId = appDetRec.HDFC_DASH_Application__c; //Application
        task.WhoId = appDetRec.HDFC_DASH_Contact__c; //Customer
        task.HDFC_DASH_Status__c = HDFC_DASH_Constants.STATUS_OPEN;
        
        task.HDFC_DASH_Communication_ID__c = appStatus;
        
        //System.debug('appStatus ' + appStatus);
        //System.debug('payment detail ' + appToPaymentDetail);
        if(!appStatus.contains(HDFC_DASH_Constants.STRING_250)|| (appStatus.equals(HDFC_DASH_Constants.SMS_PreApproved_Loan_Offer) ||
                                                                  appStatus.equals(HDFC_DASH_Constants.WA_PreApproved_Loan_Offer))){ // Application/Applicant detail
                                                                      task.HDFC_DASH_Related_Record__c = appDetRec.id;
                                                                      //System.debug(' task.HDFC_DASH_Related_Record__c===='+ task.HDFC_DASH_Related_Record__c);
                                                                  } else if(appStatus.contains(HDFC_DASH_Constants.STRING_250) && appToPaymentDetail.get(appDetRec.HDFC_DASH_Application__c) != NULL){
                                                                      task.HDFC_DASH_Related_Record__c = appToPaymentDetail.get(appDetRec.HDFC_DASH_Application__c).Id;
                                                                  }
        
        task.HDFC_DASH_Is_Outbound_Communication__c = true;
        //System.debug('ccc ' + appDetRec.Id+ 'contact : '+ appDetRec.HDFC_DASH_Contact__c +'  phone num: ' + appDetRec.HDFC_DASH_Contact__r.HDFC_DASH_Mobile_Country_Code__c + appDetRec.HDFC_DASH_Contact__r.MobilePhone);
        task.HDFC_DASH_Target_Number__c = appDetRec.HDFC_DASH_Contact__r.HDFC_DASH_Mobile_Country_Code__c + appDetRec.HDFC_DASH_Contact__r.MobilePhone; 
        task.HDFC_DASH_Communication_Type__c = commType;
        return task;
        
    }
    
    /*
Method Name :sendEmail
Description :This method would send the respective email for the filtered applications whose status is not chages above 24 hours.
*/    
    public  static void callSMSWAEmailCommunications(List<Task> tasksToInsert,List<Messaging.SingleEmailMessage> singleEmailMsgList){
        string waMessageBody;
        string smsMessageBody;
        Task smsTask;
        Task waTask;
        for(Task task :tasksToInsert){
            
            HDFC_DASH_CommunicationWrapper commWrapper = new HDFC_DASH_CommunicationWrapper(task);
            
            if(task.HDFC_DASH_Communication_Type__c == HDFC_DASH_Constants.STRING_SMS){
                //system.debug('task.HDFC_DASH_Communication_ID__c===='+task.HDFC_DASH_Communication_ID__c);
                
                smsMessageBody = commWrapper.callGenerateContent(task);
                //system.debug('sms message body==='+smsMessageBody);
                smsTask = task;
            }else if(task.HDFC_DASH_Communication_Type__c == HDFC_DASH_Constants.STRING_WA){
                waMessageBody = commWrapper.callGenerateContent(task);
                //system.debug('whatsapp message body==='+waMessageBody);
                waTask = task;
            }
            
        }
        String jobId;
        if(jobId == null && ((smsTask !=NULL && smsMessageBody !=NULL) || (waTask != NULL && waMessageBody!= NULL))){
            jobId = System.enqueueJob(new HDFC_DASH_SMSIntegration(smsTask, smsMessageBody,waTask,waMessageBody));
        }
        else{
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_GSTNOTAPPLICABLE);
        }
        Messaging.sendEmail(singleEmailMsgList); 
        
        
    }
}