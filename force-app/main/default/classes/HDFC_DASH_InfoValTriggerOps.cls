/**
className: HDFC_DASH_InfoValTriggerOps
DevelopedBy: Janani Mohankumar
Date: 8 November 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on Info Validation object.
**/
public with sharing class HDFC_DASH_InfoValTriggerOps {
    /**
className: InfoVal_LatestFlag_BeforeTrigger
DevelopedBy: Tejeswari
Date: 10 November 2021
Company: Accenture
Class Description: This Class contains the trigger operation for prosessing latest flag beforeinsert
*/ 
    public with sharing class InfoVal_LatestFlag_BeforeTrigger implements HDFC_DASH_TriggerOps
    {
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() 
        {
            system.debug('***Info Val Before Enable***');
            return HDFC_DASH_TriggerOpsRecursionFlags.infoValRecordsFirstRun;
        }
        
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter()
        {
            system.debug('***Info Val Before filter***');
            return Trigger.new;
        }
        
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listInfoValRecords)
        {
            system.debug('***Info Val Before Execute***');
            HDFC_DASH_TriggerOpsRecursionFlags.infoValRecordsFirstRun = false;
            HDFC_DASH_InfoValTriggerOpsHelper.changeInfoValBeforeInsert(listInfoValRecords);
        } 
    }
    
    
    /**
className: InfoVal_LatestFlag_AfterTrigger
DevelopedBy: Tejeswari
Date: 10 November 2021
Company: Accenture
Class Description: This Class contains the trigger operation for prosessing latest flag after insert
*/
    public with sharing class InfoVal_LatestFlag_AfterTrigger implements HDFC_DASH_TriggerOps{
        /*
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() 
        {
            system.debug('***Info Val After Enable***');
            return HDFC_DASH_TriggerOpsRecursionFlags.infoValRecAfterIns;
        }    
        
        /*
Method Name :filter
Description :This method would filter for the records.
*/ 
        public SObject[] filter()
        {
            system.debug('***Info Val After Filter***');
            List<HDFC_DASH_Info_Validation__c> listInfoValRecs =
                HDFC_DASH_InfoValTriggerOpsHelper.filterInfoValforLatestFlagSenario();
            return listInfoValRecs;
        } 
        
        /*
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Sobject[] listInfoValRecs)
        {
            system.debug('***Info Val After Execute***');
            HDFC_DASH_TriggerOpsRecursionFlags.infoValRecAfterIns = false;
            HDFC_DASH_InfoValTriggerOpsHelper.updateInfoValRec(listInfoValRecs);
        }  
        
    }
    /**
className: ContentDocDeletion
DevelopedBy: Tejeswari
Date: 29 November 2021
Company: Accenture
Class Description: This Class delete the Content Document records.
*/ 
    public with sharing class ContentDocDelAndDDCallOut implements HDFC_DASH_TriggerOps
    {
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() 
        {
            return HDFC_DASH_TriggerOpsRecursionFlags.infoVal_DeleteConDoc_FirstRun;
        }
        
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter()
        {
          List<HDFC_DASH_Info_Validation__c> listInfoValRecs =
                HDFC_DASH_InfoValTriggerOpsHelper.filterInfoValforConDocDelAndDDCallOut();
            return listInfoValRecs;
        }
        
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listInfoValRecords)
        {
       
            HDFC_DASH_InfoValTriggerOpsHelper.delOfconDocument(listInfoValRecords);
            HDFC_DASH_InfoValTriggerOpsHelper.docDetailCallout(listInfoValRecords);
            HDFC_DASH_TriggerOpsRecursionFlags.infoVal_DeleteConDoc_FirstRun = false;
            
        } 
    }

}