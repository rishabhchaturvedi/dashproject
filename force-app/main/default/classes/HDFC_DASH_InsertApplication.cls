/**
className: HDFC_DASH_InsertApplication
DevelopedBy: Raghavendra Koora
Date: 4 NOV 2021
Company: Accenture
Class Description: This calss gets invoked when there is no application id passed in the request
**/
public with sharing class HDFC_DASH_InsertApplication 
{

    public static String sfCustomerId = '';
    /*
Method Name :getLead
Parameters  :HDFC_DASH_ParseApplication parseApp
Description :This method will check for existing lead and invokes callLMS.
*/
    public static Lead getLead(HDFC_DASH_ParseApplication parseApp)
    {
        //String sfCustomerId = null;
        //String requestLMSId = parseApp?.application?.applicationDetails?.lmsLeadId;
        Lead lead = new Lead();
        List<Lead> leadRecords = new List<Lead>();
        HDFC_DASH_APIResponse_LMS lmsApiResponse = new HDFC_DASH_APIResponse_LMS();
        HDFC_DASH_APIRequest_LMS lmsApiRequest = new HDFC_DASH_APIRequest_LMS();
        String lmsLeadId;
        
        List<String> leadsFieldList = new List<String>{HDFC_DASH_Constants.ID_STRING , HDFC_DASH_Constants.STRING_Name, HDFC_DASH_Constants.STRING_FirstName,
            HDFC_DASH_Constants.STRING_LastName , HDFC_DASH_Constants.STRING_CONTACT , HDFC_DASH_Constants.STRING_STATUS, HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch,
            HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id, HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_Status, HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID,HDFC_DASH_Constants.HDFC_DASH_Owner_Id,
            HDFC_DASH_Constants.HDFC_DASH_Source_Id,HDFC_DASH_Constants.HDFC_DASH_Source,HDFC_DASH_Constants.OwnerId,
            HDFC_DASH_Constants.HDFC_DASH_Leap_LR_Number,HDFC_DASH_Constants.HDFC_DASH_Leap_LR_Date,
            HDFC_DASH_Constants.HDFC_DASH_Source_Reference,HDFC_DASH_Constants.HDFC_DASH_LMSLeadSource,HDFC_DASH_Constants.HDFC_DASH_LMSLeadSubSource,
            HDFC_DASH_Constants.HDFC_DASH_LMSLeadTermSource,HDFC_DASH_Constants.HDFC_DASH_LMSLeadDistribution,HDFC_DASH_Constants.HDFC_DASH_LMSLeadOwner,
            HDFC_DASH_Constants.HDFC_DASH_Source_Executive,HDFC_DASH_Constants.HDFC_DASH_Source_Executive_Id,
            HDFC_DASH_Constants.HDFC_DASH_Campaign_Code,HDFC_DASH_Constants.HDFC_DASH_Subvention_Amount,
            HDFC_DASH_Constants.HDFC_DASH_Subvention_Flag,HDFC_DASH_Constants.HDFC_DASH_Reach_Flag
            };
                
        if(String.isNotBlank(parseApp?.application?.applicationDetails?.lmsLeadId)){
            lmsLeadId = parseApp?.application?.applicationDetails?.lmsLeadId;
        }
        else{
            lmsLeadId = parseApp?.application?.lmsDetails?.lmsLeadId;
        }
        
        
        for(HDFC_DASH_ParseApplication.ApplicantInfo appDetailRec: parseApp?.application?.applicantInfo)
        {
            if(appDetailRec?.personalApplicantInfo?.applicantCapacity == HDFC_DASH_Constants.STRING_B ){
                if(appDetailRec?.applicantdetail?.sfCustomerId!=NULL){
                sfCustomerId = appDetailRec?.applicantdetail?.sfCustomerId;
                }
                else
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_SFCUSTOMERID_DOESNOTEXIST);
            }
        }
        
        String conditionOn = 'HDFC_DASH_LMS_Lead_ID__c';
    
        if(!String.isBlank(lmsLeadId)){

            List<String> queryParam = new List<String>{lmsLeadId};
            leadRecords = HDFC_DASH_Lead_Selector.getLeadsBasedOnCondition(queryParam,leadsFieldList, conditionOn);

            if(leadRecords != null && !leadRecords.isEmpty()){
                //Lead lead = new Lead();
                System.debug('xxx lmsLeadId prsent in salesforce ');
                if(leadRecords[HDFC_DASH_Constants.INT_ZERO].Status == HDFC_DASH_Constants.STRING_QUALIFIED){ //Closed means Qualified right?
                    lead = leadRecords[HDFC_DASH_Constants.INT_ZERO].clone(true,false,false,false);   
                    lead.id = null;
                }
                lead.Status = HDFC_DASH_Constants.STRING_QUALIFIED;
                lead.Contact__c = sfCustomerId;
                upsert lead;
                return lead;
                
            }
            else { // no records with LMS Id
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_LMSLeadIdRecordNotPresent);
            }

        }
        else { //LMS leadId not present in request

            lmsApiRequest = setLMSRequestBody(parseApp, lmsApiRequest);
            lmsApiResponse = HDFC_DASH_UtilityClass.callLMS(lmsApiRequest);
            
            String lmsResponseNo = lmsApiResponse?.lead_Details?.lead_no;
            List<String> queryParam = new List<String>{lmsResponseNo};
            leadRecords = HDFC_DASH_Lead_Selector.getLeadsBasedOnCondition(queryParam,leadsFieldList, conditionOn);

            if(leadRecords != null && leadRecords.size() > HDFC_DASH_Constants.INT_ZERO &&  leadRecords[HDFC_DASH_Constants.INT_ZERO].Status == HDFC_DASH_Constants.STRING_NEW){
                lead = leadRecords[HDFC_DASH_Constants.INT_ZERO];
                lead = setLeadRecordsWithLMSResponse(parseApp, lead, lmsApiResponse);
            }
            else {
                lead = setLeadRecordsWithLMSResponse(parseApp,lead,lmsApiResponse);
            }
			lead.Status = HDFC_DASH_Constants.STRING_QUALIFIED;
            lead = mapLeadStandarFields(parseApp, lead);

            upsert lead;
        }
        return lead;
    }

    /*
    Method Name :mapLeadStandarFields
    Parameters  :HDFC_DASH_ParseApplication parseApp,Lead lead
    Description :This method will insert Lead.
    */
    private static Lead mapLeadStandarFields(HDFC_DASH_ParseApplication parseApp,Lead lead){

        for(HDFC_DASH_ParseApplication.ApplicantInfo appDetailRec: parseApp?.application?.applicantInfo){
            lead.LastName = appDetailRec?.personalApplicantInfo?.lastName;
            lead.FirstName = appDetailRec?.personalApplicantInfo?.firstName;
            lead.HDFC_DASH_Resident_Type__c = appDetailRec?.applicantdetail?.residentType;
        }

        return lead;
    }
    
    /* This method will be used for storing sales app nodes when a lead is created, these will be used in future reference
Method Name :insertLeadFromLMS
Parameters  :HDFC_DASH_ParseApplication parseApp,Lead lead,HDFC_DASH_APIResponse_LMS lmsApiResponse ,String sfCustomerId
Description :This method will insert Lead.

    private static Lead insertLeadFromLMS(HDFC_DASH_ParseApplication parseApp,Lead lead,HDFC_DASH_APIResponse_LMS lmsApiResponse ,String sfCustomerId){
        if(lmsApiResponse != null && sfCustomerId != null){
            lead = createLead(lmsApiResponse, parseApp);
            //lead.HDFC_DASH_LMS_Lead_ID__c = lmsApiResponse?.lead_Details?.lead_no;
            lead.HDFC_DASH_LMS_Lead_ID__c = parseApp?.application?.lmsDetails?.lmsLeadId;
            if(!Test.isRunningTest()){
                lead.Contact__c = sfCustomerId;
            }
            lead.Status = HDFC_DASH_Constants.STRING_QUALIFIED;
            lead.HDFC_DASH_Owner_Id__c =  parseApp?.application?.applicationDetails?.ownerId;
            lead.HDFC_DASH_Source_Id__c = parseApp?.application?.applicationDetails?.lmsAgencyCode;
            lead.HDFC_DASH_Source_Executive_Id__c = parseApp?.application?.applicationDetails?.sourceExecutiveId;
            lead.HDFC_DASH_Leap_LR_Number__c =  parseApp?.application?.applicationDetails?.leapLRNumber;
            lead.HDFC_DASH_Leap_LR_Date__c =  parseApp?.application?.applicationDetails?.leapLRDate;
            lead.HDFC_DASH_Source_Reference__c =   parseApp?.application?.applicationDetails?.sourceReference;
            lead.HDFC_DASH_Campaign_Code__c = parseApp?.application?.RecommendedLoanDetails?.campaignCode;
            lead.HDFC_DASH_Reach_Flag__c = parseApp?.application?.applicationDetails?.reachFlag;
            lead.HDFC_DASH_Subvention_Amount__c = parseApp?.application?.applicationDetails?.subventionAmount;
            lead.HDFC_DASH_Subvention_Flag__c = parseApp?.application?.applicationDetails?.subventionFlag;
            insert lead;


            if(parseApp?.application?.applicationDetails?.ownerId != null)
                {
                lead.HDFC_DASH_Owner_Id__c =  parseApp?.application?.applicationDetails?.ownerId;
                }
                if(parseApp?.application?.applicationDetails?.lmsAgencyCode != null)
                {
                lead.HDFC_DASH_Source_Id__c = parseApp?.application?.applicationDetails?.lmsAgencyCode;
                }
                if( parseApp?.application?.applicationDetails?.sourceExecutiveId != null)
                {
                lead.HDFC_DASH_Source_Executive_Id__c = parseApp?.application?.applicationDetails?.sourceExecutiveId;
                }
                if(parseApp?.application?.applicationDetails?.leapLRNumber != null)
                {
                lead.HDFC_DASH_Leap_LR_Number__c =  parseApp?.application?.applicationDetails?.leapLRNumber;
                }
                if(parseApp?.application?.applicationDetails?.leapLRDate != null)
                {
                lead.HDFC_DASH_Leap_LR_Date__c =  parseApp?.application?.applicationDetails?.leapLRDate;
                }
                if(parseApp?.application?.applicationDetails?.sourceReference != null)
                {
                    lead.HDFC_DASH_Source_Reference__c = parseApp?.application?.applicationDetails?.sourceReference;
                }
                if(parseApp?.application?.RecommendedLoanDetails?.campaignCode != null)
                {
                lead.HDFC_DASH_Campaign_Code__c = parseApp?.application?.RecommendedLoanDetails?.campaignCode;
                }
                if(parseApp?.application?.applicationDetails?.reachFlag != null)
                {
                lead.HDFC_DASH_Reach_Flag__c = parseApp?.application?.applicationDetails?.reachFlag;
                }
                if(parseApp?.application?.applicationDetails?.subventionAmount != null)
                {
                lead.HDFC_DASH_Subvention_Amount__c = parseApp?.application?.applicationDetails?.subventionAmount;
                }
                if(parseApp?.application?.applicationDetails?.subventionFlag != null)
                {
                lead.HDFC_DASH_Subvention_Flag__c = parseApp?.application?.applicationDetails?.subventionFlag;
                }
                //update lead;
        }
        return lead;
    }
    */
    
    /* This method will be used for storing sales app nodes when a lead is created, these will be used in future reference
    Method Name :createLead
    Parameters  :HDFC_DASH_APIRequest_LMS  LMSAPIRequest, HDFC_DASH_ParseApplication parseApp
    Description :This method will create Lead.
    
    private static Lead createLead(HDFC_DASH_APIResponse_LMS  lmsApiResponse, HDFC_DASH_ParseApplication parseApp){
        Lead leadRecord = new Lead();
        
        if(lmsApiResponse != null){
            for(HDFC_DASH_ParseApplication.ApplicantInfo appDetailRec: parseApp?.application?.applicantInfo){
                if(appDetailRec?.personalApplicantInfo?.applicantCapacity == 'B'){
                    //system.debug('xxx Inside populating lead' + appDetailRec?.personalApplicantInfo);
                    leadRecord.Salutation = appDetailRec?.personalApplicantInfo?.salutation;
                    leadRecord.FirstName = appDetailRec?.personalApplicantInfo?.firstName;
                    leadRecord.LastName = appDetailRec?.personalApplicantInfo?.lastName;
                    leadRecord.Email = appDetailRec?.personalApplicantInfo?.email;
                    leadRecord.MobilePhone = appDetailRec?.personalApplicantInfo?.mobile;
                    leadRecord.HDFC_DASH_Pan_Applied__c = appDetailRec?.personalApplicantInfo?.panApplied;
                    leadRecord.HDFC_DASH_Pan_Status_From_NSDL__c = appDetailRec?.personalApplicantInfo?.panStatusFromNsdl;
                    leadRecord.HDFC_DASH_PAN__c = appDetailRec?.personalApplicantInfo?.pan;
                    leadRecord.HDFC_DASH_Date_Of_Birth__c = appDetailRec?.personalApplicantInfo?.dateOfBirth;
                    leadRecord.HDFC_DASH_City_Code__c = appDetailRec?.residenceLocation?.cityOfResidenceCode;
                    leadRecord.HDFC_DASH_City__c = appDetailRec?.residenceLocation?.cityOfResidence;
                    leadRecord.HDFC_DASH_State_Code__c = appDetailRec?.residenceLocation?.stateOfResidenceCode;
                    leadRecord.HDFC_DASH_State__c = appDetailRec?.residenceLocation?.stateOfResidence;
                    leadRecord.HDFC_DASH_Country__c	= appDetailRec?.residenceLocation?.countryOfResidence;
                    leadRecord.HDFC_DASH_Country_Code__c = appDetailRec?.residenceLocation?.countryOfResidenceCode;
                    leadRecord.HDFC_DASH_Mobile_Country_Code__c = appDetailRec?.residenceLocation?.cityOfResidenceCode;
                    leadRecord.HDFC_DASH_Outsystems_ID__c = parseApp?.application?.applicationDetails?.outSystemsId;

                    leadRecord = setLeadRecordsWithLMSResponse(parseApp, leadRecord, lmsApiResponse);
                    
                }
            }
        }
        return leadRecord;
    }
    */
       
    /*
    Method Name :setLeadRecordsWithLMSResponse
    Parameters  :Lead leadRecord, HDFC_DASH_APIResponse_LMS  lmsApiResponse
    Description :This method will create Lead.
    */
    private static Lead setLeadRecordsWithLMSResponse(HDFC_DASH_ParseApplication parseApp , Lead leadRecord, HDFC_DASH_APIResponse_LMS  lmsApiResponse){

        leadRecord.HDFC_DASH_LMS_Lead_ID__c = parseApp?.application?.lmsDetails?.lmsLeadId;
        if(!Test.isRunningTest()){
            leadRecord.Contact__c = sfCustomerId;
        }

        leadRecord.HDFC_DASH_LMS_Lead_Status__c = lmsApiResponse?.lead_Details?.lead_status;
        leadRecord.HDFC_DASH_LMS_Origin_Branch_Id__c = lmsApiResponse?.lead_Details?.lead_branch_cd;
        leadRecord.HDFC_DASH_LMS_Origin_Branch__c = lmsApiResponse?.lead_Details?.lead_branch;
        leadRecord.HDFC_DASH_LMS_Lead_ID__c = lmsApiResponse?.lead_Details?.lead_no;
        leadRecord.HDFC_DASH_LMSLeadDate__c =  lmsApiResponse?.lead_Details?.lead_dt;
        leadRecord.HDFC_DASH_LMSLeadSource__c  =  lmsApiResponse?.lead_Details?.source;
        leadRecord.HDFC_DASH_LMSLeadSubSource__c = lmsApiResponse?.lead_Details?.sub_source;
        leadRecord.HDFC_DASH_LMSLeadTermSource__c = lmsApiResponse?.lead_Details?.term_source;
        leadRecord.HDFC_DASH_LMSLeadDistribution__c = lmsApiResponse?.lead_Details?.lead_distribution;
        leadRecord.HDFC_DASH_LMSLeadOwner__c = lmsApiResponse?.lead_Details?.lead_owner;
        leadRecord.HDFC_DASH_LMS_Lead_Type__c = lmsApiResponse?.lead_Details?.lead_type;

        return leadRecord;
    }
    
    /*
Method Name :setLMSRequest
Parameters  :HDFC_DASH_ParseApplication parseApp,HDFC_DASH_APIRequest_LMS  LMSAPIRequest
Description :This method generate the request parameters for LMS.
*/
    private static HDFC_DASH_APIRequest_LMS setLMSRequestBody(HDFC_DASH_ParseApplication parseApp,HDFC_DASH_APIRequest_LMS  lmsApiRequest){
        lmsApiRequest.ref_id = parseApp?.application?.applicationDetails?.outSystemsId;
        //lmsApiRequest.ref_type = HDFC_DASH_Constants.STRING_HDFC_DASH_SPOTOFFER;
        
        
        for(HDFC_DASH_ParseApplication.ApplicantInfo appDetailRec: parseApp?.application?.applicantInfo){
            //system.debug('Raghav xxx parseapp '+appDetailRec?.personalApplicantInfo?.salutation );
            if(appDetailRec?.personalApplicantInfo?.applicantCapacity == 'B'){ //Only creating for primary applicant
                lmsApiRequest.lead_title = appDetailRec?.personalApplicantInfo?.salutation;
                if(appDetailRec?.personalApplicantInfo?.mobile !=NULL){
                    lmsApiRequest.mobile_no = Long.valueof(appDetailRec?.personalApplicantInfo?.mobile);
                }
                lmsApiRequest.middle_name = appDetailRec?.personalApplicantInfo?.middleName;
                lmsApiRequest.last_name = appDetailRec?.personalApplicantInfo?.lastName;
                lmsApiRequest.first_name = appDetailRec?.personalApplicantInfo?.lastName;
                lmsApiRequest.email_id = appDetailRec?.personalApplicantInfo?.email;
                lmsApiRequest.dob = String.valueOf(appDetailRec?.personalApplicantInfo?.dateOfBirth);
                lmsApiRequest.pan_no = appDetailRec?.personalApplicantInfo?.pan;
                lmsApiRequest.resident_type = appDetailRec?.applicantdetail?.residentType;
                lmsApiRequest.voice_consent_flag= appDetailRec?.consent?.voiceConsent; 
                lmsApiRequest.Voice_consent_dt= String.valueOf(appDetailRec?.consent?.voiceConsentDate);
                lmsApiRequest.sms_consent_flag = appDetailRec?.consent?.smsConsent; 
                lmsApiRequest.sms_consent_dt = String.valueOf(appDetailRec?.consent?.smsConsentDate);  
                lmsApiRequest.whatsapp_consent_flag = appDetailRec?.consent?.whatsappConsent;
                lmsApiRequest.whatsapp_consent_dt= String.valueOf(appDetailRec?.consent?.whatsappConsentDate);
                lmsApiRequest.consent_content = appDetailRec?.consent?.consentContent;
                lmsApiRequest.lead_source = HDFC_DASH_Constants.STRING_INTERNET;
                lmsApiRequest.ref_type = HDFC_DASH_Constants.STRING_HDFC_DASH_SPOTOFFER;
                lmsApiRequest.lead_sub_source = HDFC_DASH_Constants.STRING_HDFC_DIGITAL;
                lmsApiRequest.lead_term_Source = HDFC_DASH_Constants.STRING_HDFC_DASH_SPOTOFFER;
                lmsApiRequest.loan_product = 'LOAN';
            }
        }
        //lmsApiRequest.isd_code = leadDetails?.lead?.mobileCountryCode;
        
        //lmsApiRequest.lead_source = HDFC_DASH_Constants.STRING_INTERNET;  
        //lmsApiRequest.lead_sub_source = HDFC_DASH_Constants.STRING_HDFC_DIGITAL;
        //lmsApiRequest.lead_term_Source = HDFC_DASH_Constants.STRING_HDFCDASHSPOTOFFER;
        //lmsApiRequest.loan_product = HDFC_DASH_Constants.STRING_LOAN;
        //system.debug('lmsApi Request '+lmsApiRequest);
        return lmsApiRequest;
    }
}