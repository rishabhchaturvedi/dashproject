/**
className: HDFC_DASH_InsertIncome_StoreAppService
DevelopedBy: Janani Mohankumar
Date: 8 Sep 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the Applicant Income details.
**/
public class HDFC_DASH_InsertIncome_StoreAppService {
 /*
    Method Name :mapIncomeDetails
    Parameters  :Id applicantID, HDFC_DASH_ParseApplication.applicantIncomeDetails parseIncomeDetails
    Description :This method would map all the applicant Income details node for storeApp API and sends us the applicant Income record.
    */
    public static HDFC_DASH_Applicant_Income_Details__c mapIncomeDetails(Id applicantID, HDFC_DASH_ParseApplication.applicantIncomeDetails parseIncomeDetails)
    {
        HDFC_DASH_Applicant_Income_Details__c appIncome = new HDFC_DASH_Applicant_Income_Details__c();
        appIncome.HDFC_DASH_Applicant_Details__c = applicantID;
        appIncome.Id =  parseIncomeDetails?.sfIncomeDetailId;
        appIncome.HDFC_DASH_IncomeType__c = parseIncomeDetails?.incomeType;
        appIncome.HDFC_DASH_Amount__c = parseIncomeDetails?.amount;
        appIncome.HDFC_DASH_Appraised_Variable_Amt__c = parseIncomeDetails?.appraisedVariableAmount;
        appIncome.HDFC_DASH_ILPS_Income_Detail_Id__c = parseIncomeDetails?.ILPSIncomeDetailID;
        
        return appIncome;
}
}