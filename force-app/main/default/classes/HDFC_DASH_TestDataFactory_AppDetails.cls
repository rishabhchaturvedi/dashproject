/* 
ClassName: HDFC_DASH_TestDataFactory_AppDetails
DevelopedBy: Anisha Arumugamv
Date: 05 July 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Applicant Details object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_AppDetails {

    public static final string NAME = 'Name';
    public static final string STAGE = 'Qualification';
    public static final string USER_CONSENT_FOR_CIBIL ='Yes'; 
  	public static final string SALUTATION = 'MR';
    public static final string LAST_NAME = 'AppTest';
   public static final string CAPACITY = 'B';
    public static final Id recordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.RECORDTYPE_PRIMARYAPP).getRecordTypeId();
    
    /* Method Name: createBasicApplicantDetails
    parameters: NONE
    Method Description: This method is to create test data for Applicant Details.
    Pass fields of Applicant Details, and based on doInsert it will either
    pass an instance of uninserted application or inserted application id. 
    */
    public static HDFC_DASH_Applicant_Details__c createBasicApplicantDetails(ID contactID, ID appID)
    {
        HDFC_DASH_Applicant_Details__c appDetails = new HDFC_DASH_Applicant_Details__c();
        appDetails.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        appDetails.HDFC_DASH_Contact__c = contactID;
        appDetails.HDFC_DASH_Application__c = appID;  
        appDetails.HDFC_DASH_User_Consent_For_CIBIL__c = USER_CONSENT_FOR_CIBIL;
        appDetails.RecordTypeId =recordTypeId; 
     	appDetails.HDFC_DASH_Salutation__c =SALUTATION; 
        appDetails.HDFC_DASH_LastName__c =LAST_NAME;
        appDetails.HDFC_DASH_Applicant_Capacity__c  =CAPACITY;
        return appDetails;        
    }
    
    /* Method Name: getBasicApplicantDetails
    parameters: NONE
    Method Description: This method is to generate test data for Applicant Details.
    */
    public static HDFC_DASH_Applicant_Details__c getBasicApplicantDetails(ID contactID, ID appID)
    {
        HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(contactID, appID);
        if(Schema.sObjectType.HDFC_DASH_Applicant_Details__c.isCreateable()){
            Database.insert(appDetails);
        }
        return appDetails;
    }
    
    /* Method Name: createBasicApplicantDetailslist
    parameters: Integer numberOfApplicantDetails
    Method Description: This method is to create a list of test data for Applicant Details
    by calling a createBasicApplicantDetails method.
    */
    public static List<HDFC_DASH_Applicant_Details__c> createBasicApplicantDetailslist(Integer numberOfApplicantDetails, ID contactID, ID appID)
    {
        list<HDFC_DASH_Applicant_Details__c> appDetailslist = new list<HDFC_DASH_Applicant_Details__c>();
        for(integer i = 0 ; i < numberOfApplicantDetails; i++){
            appDetailslist.add(HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(contactID, appID));
        }
        return appDetailslist;
    }
    
    /* Method Name: getBasicApplicantDetailslist
    parameters: Integer numberOfApplicantDetails
    Method Description: This method is to generate a list of test data for Applicant Details.
    */
    public static List<HDFC_DASH_Applicant_Details__c> getBasicApplicantDetailslist(Integer numberOfApplicantDetails, ID contactID, ID appID)
    {
        list<HDFC_DASH_Applicant_Details__c> appDetailslist = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetailslist(numberOfApplicantDetails, contactID, appID);
        if(Schema.sObjectType.HDFC_DASH_Applicant_Details__c.isCreateable()){
            Database.insert(appDetailslist,false);
        }
        return appDetailslist;
    }   
    
}