/* 
ClassName: HDFC_DASH_TestDataFactory_Application
DevelopedBy: Anisha
Date: 06 July 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating test data for Msster Table,Pincode Master,Emp Master,City master,ImageDoc Master,Project Master,KYCDoc Master
object based on parameters received from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_MasterTables {

    public static final string STATE_CD_VAL = 'KA';
    public static final string STATE_CD_DESC = 'KARNATAKA';
    public static final string COUNTRY_CD_VAL = 'INDIA';
    public static final string COUNTRY_CD_DESC = 'INDIA';
    public static final string LOANTYPE_CD_VAL = 'EDU';
    public static final string LOANTYPE_CD_DESC = 'EDUCATIONAL LOAN';
    public static final string CITY_CODE = 'HARAPANAHALLI';
    public static final string PIN_CODE = '636403';
    public static final string PINCODE_PLACE = 'RAMANNAGAR';
    public static final string CITY_NAME = 'HARAPANAHALLI';
    public static final string PROJECT_UID = '178';
    public static final string PROJECT_NAME = 'CHARU HEIGTHS';
    public static final string EMP_CORP_CUST_NO = '1076600';
    public static final string EMP_COMP_NAME = 'ZS ASSOCIATES INDIA PRIVATE LIMITED';
    public static final string GENDER_CD_VAL = 'M';
    public static final string GENDER_CD_DESC = 'Male';
    public static final string DOC_TYPE='APPLFORM';
    public static final string DOC_CODE='PASS';
    public static final string DOC_DESC='PASSPORT';
    public static final string LOAN_PURPOSE = 'PUR';
    public static final string LOAN_PURPOSE_DESC = 'Property from Developer/ Builder';
    public static final string INST_CODE = '100';
    public static final string INST_NAME = 'THE RAJASTHAN STATE COOPERATIVE BANK LTD';
    public static final string RERA_ID = 'P02200001627';
    public static final string RERA_PROJECT_NAME = 'SONABHAN MOHAN BLOCK B';
    public static final string Bank_Name = 'City Bank';
    public static final string Bank_Code = '41';
    public static final string Kyc_RecordType_Id = Schema.SObjectType.HDFC_DASH_KYC_Document_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.KYCMASTERTABLE_RECORDTYPE_KYCDOC).getRecordTypeId();
    public static final string AlterKyc_RecordType_Id = Schema.SObjectType.HDFC_DASH_KYC_Document_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.KYCMASTERTABLE_RECORDTYPE_ALTERKYCDOC).getRecordTypeId();
    
    
    public static final Id recordTypeId = Schema.SObjectType.HDFC_DASH_Master_Table__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_BCP).getRecordTypeId();
    public static final Id state_RecordTypeId = Schema.SObjectType.HDFC_DASH_Master_Table__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_STATECODE).getRecordTypeId();
    public static final Id country_RecordTypeId = Schema.SObjectType.HDFC_DASH_Master_Table__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_COUNTRYCODE).getRecordTypeId();
    public static final Id loanType_RecordTypeId = Schema.SObjectType.HDFC_DASH_Master_Table__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_LOANPRODUCTTYPE).getRecordTypeId();
    public static final Id institution_RecordTypeId =  Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.INSTITUTION_Master).getRecordTypeId();
    /* Method Name: createBasicMasterTable
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Master_Table__c.
    */
    public static List<HDFC_DASH_Master_Table__c> createBasicMasterTable()
    {
        List<HDFC_DASH_Master_Table__c> listMasterTables = new List<HDFC_DASH_Master_Table__c>();
        
        HDFC_DASH_Master_Table__c stateMaster = new HDFC_DASH_Master_Table__c();
        HDFC_DASH_Master_Table__c countryMaster = new HDFC_DASH_Master_Table__c();
        HDFC_DASH_Master_Table__c loanTypeMaster = new HDFC_DASH_Master_Table__c();
         //HDFC_DASH_Master_Table__c genderMaster = new HDFC_DASH_Master_Table__c();

            //Create State Master Record
            stateMaster.HDFC_DASH_CD_Val__c = STATE_CD_VAL;
            stateMaster.HDFC_DASH_CD_Desc__c = STATE_CD_DESC;    
            stateMaster.RecordtypeId = state_RecordTypeId;
            listMasterTables.add(stateMaster);
            
            //Create Country Master Record
            countryMaster.HDFC_DASH_CD_Val__c = COUNTRY_CD_VAL;
            countryMaster.HDFC_DASH_CD_Desc__c = COUNTRY_CD_DESC;   
            countryMaster.RecordtypeId = country_RecordTypeId;
            listMasterTables.add(countryMaster);
            
            //Create Loan Type Master Record
            loanTypeMaster.HDFC_DASH_CD_Val__c = LOANTYPE_CD_VAL;
            loanTypeMaster.HDFC_DASH_CD_Desc__c = LOANTYPE_CD_DESC; 
            loanTypeMaster.RecordtypeId = loanType_RecordTypeId;
            listMasterTables.add(loanTypeMaster);
            
            //Create gender Type Master Record
            //genderMaster.HDFC_DASH_CD_Val__c = GENDER_CD_VAL;
            //genderMaster.HDFC_DASH_CD_Desc__c = GENDER_CD_DESC;  
            //listMasterTables.add(genderMaster);

            //listMasterTables[0].RecordTypeId =recordTypeId;

        return listMasterTables;        
    }
    
    /* Method Name: getBasicMasterTable
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Master_Table__c.
    */
    public static List<HDFC_DASH_Master_Table__c> getBasicMasterTable()
    {
        List<HDFC_DASH_Master_Table__c> listMasterTable = HDFC_DASH_TestDataFactory_MasterTables.createBasicMasterTable();
        if(Schema.sObjectType.HDFC_DASH_Master_Table__c.isCreateable()){
            Database.insert(listMasterTable);
        }
        return listMasterTable;
    }

    /* Method Name: createBasicCityMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_City_Master__c.
    */
    public static HDFC_DASH_City_Master__c createBasicCityMaster()
    {
        HDFC_DASH_City_Master__c cityMaster = new HDFC_DASH_City_Master__c();
            cityMaster.HDFC_DASH_City_Code__c = CITY_CODE;
            cityMaster.HDFC_DASH_City_Name__c = CITY_NAME;        
        return cityMaster;        
    }
    
    /* Method Name: getBasicCityMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_City_Master__c.
    */
    public static HDFC_DASH_City_Master__c getBasicCityMaster()
    {
        HDFC_DASH_City_Master__c cityMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicCityMaster();
        if(Schema.sObjectType.HDFC_DASH_City_Master__c.isCreateable()){
            Database.insert(cityMaster, false);
        }
        return cityMaster;
    }

     /* Method Name: createBasicPincodeMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Pincode_Master__c.
    */
    public static HDFC_DASH_Pincode_Master__c createBasicPincodeMaster()
    {
        HDFC_DASH_Pincode_Master__c pincodeMaster = new HDFC_DASH_Pincode_Master__c();
        pincodeMaster.HDFC_DASH_Pincode__c = PIN_CODE;
        pincodeMaster.HDFC_DASH_Place__c = PINCODE_PLACE;        
        return pincodeMaster;        
    }
    
    /* Method Name: getBasicPincodeMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Pincode_Master__c.
    */
    public static HDFC_DASH_Pincode_Master__c getBasicPincodeMaster()
    {
        HDFC_DASH_Pincode_Master__c pincodeMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicPincodeMaster();
        if(Schema.sObjectType.HDFC_DASH_Pincode_Master__c.isCreateable()){
            Database.insert(pincodeMaster, false);
        }
        return pincodeMaster;
    }
    
    /* Method Name: createBasicProjectMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Project_Master__c.
    */
    public static HDFC_DASH_Project_Master__c createBasicProjectMaster()
    {
        HDFC_DASH_Project_Master__c projectMaster = new HDFC_DASH_Project_Master__c();
            projectMaster.HDFC_DASH_Project_UID__c = PROJECT_UID;
            projectMaster.HDFC_DASH_Project_Name__c = PROJECT_NAME;        
        return projectMaster;        
    }

    /* Method Name: getBasicProjectMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Project_Master__c.
    */
    public static HDFC_DASH_Project_Master__c getBasicProjectMaster()
    {
        HDFC_DASH_Project_Master__c projectMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicProjectMaster();
        if(Schema.sObjectType.HDFC_DASH_Project_Master__c.isCreateable()){
            Database.insert(projectMaster, false);
        }
        return projectMaster;
    }
    
    /* Method Name: createBasicEmpMaster
    parameters: NONE
    Method Description: This method is to create test data for Application.
    Pass fields of Application, and based on doInsert it will either
    pass an instance of uninserted application or inserted application id. 
    */
    public static Account createBasicEmpMaster()
    {
        Account empMaster = new Account();
            empMaster.HDFC_DASH_Corp_Cust_No__c = EMP_CORP_CUST_NO;
            empMaster.Name = EMP_COMP_NAME;        
        return empMaster;        
    }

    /* Method Name: getBasicProjectMaster
    parameters: NONE
    Method Description: This method is to generate test data for Application.
    */
    public static Account getBasicEmpMaster()
    {
        Account empMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicEmpMaster();
        if(Schema.sObjectType.Account.isCreateable()){
            Database.insert(empMaster, false);
        }
        return empMaster;
    }

        /* Method Name: createBasicIMGDocMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Image_Document_Master__c . 
    */
    public static HDFC_DASH_Image_Document_Master__c createBasicIMGDocMaster()
    {
        HDFC_DASH_Image_Document_Master__c ImageMaster = new HDFC_DASH_Image_Document_Master__c();
        ImageMaster.HDFC_DASH_Doc_Type__c = DOC_TYPE;
                
        return ImageMaster;        
    }
     /* Method Name: getBasicIMGDocMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Image_Document_Master__c.
    */
    public static HDFC_DASH_Image_Document_Master__c getBasicIMGDocMaster()
    {
        HDFC_DASH_Image_Document_Master__c ImageMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicIMGDocMaster();
        if(Schema.sObjectType.HDFC_DASH_Image_Document_Master__c.isCreateable()){
            Database.insert(ImageMaster, false);
        }
        return ImageMaster;
    }
    

        /* Method Name: createBasicKYCDocMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_KYC_Document_Master__c.
    */
    public static HDFC_DASH_KYC_Document_Master__c createBasicKYCDocMaster()
    {
        HDFC_DASH_KYC_Document_Master__c KYCMaster = new HDFC_DASH_KYC_Document_Master__c();
        KYCMaster.HDFC_DASH_Doc_Code__c = DOC_CODE;
        KYCMaster.HDFC_DASH_Doc_Desc__c = DOC_DESC; 
        KYCMaster.RecordTypeId = Kyc_RecordType_Id;
        return KYCMaster;        
    }
     /* Method Name: getBasicKYCDocMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_KYC_Document_Master__c.
    */
    public static HDFC_DASH_KYC_Document_Master__c getBasicKYCDocMaster()
    {
        HDFC_DASH_KYC_Document_Master__c KYCMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicKYCDocMaster();
        if(Schema.sObjectType.HDFC_DASH_KYC_Document_Master__c.isCreateable()){
            Database.insert(KYCMaster, false);
        }
        return KYCMaster;
    }
       /* Method Name: createBasicKYCDocMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_KYC_Document_Master__c.
    */
    public static HDFC_DASH_KYC_Document_Master__c createBasicAlterKYCDocMaster()
    {
        HDFC_DASH_KYC_Document_Master__c KYCMaster = new HDFC_DASH_KYC_Document_Master__c();
        KYCMaster.HDFC_DASH_Doc_Code__c = DOC_CODE;
        KYCMaster.HDFC_DASH_Doc_Desc__c = DOC_DESC;
        KYCMaster.RecordTypeId = AlterKyc_RecordType_Id;
        return KYCMaster;        
    }
     /* Method Name: getBasicKYCDocMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_KYC_Document_Master__c.
    */
    public static HDFC_DASH_KYC_Document_Master__c getBasicAlterKYCDocMaster()
    {
        HDFC_DASH_KYC_Document_Master__c KYCMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicAlterKYCDocMaster();
        if(Schema.sObjectType.HDFC_DASH_KYC_Document_Master__c.isCreateable()){
            Database.insert(KYCMaster, false);
        }
        return KYCMaster;
    }
      /* Method Name: createBasicLoanAvailMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c createBasicLoanAvailMaster()
    {
        HDFC_DASH_Property_Loan_Master__c loanAvailedMaster = new HDFC_DASH_Property_Loan_Master__c();
        loanAvailedMaster.HDFC_DASH_Loan_Purpose__c = LOAN_PURPOSE;
        loanAvailedMaster.HDFC_DASH_Loan_Purpose_Desc__c = LOAN_PURPOSE_DESC; 
        loanAvailedMaster.RecordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.LOAN_AVAILED_Master).getRecordTypeId(); 
        return loanAvailedMaster;        
    }
    
     /* Method Name: getBasicLoanAvailMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c getBasicLoanAvailMaster()
    {
        HDFC_DASH_Property_Loan_Master__c loanAvailedMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicLoanAvailMaster();
        if(Schema.sObjectType.HDFC_DASH_Property_Loan_Master__c.isCreateable()){
            Database.insert(loanAvailedMaster, false);
        }
        return loanAvailedMaster;
    }
      /* Method Name: createBasicInstitutionMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c createBasicInstitutionMaster()
    {
        HDFC_DASH_Property_Loan_Master__c institutionMaster = new HDFC_DASH_Property_Loan_Master__c();
        institutionMaster.HDFC_DASH_Institution_Code__c = INST_CODE;
        institutionMaster.HDFC_DASH_Institution_Name__c = INST_NAME;
        institutionMaster.RecordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.INSTITUTION_Master).getRecordTypeId(); 
        return institutionMaster;        
    }
    
     /* Method Name: getBasicInstitutionMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c getBasicInstitutionMaster()
    {
        HDFC_DASH_Property_Loan_Master__c institutionMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicInstitutionMaster();
        if(Schema.sObjectType.HDFC_DASH_Property_Loan_Master__c.isCreateable()){
            Database.insert(institutionMaster, false);
        }
        return institutionMaster;
    }
     /* Method Name: createBasicRERAMaster
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c createBasicRERAMaster()
    {
        HDFC_DASH_Property_Loan_Master__c reraMaster = new HDFC_DASH_Property_Loan_Master__c();
        reraMaster.HDFC_DASH_RERA_Id__c = RERA_ID;
        reraMaster.HDFC_DASH_Project_Name__c = RERA_PROJECT_NAME; 
        reraMaster.RecordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.RERA_MASTER).getRecordTypeId(); 
        return reraMaster;        
    }
    
     /* Method Name: getBasicRERAMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c getBasicRERAMaster()
    {
        HDFC_DASH_Property_Loan_Master__c reraMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicRERAMaster();
        if(Schema.sObjectType.HDFC_DASH_Property_Loan_Master__c.isCreateable()){
            Database.insert(reraMaster, false);
        }
        return reraMaster;
    }
      /* Method Name: createBasicBankDetailMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c createBasicBankDetailMaster()
    {
        HDFC_DASH_Property_Loan_Master__c bankDetailsMaster = new HDFC_DASH_Property_Loan_Master__c();
        bankDetailsMaster.HDFC_DASH_Bank_Name__c = Bank_Name;
        bankDetailsMaster.HDFC_DASH_Bank_Cd__c = Bank_Code; 
        bankDetailsMaster.RecordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.Bank_Details_Master).getRecordTypeId(); 
        return bankDetailsMaster;        
    }
       /* Method Name: getBasicBankDetailsMaster
    parameters: NONE
    Method Description: This method is to generate test data for HDFC_DASH_Property_Loan_Master__c.
    */
    public static HDFC_DASH_Property_Loan_Master__c getBasicBankDetailsMaster()
    {
        HDFC_DASH_Property_Loan_Master__c bankDetailsMaster = HDFC_DASH_TestDataFactory_MasterTables.createBasicBankDetailMaster();
        if(Schema.sObjectType.HDFC_DASH_Property_Loan_Master__c.isCreateable()){
            Database.insert(bankDetailsMaster, false);
        }
        return bankDetailsMaster;
    }
    
}