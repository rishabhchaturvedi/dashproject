public class HDFC_DASH_CaseUpdateUtil {
    
    public static void publishCaseUpdate(Id existingCaseList,String newMode,String newSubMode,String reason,String fromAddress,
                                        String toAddress, String subject, String textBody, String category, String subCategory,string priority,String socialMediaLink){
        
        Case_Update__e caseUpdateEvent = new Case_Update__e();
        caseUpdateEvent.Existing_CaseId__c = existingCaseList;
        caseUpdateEvent.Mode__c = newMode;
        caseUpdateEvent.Sub_Mode__c = newSubMode;
        caseUpdateEvent.Reason__c = reason;
        caseUpdateEvent.FromAddress__c = fromAddress;
        caseUpdateEvent.ToAddress__c = toAddress;
        caseUpdateEvent.Subject__c = subject;
        caseUpdateEvent.TextBody__c = textBody;
                                            if(category !=null)
                                            {
        caseUpdateEvent.Category__c = category;
                                            }
                                            if(subCategory !=null)
                                            {
        caseUpdateEvent.Sub_Category__c = subCategory;
                                            }
        system.debug('SocialMediaLink'+socialMediaLink);
          if(newMode=='Social Media')
                                            {
        caseUpdateEvent.Social_Media_Priority__c = priority;  
         system.debug('SocialMediaLink1'+socialMediaLink);
        caseUpdateEvent.Social_Media_Link__c = socialMediaLink; 
         system.debug('SocialMediaLink2'+socialMediaLink);
                                            }
                                            
        eventBus.publish(caseUpdateEvent);
      }

}