/**
className: HDFC_DASH_Applicant_Income_SelectorTest
DevelopedBy: Sai Shruthi
Date: 09 Sep 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Applicant_Income_Selector class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_Applicant_Income_SelectorTest {
    private static final string STRING_ID='Id';
    private static final string STRING_NAME='Name'; 
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Income_Details__c appIncome = HDFC_DASH_TestDataFactory_ApplIncDetail.getBasicApplIncomeDetail(appDetails.Id);
      /* 
    Method Name :testGetIncomeDetailsBasedOnAppDetailIds
    Description :This method will test the getIncomeDetailsBasedOnAppDetailIds.
    */
    /*static testmethod void testGetIncomeDetailsBasedOnAppDetailIds(){   
        List<HDFC_DASH_Applicant_Income_Details__c> resAppIncRec = null; 
        List<Id> appIncRecIdList = new List<Id>{appIncome.id} ; 
        System.runAs(sysAdmin){   
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME};
            Test.startTest();
            resAppIncRec = HDFC_DASH_Applicant_Income_Selector.getIncomeDetailsBasedOnAppDetailIds(fieldList,appIncRecIdList);
        	Test.stopTest();
        }
        system.assertNotEquals( null, resAppIncRec);
    }*/
    
    /* 
    Method Name :testGetEmploymentDetailsBasedOnId
    Description :This method will test the GetEmploymentDetailsBasedOnId.
    */
    /*static testmethod void testGetIncomeDetailsBasedOnId(){  
        HDFC_DASH_Applicant_Income_Details__c resAppIncRec = null;     
        System.runAs(sysAdmin){   
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME};
            Test.startTest();
             resAppIncRec = HDFC_DASH_Applicant_Income_Selector.getIncomeDetailsBasedOnId(fieldList, appIncome.Id);
        	Test.stopTest();
        }
        system.assertNotEquals( null, resAppIncRec);
        system.assertNotEquals( null, appIncome);
        system.assertEquals(appIncome.Id,resAppIncRec.Id); 
    }*/
    /* 
    Method Name :testGetIncomeDetails
    Description :This method will test the getIncomeDetails.
    */
    static testmethod void testGetIncomeDetails(){  
        List<HDFC_DASH_Applicant_Income_Details__c>  listIncDetailRec = null;
        System.runAs(sysAdmin){ 
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME};
            Test.startTest();
            listIncDetailRec = HDFC_DASH_Applicant_Income_Selector.getIncomeDetails(new List<String> {appIncome.Id}  ,new List<String>{HDFC_DASH_Constants.STRING_ID},HDFC_DASH_Constants.ID_STRING);          
        	Test.stopTest();
        }
        system.assertNotEquals( null, listIncDetailRec);
        system.assertEquals(1,listIncDetailRec.size()); 
    }

    /* 
    Method Name :testGetIncomeDetailsBasedOnQuery
    Description :This method will test the getIncomeDetailsBasedOnQuery.
    */
   /* static testmethod void testGetIncomeDetailsBasedOnQuery(){  
        List<HDFC_DASH_Applicant_Income_Details__c>  listIncDetailRec = null;
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS};
            Test.startTest();
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_APPDETAILS =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                appDetails.Id+HDFC_DASH_Constants.STRING_QUOTE};
            listIncDetailRec = HDFC_DASH_Applicant_Income_Selector.getIncomeDetailsBasedOnQuery(fieldList,condition);      
        	Test.stopTest();
        }
        system.assertNotEquals( null, listIncDetailRec);
        system.assertEquals(1,listIncDetailRec.size()); 
    }*/
}