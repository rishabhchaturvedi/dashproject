/**
className: HDFC_DASH_APIResponse_DeDupeCheck
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: Apex Class for handling DeDupe Response .
**/
public inherited sharing class HDFC_DASH_APIResponse_DedupeCheck { 
     public Boolean isCustomer;
     public Boolean isRegisteredCustomer;
	 public String sfApplicantID;
     public String sfCustomerId;
     public String sfCustomerNumber;
     public Boolean eligibleForExistingCustJourney;
     public Application application;
     public LMSDetails lmsDetails; 
     public String customerEmail;
     public String customerMobile;
    
/*
	Class: Application
	Description: Class for returning ApplicationNode
*/
	public class Application {
       public String sfApplicationId;
		public String sfApplicationNumber;   
	}
/*
	Class: lmsDetails
	Description: Class for returning lmsDetails
*/
	public class LMSDetails {
     public String lmsOriginBranch;
     public String lmsOriginBranchId;
	 public String lmsLeadID;
     public String lmsLeadStatus;
     public String lmsLeadType;
     public Date lmsLeadDate;
     public String lmsLeadSource;
     public String lmsLeadSubSource;
     public String lmsLeadTermSource;
     public String lmsLeadDistribution;
     public String lmsLeadOwner;
	}
    
}