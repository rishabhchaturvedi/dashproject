/**
className: HDFC_DASH_InboundRest_FeeDetailsTest
DevelopedBy: Sai Shruthi
Date: 19 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestService_FeeDetails
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_InboundRest_FeeDetailsTest {
	private static final string SUCCESS ='Success';
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Application_Payment_Details__c appPaymentDetail = HDFC_DASH_TestDataFactory_ApplPayDetail.getBasicApplPaymentDetail(app.id);
    private static HDFC_DASH_Fee_Detail__c feeDetail = HDFC_DASH_TestDataFactory_FeeDetails.getBasicFeeDetail(app.id);
    private static Interface_Framework_Settings__c ifSetting = new Interface_Framework_Settings__c();
    private static Interface_Settings__c interfaceSetting = new Interface_Settings__c();
    private static final string requestBody ='{"sfApplicationId":"'+ app.id +'","sfApplicationNumber":"'+ app.HDFC_DASH_ApplicationNumber__c +'","applicationProcessingFee":"5000","taxes":"900","grossTotal":"5900","discount":"3000","netApplicationFee":"2900","payNow":"1900","payableLater":"4720","paymentTermsAccepted":"Yes","paymentTermsContent":"terms accepted","paymentTermsAcceptedDate":null,"leadStageNumber":"6","leadStageDescription":"description","paymentGateway":{"merchantId":"1234","customerId":"4354354","txnReferenceNo":"3534534","bankReferenceNo":"765756","txnAmount":"5677","txnDate":"2021-12-24","authStatus":"0300","errorStatus":"tno error","errorDescription":"44564","checksum":"65656"},"feeConstruct":{"pfExclTax":"5000","pfInclTax":"5560","stampDuty":"3660","sfExclTax":"3660","sfInclTax":"3660","minFeesExclTax":"1900","minFeesInclTax":"3660","feeosExclTax":"3660","feeosInclTax":"3660","subventAmt":"3660","payableLater":"3660","totFeeExclTax":"5000","totFeeInclTax":"5560"}}';   

    /* 
    Method Name : testprocessFeeDetails
    Parameters  : 
    Description : This method is will call  HDFC_DASH_InboundRestService_FeeDetails.processFeeDetails().
    */
    static testmethod void testprocessFeeDetails(){   
        System.runAs(sysAdmin){
            Test.startTest();
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            HDFC_DASH_APIResponse_FeeDetails feeDetRes =  HDFC_DASH_InboundRestService_FeeDetails.processFeeDetails(request);
            Test.stopTest();
            System.assertNotEquals(null,feeDetRes,SUCCESS);
        }
        
    }
    
    /* 
    Method Name : testUpdateFeeDetails
    Parameters  : 
    Description : This method is will call  HDFC_DASH_InboundRestService_FeeDetails.updateFeeDetails().
    */
    static testmethod void testUpdateFeeDetails(){
        System.runAs(sysAdmin){    

            Test.startTest();
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            HDFC_DASH_APIResponse_FeeDetails apiRes = new HDFC_DASH_APIResponse_FeeDetails();
            HDFC_DASH_ParseFeeDetails feeDetails = HDFC_DASH_ParseFeeDetails.parse(request);
            apiRes = HDFC_DASH_InboundRestService_FeeDetails.updateFeeDetails(feeDetails, apiRes);           
            Test.stopTest();
            system.assertEquals(HDFC_DASH_Constants.FEEDETAILS_SUCCESSMESSAGE, apiRes.successMessage,SUCCESS);
        }
    }   
     /* 
    Method Name : testInsertFeeDetails
    Parameters  : 
    Description : This method is will call  HDFC_DASH_InboundRestService_FeeDetails.insertFeeDetails().
    */
    static testmethod void testInsertFeeDetails(){
        System.runAs(sysAdmin){
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(requestBody);
            Test.startTest();
            HDFC_DASH_ParseFeeDetails feeDetails =  HDFC_DASH_ParseFeeDetails.parse(request);
            HDFC_DASH_InboundRestService_FeeDetails.insertFeeDetails(app.id, feeDetails);
            Test.stopTest();
            //system.assertNotEquals(null,app.HDFC_DASH_SO_App_Processing_Fee__c,SUCCESS);    
            system.assertEquals(2,[SELECT count() FROM HDFC_DASH_Fee_Detail__c where HDFC_DASH_Application__c =:app.Id], SUCCESS); 
               
        }      
    } 

    /* 
    Method Name :testInsertAppPaymentDetail
    Parameters  :
    Description :This method is will call HDFC_DASH_InboundRestService_FeeDetails.insertAppPaymentDetail().
    */
    static testmethod void testInsertAppPaymentDetail(){
        
        System.runAs(sysAdmin){
            RestRequest request = new RestRequest();    
            request.requestBody = Blob.valueOf(requestBody);
            Test.startTest();
            HDFC_DASH_ParseFeeDetails feeDetails =  HDFC_DASH_ParseFeeDetails.parse(request);
            HDFC_DASH_InboundRestService_FeeDetails.insertAppPaymentDetail(app.Id, feeDetails,feeDetail.Id);
            Test.stopTest();
            system.assertEquals(2,[SELECT count() FROM HDFC_DASH_Application_Payment_Details__c where HDFC_DASH_Application__c =:app.Id], SUCCESS);        
        }     
    } 

    /* 
    Method Name :testCreateResponseStructure
    Parameters  :
    Description :This method is will call HDFC_DASH_InboundRestService_FeeDetails.createResponseStructure().
    */
    static testmethod void testCreateResponseStructure(){
        System.runAs(sysAdmin){
            RestRequest request = new RestRequest();        
            request.requestBody = Blob.valueOf(requestBody);
            Test.startTest();
            HDFC_DASH_APIResponse_FeeDetails apiRes = new HDFC_DASH_APIResponse_FeeDetails();
            HDFC_DASH_ParseFeeDetails feeDetails = HDFC_DASH_ParseFeeDetails.parse(request);
            apiRes = HDFC_DASH_InboundRestService_FeeDetails.createResponseStructure(app, apiRes);  
        Test.stopTest();
        system.assertEquals(apiRes.sfApplicationId,app.id,SUCCESS); 
        }
    }
}