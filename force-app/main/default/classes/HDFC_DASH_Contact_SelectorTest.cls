/**
className: HDFC_DASH_Contact_SelectorTest
DevelopedBy: Punam Marbate
Date: 05 July 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Contact_Selector class.
**/
@isTest(SeeAllData = false)
public with sharing class HDFC_DASH_Contact_SelectorTest {

    private static final string STRING_ID='Id';
    private static final string STRING_FIRSTNAME='FirstName'; 
    private static final string STRING_LASTNAME= 'LastName';
    
    /* 
    Method Name :testGetContactsBasedOnIds
    Description :This method will test the getContactsBasedOnIds.
    */
    static testmethod void testGetContactsBasedOnIds(){
        
        List<Contact> resultConList = null ;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        List<Contact> conRecords = new List<Contact>();        
        System.runAs(sysAdmin){
            conRecords = HDFC_DASH_TestDataFactory_Contacts.getBasicContactlist(3);
            Set<Id> resultIds =(new Map<Id,SObject>(conRecords)).keySet();
            List<Id> conIdList = new List<Id>(resultIds);
            List<String> fieldList = new List<string>{STRING_ID,STRING_FIRSTNAME,STRING_LASTNAME};
            Test.startTest();
                resultConList =  HDFC_DASH_Contact_Selector.getContactsBasedOnIds(fieldList, conIdList);  
        	Test.stopTest();
        }
        system.assertNotEquals( null, resultConList);
        system.assertNotEquals( null, conRecords);
        system.assertEquals(3,resultConList.size()); 
        
    }
   
    /* 
    Method Name :testGetContactsBasedOnId
    Description :This method will test the getContactsBasedOnId.
   */
    static testmethod void testGetContactsBasedOnId(){    
        Contact resultContact = null ;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Contact conRecord = null;        
        System.runAs(sysAdmin){
            conRecord = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
            List<String> fieldList = new List<string>{STRING_ID,STRING_FIRSTNAME,STRING_LASTNAME};
            Test.startTest();
                resultContact =  HDFC_DASH_Contact_Selector.getContactBasedOnId(fieldList, conRecord.Id);  
        	Test.stopTest();
        }
        system.assertNotEquals( null, resultContact);
        system.assertNotEquals( null, conRecord);
        system.assertEquals(conRecord.Id,resultContact.Id); 
        
    }
    
    /* 
    Method Name :testGetContactsBasedOnQuery
    Description :This method will test the getContactsBasedOnQuery.
   */   
    static testmethod void testGetContactsBasedOnQuery(){       
        List<Contact> resultConList = null ;
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Contact conRecord;
        System.runAs(sysAdmin){
            conRecord = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();      
            List<String> fieldList = new List<string>{STRING_ID,STRING_FIRSTNAME,STRING_LASTNAME};
            Map<String,String> condition= new Map<string,String>{'HDFC_DASH_PAN__c' => '='+'\''+conRecord.HDFC_DASH_PAN__c+'\''};       
            Test.startTest();
                resultConList =  HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(fieldList, condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, resultConList);
        system.assertNotEquals( null, conRecord);
        system.assertEquals(conRecord.Id,resultConList[0].Id);    
    }
}