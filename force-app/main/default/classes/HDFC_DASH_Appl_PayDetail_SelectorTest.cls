/**
className: HDFC_DASH_Appl_PayDetail_SelectorTest
DevelopedBy: Punam Marbate
Date: 26 July 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Applicant_PayDetail_Selector class.
**/
@isTest(SeeAllData = false)
public with sharing class HDFC_DASH_Appl_PayDetail_SelectorTest {

    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                    HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Application_Payment_Details__c appPayDetail = 
                    HDFC_DASH_TestDataFactory_ApplPayDetail.getBasicApplPaymentDetail(app.Id);
    /* 
    Method Name :testGetApplicationPaymentDetailOnId
    Description :This method will test the method getApplicationPaymentDetailOnId().
    */
   /* static testmethod void testGetApplicationPaymentDetailOnId(){
        System.runAs(sysAdmin){
            HDFC_DASH_Application_Payment_Details__c resApplPayDetailRec;   
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            resApplPayDetailRec = HDFC_DASH_Applicant_PayDetail_Selector.getApplicationPaymentDetailOnId(fieldList,appPayDetail.Id);       
            Test.stopTest();
            system.assertNotEquals( null, resApplPayDetailRec);
            system.assertEquals(appPayDetail.Id,resApplPayDetailRec.Id); 
        }
    }
    */
    /* 
    Method Name :testGetApplicationPaymentDetailOnIds
    Description :This method will test the method getApplicationPaymentDetailOnIds().
    */
    static testmethod void testGetApplicationPaymentDetailOnIds(){
        System.runAs(sysAdmin){
			List<HDFC_DASH_Application_Payment_Details__c>  listPaymentDetail = null;
			List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
			List<Id> listPaymentDetailIds = new List<Id>{appPayDetail.Id};
            Test.startTest();   
            listPaymentDetail = HDFC_DASH_Applicant_PayDetail_Selector.getApplicationPaymentDetailOnIds(fieldList,listPaymentDetailIds);       
            Test.stopTest();
            system.assertNotEquals( null, listPaymentDetail);
            system.assertEquals(listPaymentDetailIds.size(),listPaymentDetail.size()); 
        }
    }
    /* 
    Method Name :testGetApplicationPaymentDetail
    Description :This method will test the method getApplicationPaymentDetail().
    */
    static testmethod void testGetApplicationPaymentDetail(){    
        List<HDFC_DASH_Application_Payment_Details__c>  listAppPayDetailRec = null;
        System.runAs(sysAdmin){   
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            listAppPayDetailRec = HDFC_DASH_Applicant_PayDetail_Selector.getApplicationPaymentDetail(
                                new List<String> {appPayDetail.Id}  ,
                                new List<String>{HDFC_DASH_Constants.STRING_ID}, 
                                HDFC_DASH_Constants.ID_STRING);      
            Test.stopTest();
        }
        system.assertNotEquals( null, listAppPayDetailRec);
        system.assertEquals(1,listAppPayDetailRec.size()); 
    }
     /* 
    Method Name :testGetApplicationPaymentDetailBasedOnQuery
    Description :This method will test the method getApplicationPaymentDetailBasedOnQuery().
    */
    static testmethod void testGetApplicationPaymentDetailBasedOnQuery(){
        List<HDFC_DASH_Application_Payment_Details__c>  listAppPayDetailRec = null;
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Application =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                app.Id+HDFC_DASH_Constants.STRING_QUOTE};
            listAppPayDetailRec = HDFC_DASH_Applicant_PayDetail_Selector.getApplicationPaymentDetailBasedOnQuery(
                new List<String>{HDFC_DASH_Constants.STRING_ID},condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listAppPayDetailRec);
        system.assertEquals(1,listAppPayDetailRec.size());       
      }
}