/**
className: HDFC_DASH_Task_SelectorTest
DevelopedBy: Janani Mohankumar
Date: 27 NOV 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_Task_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_Task_SelectorTest {
//Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Id sysAdminId = UserInfo.getUserId();
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static Task task = HDFC_DASH_TestDataFactory_Task.getBasicTask(app.id,con.id,sysAdminId);
    private static final string SUCCESS ='Success';
    
     /* 
    Method Name :testGetTask
    Description :This method will test the method getTask().
    */
    static testmethod void testGetTask(){
        List<Task> resTaskRec = new List<Task>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Test.startTest();   
            resTaskRec = HDFC_DASH_Task_Selector.getTask(new List<String> {task.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, resTaskRec);
            system.assertEquals(1,resTaskRec.size());  
        }
    }
    /* 
    Method Name :testGetTaskBasedOnQuery
    Description :This method will test the method getTaskBasedOnQuery().
    */
    static testmethod void testGetTaskBasedOnQuery(){
        List<Task> listTaskRec  = new List<Task>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.ID_STRING =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                task.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listTaskRec = HDFC_DASH_Task_Selector.getTaskBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listTaskRec);
        system.assertEquals(1,listTaskRec.size(),SUCCESS);       
      }
     /* 
    Method Name :testGetTaskBasedOnLimit
    Description :This method will test the method getTaskBasedOnLimit().
    */
     static testmethod void testGetTaskBasedOnLimit(){
        List<Task> listTaskRec  = new List<Task>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.ID_STRING =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                task.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listTaskRec = HDFC_DASH_Task_Selector.getTaskBasedOnLimit(fieldList,condition,HDFC_DASH_Constants.INT_THREE);
            Test.stopTest();
        }
        system.assertNotEquals( null, listTaskRec);
        system.assertEquals(1,listTaskRec.size(),SUCCESS);       
      }
}