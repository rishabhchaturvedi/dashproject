/**
className: HDFC_DASH_InboundRestServiceStoreAppILPS
DevelopedBy: Punam Marbate
Date: 30 Nov 2021
Company: Accenture
Class Description: This class would get the application details and related records as request from ILPS and storing it in salesforce.
**/
public with sharing class HDFC_DASH_InboundRestServiceStoreAppILPS {

    public static String applicationId;
    public static Boolean isUpdatePrimaryApplicant = false;
    public static Boolean byPassEditFlags = false;
    /* 
    Method Name :processApplication
    Parameters  :RestRequest request
    Description :This method would store application.
    */
    public static HDFC_DASH_ParseApplication processApplication(RestRequest request,Boolean isFullApplicationILPSPush){
     
        HDFC_DASH_ParseApplication application = HDFC_DASH_ParseApplication.parse(request);
        callUpsertApplicationData(application,isFullApplicationILPSPush);
        HDFC_DASH_ParseApplication apiResp = new HDFC_DASH_ParseApplication();
        apiResp = HDFC_DASH_InboundRestService_GetApp.getApplicationFields(applicationId);
        return apiResp;
    }
    /* 
    Method Name :callUpsertApplicationData
    Parameters  :HDFC_DASH_ParseApplication parseApp 
    Description :This method would call different methods from HDFC_DASH_StoreAppHelper and store application.
    */
    public static void callUpsertApplicationData(HDFC_DASH_ParseApplication parseApp,Boolean isFullApplicationILPSPush)
    {
        Opportunity appRec = new opportunity();
        HDFC_DASH_StoreAppWrapper storeWrapper = new HDFC_DASH_StoreAppWrapper();
        Lead lead = null;
        Map<String,HDFC_DASH_Applicant_Details__c> customerNoApplicantMap = new  Map<String,HDFC_DASH_Applicant_Details__c>();
        List<HDFC_DASH_Applicant_Details__c> listApplDetail = new List<HDFC_DASH_Applicant_Details__c>(); 
        Map<HDFC_DASH_Applicant_Details__c,Account> personAccToInsert = new Map<HDFC_DASH_Applicant_Details__c,Account>();
        Map<Id,Id> conAccountMap = new Map<Id,Id>();
        Map<HDFC_DASH_ParseApplication.ApplicantInfo,HDFC_DASH_Applicant_Details__c> mapApplicantInfoDetail = new Map<HDFC_DASH_ParseApplication.ApplicantInfo,HDFC_DASH_Applicant_Details__c>();
        Map<HDFC_DASH_ParseApplication.ApplicantEmploymentDetails,HDFC_DASH_Applicant_Employment_Details__c> mapAppEmpDetail = new Map<HDFC_DASH_ParseApplication.ApplicantEmploymentDetails,HDFC_DASH_Applicant_Employment_Details__c>();
        Map<id,HDFC_DASH_Applicant_Details__c> mapApplicant = new Map<id,HDFC_DASH_Applicant_Details__c>();
        Map<Id,Id> mapContactAccount = new Map<id,id>();
        //appRec.HDFC_DASH_Application_Created_From_ILPS__c = isFullApplicationILPSPush;
        //system.debug('appRec.HDFC_DASH_Application_Created_From_ILPS__c'+ appRec.HDFC_DASH_Application_Created_From_ILPS__c);
        //Insert Application
        Savepoint sp = Database.setSavepoint();
        try
        {
            if(appRec.id == null){
                isUpdatePrimaryApplicant = true;
                byPassEditFlags = true;
            }
            appRec = HDFC_DASH_StoreAppHelper.upsertApplicationDetails(parseApp,lead,isFullApplicationILPSPush,byPassEditFlags);
            applicationId = appRec.Id;
            if(parseApp.application.applicationDetails.sfApplicationId == null){
                parseApp.application.applicationDetails.sfApplicationId = appRec.Id;
            }
            
            //update or insert Funding Source
            HDFC_DASH_StoreAppHelper.upsertFundingSource(parseApp,byPassEditFlags);
            
            //update or insert Additional Loan Details
            Opportunity primeApp = HDFC_DASH_Application_Selector.getApplicationBasedOnId(new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id},appRec.id);
            HDFC_DASH_StoreAppHelper.upsertAdditionalLoan(parseApp,primeApp,byPassEditFlags);
            
            //update or insert Applicant Detail
            storeWrapper = HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForApplicantDetail(parseApp, isUpdatePrimaryApplicant);
        
            //collect customer no from the inserted Applicant take it in customerNoApplicantMap
            customerNoApplicantMap = storeWrapper.customerNoApplicantMap;
            //system.debug('StoreWrapper--'+storeWrapper.customerNoApplicantMap);

            List<String> customerNoList = new List<String>();
            if(customerNoApplicantMap != null){
                for(String customerNo : customerNoApplicantMap.keySet())
                {
                    if(String.isNotBlank(customerNo)){
                    customerNoList.add(customerNo);
                    }else if(String.isBlank(customerNo)){
                        HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_CUSTNO_EMPTY_FROM_ILPS); 
                    }
                }
            }
            //system.debug('Customer No List'+customerNoList);
            List<String> fieldConList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_ACCOUNTID,HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number};
            List<Contact> conRecs = HDFC_DASH_Contact_Selector.getContacts(customerNoList,fieldConList,HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number);
            //system.debug('conRec'+conRecs);
            Map<String,Contact> mapCustomerNoContact = new Map<String,Contact>();
            if(conRecs != null){
                for(Contact con : conRecs){
                    mapCustomerNoContact.put(con.Account.HDFC_DASH_Customer_Number__c,con);
                    mapContactAccount.put(con.Id,con.AccountId);
                }
            }
            for(HDFC_DASH_ParseApplication.ApplicantInfo appDetailRec: parseApp?.application?.applicantInfo)
            {
                if(!appDetailRec?.applicantdetail?.deleteApplicantFlag)
                {
                    HDFC_DASH_Applicant_Details__c applDetail = customerNoApplicantMap.get(appDetailRec.applicantDetail.sfCustomerNumber);
                    if(appldetail.HDFC_DASH_Contact__c == null){
                        if(mapCustomerNoContact.values() != null && mapCustomerNoContact.get(appDetailRec.applicantdetail.sfCustomerNumber) != null){
                            //ILPS will populate both ILPSCustNo and sfCustomerNumber
                            appldetail.HDFC_DASH_Contact__c = mapCustomerNoContact.get(appDetailRec.applicantdetail.sfCustomerNumber).id;
                            appldetail.HDFC_DASH_ILPS_Customer_Number__c = appDetailRec.applicantdetail.ilpsCustomerNumber;
                        system.debug('ilpsCustomerNumber'+appDetailRec.applicantdetail.ilpsCustomerNumber);
                            //system.debug('Conatct Id for matching customer with customer number--'+appldetail.HDFC_DASH_Contact__c);
                        }
                        else{
                        Account newPersonAccount = HDFC_DASH_SalesforceDedupe.createPersonAccount(applDetail);
                            //ILPS will populate both ILPSCustNo and sfCustomerNumber
                        applDetail.HDFC_DASH_ILPS_Customer_Number__c = appDetailRec.applicantdetail.ilpsCustomerNumber;
                        newPersonAccount.HDFC_DASH_Customer_Number__c = appDetailRec.applicantdetail.sfCustomerNumber;
                        newPersonAccount.HDFC_DASH_Is_Customer__pc = true;
                        newPersonAccount.HDFC_DASH_Eligble_For_Exstng_Cust_Journy__pc = true;
                        personAccToInsert.put(appldetail, newPersonAccount);
                        mapApplicant.put(applDetail.Id,applDetail);
                        
                        }
                    }      
                }
            }
            //take inserted applicant list from storeApphelper from the wrapper
            listApplDetail = storeWrapper.listApplicantDetails;
            if(personAccToInsert.values() != NULL && personAccToInsert.values().size() > HDFC_DASH_Constants.INT_ZERO){
                //Insert person Account
                insert personAccToInsert.values();
                
            
                List<Id> listAccountIds = new List<Id>();
                for(Account acc : personAccToInsert.values())
                {
                    listAccountIds.add(acc.Id);
                }
                List<String> conFieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_ACCOUNTID};
                conRecs = HDFC_DASH_Contact_Selector.getContacts(listAccountIds,conFieldList,HDFC_DASH_Constants.STRING_ACCOUNTID);
                //system.debug('conRec'+conRecs);
            
                for(Contact con : conRecs){
                    conAccountMap.put(con.AccountId,con.Id);
                    mapContactAccount.put(con.Id,con.AccountId);
                }
                
                if(conAccountMap != NULL && conAccountMap.size() > HDFC_DASH_Constants.INT_ZERO){
                    for(HDFC_DASH_Applicant_Details__c appldetail: listApplDetail){
                        if(appldetail.HDFC_DASH_Contact__c == null){
                            
                            appldetail.HDFC_DASH_Contact__c = conAccountMap.get(personAccToInsert.get(appldetail)?.id);
                        }
                        //system.debug('appldetail.HDFC_DASH_Contact__c--'+appldetail.HDFC_DASH_Contact__c);
                    }
                }
            }
        
            if(listApplDetail != null){
                database.update(listApplDetail);
            }
            if(listApplDetail != null){
                for(HDFC_DASH_Applicant_Details__c appDetail :listApplDetail){
                    //system.debug('Inside for application Update');
                    if(appDetail.HDFC_DASH_Application__c == parseApp.application.applicationDetails.sfApplicationId && appDetail.HDFC_DASH_Applicant_Capacity__c == HDFC_DASH_Constants.STRING_B){
                        //system.debug('inside if--');
                        system.debug('applicantDetail--'+appDetail);
                        //system.debug('AccounId=='+appDetail.HDFC_DASH_Contact__r.AccountId);
                        mapContactAccount.get(appDetail.HDFC_DASH_Contact__c);
                        appRec.AccountId = mapContactAccount.get(appDetail.HDFC_DASH_Contact__c);
                        //system.debug('appRec.AccountId--'+ appRec.AccountId);
                    }
                }
            }
            //database.update(appRec);
            
            mapApplicantInfoDetail = storeWrapper.mapApplicantInfoDetail;
            Map<Id,HDFC_DASH_Applicant_Details__c> mapAddressdetailApplicantRec = new Map<Id,HDFC_DASH_Applicant_Details__c>();
            List<HDFC_DASH_Contact_Address_Details__c> addressDetailListForContactUpdate = new List<HDFC_DASH_Contact_Address_Details__c>();
            List<HDFC_DASH_Contact_Address_Details__c> listAddrDetails = HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForApplicantAddressDetail(parseApp,listApplDetail,mapApplicantInfoDetail);
            //system.debug('mapAddressdetailApplicantRec keyset'+mapAddressdetailApplicantRec.keyset());
            /* for(HDFC_DASH_ParseApplication.ApplicantInfo appDetailRec: parseApp?.application?.applicantInfo)
            {
                if(!appDetailRec?.applicantdetail?.deleteApplicantFlag)
                {
                    for(HDFC_DASH_Contact_Address_Details__c addrDetail :listAddrDetails)
                    {
                        mapAddressdetailApplicantRec.put(addrDetail.id,appDetailRec);
                        //system.debug(mapAddressdetailApplicantRec);
                    }

                }
            }*/
            if(listApplDetail != null){
                for(HDFC_DASH_Applicant_Details__c appDetail :listApplDetail){
                    for(HDFC_DASH_Contact_Address_Details__c addrDetail :listAddrDetails)
                        {
                            if(addrDetail.HDFC_DASH_Applicant_Details__c == appDetail.id){
                            mapAddressdetailApplicantRec.put(addrDetail.id,appDetail);
                            //system.debug('mapAddress and applicant--'+mapAddressdetailApplicantRec);
                            }
                        }
                }
            }

            List<Id> addrIds = new List<Id>();
            addrIds.addall(mapAddressdetailApplicantRec.keyset());
            List<HDFC_DASH_Applicant_Details__c> applicantDetailsList =new list<HDFC_DASH_Applicant_Details__c>();
            List<HDFC_DASH_Contact_Address_Details__c> listAddress = HDFC_DASH_Contact_Address_Selector.getContactAddressBasedOnIds(new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.ADDRESS_TYPE},addrIds);
            for(HDFC_DASH_Contact_Address_Details__c addDetail : listAddress)
            {
                //system.debug('mapAddressdetailApplicantRec--'+mapAddressdetailApplicantRec);
                //HDFC_DASH_ParseApplication.ApplicantInfo appdetailRec = mapAddressdetailApplicantRec.get(addDetail.id);
                HDFC_DASH_Applicant_Details__c applicantDetail = mapAddressdetailApplicantRec.get(addDetail.id);
                
                //HDFC_DASH_Applicant_Details__c applicantDetail = mapApplicantInfoDetail.get(appdetailRec);
                //system.debug('applicantDetail--'+applicantDetail);
                if(applicantDetail!=null)
                {
                    //system.debug('applicant details before update' + applicantDetail);
                    if(addDetail.HDFC_DASH_Address_Type__c == HDFC_DASH_Constants.STRING_P){
                        applicantDetail.HDFC_DASH_Permanent_Address__c = addDetail.id;
                    }else if(addDetail.HDFC_DASH_Address_Type__c == HDFC_DASH_Constants.STRING_C){
                        applicantDetail.HDFC_DASH_Current_Address__c = addDetail.id;
                    } 
                    if(!applicantDetailsList.contains(applicantDetail)){
                        applicantDetailsList.add(applicantDetail);   
                    }
                }      
                if(mapApplicant.get(applicantDetail.id)!=null)
                {
                    addDetail.HDFC_DASH_Contact__c = applicantDetail.HDFC_DASH_Contact__c;
                    addDetail.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_YES;
                    addressDetailListForContactUpdate.add(addDetail);
                }
            }
            
            
            Database.update(applicantDetailsList);
            database.update(addressDetailListForContactUpdate);
            
            mapAppEmpDetail = HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForApplicantEmploymentDetail(parseApp,listApplDetail,mapApplicantInfoDetail);
            List<HDFC_DASH_Applicant_Bank_Detail__c> listBankDetail = HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForBankDetails(parseApp,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.upsertApplicantIncomeDetails(parseApp,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForFinancialInfoDetails(parseApp,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.upsertNoteDetails(parseApp,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.upsertApplicantGSTDetails(parseApp, mapAppEmpDetail);
        
            for(HDFC_DASH_Applicant_Employment_Details__c empDetail : mapAppEmpDetail.values())
            {
                HDFC_DASH_Applicant_Details__c applDetail = mapApplicant.get(empDetail.HDFC_DASH_Applicant_Details__c);
                if(applDetail != null){
                    empDetail.HDFC_DASH_Contact__c = applDetail.HDFC_DASH_Contact__c;
                    empDetail.HDFC_DASH_Latest__c = HDFC_DASH_Constants.STRING_YES;
                }
                //system.debug('empDetail.HDFC_DASH_Contact__c--'+ empDetail.HDFC_DASH_Contact__c);
            }
            database.update(mapAppEmpDetail.values());

            for(HDFC_DASH_Applicant_Bank_Detail__c bankDetail : listBankDetail){
                HDFC_DASH_Applicant_Details__c applDetail = mapApplicant.get(bankDetail.HDFC_DASH_Applicant_Details__c);
                if(applDetail != null){
                    bankDetail.HDFC_DASH_Contact__c = applDetail.HDFC_DASH_Contact__c;
                    bankDetail.HDFC_DASH_Bank_Detail_Latest__c = HDFC_DASH_Constants.STRING_YES;
                }
                //system.debug('bankDetail.HDFC_DASH_Contact__c--'+ bankDetail.HDFC_DASH_Contact__c);
            }
            database.update(listBankDetail);
            
            system.debug('listApplDetail==>'+listApplDetail);
            //status mapped here becuase to copy child records onto contact filter condition needs to be satisfy i.e.
            //application status should be changed to Fully Disbursed,
            //In first insert call status would be new application created as we are not mapping status there and stage is onbording
            appRec.HDFC_DASH_Status__c = parseApp.application?.applicationDetails?.status;
            system.debug('Application Record '+appRec.HDFC_DASH_LMS_Origin_Branch_Id__c);
            database.update(appRec);
    }catch(Exception e){
        
        Database.rollback(sp);   
        throw e;
    }
    }

}