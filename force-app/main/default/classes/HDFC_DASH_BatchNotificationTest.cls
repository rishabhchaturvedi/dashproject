/**
className: HDFC_DASH_BatchNotificationTest
DevelopedBy: Tejeswari
Date: 27 January 2022
Company: Accenture 
Class Description: Test class for  HDFC_DASH_BatchNotification class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_BatchNotificationTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static User userRec = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string ROLE_NAME= 'Sales Officer';
    private static final string PROFILE_NAME= 'HDFC Partner Community User';
    private static final string FEDERATION_IDENTIFIER= 'Johnrocky@abc.com';
    private static final string USER_NAME= 'usertestcls@testorg.com';
    private static final DateTime currentDateTime= System.Now();
    private static final string EVENT_TYPE= 'Activity';
    private static final string STATUS= 'Open';
    
    
    /* 
Method Name :testBatchNotificationTest
Description :This method would test HDFC_DASH_BatchNotification batch class.
*/
    static testmethod void testBatchNotificationTest(){
        Exception testException;
        UserRole role = [SELECT Id FROM UserRole WHERE Name=:ROLE_NAME];
        userRec.UserRoleId=role.id;
        userRec.UserName = FEDERATION_IDENTIFIER+string.valueOf(datetime.now().getTime());
        userRec.FederationIdentifier = FEDERATION_IDENTIFIER;
        String profileRec = [Select id,name from profile WHERE Name=:PROFILE_NAME].Id;
        
        System.runAs(sysAdmin) {
            try{
                Database.insert(userRec);
                acc.OwnerId=userRec.Id;
                Database.insert(acc);
                
                Contact conRec = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
                User partnerUser= HDFC_DASH_TestDataFactory.createUser(PROFILE_NAME);
                partnerUser.FederationIdentifier = USER_NAME;
                partnerUser.UserName = USER_NAME+string.valueOf(datetime.now().getTime());
                partnerUser.ProfileId = profileRec;
                partnerUser.contactId = conRec.id;
                Database.insert(partnerUser);
                
                Event event=HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.Id,sysAdmin.Id);
                event.Type = EVENT_TYPE;
                event.StartDateTime = currentDateTime.addMinutes(-240);
                event.EndDateTime= event.StartDateTime + (30/1440); 
                event.HDFC_DASH_BSA__c = null;
                event.HDFC_DASH_Status__c = STATUS;
                event.OwnerId = partnerUser.Id;
                
                Database.insert(event);
                Event eventRec=[select type,HDFC_DASH_User_Type__c,StartDateTime,HDFC_DASH_Missed__c,HDFC_DASH_Actor_Executive_Code_Text__c, id from Event where id=: event.id];
                system.debug('eventRec '+eventRec);
                Test.StartTest();
                HDFC_DASH_BatchNotification notificationBatch = new HDFC_DASH_BatchNotification(); 
                Database.executebatch(notificationBatch, 200);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            system.assertEquals(null, testException);
        }
    }
    
}