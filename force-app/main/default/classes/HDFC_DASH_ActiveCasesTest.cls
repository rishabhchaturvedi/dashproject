/**
*className: HDFC_DASH_ActiveCases
DevelopedBy: Kajal Mehta
Date: 22 November 2021
Company: Accenture
Class Description: To count active cases for particular role**/

@isTest(SeeAllData = false)
public class HDFC_DASH_ActiveCasesTest 
{
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);  
    private static User standardUser = HDFC_DASH_TestDataFactory.createUser('Chatter Free User');  
    
    private static List<Integer> activeCases = new List<Integer>();
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static Case caseRecord = HDFC_DASH_TestDataFactory_Case.getBasicCase(acc.Id,app.Id);
 
    static testmethod void countActiveCasesTest()
    { 
        Exception testException;
        System.runAs(sysAdmin){
            Test.startTest();
            try{
                activeCases = HDFC_DASH_ActiveCases.countActiveCases();
                //system.assertNotEquals(0, activeCases.size())
                  //  throw testException;
            }
            catch (Exception ex)
            {
                testException = ex;
            }
            Test.stopTest();
            system.assertEquals(1,activeCases.size());
            
        }
    }
    static testmethod void countActiveCasesTestCatchBlock()
    {
       
        Exception testException;
        System.runAs(standardUser){
            Test.startTest();
            try{
                HDFC_DASH_ActiveCases.countActiveCases();                
            }
            catch (Exception ex)
            {
                testException = ex;
            }
            Test.stopTest(); 
        }
    }
}