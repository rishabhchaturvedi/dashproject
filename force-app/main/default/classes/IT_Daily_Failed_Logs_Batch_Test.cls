/* ================================================
 * @Class Name :  IT_Daily_Failed_Logs_Details_Mail_Batch_Test
 * @author : Accenture
 * @Purpose: This class is used to test the functionality of class IT_Daily_Failed_Logs_Details_Mail_Batch
 * @created date:
 * @Last modified date:19/1/2016
 * @Last modified by : Accenture
 ================================================*/
@istest(SeeAllData = false)
public class IT_Daily_Failed_Logs_Batch_Test{
    
    /***************************************************************************************************************
    *   @Name        :  testMethodFirst                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  It is test Method for execute method in IT_Daily_Failed_Logs_Details_Mail_Batch                       
    ***************************************************************************************************************/
    public static testMethod void testMethodFirst(){
        IT_Test_SingleRequestMock fakeResponse = new IT_Test_SingleRequestMock(200,
                                                 'Complete',
                                                 '[{"Name": "sForceTest1"}]',
                                                 null);
        
        // generate header map
        Map<String, string> headermp = new Map<String, string>();
        headermp.put('adi','123');
        headermp.put('adit','1234');
        headermp.put('aditya','12345');
        string headermpString = JSON.serialize(headermp);
        
        // get utility class object
        IT_TestUtility utility = new IT_TestUtility();
        
        // insert Integration Message Log record
        Integration_Message_Log__c intLogObj = new Integration_Message_Log__c();
        utility.createInterationLog(intLogObj);
        intLogObj.Apex_Error__c = false;
        intLogObj.Status_Code__c = null;
        intLogObj.Header_Map__c = headermpString;
        intLogObj.Expiry_time_mins__c = 30;
        intLogObj.Number_Of_Retries__c = 0;
        intLogObj.Maximum_No_Of_Retries__c = 2;
        intLogObj.Retry_Interval__c = 0;
        intLogObj.Callout_Date__c = system.Now() - (2/1440) ;
        Database.update(intLogObj);

        //insert framework setting record
        utility.insertFrameworkSettingsRecord(false);
        
        // insert interface setting record
        Interface_Settings__c interfaceSettingObject = new Interface_Settings__c();
        interfaceSettingObject = utility.insertIntefaceSettingsRecord();
        interfaceSettingObject.User_Contact_ID_for_Email__c = utility.insertContact();
        //interfaceSettingObject.Email_Template_Id__c = utility.createEmailTemplate();
        
        Database.update(interfaceSettingObject);
        
        test.starttest();
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Database.executeBatch(new IT_Daily_Failed_Logs_Details_Mail_Batch());
        test.stoptest();
    }
}