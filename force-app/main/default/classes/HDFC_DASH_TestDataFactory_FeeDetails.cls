/* 
ClassName: HDFC_DASH_TestDataFactory_FeeDetails
DevelopedBy: Punam Marbate
Date: 26 Aug 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Fee Detail object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_FeeDetails {
    public static final string NAME = 'Name';
    
    /* Method Name: createBasicFeeDetail
    parameters: ID applID
    Method Description: This method is to create test data for HDFC_DASH_Fee_Detail__c. 
    */
    public static HDFC_DASH_Fee_Detail__c createBasicFeeDetail(ID applID)
    {
        HDFC_DASH_Fee_Detail__c feeDetail = new HDFC_DASH_Fee_Detail__c();
            
        //feeDetail.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        feeDetail.HDFC_DASH_Application__c =  applID  ;       
        return feeDetail;        
    }
    
    /* Method Name: getBasicFeeDetail
    parameters: ID applID
    Method Description: This method is to generate test data for Fee Detail.
    */
    public static HDFC_DASH_Fee_Detail__c getBasicFeeDetail(ID applID)
    {
        HDFC_DASH_Fee_Detail__c feeDetail = HDFC_DASH_TestDataFactory_FeeDetails.createBasicFeeDetail(applID);
        if(Schema.sObjectType.HDFC_DASH_Fee_Detail__c.IsCreateable()){
        Database.insert(feeDetail);
        }
        return feeDetail;
    } 
}