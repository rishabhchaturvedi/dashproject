/**
* 	className: HDFC_DASH_AccountTriggerOps
DevelopedBy: Anmol
Date: 25 May 2021
Company: Accenture
Class Description: Simply a collection of all trigger operations in relation to Account, organised in a top-level class.
* 
*/
public with sharing class HDFC_DASH_AccountTriggerOps { 
    /* ClassName: Validation
Class Description: Phone would be a required field if type='Prospect'.
*/
   // public with sharing class Validation implements HDFC_DASH_TriggerOps {
        /* Method Name: isEnabled
Class Description: method check if the operation needs to be executed .
*/
       /* public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.validationIsFirstRun;
           
        }*/
        /* Method Name: filter
Class Description: method to return the sobjects .
*/
       /* public SObject[] filter() {
            system.debug('***Account Trigger Filter');    
            
            if(Trigger.new != NULL){
                return Trigger.new;
            }else{
                return Trigger.old;
            }
        }*/
        /* Method Name: execute
Class Description: Execute the business logic that is implemented in the execute() method .
*/
     /*   public void execute(Account[] accounts) {
            HDFC_DASH_TriggerOpsRecursionFlags.validationIsFirstRun= false;
        }
    }*/
    /* ClassName: UpdateContactDescription
Class Description: Updates the contact description if Employees > 50.
*/
   // public without sharing class UpdateContactDescription implements HDFC_DASH_TriggerOps {
        /* Method Name: isEnabled
Class Description: method check if the operation needs to be executed .
*/
      /*  public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.cont_Desc_IsFirstRun;
        }*/
        /* Method Name: filter
Class Description: method to return the sobjects .
*/
      /*  public SObject[] filter() {
            return Trigger.new;
        }*/
        /* Method Name: execute
Class Description: Execute the business logic that is implemented in the execute() method .
*/
        /*public void execute(Account[] accounts) {
            //HDFC_DASH_TriggerOpsRecursionFlags.cont_Desc_IsFirstRun = false;        
        }
    }*/
    
    
    /**
className: OptinIntegrationForInsert
DevelopedBy: Tejeswari
Date: 20 January 2022
Company: Accenture 
Class Description: This Class contains the trigger operation on Account object.
*/
    public with sharing class OptinIntegrationForInsert implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.PersonAcc_OptinCalloutInsert_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Account> listAccountRecs = HDFC_DASH_AccountTriggerOpsHelper.filterInsertAccForOptinCallout();
            return listAccountRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfAccounts){
            HDFC_DASH_AccountTriggerOpsHelper.optinCallout(listOfAccounts);
            HDFC_DASH_TriggerOpsRecursionFlags.PersonAcc_OptinCalloutInsert_FirstRun = false;
        }  
    }
    
    /**
className: OptinIntegrationForUpdate
DevelopedBy: Tejeswari
Date:20 January 2022
Company: Accenture 
Class Description: This Class contains the trigger operation on Account object.
*/
    public with sharing class OptinIntegrationForUpdate implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.PersonAcc_OptinCalloutUpdate_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Account> listAccountRecs = HDFC_DASH_AccountTriggerOpsHelper.filterUpdateAccForOptinCallout();
            return listAccountRecs;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listOfAccounts){
            HDFC_DASH_AccountTriggerOpsHelper.optinCallout(listOfAccounts);
            HDFC_DASH_TriggerOpsRecursionFlags.PersonAcc_OptinCalloutUpdate_FirstRun = false;
        }  
    }
    
}