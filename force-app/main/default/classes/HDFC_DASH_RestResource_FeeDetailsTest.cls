/**
className: HDFC_DASH_RestResource_FeeDetailsTest
DevelopedBy: Sai Shruthi
Date: 12 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_FeeDetails
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_RestResource_FeeDetailsTest {
    
	private static final string SUCCESS ='Success';
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    
    //request and response body for testing
    private static final string REQUEST_BODY ='{"sfApplicationId":"'+ app.id +'","sfApplicationNumber":"'+ app.HDFC_DASH_ApplicationNumber__c +'","applicationProcessingFee":"5000","taxes":"900","grossTotal":"5900","discount":"3000","netApplicationFee":"2900","payNow":"1900","payableLater":"4720","paymentTermsAccepted":"Yes","paymentTermsContent":"terms accepted","paymentTermsAcceptedDate":null,"leadStageNumber":"6","leadStageDescription":"description","paymentGateway":{"merchantId":"1234","customerId":"4354354","txnReferenceNo":"3534534","bankReferenceNo":"765756","txnAmount":"5677","txnDate":"2021-12-24","authStatus":"345435","errorStatus":"tno error","errorDescription":"44564","checksum":"65656"},"feeConstruct":{"pfExclTax":"5000","pfInclTax":"5560","stampDuty":"3660","sfExclTax":"3660","sfInclTax":"3660","minFeesExclTax":"1900","minFeesInclTax":"3660","feeosExclTax":"3660","feeosInclTax":"3660","subventAmt":"3660","payableLater":"3660","totFeeExclTax":"5000","totFeeInclTax":"5560"}}';
    private static final string STRING_URL = '/services/apexrest/hdfc/feeDetails/V1.0';

    /* 
    Method Name :testhandleFeeDetailsRequest
    Parameters  :
    Description :This method would call HDFC_DASH_RestResource_FeeDetails.handleFeeDetailsRequest().
    */
    static TestMethod void testhandleFeeDetailsRequest() 
    {        
	    HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = STRING_URL;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
        request.requestBody = Blob.valueOf(REQUEST_BODY);
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        RestContext.request = request;
        RestContext.response = response;
        
        System.runAs(sysAdmin)
        {
            test.startTest();
            HDFC_DASH_RestResource_FeeDetails.handleFeeDetailsRequest();
        
            test.stopTest();
            System.assertNotEquals(null, response,SUCCESS);
        }
    }
      
    /*Method Name: testException
    parameters: NONE
    Method Description: This method would check if the exception is thrown when the request headers are not sent correctly.
    */
    static TestMethod void testException() 
    {
         User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);  
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        System.runAs(sysAdmin)
        {
            RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            request.requestUri = STRING_URL;
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
            request = HDFC_DASH_TestDataFactory_API.assignPartialHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY);
            RestContext.request = request;
            RestContext.response = response;
            test.startTest();
                	HDFC_DASH_RestResource_FeeDetails.handleFeeDetailsRequest();                 	
            test.stopTest();
            system.assertEquals(HDFC_DASH_Constants.INT_FIVE_HUNDRED, response.statusCode,SUCCESS);
        }
    }
}