/*
Class: HDFC_DASH_PostApplicationFormCalloutTest
Author: Sai Suman
Date: 11 Dec 2021
Company: Accenture
Description: This is the test class for Queueable Class to make PostApplicationForm Callout.
*/
@isTest (SeeAllData=false)
public class HDFC_DASH_PostApplicationFormCalloutTest {
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);

    /* 
  Method Name: testEPFOCallout
  parameter:none
  Method Description: tests the functionality of EPFO callout
  */ 
    static testmethod void testEPFOCallout(){
        System.runAs(sysAdmin)
        {             
        ID jobID=null;
       	HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        HDFC_DASH_TestDataFactory_API.createInterfaceSetting_PostApplicationForm();
        Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockPostApplicationFormResGen());
        HDFC_DASH_PostApplicationFormCallout appFormCallOut  = new HDFC_DASH_PostApplicationFormCallout(app.Id);
		Test.startTest();
        jobID = System.enqueueJob(appFormCallOut);
        Test.stopTest();  
        System.assertNotEquals(null,jobID);
        }
    }
    
    /*
  Method Name: testEPFOCalloutException
  Method Description: This method will test the exception scenario for EPFO callout
*/
    static testmethod void testEPFOCalloutException(){
        System.runAs(sysAdmin)
        {
        //Database.update(appDetails);
        //Database.update(applEmpDetail);
        ID jobID=null;
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        HDFC_DASH_TestDataFactory_API.createInterfaceSettingPollingPaymentStatus();
        Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockPostApplicationFormResGen());
        HDFC_DASH_PostApplicationFormCallout appFormCallOut  = new HDFC_DASH_PostApplicationFormCallout(app.Id);
		Test.startTest();
        jobID = System.enqueueJob(appFormCallOut);
        Test.stopTest();  
        System.assertNotEquals(null,jobID);
        }
    }
}