/**
className: HDFC_DASH_Event_SelectorTest
DevelopedBy: Kumar Gourav
Date: 28 Sep 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_Event_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_Event_SelectorTest {
    
//Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Id sysAdminId = UserInfo.getUserId();
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static Event event = HDFC_DASH_TestDataFactory_Event.getBasicEvent(app.id,con.id,sysAdminId);
    private static final string SUCCESS ='Success';
    
     /* 
    Method Name :testGetEvent
    Description :This method will test the method getEvent().
    */
    static testmethod void testGetEvent(){
        List<Event> resEventRec = new List<Event>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Test.startTest();   
            resEventRec = HDFC_DASH_Event_Selector.getEvent(new List<String> {event.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, resEventRec);
            system.assertEquals(1,resEventRec.size());  
        }
    }
    /* 
    Method Name :testGetEventBasedOnQuery
    Description :This method will test the method getEventBasedOnQuery().
    */
   /* static testmethod void testGetEventBasedOnQuery(){
        List<Event> listEventRec  = new List<Event>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.ID_STRING =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                event.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listEventRec = HDFC_DASH_Event_Selector.getEventBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listEventRec);
        system.assertEquals(1,listEventRec.size(),SUCCESS);       
      } */
     /* 
    Method Name :testGetEventBasedOnLimit
    Description :This method will test the method getEventBasedOnLimit().
    */
     static testmethod void testGetEventBasedOnLimit(){
        List<Event> listEventRec  = new List<Event>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.ID_STRING =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                event.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listEventRec = HDFC_DASH_Event_Selector.getEventBasedOnLimit(fieldList,condition,HDFC_DASH_Constants.INT_THREE);
            Test.stopTest();
        }
        system.assertNotEquals( null, listEventRec);
        system.assertEquals(1,listEventRec.size(),SUCCESS);       
      }
    /* 
    Method Name :testWOGetEvent
    Description :This method will test the method getEvent().
    */
    static testmethod void testWOGetEvent(){
        List<Event> resEventRec = new List<Event>(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Test.startTest();   
            resEventRec = HDFC_DASH_Event_WOSharing_Selector.getEvent(new List<String> {event.Id}, fieldList, HDFC_DASH_Constants.ID_STRING);       
            Test.stopTest();
            system.assertNotEquals( null, resEventRec);
            system.assertEquals(1,resEventRec.size());  
        }
    }
      /* 
    Method Name :testWOGetEventBasedOnLimit
    Description :This method will test the method getEventBasedOnLimit().
    */
     static testmethod void testWOGetEventBasedOnLimit(){
        List<Event> listEventRec  = new List<Event>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_WHOID};
            Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.ID_STRING =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                event.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listEventRec = HDFC_DASH_Event_WOSharing_Selector.getEventBasedOnLimit(fieldList,condition,HDFC_DASH_Constants.INT_THREE);
            Test.stopTest();
        }
        system.assertNotEquals( null, listEventRec);
        system.assertEquals(1,listEventRec.size(),SUCCESS);       
      }
}