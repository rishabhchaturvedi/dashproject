/* ================================================
 * @Class Name :  IT_InterfaceManualRetryController
 * @author : Accenture
 * @Purpose: This purpose of this class is to handle doManualRetry method InterfaceCallOutProcess.
 * @created date:
 ================================================*/
public with sharing class IT_InterfaceManualRetryController {
    public boolean isSuccess {get;set;}
    public boolean isError {get;set;}
    public boolean isWarning {get;set;}
    public string recordId {get;set;}

    /***************************************************************************************************************
    *   @Name        :  IT_InterfaceManualRetryController                                                              
    *   @Parameters  :  ApexPages.StandardController stdController                                                                           
    *   @Description :  It is a constructor for class IT_InterfaceManualRetryController.                             
    ********************************************************************of*******************************************/
    public IT_InterfaceManualRetryController(ApexPages.StandardController stdController){
        
        isSuccess =false;
        isError =false;
        isWarning =false;
        
    } 
    /***************************************************************************************************************
    *   @Name        :  onLoad                                                              
    *   @Return      :  void                                                                         
    *   @Description :   This method onLoad() is used to call doManualRetry method of InterfaceCallOutProcess.                      
    ***************************************************************************************************************/
    public void onLoad(){
        //try{
            Boolean errorFlag = false;
            recordId =  ApexPages.currentPage().getParameters().get('id');
            if(string.isnotblank(recordId)){
                list<Integration_Message_Log__c> lstCallOut = [select Header_Map__c,Post_Retry_Process__c,Method_Type__c,id,Name,Do_Retry__c,
                                                                      Number_Of_Retries__c,Status_Code__c,Callout_Date__c,Request_Body__c,
                                                                      Interface_Name__c,Error_Description__c
                                                                      from Integration_Message_Log__c where id =: recordId];
                if(lstCallOut.size()>0 ){
                        InterfaceCallOutProcess calloutProcessObj = new InterfaceCallOutProcess();
                        errorFlag = calloutProcessObj.doManualRetry(lstCallOut[0]); 
                        if(!errorFlag){
                            isSuccess = true;
                        }else{
                            isError = true;
                        }               
                }else{
                    isError = true;
                }
            }
        /*}
        catch(exception ex){
            system.debug(ex.getmessage()+'linenumber'+ex.getLineNumber());
            isError = true;
        }*/
    }
}