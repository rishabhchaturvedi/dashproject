/**
* 	className: HDFC_DASH_SetupObjects_Selector
DevelopedBy: Tejeswari
Date: 19 November 2021
Company: Accenture
Class Description: This class would create the select queries for setup objects.
**/
public with sharing class HDFC_DASH_SetupObjects_Selector {
    private static final string FROM_STRING = ' from ';
    private static final string OBJECT_Profile = 'Profile ';
    private static final string OBJECT_PermissionSet = 'PermissionSet ';
    private static final string OBJECT_USERROLE = 'UserRole ';
    private static final string OBJECT_GROUP = 'Group ';
    
    private static final string ID_STRING = 'ID ';
    private static final string SELECT_STRING = 'Select ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    public static final string  QUERY_LIMIT = ' Limit';
    
    /* 
Method Name :getProfile
Parameters  : List<string> fieldList, String profileName
Description :This method would get the Profile based on the condition.
*/
    public static Profile getProfile(List<string> fieldList, String profileName)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_Profile + WHERE_STRING+ HDFC_DASH_CONSTANTS.NAME+ HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ profileName +HDFC_DASH_Constants.STRING_QUOTE;
        Profile Profile = database.query(querystring);  
        
        return Profile;
        
    } 
    /* 
Method Name :getPermissionSet
Parameters  : List<string> fieldList, String permissionSetName
Description :This method would get the permissionSet based on the condition.
*/
    public static PermissionSet getPermissionSet(List<string> fieldList, String permissionSetName)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_PermissionSet + WHERE_STRING+ HDFC_DASH_CONSTANTS.NAME+ HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ permissionSetName +HDFC_DASH_Constants.STRING_QUOTE;
        PermissionSet permissionSet = database.query(querystring);          
        return permissionSet;
    } 
    /* 
Method Name :getUserRole
Parameters  : List<string> fieldList, String roleName
Description :This method would get the UserRole based on the condition.
*/
    public static UserRole getUserRole(List<string> fieldList, String roleName) 
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_USERROLE + WHERE_STRING+ HDFC_DASH_CONSTANTS.NAME+ HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ roleName +HDFC_DASH_Constants.STRING_QUOTE;
        UserRole role = database.query(querystring);  
        return role;
    }
    /* 
Method Name :getUserRoles
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get a list of UserRoles based on one condition.
*/   
    public static List<UserRole> getUserRoles(List<String> queryParameters,List<string> fieldList, String conditionOn){
        
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_USERROLE + WHERE_STRING + conditionOn + QUERY_PARAMETER;
        // system.debug('Rolequerystring'+querystring);
        List<UserRole> listUserRole = database.query(querystring); 
        //system.debug('Rolequerystring'+querystring);
        return listUserRole;
    }
    /* 
Method Name :getPublicGroup
Parameters  : List<string> fieldList, String groupName
Description :This method would get the Group based on the condition.
*/
    public static Group getPublicGroup(List<string> fieldList, String groupName)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_STRING + OBJECT_GROUP + WHERE_STRING+ HDFC_DASH_CONSTANTS.NAME+ HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ groupName +HDFC_DASH_Constants.STRING_QUOTE;
        Group groupRec = database.query(querystring);          
        return groupRec;
    }
}