/*
Class:HDFC_DASH_RuleEngineReq_ILPSCallout
Author: Kumar Gourav
Date: 4 Dec 2021
Company: Accenture
Description: This class will generate the request body for Rule Engine to ILPS 
*/
public with sharing class HDFC_DASH_RuleEngineReq_ILPSCallout {
    
    public List<ruleEngineCapture> ruleEngineCapture;
    /*
Class: ruleEngineCapture
Description: Class to parse Rule Engine Details
*/
    public with sharing class ruleEngineCapture {
        public String sfApplicationID;
        public String sfApplicationNumber;
        public String fileNumber;
        public String sfApplicantID;
        public String customerNumber;
        public String ruleId;
        public String stage;
        public Boolean docRuleEngineOutcome;
        public Boolean confidenceRuleEngineOutcome;
        public Boolean customerResolved;
        public String customerAction;
        public Boolean customerSkipped;
        public String docCode;
        public String docName;
        public String subDetails;
    }
    
}