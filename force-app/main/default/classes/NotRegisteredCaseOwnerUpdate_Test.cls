@isTest
public class NotRegisteredCaseOwnerUpdate_Test {

    	@isTest
    public static void testMethod1(){
        Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
        Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
        app.HDFC_DASH_File_Number__c = app.HDFC_DASH_File_Number__c.left(9);
        app.HDFC_DASH_LMS_Place_Of_Service__c = 'NOIDA';
        app.HDFC_DASH_LMS_Origin_Branch__c = 'NEW DELHI';
        app.HDFC_DASH_Loan_Type__c = 'Loan';
        update app;
        
        Case c = new Case(
        	Status = 'Not Registered',
            HDFC_DASH_File_Number__c = app.Id            
        );
        
        Test.startTest();
        insert c;
        Id jobId = database.executeBatch(new NotRegisteredCaseOwnerUpdate());
        Test.stopTest();
    }
}