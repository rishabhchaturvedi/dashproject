public class EntitlementRecordCreation implements Database.Batchable <sObject>,Database.Stateful {
	
    public  Id businessHoursId;
    public  Id entitlementProc;
    
    public EntitlementRecordCreation(){
    	businessHoursId = [select Id from BusinessHours where Name = 'India'].Id;
        entitlementProc = [select Id,Name from slaProcess limit 1].Id;
        system.debug('business hours constructor...'+businessHoursId);
        system.debug('entitlement proc constructor...'+entitlementProc);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        String personAccRecType = 'Person Account';
        String hdfcRecType = 'HDFC Record Type';
        String query = 'Select Id,Name from Account where RecordType.Name =: personAccRecType or RecordType.Name =: hdfcRecType';
        system.debug('query...'+query);
        
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc,List<Account> scope){
        List<Entitlement> entitlementList = new List<Entitlement>();
        
        system.debug('business hours execute...'+businessHoursId);
        system.debug('entitlement proc execute...'+entitlementProc);
        system.debug('scope size...'+scope.size());
        
        for(Account a : scope){
            Entitlement e = new Entitlement(
            	BusinessHoursId = businessHoursId,
                AccountId = a.Id,
                Name = a.Name,
                SlaProcessId = entitlementProc,
                StartDate = Date.today(),                
                Type = 'Phone Support'
            );
            entitlementList.add(e);
            
        }
        
        try{
            if(entitlementList.size() > 0){}
            Database.insert(entitlementList);
        }catch(Exception e){
            system.debug('exception...'+e);
        }
        
        
    }
    
    public void finish(Database.BatchableContext bc){
        system.debug('business hours finish...'+businessHoursId);
        system.debug('entitlement proc finish...'+entitlementProc);
    }
}