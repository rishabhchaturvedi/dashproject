/**
className: HDFC_DASH_Application_SelectorTest
DevelopedBy: Punam Marbate
Date: 06 July 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Application_Selector class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_Application_SelectorTest {

    private static final string STRING_ID='Id';
    private static final string STRING_ACCOUNTID='AccountId'; 
    private static final string STRING_CLOSEDATE= 'CloseDate';
    private static final string STRING_STAGENAME= 'StageName';
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static final string SUCCESS ='Success';
     /* 
    Method Name :testGetApplicationBasedOnId
    Description :This method will test the getApplicationBasedOnId.
    */
    static testmethod void testGetApplicationBasedOnId(){
        
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        Opportunity resultOppRecord = new Opportunity();
        Opportunity oppApplicationRecord = new Opportunity();
        System.runAs(sysAdmin){
            //Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
           
            oppApplicationRecord = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id); 
            
            List<String> fieldList = new List<string>{STRING_ID,STRING_ACCOUNTID,STRING_CLOSEDATE,STRING_STAGENAME};
            Test.startTest();
                resultOppRecord =  HDFC_DASH_Application_Selector.getApplicationBasedOnId(fieldList, oppApplicationRecord.Id);  
        	Test.stopTest();
        }
        system.assertNotEquals( null, resultOppRecord);
        system.assertNotEquals( null, oppApplicationRecord);
        system.assertEquals(oppApplicationRecord.Id,resultOppRecord.Id); 
        
    }  
     /* 
    Method Name :testGetApplication
    Description :This method will test the getApplication.
    */
    static testmethod void testGetApplication(){     
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        List<Opportunity> resultOppList = new List<Opportunity>();
        Opportunity oppApplicationRecord = new Opportunity();
        System.runAs(sysAdmin){
            //Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
          
            oppApplicationRecord = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
           
            List<string> listAppID = new List<string>();
            listAppID.add(oppApplicationRecord.Id);
            List<String> fieldList = new List<string>{STRING_ID,STRING_ACCOUNTID,STRING_CLOSEDATE,STRING_STAGENAME};
            Test.startTest();
                resultOppList = HDFC_DASH_Application_Selector.getApplication(listAppID, fieldList, HDFC_DASH_Constants.ID_STRING);
            Test.stopTest();
        }
        system.assertNotEquals( null, resultOppList);
        system.assertNotEquals( null, oppApplicationRecord);
        system.assertEquals(oppApplicationRecord.Id,resultOppList[0].Id );         
}
     /* 
    Method Name :testGetApplicationBasedOnQuery
    Description :This method will test the method getApplicationBasedOnQuery().
    */
    static testmethod void testGetApplicationBasedOnQuery(){
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        List<Opportunity> listApplicationRec  = new List<Opportunity>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{STRING_ID,STRING_ACCOUNTID,STRING_CLOSEDATE,STRING_STAGENAME};
            Map<String,String> condition= new Map<string,String>{STRING_ID =>
                HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
                app.Id+HDFC_DASH_Constants.STRING_QUOTE};
            Test.startTest();
            listApplicationRec = HDFC_DASH_Application_Selector.getApplicationBasedOnQuery(fieldList,condition);
            Test.stopTest();
        }
        system.assertNotEquals( null, listApplicationRec);
        system.assertEquals(1,listApplicationRec.size(),SUCCESS);       
      }

    /* 
    Method Name :testGetApplicationBasedOnQuery
    Description :This method will test the method getAppRecsOrderBy().
    */
    static testmethod void testGetAppRecsOrderBy(){
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        List<Opportunity> listApplicationRec  = new List<Opportunity>();
        System.runAs(sysAdmin){     
            List<String> fieldList = new List<string>{STRING_ID,STRING_ACCOUNTID,STRING_CLOSEDATE,STRING_STAGENAME};
            List<String> queryId = new List<string>();
            queryId.add(app.Id);
            Test.startTest();
            listApplicationRec = HDFC_DASH_Application_Selector.getAppRecsOrderBy(fieldList,queryId);
            Test.stopTest();
        }
        system.assertNotEquals( null, listApplicationRec);
        system.assertEquals(1,listApplicationRec.size(),SUCCESS);       
      }
}