/**
className: HDFC_DASH_InfoValTriggerOpsTest
DevelopedBy: Tejeswari
Date: 11 Nov 2021
Company: Accenture
Class Description: test class for HDFC_DASH_InfoValTriggerOps.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_InfoValTriggerOpsTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id); 
    private static Opportunity app1 = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id); 
    private static HDFC_DASH_Applicant_Details__c appDetails = 
        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static ContentVersion contentVer = HDFC_DASH_TestDataFactory_ContentDoc.getContestVersion();
   private static String contenDocId = [select id, ContentDocumentId from ContentVersion where Id=: contentVer.id].ContentDocumentId;
    private static final string STORAGE_IDENTIFIER= '4345664_34434_3243.pdf';
    private static final string FILE_NUMBER= '7373675';
    /*
Method Name :testInsertInfoValRec
Description :This method would test the trigger for Insert infoVal records.
*/
    @IsTest
    public static void testInsertInfoValRec()
    {
        System.runAs(sysAdmin){
            
            
            List<HDFC_DASH_Info_Validation__c> listInfoVal = new List<HDFC_DASH_Info_Validation__c>();
            
            HDFC_DASH_Info_Validation__c infoValRec1 = HDFC_DASH_TestDataFactory_AppInfoValid.createBasicInfoValWithApplication(app.Id);
            infoValRec1.HDFC_DASH_Applicant_Details__c = appDetails.id;
            listInfoVal.add(infoValRec1);
            
            HDFC_DASH_Info_Validation__c infoValRec2 = HDFC_DASH_TestDataFactory_AppInfoValid.createBasicInfoValWithApplication(app1.Id);
            infoValRec2.HDFC_DASH_Applicant_Details__c = appDetails.id;
            listInfoVal.add(infoValRec2);
            
            Test.startTest();
            Database.insert(listInfoVal);
            Test.stopTest();
            
            HDFC_DASH_Info_Validation__c infoValRecupdated1 = [select id,HDFC_DASH_Latest__c from HDFC_DASH_Info_Validation__c where id =: listInfoVal[0].id];
            system.assertEquals(HDFC_DASH_Constants.STRING_YES,infoValRecupdated1.HDFC_DASH_Latest__c);
            
            
        }
    }
    /*
Method Name :testupdateInfoValRecords
Description :This method would test the trigger for Update infoVal records.
*/
    @IsTest
    public static void testupdateInfoValRecords()
    {
        System.runAs(sysAdmin){
            List<HDFC_DASH_Info_Validation__c> listInfoVal = new List<HDFC_DASH_Info_Validation__c>();
            HDFC_DASH_Info_Validation__c infoValRec3 = HDFC_DASH_TestDataFactory_AppInfoValid.createBasicInfoValWithApplication(app.Id);
            listInfoVal.add(infoValRec3);
            
            HDFC_DASH_Info_Validation__c infoValRec4 = HDFC_DASH_TestDataFactory_AppInfoValid.createBasicInfoValWithApplication(app1.Id);
            infoValRec4.HDFC_DASH_Applicant_Details__c = appDetails.id;
            listInfoVal.add(infoValRec4);
            
            Database.insert(listInfoVal);
            HDFC_DASH_Info_Validation__c infoValRecupdated1 = [select id,HDFC_DASH_Latest__c from HDFC_DASH_Info_Validation__c where id =: listInfoVal[0].id];
            infoValRecupdated1.HDFC_DASH_Applicant_Details__c = appDetails.id;
            
            Test.startTest();
            Database.update(infoValRecupdated1);
            Test.stopTest();
            system.assertEquals(HDFC_DASH_Constants.STRING_YES,infoValRecupdated1.HDFC_DASH_Latest__c);
        }
    }  
    
    /*
Method Name :testFilternfoValforConDocDeletion
Description :This method would test the trigger for Deletion of Content Document records.
*/
    @IsTest
    public static void testFilternfoValforConDocDeletion()
    { 
        System.runAs(sysAdmin) {
            HDFC_DASH_Info_Validation__c infoValApp = HDFC_DASH_TestDataFactory_AppInfoValid.createBasicInfoValWithApplication(app.Id);
            infoValApp.HDFC_DASH_Storage_Identifier__c= HDFC_DASH_Constants.STRING_BLANK;
            infoValApp.HDFC_DASH_File_Number__c = FILE_NUMBER;
            infoValApp.HDFC_DASH_Sync_Document_To_ILPS__c = true;
            Database.insert(infoValApp);
          ContentDocumentLink conDocLink = HDFC_DASH_TestDataFactory_ContentDoc.getConDocLink(contenDocId,infoValApp.id);
               HDFC_DASH_Info_Validation__c infoValRecupd = [select id,HDFC_DASH_Storage_Identifier__c,HDFC_DASH_Content_Document_Id__c from HDFC_DASH_Info_Validation__c where id =: infoValApp.id];
            infoValRecupd.HDFC_DASH_Storage_Identifier__c = STORAGE_IDENTIFIER;
           Test.startTest();
          Database.update(infoValRecupd);
            Test.stopTest();
            
        }
    }
    
}