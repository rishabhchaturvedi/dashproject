/*
Class: HDFC_DASH_SpotOfferFile_ILPSCalloutTest
Author: Anisha Arumugam
Date: 22 July 2021
Company: Accenture
Description:This is the test class for Spot Offer File ILPS Callout.
*/
@isTest(SeeAllData=false)
private with sharing class HDFC_DASH_SpotOfferFile_ILPSCalloutTest 
{    
    private static final string SUCCESS ='Success';
    private static final string CUSTNO ='123454321';  
    private static final string UNIQ_CUSTNO = '678901234';
    private static List<string> listFields_Acc = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_Customer_Number}; 
        private static List<string> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_File_Number, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber}; 
            private static List<string> listFields_AppDet = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ILPS_Customer_Number,HDFC_DASH_Constants.APPDETAIL_CONTACT_R_ACCOUNT_CUSTOMER_NUMBER};       
                //Getting the object records
                private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c empAppDetail = HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.id,con.id);
    private static Account newAcc = HDFC_DASH_Account_Selector.getAccountsBasedOnId(listFields_Acc, acc.id);
    private static HDFC_DASH_Applicant_Details__c newAppDet = HDFC_DASH_Applicant_Detail_Selector.getApplicantDetailBasedOnId(listFields_AppDet,appDetails.id);
    private static Opportunity newApplication = HDFC_DASH_Application_Selector.getApplicationBasedOnId(listOfFields_App, app.id);
    private static final string requestBody ='{"sfApplicationId":"'+ app.id +'","sfApplicationNumber":"'+ app.HDFC_DASH_ApplicationNumber__c +'","applicationProcessingFee":"5000","taxes":"900","grossTotal":"5900","discount":"3000","netApplicationFee":"2900","payNow":"1900","payableLater":"4720","paymentTermsAccepted":"Yes","paymentTermsContent":"terms accepted","paymentTermsAcceptedDate":null,"leadStageNumber":"6","leadStageDescription":"description","paymentGateway":{"merchantId":"1234","customerId":"4354354","txnReferenceNo":"3534534","bankReferenceNo":"765756","txnAmount":"5677","txnDate":"2021-12-24","authStatus":"0300","errorStatus":"tno error","errorDescription":"44564","checksum":"65656"},"feeConstruct":{"pfExclTax":"5000","pfInclTax":"5560","stampDuty":"3660","sfExclTax":"3660","sfInclTax":"3660","minFeesExclTax":"1900","minFeesInclTax":"3660","feeosExclTax":"3660","feeosInclTax":"3660","subventAmt":"3660","payableLater":"3660","totFeeExclTax":"5000","totFeeInclTax":"5560"}}';   
    
    /*
Method: testSpotOfferFile_ILPSCallout
Description: This method will test the HDFC_DASH_SpotOfferFile_ILPSCallout queueable class
*/
    static testmethod void  testSpotOfferFile_ILPSCallout()
    {   
        System.runAs(sysAdmin)
        { 
            RestRequest request = new RestRequest();   
            RestResponse response = new RestResponse();
            
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockSpotOfferResGenerator());
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_TestDataFactory_API.createInterfaceSetting_SO_ILPS();
            
            request.requestBody = Blob.valueOf(requestBody);
            HDFC_DASH_ParseFeeDetails feeDetails = HDFC_DASH_ParseFeeDetails.parse(request);
            
            Test.startTest();            
            HDFC_DASH_APIResponse_FeeDetails apiRes = new HDFC_DASH_APIResponse_FeeDetails();
            HDFC_DASH_SpotOfferFile_ILPSCallout soILPSCallout = new HDFC_DASH_SpotOfferFile_ILPSCallout();
            apiRes = soILPSCallout.doSpotOfferFileCallout(feeDetails, apiRes);
            Test.stopTest();  
            System.assertNotEquals(null, apiRes.fileNo);          
        }        
    } 
}