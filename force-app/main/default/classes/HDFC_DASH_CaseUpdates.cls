public class HDFC_DASH_CaseUpdates {
	
    @future
    public static void updateCaseWithNewOwner(Map<Id,Id> userIds){
        List<Case> casesToBeUpdated = new List<Case>();
        List<Case> caseList = [select Id,OwnerId from Case where (Status = 'New' or Status = 'Pending') and OwnerId in :userIds.keySet()];
        for(Case caseRec : caseList){
            caseRec.OwnerId = (userIds.get(caseRec.OwnerId) != null ? userIds.get(caseRec.OwnerId) : caseRec.OwnerId);
            casesToBeUpdated.add(caseRec);
        }
        if(casesToBeUpdated.size()>0){
            update casesToBeUpdated;
        }
        
         //Update all Tasks
        List<Task> tasksToBeUpdated = new List<Task>();
        List<Task> taskList = [select Id,OwnerId from Task where Status != 'Completed' and OwnerId in :userIds.keySet()];
        for(Task taskRec : taskList){
            taskRec.OwnerId = (userIds.get(taskRec.OwnerId) != null ? userIds.get(taskRec.OwnerId) : taskRec.OwnerId);
            tasksToBeUpdated.add(taskRec);
        }
        if(tasksToBeUpdated.size()>0)
            update tasksToBeUpdated;
    }
}