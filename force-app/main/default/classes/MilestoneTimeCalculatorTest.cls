@isTest 
private class MilestoneTimeCalculatorTest {

     static testMethod void testMilestoneTimeCalculator() {
     
         MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];      
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        
        // Create case data.
        // Typically, the milestone type is related to the case, 
        // but for simplicity, the case is created separately for this test.
        Case c = new Case(priority = 'High');
        c.HDFC_DASH_Remarks__c = ' Testing milestone';
        c.HDFC_DASH_Case_Type__c='Complaint';
        insert c;
        
        MilestoneTimeCalculator calculator = new MilestoneTimeCalculator();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
        
        
            //System.assertEquals(actualTriggerTime, 5);
                
        /*c.priority = 'Low';
        update c;
        actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
        System.assertEquals(actualTriggerTime, 18);*/
     }

	static testMethod void testMilestoneTimeCalculator1() {
     
         MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType];      
       
        MilestoneType mt = mtLst[1];
        
        // Create case data.
        // Typically, the milestone type is related to the case, 
        // but for simplicity, the case is created separately for this test.
        Case c = new Case(priority = 'High');
        c.HDFC_DASH_Remarks__c = ' Testing milestone';
        c.HDFC_DASH_Case_Type__c='Complaint';
        insert c;
        
        MilestoneTimeCalculator calculator = new MilestoneTimeCalculator();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
       
     }



}