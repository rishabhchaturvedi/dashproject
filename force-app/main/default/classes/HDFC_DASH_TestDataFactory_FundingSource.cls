/* 
ClassName: HDFC_DASH_TestDataFactory_FundingSource
DevelopedBy: Janani Mohankumar
Date: 22 Sep 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Funding Source object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_FundingSource {
   /* Method Name: createBasicFundingSource
    parameters: ID applID
    Method Description: This method is to create test data for HDFC_DASH_Funding_Source__c. 
    */
    public static HDFC_DASH_Funding_Source__c createBasicFundingSource(ID applID)
    {
        HDFC_DASH_Funding_Source__c fundingSource = new HDFC_DASH_Funding_Source__c();
        fundingSource.HDFC_DASH_Application__c =  applID  ;       
        return fundingSource;        
    }
    
    /* Method Name: getBasicFundingSource
    parameters: ID applID
    Method Description: This method is to generate test data for Funding Source.
    */
    public static HDFC_DASH_Funding_Source__c getBasicFundingSource(ID applID)
    {
        HDFC_DASH_Funding_Source__c fundingSource = HDFC_DASH_TestDataFactory_FundingSource.createBasicFundingSource(applID);
        if(Schema.sObjectType.HDFC_DASH_Funding_Source__c.isCreateable()){
            Database.insert(fundingSource);
        }
        return fundingSource;
    } 
}