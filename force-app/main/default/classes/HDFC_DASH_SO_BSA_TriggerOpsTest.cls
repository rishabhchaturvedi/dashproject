/**
className: HDFC_DASH_SO_BSA_TriggerOpsTest 
DevelopedBy: Tejeswari
Date: 19 November 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_SO_BSA_TriggerOps class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_SO_BSA_TriggerOpsTest 
{
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static User userRec = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string Agency_Code = '12345';
    private static final string Federation_Identifier = '1357989';
    
    /* 
Method Name :testfilterSoBsaWithAgencyAndFederation
Description :This method would test the method in trigger helper for the list of HDFC_DASH_SO_BSA_Association__c for which have the related fields.
*/
    @isTest 
    public static void testfilterSoBsaWithAgencyAndFederation(){
        Exception testException;
        HDFC_DASH_SO_BSA_Association__c SoBsaRec = HDFC_DASH_TestDataFactory_SO_BSA.createBasicSoBsa(acc.id);
        SoBsaRec.HDFC_DASH_Agency_Code__c = Agency_Code;
        SoBsaRec.HDFC_DASH_Federation_Identifier__c = Federation_Identifier;
        System.runAs(sysAdmin) {
            Test.startTest();
            
            try{
                Database.insert(SoBsaRec);   
            }
            catch(Exception exp){
                testException = exp;
            }
            Test.stopTest();
            HDFC_DASH_SO_BSA_Association__c soBsaRecord = [Select id, HDFC_DASH_BSA__c, HDFC_DASH_Sales_Officer__c from HDFC_DASH_SO_BSA_Association__c where id=: SoBsaRec.id];
            System.assertNotEquals(null, soBsaRecord.HDFC_DASH_BSA__c);
            System.assertNotEquals(null, soBsaRecord.HDFC_DASH_Sales_Officer__c);
        }
    }
}