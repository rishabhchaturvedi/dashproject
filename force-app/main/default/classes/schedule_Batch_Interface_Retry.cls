/*@author        
Accenture 
@date           23/10/2017
@name           schedule_Batch_Interface_Retry 
@description    The purpose of this class is to schedule Batch_Interface_Retry
*/
global class schedule_Batch_Interface_Retry implements Schedulable {
   /***************************************************************************************************************
    *   @Name        :  execute                                                             
    *   @Return      :  void                                                                                   
    *   @Description :  To execute batch retry process                      
    ***************************************************************************************************************/
   global void execute(SchedulableContext sc) {
      Batch_Interface_Retry b = new Batch_Interface_Retry(); 
      database.executebatch(b);
   }
}