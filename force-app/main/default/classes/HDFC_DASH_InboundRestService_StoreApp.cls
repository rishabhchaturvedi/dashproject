/**
className: HDFC_DASH_InboundRestService_StoreApp
DevelopedBy: Sai Shruthi
Date: 25 Aug 2021
Company: Accenture
Class Description: This class would get the application details and related records as request and storing it in salesforce.
**/
public inherited sharing class HDFC_DASH_InboundRestService_StoreApp { 

    public static Lead lead = new Lead();
    public static Boolean isNewApplication = false;
    public static Boolean isUpdatePrimaryApplicant = false;
    public static Boolean isExistingCoApplicant = false;
    public static String applicationId;
    public static Boolean byPassEditFlags = false;
    public static map<string, List<HDFC_DASH_Applicant_Details__c>> coApplicantMap = new map<string, List<HDFC_DASH_Applicant_Details__c>>();
    public static List<HDFC_DASH_Applicant_Details__c> matchingApplicants = new List<HDFC_DASH_Applicant_Details__c>();
    public static List<HDFC_DASH_Applicant_Details__c> nonMatchingApplicants = new List<HDFC_DASH_Applicant_Details__c>();
    public static List<HDFC_DASH_Applicant_Details__c> newApplicants = new List<HDFC_DASH_Applicant_Details__c>();
    /* 
    Method Name :processApplication
    Parameters  :RestRequest request
    Description :This method would store application.
    */
    public static HDFC_DASH_APIResponse_StoreApp processApplication(RestRequest request){

        //HDFC_DASH_APIResponse_StoreAppWrapper apiRespSToreWrapper = new HDFC_DASH_APIResponse_StoreAppWrapper();
        HDFC_DASH_APIResponse_StoreApp  apiResp = new HDFC_DASH_APIResponse_StoreApp(); 
        //Calling Parser Class
        HDFC_DASH_ParseApplication application = HDFC_DASH_ParseApplication.parse(request);
        
        if(application?.application?.applicationDetails?.sfApplicationId == null){
            lead = HDFC_DASH_InsertApplication.getLead(application);
 
        }
        if(application?.application?.applicationDetails?.preApprovedLoanFlag==null?false:application?.application?.applicationDetails?.preApprovedLoanFlag && 
        application?.application?.applicationDetails?.consentForTermsAndConditionBeforeFeePayment==null?false:application?.application?.applicationDetails?.consentForTermsAndConditionBeforeFeePayment)
        {
            system.debug('Inside preApproved Loan Part');
            //Send application to ILPS
            system.debug('Application Id--'+application);
            //we are going to send storeApp Request in case of preApproved Loan flag
            HDFC_DASH_ILPS_FilePush_Callout.calloutToILPSForPreApprovedLoan(application);//this call will be synchronous here
        }
        //method to update application details by checking each flag for each node.
        checkForFlags(application);

        matchingApplicants= coApplicantMap.get(HDFC_DASH_Constants.HDFC_DASH_Matching);
        nonMatchingApplicants=coApplicantMap.get(HDFC_DASH_Constants.HDFC_DASH_NonMatching);
        newApplicants=coApplicantMap.get(HDFC_DASH_Constants.HDFC_DASH_NewApplicant);

        if((matchingApplicants != null && matchingApplicants.size()>0) || (nonMatchingApplicants!= null && nonMatchingApplicants.size()>0) || (newApplicants != null && newApplicants.size()>0)){
            apiResp = generateAPIResponseExistingCoApp();

        }

        if(isUpdatePrimaryApplicant){
            HDFC_DASH_APIResponse_StoreApp.Application  apiRespApplication = new HDFC_DASH_APIResponse_StoreApp.Application(); 
            apiRespApplication.applicationId = applicationId;
            apiResp.application = apiRespApplication;
        }
        apiResp.successMessage = HDFC_DASH_Constants.STOREAPP_SUCCESSMESSAGE; 
        
        
        
        apiResp.applicationDetails = HDFC_DASH_InboundRestService_GetApp.getApplicationFields(applicationId);
        //apiRespSToreWrapper.apiResp = apiResp;
        
        return apiResp;
    }

    private static HDFC_DASH_APIResponse_StoreApp generateAPIResponseExistingCoApp(){
        HDFC_DASH_APIResponse_StoreApp  apiResp = new HDFC_DASH_APIResponse_StoreApp(); 
        apiResp.coApplicant = new List<HDFC_DASH_APIResponse_StoreApp.CoApplicant>();
        Contact customer;
        Map<Id,Id> contactIds= new Map<Id,Id>();
        List<String> fields = new List<string>{HDFC_DASH_Constants.ID_STRING,
            HDFC_DASH_Constants.STRING_MobilePhone,
            HDFC_DASH_Constants.STRING_Email,HDFC_DASH_Constants.Account_HDFC_DASH_Customer_Number};
        List<HDFC_DASH_Applicant_Details__c> allAppicantsList = new List<HDFC_DASH_Applicant_Details__c>();  
        allAppicantsList.addAll(nonMatchingApplicants);
        allAppicantsList.addAll(matchingApplicants);
        allAppicantsList.addAll(newApplicants);
       
        for(HDFC_DASH_Applicant_Details__c appDetRec : allAppicantsList){
            contactIds.put(appDetRec?.HDFC_DASH_Contact__c, appDetRec.Id);
        }
        List<Id> contactIdsList = new List<Id>(contactIds.keySet());
        List<Contact> listContacts = HDFC_DASH_Contact_Selector.getContactsBasedOnIds(fields, contactIdsList);

        Map<Id,Contact> appDetailContacts= new Map<Id,Contact>();
        for(Contact contact:listContacts){
            appDetailContacts.put(contactIds.get(contact.Id), contact);
        }
        //Response for Matching Coapplicants where detailsMatching = true
        for(HDFC_DASH_Applicant_Details__c appDetRec : matchingApplicants){
            HDFC_DASH_APIResponse_StoreApp.CoApplicant coApp= new HDFC_DASH_APIResponse_StoreApp.CoApplicant();
            coApp.sfApplicantId = appDetRec?.id;
            coApp.sfCustomerId = appDetRec?.HDFC_DASH_Contact__c;
            coApp.sfCustomerNumber = appDetailContacts.get(appDetRec?.id).Account.HDFC_DASH_Customer_Number__c;
            coApp.detailsMatching = true;
            coApp.isExistingCustomer = true;
            apiResp.coApplicant.add(coApp);
            
        }
        //Response for Non Matching Coapplicants where detailsMatching = false
        for(HDFC_DASH_Applicant_Details__c appDetRec : nonMatchingApplicants){
            HDFC_DASH_APIResponse_StoreApp.CoApplicant coApp= new HDFC_DASH_APIResponse_StoreApp.CoApplicant();
            coApp.sfApplicantId = appDetRec?.id;
            coApp.sfCustomerId = appDetRec?.HDFC_DASH_Contact__c;
            coApp.sfCustomerNumber = appDetailContacts.get(appDetRec?.id).Account.HDFC_DASH_Customer_Number__c;
            coApp.customerMobile= appDetailContacts.get(appDetRec?.id).MobilePhone;
            coApp.customerEmail = appDetailContacts.get(appDetRec?.id).Email;
            coApp.detailsMatching = false;
            coApp.isExistingCustomer = true;
            apiResp.coApplicant.add(coApp);
         
        }
        //Response for New Coapplicants where detailsMatching = false and isExistingCustomer = false
        for(HDFC_DASH_Applicant_Details__c appDetRec : newApplicants){
            HDFC_DASH_APIResponse_StoreApp.CoApplicant coApp= new HDFC_DASH_APIResponse_StoreApp.CoApplicant();
            coApp.sfApplicantId = appDetRec?.id;
            coApp.sfCustomerId = appDetRec?.HDFC_DASH_Contact__c;
            coApp.sfCustomerNumber = appDetailContacts.get(appDetRec?.id).Account.HDFC_DASH_Customer_Number__c;
            coApp.detailsMatching = false;
            coApp.isExistingCustomer = false;
            apiResp.coApplicant.add(coApp);
        }

        return apiResp;
    }

    /* 
    Method Name :checkForFlags
    Parameters  :HDFC_DASH_ParseApplication parseApp 
    Description :This method would check for flags and store application.
    */
    public static void checkForFlags(HDFC_DASH_ParseApplication parseApp)
    {
        
        //if(!String.isBlank(parseApp?.application?.applicationDetails?.sfApplicationId))
        //{
        Opportunity appRec = new opportunity();
        appRec.id = parseApp.application?.applicationDetails?.sfApplicationId;
        if(appRec.id == null){
            // isNewApplication = true;
            isUpdatePrimaryApplicant = true;
            byPassEditFlags = true;
        }
        Boolean isApplicationILPSPush = false;
        Savepoint sp = Database.setSavepoint();
        try
        {
            appRec = HDFC_DASH_StoreAppHelper.upsertApplicationDetails(parseApp,lead,isApplicationILPSPush, byPassEditFlags);
            //system.debug('Check For Flags after upsertApplication Details ' +'isUpdatePrimaryApplicant ' + isUpdatePrimaryApplicant+ ' ' +  apprec);
            applicationId = appRec.Id;
            if(parseApp.application.applicationDetails.sfApplicationId == null){
                parseApp.application.applicationDetails.sfApplicationId = appRec.Id;
            }
            
            //Fee Details
            HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForFeeDetails(parseApp,byPassEditFlags);
             
            //Funding Source
            HDFC_DASH_StoreAppHelper.upsertFundingSource(parseApp,byPassEditFlags);
            
            //Additional Loan
            Opportunity primeApp = HDFC_DASH_Application_Selector.getApplicationBasedOnId(new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id},appRec.id);
            HDFC_DASH_StoreAppHelper.upsertAdditionalLoan(parseApp,primeApp,byPassEditFlags);
            
            //Application Payment Details
            HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForPayementDetail(parseApp);
            //Applicant Details       
            List<HDFC_DASH_Applicant_Details__c> listApplDetail = new List<HDFC_DASH_Applicant_Details__c>();
            List<HDFC_DASH_Contact_Address_Details__c> listAddrDetails = new List<HDFC_DASH_Contact_Address_Details__c>(); 
            List<HDFC_DASH_Applicant_Employment_Details__c> listAppEmpDetails = new List<HDFC_DASH_Applicant_Employment_Details__c>();
            List<HDFC_DASH_Applicant_Income_Details__c> listIncomeDetails = new List<HDFC_DASH_Applicant_Income_Details__c>();          
            List<HDFC_DASH_Applicant_Income_Details__c> incomeDeleteList = new List<HDFC_DASH_Applicant_Income_Details__c>();
            List<HDFC_DASH_Applicant_Employment_Details__c> employmentDeleteList = new List<HDFC_DASH_Applicant_Employment_Details__c>();
            //List<Id> employmentDeleteList = new List<Id>();
        	Set<HDFC_DASH_Applicant_Employment_Details__c> employmentDeleteIds = new Set<HDFC_DASH_Applicant_Employment_Details__c>();
            List<HDFC_DASH_Info_Validation__c> appInfoValList = new List<HDFC_DASH_Info_Validation__c>();
            List<HDFC_DASH_Applicant_Financial_Info__c> financialInfoList = new List<HDFC_DASH_Applicant_Financial_Info__c>();
        	List<HDFC_DASH_Applicant_Financial_Info__c> financialInfoDelList = new List<HDFC_DASH_Applicant_Financial_Info__c>();
            List<HDFC_DASH_Applicant_Bank_Detail__c> bankDetailsList = new List<HDFC_DASH_Applicant_Bank_Detail__c>();
            List<HDFC_DASH_GST_Business_Details__c> empGSTDetailsList = new List<HDFC_DASH_GST_Business_Details__c>();
            List<Id> applicantDetailIds = new List<Id>();
           // List<Id> applicantEmpIds = new List<Id>();
            List<Note> listRemarks = new List<Note>();
            Map<HDFC_DASH_ParseApplication.ApplicantInfo,HDFC_DASH_Applicant_Details__c> mapApplicantInfoDetail = new Map<HDFC_DASH_ParseApplication.ApplicantInfo,HDFC_DASH_Applicant_Details__c>();
            Map<HDFC_DASH_ParseApplication.ApplicantEmploymentDetails,HDFC_DASH_Applicant_Employment_Details__c> mapAppEmpDetail = new Map<HDFC_DASH_ParseApplication.ApplicantEmploymentDetails,HDFC_DASH_Applicant_Employment_Details__c>();
            //HDFC_DASH_Applicant_Details__c applDetail = new HDFC_DASH_Applicant_Details__c() ;
            HDFC_DASH_StoreAppWrapper storeWrapper = new HDFC_DASH_StoreAppWrapper();
            storeWrapper = HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForApplicantDetail(parseApp, isUpdatePrimaryApplicant);
            listApplDetail = storeWrapper.listApplicantDetails;
            mapApplicantInfoDetail = storeWrapper.mapApplicantInfoDetail;
            
            // Run Salesforce Dedupe only on insert of applicant details
            if(storeWrapper.insertedApplicantDetailsList != null && storeWrapper.insertedApplicantDetailsList.size() > HDFC_DASH_Constants.INT_ZERO){
                coApplicantMap = HDFC_DASH_SalesforceDedupe.runSalesforceDedupeonAppDet(storeWrapper.insertedApplicantDetailsList);
                //system.debug('Coapplicant Map--'+coApplicantMap);

                HDFC_DASH_SalesforceDedupe.runSalesforceDedupeOnRef(storeWrapper.insertedApplicantDetailsList);
            }

            
            HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForApplicantAddressDetail(parseApp,listApplDetail,mapApplicantInfoDetail);
            mapAppEmpDetail = HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForApplicantEmploymentDetail(parseApp,listApplDetail,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.upsertApplicantIncomeDetails(parseApp,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForBankDetails(parseApp,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.processUpsertAndDeleteForFinancialInfoDetails(parseApp,mapApplicantInfoDetail);
            HDFC_DASH_StoreAppHelper.upsertNoteDetails(parseApp,mapApplicantInfoDetail);
            
           // if(!isNewApplication){
           if(Schema.sObjectType.Opportunity.isAccessible())
                database.upsert(appRec);////we are updating inserted application to set isLatest flags of child records on contact when application from ILPS 
           // }
            
            HDFC_DASH_StoreAppHelper.upsertApplicantGSTDetails(parseApp, mapAppEmpDetail);
            List<String> incomeFields = new List<String>{HDFC_DASH_Constants.ID_STRING};
                incomeDeleteList = HDFC_DASH_Applicant_Income_Selector.getIncomeDetails(applicantDetailIds,incomeFields,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS);
            
            //getEmpDetails Based On AppDetailIds
            List<HDFC_DASH_Applicant_Employment_Details__c> employmentDelList = new List<HDFC_DASH_Applicant_Employment_Details__c>();
            List<String> employmentFields = new List<String>{HDFC_DASH_Constants.ID_STRING};
            
            
            //getAppInfoVal Based On AppDetailIds
            List<String> infoValFields = new List<String>{HDFC_DASH_Constants.ID_STRING};
            appInfoValList = HDFC_DASH_AppInfoValidation_Selector.getAppInfoVal(applicantDetailIds,infoValFields,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS);
            
            //getBankDetails Based on AppDetailIds
            List<String> bankDetailFields = new List<String>{HDFC_DASH_Constants.ID_STRING};
            bankDetailsList = HDFC_DASH_Applicant_BankDetail_Selector.getBankDetails(applicantDetailIds,bankDetailFields,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS);

            //getFinancialDetails Based on AppDetailIds
            List<String> obligationnFields = new List<String>{HDFC_DASH_Constants.ID_STRING};
            financialInfoDelList = HDFC_DASH_ApplFinancialInfo_Selector.getApplicantFinancialInfo(applicantDetailIds,bankDetailFields,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS);
            
            //get applicant detail list
            List<HDFC_DASH_Applicant_Details__c> applicantdetailList = new List<HDFC_DASH_Applicant_Details__c>();
            List<String> applicantFields = new List<String>{HDFC_DASH_Constants.ID_STRING};
            applicantdetailList = HDFC_DASH_Applicant_Detail_Selector.getApplicantDetailBasedOnIds(applicantFields, applicantDetailIds);
            
            //delete income and employment details
            //if(incomeDeleteList != null && employmentDeleteList != null && appInfoValList != null && bankDetailsList != null && obligationList != null && applicantEmpIds != null){
            if(incomeDeleteList != null && incomeDeleteList.size() > HDFC_DASH_Constants.INT_ZERO){
                if(Schema.sObjectType.HDFC_DASH_Applicant_Income_Details__c.isDeletable())
                database.delete(incomeDeleteList);
            }
        	//system.debug('xxx listempDelete scenario' + employmentDeleteList);
            employmentDeleteIds.addAll(employmentDeleteList);
            if(appInfoValList != null && appInfoValList.size() > HDFC_DASH_Constants.INT_ZERO){
                 if(Schema.sObjectType.HDFC_DASH_Info_Validation__c.isDeletable())
                database.delete(appInfoValList);
            }
            if(bankDetailsList != null && bankDetailsList.size() > HDFC_DASH_Constants.INT_ZERO){
                if(Schema.sObjectType.HDFC_DASH_Applicant_Bank_Detail__c.isDeletable())
                database.delete(bankDetailsList);
            }
            //delete Applicant details
            if(applicantDetailIds != null && applicantDetailIds.size() > HDFC_DASH_Constants.INT_ZERO){
                if(Schema.sObjectType.HDFC_DASH_Applicant_Details__c.isDeletable())
                database.delete(applicantdetailList);
            }
        }catch(Exception e){
        
            Database.rollback(sp);   
            throw e;
        }
    }
}