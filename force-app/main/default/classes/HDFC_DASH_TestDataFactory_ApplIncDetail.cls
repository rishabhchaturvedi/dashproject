/* 
ClassName: HDFC_DASH_TestDataFactory_ApplIncDetail
DevelopedBy: Punam Marbate
Date:9 Aug 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Applicant Income Detail object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_ApplIncDetail {
    public static final string NAME = 'Name';
    
    /* Method Name: createBasicApplAddressDetail
    parameters: ID aplDetailID
    Method Description: This method is to create test data for HDFC_DASH_Applicant_Income_Details__c.
    Pass fields of HDFC_DASH_Applicant_Income_Details__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Applicant_Income_Details__c or inserted HDFC_DASH_Applicant_Income_Details__c id. 
    */
    public static HDFC_DASH_Applicant_Income_Details__c createBasicApplIncomeDetail(ID aplDetailID)
    {
        HDFC_DASH_Applicant_Income_Details__c appIncomeDetail = new HDFC_DASH_Applicant_Income_Details__c();
            
        //appIncomeDetail.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        appIncomeDetail.HDFC_DASH_Applicant_Details__c =  aplDetailID  ;       
        return appIncomeDetail;        
    }
    
    /* Method Name: getBasicApplIncomeDetail
    parameters: ID aplDetailID
    Method Description: This method is to generate test data for Applicant Income Detail.
    */
    public static HDFC_DASH_Applicant_Income_Details__c getBasicApplIncomeDetail(ID aplDetailID)
    {
        HDFC_DASH_Applicant_Income_Details__c appIncomeDetail = 
                HDFC_DASH_TestDataFactory_ApplIncDetail.createBasicApplIncomeDetail(aplDetailID);
        if(Schema.sObjectType.HDFC_DASH_Applicant_Income_Details__c.isCreateable()){
            Database.insert(appIncomeDetail);
        }
        return appIncomeDetail;
    }
}