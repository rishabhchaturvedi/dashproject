/**
className: HDFC_DASH_RestResource_DedupeCheckTest
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_DedupeCheck
**/
@isTest(SeeAllData = false)
private class  HDFC_DASH_RestResource_DedupeCheckTest {
    private static final string SUCCESS ='Success';
    private static final string EXCEPTION_OCCURED ='Exception Occured';
    //request and response body for testing
    private static final string REQUEST_BODY ='{"Lead":{"Salutation":"Mr.","FirstName":"FirstName","MiddleName":"","LastName":"LastName","GenderCode":"2","PAN":"LKOPS1234L","PanApplied":"No","PanStatusFromNSDL":"Status","CityCode":"1 SGM","StateCode":"RJ","CountryCode":"INDIA","CityName":"","StateName":"","CountryName":"","ResidentType":"RES","Email":"","DateOfBirth":"1998-08-24","MobileCountryCode":"91","Mobile":"3236115400","OutSystemsID":"100001","consent":{"voiceConsent":"Y","voiceConsentDate":"2000-10-30","SMSConsent":"Y","SMSConsentDate":"2016-10-30","WhatsAppConsent":"N","whatsAppConsentDate":null,"consentContent":"I/We allow HDFC to contact me towards this application"}},"validationOutcome":[{"User_input_type":"PAN","User_Input_value":"ASD123DSedf","Validated_with":"NSDL","validation_response":{}}]}';
    private static final string SUCCESS_RESPONSE_BODY = '{"sfCustomerNumber":"700000434","sfCustomerId":"0039D00000ENSFlQAP","sfApplicantID":"a0D9D000005MNawUAG","lmsDetails":{"lmsOriginBranchId":"90","lmsOriginBranch":"HDFC_ONLINE","lmsLeadType":"EXISTING","lmsLeadStatus":"NEW","lmsLeadID":"101801090"},"isRegisteredCustomer":false,"isCustomer":false,"eligibleForExistingCustJourney":false,"customerMobile":"34533224209","customerEmail":"gfgfgfg@gmail.com","application":{"sfApplicationNumber":"10000754","sfApplicationId":"0069D00000DD35xQAD"}}';
    private static final string ERROR_RESPONSE_BODY = '{"errorType":"HDFC_DASH_Exceptionclass.API_header_Exception","errorMessage":"The Header of the request is incomplete"}';
    private static final string STRING_POST ='POST';
    private static final string STRING_URL = '/services/apexrest/hdfc/dedupecheck/V1.0/';  
    /* 
    Method Name :testHandleDedupeCheckRequest
    Parameters  :
    Description :This method is for success senario would call HDFC_DASH_RestResource_DedupeCheck.handleDedupeCheckRequest(request).
    */
    static testmethod void testHandleDedupeCheckRequest(){
        
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        System.runAs(sysAdmin)
        {       
            Map<String,String> requestHeader = new Map<String,String>();
            
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            request.requestUri = STRING_URL;
            request.httpMethod = STRING_POST;
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY);
            RestContext.request = request;
            RestContext.response = response;
            test.startTest();
            	HDFC_DASH_RestResource_DedupeCheck.handleDedupeCheckRequest();
            test.stopTest();
            System.assertNotEquals(null, response);
        }
        
    }
    
    /* 
    Method Name :testHandleDedupeCheckRequestException
    Parameters  :
    Description :This method is for error senario would call HDFC_DASH_RestResource_DedupeCheck.handleDedupeCheckRequest(request).
    */
    static testmethod void testHandleDedupeCheckRequestException(){
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);  
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        System.runAs(sysAdmin)
        {
            RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            request.requestUri = STRING_URL;
            request.httpMethod = STRING_POST;
            request = HDFC_DASH_TestDataFactory_API.assignPartialHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY);
            RestContext.request = request;
            RestContext.response = response;
            test.startTest();
                	HDFC_DASH_RestResource_DedupeCheck.handleDedupeCheckRequest();                 	
            test.stopTest();
            system.assertEquals(HDFC_DASH_Constants.INT_FIVE_HUNDRED, response.statusCode);
        }
    }
}