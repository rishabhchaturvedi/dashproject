/**
className: HDFC_DASH_RestResource_GetAppListTest
DevelopedBy: Punam Marbate
Date: 16 Aug 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_GetAppListTest and HDFC_DASH_InboundRestService_GetAppList.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_RestResource_GetAppListTest {

    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR); 
    private static Id sysAdminId = UserInfo.getUserId();
    private static final string BASEURL = URL.getOrgDomainUrl().toExternalForm();
    private static final string STRING_URL = BASEURL + '/services/apexrest/hdfc/getApplication/V1.0';
    private static final string PARAM_CUSTOMERID = 'customerid';
     private static final string param_TLID = 'TLID';
    private static final string param_SOID = 'SOID';
    private static final string param_BSAID = 'BSAID';
     private static final string PARAM_CUSTOMERNUMBER = 'customerNumber';

    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
    private static Contact conWithoutApp = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    /* 
    Method Name : testGetApplicationList
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList().
    */
    static testmethod void testGetApplicationList()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL; 
            request.addParameter(PARAM_CUSTOMERID, con.id);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
                HDFC_DASH_RestResource_GetAppList.getApplicationList();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
     /* 
    Method Name : testGetApplicationTypeCustNumber
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList().
    */
     static testmethod void testGetApplicationTypeCustNumber()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL; 
            request.addParameter(PARAM_CUSTOMERNUMBER, acc.HDFC_DASH_Customer_Number__c);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
                HDFC_DASH_RestResource_GetAppList.getApplicationList();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
     /* 
    Method Name : testGetApplicationTypeTLID
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList().
    */
    static testmethod void testGetApplicationTypeTLID()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL; 
            request.addParameter(param_TLID, sysAdminId);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
                HDFC_DASH_RestResource_GetAppList.getApplicationList();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    /* 
    Method Name : testGetApplicationTypeSOID
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList().
    */
     static testmethod void testGetApplicationTypeSOID()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL; 
            request.addParameter(param_SOID, sysAdminId);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
                HDFC_DASH_RestResource_GetAppList.getApplicationList();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
     /* 
    Method Name : testGetApplicationTypeBSAID
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList().
    */
     static testmethod void testGetApplicationTypeBSAID()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL; 
            request.addParameter(param_BSAID, app.HDFC_DASH_LMS_Agency_Code__c);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
                HDFC_DASH_RestResource_GetAppList.getApplicationList();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    /* Method Name: testExceptionCUST_ID_MISSING
    parameters: NONE
    Method Description: This method is to test Exception scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList() when CustId is missing
    */
    static TestMethod void testExceptionCUST_ID_MISSING() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        Exception testException;
        
        //Request Header is not added to check whether the exception is throw       
        RestRequest request = new RestRequest();
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
        request.requestUri = STRING_URL;      
        RestContext.request = request;

        test.startTest();
        System.runAs(sysAdmin)
        {
            try
            {
                HDFC_DASH_RestResource_GetAppList.getApplicationList();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    } 
    
    /* 
    Method Name : testException_APPLICATIONID_DOESNOTEXIST
    Parameters  : No
    Description :This method is to test Exception scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList() when there is no application for Customer.
    */
    static TestMethod void testException_APPLICATIONID_DOESNOTEXIST() 
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            Exception testException;
            RestRequest request = new RestRequest();
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestUri = STRING_URL + conWithoutApp.Id;      
            RestContext.request = request;
            Test.startTest();
            try
            {
                HDFC_DASH_RestResource_GetAppList.getApplicationList();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    }
   
}