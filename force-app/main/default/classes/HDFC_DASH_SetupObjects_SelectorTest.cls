/**
className: HDFC_DASH_SetupObjects_SelectorTest
DevelopedBy: Janani Mohankumar
Date: 21 NOV 2021
Company: Accenture
Class Description: Test class for HDFC_DASH_SetupObjects_Selector class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_SetupObjects_SelectorTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    
         
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
     
    private static final String profilename = HDFC_DASH_Constants.PARTNER_COMMUNITY_USER;
    private static final String permissionSetName = HDFC_DASH_Constants.SALES_USER_PERMISSION;
    private static final String UserRoleName ='Sales Officer';
    private static final String groupName = HDFC_DASH_Constants.SALES_USER_PUBLICGROUP;
    
    
    /* 
Method Name :testGetProfile
Description :This method will test the getProfile method.
*/
    static testmethod void testGetProfile()
    {  
        Profile pf = new Profile(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            pf = HDFC_DASH_SetupObjects_Selector.getProfile( fieldList, profilename);       
            Test.stopTest();
            system.assertNotEquals( null, pf);
            
        } 
    }
     /* 
Method Name :testGetPermissionSet
Description :This method will test the getPermissionSet method.
*/
    static testmethod void testGetPermissionSet()
    {  
        PermissionSet permSet = new PermissionSet(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            permSet = HDFC_DASH_SetupObjects_Selector.getPermissionSet( fieldList, permissionSetName);       
            Test.stopTest();
            system.assertNotEquals( null, permSet);
            
        } 
    }
         /* 
Method Name :testGetUserRole
Description :This method will test the getUserRole method.
*/
    static testmethod void testGetUserRole()
    {  
        UserRole role = new UserRole(); 
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            role = HDFC_DASH_SetupObjects_Selector.getUserRole( fieldList, UserRoleName);       
            Test.stopTest();
            system.assertNotEquals( null, role);
            
        } 
    }
    /* 
Method Name :testGetUserRoles
Description :This method will test the getUserRoles method.
*/
    static testmethod void testGetUserRoles()
    {  
        List<UserRole> role = new List<UserRole>();
         System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
              list<String> listUserRoleName =new list<String>{UserRoleName};
            role = HDFC_DASH_SetupObjects_Selector.getUserRoles(listUserRoleName,fieldList, HDFC_DASH_Constants.STRING_Name);       
            Test.stopTest();
            system.assertNotEquals( null, role);
            
        } 
      
    }
    
/* 
Method Name :testGetPublicGroup
Description :This method will test the getPublicGroup method.
*/
    static testmethod void testGetPublicGroup()
    {  
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
            Test.startTest();   
            Group groupRec = HDFC_DASH_SetupObjects_Selector.getPublicGroup(fieldList, groupName);       
            Test.stopTest();
            system.assertNotEquals( null, groupRec);
            
        } 
        
    }
    
}