/*
Author: Anisha Arumugam
Class: HDFC_DASH_InboundRestService_QuoteInfo
Description: Service Class for Quote Info API
*/
public inherited sharing class HDFC_DASH_InboundRestService_QuoteInfo { 
    
    /* 
    Method:processQuoteDetails
    Description: Method to process Quote information
    */
   public static HDFC_DASH_APIResponse_QuoteInfo processQuoteDetails(RestRequest request)
    {      
        //Calling JSON Parser Class
        HDFC_DASH_ParseQuoteInfo result = HDFC_DASH_ParseQuoteInfo.parse(request);
        
        //Update Application and create Applicant Address Details Object
        HDFC_DASH_APIResponse_QuoteInfo apiRes = updateQuoteDetails(result);
        
        return apiRes;  
    }
    
    /*  
    Method : updateQuoteDetails
    Description:This method will update the Application and Applicant Employment Details Objects.
    */
   private static HDFC_DASH_APIResponse_QuoteInfo updateQuoteDetails(HDFC_DASH_parseQuoteInfo result)
    {             
        HDFC_DASH_APIResponse_QuoteInfo resp = new HDFC_DASH_APIResponse_QuoteInfo();
        
        List<string> listAppID = new List<string>();
        List<string> listAppDetailID = new List<string>();       
        List<Opportunity> listApp = new List<Opportunity>();
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>();  
        List<string> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING};  
        List<string> listOfFields_AppDetails = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Permanent_Address,
            													HDFC_DASH_Constants.HDFC_DASH_Current_Address,HDFC_DASH_Constants.HDFC_DASH_Contact}; 
                
        List<HDFC_DASH_Master_Table__c> listMasterTable = new List<HDFC_DASH_Master_Table__c>();
        List<HDFC_DASH_City_Master__c> listCityMaster = new List<HDFC_DASH_City_Master__c>();
        List<HDFC_DASH_Pincode_Master__c> listPinCodeMaster = new List<HDFC_DASH_Pincode_Master__c>();
        
        List<string> listMasterTableIDs = new List<string>();
        List<string> listCityMasterIDs = new List<string>();
        List<string> listPinCodeMasterIDs = new List<string>();
        
        map<string, HDFC_DASH_Master_Table__c> mapMasterTable = new map<string, HDFC_DASH_Master_Table__c>();
        map<string, HDFC_DASH_City_Master__c> mapCityMaster = new map<string, HDFC_DASH_City_Master__c>();
        map<string, HDFC_DASH_Pincode_Master__c> mapPinCodeMaster = new map<string, HDFC_DASH_Pincode_Master__c>();
        
        if(!String.isBlank(result.QuoteInfo.sfApplicationID) && !String.isBlank(result.QuoteInfo.sfApplicantID))
        {
            listAppID.add(result.QuoteInfo.sfApplicationID);                            
            listAppDetailID.add(result.QuoteInfo.sfApplicantID); 
            
            //Adding the list of IDs to be queried  
            listMasterTableIDs.add(result?.QuoteInfo?.PrePopulatedAddressDetails?.countryCode);
            listCityMasterIDs.add(result?.QuoteInfo?.PrePopulatedAddressDetails?.cityCode);
            listMasterTableIDs.add(result?.QuoteInfo?.PrePopulatedAddressDetails?.stateCode);
            listPinCodeMasterIDs.add(result?.QuoteInfo?.PrePopulatedAddressDetails?.pinCode);
            
            listMasterTableIDs.add(result?.QuoteInfo?.CustomerSelectedAddressDetails?.countryCode);
            listCityMasterIDs.add(result.QuoteInfo?.CustomerSelectedAddressDetails?.cityCode);
            listMasterTableIDs.add(result.QuoteInfo?.CustomerSelectedAddressDetails?.stateCode);
            listPinCodeMasterIDs.add(result.QuoteInfo?.CustomerSelectedAddressDetails?.pinCode);
            
            //Calling Application and Applicant Detail Selector Classes
            listApp = HDFC_DASH_Application_Selector.getApplication(listAppID, listOfFields_App, HDFC_DASH_Constants.ID_STRING);
            listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppDetailID, listOfFields_AppDetails, HDFC_DASH_Constants.ID_STRING);

            //Calling Selector to query Master Tables     
            listMasterTable = HDFC_DASH_MasterTables_Selector.getMasterTable(listMasterTableIDs);                
            listCityMaster = HDFC_DASH_MasterTables_Selector.getCityMaster(listCityMasterIDs);
            listPinCodeMaster = HDFC_DASH_MasterTables_Selector.getPinCodeMaster(listPinCodeMasterIDs);

            for(HDFC_DASH_Master_Table__c objMT : listMasterTable)
                mapMasterTable.put(objMT.HDFC_DASH_CD_Val__c, objMT);
            
            for(HDFC_DASH_City_Master__c objCM : listCityMaster)
                mapCityMaster.put(objCM.HDFC_DASH_City_Code__c, objCM);

            for(HDFC_DASH_Pincode_Master__c objPM : listPinCodeMaster)
                mapPinCodeMaster.put(objPM.HDFC_DASH_Pincode__c, objPM);
            
            if(!listApp.isEmpty() && !listAppDetails.isEmpty())
            {
                Opportunity objApplication = listApp[HDFC_DASH_Constants.INT_ZERO];
                objApplication.Id = result.QuoteInfo.sfApplicationID;
                
                //HDFC_DASH_Applicant_Address_Details__c conAddDet = new HDFC_DASH_Applicant_Address_Details__c();
                HDFC_DASH_Contact_Address_Details__c conAddDet = new HDFC_DASH_Contact_Address_Details__c();
                //conAddDet.HDFC_DASH_Applicant_Details__c = result?.QuoteInfo?.sfApplicantID;
                conAddDet.HDFC_DASH_Contact__c = listAppDetails[HDFC_DASH_Constants.INT_ZERO].HDFC_DASH_Contact__c;
                conAddDet.HDFC_DASH_Address_Line1__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.addressLine1;
                conAddDet.HDFC_DASH_Address_Line2__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.addressLine2;
                conAddDet.HDFC_DASH_Address_Line3__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.addressLine3;
                conAddDet.HDFC_DASH_Address_Line4__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.addressLine4;
                conAddDet.HDFC_DASH_Landmark__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.landmark;
                conAddDet.HDFC_DASH_Taluka__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.taluka;
                conAddDet.HDFC_DASH_District__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.district;
                conAddDet.HDFC_DASH_Post_Office_Name__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.PostOfficeName;
                
                conAddDet.HDFC_DASH_Address_Line1_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.addressLine1;
                conAddDet.HDFC_DASH_Address_Line2_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.addressLine2;
                conAddDet.HDFC_DASH_Address_Line3_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.addressLine3;
                conAddDet.HDFC_DASH_Address_Line4_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.addressLine4;
                conAddDet.HDFC_DASH_Landmark_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.landmark;
                conAddDet.HDFC_DASH_Taluka_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.taluka;
                conAddDet.HDFC_DASH_District_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.district;
                conAddDet.HDFC_DASH_PostOfficeNameCustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.PostOfficeName;
                conAddDet.HDFC_DASH_Address_Type__c=result?.QuoteInfo?.addressType;
                conAddDet.HDFC_DASH_Address_From__c=result?.QuoteInfo?.addressFrom;
                conAddDet.HDFC_DASH_Address_Source__c=result?.QuoteInfo?.addressSource;
                conAddDet.HDFC_DASH_Address_Changed__c=result?.QuoteInfo?.addressChangedByCustomer;
                    
                objApplication.HDFC_DASH_SO_Total_Existing_EMI__c=result?.QuoteInfo?.totalExistingEMI;
                objApplication.HDFC_DASH_SO_Required_Loan_Amount__c=result?.QuoteInfo?.loanAmount;
                objApplication.HDFC_DASH_No_EMI_Paid__c=result?.QuoteInfo?.NoEMIPaid;
                objApplication.HDFC_DASH_Cibil_Loan_Purpose__c=result?.QuoteInfo?.cibilLoanPurpose;
                objApplication.HDFC_DASH_Loan_Purpose__c=result?.QuoteInfo?.loanPurpose;
                objApplication.HDFC_DASH_Spot_Offer_Accepted__c=result?.QuoteInfo?.spotOfferAccepted;
                objApplication.HDFC_DASH_Spot_Offer_Eligible__c=result?.QuoteInfo?.spotOfferEligible;
                objApplication.HDFC_DASH_TF_Spot_Offer_Eligible__c=result?.QuoteInfo?.spotOfferEligibilityTechnicalFailure==null?false:result?.QuoteInfo?.spotOfferEligibilityTechnicalFailure;
                objApplication.HDFC_DASH_TF_Eligibility_Amount__c=result?.QuoteInfo?.eligibilityAmountTechnicalFailure==null?false:result?.QuoteInfo?.eligibilityAmountTechnicalFailure;
                objApplication.HDFC_DASH_EA_Loan_Product_Code__c=result?.QuoteInfo?.EligibilityAmount?.loan_product;
                objApplication.HDFC_DASH_EA_Loan_Product_Description__c=result?.QuoteInfo?.EligibilityAmount?.product_desc;
                objApplication.HDFC_DASH_EA_Loan_Term_Months__c=result?.QuoteInfo?.EligibilityAmount?.loan_term;
                objApplication.HDFC_DASH_EA_Requested_Loan__c=result?.QuoteInfo?.EligibilityAmount?.requested_loan;
                objApplication.HDFC_DASH_EA_Current_ROI__c=result?.QuoteInfo?.EligibilityAmount?.current_roi;
                objApplication.HDFC_DASH_EA_Possible_Term_Months__c=result?.QuoteInfo?.EligibilityAmount?.possible_term;
                objApplication.HDFC_DASH_EA_Stretched_Term_Months__c=result?.QuoteInfo?.EligibilityAmount?.stretched_term;
                objApplication.HDFC_DASH_EA_Loan_Possible__c=result?.QuoteInfo?.EligibilityAmount?.loan_possible;
                objApplication.HDFC_DASH_EA_Stretched_Amount__c=result?.QuoteInfo?.EligibilityAmount?.stretched_amt;
                objApplication.HDFC_DASH_EA_Possible_EMI__c=result?.QuoteInfo?.EligibilityAmount?.possible_emi;
                objApplication.HDFC_DASH_EA_Stretched_EMI__c=result?.QuoteInfo?.EligibilityAmount?.stretched_emi;
                objApplication.HDFC_DASH_EA_Concession__c=result?.QuoteInfo?.EligibilityAmount?.concession;
                objApplication.HDFC_DASH_EA_Normal_IIR__c=result?.QuoteInfo?.EligibilityAmount?.normal_iir;
                objApplication.HDFC_DASH_EA_Normal_FOIR__c=result?.QuoteInfo?.EligibilityAmount?.normal_foir;
                objApplication.HDFC_DASH_EA_Max_IIR__c=result?.QuoteInfo?.EligibilityAmount?.max_iir;
                objApplication.HDFC_DASH_EA_Max_FOIR__c=result?.QuoteInfo?.EligibilityAmount?.max_foir;
                objApplication.HDFC_DASH_EA_Campaign_Code__c=result?.QuoteInfo?.EligibilityAmount?.campaign_code;
                objApplication.HDFC_DASH_EA_NIIR__c=result?.QuoteInfo?.EligibilityAmount?.niir;
                objApplication.HDFC_DASH_EA_NFOIR__c=result?.QuoteInfo?.EligibilityAmount?.nfoir;
                objApplication.HDFC_DASH_EA_NLCR__c=result?.QuoteInfo?.EligibilityAmount?.nlcr;
                objApplication.HDFC_DASH_EA_NLVR__c=result?.QuoteInfo?.EligibilityAmount?.nlvr;
                objApplication.HDFC_DASH_EA_Comb_LCR_LTV__c=result?.QuoteInfo?.EligibilityAmount?.comb_lcr_ltv;
                objApplication.HDFC_DASH_EA_LTV_Val__c=result?.QuoteInfo?.EligibilityAmount?.ltv_val;
                objApplication.HDFC_DASH_EA_PLR_Rate__c=result?.QuoteInfo?.EligibilityAmount?.plr_rate;
                objApplication.HDFC_DASH_EA_PLR_ID__c=result?.QuoteInfo?.EligibilityAmount?.plr_id;
                
                objApplication.HDFC_DASH_CS_Interest_Type__c=result?.QuoteInfo?.CustomerSelectedSpotOffer?.customerSelectedInterestType;
                objApplication.HDFC_DASH_CS_Interest_Rate__c=result?.QuoteInfo?.CustomerSelectedSpotOffer?.customerSelectedInterestRate;
                objApplication.HDFC_DASH_CS_Tenure_Months__c=result?.QuoteInfo?.CustomerSelectedSpotOffer?.customerSelectedTenureInMonths;
                objApplication.HDFC_DASH_CS_Loan_Amount__c=result?.QuoteInfo?.CustomerSelectedSpotOffer?.customerSelectedLoanAmount;
                objApplication.HDFC_DASH_CS_EMI_Amount__c=result?.QuoteInfo?.CustomerSelectedSpotOffer?.customerSelectedEMIAmount;
                
                //Set city name and city record id from City Master using City Code
                conAddDet.HDFC_DASH_City__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.city;
                conAddDet.HDFC_DASH_City_Code__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.cityCode;
                if(!String.isBlank(result?.QuoteInfo?.PrePopulatedAddressDetails?.cityCode) && mapCityMaster.get(result?.QuoteInfo?.PrePopulatedAddressDetails?.cityCode) != null)
                {    
                    conAddDet.HDFC_DASH_City__c = mapCityMaster.get(result.QuoteInfo.PrePopulatedAddressDetails.cityCode)?.HDFC_DASH_City_Name__c;
                    conAddDet.HDFC_DASH_City_RecordID__c = mapCityMaster.get(result.QuoteInfo.PrePopulatedAddressDetails.cityCode)?.id;
                }
                
                //Set State name and State record id from Master Table using State code   
                conAddDet.HDFC_DASH_State__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.state;
                conAddDet.HDFC_DASH_State_Code__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.stateCode;             
                if(!String.isBlank(result?.QuoteInfo?.PrePopulatedAddressDetails?.stateCode) && mapMasterTable.get(result?.QuoteInfo?.PrePopulatedAddressDetails?.stateCode) != null)
                {
                    conAddDet.HDFC_DASH_State__c = mapMasterTable.get(result.QuoteInfo.PrePopulatedAddressDetails.stateCode)?.HDFC_DASH_CD_Desc__c;
                    conAddDet.HDFC_DASH_State_RecordId__c = mapMasterTable.get(result.QuoteInfo.PrePopulatedAddressDetails.stateCode)?.id;
                }
                
                //Set Country name and Country record id from Master Table using Country code     
                conAddDet.HDFC_DASH_Country__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.Country;
                conAddDet.HDFC_DASH_Country_Code__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.countryCode;           
                if(!String.isBlank(result?.QuoteInfo?.PrePopulatedAddressDetails?.countryCode) && mapMasterTable.get(result?.QuoteInfo?.PrePopulatedAddressDetails?.countryCode) != null)
                {
                    conAddDet.HDFC_DASH_Country__c = mapMasterTable.get(result.QuoteInfo.PrePopulatedAddressDetails.countryCode)?.HDFC_DASH_CD_Desc__c;    
                    conAddDet.HDFC_DASH_Country_RecordId__c = mapMasterTable.get(result.QuoteInfo.PrePopulatedAddressDetails.countryCode)?.id;    
                }    

                //Set city name and city record id from City Master using City Code for Customer Selected
                conAddDet.HDFC_DASH_City_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.city;
                conAddDet.HDFC_DASH_City_Code_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.cityCode;
                if(!String.isBlank(result?.QuoteInfo?.CustomerSelectedAddressDetails?.cityCode) && mapCityMaster.get(result?.QuoteInfo?.CustomerSelectedAddressDetails?.cityCode) != null)
                {
                    conAddDet.HDFC_DASH_City_CustomerSelected__c = mapCityMaster.get(result.QuoteInfo.CustomerSelectedAddressDetails.cityCode)?.HDFC_DASH_City_Name__c;
                    conAddDet.HDFC_DASH_City_RecordID_CS__c = mapCityMaster.get(result.QuoteInfo.CustomerSelectedAddressDetails.cityCode)?.id;                   
                }
                
                //Set State name and State record id from Master Table using State code for Customer Selected    
                conAddDet.HDFC_DASH_State_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.state;
                conAddDet.HDFC_DASH_State_Code_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.stateCode;          
                if(!String.isBlank(result?.QuoteInfo?.CustomerSelectedAddressDetails?.stateCode) && mapMasterTable.get(result?.QuoteInfo?.CustomerSelectedAddressDetails?.stateCode) != null)
                {  
                    conAddDet.HDFC_DASH_State_CustomerSelected__c = mapMasterTable.get(result.QuoteInfo.CustomerSelectedAddressDetails.stateCode)?.HDFC_DASH_CD_Desc__c;
                    conAddDet.HDFC_DASH_StateRecordID_CS__c = mapMasterTable.get(result.QuoteInfo.CustomerSelectedAddressDetails.stateCode)?.id;
                }
                 
                //Set Country name and Country record id from Master Table using Country code for Customer Selected      
                conAddDet.HDFC_DASH_Country_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.Country;
                conAddDet.HDFC_DASH_Country_Code_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.countryCode;         
                if(!String.isBlank(result?.QuoteInfo?.CustomerSelectedAddressDetails?.countryCode) && mapMasterTable.get(result?.QuoteInfo?.CustomerSelectedAddressDetails?.countryCode) != null)
                {
                    conAddDet.HDFC_DASH_Country_CustomerSelected__c = mapMasterTable.get(result.QuoteInfo.CustomerSelectedAddressDetails.countryCode)?.HDFC_DASH_CD_Desc__c;
                    conAddDet.HDFC_DASH_Cou_RecordID_CustomerSelected__c = mapMasterTable.get(result.QuoteInfo.CustomerSelectedAddressDetails.countryCode)?.id;
                }    

                //Set Pincode and Pincode Record Id from Pincode Master using Pincode      
                conAddDet.HDFC_DASH_Pincode__c=result?.QuoteInfo?.PrePopulatedAddressDetails?.pinCode;       
                if(!String.isBlank(result?.QuoteInfo?.PrePopulatedAddressDetails?.pinCode) && mapPinCodeMaster.get(result?.QuoteInfo?.PrePopulatedAddressDetails?.pinCode) != null)
                {
                    conAddDet.HDFC_DASH_Pincode_RecordID__c = mapPinCodeMaster.get(result.QuoteInfo.PrePopulatedAddressDetails.pinCode)?.id;
                    conAddDet.HDFC_DASH_Post_Office_Name__c = mapPinCodeMaster.get(result.QuoteInfo.PrePopulatedAddressDetails.pinCode)?.HDFC_DASH_Place__c;
                }    

                //Set Pincode and Pincode Record Id from Master Table using Pincode for Customer Selected      
                conAddDet.HDFC_DASH_Pincode_CustomerSelected__c=result?.QuoteInfo?.CustomerSelectedAddressDetails?.pinCode;       
                if(!String.isBlank(result?.QuoteInfo?.CustomerSelectedAddressDetails?.pinCode) && mapPinCodeMaster.get(result?.QuoteInfo?.CustomerSelectedAddressDetails?.pinCode) != null)
                {
                    conAddDet.HDFC_DASH_Pin_RecordID_CustomerSelected__c = mapPinCodeMaster.get(result.QuoteInfo.CustomerSelectedAddressDetails.pinCode)?.id;
                    conAddDet.HDFC_DASH_PostOfficeNameCustomerSelected__c = mapPinCodeMaster.get(result.QuoteInfo.CustomerSelectedAddressDetails.pinCode)?.HDFC_DASH_Place__c;
                }    
                
                if(Schema.sObjectType.Opportunity.isUpdateable()){
                    Database.Update(objApplication);
                }
                if(Schema.sObjectType.HDFC_DASH_Contact_Address_Details__c.isCreateable()){
                Database.Insert(conAddDet);
                }
                updateApplicantDetail(conAddDet,listAppDetails);
                resp.successMessage = HDFC_DASH_Constants.QUOTEINFO_SUCCESSMESSAGE;                
            }   
            else
            {
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_NOTEXIST); 
            }  
        } 
        else
        {
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_APPID_MISSING); 
        }  
        return resp;
    }
    
    /*  
Method : updateApplicantDetail
Description:This method will update the Applicant Detail Object.
*/
    private static void updateApplicantDetail(HDFC_DASH_Contact_Address_Details__c conAddDet, List<HDFC_DASH_Applicant_Details__c> listAppDetails){
        listAppDetails[HDFC_DASH_Constants.INT_ZERO].HDFC_DASH_Current_Address__c = conAddDet.id;
        if(Schema.sObjectType.HDFC_DASH_Applicant_Details__c.isUpdateable()){
            update listAppDetails[HDFC_DASH_Constants.INT_ZERO];
        }
        
    }

}