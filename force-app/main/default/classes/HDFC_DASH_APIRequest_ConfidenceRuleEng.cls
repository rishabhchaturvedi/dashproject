/*
Class: HDFC_DASH_APIRequest_ConfidenceRuleEng
Author: Sai Suman
Date: 26 Nov 2021
Company: Accenture
Description:This class will define the request body of ConfidenceRuleEngine.
*/
public class HDFC_DASH_APIRequest_ConfidenceRuleEng {
    
    /*HEADER Node*/
    public class Header{
        public string APPLICATION_ID;
        public string INSTITUTION_ID;
        public string REQUEST_TIME;
        public string REQUEST_TYPE;
        public string APP_TYPE;
        public string sCheckBureauFlag;
        public string sCheckScoreFlag;
    }
    /*APPLICATION Node*/
    public class Application {
        public string sLoanType;
        public Decimal loanAmt;
        public Decimal totalPropertyCost;
        public Decimal reqLoanAmount;
        public string loanProductType;
        public string minMandDocRcvd;
        public string stage;
        public Decimal roi;
        public Decimal loanTerm;
        public Decimal maxIIR;
        public Decimal totalObligation;
        public Decimal maxFOIR;
    }
    /*APPLICANTS Node*/
    public class Applicants{
        public String APPLICANT_ID;
        public Request REQUEST;
        
    }
    /*Request Node*/
    public class Request{
        public String sCustomerId;
        public String nMbAckId;
        public String sApplicantType;
        public String firstName;
		public String middleName;
		public String lastName;
        public String salutation;
        public String pan;
        public String incomeConsidered;
        public String resiType;
        public String natOfEmp;
        public String selfEmpType;
        public String gender;
        //public String customerStatus;
        public String customerGrade;
        public String hdfcStaff;
        public Decimal confidenceScore;
        public Decimal income;
        public Decimal addlIncome;
        public Decimal age;
        public String relWithBorr;
        public String karzaPhotoMatch;
        public String form16TracesFormat;
        public Decimal form16IdMatch;
        public Decimal salSlipIdMatch;
        public Decimal bkstmntIdMatch;
        public String bkstmntExtractable;
        public Decimal curEmpYears;
        public Decimal totalExpYears;
        public Boolean existingCustomer;
        public String maritalStatus;
        public Decimal grossSalary;
        public String form16LatestYear;
    }
    public Header HEADER;
    public Application APPLICATION;
    public List<Applicants> APPLICANTS;
    
}