/**
*   className: HDFC_DASH_TriggerHandler
DevelopedBy: Anmol
Date: 25 May 2021
Company: Accenture
Class Description: The common trigger handler that is called by every Apex trigger.
* Simply delegates the work to config's before and after operations.
*/
public with sharing class HDFC_DASH_TriggerHandler {
/* methodName:handle
parameters:HDFC_DASH_TriggerConfig 
methodDescription:This method handles the trigger operations
*/  
    public static Boolean isFirstTime = true; 
    public static Boolean isFirstTimeOwner = false; 
    public static Boolean isFirstTimeRun = false; 
    public static Boolean isInfluencer=false; 
    public static Boolean OwnerMailsent=false; 
    public static Boolean isModechange=false; 
public static void handle(HDFC_DASH_TriggerConfig config) {
    if (!config.isEnabled) {return;}
    if (trigger.isBefore && trigger.isInsert) {
    if (!config.isBeforeInsertEnabled) {return;}
    for (HDFC_DASH_TriggerOps operation : config.beforeInsertOps) {
    run(operation);
    }
}
    if (trigger.isBefore && trigger.isUpdate) {
    if (!config.isBeforeUpdateEnabled) {return;}
    for (HDFC_DASH_TriggerOps operation : config.beforeUpdateOps) {
    run(operation);
    }
    }if (trigger.isBefore && trigger.isDelete) {
    if (!config.isBeforeDeleteEnabled) {return;}
    for (HDFC_DASH_TriggerOps operation : config.beforeDeleteOps) {
    run(operation);
    }
    }
    if (trigger.isAfter && trigger.isInsert) {
    if (!config.isAfterInsertEnabled) {return;}
    for (HDFC_DASH_TriggerOps operation : config.afterInsertOps) {
    run(operation);
    }
    }
    if (trigger.isAfter && trigger.isUpdate) {
    if (!config.isAfterUpdateEnabled) {return;}
    for (HDFC_DASH_TriggerOps operation : config.afterUpdateOps) {
    run(operation);
    }
    }if (trigger.isAfter && trigger.isDelete) {
    if (!config.isAfterDeleteEnabled) {return;}
    for (HDFC_DASH_TriggerOps operation : config.afterDeleteOps) {
    run(operation);
    }
    }if (trigger.isAfter && trigger.isUndelete) {
    if (!config.isAfterUndeleteEnabled) {return;}
    for (HDFC_DASH_TriggerOps operation : config.afterUndeleteOps) {
    run(operation);
    }
    }
}
/* methodName:run
parameters:HDFC_DASH_TriggerOps 
methodDescription:This method executes the operation
*/
private static void run(HDFC_DASH_TriggerOps operation) {
    if (operation.isEnabled()) {
    SObject[] sobs = operation.filter();
    if (sobs.size() > 0) {
    operation.execute(sobs);
    }
    }
}
}