/*
Class:HDFC_DASH_DocDetailsReq_ILPSCallout
Author: Anisha Arumugam
Description: This class will generate the request body for Document Details Callout to ILPS 
*/
public inherited sharing class HDFC_DASH_DocDetailsReq_ILPSCallout {
    
    public List<documentDetails> documentDetails;
    /*
Class: documentDetails
Description: Class to parse document Details
*/
    public class documentDetails {
        public String sfCustNo;
        public String sfApplId;
        public String fileNo;
        public String custNo;
        public String docType;
        public String capacity;
        public String monthOfPayslip;
        public String monthOfGstReturn;
        public String assessmentYear;
        public String financialYear;
        public String accountType;
        public String bkAccNo;
        public Date fromDate;
        public Date toDate;
        public String bankCode;
        public String currentEmployer;
        public Decimal totalAmountPaid;
        public Integer noOfQuartersPaid;
        public String proofOfCommunication;
        public String kycDocType;
        public String bcpDocType;
        public String docExtractable;
        public String docScannedBy;
        public String docImageOrigin;
        public Decimal latitute;
        public Decimal longitute;
        public String storageIdentifier;
        public String storageType;
        public String isSalaryAccount;
    }
    
}