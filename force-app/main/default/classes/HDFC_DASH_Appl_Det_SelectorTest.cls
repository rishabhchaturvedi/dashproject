/**
className: HDFC_DASH_Appl_Det_SelectorTest
DevelopedBy: Punam Marbate
Date: 26 July 2021
Company: Accenture
Class Description: Test class for  Applicant_Detail_Selector class.
**/
@isTest(SeeAllData = false)
public class HDFC_DASH_Appl_Det_SelectorTest {
  //Getting the object records
  private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
  private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
  private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
  private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
  private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);  
    /*
  Method Name :testGetApplicationDetails
  Description :This method will test the getApplicationDetails().
  */  
  static testmethod void testGetApplicationDetails(){

      
    List<HDFC_DASH_Applicant_Details__c>  listApplDetailRec = null;
    System.runAs(sysAdmin){
       
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
        Test.startTest();
        listApplDetailRec = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(new List<String> {appDetails.Id}  ,new List<String>{HDFC_DASH_Constants.STRING_ID},HDFC_DASH_Constants.ID_STRING);
             
        Test.stopTest();
    }
    system.assertNotEquals( null, listApplDetailRec);
    system.assertEquals(1,listApplDetailRec.size()); 
  }
  /* 
  Method Name :testGetApplicationDetailsByRecordType
  Description :This method will test the getApplicationDetailsByRecordType().
  */  
  static testmethod void testGetApplicationDetailsByRecordType(){

    List<HDFC_DASH_Applicant_Details__c>  listApplDetailRec = null;
    System.runAs(sysAdmin){
       
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
        Test.startTest();
        listApplDetailRec = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetailsByRecordType(
                              new List<String> {appDetails.Id},
                              new List<String>{HDFC_DASH_Constants.STRING_ID},
                              HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.RECORDTYPE_PRIMARYAPP );
             
        Test.stopTest();
    }
    system.assertNotEquals( null, listApplDetailRec);
    system.assertEquals(1,listApplDetailRec.size()); 
  }

  /* 
  Method Name :testGetApplicantDetailBasedOnId
  Description :This method will test the getApplicantDetailBasedOnId().
  */  
  static testmethod void testGetApplicantDetailBasedOnId(){   
    System.runAs(sysAdmin){
        HDFC_DASH_Applicant_Details__c resApplDetailRec;
           
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
        Test.startTest();  
         resApplDetailRec = HDFC_DASH_Applicant_Detail_Selector.getApplicantDetailBasedOnId(fieldList, appDetails.Id);           
        Test.stopTest();
        system.assertNotEquals( null, resApplDetailRec);
        system.assertEquals(appDetails.Id,resApplDetailRec.Id); 
    }
      
  }
  /* 
  Method Name :testGetAppDetailsBasedOnQuery
  Description :This method will test the getAppDetailsBasedOnQuery().
  */   
  static testmethod void testGetAppDetailsBasedOnQuery(){
    List<HDFC_DASH_Applicant_Details__c>  listApplDetailRec = null;
    HDFC_DASH_Applicant_Details__c applDetailRec = new HDFC_DASH_Applicant_Details__c();
    System.runAs(sysAdmin){
       
        List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name};
        Test.startTest();
        Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_Application =>
            HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+
            app.Id+HDFC_DASH_Constants.STRING_QUOTE};
        listApplDetailRec = HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(
                              new List<String>{HDFC_DASH_Constants.STRING_ID},condition);
             
        Test.stopTest();
    }
    system.assertNotEquals( null, listApplDetailRec);
    system.assertEquals(1,listApplDetailRec.size()); 
      
  }
     /* 
    Method Name :getappDetailRecsBasedOnString
    Parameters  :List<string> fieldList,String queryParameter,String conditionField
    Description :This method would get the List of Applicant Details based on single string condition sent to it.
    */
    static testmethod void testgetappDetailRecsBasedOnString()
    {
        
         List<HDFC_DASH_Applicant_Details__c> appDetailRecsList=null;
         List<String> fieldList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Contact,HDFC_DASH_Constants.HDFC_DASH_Application};
         System.runAs(sysAdmin)
        {
          Test.startTest();
         appDetailRecsList= HDFC_DASH_Applicant_Detail_Selector.getappDetailRecsBasedOnString(fieldList,con.id ,HDFC_DASH_Constants.HDFC_DASH_Contact);
         Test.stopTest();
        }
        system.assertNotEquals( null, appDetailRecsList);
        system.assertEquals(1,appDetailRecsList.size()); 

        
    }
}