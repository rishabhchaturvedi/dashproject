/* 
ClassName: HDFC_DASH_TestDataFactory_Application
DevelopedBy: Anisha
Date: 05 July 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Application object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_Application {
    
    public static final string NAME = 'Name';
    public static final string STAGE = 'Onboarding';
    public static final date CLOSEDDATE = Date.valueOf('2021-06-06'); 
    public static final string FILENUMBER = '6580';
    public static final string LEADID = '101793229';
    public static final string SOURCE_ID = '101793229';
    public static final string LEAD_BRANCH_ID = '201';
    
    /* Method Name: createBasicApplication
    parameters: NONE
    Method Description: This method is to create test data for Application.
    Pass fields of Application, and based on doInsert it will either
    pass an instance of uninserted application or inserted application id. 
    */
    public static Opportunity createBasicApplication(ID accountID)
    {
        Opportunity app = new Opportunity();
            app.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
            app.StageName = STAGE;
            app.CloseDate = CLOSEDDATE;
            app.AccountId = accountID;
            app.HDFC_DASH_File_Number__c = FILENUMBER+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
            app.HDFC_DASH_LMS_Lead_ID__c = LEADID;
            app.HDFC_DASH_LMS_Agency_Code__c = SOURCE_ID;
            app.HDFC_DASH_LMS_Origin_Branch_Id__c = LEAD_BRANCH_ID;
            //system.debug('app '+app);
        return app;        
    }
    
    /* Method Name: getBasicApplication
    parameters: NONE
    Method Description: This method is to generate test data for Application.
    */
    public static Opportunity getBasicApplication(ID accountID)
    {

        Opportunity app = HDFC_DASH_TestDataFactory_Application.createBasicApplication(accountID);
        if(Schema.sObjectType.Opportunity.isCreateable()){
            Database.insert(app,false);
        }
        return app;
    }
    
    /* Method Name: createBasicApplicationlist
    parameters: Integer numberOfApplications
    Method Description: This method is to create a list of test data for Application
    by calling a createBasicApplication method.
    */
   /* public static List<Opportunity> createBasicApplicationlist(Integer numberOfApplications, ID accountID)
    {
        list<Opportunity> applist = new list<Opportunity>();
        for(integer i = 0 ; i < numberOfApplications; i++){
            applist.add(HDFC_DASH_TestDataFactory_Application.createBasicApplication(accountID)); 
        }
        return applist;
    } */
    
    /* Method Name: getBasicApplicationlist
    parameters: Integer numberOfApplications
    Method Description: This method is to generate a list of test data for Application.
    */
   /* public static List<Opportunity> getBasicApplicationlist(Integer numberOfApplications, ID accountID)
    {
        list<Opportunity> applist = HDFC_DASH_TestDataFactory_Application.createBasicApplicationlist(numberOfApplications, accountID);
        Database.insert(applist,false);
        return applist;
    } */  
    
    /* Method Name: createAdditionalLoan
    parameters: NONE
    Method Description: This method is to create test data for Addition Loan. 
    */
    public static Opportunity createAdditionalLoan(ID accountID)
    {
        Opportunity app = new Opportunity();
            app.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
            app.StageName = STAGE;
            app.CloseDate = CLOSEDDATE;
            app.AccountId = accountID;
            app.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(HDFC_DASH_Constants.ADDITIONAL_LOAN).getRecordTypeId(); 

        return app;        
    }
    
    /* Method Name: getAdditionalLoan
    parameters: NONE
    Method Description: This method is to generate test data for Addition Loan.
    */
    public static Opportunity getAdditionalLoan(ID accountID)
    {
        Opportunity app = HDFC_DASH_TestDataFactory_Application.createAdditionalLoan(accountID);
        if(Schema.sObjectType.Opportunity.isCreateable()){
            Database.insert(app,false);
        }
        return app;
    }
    
    /* Method Name: createAdditionalLoanList
    parameters: Integer numberOfApplications
    Method Description: This method is to create a list of test data for Addition Loan
    by calling a createAdditionalLoan method.
    */
   /* public static List<Opportunity> createAdditionalLoanList(Integer numberOfApplications, ID accountID)
    {
        list<Opportunity> applist = new list<Opportunity>();
        for(integer i = 0 ; i < numberOfApplications; i++){
            applist.add(HDFC_DASH_TestDataFactory_Application.createAdditionalLoan(accountID)); 
        }
        return applist;
    } */
    
    /* Method Name: getAdditionalLoanList
    parameters: Integer numberOfApplications
    Method Description: This method is to generate a list of test data for Addition Loan.
    */
   /* public static List<Opportunity> getAdditionalLoanList(Integer numberOfApplications, ID accountID)
    {
        list<Opportunity> applist = HDFC_DASH_TestDataFactory_Application.createAdditionalLoanList(numberOfApplications, accountID);
        Database.insert(applist,false);
        return applist;
    }  */ 
    
}