/*@author        
Accenture 
@date           23/10/2017
@name           Batch_Interface_Retry 
@description    The purpose of this class is to send details of all failed logs.
Modification Log: 
------------------------------------------------------------------------------------
Developer                     Date                    Description  
------------------------------------------------------------------------------------ 
Accenture                    23/10/2017               Class for  Batch_Interface_Retry   
*/
global with sharing class IT_Daily_Failed_Logs_Details_Mail_Batch implements database.Stateful,
                                        database.Batchable<Sobject>,Database.AllowsCallouts{
 
 
    global List<Integration_Message_Log__c> failedLogsList = null;
    
    /***************************************************************************************************************
    *   @Name        :  IT_Daily_Failed_Logs_Details_Mail_Batch                                                              
    *   @Return      :  --                                                                                     
    *   @Description :  It is a constructor for class IT_Daily_Failed_Logs_Details_Mail_Batch                             
    ***************************************************************************************************************/
    global IT_Daily_Failed_Logs_Details_Mail_Batch(){
        failedLogsList = new List<Integration_Message_Log__c>();
    }   
    /***************************************************************************************************************
    *   @Name        :  Start                                                              
    *   @Return      :  database.QueryLocator                                                                                       
    *   @Description :  All Callout records to cross check whether retries required or not..                              
    ***************************************************************************************************************/
    global database.QueryLocator start(database.batchablecontext BC){
        String query = 'Select id,Name,Interface_Name__c'+ 
                       ' FROM Integration_Message_Log__c where Do_Retry__c= true and Callout_Date__c = Today and Consolidated_Email__c = true';
        return Database.getQueryLocator(query);
    }
    
    /***************************************************************************************************************
    *   @Name        :  Execute                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  Mail triggering to be done in execute methods.                              
    ***************************************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Integration_Message_Log__c> scope){
       
        failedLogsList = scope;
        /*sendDailyFailedLogMails classObj = new sendDailyFailedLogMails(scope);
        classObj.triggerLogEmail();*/
    }
    /***************************************************************************************************************
    *   @Name        :  finish                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  Mail triggering to be done in finish methods.                              
    ***************************************************************************************************************/
    global void finish(Database.BatchableContext BC) {
        sendDailyFailedLogMails classObj = new sendDailyFailedLogMails(failedLogsList);
        classObj.triggerLogEmail();
    }
}