/**
className: HDFC_DASH_RestResource_InfoValTest
DevelopedBy: Punam Marbate
Date: 27 Aug 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_InfoValidation and HDFC_DASH_InboundRestService_InfoVal.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_RestResource_InfoValTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
	//private static ContentVersion contentVer = HDFC_DASH_TestDataFactory_ContentDoc.getContestVersion();
   //private static String contenDocId = [select id, ContentDocumentId from ContentVersion where Id=: contentVer.id].ContentDocumentId;
    //private static ContentDocumentLink contentDocLink = HDFC_DASH_TestDataFactory_ContentDoc.getConDocLink(contenDocId,con.id);
    
    private static List <HDFC_DASH_Master_Table__c> masterData = HDFC_DASH_TestDataFactory_MasterTables.getBasicMasterTable();                                
    private static HDFC_DASH_City_Master__c cityMasterData = HDFC_DASH_TestDataFactory_MasterTables.getBasicCityMaster(); 
    private static HDFC_DASH_Pincode_Master__c pinCodeMasterData = HDFC_DASH_TestDataFactory_MasterTables.getBasicPincodeMaster();
    
    
    private static final string REQUEST_BODY ='{ "sfApplicationId": "'+ app.Id+'", "sfApplicantId": "'+appDetails.Id+'","sfCustomerId": "'+con.Id+'","customerNumber": "213432503","customerCapacity": "B","uploadedDate": "2017-10-30","applicantInfoValidation": { "validationType": "KYC", "source": "ckyc", "docType": "PAN", "docNumber": "ASD123DSedf", "salutation": "Mr.", "firstName": "SHIVAM", "middleName": "", "lastName": "MITTAL", "dateOfBirth": "2001-10-30", "genderCode": "M", "addressType": "C", "applicantImage": { "imageSource" : "Uploaded" , "validImage" : "Yes" , "fileName" : "TextFile" , "fileExtension": "txt", "base64File" : "VGhpcyBpcyBhIFNhbXBsZSB0ZXh0IGZpbGU="}, "prePopulatedPassportInformation": { "passportNumber": "123", "dateOfExpiry": "2001-10-30", "nationality": "Indian" }, "prePopulatedPermanentResidency": { "prNumber": "SFASF2352FSF", "prCountry": "AUSTRALIA", "prCountryCode": "AUSTRALIA", "prValidityDate": "2026-04-01" }, "prePopulatedAddressDetails": { "addressLine1": "123", "addressLine2": "Kamarajar street", "addressLine3": "ACHABAL", "addressLine4": "636701", "landmark": "police station", "city": "'+cityMasterData.HDFC_DASH_City_Name__c+'", "cityCode": "'+cityMasterData.HDFC_DASH_City_Code__c+'", "taluka": "Dharmapuri", "district": "Dharmapuri", "state": "'+masterData[0].HDFC_DASH_CD_Desc__c+'", "stateCode": "'+masterData[0].HDFC_DASH_CD_Val__c+'", "country": "INDIA", "countryCode": "INDIA", "pinCode": "'+pinCodeMasterData.HDFC_DASH_Pincode__c+'", "postOfficeName": "K2" }, "prePopulatedEmpGstBusinessDetails": { "gstNumber": "KBSLF755352", "empGstStateCode": "KA", "empGstState": "Karnataka" }, "prePopulatedBusinessInformation": { "designationAndEntityType": "PROPRIETOR", "businessName": "Robin Infra Pvt Ltd", "yearOfIncorporation": 1999, "annualTurnover": "5CR_TO_50CR", "natureOfBusiness": "SERVICES", "industryType": "SPORTS", "businessPAN": "BNIPA8045N", "udyamRegistrationNumber": "AEDDHT47293RWS", "shareholdingPercentage": 51, "cinNumber": "UL098MH01AAAPC1234D" }, "validationResponse": {} } }';
    private static final string REQUEST_BODY_WITHOUT_APPLICATION = '{"sfApplicationId":"","sfApplicantId":"","sfCustomerId": "'+con.Id+'","customerNumber": "213432503","customerCapacity": "B","uploadedDate": "2017-10-30","applicantInfoValidation":{"validationType":"KYC","source":"ckyc","docType":"PAN","docNumber":"ASD123DSedf","salutation":"MR","firstName":"SHIVAM","middleName":"","lastName":"MITTAL","dateOfBirth":"2001-10-30","genderCode":"M","addressType":"C", "applicantImage": { "imageSource" : "Uploaded" , "validImage" : "Yes" , "fileName" : "TextFile" , "fileExtension": "txt", "base64File" : "VGhpcyBpcyBhIFNhbXBsZSB0ZXh0IGZpbGU="} ,"prePopulatedPassportInformation":{"passportNumber":"123","dateOfExpiry":"2001-10-30","nationality":"Indian"},"prePopulatedPermanentResidency":{"prNumber":"SFASF2352FSF","prCountry":"AUSTRALIA","prCountryCode":"AUSTRALIA","prValidityDate":"2026-04-01"},"prePopulatedAddressDetails":{"addressLine1":"123","addressLine2":"Kamarajar street","addressLine3":"ACHABAL","addressLine4":"636701","landmark":"police station","city":"ACHABAL","cityCode":"ACHABAL","taluka":"Dharmapuri","district":"Dharmapuri","state":"JK","stateCode":"JK","country":"INDIA","countryCode":"INDIA","pinCode":"636701","postOfficeName":"K2"},"validationResponse":{}}}';
    /* 
    Method Name : testStoreInfoValidation
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_InfoValidation.storeInfoValidation().
    */
    static testmethod void testStoreInfoValidation()
    {
        System.runAs(sysAdmin)
        {               List<HDFC_DASH_City_Master__c> listcitymasterRec = new List<HDFC_DASH_City_Master__c>{cityMasterData} ;
            List<HDFC_DASH_Pincode_Master__c> listPincodeMastRec = new List<HDFC_DASH_Pincode_Master__c>{pinCodeMasterData};
            Id recordTypeIdStateCode = Schema.SObjectType.HDFC_DASH_Master_Table__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.MASTERTABLE_RECORDTYPE_STATECODE).getRecordTypeId();
            masterData[0].RecordTypeId=recordTypeIdStateCode;
            Database.update(masterData);
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_POST;
            request.requestBody = Blob.valueOf(REQUEST_BODY);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_InfoValidation.storeInfoValidation();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    /* 
    Method Name : testException_APPLICANTID_MISSING
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_RestResource_InfoValidation.storeInfoValidation() when Applicant ID is null.
    */
   static TestMethod void testException_APPLICANTID_MISSING() 
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            Exception testException;          
            RestRequest request = new RestRequest();
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_POST;
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITHOUT_APPLICATION);       
            RestContext.request = request;
            Test.startTest();   
            try
            {
                HDFC_DASH_RestResource_InfoValidation.storeInfoValidation();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            Test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    }
}