public class CaseCategoryUpdate implements Database.Batchable<sObject>{
	
    public static Database.QueryLocator start(Database.BatchableContext bc){
        
        string cat1 = 'Website / App Services';
        string cat2 = 'Accounts - Transaction Related';
        //string caseN = '00001153';
        String query = 'select id,casenumber,hdfc_dash_category__c,hdfc_dash_sub_category__c from Case where hdfc_dash_category__c =: cat1 or hdfc_dash_category__c =: cat2';
        system.debug('query start...'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc,List<Case> scope){
        List<Case> toBeUpdated = new List<Case>();
        for(Case c : scope){
            c.hdfc_dash_category__c = '';
            c.hdfc_dash_sub_category__c = '';
            
            toBeUpdated.add(c);
            
            system.debug('c.casenumber...'+ c.casenumber);
            system.debug('c.hdfc_dash_category__c...'+ c.hdfc_dash_category__c);
            system.debug('c.hdfc_dash_sub_category__c...'+ c.hdfc_dash_sub_category__c);
        }
        
        system.debug('toBeUpdated size...'+toBeUpdated.size());
        Database.update(toBeUpdated);
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
}