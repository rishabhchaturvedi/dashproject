/**
className: HDFC_DASH_UserCreation
DevelopedBy: Tejeswari
Date: 21 November 2021
Company: Accenture 
Class Description:  Queueable class for Creating Partner User.
*/
public with sharing class HDFC_DASH_UserCreation implements Queueable 
{
    private List<Contact> listOfContacts;
    
    /**
ConstructorName : HDFC_DASH_UserCreation
* Parameters : List<Contact> listContacts
* Description : It is a constructor for class HDFC_DASH_UserCreation.
*/
    public HDFC_DASH_UserCreation(List<Contact> listContacts){
        this.listOfContacts = listContacts;
    }
    /* 
Method Name :execute
Description :This method would be to creating the- partner user records .
*/
    public void execute(QueueableContext context)
    {
        string aliasName =HDFC_DASH_Constants.STRING_BLANK;
        list<String> listUserRoleName = new list<String>();
        List<User> listUsers = new list<User>();
        List<UserRole> listUserRole = new List<UserRole>();
        list<User> listUser =new list<User>();
        list<User> listUpdateUsers =new list<User>();
        Map<String,String> mapUserRole =new Map<String,String>();
        list<String> listUserIds =new list<String>();
        List<PermissionSetAssignment> listpsa = new List<PermissionSetAssignment>();
        List<GroupMember> listGroupMember =new List<GroupMember>(); 
        List<String> listFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.NAME};
            List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.NAME,HDFC_DASH_Constants.ACCOUNT_NAME,HDFC_DASH_Constants.USER_ROLEID,HDFC_DASH_Constants.CONTACT_ROLE};
                
                Profile profile = HDFC_DASH_SetupObjects_Selector.getProfile(listFields,HDFC_DASH_Constants.HDFC_PARTNER_COMMUNITY_USER);
        PermissionSet permissionSet = HDFC_DASH_SetupObjects_Selector.getPermissionSet(listFields,HDFC_DASH_Constants.SALES_USER_PERMISSION);
        Group groupRec = HDFC_DASH_SetupObjects_Selector.getPublicGroup(listFields,HDFC_DASH_Constants.SALES_USER_PUBLICGROUP);
        for(Contact conRec:listOfContacts)
        {
            User userRec= new User();
            userRec.FirstName = conRec.FirstName;
            userRec.LastName = conRec.LastName;
            userRec.MiddleName = conRec.MiddleName;
            userRec.Email = conRec.Email;
            userRec.FederationIdentifier = conRec.HDFC_DASH_Federation_Identifier__c;
            userRec.Username = conRec.HDFC_DASH_Federation_Identifier__c;
            userRec.EmailEncodingKey = HDFC_DASH_Constants.STRING_UTF;
            userRec.LanguageLocaleKey = HDFC_DASH_Constants.STRING_EN_US;
            userRec.TimeZoneSidKey = HDFC_DASH_Constants.STRING_ASIA_KOLKATA;
            userRec.LocaleSidKey = HDFC_DASH_Constants.STRING_EN_US;
            
            if(String.isNotBlank(conRec.FirstName))
               {
                   aliasName = conRec.FirstName.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_ONE);
               }
               if(conRec.LastName.length() >= 4)
               {
                   userRec.Alias = aliasName + conRec.LastName.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_FOUR);
               }
               else
               {
                   userRec.Alias = aliasName + conRec.LastName;
               }
            userRec.ContactId = conRec.Id;
            userRec.ProfileId =profile.Id;
            listUser.add(userRec);
        }
        
        //Database.DMLOptions dlo = new Database.DMLOptions();
        // dlo.EmailHeader.triggerUserEmail= false;
        
        Database.SaveResult[] result_Users = Database.insert(listUser, true);
        
        for(Database.SaveResult sr: result_Users)
        {
            PermissionSetAssignment permissionSetAssign = new PermissionSetAssignment (PermissionSetId = permissionSet.Id, AssigneeId = sr.getId());
            listpsa.add(permissionSetAssign);
            
            GroupMember groupMemberRec= new GroupMember(); 
            groupMemberRec.GroupId = groupRec.id;
            groupMemberRec.UserOrGroupId = sr.getId();
            listGroupMember.add(groupMemberRec);
            listUserIds.add(sr.getId());
        }
        Database.insert(listpsa);
        Database.insert(listGroupMember);
        
        if(!listUserIds.isEmpty())
        {
            listUsers = HDFC_DASH_User_Selector.getUser(listUserIds,listUserFields,HDFC_DASH_Constants.ID_STRING);
        }
        
        for(User userRec:listUsers)
        {
            if(!String.IsBlank(userRec?.account?.name))
            {
                string roleName_Manager = userRec.account.name + HDFC_DASH_Constants.PARTNER_MANAGER;
                string roleName_User = userRec.account.name + HDFC_DASH_Constants.PARTNER_USER;
                
                listUserRoleName.add(roleName_Manager);
                listUserRoleName.add(roleName_User);
            }
        }
        
        if(!listUserRoleName.isEmpty())
        {
            listUserRole = HDFC_DASH_SetupObjects_Selector.getUserRoles(listUserRoleName,listFields,HDFC_DASH_Constants.NAME);
        }
        
        for(UserRole userRole :listUserRole)
        {
            mapUserRole.put(userRole.Name, userRole.Id);
        }
        
        for(User userRec:listUsers)
        {
            if(userRec?.Contact?.HDFC_DASH_Function_Role_Code__c == HDFC_DASH_Constants.STRING_TL)
            {
                string roleName_Manager = userRec?.account?.name + HDFC_DASH_Constants.PARTNER_MANAGER;
                if(mapUserRole.containsKey(roleName_Manager))
                {
                    if(userRec.UserRoleId != mapUserRole.get(roleName_Manager))
                    {
                        userRec.UserRoleId = mapUserRole.get(roleName_Manager);
                        listUpdateUsers.add(userRec);
                    }
                }
            }
            else if(userRec?.Contact?.HDFC_DASH_Function_Role_Code__c == HDFC_DASH_Constants.STRING_TE)
            {
                string roleName_User = userRec?.account?.name + HDFC_DASH_Constants.PARTNER_USER;
                if(mapUserRole.containsKey(roleName_User))
                {
                    if(userRec.UserRoleId != mapUserRole.get(roleName_User))
                    {
                        userRec.UserRoleId = mapUserRole.get(roleName_User);
                        listUpdateUsers.add(userRec);
                    }
                }
            }
        }
        Database.update(listUpdateUsers);
    }
}