/**
* 	className: HDFC_DASH_ProcessExceptionTriggerOps
DevelopedBy: Anmol
Date: 26 May 2021
Company: Accenture
Class Description: This Class contains the trigger operation to process Exception 
and log it in exception log.
**/
public with sharing class HDFC_DASH_ProcessExceptionTriggerOps {
/**
* 	className: ProcessExcep
DevelopedBy: Anmol
Date: 26 May 2021
Company: Accenture
Class Description: This Class contains the trigger operation to process Exception 
and log it in exception log.
**/
    public with sharing class ProcessExcep implements HDFC_DASH_TriggerOps{
/* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
     public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.proc_ExcepIsFirstRun
             && HDFC_DASH_Constants.TRUESTR.equalsignorecase(system.Label.HDFC_DASH_ProcessExcepFlag);
            
     }
 /* 
Method Name :filter
Description :This method would filter for the records.
*/
     public SObject[] filter(){
            return Trigger.new; 
     }
 /* 
Method Name :execute
Description :This method would execute the logic.
*/
     public void execute(HDFC_DASH_Exception_Logs__e[] pexcList){
     	List<HDFC_DASH_Exception_Log__c> eceList = new List<HDFC_DASH_Exception_Log__c>();
        	for(HDFC_DASH_Exception_Logs__e ex :pexcList){
                HDFC_DASH_Exception_Log__c excep = new HDFC_DASH_Exception_Log__c();
                excep.HDFC_DASH_Object__c = ex.HDFC_DASH_Object__c;
                excep.HDFC_DASH_Operation__c = ex.HDFC_DASH_Operation__c ;
                excep.HDFC_DASH_Exception_Details__c = ex.HDFC_DASH_Exception_Details__c ;
                excep.HDFC_DASH_Record_ID__c = ex.HDFC_DASH_Record_ID__c;
                eceList.add(excep);
            }
            
            if((eceList.size()>0)
            &&Schema.sObjectType.HDFC_DASH_Exception_Log__c.fields.HDFC_DASH_Object__c .isAccessible()
            &&Schema.sObjectType.HDFC_DASH_Exception_Log__c.fields.HDFC_DASH_Operation__c.isAccessible()
            &&Schema.sObjectType.HDFC_DASH_Exception_Log__c.fields.HDFC_DASH_Record_ID__c.isAccessible()
            &&Schema.sObjectType.HDFC_DASH_Exception_Log__c.fields.HDFC_DASH_Exception_Details__c.isAccessible()){
            Database.insert(eceList);
            }
            HDFC_DASH_TriggerOpsRecursionFlags.proc_ExcepIsFirstRun = false;
        }
    }     
}