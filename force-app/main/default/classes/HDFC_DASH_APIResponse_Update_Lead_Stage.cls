/*
Class: HDFC_DASH_APIResponse_Update_Lead_Stage
Author:Sai Suman	
Date: 27 Nov 2021
Company: Accenture
Description:This class will define the response body of Update Lead Stage callout 
*/
public class HDFC_DASH_APIResponse_Update_Lead_Stage {
	public String return_cd;
    public String return_msg;
    public String lead_no;
    public String lead_Details;
    public String appln_form;
    public String new_lead_no;
    
/*
Method: HDFC_DASH_APIResponse_Update_Lead_Stage
Description: Method for Response Parsing
*/	
		public static HDFC_DASH_APIResponse_Update_Lead_Stage parse(HttpResponse response) {
        string json = response.getBody();
		return (HDFC_DASH_APIResponse_Update_Lead_Stage) System.JSON.deserialize(json, HDFC_DASH_APIResponse_Update_Lead_Stage.class);
	}
}