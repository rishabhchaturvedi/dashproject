/* Author: Anisha Arumugam
    Class: HDFC_DASH_MasterTables_Selector
    Description: Apex class for querying master data
 */
public inherited sharing class HDFC_DASH_MasterTables_Selector {
    
    private static final string SELECT_STRING = 'Select ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string AND_STRING = ' and ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    private static final string QUERY_RECORDTYPE = '=:recordTypeId';
    private static final string STRING_RECORDTYPEID = 'RecordTypeId ';
    
    private static final string FROM_MASTERTABLE = ' from HDFC_DASH_Master_Table__c ';
    private static final string MASTERTABLE_CONDITION = 'HDFC_DASH_CD_Val__c ';
    private static final string MASTERTABLE_SELECTVALUE = 'HDFC_DASH_CD_Desc__c ';
    
    private static final string FROM_CITYMASTER = ' from HDFC_DASH_City_Master__c ';
    private static final string CITYMASTER_CONDITION = 'HDFC_DASH_City_Code__c ';
    private static final string CITYMASTER_SELECTVALUE = 'HDFC_DASH_City_Name__c ';
    
    private static final string FROM_PINCODEMASTER = ' from HDFC_DASH_Pincode_Master__c ';
    private static final string PINCODEMASTER_CONDITION = 'HDFC_DASH_Pincode__c ';
    private static final string PINCODEMASTER_SELECTVALUE = 'HDFC_DASH_Place__c ';
        
    private static final string FROM_PROJECTMASTER = ' from HDFC_DASH_Project_Master__c ';
    private static final string PROJECTMASTER_CONDITION = 'HDFC_DASH_Project_UID__c ';
    private static final string PROJECTMASTER_SELECTVALUE = 'HDFC_DASH_Project_Name__c ';
    
    private static final string FROM_EMPMASTER = ' from Account ';
    private static final string EMPMASTER_CONDITION = 'HDFC_DASH_Corp_Cust_No__c ';   
    private static final string EMPMASTER_SELECTVALUE = 'Name '; 
    private static final string EMPMASTER_HDFCEMP_RECORDTYPENAME = 'HDFC Employer Record Type';

    private static final string FROM_IMGDOCMASTER = ' from HDFC_DASH_Image_Document_Master__c ';
    private static final string IMGDOCMASTER_CONDITION = 'HDFC_DASH_Doc_Type__c ';   
    private static final string IMGDOCMASTER_SELECTVALUE = 'HDFC_DASH_Appl_Name__c ';

    private static final string FROM_KYCDOCMASTER = ' from HDFC_DASH_KYC_Document_Master__c ';
    private static final string KYCDOCMASTER_CONDITION = 'HDFC_DASH_Doc_Code__c ';   
    private static final string KYCDOCMASTER_SELECTVALUE = 'HDFC_DASH_Doc_Desc__c ';
    
    private static final string FROM_PROPLOANMASTER = ' from HDFC_DASH_Property_Loan_Master__c ';
    private static final string LOANAVAILEDMASTER_CONDITION = 'HDFC_DASH_Loan_Purpose_Desc__c ';   
    private static final string LOANAVAILEDMASTER_SELECTVALUE = 'HDFC_DASH_Loan_Purpose__c ';
    
    private static final string INSTITUTIONMASTER_CONDITION = 'HDFC_DASH_Institution_Code__c';   
    private static final string INSTITUTIONMASTER_SELECTVALUE = 'HDFC_DASH_Institution_Name__c';
    
    private static final string RERAMASTER_CONDITION = 'HDFC_DASH_RERA_Id__c';   
    private static final string RERAMASTER_SELECTVALUE = 'HDFC_DASH_Project_Name__c';
    
    private static final string BankDetails_CONDITION = 'HDFC_DASH_Bank_Cd__c';   
    private static final string BankDetails_SELECTVALUE = 'HDFC_DASH_Bank_Name__c';
    
    /*
      Name: getMasterTable
      Description: Method to fetch data from Master Table
    */
    public static List<HDFC_DASH_Master_Table__c> getMasterTable(List<String> queryParameters)
    {   
        List<String> listOfFields = new List<String>{ID_STRING, MASTERTABLE_SELECTVALUE, MASTERTABLE_CONDITION};
            
        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_MASTERTABLE + WHERE_STRING + MASTERTABLE_CONDITION + QUERY_PARAMETER;  
        
        List<HDFC_DASH_Master_Table__c> resultList = database.query(querystring); 
        
        
        return resultList;
    }  
    
    /*
      Name: getMasterTableWithRecordType
      Description: Method to fetch data from Master Table with record type
    */
    public static List<HDFC_DASH_Master_Table__c> getMasterTableWithRecordType(List<String> queryParameters, String recordType)
    {   
        Id recordTypeId = Schema.SObjectType.HDFC_DASH_Master_Table__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();

        List<String> listOfFields = new List<String>{ID_STRING, MASTERTABLE_SELECTVALUE, MASTERTABLE_CONDITION};
            
        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_MASTERTABLE + WHERE_STRING + MASTERTABLE_CONDITION + QUERY_PARAMETER
             + AND_STRING + STRING_RECORDTYPEID + QUERY_RECORDTYPE;  

        List<HDFC_DASH_Master_Table__c> resultList = database.query(querystring); 

        return resultList;
    }  
       
    /*
      Name: getCityMaster
      Description: Method to fetch data from City Master Table
    */
    public static List<HDFC_DASH_City_Master__c> getCityMaster(List<String> queryParameters)
    {   
        List<String> listOfFields = new List<String>{ID_STRING, CITYMASTER_SELECTVALUE, CITYMASTER_CONDITION};

        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_CITYMASTER + WHERE_STRING + CITYMASTER_CONDITION + QUERY_PARAMETER;  

        List<HDFC_DASH_City_Master__c> resultList = database.query(querystring);        
        
        return resultList;
    } 
    
    /*
      Name: getPinCodeMaster
      Description: Method to fetch data from Pincode Master Table
    */
    public static List<HDFC_DASH_Pincode_Master__c> getPinCodeMaster(List<String> queryParameters)
    {   
        List<String> listOfFields = new List<String>{ID_STRING, PINCODEMASTER_SELECTVALUE, PINCODEMASTER_CONDITION};

        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_PINCODEMASTER + WHERE_STRING + PINCODEMASTER_CONDITION + QUERY_PARAMETER;  

        List<HDFC_DASH_Pincode_Master__c> resultList = database.query(querystring);        
        
        return resultList;
    } 
    
    /*
      Name: getProjectMaster
      Description: Method to fetch data from Project Master Table
    */
    public static List<HDFC_DASH_Project_Master__c> getProjectMaster(List<String> queryParameters)
    {   
        List<String> listOfFields = new List<String>{ID_STRING, PROJECTMASTER_SELECTVALUE, PROJECTMASTER_CONDITION};

        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_PROJECTMASTER + WHERE_STRING + PROJECTMASTER_CONDITION + QUERY_PARAMETER;  

        List<HDFC_DASH_Project_Master__c> resultList = database.query(querystring);        
        
        return resultList;
    } 
    
   /*
      Name: getEmpMaster
      Description: Method to fetch data from Employer Master Table
    */
    public static List<Account> getEmpMaster(List<String> queryParameters)
    {   
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(HDFC_DASH_Constants.HDFC_EMPLOYER_RECORD_TYPE).getRecordTypeId();

        List<String> listOfFields = new List<String>{ID_STRING, EMPMASTER_SELECTVALUE, EMPMASTER_CONDITION};

        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_EMPMASTER + WHERE_STRING + EMPMASTER_CONDITION + QUERY_PARAMETER 
            + AND_STRING + STRING_RECORDTYPEID + QUERY_RECORDTYPE;  

        List<Account> resultList = database.query(querystring);        

        return resultList;
    } 
     /*
      Name: getKYCDocMasterWithRecordType
      Description: Method to fetch data from Employer Master Table
    */
    public static List<HDFC_DASH_KYC_Document_Master__c> getKYCDocMasterWithRecordType(List<String> queryParameters, String recordType)
    {   
       	Id recordTypeId = Schema.SObjectType.HDFC_DASH_KYC_Document_Master__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        List<String> listOfFields = new List<String>{ID_STRING, KYCDOCMASTER_SELECTVALUE, KYCDOCMASTER_CONDITION};

        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_KYCDOCMASTER + WHERE_STRING + KYCDOCMASTER_CONDITION + QUERY_PARAMETER + AND_STRING + STRING_RECORDTYPEID + QUERY_RECORDTYPE;  

        List<HDFC_DASH_KYC_Document_Master__c> resultList = database.query(querystring);        

        return resultList;
    } 
    /*
      Name: getImageDocMaster
      Description: Method to fetch data from Employer Master Table
    */
    public static List<HDFC_DASH_Image_Document_Master__c> getImageDocMaster(List<String> queryParameters)
    {   
        List<String> listOfFields = new List<String>{ID_STRING, IMGDOCMASTER_SELECTVALUE, IMGDOCMASTER_CONDITION};

        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_IMGDOCMASTER + WHERE_STRING + IMGDOCMASTER_CONDITION + QUERY_PARAMETER;  

        List<HDFC_DASH_Image_Document_Master__c> resultList = database.query(querystring);        

        return resultList;
    }
      
    /* 
      Name: getLoanAvailedMaster
      Description: Method to fetch data from Property Loan Master with record type
    */
    /*public static List<HDFC_DASH_Property_Loan_Master__c> getLoanAvailedMaster(List<String> queryParameters)
    {   
        Id recordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.LOAN_AVAILED_Master).getRecordTypeId();

        List<String> listOfFields = new List<String>{ID_STRING, LOANAVAILEDMASTER_SELECTVALUE, LOANAVAILEDMASTER_CONDITION};
            
        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_PROPLOANMASTER + WHERE_STRING + LOANAVAILEDMASTER_CONDITION + QUERY_PARAMETER
             + AND_STRING + STRING_RECORDTYPEID + QUERY_RECORDTYPE;  

        List<HDFC_DASH_Property_Loan_Master__c> resultList = database.query(querystring); 

        return resultList;
    }  */
       
   
    /*
      Name: getInstitutionMaster
      Description: Method to fetch data from Property Loan Master with record type
    */
    /*public static List<HDFC_DASH_Property_Loan_Master__c> getInstitutionMaster(List<String> queryParameters)
    {   
        Id recordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.INSTITUTION_Master).getRecordTypeId();

        List<String> listOfFields = new List<String>{ID_STRING, INSTITUTIONMASTER_SELECTVALUE, INSTITUTIONMASTER_CONDITION};
            
        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_PROPLOANMASTER + WHERE_STRING + INSTITUTIONMASTER_CONDITION + QUERY_PARAMETER
             + AND_STRING + STRING_RECORDTYPEID + QUERY_RECORDTYPE;  

        List<HDFC_DASH_Property_Loan_Master__c> resultList = database.query(querystring); 

        return resultList;
    }  */
    
       
    /*
      Name: getRERAMaster
      Description: Method to fetch data from Property Loan Master with record type
    */
    /*public static List<HDFC_DASH_Property_Loan_Master__c> getRERAMaster(List<String> queryParameters)
    {   
        Id recordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.RERA_MASTER).getRecordTypeId();

        List<String> listOfFields = new List<String>{ID_STRING, RERAMASTER_SELECTVALUE, RERAMASTER_CONDITION};
            
        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_PROPLOANMASTER + WHERE_STRING + RERAMASTER_CONDITION + QUERY_PARAMETER
             + AND_STRING + STRING_RECORDTYPEID + QUERY_RECORDTYPE;  

        List<HDFC_DASH_Property_Loan_Master__c> resultList = database.query(querystring); 

        return resultList;
    }  */


    /*
      Name: getBankDetailsMaster
      Description: Method to fetch data from Property Loan Master with record type
    */    
     /*public static List<HDFC_DASH_Property_Loan_Master__c> getBankDetailMaster(List<String> queryParameters)
    {   
        Id recordTypeId = Schema.SObjectType.HDFC_DASH_Property_Loan_Master__c.getRecordTypeInfosByName().get(HDFC_DASH_Constants.Bank_Details_Master).getRecordTypeId();

        List<String> listOfFields = new List<String>{ID_STRING, BankDetails_SELECTVALUE, BankDetails_CONDITION};
            
        string querystring = SELECT_STRING + String.join(listOfFields, HDFC_DASH_Constants.COMMA) + FROM_PROPLOANMASTER + WHERE_STRING + BankDetails_CONDITION + QUERY_PARAMETER
             + AND_STRING + STRING_RECORDTYPEID + QUERY_RECORDTYPE;  

        List<HDFC_DASH_Property_Loan_Master__c> resultList = database.query(querystring); 

        return resultList;
    }  */

}