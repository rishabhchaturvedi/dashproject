/**
className: HDFC_DASH_ApplFinancialInfo_Selector
DevelopedBy:Punam Marbate
Date: 27 sep 2021
Company: Accenture
Class Description: This class would create the select queries for Applicant Financial Info object .
**/
public with sharing class HDFC_DASH_ApplFinancialInfo_Selector {
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Applicant_Financial_Info__c ';
    private static final string WHERE_STRING = 'where '; 
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    /* 
    Method Name :getAppFinInfoBasedOnId
    Parameters  :List<string> fieldList, Id appFinInfoid
    Description :This method would get a single appFinInfoid  based on the ID.
    */
    /*public static HDFC_DASH_Applicant_Financial_Info__c getAppFinInfoBasedOnId
        (List<string> fieldList, Id appFinInfoid)
    {
        string querystring = SELECT_STRING+String.join(fieldList, HDFC_DASH_Constants.COMMA)
        +FROM_OBJECT+ WHERE_STRING+ ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO
        +HDFC_DASH_Constants.STRING_QUOTE+appFinInfoid+ HDFC_DASH_Constants.STRING_QUOTE;
        HDFC_DASH_Applicant_Financial_Info__c appFinInfoDetail = database.query(querystring);
        return appFinInfoDetail;
    }*/
    /* 
    Method Name :getAppFinInfoBasedOnIds
    Parameters  :List<string> fieldList,List<ID> appFinInfoIds
    Description :This method would get the AppFinInfo based on IDs.
    */
    /*public static List<HDFC_DASH_Applicant_Financial_Info__c> getAppFinInfoBasedOnIds
        (List<string> fieldList,List<ID> appFinInfoIds)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA)
        +FROM_OBJECT+WHERE_STRING+HDFC_DASH_CONSTANTS.STRING_ID
        +HDFC_DASH_CONSTANTS.STRING_IN
        +HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE
        +String.join(appFinInfoIds,HDFC_DASH_Constants.STRING_QUOTE
        +HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)
        +HDFC_DASH_Constants.STRING_QUOTE
        +HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<HDFC_DASH_Applicant_Financial_Info__c> appFinInfoList = database.query(querystring);
        return appFinInfoList;
    }*/
    /*
    Method Name :getApplFinInfoBasedOnAppDetailIds
    Parameters  :List<string> fieldList,List<ID> appDetailIds
    Description :This method would get the HDFC_DASH_Applicant_Financial_Info based on HDFC_DASH_Applicant_Details__c IDs.
    */
    /*public static List<HDFC_DASH_Applicant_Financial_Info__c> getApplFinInfoBasedOnAppDetailIds
    (List<string> fieldList,List<ID> appDetailIds)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT 
        +String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT + WHERE_STRING 
        +HDFC_DASH_CONSTANTS.HDFC_DASH_APPDETAILS + HDFC_DASH_CONSTANTS.STRING_IN 
        +HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE
        +String.join(appDetailIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)
        +HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<HDFC_DASH_Applicant_Financial_Info__c> appFinInfoList = database.query(querystring); 
        return appFinInfoList;
    }*/
     /* 
    Method Name :getApplicantFinancialInfo
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get the List of Applicant Financial details based on only one condition.
    */
    public static List<HDFC_DASH_Applicant_Financial_Info__c> getApplicantFinancialInfo(List<String> queryParameters, List<string> fieldList, String conditionOn)
    {
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;  
        List<HDFC_DASH_Applicant_Financial_Info__c> appFinInfoList = database.query(querystring);
        return appFinInfoList;
    }   
   
}