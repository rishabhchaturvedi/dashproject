/*
Class: HDFC_DASH_APIRequest_Update_Lead_Stage
Author:Ayesha Gavandi	
Date: 24 Nov 2021
Company: Accenture
Description:This class will define the request body of Update Lead Stage callout 
*/
public class HDFC_DASH_APIRequest_Update_Lead_Stage {
	public String lead_no;
    public String lead_stage;
    public String file_no ;
}