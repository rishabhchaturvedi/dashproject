/**
* 	className: HDFC_DASH_Application_Selector
DevelopedBy: Anmol
Date: 06 July 2021
Company: Accenture
Class Description: This class would create the select queries for Application(Oppotrunity) object .
**/
public inherited sharing class HDFC_DASH_Application_Selector {
    
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from Opportunity ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    private static final string QUERY_PARAMETER2 = 'queryParameters';

    /* 
    Method Name :getApplicationBasedOnId
    Parameters  :List<string> fieldList, Id ApplicationId
    Description :This method would get a single Application based on the ID.
    */   
    public static Opportunity getApplicationBasedOnId(List<string> fieldList, Id ApplicationId){
        
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT+ WHERE_STRING+ ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ApplicationId+ HDFC_DASH_Constants.STRING_QUOTE;
        system.debug('querystring'+querystring);
        Opportunity applicationRec = database.query(querystring); 
         
        return applicationRec;
    }
    /* 
    Method Name :getApplicationBasedOnIds
    Parameters  :List<string> fieldList,List<ID> applicationIds
    Description :This method would get the Applications based on the IDs.
    */
    public static List<Opportunity> getApplicationBasedOnIds(List<string> fieldList,List<ID> applicationIds){
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT+ WHERE_STRING+ HDFC_DASH_CONSTANTS.STRING_ID + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(applicationIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
        List<Opportunity> applList = database.query(querystring); 
        return applList;
    }

    
    /* 
    Method Name :getApplication
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get the List of Applications based on only one condition.
    */
    public static List<Opportunity> getApplication( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER + HDFC_DASH_CONSTANTS.STRING_ORDERBY;  

        List<Opportunity> applicationList = database.query(querystring);        

        return applicationList;
    }   

/* 
    Method Name :getApplicationBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Application based on fieldsAndparameters sent to it.
    */
    public static List<Opportunity> getApplicationBasedOnQuery(List<String> fieldList,
                                                            Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +
                                FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<Opportunity> applicationList = database.query(querystring); 
        return applicationList;   
    }
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getApplicationBasedOnQuery .
    */
    public static String getCondition(Map<String,String> fieldsAndparameters){
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }
    
    /* 
    Method Name :getAppRecsOrderBy
    Parameters  :List<String> fieldList,List<String> relatedapplications
    Description :This method would get the List of Application based on field order by.
    */
    public static List<Opportunity> getAppRecsOrderBy(List<string> fieldList,List<String> queryParameters){
		string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
            				 +FROM_OBJECT+ WHERE_STRING+ HDFC_DASH_Constants.ID_STRING
            				 +HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_CONSTANTS.COLON + QUERY_PARAMETER2 + HDFC_DASH_CONSTANTS.STRING_ORDERBY;
        List<Opportunity> appContactRecsList = database.query(querystring); 
        return appContactRecsList;
    }

}