/**
className: HDFC_DASH_InboundRestSer_GetActivities
DevelopedBy:  Anisha Arumugam
Date: 23 Sep 2021
Company: Accenture
Class Description: Service Class for getActivities API
**/
public with sharing class HDFC_DASH_InboundRestSer_GetActivities
{
    public static String param_TL, param_Type, param_StartDateTime, param_EndDateTime, param_Status;
    public static String param_SO, param_BSA, param_FileNumber, param_LeadId, param_ShowAll,param_BSAOwner;
    public static String contactId;
    public static String accountId;
    public static List<Opportunity> list_App = new  List<Opportunity>();
    public static List<Lead> list_Lead = new List<Lead>();
    public static List<String> listOfBSA = new  list<String>();
    public static Map<String,Contact> mapBsa = new Map<String,Contact>();
    public static Map<String,String> mapFilenumber = new Map<String,String>();
    public static Map<String,String> mapAccountSourceRSNO = new Map<String,String>();
    public static Map<String,String> mapLeadId = new Map<String,String>();
    public static Map<String,String> mapHuddleSO = new Map<String,String>();
    public static Map<String,User> mapUsers = new Map<String,User>();
    public static List<String> listOfFields_Contact = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Executive_Code};
        public static List<String> listOfFields_Account = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Source_Rec_SrNo}; 
            /* 
Method:getActivities
Description: Method to get a list of Activities (Events).
*/
            public static HDFC_DASH_ParseActivities getActivities(RestRequest request)
        {      
            //get a list of Activities
            HDFC_DASH_ParseActivities apiRes = generateActivitiesResponse(request);
            return apiRes;  
        }
    
    /* 
Method: generateActivitiesResponse
Description: Method to get the list of Activities (Events).
*/
    public static HDFC_DASH_ParseActivities generateActivitiesResponse(RestRequest request)
    {            
        HDFC_DASH_ParseActivities apiRes = new HDFC_DASH_ParseActivities();
        
        List<HDFC_DASH_ParseActivities.Activities> listActivities =  new List<HDFC_DASH_ParseActivities.Activities>();
        List<Event> listEvents = New List<Event>();
        List<Contact> listContacts = New List<Contact>();
        List<Account> listAccounts = New List<Account>();
        
        Map<String,String> mapCondition = new Map<string,String>();
        
        
        List<String> listOfFields_App = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_name, HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID};
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.EVENT_SUBJECT,HDFC_DASH_Constants.EVENT_TYPE,HDFC_DASH_Constants.STATUS,HDFC_DASH_Constants.EVENT_Description,
                HDFC_DASH_Constants.EVENT_StartDateTime,HDFC_DASH_Constants.EVENT_EndDateTime,HDFC_DASH_Constants.HDFC_DASH_Location_Latitude,HDFC_DASH_Constants.HDFC_DASH_Location_Longitude,
                HDFC_DASH_Constants.HDFC_DASH_BSA_AssignedId,HDFC_DASH_Constants.HDFC_DASH_BSA,HDFC_DASH_Constants.EVENT_WHATID,HDFC_DASH_Constants.EVENT_OWNERID,HDFC_DASH_Constants.EVENT_WHOID,
                HDFC_DASH_Constants.HDFC_DASH_Actual_End_DateTime,HDFC_DASH_Constants.HDFC_DASH_Meeting_Type,HDFC_DASH_Constants.HDFC_DASH_Is_Missed,HDFC_DASH_Constants.HDFC_DASH_Activity_Number,HDFC_DASH_Constants.HDFC_DASH_Application_Status,HDFC_DASH_Constants.HDFC_DASH_Application_Stage,
                HDFC_DASH_Constants.HDFC_DASH_Subject_Name_From_OS};    
                    List<String> listOfFields_Lead = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID };        
                        Map<String,String> mapConditionForBsa = new Map<String,String>();
        Map<String,String> mapConditionForSource = new Map<String,String>();
        param_Type = RestContext.request.params.get(HDFC_DASH_Constants.STRING_TYPE);
        param_StartDateTime = RestContext.request.params.get(HDFC_DASH_Constants.STRING_STARTDATETIME);
        param_EndDateTime = RestContext.request.params.get(HDFC_DASH_Constants.STRING_ENDDATETIME);
        param_TL = RestContext.request.params.get(HDFC_DASH_Constants.STRING_TL);
        param_SO = RestContext.request.params.get(HDFC_DASH_Constants.STRING_SO);
        param_BSA = RestContext.request.params.get(HDFC_DASH_Constants.STRING_BSA);
        param_Status = RestContext.request.params.get(HDFC_DASH_Constants.STRING_EVENTSTATUS);
        param_FileNumber = RestContext.request.params.get(HDFC_DASH_Constants.STRING_FILENUMBER);
        param_LeadId = RestContext.request.params.get(HDFC_DASH_Constants.STRING_LEADID);
        param_ShowAll = RestContext.request.params.get(HDFC_DASH_Constants.STRING_SHOWALL);
        param_BSAOwner = RestContext.request.params.get(HDFC_DASH_Constants.STRING_BSAOWNER);
        
        if((String.isNotBlank(param_TL) && String.isBlank(param_SO) && String.isBlank(param_BSAOwner)) || (String.isNotBlank(param_SO) && String.isBlank(param_TL) && String.isBlank(param_BSAOwner)) ||(String.isNotBlank(param_BSAOwner) && String.isBlank(param_TL) && String.isBlank(param_SO)) ||(String.isBlank(param_TL) && String.isBlank(param_SO) && String.isBlank(param_BSAOwner)))
        {
            if(param_Type == HDFC_DASH_Constants.STRING_ACTIVITY || param_Type == HDFC_DASH_Constants.STRING_HUDDLE)
            {
                //Check if the File Number exist
                if(String.isNotBlank(param_FileNumber))
                {
                    Map<String,String> condition= new Map<string,String>{HDFC_DASH_Constants.STRING_name => HDFC_DASH_Constants.STRING_EQUALTO
                        +HDFC_DASH_Constants.STRING_QUOTE+param_FileNumber+HDFC_DASH_Constants.STRING_QUOTE};      
                            list_App = HDFC_DASH_Application_Selector.getApplicationBasedOnQuery(listOfFields_App,condition);
                }                
                if(String.isNotBlank(param_FileNumber) && list_App.isEmpty())
                {
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_FILENUMBER_EXP); 
                }
                
                //Check if the Lead Id exist
                if(String.isNotBlank(param_LeadId))
                {
                    Map<String,String> condition = new Map<string,String>{HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID => HDFC_DASH_Constants.STRING_EQUALTO
                        +HDFC_DASH_Constants.STRING_QUOTE+param_LeadId+HDFC_DASH_Constants.STRING_QUOTE};
                            list_Lead =  HDFC_DASH_Lead_Selector.getLeadsBasedOnQuery(listOfFields_Lead, condition);
                    
                }
                if(String.isNotBlank(param_LeadId) && list_Lead.isEmpty())
                {
                    
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_LEADID_EXP); 
                }
                
                //Check if BSA exist
                if(String.isNotBlank(param_BSA) && String.isBlank(param_BSAOwner))
                {
                    mapConditionForSource.put(HDFC_DASH_Constants.HDFC_DASH_Source_Rec_SrNo, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_BSA + HDFC_DASH_Constants.STRING_QUOTE);                    
                }
                //Check if BSA owner exist
                if(String.isNotBlank(param_BSAOwner) && String.isBlank(param_BSA))
                {
                    mapConditionForBsa.put(HDFC_DASH_Constants.HDFC_DASH_Executive_Code, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_BSAOwner + HDFC_DASH_Constants.STRING_QUOTE);
                }
                if(!mapConditionForBsa.isEmpty())
                {
                    listContacts = HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(listOfFields_Contact,mapConditionForBsa);
                    if(!listContacts.isEmpty())
                    {
                        contactId = listContacts[HDFC_DASH_Constants.INT_ZERO]?.Id;
                    } 
                }
                if(!mapConditionForSource.isEmpty())
                {
                    listAccounts = HDFC_DASH_Account_Selector.getAccountsBasedOnQuery(listOfFields_Account,mapConditionForSource);
                    if(!listAccounts.isEmpty())
                    {
                        accountId = listAccounts[HDFC_DASH_Constants.INT_ZERO]?.Id;
                    }
                }
                
                if(String.isNotBlank(param_BSA) && String.isBlank(accountId))   
                {                    
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_BSA_EXP);  
                }
                
                if(String.isNotBlank(param_BSAOwner) && String.isBlank(contactId))
                {
                    HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_BSAOWNER_EXP); 
                }
                
                HDFC_DASH_Config_Detail__mdt mdt_ActivitiQueryLimit = HDFC_DASH_Config_Detail__mdt.getInstance(HDFC_DASH_Constants.HDFC_DASH_ACTIVITY_QUERY_LIMIT);         
                Integer queryLimit = Integer.ValueOf(mdt_ActivitiQueryLimit.HDFC_DASH_Value__c);
                
                mapCondition = getQueryCondition();
                if(!mapCondition.isEmpty()){
                   if(param_ShowAll == HDFC_DASH_Constants.TRUESTR || param_Type == HDFC_DASH_Constants.STRING_HUDDLE)
                    {
                        listEvents = HDFC_DASH_Event_WOSharing_Selector.getEventBasedOnLimit(fieldList,mapCondition,queryLimit);
                    }
                    else
                    {
                        listEvents = HDFC_DASH_Event_Selector.getEventBasedOnLimit(fieldList,mapCondition,queryLimit);
                    }
                }
                Map<string,string> mapAssignedToNames = getDataFromLookup(listEvents);
                
                for(Event e : listEvents)
                {
                    HDFC_DASH_ParseActivities.Activities activity = new HDFC_DASH_ParseActivities.Activities();
                    activity.startDateTime = e.StartDateTime;
                    activity.endDateTime = e.EndDateTime;
                    activity.title = e.Subject;
                    activity.type = e.Type;
                    activity.sfActivityId = e.Id;
                    activity.sfActivityNumber = e.HDFC_DASH_Activity_Number__c;
                    activity.status=e.HDFC_DASH_Status__c;
                    activity.description = e.Description;
                    activity.actualEndDateTime = e.HDFC_DASH_Actual_End_DateTime__c;
                    activity.meetingType = e.HDFC_DASH_Meeting_Type__c;
                    activity.isMissed = e.HDFC_DASH_Missed__c;
                    activity.subjectName = e.HDFC_DASH_Subject_Name_From_OS__c;
                    activity.applicationStage = e.HDFC_DASH_Application_Stage__c;
                    activity.applicationStatus = e.HDFC_DASH_Application_Status__c;
                    
                    if(activity.type == HDFC_DASH_Constants.STRING_HUDDLE && String.isNotBlank(e.WhoId))
                    {  
                        activity.assignedToSfId = mapHuddleSO.get(e.WhoId);
                    }
                    else if(activity.type == HDFC_DASH_Constants.STRING_ACTIVITY)
                    {                      
                        if(mapAssignedToNames.get(e.HDFC_DASH_BSA_AssignedId__c) != Null){
                            activity.assignedToName = mapAssignedToNames.get(e.HDFC_DASH_BSA_AssignedId__c);
                        }
                        activity.assignedToSfId = e.HDFC_DASH_BSA_AssignedId__c;
                        
                        if(e.HDFC_DASH_BSA_AssignedId__c?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_USER)
                        {
                            activity.executiveCode = mapUsers.get(e.HDFC_DASH_BSA_AssignedId__c).HDFC_DASH_Executive_Code__c;
                        }
                        else{
                            activity.executiveCode = e.HDFC_DASH_BSA_AssignedId__c;
                        }
                        if(mapAccountSourceRSNO.get(e.whatid)!=null){
                            activity.bsa = mapAccountSourceRSNO.get(e.whatid);    
                        }
                        
                        if(String.valueOf(e.WhatID)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_APPLICATION)
                        {
                            activity.fileNumber = mapFilenumber.get(e.WhatID); 
                        }
                        if(String.valueOf(e.WhoID)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_Lead)
                        {
                            activity.leadID = mapLeadId.get(e.WhoID);  
                        }
                    }
                    activity.latitude = e.HDFC_DASH_Location__Latitude__s;
                    activity.longitude = e.HDFC_DASH_Location__Longitude__s;
                    listActivities.add(activity);
                }
                apiRes.activities = listActivities;            
            }        
            else
            {
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_ACTIVITYTYPE_EXP); 
            }
        }
        else{
            HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_Error_Multiple_Params_Exp); 
        }
        return apiRes;     
    }
    
    /* 
Method: getQueryCondition
Description: Method to get the query conditions for Event selector.
*/
    Public Static Map<string,string> getQueryCondition()
    {
        Map<string,string> mapCondition = new Map<string,string>();
        
        //Scenario Huddle - If TL is sent in the URL
        if( param_Type == HDFC_DASH_Constants.STRING_HUDDLE)
        {
            List<Contact>listOfContact = new List<Contact>();
            
            if(String.isNotBlank(param_TL)){
                mapCondition.put(HDFC_DASH_Constants.EVENT_OWNERID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_TL + HDFC_DASH_Constants.STRING_QUOTE);
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_TYPE, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_HUDDLE + HDFC_DASH_Constants.STRING_QUOTE);
            }
            else if(String.isNotBlank(param_SO)){
                //query the TL of the SO
                //Query all the activties where - type is huddle && (  (TL is the owner && whoId is null) or (whoid has the contact of the SO)) 
                
                //Query user(id, STRING_MANAGERID, contactId) where exe_code = param_SO
                List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_MANAGERID,HDFC_DASH_Constants.HDFC_DASH_Executive_Code,HDFC_DASH_Constants.STRING_ContactId};
                    List<User> listSoUsers = HDFC_DASH_User_Selector.getUser(new List<string>{param_SO}, listUserFields, HDFC_DASH_Constants.HDFC_DASH_Executive_Code);
                
                if(!listSoUsers.isEmpty())
                {
                    //Query condition EVENT_OWNERID where STRING_MANAGERID
                    mapCondition.put(HDFC_DASH_Constants.EVENT_TYPE, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_HUDDLE + HDFC_DASH_Constants.STRING_QUOTE);
                    mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.EVENT_OWNERID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + listSoUsers[HDFC_DASH_Constants.INT_ZERO].ManagerId + HDFC_DASH_Constants.STRING_QUOTE);
                    mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHOID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_NULL + HDFC_DASH_Constants.STRING_CLOSING_BRACE);
                    mapCondition.put(HDFC_DASH_Constants.STRING_OR + HDFC_DASH_Constants.EVENT_WHOID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + listSoUsers[HDFC_DASH_Constants.INT_ZERO].ContactId + HDFC_DASH_Constants.STRING_QUOTE + HDFC_DASH_Constants.STRING_CLOSING_BRACE);
                } 
            }
            else{
                HDFC_DASH_UtilityClass.generateCustomException(HDFC_DASH_Constants.HDFC_DASH_ERROR_PARAMS_EXP);
            }
        }
        //Scenario 1 - If TL is sent in the URL
        else if(String.isNotBlank(param_TL) && String.isBlank(param_BSA) && String.isBlank(param_SO) && String.isBlank(param_BSAOwner) && param_Type == HDFC_DASH_Constants.STRING_ACTIVITY)
        {
            List<String> listUserId = new List<String>();
            listUserId.add(param_TL);
            
            if(param_ShowAll == HDFC_DASH_Constants.TRUESTR){
                List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_MANAGERID};
                    List<User> listSOUsers = HDFC_DASH_User_Selector.getUser(new List<string>{param_TL}, listUserFields, HDFC_DASH_Constants.STRING_MANAGERID);
                for(User userRec:listSOUsers){
                    listUserId.add(userRec.Id);
                }
            }
            
            mapCondition.put(HDFC_DASH_Constants.EVENT_OWNERID, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listUserId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+ HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            
            if(!list_App.isEmpty()) {
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHATID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_App[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
            }
            else if(!list_Lead.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHOID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_Lead[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
                
            }
            
            mapCondition = getQueryCommonCondition(mapCondition);
        }
        
        //Scenario 2 - If SO is sent in the URL
        else if(String.isNotBlank(param_SO) && String.isBlank(param_BSA) && String.isBlank(param_TL) && String.isBlank(param_BSAOwner) && param_Type == HDFC_DASH_Constants.STRING_ACTIVITY)
        { 
            List<Contact>listOfContacts = new List<Contact>();
            List<String> listOfBSA_Account = new List<String>();
            List<String> listContact = new List<String>();
            
            if(param_ShowAll == HDFC_DASH_Constants.TRUESTR)
            {
                List<String> listOfFields_SO_BSA = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Sales_Officer,HDFC_DASH_Constants.HDFC_DASH_BSA};
                    
                    List<HDFC_DASH_SO_BSA_Association__c> listOf_SO_BSA_Association = HDFC_DASH_SO_BSA_Selector.getBSA(new List<string>{param_SO}, listOfFields_SO_BSA ,HDFC_DASH_Constants.HDFC_DASH_Sales_Officer);
                for(HDFC_DASH_SO_BSA_Association__c  obj_SO_BSA : listOf_SO_BSA_Association ){
                    listOfBSA_Account.add(obj_SO_BSA.HDFC_DASH_BSA__c);
                }
                
                Map<String,String> mapConditionForContact = new Map<String,String>();
                
                if(!listOfBSA_Account.isEmpty())
                {
                    mapConditionForContact.put( HDFC_DASH_Constants.STRING_ACCOUNTID,
                                               HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE +
                                               String.join(listOfBSA_Account, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
                    
                    listOfContacts = HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(listOfFields_Contact,mapConditionForContact);
                    
                    for(Contact con:listOfContacts)
                    {
                        listContact.add(con.id);
                    }
                } 
            }
            
            if(!listContact.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.EVENT_OWNERID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_SO + HDFC_DASH_Constants.STRING_QUOTE);
                mapCondition.put(HDFC_DASH_Constants.STRING_OR +  HDFC_DASH_Constants.HDFC_DASH_BSA, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listContact, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+ HDFC_DASH_Constants.STRING_CLOSING_BRACE + HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            }
            else
            {
                mapCondition.put(HDFC_DASH_Constants.EVENT_OWNERID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_SO + HDFC_DASH_Constants.STRING_QUOTE);
            }
            
            if(!list_App.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHATID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_App[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
            }
            else if(!list_Lead.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHOID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_Lead[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
            }
            
            mapCondition = getQueryCommonCondition(mapCondition);
        }
        
        //Scenario 3 - If BSA Owner is sent in the URL
        else if(String.isNotBlank(param_BSAOwner) && String.isBlank(param_BSA) && String.isBlank(param_TL) && String.isBlank(param_SO) && param_Type == HDFC_DASH_Constants.STRING_ACTIVITY )
        {
            mapCondition.put(HDFC_DASH_Constants.HDFC_DASH_BSA, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + contactId + HDFC_DASH_Constants.STRING_QUOTE);
            
            if(!list_App.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHATID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_App[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
            }
            else if(!list_Lead.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHOID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_Lead[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
                
            }
            
            mapCondition = getQueryCommonCondition(mapCondition);
        }
        
        //Scenario 4 - If FileNumber is sent in the URL
        else if(String.isNotBlank(param_FileNumber) && String.isBlank(param_TL) && String.isBlank(param_BSA) && String.isBlank(param_SO) && String.isBlank(param_BSAOwner) && String.isBlank(param_LeadId) && param_Type == HDFC_DASH_Constants.STRING_ACTIVITY)
        {
            if(!list_App.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.EVENT_WHATID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_App[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
            }
            
            mapCondition = getQueryCommonCondition(mapCondition);        
        }
        
        //Scenario 5 - If LeadId is sent in the URL
        else if(String.isNotBlank(param_LeadId) && String.isBlank(param_FileNumber) && String.isBlank(param_TL) && String.isBlank(param_BSA) && String.isBlank(param_SO) && String.isBlank(param_BSAOwner) && param_Type == HDFC_DASH_Constants.STRING_ACTIVITY)
        {
            if(!list_Lead.isEmpty()){
                mapCondition.put(HDFC_DASH_Constants.EVENT_WHOID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + list_Lead[HDFC_DASH_Constants.INT_ZERO].Id + HDFC_DASH_Constants.STRING_QUOTE);
            }
            mapCondition = getQueryCommonCondition(mapCondition);
        }
        
        //Scenario 6 - If TL and BSA is sent in the URL
        else if(String.isNotBlank(param_TL) && String.isNotBlank(param_BSA) && String.isBlank(param_SO) && String.isBlank(param_BSAOwner) && String.isBlank(param_LeadId) && String.isBlank(param_FileNumber) && param_Type == HDFC_DASH_Constants.STRING_ACTIVITY)
        {
            List<String> listUserId = new List<String>();
            listUserId.add(param_TL);
            if(param_ShowAll == HDFC_DASH_Constants.TRUESTR){
                
                List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_MANAGERID};
                    
                    List<User> listSOUsers = HDFC_DASH_User_Selector.getUser(new List<string>{param_TL}, listUserFields, HDFC_DASH_Constants.STRING_MANAGERID);
                for(User userRec:listSOUsers){
                    listUserId.add(userRec.Id);
                }
            }
            //Add listUserId in the condition
            mapCondition.put(HDFC_DASH_Constants.EVENT_OWNERID, HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE + String.join(listUserId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+ HDFC_DASH_Constants.STRING_CLOSING_BRACE);
            
            if(String.isNotBlank(accountId)){
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHATID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + accountId + HDFC_DASH_Constants.STRING_QUOTE);
            }
            mapCondition = getQueryCommonCondition(mapCondition);        
        }
        
        //Scenario 7 - If SO and BSA is sent in the URL
        else if(String.isNotBlank(param_SO) && String.isNotBlank(param_BSA) && String.isBlank(param_TL) && String.isBlank(param_BSAOwner) && String.isBlank(param_LeadId) && String.isBlank(param_FileNumber) && param_Type == HDFC_DASH_Constants.STRING_ACTIVITY)
        {
            if(String.isNotBlank(param_SO)){
                mapCondition.put(HDFC_DASH_Constants.EVENT_OWNERID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_SO + HDFC_DASH_Constants.STRING_QUOTE);
            }
            if(String.isNotBlank(accountId)){
                mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_WHATID, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + accountId + HDFC_DASH_Constants.STRING_QUOTE);
            }
            mapCondition = getQueryCommonCondition(mapCondition); 
        }
        return mapCondition;
    }
    
    /* 
Method: getQueryCommonCondition
Description: Method to get the query the common conditions for Event selector.
*/    
    Public Static Map<string,string> getQueryCommonCondition(Map<string,string> mapCondition){
        
        if(String.isNotBlank(param_Type))
            mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_TYPE, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_Type + HDFC_DASH_Constants.STRING_QUOTE);
        if(String.isNotBlank(param_Status))
            mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.STATUS, HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + param_Status + HDFC_DASH_Constants.STRING_QUOTE);        
        if(String.isNotBlank(param_StartDateTime))
            mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.EVENT_StartDateTime, HDFC_DASH_Constants.STRING_GRETHERTHAN_EQUALTO + param_StartDateTime); 
        if(String.isNotBlank(param_EndDateTime))
            mapCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.EVENT_StartDateTime, HDFC_DASH_Constants.STRING_LESSTHAN_EQUALTO + param_EndDateTime);
        return mapCondition;
        
    }
    
    /* 
Method: getDataFromLookup
Description: Method to get data from the related objects.
*/
    Public Static Map<string,string> getDataFromLookup(List<Event> listEvents){
        List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name,HDFC_DASH_Constants.HDFC_DASH_Executive_Code};
            List<String> listConFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.STRING_Name,HDFC_DASH_Constants.HDFC_DASH_Executive_Code};
                List<String> listAppFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_name, HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID};
                    List<String> listLeadFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID };  
                        
                        List<String> listUserId=New List<String>();
        List<String> listConExecutiveId=New List<String>();
        List<String> listWhatId=New List<String>();
        List<String> listWhoId=New List<String>();
        Map<string,string> mapAssignedToNames= new Map<string,string>();
        Map<String,String> mapBsaCondition = new Map<string,String>();
        List<User> listUsers;
        List<Contact> listContacts;
        
        for(Event event:listEvents)
        {
            if(event.HDFC_DASH_BSA_AssignedId__c?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_USER){
                listUserId.add(event.HDFC_DASH_BSA_AssignedId__c);
            }
            else{   
                listConExecutiveId.add(event.HDFC_DASH_BSA_AssignedId__c);
            }
            
            if(String.isNotBlank(event.HDFC_DASH_BSA__c))
                listOfBSA.add(event.HDFC_DASH_BSA__c);
            if(String.isNotBlank(event.WhatId))
                listWhatId.add(event.WhatId);
            if(String.isNotBlank(event.WhoId))
                listWhoId.add(event.WhoId);
        }
        
        if(!listUserId.isEmpty())
        {
            listUsers = HDFC_DASH_User_Selector.getUser(listUserId, listUserFields, HDFC_DASH_Constants.ID_STRING);
            for(User user:listUsers){
                mapAssignedToNames.put(user.Id,user.Name);
                mapUsers.put(user.Id,user);
            }
            
        }
        if(!listWhatId.isEmpty())
        {
            List<String> listWhatId_App = new List<String>();
            List<String> listWhatIdAccount = new List<String>();
            
            for(String whatIdRec : listWhatId)
            {
                if(String.valueOf(whatIdRec)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_APPLICATION)
                {
                    listWhatId_App.add(whatIdRec);
                }
                else if(String.valueOf(whatIdRec)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_ACCOUNT)
                {
                    listWhatIdAccount.add(whatIdRec);
                }
            }
            
            if(!listWhatId_App.isEmpty())
            {
                List<Opportunity> listAppBasedOnWhatid = HDFC_DASH_Application_Selector.getApplication(listWhatId_App,listAppFields,HDFC_DASH_Constants.ID_STRING);
                for(Opportunity app : listAppBasedOnWhatid){
                    if(String.isNotBlank(app.Name))
                        mapFilenumber.put(app.id, app.Name);
                }
            }
            
            if(!listWhatIdAccount.isEmpty())
            {
                List<Account> listAccBasedOnWhatid = HDFC_DASH_Account_Selector.getAccounts(listWhatIdAccount,listOfFields_Account,HDFC_DASH_Constants.ID_STRING);
                for(Account acc : listAccBasedOnWhatid){
                    if(acc.HDFC_DASH_Source_Rec_SrNo__c != null){
                        mapAccountSourceRSNO.put(acc.id, acc.HDFC_DASH_Source_Rec_SrNo__c); 
                    }
                }
            }
        }
        
        if(!listWhoId.isEmpty())
        { 
            List<String> listWhoId_Lead = new List<String>();
            List<String> listWhoId_Contact = new List<String>();
            
            for(String whoIdRec : listWhoId)
            {
                if(String.valueOf(whoIdRec)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_Lead)
                {
                    listWhoId_Lead.add(whoIdRec);
                }
                else if(String.valueOf(whoIdRec)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_CONTACT)
                {
                    listWhoId_Contact.add(whoIdRec);
                }
            }
            if(!listWhoId_Lead.isEmpty()){
                List<Lead> listLeadBasedOnWhoid = HDFC_DASH_Lead_Selector.getLeadsBasedOnIds(listLeadFields,listWhoId_Lead);
                for(Lead leadRec : listLeadBasedOnWhoid){
                    if(String.isNotBlank(leadRec.HDFC_DASH_LMS_Lead_ID__c))
                        mapLeadId.put(leadRec.id, leadRec.HDFC_DASH_LMS_Lead_ID__c);
                }
            }
            
            if(!listWhoId_Contact.isEmpty()){
                List<Contact> listContactBasedOnWhoid = HDFC_DASH_Contact_Selector.getContactsBasedOnIds(listConFields,listWhoId_Contact);
                for(Contact contactRec : listContactBasedOnWhoid){
                    if(String.isNotBlank(contactRec.HDFC_DASH_Executive_Code__c))
                        mapHuddleSO.put(contactRec.id, contactRec.HDFC_DASH_Executive_Code__c);
                }
            }
        }
        if(!listOfBSA.isEmpty())
        {
            mapBsaCondition.put(HDFC_DASH_Constants.ID_STRING,
                                HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE +
                                String.join(listOfBSA, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        }
        
        if(!listConExecutiveId.isEmpty())
        {
            mapBsaCondition.put(HDFC_DASH_Constants.STRING_AND + HDFC_DASH_Constants.HDFC_DASH_Executive_Code,
                                HDFC_DASH_Constants.STRING_IN +HDFC_DASH_Constants.STRING_OPEN_BRACE + HDFC_DASH_Constants.STRING_QUOTE +
                                String.join(listConExecutiveId, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE) + HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE);
        }
        
        if(!mapBsaCondition.isEmpty())
        {
            List<Contact> listCon = HDFC_DASH_Contact_Selector.getContactsBasedOnQuery(listConFields,mapBsaCondition);   
            
            for(Contact contactRec:listCon)
            {
                if(String.isNotBlank(contactRec.HDFC_DASH_Executive_Code__c)){
                    mapAssignedToNames.put(contactRec.HDFC_DASH_Executive_Code__c,contactRec.Name);
                }
                mapBsa.put(contactRec.id, contactRec);
            }
        }
        
        return mapAssignedToNames;
    }
    
}