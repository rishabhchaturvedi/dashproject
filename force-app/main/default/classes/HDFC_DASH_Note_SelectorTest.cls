/**
className: HDFC_DASH_Note_SelectorTest
DevelopedBy: Ashita Jain
Date: 1 Nov 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Note_Selector class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_Note_SelectorTest {
    private static final string STRING_ID='Id';
    private static final string STRING_NAME='Name'; 
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static Note note = HDFC_DASH_TestDataFactory_Note.getBasicNote(appDetails.Id);
    
  
    /* 
    Method Name :testGetNotes
    Description :This method will test the getNotes.
    */
    static testmethod void testGetNotes(){  
        List<Note>  listNoteRec = null;
        System.runAs(sysAdmin){ 
            List<String> fieldList = new List<string>{STRING_ID,STRING_NAME};
            Test.startTest();
            listNoteRec = HDFC_DASH_Note_Selector.getNotes(new List<String> {note.Id}  ,new List<String>{HDFC_DASH_Constants.STRING_ID},HDFC_DASH_Constants.ID_STRING);          
        	Test.stopTest();
        }
        system.assertNotEquals( null, listNoteRec);
        system.assertEquals(1,listNoteRec.size()); 
    }

 
}