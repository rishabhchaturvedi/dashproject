/*
Author: Anisha Arumugam
Class:HDFC_DASH_SpotOfferFileReq_ILPSCallout
Description: This class will generate the request body for Spot Offer File Callout to ILPS 
*/
public  with sharing class HDFC_DASH_SpotOfferFileReq_ILPSCallout 
{
    /*
Class: EmploymentDetails
Description: Class to generate EmploymentDetails
*/
    public without sharing class EmploymentDetails {
        public String natOfEmp;
        public String selfEmpType;
        public String corpCustNo;
        public String compName;
    }
    
    /*
Class: PropertyDetails
Description: Class to generate PropertyDetails
*/
    public  without sharing class PropertyDetails {
        public String propIdentified;
        public String propLocation;
        public decimal propCost;
        public String projCode;
        public String projName;
    }
    
    public String sfApplId;
    public String leadNo;
    public String originBranch;
    public String originPlace;
    public String placeOfService;
    public String sourceBranch;
    public String sourcePlace;
    public String originMode;
    public String agencyCode;
    public String salesexeCd;
    public String associateCd;
    public String applnMode;
    public String empClass;
    public String selfEmpType;
    public LoanDetails loanDetails;
    public FeesDetails feesDetails;
    public PropertyDetails propertyDetails;
    public String staffLoan;
    public List<CustDetails> custDetails;
    /*
Class: AddressDetails
Description: Class to generate AddressDetails
*/
    public without sharing class AddressDetails {
        public String addressLine1;
        public String addressLine2;
        public String addressLine3;
        public String addressLine4;
        public String villageTownCity;
        public String landmark;
        public String subDistrict;
        public String district;
        public String state;
        public String country;
        public String pincode;
        public String postOfficeName;
        public String addressSource;
        public String addressFrom;
        public String addressType;
    }
    
    /*
Class: SpotOfferEligibilityDetails
Description: Class to generate SpotOfferEligibilityDetails
*/
    public without sharing class SpotOfferEligibilityDetails {
        public decimal currentRoi;
        public decimal possibleTerm;
        public decimal stretchedTerm;
        public decimal loanPossible;
        public decimal stretchedAmt;
        public decimal possibleEmi;
        public decimal stretchedEmi;
        public decimal normalIir;
        public decimal normalFoir;
        public decimal maxIir;
        public decimal maxFoir;
        public String campaignCode;
        public decimal niir;
        public decimal nfoir;
        public decimal nlcr;
        public decimal nlvr;
        public decimal combLcrLtv;
        public decimal ltvVal;
    }
    
    /*
Class: CustDetails
Description: Class to generate CustDetails
*/
    public without sharing class CustDetails {
        public String sfCustNo;
        public String title;
        public String firstName;
        public String middleName;
        public String lastName;
        public String capacity;
        public String pan;
        public date dob;
        public String residentType;
        public String mobileNo;
        public String mobile_consent;
        public String emailId;
        public String relWithBorr;
        public EmploymentDetails employmentDetails;
        public AddressDetails addressDetails;
        public IncomeDetails incomeDetails;
    }
    /*
Class: LoanDetails
Description: Class to generate LoanDetails
*/
    public without sharing class LoanDetails {
        public String spotOfferAccepted;
        public decimal requiredAmt;
        public decimal acceptedLoanAmount;
        public decimal requiredLoanTerm;
        public decimal accpetedLoanTerm;
        public decimal roi;
        public decimal emi;
        public String loanProductType;
        public String interestType;
        public String campaignCode;
        public String financingFor;
        public SpotOfferEligibilityDetails spotOfferEligibilityDetails;
    }
    /*
Class: FeesDetails
Description: Class to generate FeesDetails
*/
    public without sharing class FeesDetails {
        public decimal applFees;
        public decimal applTaxes;
        public decimal applFeesTot;
        public decimal applDiscount;		
    }
    
    /*
Class: IncomeDetails
Description: Class to generate IncomeDetails
*/
    public without sharing class IncomeDetails {
        public decimal income;
        public String incomeType;
        public decimal othIncome;
    }	
}