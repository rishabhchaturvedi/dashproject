@isTest
public class UpdateCaseOwnerSystemTest {
	
    @isTest 
    public static void test1(){
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
        Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
		app.HDFC_DASH_File_Number__c = app.HDFC_DASH_File_Number__c.left(9);
        app.HDFC_DASH_LMS_Place_Of_Service__c = 'NOIDA';
        app.HDFC_DASH_LMS_Origin_Branch__c = 'NEW DELHI';
        app.HDFC_DASH_Loan_Type__c = 'Loan';
        update app;       
        string systemUser = [select id from User where LastName =: 'System' and isactive=true limit 1].Id;
        Case c = new Case(
        	OwnerId = systemUser,
            HDFC_DASH_File_Number__c=app.Id,
            HDFC_DASH_Skip_AutoAssignment__c=false,
            hdfc_dash_case_Type__c = 'Query',
            status='New'
            
        );
        Test.startTest();
        insert c;
        Id jobId = database.executeBatch(new UpdateCaseOwnerSystem(),1);
        Test.stopTest();
    }
}