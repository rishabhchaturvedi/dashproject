/*
Class: HDFC_DASH_WhatsappIntegration
Author: Tejeswari
Date: 08 Nov 2021
Company: Accenture
Description: Queueable Class to make Whatsapp Details Callout to ILPS 
*/
public inherited sharing class HDFC_DASH_WhatsappIntegration implements Queueable, Database.AllowsCallouts{
    public Map<String,String> requestHeader = new Map<String,String>();
    HttpResponse response;
    Task objTask = new Task();
    String messageBody;
    
    /**
ConstructorName : HDFC_DASH_WhatsappIntegration
* Parameters : Task objTask
* Description : It is a constructor for class HDFC_DASH_WhatsappIntegration.
*/
    public HDFC_DASH_WhatsappIntegration(Task objTask,String messageBody)
    {
        this.objTask = objTask;
        this.messageBody = messageBody;
    }
    /*
Method: execute
Description: This method will Post the Whatsapp Details to ILPS.
*/
    public void execute(QueueableContext context) 
    {
        try
        {   
            doWhatsappCallout();
        }
        catch(Exception e)
        {     
            Exception exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            //system.debug('expDetails => ' + expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_WHATSAPP_ILPSCALLOUT, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }
    
    /*
Method: doWhatsappCallout
Description: This method will make the Whatsapp callout to ILPS.
*/
    public void doWhatsappCallout()
    {  
        if(messageBody != null && (!messageBody.contains(HDFC_DASH_Constants.STRING_HDFC_DASH))) 
        {
            string postProcessDataStr;
            HDFC_DASH_WhatsappIntegrationReq reqBody = getRequestBody();
            HDFC_DASH_CommunicationWrapper.ProcessData objProcessData;
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
            //Populate headers
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.HDFC_DASH_WHATSAPP_ILPSCALLOUT);
            objProcessData = new HDFC_DASH_CommunicationWrapper.ProcessData(objTask.id, string.valueof(reqBody?.message[0].type_template[0]?.attributes), null);   
            postProcessDataStr = json.serialize(objProcessData);
            //Make the SMS Callout
            if(reqBody != null)
            {
                response = interfaceObj.doCallOut(HDFC_DASH_Constants.HDFC_DASH_WHATSAPP_ILPSCALLOUT, HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), requestHeader, postProcessDataStr, requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));  
                Integration_Message_Log__c objLog = interfaceObj.objIntMsgLog;
                objProcessData = new HDFC_DASH_CommunicationWrapper.ProcessData(objTask.id, string.valueof(reqBody?.message[0].type_template[0]?.attributes), objLog?.id);   
                postProcessDataStr = json.serialize(objProcessData);
                HDFC_DASH_WhatsAppResponseHandler responseHandler = new HDFC_DASH_WhatsAppResponseHandler();
                responseHandler.doRetryProcess(response, postProcessDataStr);
            }
        }
    }
    
    /*
Method: getRequestBody
Parameters:None
Description: This method will create the requestBody from the request.
*/ 
    public HDFC_DASH_WhatsappIntegrationReq getRequestBody()
    {
        Interface_Settings__c Whatsapp_Integration = Interface_Settings__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_WHATSAPP_ILPSCALLOUT);
        HDFC_DASH_Communication__mdt mdt_Communication = HDFC_DASH_Communication__mdt.getInstance(objTask.HDFC_DASH_Communication_ID__c); 
        HDFC_DASH_WhatsappIntegrationReq whatsappIntegrationReq = new HDFC_DASH_WhatsappIntegrationReq();
        List<HDFC_DASH_WhatsappIntegrationReq.Message> listwhatsappIntegration = new List<HDFC_DASH_WhatsappIntegrationReq.Message>();
        List<HDFC_DASH_WhatsappIntegrationReq.Type_template> listwhatsappIntegration_type = new List<HDFC_DASH_WhatsappIntegrationReq.Type_template>();
        
        HDFC_DASH_WhatsappIntegrationReq.Message whatsappInt = new HDFC_DASH_WhatsappIntegrationReq.Message();
        HDFC_DASH_WhatsappIntegrationReq.Type_template type_template = new HDFC_DASH_WhatsappIntegrationReq.Type_template();
        HDFC_DASH_WhatsappIntegrationReq.Language language = new HDFC_DASH_WhatsappIntegrationReq.Language();
        
        whatsappInt.recipient_whatsapp = objTask.HDFC_DASH_Target_Number__c;
        whatsappInt.message_type = Whatsapp_Integration.HDFC_DASH_Message_Type__c; 
        whatsappInt.recipient_type =  Whatsapp_Integration.HDFC_DASH_Recipient_Type__c;
        whatsappInt.source =  Whatsapp_Integration.Source__c;
        type_template.name = mdt_Communication.HDFC_DASH_TemplateID__c;
        
        //Split the msgbody to get the list and asssign it to attributes.
        type_template.attributes = messageBody.split(HDFC_DASH_Constants.VALUE_SEPERATOR);
        language.locale = Whatsapp_Integration.HDFC_DASH_Locale__c;
        language.policy = Whatsapp_Integration.HDFC_DASH_Policy__c;
        type_template.language = language;
        
        listwhatsappIntegration_type.add(type_template);
        whatsappInt.type_template = listwhatsappIntegration_type;
        
        listwhatsappIntegration.add(whatsappInt);
        
        whatsappIntegrationReq.Message = listwhatsappIntegration;
        return whatsappIntegrationReq;  
    }
    
}