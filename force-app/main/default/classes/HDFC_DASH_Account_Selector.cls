/**
* 	className: HDFC_DASH_Account_Selector
DevelopedBy: Sai Shruthi
Date: 05 August 2021
Company: Accenture
Class Description: This class would create the select queries for Account object .
**/
public inherited sharing class HDFC_DASH_Account_Selector {
    private static final string QUERY_PARAMETER = '=:queryParameters';
	 /* 
    Method Name :getAccountsBasedOnIds
    Parameters  :List<string> fieldList,List<ID> AccountIds
    Description :This method would get the Accounts based on the IDs.
    */
        public static List<Account> getAccountsBasedOnIds(List<string> fieldList,List<ID> accountIds){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
                                +HDFC_DASH_CONSTANTS.STRING_FROMACCOUNTWHERE+ HDFC_DASH_CONSTANTS.STRING_ID 
                				+ HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE 
                                +HDFC_DASH_Constants.STRING_QUOTE+String.join(accountIds, HDFC_DASH_Constants.STRING_QUOTE
                                +HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)
                				+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
            List<Account> accList = database.query(querystring); 
            return accList;
        }
    /* 
    Method Name :getAccountsBasedOnId
    Parameters  :List<string> fieldList, Id AccountId
    Description :This method would get a single Account based on the ID.
    */
        public static Account getAccountsBasedOnId(List<string> fieldList, Id accountId){
            
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
                				 +HDFC_DASH_CONSTANTS.STRING_FROMACCOUNTWHERE+ HDFC_DASH_CONSTANTS.STRING_ID
                				 +HDFC_DASH_Constants.STRING_EQUALTO+HDFC_DASH_Constants.STRING_QUOTE+accountId+HDFC_DASH_Constants.STRING_QUOTE;
            
            Account accRec = database.query(querystring); 
            
            return accRec;
        }
        
    /* 
    Method Name :getAccountsBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Accounts based on fieldsAndparameters sent to it.
    */
        public static List<Account> getAccountsBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) 
                                +HDFC_DASH_CONSTANTS.STRING_FROMACCOUNTWHERE +getCondition(fieldsAndparameters) ;
            List<Account> accList = database.query(querystring); 
            return accList;   
        }
        
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getAccountsBasedOnQuery .
    */
        public static String getCondition(Map<String,String> fieldsAndparameters){
            String condition ='';
            for(String fieldName : fieldsAndparameters.keySet()){
                
                condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
            }
            return condition;
        }
     /* 
Method Name :getAccounts
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of Accounts based on the condition.
*/
    public static List<Account> getAccounts(List<string> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + HDFC_DASH_CONSTANTS.STRING_FROMACCOUNTWHERE + conditionOn + QUERY_PARAMETER;  
        List<Account> resultList = database.query(querystring);        
        
        return resultList;
    } 
     

}