/*@author        
Accenture 
@date           23/10/2017
@name           schedule_IT_Daily_Failed_Logs 
@description    The purpose of this class is to schedule IT_Daily_Failed_Logs_Details_Mail_Batch
*/
global class schedule_IT_Daily_Failed_Logs implements Schedulable {
    /***************************************************************************************************************
    *   @Name        :  execute                                                              
    *   @Return      :  void                                                                                       
    *   @Description :  This method will log failed batch details
    ***************************************************************************************************************/
   global void execute(SchedulableContext sc) {
      IT_Daily_Failed_Logs_Details_Mail_Batch b = new IT_Daily_Failed_Logs_Details_Mail_Batch(); 
      database.executebatch(b);
   }
}