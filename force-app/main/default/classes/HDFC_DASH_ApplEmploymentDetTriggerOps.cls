/*
className: HDFC_DASH_ApplEmploymentDetTriggerOps
DevelopedBy: Sai Suman
Date: 04 October 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on Applicant Employment Details object.
*/
public with sharing class HDFC_DASH_ApplEmploymentDetTriggerOps {
    /* Class Name:ChangeEmpLatestFlagBeforeInsert
Class Description: This Class contains the trigger operation for prosessing latest flag beforeinsert
*/
    public with sharing class ChangeEmpLatestFlagBeforeInsert implements HDFC_DASH_TriggerOps{
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applicantEmpDetailsFirstRun;
        } 
        /*
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
           List<HDFC_DASH_Applicant_Employment_Details__c> empDetRecList =
                HDFC_DASH_ApplEmplymtDetTriggerOpsHelper.filterEDWhereConIsPopulated();
            return empDetRecList;
        }
        /*
Method Name :execute 
Description :This method would execute the logic.
*/
        public void execute(Sobject[] empDetRecList){
            HDFC_DASH_ApplEmplymtDetTriggerOpsHelper.changeEmpDetRecBeforeUpdate(empDetRecList);
        }
    }
    /* 
Class Name:UpdateApplicantEmpDetailsLatestFlag
Class Description: This Class contains the trigger operation for prosessing latest flag after insert
*/
    public with sharing class UpdateApplicantEmpDetailsLatestFlag implements HDFC_DASH_TriggerOps{ 
        /*
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applicantEmpDetailsFirstRun;
        }
        /*
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<HDFC_DASH_Applicant_Employment_Details__c> empDetRecList =
                HDFC_DASH_ApplEmplymtDetTriggerOpsHelper.filterEDforLatestFlagScenario();
            return empDetRecList;
        }
        /*
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(Sobject[] empDetRecList){
            HDFC_DASH_ApplEmplymtDetTriggerOpsHelper.updateEmpDetRec(empDetRecList);
            HDFC_DASH_TriggerOpsRecursionFlags.applicantEmpDetailsFirstRun = false;
        }
    }
    
    
    /* 
Class Name:UpdateApplicantDetailsNatOfEmp
DevelopedBy: Tejeswari
Class Description: This Class contains the trigger operation for prosessing the NatOf_EMp in realted Applicant Details Record
*/
    public with sharing class UpdateApplicantDetailsNatOfEmp implements HDFC_DASH_TriggerOps{ 
        /*
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
            return HDFC_DASH_TriggerOpsRecursionFlags.applicantEmpDetails_AD_NatureOfEmp_FirstRun;
        }
        /*
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<HDFC_DASH_Applicant_Employment_Details__c> listEmpDetRecs =
                HDFC_DASH_ApplEmplymtDetTriggerOpsHelper.filterEDforNatOfEmpScenario();
            return listEmpDetRecs;
        }
        /*
Method Name :execute
Description :This method would execute the logic.
*/      
        public void execute(Sobject[] listEmpDetRecs){
            HDFC_DASH_ApplEmplymtDetTriggerOpsHelper.updateApplDetRec(listEmpDetRecs);
            HDFC_DASH_TriggerOpsRecursionFlags.applicantEmpDetails_AD_NatureOfEmp_FirstRun = false;
           // HDFC_DASH_ApplEmplymtDetTriggerOpsHelper.updateContactOnEmployment(Trigger.new);
            
        }
    }
}