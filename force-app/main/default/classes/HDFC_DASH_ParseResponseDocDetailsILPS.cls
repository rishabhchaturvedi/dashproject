/*
Class: HDFC_DASH_ParseResponseDocDetailsILPS
Author: Punam Marbate
Date: 20 July 2021
Company: Accenture
Description:This class will parse the response body of Document details callout to ILPS
*/
public with sharing class HDFC_DASH_ParseResponseDocDetailsILPS {
    
    public String error_message;

    public List<DocumentDetail> document_details;
         /*
class name: DocumentDetail
Description: This class will parse the response body of Document details callout to ILPS
*/
	public without sharing class DocumentDetail {
		public String storage_identifier;
        public String doc_rec_srno;
	}

     /*
Method: parse
Description: This method will parse the response body of Document details callout to ILPS
*/
    public static HDFC_DASH_ParseResponseDocDetailsILPS parse(String json) {
		return (HDFC_DASH_ParseResponseDocDetailsILPS) System.JSON.deserialize(json, HDFC_DASH_ParseResponseDocDetailsILPS.class);
	}
}