/**
className: HDFC_DASH_EventTriggerOpsTest
DevelopedBy: Tejeswari
Date: 21 January 2022
Company: Accenture 
Class Description: Test class for  HDFC_DASH_EventTriggerOps class.
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_EventTriggerOpsTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static Lead leadRec = HDFC_DASH_TestDataFactory_Leads.getBasicLead();
    
    /* 
Method Name :testFilterEventsWithWhoId_Insert
Description :This method would test the method in trigger helper for the list of Events which have the WhoId field for insert.
*/
    @isTest 
    public static void testFilterEventsWithWhoId_Insert()
    {  
        Exception testException;
        Event eventRec;
        System.runAs(sysAdmin){
            try{
                eventRec= HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.id,sysAdmin.id);
                Test.startTest();
                Database.insert(eventRec);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            Event objEvent = [select id,WhatId,WhoId,HDFC_DASH_Subject_Name__c from Event where id =:eventRec.id];
            system.assertNotEquals(null, objEvent.HDFC_DASH_Subject_Name__c);
            system.assertEquals(null, testException);
        }
    }
    
    /* 
Method Name :testFilterEventsWithWhatId_insert
Description :This method would test the method in trigger helper for the list of Events which have the WhatId field for insert.
*/
    @isTest 
    public static void testFilterEventsWithWhatId_insert()
    {  
        Exception testException;
        Event eventRec;
        System.runAs(sysAdmin){
            try{
                eventRec= HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.id,sysAdmin.id);
                eventRec.WhoId = null;
                Test.startTest();
                Database.insert(eventRec);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            Event objEvent = [select id,WhatId,WhoId,HDFC_DASH_Subject_Name__c from Event where id =:eventRec.id];
            system.assertNotEquals(null, objEvent.HDFC_DASH_Subject_Name__c);
            system.assertEquals(null, testException);
        }
    }
    
    /* 
Method Name :testFilterEventsWithWhoId_Update
Description :This method would test the method in trigger helper for the list of Events which have the WhoId field for update.
*/
    @isTest 
    public static void testFilterEventsWithWhoId_Update()
    {  
        Exception testException;
        Event objEvent;
        System.runAs(sysAdmin){
            try{
                Event eventRec= HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.id,sysAdmin.id);
                Database.insert(eventRec);
                objEvent = [select id,WhatId,WhoId from Event where id =:eventRec.id];
                objEvent.WhoId = leadRec.id;
                objEvent.WhatId = null;
                Test.startTest();
                Database.update(objEvent);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            Event objEventRec = [select id,WhatId,WhoId,HDFC_DASH_Subject_Name__c from Event where id =:objEvent.id];
            system.assertNotEquals(null, objEventRec.HDFC_DASH_Subject_Name__c);
            system.assertEquals(null, testException);
        }
    }
    
    /* 
Method Name :testFilterEventsWithWhoId_Update
Description :This method would test the method in trigger helper for the list of Events which have the WhatId field for update.
*/
    @isTest 
    public static void testFilterEventsWithWhatId_Update()
    {  
        Exception testException;
        Event objEvent;
        System.runAs(sysAdmin){
            try{
                Event eventRec= HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.id,sysAdmin.id);
                Database.insert(eventRec);
                objEvent = [select id,WhatId,WhoId from Event where id =:eventRec.id];
                objEvent.WhatId = acc.id;
                objEvent.WhoId = null;
                Test.startTest();
                Database.update(objEvent);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            Event objEventRec = [select id,WhatId,WhoId,HDFC_DASH_Subject_Name__c from Event where id =:objEvent.id];
            system.assertNotEquals(null, objEventRec.HDFC_DASH_Subject_Name__c);
            system.assertEquals(null, testException);
        }
    }
    /* 
Method Name :testFilterEventsWithExceCode
Description :This method would test the method in trigger helper for the list of Events for populating Actor ExecutiveCode Text.
*/
    @isTest 
    public static void testFilterEventsWithExceCode()
    {  
        Exception testException;
        Event objEvent;
        System.runAs(sysAdmin){
            try{
                Event eventRec= HDFC_DASH_TestDataFactory_Event.createBasicEvent(app.id,con.id,sysAdmin.id);
                eventRec.HDFC_DASH_BSA__c = null;
                Database.insert(eventRec);
                objEvent = [select id,WhatId,WhoId from Event where id =:eventRec.id];
                objEvent.HDFC_DASH_BSA__c = con.id;
                Test.startTest();
                Database.update(objEvent);
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            Event objEventRec = [select id,HDFC_DASH_Actor_Executive_Code_Text__c from Event where id =:objEvent.id];
            system.assertNotEquals(null, objEventRec.HDFC_DASH_Actor_Executive_Code_Text__c);
            system.assertEquals(null, testException);
        }
    }
}