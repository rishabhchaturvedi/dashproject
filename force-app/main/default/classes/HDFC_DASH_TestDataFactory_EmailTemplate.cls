/* 
ClassName:  HDFC_DASH_TestDataFactory_EmailTemplate
DevelopedBy: Sai Suman
Date: 23 Dec 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for EmailTemplate object based on parameters received
from the calling test class
*/
public class HDFC_DASH_TestDataFactory_EmailTemplate {
   public static final String NAME = 'NAME';
    public static final String DEVELOPERNAME = 'Client';
    public static final String SUBJECT = 'Test';
    
    /* Method Name: createEmailTemplate
parameters: ID appId
Method Description: This method is to create test data for Email Template.
Pass fields of Email Template Object
*/
     public static EmailTemplate createEmailTemplate()
    {
        EmailTemplate objEmailTemp = new EmailTemplate();
        
        objEmailTemp.Name = NAME;
        objEmailTemp.DeveloperName = DEVELOPERNAME;
        objEmailTemp.Subject = SUBJECT;
        //objEmailTemp.Id = appID;
        return objEmailTemp;        
    }
    
    /* Method Name: getBasicEmailTemp
parameters: ID appId
Method Description: This method is to generate test data for Email Template.
*/
    public static EmailTemplate getBasicEmailTemp()
    {
        EmailTemplate emailTemplateDetails = HDFC_DASH_TestDataFactory_EmailTemplate.createEmailTemplate();
        if(Schema.sObjectType.EmailTemplate.isCreateable()){
            Database.insert(emailTemplateDetails);
        }
        return emailTemplateDetails;
    }  
}