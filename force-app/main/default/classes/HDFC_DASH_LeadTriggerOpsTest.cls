/**
className: HDFC_DASH_LeadTriggerOpsTest 
DevelopedBy: Punam Marbate
Date: 12 Oct 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_LeadTriggerOps class.
**/
@isTest(SeeAllData = false)
private with sharing class HDFC_DASH_LeadTriggerOpsTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static User userRec = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static final string SOURCE_CODE = '12345';
    private static final string EXECUTIVE_ID = '67890';
    private static final string OWNER_ID = '67890';
    private static final string PROFILE_NAME = 'Partner Community User';
    private static final string ROLE_NAME= 'Sales Officer';
    private static final string USER_NAME= 'usertestclass@testorg.com';
    private static final string SOURCE_REFERENCE = 'Archivo';
    /* 
Method Name :testFilterLeadsWithSameLeadId
Description :This method would test the method in trigger helper for filtereing Lead Records with same Lead id.
*/
    /*  @isTest 
public static void testFilterLeadsWithSameLeadId() 
{ 
Exception ex;
System.runAs(sysAdmin) {
Test.startTest();
//List<Lead> duplicateLeadRecs = new List<Lead>{leadRecord,leadRecord1};
//database.insert(duplicateLeadRecs);
try{
HDFC_DASH_TestDataFactory_Leads.getBasicLead(); 
HDFC_DASH_TestDataFactory_Leads.getBasicLead(); 
}
catch(Exception ex1){
ex=ex1;
}
Test.stopTest();
System.assertNotEquals(null, ex);
System.assertEquals(true, ex.getMessage().indexOf(HDFC_DASH_Constants.Duplicate)>-HDFC_DASH_Constants.INT_ONE);
}

}  */
    /* 
Method Name :testfilterLeadsWithBsaAndSoFields
Description :This method would test the method in trigger helper for the list of Leads for which have the related fields.
*/
    @isTest 
    public static void testFilterLeadsWithBsaAndSoFields()
    { 
        list<Lead> listLead = new List<Lead>();
        UserRole role = [SELECT Id FROM UserRole WHERE Name=:ROLE_NAME];
        userRec.UserRoleId=role.id;
        userRec.FederationIdentifier = USER_NAME;
        userRec.UserName = USER_NAME+string.valueOf(datetime.now().getTime());
        System.runAs(sysAdmin) {
            
            Database.insert(userRec);
            acc.OwnerId=userRec.Id;
            acc.HDFC_DASH_Source_Id__c = SOURCE_CODE;
            Database.insert(acc);
            Contact conRec = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
            User partnerUserRec = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
            partnerUserRec.ContactId = conRec.id;
            partnerUserRec.UserName = USER_NAME+string.valueOf(datetime.now().getTime());
            partnerUserRec.FederationIdentifier = USER_NAME+string.valueOf(datetime.now().getTime());
            partnerUserRec.ProfileId = [SELECT Id FROM Profile WHERE Name=: PROFILE_NAME].Id;
            Database.insert(partnerUserRec);
            User objUser = [Select id,FederationIdentifier from User where id=:partnerUserRec.id];
            
            Lead leadRec = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
            leadRec.HDFC_DASH_Source_Id__c= SOURCE_CODE;
            leadRec.HDFC_DASH_Source_Executive_Id__c = EXECUTIVE_ID;
            leadRec.HDFC_DASH_Owner_Id__c = OWNER_ID; 
            leadRec.HDFC_DASH_Source_Reference__c = null;
            listLead.add(leadRec);
            Lead leadRec2 = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
            leadRec2.HDFC_DASH_Source_Id__c= SOURCE_CODE;
            leadRec2.HDFC_DASH_Source_Executive_Id__c = EXECUTIVE_ID;
            leadRec2.HDFC_DASH_Owner_Id__c =objUser.FederationIdentifier; 
            leadRec2.HDFC_DASH_Source_Reference__c = SOURCE_REFERENCE;
            listLead.add(leadRec2);
            Test.startTest();
            Database.insert(listLead);   
            //Database.insert(leadRec2);   
            Test.stopTest();
            Lead objlead = [Select id,HDFC_DASH_Source__c,HDFC_DASH_Source_Id__c,OwnerId,HDFC_DASH_Source_Reference__c,HDFC_DASH_Source_Executive__c from lead where id=:listLead[0].id];
            System.assertNotEquals(null, objlead.HDFC_DASH_Source__c);
            System.assertNotEquals(null, objlead.HDFC_DASH_Source_Executive__c);
            System.assertNotEquals(null, objlead.OwnerId); 
        }
        
    }  
    /* 
Method Name :testFilterLeadsWithSourceReference
Description :This method would test the method in trigger helper for the list of Leads for which have the related fields.
*/
    @isTest 
    public static void testFilterLeadsWithSourceReference()
    { 
        
        System.runAs(sysAdmin) {
            acc.HDFC_DASH_Source_Id__c = SOURCE_CODE;
            Database.insert(acc);
            Lead leadRec = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
            leadRec.HDFC_DASH_Source_Reference__c = SOURCE_REFERENCE;
            leadRec.HDFC_DASH_Source_Id__c= SOURCE_CODE;
            
            Test.startTest();
            Database.insert(leadRec);   
            Test.stopTest();
            Lead objlead = [Select id,HDFC_DASH_Source__c,HDFC_DASH_Source_Id__c,OwnerId,HDFC_DASH_Source_Reference__c from lead where id=:leadRec.id];
            System.assertNotEquals(null, objlead.HDFC_DASH_Source__c);
            
        }
        
    }  
    
    /* 
Method Name :testUpdateFilterLeads
Description :This method would test the method in trigger helper for the list of Leads for which have the related fields.
*/
    @isTest 
    public static void testUpdateFilterLeads()
    { 
        list<Lead> listLead = new List<Lead>();
        list<Lead> listUpdateLead = new List<Lead>();
        UserRole role = [SELECT Id FROM UserRole WHERE Name=:ROLE_NAME];
        userRec.UserRoleId=role.id;
        userRec.FederationIdentifier = USER_NAME;
        userRec.UserName = USER_NAME+string.valueOf(datetime.now().getTime());
        System.runAs(sysAdmin) {
            Database.insert(userRec);
            acc.OwnerId=userRec.Id;
            acc.HDFC_DASH_Source_Id__c = SOURCE_CODE;
            Database.insert(acc);
            Contact conRec = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
            User partnerUserRec = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
            partnerUserRec.ContactId = conRec.id;
            partnerUserRec.UserName = USER_NAME+string.valueOf(datetime.now().getTime());
            partnerUserRec.FederationIdentifier = USER_NAME+string.valueOf(datetime.now().getTime());
            partnerUserRec.ProfileId = [SELECT Id FROM Profile WHERE Name=: PROFILE_NAME].Id;
            Database.insert(partnerUserRec);
            Lead leadRec = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
            listLead.add(leadRec);
            Lead leadRec2 = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
            listLead.add(leadRec2);
            Database.insert(listLead);   
            
            Lead objlead = [Select id,HDFC_DASH_Source__c,HDFC_DASH_Source_Id__c,HDFC_DASH_Source_Executive__c,OwnerId,HDFC_DASH_Source_Executive_Id__c,HDFC_DASH_Owner_Id__c from lead where id=:listLead[0].id];
            objlead.HDFC_DASH_Source_Id__c= SOURCE_CODE;
            objlead.HDFC_DASH_Source_Executive_Id__c = EXECUTIVE_ID;
            objlead.HDFC_DASH_Owner_Id__c = OWNER_ID; 
            listUpdateLead.add(objlead);
            Lead objlead2 = [Select id,HDFC_DASH_Source__c,HDFC_DASH_Source_Id__c,HDFC_DASH_Source_Executive__c,OwnerId,HDFC_DASH_Source_Executive_Id__c,HDFC_DASH_Owner_Id__c from lead where id=:listLead[1].id];
            objlead2.HDFC_DASH_Source_Id__c= SOURCE_CODE;
            objlead2.HDFC_DASH_Source_Executive_Id__c = EXECUTIVE_ID;
            objlead2.HDFC_DASH_Owner_Id__c = USER_NAME; 
            objlead2.HDFC_DASH_Source_Reference__c = SOURCE_REFERENCE;
            listUpdateLead.add(objlead2);
            Test.startTest();
            Database.Update(listUpdateLead);
            Test.stopTest();
            
            Lead objUpdatedlead = [Select id,HDFC_DASH_Source__c,HDFC_DASH_Source_Id__c from lead where id=:objlead.id];
            System.assertNotEquals(null, objUpdatedlead.HDFC_DASH_Source__c);
        }
        
    }  
    /* 
Method Name :testFilterLeadsForOwnerAgency
Description :This method would test the method in trigger helper for the list of Leads for which have the related fields.
*/
    @isTest 
    public static void testFilterLeadsForOwnerAgency()
    { 
        
        System.runAs(sysAdmin) {
            acc.HDFC_DASH_Source_Id__c = SOURCE_CODE;
            Database.insert(acc);
            Lead leadRec = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
            leadRec.HDFC_DASH_Owner_Agency_Id__c= SOURCE_CODE;
            Test.startTest();
            Database.insert(leadRec);   
            Test.stopTest();
            Lead objlead = [Select id,HDFC_DASH_Owner_Agency_Id__c,HDFC_DASH_Owner_Agency__c from lead where id=:leadRec.id];
            System.assertNotEquals(null, objlead.HDFC_DASH_Owner_Agency__c);
        }
        
    }  
    /* 
Method Name :testFilterUpdateLeadsForOwnerAgency
Description :This method would test the method in trigger helper for the list of Leads for which have the related fields.
*/
    @isTest 
    public static void testFilterUpdateLeadsForOwnerAgency()
    { 
        
        System.runAs(sysAdmin) {
            acc.HDFC_DASH_Source_Id__c = SOURCE_CODE;
            Database.insert(acc);
            Lead leadRec = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
            
            Database.insert(leadRec);  
            Test.startTest();
            Lead objlead = [Select id,HDFC_DASH_Owner_Agency_Id__c,HDFC_DASH_Owner_Agency__c from lead where id=:leadRec.id];
            objlead.HDFC_DASH_Owner_Agency_Id__c= SOURCE_CODE;
            Database.update(objlead);  
            Test.stopTest();
            Lead objUpdatedlead = [Select id,HDFC_DASH_Owner_Agency_Id__c,HDFC_DASH_Owner_Agency__c from lead where id=:leadRec.id];
            System.assertNotEquals(null, objUpdatedlead.HDFC_DASH_Owner_Agency__c);
        }
        
    }  
    
}