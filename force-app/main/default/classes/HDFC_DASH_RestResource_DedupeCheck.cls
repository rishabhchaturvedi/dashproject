/**
* 	className: HDFC_DASH_RestResource_DedupeCheck
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This is a webservice class for DedupeCheck .
**/
@RestResource(urlMapping='/hdfc/dedupecheck/V1.0/*') 
global with sharing class HDFC_DASH_RestResource_DedupeCheck {
    
@HttpPost
    /* 
Method Name :handleDedupeCheckRequest
Parameters  :
Description :This method would get the request and pass it to HDFC_DASH_InboundRestService_DupeCheck.processDedupeCheck.
*/
    global static void handleDedupeCheckRequest() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        map<string,string> responseheaders;
        HDFC_DASH_APIResponse_DedupeCheck apiResponse;
        exception exp;
        try{
            responseheaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse)); 
        }
        catch(Exception e){
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON +e.getMessage()+ HDFC_DASH_Constants.SEMICOLON +e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON +e.getTypeName() ;  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_DEDUPE_CHECK,HDFC_DASH_Constants.HANDLE_DEDUPE_CHECK_REQUEST , HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
            // Throw Exception
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e,res);
            //system.debug('expDetails===='+expDetails);
        }
        finally{	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_Dedupe, 
                                                                request.requestBody.tostring(),(res.responseBody)?.toString(), 
                                                                HDFC_DASH_Constants.METHOD_TYPE_POST, HDFC_DASH_Constants.HANDLE_DEDUPE_CHECK_REQUEST, 
                                                                Exp, request.headers);
            if(responseheaders!= NULL){
                for(string responseheaderstr : responseheaders.keySet()){
                    if(!Test.isRunningTest())
                    {
                        res.addHeader(responseheaderstr, responseheaders.get(responseheaderstr));
                    }
                }
            }
        }
    }
}