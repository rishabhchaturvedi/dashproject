/* 
ClassName: HDFC_DASH_TestDataFactory_AppInfoValid
DevelopedBy: Punam Marbate
Date: 23 July 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Applicant Info Validation object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_AppInfoValid {
    public static final string NAME = 'Name';
    public static final string VALIDATION_RESPONSE = '{"response":{"attributes":{"Field2":"abc","Field1":"abc"}}}';
    public static final string BORROWER = 'B';
    public static final Decimal SALARYYEAR = 2021;
    public static final string SALARYMONTH = 'JUNE';
    public static final string RECORDTYPE= 'Document';
   
    /* Method Name: createBasicApplInfoValidation
    parameters: ID aplDetailID
    Method Description: This method is to create test data for HDFC_DASH_Info_Validation__c.
    Pass fields of HDFC_DASH_Info_Validation__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Info_Validation__c or inserted HDFC_DASH_Info_Validation__c id. 
    */
    public static HDFC_DASH_Info_Validation__c createBasicApplInfoValidation(ID aplDetailID,String applicantCapacity)
    {
        HDFC_DASH_Info_Validation__c applInfoValidation = new HDFC_DASH_Info_Validation__c();
            
        //applInfoValidation.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        applInfoValidation.HDFC_DASH_Validation_Response__c = VALIDATION_RESPONSE;
        applInfoValidation.HDFC_DASH_Applicant_Details__c =  aplDetailID  ;
        applInfoValidation.HDFC_DASH_Applicant_Capacity__c = applicantCapacity;
        applInfoValidation.HDFC_DASH_Salary_Year__c = SALARYYEAR;
        applInfoValidation.HDFC_DASH_Salary_Month__c = SALARYMONTH;
       	return applInfoValidation;        
    }
    
    /* Method Name: getBasicApplInfoValidation
    parameters: ID aplDetailID
    Method Description: This method is to generate test data for Applicant Info Validation.
    */
    public static HDFC_DASH_Info_Validation__c getBasicApplInfoValidation(ID aplDetailID,String applicantCapacity)
    {
        HDFC_DASH_Info_Validation__c applInfoValidation = HDFC_DASH_TestDataFactory_AppInfoValid.createBasicApplInfoValidation(aplDetailID,applicantCapacity);
        if(Schema.sObjectType.HDFC_DASH_Info_Validation__c.isCreateable()){
            Database.insert(applInfoValidation);
        }
        return applInfoValidation;
    }
    /* Method Name: createBasicInfoValWithApplication
    parameters: NONE
    Method Description: This method is to create test data for HDFC_DASH_Info_Validation__c.
    Pass fields of HDFC_DASH_Info_Validation__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Info_Validation__c or inserted HDFC_DASH_Info_Validation__c id. 
    */
    public static HDFC_DASH_Info_Validation__c createBasicInfoValWithApplication(ID appID)
    {
        HDFC_DASH_Info_Validation__c infoVal = new HDFC_DASH_Info_Validation__c();
            
       // doc.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        infoVal.HDFC_DASH_Application__c = appID;     
        infoVal.HDFC_DASH_Storage_Identifier__c = string.valueOf(Integer.valueof((Math.random() * 100000000)));
        //infoVal.HDFC_DASH_Document_File_Uploaded__c= true;
        infoVal.HDFC_DASH_Info_Validation_Record_Type__c= RECORDTYPE;
        infoVal.HDFC_DASH_SF_Triggered__c =false;
        infoVal.HDFC_DASH_Salary_Year__c = SALARYYEAR;
        infoVal.HDFC_DASH_Salary_Month__c = SALARYMONTH;
        infoVal.HDFC_DASH_Doc_Type_Code__c = HDFC_DASH_Constants.STRING_BKST;
        infoVal.HDFC_DASH_Document_Extractable__c = HDFC_DASH_Constants.STRING_YES;
  
        return infoVal;        
    }
    
    /* Method Name: getBasicDocumentWithApplication
    parameters: NONE
    Method Description: This method is to generate test data for InfoValidation.
    */
    public static HDFC_DASH_Info_Validation__c getBasicInfoValWithApplication(ID appID)
    {
        HDFC_DASH_Info_Validation__c infoVal = HDFC_DASH_TestDataFactory_AppInfoValid.createBasicInfoValWithApplication(appID);
        if(Schema.sObjectType.HDFC_DASH_Info_Validation__c.isCreateable()){
            Database.insert(infoVal);
        }
        return infoVal;
    }
    
}