/**
* 	className: HDFC_DASH_ExceptionLogUtil
DevelopedBy: Sai
Date: 26 May 2021
Company: Accenture
Class Description: This Class would be utilized to log exceptions.
**/
public without sharing class  HDFC_DASH_ExceptionLogUtil {

    
/* 
Method Name :publishException
Parameters  :String objname, String processname, String recordID, String exceptiondetails
Description :This method would be used to publish an event.
*/
public static void publishException(String objname, String processname, String recordID, String exceptiondetails){

HDFC_DASH_Exception_Logs__e pEvent = new HDFC_DASH_Exception_Logs__e();
pEvent.HDFC_DASH_Object__c = objname;
pEvent.HDFC_DASH_Operation__c = processname;
pEvent.HDFC_DASH_Exception_Details__c = exceptiondetails;
pEvent.HDFC_DASH_Record_ID__c = recordID;
eventBus.publish(pEvent);
}
/* 
Method Name :publishException
Parameters  :String objname, String processname, List<Database.SaveResult> listSaveResult
Description :This method would be used to publish multiple events.
*/
public static void publishException(String objname, String processname, List<Database.SaveResult> listSaveResult)
{
List<HDFC_DASH_Exception_Logs__e> pelist = new List<HDFC_DASH_Exception_Logs__e>();
String detail='';
for(Database.SaveResult res : listSaveResult){
    
    if(!res.isSuccess()){
        HDFC_DASH_Exception_Logs__e pEvent = new HDFC_DASH_Exception_Logs__e();
        pEvent.HDFC_DASH_Object__c = objname;
        pEvent.HDFC_DASH_Operation__c = processname;
        String excdetail='';
        for(Database.Error err : res.getErrors()) {
            excdetail+= err.getStatusCode()+HDFC_DASH_Constants.COLON+err.getMessage(); 
        }
        detail = new dmlException().getStackTraceString();
        pEvent.HDFC_DASH_Exception_Details__c = excdetail+detail;
        pelist.add(pEvent);
    }
}
if(pelist.size()>0){
    eventBus.publish(pelist);
}
}
}