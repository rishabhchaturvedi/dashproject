/* 
ClassName: HDFC_DASH_TestDataFactory_GSTbusinessDet
DevelopedBy: Sai Suman Kalyanam
Date:07 October 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for GST Business Detail object based on parameters received
from the calling test class
*/
public class HDFC_DASH_TestDataFactory_GSTbusinessDet {
public static final string NAME = 'Name';
private static final string STRING_ID='Id';  
     /* Method Name: createBasicGSTDetail
    parameters: ID applicantId
	*/
    public static HDFC_DASH_GST_Business_Details__c createBasicObligationDetail(ID applicantId)
    {
        HDFC_DASH_GST_Business_Details__c gstDetail = new HDFC_DASH_GST_Business_Details__c();
        //applPaymentDetail.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));    
        gstDetail.HDFC_DASH_Applicant_Employment_Details__c =  applicantId  ;       
        return gstDetail; 
    }
    /* Method Name: getBasicGSTDetail
    parameters: ID applID
    Method Description: This method is to generate test data for GST Business detail.
    */
    public static HDFC_DASH_GST_Business_Details__c getBasicGSTDetail(ID applicantId)
    {
        HDFC_DASH_GST_Business_Details__c gstDetail=
        HDFC_DASH_TestDataFactory_GSTbusinessDet.createBasicObligationDetail(applicantId);
        if(Schema.sObjectType.HDFC_DASH_GST_Business_Details__c.isCreateable()){
            Database.insert(gstDetail);
        }
        return gstDetail;
    }
}