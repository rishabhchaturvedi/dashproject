/**
className: HDFC_DASH_RestResource_GetCustDetsTest
DevelopedBy: Tejeswari
Date: 21 Oct 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_GetCustDetails.
**/
@isTest(SeeAllData = false)
Private with sharing class HDFC_DASH_RestResource_GetCustDetsTest {
private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);   
    private static final string BASEURL = URL.getOrgDomainUrl().toExternalForm();
    private static final string STRING_URL = BASEURL + '/services/apexrest/hdfc/getcustomerdetails/V1.0/*';
    private static final string PARAM_CUSTOMERID = 'customerid';
     private static final string PARAM_CUSTOMERNUMBER = 'customerNumber';
    
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
    private static Contact conWithoutAcc = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
                                                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static HDFC_DASH_Applicant_Employment_Details__c appEmpDetails = 
                                                    HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.id,con.id);
    private static HDFC_DASH_Contact_Address_Details__c addressDetail=
                                                    HDFC_DASH_TestDataFactory_ContAddDetail.getBasicContAddressDetail(con.Id);
    private static HDFC_DASH_Applicant_Bank_Detail__c appBankDetail = 
                    HDFC_DASH_TestDataFactory_BankDetails.getBasicApplBankDetail(appDetails.id,con.id);
    private static ContentVersion coVer = 
                    HDFC_DASH_TestDataFactory_ContentDoc.getContestVersion();
    private static ContentVersion ConVerRec = [Select title, ContentDocumentId from ContentVersion where IsLatest =true LIMIT 1];
    private static ContentDocumentLink conDoc = 
                    HDFC_DASH_TestDataFactory_ContentDoc.getConDocLink(ConVerRec.ContentDocumentId,con.id);
    private static HDFC_DASH_GST_Business_Details__c gstBusiness =HDFC_DASH_TestDataFactory_GSTbusinessDet.getBasicGSTDetail(appEmpDetails.id);
    private static HDFC_DASH_Application_Payment_Details__c applpayment = HDFC_DASH_TestDataFactory_ApplPayDetail.getBasicApplPaymentDetail(app.id);
    
    @testSetup static void setup() {
        DateTime myDateTime = DateTime.newInstance(2021, 11, 16, 12, 6, 13);
        HDFC_DASH_Application_Payment_Details__c applpayment = [SELECT ID, HDFC_DASH_TxnDate__c, HDFC_DASH_AuthStatus__c, HDFC_DASH_Application__c from HDFC_DASH_Application_Payment_Details__c LIMIT 1];
        applpayment.HDFC_DASH_TxnDate__c =   myDateTime.date();
        applpayment.HDFC_DASH_AuthStatus__c = '0300';
        update applpayment;
    }	
    /* 
    Method Name : testGetCustomer
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_InboundRestSer_GetCustDetails.getCustomer().
    */
    static testmethod void testGetCustomer()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL; 
            request.addParameter(PARAM_CUSTOMERID, con.id);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            HDFC_DASH_APIResponse_GetCustDetails appRes = new HDFC_DASH_APIResponse_GetCustDetails();
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
               HDFC_DASH_RestResource_GetCustDetails.getCustomerDetails();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
     /* 
    Method Name : testGetCustomerNumber
    Parameters  : No
    Description :This method is for success scenario which call HDFC_DASH_InboundRestSer_GetCustDetails.getCustomer().
    */
     static testmethod void testGetCustomerNumber()
    {
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL; 
            request.addParameter(PARAM_CUSTOMERNUMBER, acc.HDFC_DASH_Customer_Number__c);
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            HDFC_DASH_APIResponse_GetCustDetails appRes = new HDFC_DASH_APIResponse_GetCustDetails();
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
               HDFC_DASH_RestResource_GetCustDetails.getCustomerDetails();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
     /* Method Name: testExceptionCUST_ID_MISSING
    parameters: NONE
    Method Description: This method is to test Exception scenario which call HDFC_DASH_RestResource_GetCustDetails.getCustomerDetails() when CustId is missing
    */
    static TestMethod void testExceptionCUST_ID_MISSING() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        Exception testException;
        
        //Request Header is not added to check whether the exception is throw       
        RestRequest request = new RestRequest();
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
        request.requestUri = STRING_URL;      
        RestContext.request = request;

        test.startTest();
        System.runAs(sysAdmin)
        {
            try
            {
                HDFC_DASH_RestResource_GetCustDetails.getCustomerDetails();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    } 
    
}