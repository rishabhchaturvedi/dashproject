/*
	Author: Kumar Gourav
	Class: HDFC_DASH_APIResponse_GetCustDetails
	Description: Apex Class for handling the API Response of GetCustomerDetails
*/
public without sharing class HDFC_DASH_APIResponse_GetCustDetails 
{
    	public CustomerDetails customerDetails;
    	public Boolean customerHasApplication;
    	public Boolean stopCustomerDataEdit;
     /*
        Class: BankDetail
        Description: Class to get Bank Detail
    */
	public without sharing class BankDetail {
		public String sfBankDetailId;
	}
     /*
        Class: BankDetailInfo
        Description: Class to get Bank Detail Info
    */
	public without sharing class BankDetailInfo {
		public BankDetail bankDetail;
		public CustomerSelectedBankDetails customerSelectedBankDetails;
	}

     /*
        Class: CustomerSelectedBankDetails
        Description: Class to get CustomerSelected BankDetails
    */
	public without sharing class CustomerSelectedBankDetails {
		public String custSelAccountType;
		public Date custSelFromDate;
		public Date custSelToDate;
		public String custSelAccountHolderName;
		public String custSelBankCode;
		public String custSelBankName;
		public String custSelAccountNumber;
		public String custSelIfscCode;
		public String custSelSwiftCode;
	}
     /*
        Class: CustomerDetails
        Description: Class to get Customer Details
    */
	public without sharing class CustomerDetails {
		public String sfCustomerId;
		public String sfCustomerNumber;
       // public Boolean eligibleForExistingCustJourney;
		public ApplicantImage applicantImage;
        public BasicDetails basicDetails;
		public List<BankDetailInfo> bankDetailInfo;
        public List<ApplicantEmploymentDetails> applicantEmploymentDetails;
        //public ApplicantAddressInfo applicantAddressInfo;
        public List<ApplicantAddressInfo> applicantAddressInfo;
	}
     /*
        Class: ApplicantImage
        Description: Class to get Applicant Image
    */
	public without sharing class ApplicantImage {
		public String fileName;
		public String fileExtension;
        public String contentDocumentId;
		public String base64File;        
	}
   /*
        Class: BasicDetails
        Description: Class to get Basic Details
    */
    public class BasicDetails {
		public String name;
		public String genderCode;
		public Date dateOfBirth;
		public String email;
		public String residentType;
		public String pan;
		public String panStatusFromNsdl;
		public String panApplied;
		public String fatherFirstName;
		public String fatherMiddleName;
		public String fatherLastName;
		public String maritalStatus;
		public String highestQualificationCode;
		public String highestQualification;
		public String socialCategory;
		public String stateCode;
		public String state;
		public String countryCode;
		public String country;
		public String cityCode;
		public String city;
		public String capacity;
		public Decimal fixedMonthlyIncome;
		//public String ILPSCustNo;
		//public String ILPSUniqueCustNo;
		public Boolean isCustomer;
		public Boolean isRegisteredCustomer;
		public String mobileCountryCode;
		public String mobile;
		public String outSystemsID;
		public String passportNumber;
		public String passportFirstName;
		public String passportMiddleName;
		public String passportLastName;
		public String passportNationality;
		public Date passportDateOfExpiry;
        public Boolean reachCustomer;
	}
     /*
        Class: ApplicantEmploymentDetails
        Description: Class to get Applicant Employment Details
    */
     public without sharing class ApplicantEmploymentDetails {
		public EmploymentDetails employmentDetails;
		public CustomerSelectedBusinessInformation customerSelectedBusinessInformation;
		public CustomerSelectedEmpAddDetails customerSelectedEmpAddDetails;
		public CustomerSelectedEmployerInformation customerSelectedEmployerInformation;
		public AdditionalEmploymentDetails additionalEmploymentDetails;
		public List<EmpGstBusinessDetails> empGstBusinessDetails;
	}
    
   
     /*
        Class: EmploymentDetails
        Description: Class to get Employment Details
    */
    public without sharing class EmploymentDetails{
		public Boolean previousEmployer;
		public String sfEmpDetId;
		public String occupation;
		public String natureOfEmp;
		public String selfEmpType;
		public String occupationSubType;
        public string occupationSubTypeOS;
		public Integer areaUnderCultivationInAcers;
		public String cropsBeingCultivated;
		public String natureOfActivity;
		public String filingReturnsForLast3Years;
		public String noOfYearsITRsFiled;
		public String incomeSubjectToGSTTDS;
		public String statuatoryLicense;
		public String establishedBusinessPremises;
		public String workEmail;
		public String mcaActiveStatus;
		public String employerType;
		public String criticalityInd;
		public String activeEmployer;
		public Decimal monthlyPension;
		public String primaryBusiness;
	
	}
    
     /*
        Class: CustomerSelectedBusinessInformation
        Description: Class to get CustomerSelected Business Information
    */
    public without sharing class CustomerSelectedBusinessInformation {
		public String customerSelectedDesignationAndEntityType;
		public String customerSelectedBusinessName;
		public Integer customerSelectedYearOfIncorporation;
		public String customerSelectedAnnualTurnover;
		public String customerSelectedNatureOfBusiness;
		public String customerSelectedIndustryType;
		public String customerSelectedBusinessPAN;
		public String customerSelectedUdyamRegistrationNumber;
		//public Integer customerSelectedShareholdingPercentage;
		public String customerSelectedCINNumber;
	}
     
/*
        Class: AdditionalEmploymentDetails
        Description: Class to get Additional Employment Details
    */
	public with sharing class AdditionalEmploymentDetails{
		public String statuatoryDeductionsLikePFESI;
		public String isSalaryGettingCredited;
		public String onThirdPartyPayroll; 
	}

     /*
        Class: CustomerSelectedEmpAddDetails
        Description: Class to get CustomerSelected Employer Address Details
    */
    public without sharing class CustomerSelectedEmpAddDetails {
		public String customerSelectedTaluka;
		public String customerSelectedStateCode;
		public String customerSelectedppPostOfficeName;
		public String customerSelectedState;
		public String customerSelectedPincode;
		public String customerSelectedLandmark;
		public String customerSelectedDistrict;
		public String customerSelectedCountryCode;
		public String customerSelectedCountry;
		public String customerSelectedCityCode;
		public String customerSelectedCity;
		public String customerSelectedAddressLine1;
		public String customerSelectedAddressLine2;
		public String customerSelectedAddressLine3;
		public String customerSelectedAddressLine4;
	}
    
     /*
        Class: CustomerSelectedEmployerInformation
        Description: Class to get CustomerSelected Employer Information
    */
	public without sharing class CustomerSelectedEmployerInformation {
		public String customerSelectedEmployerName;
		public String customerSelectedEmployerCode;
		public String customerSelectedEmployeeNumber;
		public String customerSelectedDesignation;
		public String customerSelectedDepartment;
        public Integer customerSelectedJobDurationInMonths;
		public String customerSelectedHREmailId;
		public String customerSelectedWebsite;
        public Date customerSelectedEmploymentFromDate;
		public Date customerSelectedEmploymentToDate;
	}
     /*
        Class: EmpGstBusinessDetails
        Description: Class to get Employer Gst Business Details
    */
    public without sharing class EmpGstBusinessDetails {
		public String sfEmpGstBusinessDetId;
		public String gstNumber;
		public String stateCode;
		public String state;
	}
     /*
        Class: ApplicantAddressInfo
        Description: Class to get Applicant Address Info
    */
    public without sharing class ApplicantAddressInfo {
        public ApplicantAddressDetails applicantAddressDetails;
       	public CustomerSelectedAddressDetails customerSelectedAddressDetails; 
    }
     /*
        Class: ApplicantAddressDetails
        Description: Class to get Applicant Address Details
    */
    public without sharing class ApplicantAddressDetails {
	    public String sfConAddDetId;
		public String addressSource;
		public String addressType;
		public String addressFrom;
		public String addressChangedByCustomer;
        public String samePermanentAndCurrentAddress;       
		
	}
     
     /*
        Class: CustomerSelectedAddressDetails
        Description: Class to get CustomerSelected Address Details
    */
    public without sharing class CustomerSelectedAddressDetails {
	    public String addressLine1CustomerSelected;
		public String addressLine2CustomerSelected;
		public String addressLine3CustomerSelected;
		public String addressLine4CustomerSelected;
		public String cityCodeCustomerSelected;
		public String cityCustomerSelected;
		public String countryCodeCustomerSelected;
		public String countryCustomerSelected;
		public String districtCustomerSelected;
		public String landmarkCustomerSelected;
		public String pincodeCustomerSelected;
		public String postOfficeNameCustomerSelected;
		public String stateCodeCustomerSelected;
		public String stateCustomerSelected;
		public String talukaCustomerSelected;
		public String propertyOwnershipStatus;     
	}
}