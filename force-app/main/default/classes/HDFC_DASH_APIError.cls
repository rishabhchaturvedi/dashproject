/*
	Author: Anisha Arumugam
	Class: HDFC_DASH_APIError
	Description: Apex Class for handling API error response
*/
public with sharing class HDFC_DASH_APIError {
    public string errorMessage; 
    public Integer errorCode; 
    public String errorDescription;  
}