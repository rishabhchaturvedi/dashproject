/**
className: HDFC_DASH_Task_TriggerOps
DevelopedBy: Anisha Arumugam
Date: 25 November 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on Task object.
**/
public with sharing class HDFC_DASH_Task_TriggerOps {
    /**
className: SendMessage
DevelopedBy: Anisha Arumugam
Date: 25 November 2021
Company: Accenture 
Class Description: This Class will send SMS/WA message.
*/
    public with sharing class SendMessage implements HDFC_DASH_TriggerOps{
        /*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() 
        {
            if(system.isBatch())
            {
                HDFC_DASH_TriggerOpsRecursionFlags.taskSendMessage_FirstRun = false; 
            }
            
            return HDFC_DASH_TriggerOpsRecursionFlags.taskSendMessage_FirstRun;
        }
        /*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter()
        {
              system.debug('inside filer ');
            List<Task> listTasks = HDFC_DASH_Task_TriggerOpsHelper.filterTasksWithCommunication();
            return listTasks;
        }
        /*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] listTask){
            system.debug('inside execute filer '+listTask);
            HDFC_DASH_Task_TriggerOpsHelper.sendMessage(listTask);
            HDFC_DASH_TriggerOpsRecursionFlags.taskSendMessage_FirstRun = false; 
        }  
    }
}