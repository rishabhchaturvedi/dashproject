/**
* 	className: HDFC_DASH_APIRes_LMS_Lead_Enrichment
DevelopedBy: Sai Shruthi
Date: 21 July 2021
Company: Accenture
Class Description: Apex Class for handling LMS lead enrichment API Response
**/
public class HDFC_DASH_APIRes_LMS_Lead_Enrichment {
    public Integer return_cd;
	public String return_msg;
    
    /*
	Method: HDFC_DASH_APIRes_LMS_Lead_Enrichment
	Description: Method for Response Parsing
*/	
	public static HDFC_DASH_APIRes_LMS_Lead_Enrichment parse(HttpResponse response) {
          string json = response.getBody();
		return (HDFC_DASH_APIRes_LMS_Lead_Enrichment) System.JSON.deserialize(json, HDFC_DASH_APIRes_LMS_Lead_Enrichment.class);
	}

}