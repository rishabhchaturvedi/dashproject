/**
* 	className: HDFC_DASH_SchedulePollingPayStatusBatch
DevelopedBy: Ashita Jain
Date: 7 Jan 2021
Company: Accenture
Class Description: This class would schedule the HDFC_DASH_PollingPaymentStatusBatchClass.
**/
global class HDFC_DASH_SchedulePollingPayStatusBatch implements Schedulable  {
    
 /* 
Method Name :execute
Parameters  :SchedulableContext sc
Description :This method will schedule the HDFC_DASH_PollingPaymentStatusBatchClass
*/ 
     global void execute(SchedulableContext sc) {
      HDFC_DASH_PollingPaymentStatusBatchClass pollingPaymentStatusBatchClass = new HDFC_DASH_PollingPaymentStatusBatchClass(); 
      database.executebatch(pollingPaymentStatusBatchClass,200);
   }
}