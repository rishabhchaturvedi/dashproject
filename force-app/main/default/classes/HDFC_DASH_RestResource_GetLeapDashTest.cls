/**
className: HDFC_DASH_RestResource_GetLeapDashTest
DevelopedBy: Tejeswari
Date: 11 Dec 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestSer_GetLeapDash and HDFC_DASH_RestResource_GetLeapDashboard.
**/
@isTest(SeeAllData = false)
public class HDFC_DASH_RestResource_GetLeapDashTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR); 
    private static Id sysAdminId = UserInfo.getUserId();
    private static final string BASEURL = URL.getOrgDomainUrl().toExternalForm();
    private static final string STRING_URL = BASEURL + '/services/apexrest/hdfc/getLeapdashboard/V1.0';
    private static final string param_lrNumber = 'lrNumber';
    
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact conWithoutApp = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.createBasicApplication(acc.id);
    private static String STATUS = '25';
    private static String LR_NUMBER = '5746844';
    private static String LMS_BRANCHID = '543647';
    private static String PREFFERED_BRANCHID = '7373344';
    private static String AGENCY_CODE = '21654789';
    private static String LEAP_FLAG = 'Yes';
    private static Boolean SPOT_OFFER_REQ = false;
    private static String EMPLOYMENT_TYPE = 'E';
    private static String LOAN_TYPE_CODE = '3567363';
    private static String CAMPAIGN_CODE = '546674';
    private static String STAGE = 'Online';
    private static String DSF_Executive = '636744';
    private static String STRING_TRUE = 'true';
    /* 
Method Name : testGetLeapDashboardWithFileNumber
Parameters  : No
Description :This method is for success scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList().
*/
    static testmethod void testGetLeapDashboardWithFileNumber()
    {
        app.HDFC_DASH_Status__c = STATUS;
        app.HDFC_DASH_Preferred_Branch_Code__c = PREFFERED_BRANCHID;
        app.HDFC_DASH_LMS_Agency_Code__c = AGENCY_CODE;
        app.HDFC_DASH_Leap_Flag__c = LEAP_FLAG;
        app.HDFC_DASH_Spot_Offer_Started__c = SPOT_OFFER_REQ;
        app.HDFC_DASH_Loan_Type_Code__c = LOAN_TYPE_CODE;
        //app.HDFC_DASH_Campaign_Code__c = CAMPAIGN_CODE; 
        app.HDFC_DASH_DSF_Executive__c = DSF_Executive;
        Database.insert(app);
        
        HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(con.id,app.id);
        Database.insert(appDetails);
        HDFC_DASH_Info_Validation__c infoValApp = HDFC_DASH_TestDataFactory_AppInfoValid.getBasicInfoValWithApplication(app.Id);
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL ; 
            request.addParameter(HDFC_DASH_Constants.STRING_FILENUMBER,  app.HDFC_DASH_File_Number__c);
            request.addParameter(HDFC_DASH_Constants.STRING_DOC_RECEIVED, HDFC_DASH_Constants.STRING_Y);
            request.addParameter(HDFC_DASH_Constants.STRING_PARAM_STATUS,  app.HDFC_DASH_Status__c);
            request.addParameter(HDFC_DASH_Constants.STRING_SERVICECENTER,  app.HDFC_DASH_Preferred_Branch_Code__c);
            request.addParameter(HDFC_DASH_Constants.STRING_AGENCY,  app.HDFC_DASH_LMS_Agency_Code__c);
            request.addParameter(HDFC_DASH_Constants.STRING_LEAP_FLAG,  app.HDFC_DASH_Leap_Flag__c);
            request.addParameter(HDFC_DASH_Constants.STRING_SPOT_OFFER_REQ, string.valueof(app.HDFC_DASH_Spot_Offer_Started__c));
            request.addParameter(HDFC_DASH_Constants.STRING_PRODUCT,  app.HDFC_DASH_Loan_Type_Code__c);
            request.addParameter(HDFC_DASH_Constants.STRING_CAMPAIGN,  app.HDFC_DASH_Campaign_Code__c);
            request.addParameter(HDFC_DASH_Constants.STRING_EXISTING_CUST, STRING_TRUE);
            request.addParameter(HDFC_DASH_Constants.STRING_DSF, app.HDFC_DASH_DSF_Flag__c);
            
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetLeapDashboard.getLeapdashboard();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* 
Method Name : testGetLeapDashboardWithLRNUmber
Parameters  : No
Description :This method is for success scenario which call HDFC_DASH_RestResource_GetAppList.getApplicationList().
*/
    static testmethod void testGetLeapDashboardWithLRNUmber()
    {
        app.HDFC_DASH_Leap_LR_Number__c  = LR_NUMBER;
        app.HDFC_DASH_LMS_Origin_Branch_Id__c = LMS_BRANCHID;
        
        Database.insert(app);
        HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.createBasicApplicantDetails(con.id,app.id);
        Database.insert(appDetails);
        HDFC_DASH_Info_Validation__c infoValApp = HDFC_DASH_TestDataFactory_AppInfoValid.getBasicInfoValWithApplication(app.Id);
        System.runAs(sysAdmin)
        {
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();     
            RestRequest request = new RestRequest();
            RestResponse response= new RestResponse();
            request.requestUri = STRING_URL ; 
            request.addParameter(HDFC_DASH_Constants.STRING_LRNUMBER,  app.HDFC_DASH_Leap_LR_Number__c);
            request.addParameter(HDFC_DASH_Constants.STRING_BRANCH, app.HDFC_DASH_LMS_Origin_Branch_Id__c);
                      
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;
            request.requestBody = Blob.valueOf(HDFC_DASH_Constants.STRING_BLANK);
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            RestContext.request = request;
            RestContext.response = response;
            Test.startTest();
            HDFC_DASH_RestResource_GetLeapDashboard.getLeapdashboard();
            Test.stopTest();
            System.assertNotEquals(null, response);
        }
    }
    
    /* Method Name: testException
parameters: NONE
Method Description: This method is to test Exception scenario.
*/
    static TestMethod void testException() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        Exception testException;
        
        //Request Header is not added to check whether the exception is throw       
        RestRequest request = new RestRequest();
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_GET;   
        RestContext.request = request;
        
        test.startTest();
        System.runAs(sysAdmin) 
        {
            try
            {
                HDFC_DASH_RestResource_GetLeapDashboard.getLeapDashboard();
            }
            catch (Exception exp)
            {
                testException = exp;
            }
            test.stopTest();
            system.assertNotEquals(null, testException);
        }        
    } 
    
    
    
    
    
    
}