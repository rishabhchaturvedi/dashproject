/**
className: HDFC_DASH_MockConfidenceRuleEngResGen
Author: Sai Suman
Date: 29 Nov 2021
Company: Accenture
Class Description: This is mock class used for generating response for EPFO API callout.
**/
@isTest(SeeAllData=false)
global with sharing class HDFC_DASH_MockConfidenceRuleEngResGen implements HttpCalloutMock{
    /* 
Method Name :respond
Parameters  :HTTPRequest req
Description :This method would create a fake response for Confidence Rule Engine callout.
*/
    global HTTPResponse respond(HTTPRequest req) {
        
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        HttpResponse res = new HttpResponse();
        
        // Create a fake response
            res.setHeader('Content-Type', 'application/json'); 
            res.setBody('{"status":"SUCCESS","message":"Successfully Executed!","response":[{"responseTime":"Mon Dec 06 20:00:20 IST 2021","statusCode":200,"path":"/lentraservicelayer/v1/process-request","status":"SUCCESS","message":"Successfully Executed!","spotOffer":"N","response":{"BRE":{"SUMMARY":{"APPLICATION-APPROVED-AMOUNT":0.0,"APPLICATION-DECISION":"Declined","APPLICATION-SCORE":"","STATUS":"SUCCESS"},"APPLICANT-RESULT":[{"STATUS":"SUCCESS","DERIVED_FIELDS":{"POLICY_NAME":"CONFSCORE POSTFEE","POLICY_ID":4.0},"SCORING-REF-ID":"163880103292037","RULES":[{"CriteriaID":1.0,"RuleName":"216","Values":{"LOAN_PRODUCT_TYPE":"HU"},"Outcome":"Declined","Remark":"Update Property Details","Exp":" ( ( LOAN_PRODUCT_TYPE is not HOU ) && ( LOAN_PRODUCT_TYPE is not LND ) &&( LOAN_PRODUCT_TYPE is not LNC ) &&( LOAN_PRODUCT_TYPE is not NRP )  ) "}],"ELIGIBILITY-AMOUNT":"0.0","ELIGIBILITY-APPROVED-AMOUNT":"0.0","ELIGIBILITY-DECISION":"","DECISION":"Declined","POLICY_ID":4.0,"POLICY_NAME":"CONFSCORE POSTFEE","APPLICANT-ID":"1"},{"STATUS":"SUCCESS","DERIVED_FIELDS":{"POLICY_NAME":"CONFSCORE POSTFEE","POLICY_ID":4.0},"SCORING-REF-ID":"163880103292156","RULES":[{"CriteriaID":1.0,"RuleName":"216","Values":{"LOAN_PRODUCT_TYPE":"HU"},"Outcome":"Declined","Remark":"Update Property Details","Exp":" ( ( LOAN_PRODUCT_TYPE is not HOU ) && ( LOAN_PRODUCT_TYPE is not LND ) &&( LOAN_PRODUCT_TYPE is not LNC ) &&( LOAN_PRODUCT_TYPE is not NRP )  ) "}],"ELIGIBILITY-AMOUNT":"0.0","ELIGIBILITY-APPROVED-AMOUNT":"0.0","ELIGIBILITY-DECISION":"","DECISION":"Declined","POLICY_ID":4.0,"POLICY_NAME":"CONFSCORE POSTFEE","APPLICANT-ID":"2"}]}},"acknowledgmentId":"61ae1e7c2b103f6298f98e76"}]}');
            res.setStatusCode(200);
        
        return res;
    }
}