/*
Class: HDFC_DASH_SpotOfferFile_ILPSCallout
Author: Anisha Arumugam
Date: 20 July 2021
Company: Accenture
Description: Queueable Class to make Spot Offer File Callout to ILPS 
*/
public inherited sharing class HDFC_DASH_SpotOfferFile_ILPSCallout
{
    Id appId;
    Opportunity app = new Opportunity();
    map<String, string> mapAppDetails = new map<String, string>();
    map<Id, HDFC_DASH_Applicant_Employment_Details__c> mapEmpDetails = new map<Id, HDFC_DASH_Applicant_Employment_Details__c>();
    
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    Exception exp;  

    /*
Method: execute
Description: This method will Post the Spot Offer File to ILPS 
*/
    public HDFC_DASH_APIResponse_FeeDetails doSpotOfferFileCallout(HDFC_DASH_ParseFeeDetails feeDetailsRequest, HDFC_DASH_APIResponse_FeeDetails apiResponse) 
    {
        try
        {     
            appId = feeDetailsRequest.sfApplicationId;
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
            
            //Get the Spot Offer file request body
            HDFC_DASH_SpotOfferFileReq_ILPSCallout reqBody = getRequestBody(feeDetailsRequest);
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_SO);
            
            string postProcessDataStr = json.serialize(mapAppDetails);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.HDFC_DASH_ILPSCALLOUT_SO, HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), requestHeader, postProcessDataStr, requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            
            HDFC_DASH_Process_SpotOfferILPS_Response responseHandler = new HDFC_DASH_Process_SpotOfferILPS_Response();
            apiResponse = responseHandler.processResponse(response, apiResponse, postProcessDataStr);  
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_SO_ILPSCALLOUT, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
        return apiResponse; 
    }   
    
    /*
Method: getRequestBody
Description: This method will generate the request body for Spot Offer File ILPS Callout
*/
    public HDFC_DASH_SpotOfferFileReq_ILPSCallout getRequestBody(HDFC_DASH_ParseFeeDetails feeDetails) 
    {
        HDFC_DASH_SpotOfferFileReq_ILPSCallout spotOfferReq = new HDFC_DASH_SpotOfferFileReq_ILPSCallout();
        List<HDFC_DASH_SpotOfferFileReq_ILPSCallout.custDetails> listCustDetails = new List<HDFC_DASH_SpotOfferFileReq_ILPSCallout.custDetails>();
        
        List<Id> listAppID = new List<Id>{appId};     
            List<Id> listAppDetailsID = new List<Id>{appId};        
                List<Opportunity> listApp = new List<Opportunity>(); 
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>();      
        List<HDFC_DASH_Contact_Address_Details__c> listAddressDetails = new List<HDFC_DASH_Contact_Address_Details__c>();
        List<HDFC_DASH_Applicant_Employment_Details__c> listEmpDetails = new List<HDFC_DASH_Applicant_Employment_Details__c>();
        
        List<String> listOfFields_App = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_ApplicationNumber,HDFC_DASH_Constants.HDFC_DASH_File_Number,
            HDFC_DASH_Constants.HDFC_DASH_LMS_Lead_ID,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch,HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Branch_Id, HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Place,
            HDFC_DASH_Constants.HDFC_DASH_LMS_Place_Of_Service, HDFC_DASH_Constants.HDFC_DASH_LMS_Source_Branch, HDFC_DASH_Constants.HDFC_DASH_LMS_Source_Place, HDFC_DASH_Constants.HDFC_DASH_LMS_Origin_Mode,
            HDFC_DASH_Constants.HDFC_DASH_Owner_Agency_Id, HDFC_DASH_Constants.HDFC_DASH_Owner_Id, HDFC_DASH_Constants.HDFC_DASH_associate_cd, HDFC_DASH_Constants.HDFC_DASH_Appln_Mode, 
            HDFC_DASH_Constants.HDFC_DASH_Emp_Class, HDFC_DASH_Constants.HDFC_DASH_Self_Emp_Type, HDFC_DASH_Constants.HDFC_DASH_Staff_Loan,HDFC_DASH_Constants.SPOT_OFFER_ACCEPTED,
            HDFC_DASH_Constants.SO_REQUIRED_LOAN_AMOUNT,HDFC_DASH_Constants.CS_LOAN_AMOUNT,HDFC_DASH_Constants.EA_LOAN_TERM_MONTHS,
            HDFC_DASH_Constants.CS_TENURE_MONTHS,HDFC_DASH_Constants.CS_INTEREST_RATE,HDFC_DASH_Constants.CS_EMI_AMOUNT,HDFC_DASH_Constants.LOAN_TYPE,
            HDFC_DASH_Constants.CS_INTEREST_TYPE,HDFC_DASH_Constants.FINANCING_FOR,HDFC_DASH_Constants.EA_CURRENT_ROI,HDFC_DASH_Constants.EA_POSSIBLE_TERM_MONTHS,
            HDFC_DASH_Constants.EA_STRETCHED_TERM_MONTHS,HDFC_DASH_Constants.EA_LOAN_POSSIBLE,HDFC_DASH_Constants.EA_STRETCHED_AMOUNT,
            HDFC_DASH_Constants.EA_POSSIBLE_EMI,HDFC_DASH_Constants.EA_STRETCHED_EMI,HDFC_DASH_Constants.EA_NORMAL_IIR,HDFC_DASH_Constants.EA_NORMAL_FOIR,
            HDFC_DASH_Constants.EA_MAX_IIR,HDFC_DASH_Constants.EA_MAX_FOIR,HDFC_DASH_Constants.EA_CAMPAIGN_CODE,HDFC_DASH_Constants.EA_NIIR,HDFC_DASH_Constants.EA_NFOIR,HDFC_DASH_Constants.EA_NLCR,
            HDFC_DASH_Constants.EA_NLVR,HDFC_DASH_Constants.EA_COMB_LCR_LTV,HDFC_DASH_Constants.EA_LTV_VAL,HDFC_DASH_Constants.SO_APP_PROCESSING_FEE,
            HDFC_DASH_Constants.HDFC_DASH_Property_Locality,HDFC_DASH_Constants.HDFC_DASH_Property_Cost,
            HDFC_DASH_Constants.HDFC_DASH_Name_of_Project_Code,HDFC_DASH_Constants.HDFC_DASH_Name_Of_Project, HDFC_DASH_Constants.PROPERTY_DECIDED, HDFC_DASH_Constants.LOAN_TYPE_CODE};
                
                List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.RECORDTYPE_NAME,
                    HDFC_DASH_Constants.HDFC_DASH_Salutation,HDFC_DASH_Constants.HDFC_DASH_FirstName,HDFC_DASH_Constants.HDFC_DASH_MiddleName,
                    HDFC_DASH_Constants.HDFC_DASH_LastName,HDFC_DASH_Constants.APPLICANT_CAPACITY,HDFC_DASH_Constants.HDFC_DASH_PAN,HDFC_DASH_Constants.HDFC_DASH_Date_Of_Birth,
                    HDFC_DASH_Constants.HDFC_DASH_Resident_Type,HDFC_DASH_Constants.HDFC_DASH_Mobile,HDFC_DASH_Constants.HDFC_DASH_Email,HDFC_DASH_Constants.RELATION_WITH_PRIMARYAPPLICANT,
                    HDFC_DASH_Constants.FIXED_MONTHLY_INCOME,HDFC_DASH_Constants.ADDITIONAL_INCOME, HDFC_DASH_Constants.APPDETAIL_CONTACT_R_CONTACT_NUMBER,
                    HDFC_DASH_Constants.CA_ADDLINE1_CS,HDFC_DASH_Constants.CA_ADDLINE2_CS,
                    HDFC_DASH_Constants.CA_ADDLINE3_CS,HDFC_DASH_Constants.CA_ADDLINE4_CS,HDFC_DASH_Constants.CA_CITY_CS,
                    HDFC_DASH_Constants.CA_LANDMARK_CS,HDFC_DASH_Constants.CA_TALUKA_CS,HDFC_DASH_Constants.CA_STATE_CS,
                    HDFC_DASH_Constants.CA_COUNTRY_CS,HDFC_DASH_Constants.CA_DISTRICT_CS,HDFC_DASH_Constants.CA_PINCODE_CS,
                    HDFC_DASH_Constants.CA_POSTOFFICE_CS,HDFC_DASH_Constants.CA_ADD_SOURCE,HDFC_DASH_Constants.CA_ADD_FROM,
                    HDFC_DASH_Constants.CA_ADD_TYPE,HDFC_DASH_Constants.Contact_Account_HDFC_DASH_Customer_Number};
                        
                        List<String> listOfFields_Add = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, HDFC_DASH_Constants.CUSTOMER_SELECTED_ADDRESS_LINE1,
                            HDFC_DASH_Constants.CUSTOMER_SELECTED_ADDRESS_LINE2,HDFC_DASH_Constants.CUSTOMER_SELECTED_ADDRESS_LINE3,HDFC_DASH_Constants.CUSTOMER_SELECTED_ADDRESS_LINE4,
                            HDFC_DASH_Constants.CUSTOMER_SELECTED_CITY,HDFC_DASH_Constants.CUSTOMER_SELECTED_LANDMARK,HDFC_DASH_Constants.CUSTOMER_SELECTED_TALUKA,
                            HDFC_DASH_Constants.CUSTOMER_SELECTED_DISTRICT,HDFC_DASH_Constants.CUSTOMER_SELECTED_STATE,HDFC_DASH_Constants.CUSTOMER_SELECTED_COUNTRY,
                            HDFC_DASH_Constants.CUSTOMER_SELECTED_PINCODE,HDFC_DASH_Constants.CUSTOMER_SELECTED_POST_OFFICE_NAME,
                            HDFC_DASH_Constants.ADDRESS_SOURCE,HDFC_DASH_Constants.ADDRESS_TYPE,HDFC_DASH_Constants.HDFC_DASH_Address_From};
                                
                                List<String> listOfFields_Emp = new List<String>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.HDFC_DASH_APPDETAILS, 
                                    HDFC_DASH_Constants.HDFC_DASH_Nat_Of_Emp, HDFC_DASH_Constants.HDFC_DASH_Self_Emp_Type, HDFC_DASH_Constants.HDFC_DASH_Employer_Code, HDFC_DASH_Constants.HDFC_DASH_Employer_Name};
                                        
                                        //Calling Application and Applicant Detail Selector Classes
                                        listApp = HDFC_DASH_Application_Selector.getApplication(listAppID, listOfFields_App, HDFC_DASH_Constants.ID_STRING);
        listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppID, listOfFields_AppDetails, HDFC_DASH_Constants.HDFC_DASH_Application);
        
        app = listApp[HDFC_DASH_Constants.INT_ZERO];
        
        for(HDFC_DASH_Applicant_Details__c appDetail : listAppDetails)
        {
            listAppDetailsID.add(appDetail.Id);

            if(appDetail?.HDFC_DASH_Contact__r?.Account?.HDFC_DASH_Customer_Number__c != null)
            {
            	mapAppDetails.put(appDetail?.HDFC_DASH_Contact__r?.Account?.HDFC_DASH_Customer_Number__c, appDetail.id);  
            }
        } 
        
        listEmpDetails = HDFC_DASH_Applicant_Employment_Selector.getEmploymentDetails(listAppDetailsID, listOfFields_Emp, HDFC_DASH_Constants.HDFC_DASH_APPDETAILS);
        for(HDFC_DASH_Applicant_Employment_Details__c empDetail : listEmpDetails)
        {
            mapEmpDetails.put(empDetail.HDFC_DASH_Applicant_Details__c, empDetail);
        } 
        //Populate data from Application
        spotOfferReq.sfApplId = app?.HDFC_DASH_ApplicationNumber__c;
        spotOfferReq.leadNo=app?.HDFC_DASH_LMS_Lead_ID__c;
        spotOfferReq.originBranch=app?.HDFC_DASH_LMS_Origin_Branch_Id__c;
        spotOfferReq.originPlace=app?.HDFC_DASH_LMS_Origin_Place__c;
        spotOfferReq.placeOfService=app?.HDFC_DASH_LMS_Place_Of_Service__c;
        spotOfferReq.sourceBranch=app?.HDFC_DASH_LMS_Source_Branch__c;
        spotOfferReq.sourcePlace=app?.HDFC_DASH_LMS_Source_Place__c;
        spotOfferReq.originMode=app?.HDFC_DASH_LMS_Origin_Mode__c;
        spotOfferReq.agencyCode=app?.HDFC_DASH_Owner_Agency_Id__c;   
        spotOfferReq.salesexeCd=app?.HDFC_DASH_Owner_Id__c;
        spotOfferReq.associateCd=app?.HDFC_DASH_associate_cd__c;
        spotOfferReq.applnMode=app?.HDFC_DASH_Appln_Mode__c;
        spotOfferReq.empClass=app?.HDFC_DASH_Emp_Class__c;
        spotOfferReq.selfEmpType=app?.HDFC_DASH_Self_Emp_Type__c;
        spotOfferReq.staffLoan=app?.HDFC_DASH_Staff_Loan__c;
        
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.LoanDetails loanDetail = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.LoanDetails();
        loanDetail.spotOfferAccepted = app?.HDFC_DASH_Spot_Offer_Accepted__c;   
        loanDetail.requiredAmt=app?.HDFC_DASH_SO_Required_Loan_Amount__c;
        loanDetail.acceptedLoanAmount=app?.HDFC_DASH_CS_Loan_Amount__c;
        loanDetail.requiredLoanTerm=app?.HDFC_DASH_EA_Loan_Term_Months__c;
        loanDetail.accpetedLoanTerm=app?.HDFC_DASH_CS_Tenure_Months__c;
        loanDetail.roi=app?.HDFC_DASH_CS_Interest_Rate__c;
        loanDetail.emi=app?.HDFC_DASH_CS_EMI_Amount__c;  
        loanDetail.loanProductType=app?.HDFC_DASH_Loan_Type_Code__c;
        loanDetail.interestType=app?.HDFC_DASH_CS_Interest_Type__c;
        loanDetail.campaignCode=app?.HDFC_DASH_EA_Campaign_Code__c;
        loanDetail.financingFor=app?.HDFC_DASH_Financing_For__c;
        
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.spotOfferEligibilityDetails spotOfferEligibilityDetail = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.spotOfferEligibilityDetails();
        spotOfferEligibilityDetail.currentRoi=app?.HDFC_DASH_EA_Current_ROI__c;
        spotOfferEligibilityDetail.possibleTerm=app?.HDFC_DASH_EA_Possible_Term_Months__c;
        spotOfferEligibilityDetail.stretchedTerm=app?.HDFC_DASH_EA_Stretched_Term_Months__c;
        spotOfferEligibilityDetail.loanPossible=app?.HDFC_DASH_EA_Loan_Possible__c;
        spotOfferEligibilityDetail.stretchedAmt=app?.HDFC_DASH_EA_Stretched_Amount__c;
        spotOfferEligibilityDetail.possibleEmi=app?.HDFC_DASH_EA_Possible_EMI__c;
        spotOfferEligibilityDetail.stretchedEmi=app?.HDFC_DASH_EA_Stretched_EMI__c;
        spotOfferEligibilityDetail.normalIir=app?.HDFC_DASH_EA_Normal_IIR__c;    
        spotOfferEligibilityDetail.normalFoir=app?.HDFC_DASH_EA_Normal_FOIR__c;
        spotOfferEligibilityDetail.maxIir=app?.HDFC_DASH_EA_Max_IIR__c;
        spotOfferEligibilityDetail.maxFoir=app?.HDFC_DASH_EA_Max_FOIR__c;
        spotOfferEligibilityDetail.niir=app?.HDFC_DASH_EA_NIIR__c;
        spotOfferEligibilityDetail.nfoir=app?.HDFC_DASH_EA_NFOIR__c;
        spotOfferEligibilityDetail.nlcr=app?.HDFC_DASH_EA_NLCR__c;
        spotOfferEligibilityDetail.nlvr=app?.HDFC_DASH_EA_NLVR__c;
        spotOfferEligibilityDetail.combLcrLtv=app?.HDFC_DASH_EA_Comb_LCR_LTV__c;
        spotOfferEligibilityDetail.ltvVal=app?.HDFC_DASH_EA_LTV_Val__c;
        
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.FeesDetails feesDetail = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.FeesDetails();
        feesDetail.applFees = feeDetails?.applicationProcessingFee;
        feesDetail.applTaxes = (feeDetails?.feeConstruct?.totFeeInclTax==null?0:feeDetails?.feeConstruct?.totFeeInclTax) - (feeDetails?.feeConstruct?.totFeeExclTax==null?0:feeDetails?.feeConstruct?.totFeeExclTax);
        feesDetail.applFeesTot = feeDetails?.feeConstruct?.totFeeInclTax;
        feesDetail.applDiscount = feeDetails?.feeConstruct?.subventAmt;
        
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.PropertyDetails propertyDetail = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.PropertyDetails();
        propertyDetail.propIdentified=app?.HDFC_DASH_Property_Decided__c;
        propertyDetail.propLocation=app?.HDFC_DASH_Property_Locality__c;
        propertyDetail.propCost=app?.HDFC_DASH_Property_Cost__c;
        propertyDetail.projCode=app?.HDFC_DASH_Name_Of_Project_Code__c;
        propertyDetail.projName=app?.HDFC_DASH_Name_Of_Project__c;
        
        loanDetail.spotOfferEligibilityDetails = spotOfferEligibilityDetail;
        spotOfferReq.loanDetails = loanDetail;
        spotOfferReq.feesDetails = FeesDetail;
        spotOfferReq.propertyDetails = PropertyDetail;
        
        //Populate data from Primary Applicant Details
        for(HDFC_DASH_Applicant_Details__c appDetail : listAppDetails)
        {                   
            HDFC_DASH_SpotOfferFileReq_ILPSCallout.custDetails custDetailsApp = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.custDetails();            
            custDetailsApp = populateData(appDetail);            
            listCustDetails.add(custDetailsApp);
        }
        
        spotOfferReq.custDetails = listCustDetails;
        return spotOfferReq;
    }
    
    /*
Method: populateData
Description: This method will populate the data to the request body
*/
    public HDFC_DASH_SpotOfferFileReq_ILPSCallout.custDetails populateData(HDFC_DASH_Applicant_Details__c appDetail)
    {
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.custDetails custDetailsApp = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.custDetails();
        
        custDetailsApp.sfCustNo = appDetail?.HDFC_DASH_Contact__r?.Account.HDFC_DASH_Customer_Number__c;
        custDetailsApp.title = appDetail?.HDFC_DASH_Salutation__c;
        custDetailsApp.firstName=appDetail?.HDFC_DASH_FirstName__c;
        custDetailsApp.middleName=appDetail?.HDFC_DASH_MiddleName__c;
        custDetailsApp.lastName=appDetail?.HDFC_DASH_LastName__c;
        custDetailsApp.capacity=appDetail?.HDFC_DASH_Applicant_Capacity__c;
        custDetailsApp.pan=appDetail?.HDFC_DASH_PAN__c;
        custDetailsApp.dob=appDetail?.HDFC_DASH_Date_Of_Birth__c;
        custDetailsApp.residentType=appDetail?.HDFC_DASH_Resident_Type__c;
        custDetailsApp.mobileNo=appDetail?.HDFC_DASH_Mobile__c;
        custDetailsApp.emailId=appDetail?.HDFC_DASH_Email__c;
        custDetailsApp.relWithBorr=appDetail?.HDFC_DASH_Relation_with_PrimaryApplicant__c;
        
        //Populate Employment details
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.EmploymentDetails employmentDetail = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.EmploymentDetails();
        employmentDetail.natOfEmp=mapEmpDetails.get(appDetail.Id)?.HDFC_DASH_Nat_Of_Emp__c;
        employmentDetail.selfEmpType=mapEmpDetails.get(appDetail.Id)?.HDFC_DASH_Self_Emp_Type__c;
        employmentDetail.corpCustNo = mapEmpDetails.get(appDetail.Id)?.HDFC_DASH_Employer_Code__c;
        employmentDetail.compName = mapEmpDetails.get(appDetail.Id)?.HDFC_DASH_Employer_Name__c;
        
        //Populate Income details
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.IncomeDetails incomeDetail = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.IncomeDetails();
        incomeDetail.income = appDetail?.HDFC_DASH_SO_Fixed_Monthly_Income__c;
        incomeDetail.incomeType = mapEmpDetails.get(appDetail.Id)?.HDFC_DASH_Nat_Of_Emp__c;
        incomeDetail.othIncome = appDetail?.HDFC_DASH_SO_Additional_Income__c;
        
        //Populate Address details
        HDFC_DASH_SpotOfferFileReq_ILPSCallout.AddressDetails addressDetail = new HDFC_DASH_SpotOfferFileReq_ILPSCallout.AddressDetails();
        
        addressDetail.addressLine1 = appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Address_Line1_CustomerSelected__c;
        addressDetail.addressLine2 = appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Address_Line2_CustomerSelected__c;
        addressDetail.addressLine3= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Address_Line3_CustomerSelected__c;
        addressDetail.addressLine4= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Address_Line4_CustomerSelected__c;
        addressDetail.villageTownCity= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_City_CustomerSelected__c;
        addressDetail.landmark= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Landmark_CustomerSelected__c;
        addressDetail.subDistrict= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Taluka_CustomerSelected__c;
        addressDetail.district= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_District_CustomerSelected__c;
        addressDetail.state= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_State_CustomerSelected__c;
        addressDetail.country= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Country_CustomerSelected__c;
        addressDetail.pincode= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Pincode_CustomerSelected__c;
        addressDetail.postOfficeName= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_PostOfficeNameCustomerSelected__c;
        addressDetail.addressSource= appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Address_Source__c;
        addressDetail.addressFrom = appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Address_From__c;
        addressDetail.addressType = appDetail?.HDFC_DASH_Current_Address__r?.HDFC_DASH_Address_Type__c;
        
        custDetailsApp.employmentDetails = employmentDetail;
        custDetailsApp.incomeDetails = incomeDetail;
        custDetailsApp.addressDetails = addressDetail;
        
        return custDetailsApp;
    }
    
}