/**
className: HDFC_DASH_TriggerHandlerTest
DevelopedBy: Sai
Date: 26 May 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_TriggerHandler
* 
*/
@isTest(SeeAllData = false)
private class HDFC_DASH_TriggerHandlerTest {
    
    private static final string INSERTED ='inserted';
    private static final string UPDATED ='Updated';
    private static final string DELETED ='Deleted';
    private static final string UNDELETED ='Undeleted';
/* Method Name: testInsert
parameters: NONE
Class Description: This method would check if the record is getting 
inserted using the trigger framework significantly HDFC_DASH_TriggerHandler class.
*/
static TestMethod void testInsert() {
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    //Account acc = HDFC_DASH_TestDataFactory_Accounts.createBasicAccount();
    Lead leadRec = HDFC_DASH_TestDataFactory_Leads.createBasicLead();
    test.startTest();
    System.runAs(sysAdmin){
        database.insert(leadRec);
        test.stopTest();
        system.assertEquals(1, [select count() from Lead LIMIT 10],INSERTED);
    }
}

/* ClassName: testUpdate
Class Description: This method would check if the record is getting 
updated using the trigger framework significantly HDFC_DASH_TriggerHandler class. 
*/
static TestMethod void testUpdate() {
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    HDFC_DASH_Applicant_Details__c appDetails = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    test.startTest();
    System.runAs(sysAdmin){
    appDetails.Name = appDetails.Name +UPDATED;
    database.update(appDetails);
    test.stopTest();
    system.assert((appDetails.Name).contains(UPDATED),UPDATED);
    }
}
    
    
/* ClassName: testDelete
Class Description: This method would check if the record is getting 
deleted using the trigger framework significantly HDFC_DASH_TriggerHandler class.
*/
static TestMethod void testDelete() {
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    HDFC_DASH_Applicant_Details__c appDetails = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    test.startTest();
    System.runAs(sysAdmin){
    database.delete(appDetails);
    test.stopTest();
    system.assertEquals(0, [select count() from HDFC_DASH_Applicant_Details__c LIMIT 10],DELETED);
    }
}
/* ClassName: testUnDelete
Class Description: This method would check if the record is getting 
undeleted using the trigger framework significantly HDFC_DASH_TriggerHandler class.
*/
static TestMethod void testUnDelete() {
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    HDFC_DASH_Applicant_Details__c appDetails = 
                        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    if(Schema.sObjectType.HDFC_DASH_Applicant_Details__c.isDeletable()){
        database.delete(appDetails);
    }
    test.startTest();
    System.runAs(sysAdmin){
    database.undelete(appDetails);
    test.stopTest();
    system.assertEquals(1, [select count() from HDFC_DASH_Applicant_Details__c LIMIT 10],UNDELETED);
    }
}

}