/**
className: HDFC_DASH_MockLeadStageResGen
Date: 15 Dec 2021
Company: Accenture
Class Description: This is mock class used for generating response for ILPS File push callout.
**/ 
global with sharing class HDFC_DASH_ILPSFilePUSH_MockGen implements HttpCalloutMock{

    private Id applicationId;
    public HDFC_DASH_ILPSFilePUSH_MockGen(Id appId)
    {
        this.applicationId = appid;
    }
        /* 
Method Name :respond
Parameters  :HTTPRequest req
Description :This method would create a fake response for Confidence Rule Engine callout.
*/
global HTTPResponse respond(HTTPRequest req) {
        
    User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
  
    HttpResponse res = new HttpResponse();
    
    // Create a fake response
    
        res.setHeader('Content-Type', 'application/json'); 
        res.setBody('{"serialDetails":[{"sfapplicationid":"'+applicationId+'","fileno":658118213,"filecompletenessflag":"N"}],"applicantDetails":[{"sfcustno":"700000000","customername":"Mr. Ram L Shah","capacity":"B","ilpscustnumber":8015995}],"errorCode":0,"errorMessage":"Success"}');
        res.setStatusCode(200);
    
    return res;
}
}