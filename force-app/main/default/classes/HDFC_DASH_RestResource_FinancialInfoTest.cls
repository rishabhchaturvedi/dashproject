/**
className: HDFC_DASH_RestResource_FinancialInfoTest
DevelopedBy: Anisha Arumugam
Date: 05 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_FinancialInfo
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_RestResource_FinancialInfoTest {
    
    private static final string SUCCESS ='Success';
    private static final string STATUS ='Complete';
    private static final string CIBILID = '100032566162';
    private static final string WORKEMAIL = 'workemail@xyz.com';
    private static final integer SUCCESSCODE = 200;
   
    
    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    
    private static List<HDFC_DASH_Master_Table__c> listMasterTable = HDFC_DASH_TestDataFactory_MasterTables.getBasicMasterTable();   
    private static HDFC_DASH_City_Master__c cityMaster = HDFC_DASH_TestDataFactory_MasterTables.getBasicCityMaster();
    private static HDFC_DASH_Project_Master__c projectMaster = HDFC_DASH_TestDataFactory_MasterTables.getBasicProjectMaster();
    private static Account empMaster = HDFC_DASH_TestDataFactory_MasterTables.getBasicEmpMaster();
    
    //Request URL
    private static string requestUri = '/services/apexrest/hdfc/financialinfomation/V1.0/';
    
    //Request body
    private static string reqBodyFinancialInfo = '{ "ApplicationDetails": { "sfApplicationID": "' + app.id + '", "sfApplicationNumber": "10000003", "sfApplicantID":"' + appDetails.id + '", "loanType": "EDU", "lmsLeadID":"345689", "leadStageNumber":"6", "leadStageDescription":"", "propertyDecided": "Yes", "propertyCityCode": "HARAPANAHALLI", "propertyStateCode": "KA", "propertyCountryCode": "INDIA", "PropertyDetails": { "ProjectCode": "178", "ProjectName": "CHARU HEIGTHS", "PropertyCost": "6000000.00", "PropertyHouseIdentifier":"111", "PropertyStreetNameIdentifier":"222", "PropertyLandmark":"Mall", "PropertyLocality":"Karnataka", "PropertyVillageTownCity":"Test town", "PropertySubDistrict":"Test Sub District", "PropertyDistrict":"Test District", "PropertyState":"Test State", "PropertyCountry":"Test Country", "PropertyPostalCode":"636402", "PropertyPostOfficeName":"Test PO", "PropertyStatus": "'+ STATUS+'" }, "PropertyIndicativeCost":"5555.00", "SkipEmploymentDetails":"Yes", "ApplicantEmploymentDetails":{ "occupation":"S", "workEmail":"'+WORKEMAIL+'", "employerCode":"1076600", "employerName":"ZS ASSOCIATES INDIA PRIVATE LIMITED" }, "FinancialInfo":{ "fixedMonthlyIncome":"75000.00", "additionalIncome":"20000.00", "noAdditionalIncome":"Yes", "salarySlipAttached":"Yes" }, "riskScore":{ "riskScoreCustomerstatus":"EXISTING", "riskScoreCustomerflags":"", "riskScoreRtrexists":"N", "riskScoreCustomergrade":"", "riskScoreRiskevent":"", "riskScoreAvailedMoratorium":"N", "riskScoreAvailedRestruct":"N", "riskScoreHdfcStaff":"", "riskScoreConfidenceScore":"", "riskScoreExistInNegList":"N" }, "confidenceScore":"100", "experianCibilID":"100032566162" }, "validationOutcome":[ { "user_Input_Type":"Name Test PAN", "user_Input_Value":"ASD123DSedf", "validated_With":"NSDL", "validation_Response":{ "TestResponse":{ "test_attributes": { "Field1":"abc", "Field2":"abc" } }} }] } ';
    
    /* Method Name: teststoreFinancialInfo
    parameters: NONE
    Method Description: This method would check if the Inbound PATCH request(financial information) is getting processed and the request data is getting stored in the appropriate objects.
    */
    static TestMethod void teststoreFinancialInfo() 
    {        
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        
        RestRequest request = new RestRequest();
        RestResponse response= new RestResponse();
        request.requestUri = requestUri;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
        request.requestBody = Blob.valueOf(reqBodyFinancialInfo);
        
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        RestContext.request = request;
        RestContext.response = response;
        
        test.startTest();
        System.runAs(sysAdmin)
        {
            HDFC_DASH_RestResource_FinancialInfo.storeFinancialInfo();
            test.stopTest();

            system.assertEquals(1, [select count() from HDFC_DASH_Applicant_Details__c where HDFC_DASH_Experian_CIBIL_ID__c =:CIBILID], SUCCESS);
            system.assertEquals(1, [select count() from HDFC_DASH_Applicant_Employment_Details__c where HDFC_DASH_Work_Email__c =:WORKEMAIL], SUCCESS);
        }
    }
    
    /* Method Name: testException
    parameters: NONE
    Method Description: This method would check if the exception is thrown when the request headers are not sent
    */
    static TestMethod void testException() 
    {
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();

        //Request Header is not added to check whether the exception is throw       
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = requestUri;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
        request.requestBody = Blob.valueOf(reqBodyFinancialInfo);       
        RestContext.request = request;
        RestContext.response = response;
        
        test.startTest();
        System.runAs(sysAdmin)
        {
            HDFC_DASH_RestResource_FinancialInfo.storeFinancialInfo();
            test.stopTest();

            system.assertEquals(HDFC_DASH_Constants.INT_FIVE_HUNDRED, response.statusCode);            
        }        
    }    
    
}