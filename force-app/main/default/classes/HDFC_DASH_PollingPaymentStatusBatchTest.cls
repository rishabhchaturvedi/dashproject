/**
className: HDFC_DASH_PollingPaymentStatusBatchTest
DevelopedBy: Ashita Jain
Date: 29 Oct 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_PollingPaymentStatusBatchClass
**/
@istest(SeeAllData = false)
public with sharing class HDFC_DASH_PollingPaymentStatusBatchTest {
    
//Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Application_Payment_Details__c appPaymentDetailObj1 = HDFC_DASH_TestDataFactory_ApplPayDetail.getBasicApplPaymentDetail(app.id);
    private static HDFC_DASH_Application_Payment_Details__c appPaymentDetailObj2 = HDFC_DASH_TestDataFactory_ApplPayDetail.getBasicApplPaymentDetail(app.id);
    
  
    	
  /*
     Method Name: testPollingPaymentStatusBatch
     parameter:none
     Method Description: tests the functionality of Polling Payment Status Batch Class.
  */ 
    
    static testmethod void testPollingPaymentStatusBatch(){
        System.runAs(sysAdmin)
        { 
            appPaymentDetailObj1.HDFC_DASH_AuthStatus__c = '/0002/';
			appPaymentDetailObj1.HDFC_DASH_TxnReferenceNo__c = '3534534';
            appPaymentDetailObj2.HDFC_DASH_AuthStatus__c = '/0002/';
			appPaymentDetailObj2.HDFC_DASH_TxnReferenceNo__c = '3534536'; 
            Database.update(appPaymentDetailObj1);
            Database.update(appPaymentDetailObj2); 
            ID batchJobId=null;
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_TestDataFactory_API.createInterfaceSettingPollingPaymentStatus();
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockPollingPaymentResGenerator());
            HDFC_DASH_PollingPaymentStatusBatchClass pollingPaymentStatus = new HDFC_DASH_PollingPaymentStatusBatchClass();
            Test.startTest();
            batchJobId = Database.executeBatch(pollingPaymentStatus, 200);
            Test.stopTest();
            
             System.assertNotEquals(null,batchJobId);
        }
    }
    
     static testmethod void testPollingPaymentStatusBatchException(){
        System.runAs(sysAdmin)
        {  
            appPaymentDetailObj1.HDFC_DASH_AuthStatus__c = '/2020/';
			appPaymentDetailObj1.HDFC_DASH_TxnReferenceNo__c = '121691';
            appPaymentDetailObj2.HDFC_DASH_AuthStatus__c = '/2021/';
			appPaymentDetailObj2.HDFC_DASH_TxnReferenceNo__c = '121670'; 
            Database.update(appPaymentDetailObj1);
            Database.update(appPaymentDetailObj2);
         
             ID batchJobId=null;
             
             Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockPollingPaymentResGenerator());
         
             Test.startTest();
             batchJobId = Database.executeBatch(new HDFC_DASH_PollingPaymentStatusBatchClass(), 200);
             Test.stopTest();
         
             System.assertNotEquals(null,batchJobId);
        }
    }
    
     static testmethod void testSchedulePollingPayStatusBatch(){
        System.runAs(sysAdmin)
        { 
        HDFC_DASH_SchedulePollingPayStatusBatch  schPollingPayStatusBatch = new HDFC_DASH_SchedulePollingPayStatusBatch();
        String schCronExp = '0 0 23 * * ?'; 
        Test.starttest();
        system.schedule('Schedule Polling Payment Status Batch', schCronExp, schPollingPayStatusBatch); 
        Test.stopTest(); 
        }
    }

}