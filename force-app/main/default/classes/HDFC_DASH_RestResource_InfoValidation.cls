@RestResource(urlMapping='/hdfc/storeinfovalidation/V1.0')
/*
Author: Anisha Arumugam
Class Name:HDFC_DASH_RestResource_InfoValidation
*/
global with sharing class HDFC_DASH_RestResource_InfoValidation {

/*
Method:storeInfoValidation()
Description:This method will store the Applicant info validation data from OS
*/
    @HttpPost
    global static void storeInfoValidation() 
    {        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders;
        Exception exp;
        HDFC_DASH_APIResponse_InfoValidation apiResponse = new HDFC_DASH_APIResponse_InfoValidation();
        
        try
        { 
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestService_InfoVal.processInfoValidation(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                            HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                            + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            system.debug('expDetails => '+ expDetails);
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_INFOVALIDATION, 
                                                        HDFC_DASH_Constants.HANDLE_STOREINFOVALIDATION, 
                                                        HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_STOREINFOVAL, 
                                            request.requestBody.tostring(), (res.responseBody)?.tostring(), 
                                            HDFC_DASH_Constants.METHOD_TYPE_POST, HDFC_DASH_Constants.HANDLE_STOREINFOVALIDATION,
                                            exp, request.headers);        

            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                        res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }
    }    
}