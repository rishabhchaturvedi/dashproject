@isTest
public class HDFC_DASH_CaseUpdateUtilTest {
	
    @isTest
    public static void method1(){
         Case caseRec = HDFC_DASH_TestDataFactory_Case.createBasicCase(null,null);
        caseRec.HDFC_DASH_Mode__c = 'Email';
        caseRec.HDFC_DASH_Sub_mode__c = 'Email';
        caseRec.SuppliedEmail = UserInfo.getUserEmail();
        caseRec.Priority = 'High';
        caseRec.Subject = 'test update util';
        caseRec.Description = 'description';
        caseRec.HDFC_DASH_Category__c = 'CLSS';
        caseRec.HDFC_DASH_Sub_Category__c = 'Property ownership issue';
        caseRec.HDFC_DASH_Case_Type__c = 'Complaint';
        caseRec.HDFC_DASH_Social_media_link__c = 'https://www.google.co.in/';        
        Test.startTest();
        try{
            Database.insert(caseRec);
            HDFC_DASH_CaseUpdateUtil.publishCaseUpdate(caseRec.Id,caseRec.HDFC_DASH_Mode__c,caseRec.HDFC_DASH_Sub_mode__c,
                                                      'Higher Priority',caseRec.SuppliedEmail,'test123@hdfc.com',caseRec.Subject,
                                                      caseRec.Description,caseRec.HDFC_DASH_Category__c,caseRec.HDFC_DASH_Sub_Category__c,
                                                      caseRec.Priority,caseRec.HDFC_DASH_Social_media_link__c);
        }catch(exception e){
            
        }
        Test.stopTest();
        
    }
}