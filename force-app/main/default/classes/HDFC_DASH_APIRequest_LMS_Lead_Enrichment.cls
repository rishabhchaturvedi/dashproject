/**
* 	className: HDFC_DASH_APIRequest_LMS_Lead_Enrichment
DevelopedBy: Sai Shruthi
Date: 21 July 2021
Company: Accenture
Class Description: Apex Class for handling LMS lead enrichment API Request
**/
public inherited sharing class HDFC_DASH_APIRequest_LMS_Lead_Enrichment {
    /* FeeDetails Node */
public class FeeDetails {
		public Decimal applicationProcessingFee;
		public List<PaymentDetail> applicationPaymentDetails;
		public List<FeeConstruct> feeConstruct;
	}
	/* QuoteInfo Node */
	public class QuoteInfo {
		public PrePopulatedAddressDetails prePopulatedAddressDetails;
		public PrePopulatedAddressDetails customerSelectedAddressDetails;
		public String addressType;
		public String addressSource;
        public String addressFrom;
		public String addressChangedByCustomer;
		public Decimal totalExistingEMI;
		public Decimal loanAmount;
		public string loanPurpose;
		public string cibilLoanPurpose;
		public String noEMIPaid;
		public EligibilityAmount eligibilityAmount;
		public CustomerSelectedSpotOffer customerSelectedSpotOffer;
		public String spotOfferEligible;
		public String spotOfferAccepted;
	}
	/* PropertyDetails Node */
	public class PropertyDetails {
		public String projectCode;
		public String projectName;
		public Decimal propertyCost;
		public String propertyHouseIdentifier;
		public String propertyStreetNameIdentifier;
		public String propertyLandmark;
		public String propertyLocality;
		public String propertyVillageTownCity;
		public String propertySubDistrict;
		public String propertyDistrict;
		public String propertyState;
		public String propertyCountry;
		public String propertyPostalCode;
		public String propertyPostOfficeName;
		public String propertyStatus;
        public Decimal propertyIndicativeCost;
	}

	public ApplicationInfo applicationInfo;
	/* FeeConstruct Node */
	public class FeeConstruct {
		public Decimal pfExclTax;
		public Decimal pfInclTax;
		public Decimal stampDuty;
		public Decimal sfExclTax;
		public Decimal sfInclTax;
		public Decimal minFeesExclTax;
		public Decimal minFeesInclTax;
		public Decimal feeosExclTax;
		public Decimal feeosInclTax;
		public Decimal subventAmt;
		public Decimal payableLater;
		public Decimal totFeeExclTax;
		public Decimal totFeeInclTax;
		public String feeType;
	}
	/* EligibilityAmount Node */
	public class EligibilityAmount {
		public Decimal loanProduct;
		public String productDesc;
		public Decimal loanTerm;
		public Decimal requestedLoan;
		public Decimal currentRoi;
		public Decimal possibleTerm;
		public Decimal stretchedTerm;
		public Decimal loanPossible;
		public Decimal stretchedAmt;
		public Decimal possibleEmi;
		public Decimal stretchedEmi;
		public String concession;
		public Decimal normalIir;
		public Decimal normalFoir;
		public Decimal maxIir;
		public Decimal maxFoir;
		public String campaignCode;
		public Decimal niir;
		public Decimal nfoir;
		public Decimal nlcr;
		public Decimal nlvr;
		public Decimal combLcrLtv;
		public Decimal ltvVal;
		public Decimal plrRate;
		public String plrId;
	}
	/* ApplicationInfo Node */
	public class ApplicationInfo {
		public String sfApplicationID;
		public String sfApplicationNumber; 
		public String sfApplicantID;
		public String leadStageNumber;
		public String leadStageDescription;
        public String loanType;
		public String lmsLeadID;
        public String financingFor;
		public String preferredInterestRate;
		public String balanceTransferLoan;
        public String propertyDecided;
		public String propertyCityCode;
		public String propertyStateCode;
		public String propertyCountryCode;
        public PropertyDetails propertyDetails;
		public  ApplicantDetails applicantDetails;
		public List<CoApplicantInfo> coApplicantInfo;
		public QuoteInfo quoteInfo;
		public FeeDetails feeDetails;
	}
	/* RiskScore Node */
	public class RiskScore {
		public String riskScoreCustomerstatus;
		public String riskScoreCustomerflags;
		public String riskScoreRtrexists;
		public String riskScoreCustomergrade;
		public String riskScoreRiskevent;
		public String riskScoreAvailedMoratorium;
		public String riskScoreAvailedRestruct;
		public String riskScoreHdfcStaff;
		public String riskScoreConfidenceScore;
		public String riskScoreExistInNegList;
	}
	/* PersonalDetails Node */
	public class PersonalDetails {
		public String outSystemsID;
		public String genderCode;
		public String salutation;
		public String firstName;
		public String middleName;
		public String lastName;
		public String pan;
		public String panStatus;
		public String panApplied;
		public Date dateOfBirth;
		public String cityCode;
		public String stateCode;
		public String countryCode;
		public String cityName;
		public String stateName;
		public String countryName;
		public String residentType;
		public String relationWithPrimaryApplicant;
		public String fatherFirstName;
		public String fatherMiddleName;
		public String fatherLastName;
		public String maritalStatus;
		public String highestQualification;
		public String highestQualificationCode;
		public String socialCategory;
	}
	/* CustomerSelectedSpotOffer Node */
	public class CustomerSelectedSpotOffer {
		public Decimal customerSelectedLoanAmount;
		public Decimal customerSelectedEMIAmount;
		public String customerSelectedInterestType;
		public Decimal customerSelectedInterestRate;
		public Decimal customerSelectedTenureInMonths;
	}
	/* ApplicantEmploymentDetails Node */
	public class ApplicantEmploymentDetails {
		public String occupation;
		public String workEmail;
		public String employerCode;
		public String employerName;
	}
	/* PrePopulatedAddressDetails Node */
	public class PrePopulatedAddressDetails {
		public String addressLine1;
		public String addressLine2;
		public String addressLine3;
		public String addressLine4;
		public String landmark;
		public String city;
		public String cityCode;
		public String taluka;
		public String district;
		public String state;
		public String stateCode;
		public String country;
		public String countryCode;
		public String pinCode;
		public String postOfficeName;
	}
	/* PaymentDetail Node */
	public class PaymentDetail {
        public String paymentTermsAccepted;
		public String paymentTermsContent;
		public Date paymentTermsAcceptedDate;
		public String merchantId;
		public String customerId;
		public String txnReferenceNo;
		public String bankReferenceNo;
		public String txnAmount;
		public Date txnDate;
		public String authStatus;
		public String errorStatus;
		public String errorDescription;
		public String checksum;
	}
	/* ValidationOutcome Node */
	public class ValidationOutcome {
		public String user_Input_Type;
		public String user_Input_Value;
		public String validated_With;
		public Object validation_Response;
	}
	/* FinancialInfo Node */
	public class FinancialInfo {
		public Decimal fixedMonthlyIncome;
		public Decimal additionalIncome;
		public String noAdditionalIncome;
		public String salarySlipAttached;
	}
	/* ApplicantDetails Node */
	public class ApplicantDetails {
		public String skipEmploymentDetails;
		public ApplicantEmploymentDetails applicantEmploymentDetails;
		public FinancialInfo financialInfo;
		public RiskScore riskScore;
		public Decimal confidenceScore;
		public String experianCibilID;
		public List<ValidationOutcome> validationOutcome;
	}
	/* CoApplicantInfo Node */
	public class CoApplicantInfo {
		public String applicantType;
		public String experianCibilID;
		public PersonalDetails personalDetails;
		public RiskScore riskScore;
		public String skipEmploymentDetails;
		public ApplicantEmploymentDetails employmentDetails;
		public FinancialInfo financialInfo;
		public List<ValidationOutcome> validationOutcome;
	}
}