/* ================================================
 * @Class Name :  IT_InterfaceManualRetryController_Test
 * @author : Accenture
 * @Purpose: This class is used to test the functionality of class  schedule_Batch_Interface_Retry
 * @created date:
 * @Last modified date:03/04/2016
 * @Last modified by : Accenture
 ================================================*/
@istest(SeeAllData = false)
public with sharing class schedule_Batch_Interface_Retry_Test{
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    /***************************************************************************************************************
    *   @Name        :  testMethodFirst                                                              
    *   @Return      :  void                                                                                   
    *   @Description :  To schedule retry   
    ***************************************************************************************************************/
    private static testMethod void testMethodFirst(){
        System.runAs(sysAdmin){
        schedule_Batch_Interface_Retry sh1 = new schedule_Batch_Interface_Retry();
        String sch = '0 0 23 * * ?'; 
        Test.starttest();
        system.schedule('Interface Retry', sch, sh1); 
        Test.stopTest(); 
    }
        
    }
}