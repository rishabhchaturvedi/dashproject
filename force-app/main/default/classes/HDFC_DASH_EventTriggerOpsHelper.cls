/**
className: HDFC_DASH_EventTriggerOpsHelper
DevelopedBy: Tejeswari
Date: 20 January 2022
Company: Accenture 
Class Description: Helper class for HDFC_DASH_EventTriggerOps.
**/
public with sharing class HDFC_DASH_EventTriggerOpsHelper {
    
    /* 
Method Name :filterEventsWithRelatedFields_Insert
Description :This method would be called to filter the list of Events for which have the Related fields.
*/
    public static List<Event> filterEventsWithRelatedFields_Insert()
    {
        List<Event> listEvent = new List<Event>();
        List<Event> listUpdatedEvent = new List<Event>();
        listEvent = trigger.new;
        for(Event eventRec : listEvent)
        {
            if(String.isNotBlank(eventRec.WhoId) || String.isNotBlank(eventRec.WhatId))
            {
                listUpdatedEvent.add(eventRec);
            }
        }
        return listUpdatedEvent;
    }
    
    /* 
Method Name :filterEventsWithRelatedFields_Update
Description :This method would be called to filter the list of Events for which have the Related fields.
*/
    public static List<Event> filterEventsWithRelatedFields_Update()
    {
        List<Event> listEvent = new List<Event>();
        List<Event> listUpdatedEvent = new List<Event>();
        listEvent = trigger.new;
        for(Event eventRec : listEvent)
        {
            Event oldEventRec = (Event)Trigger.oldMap?.get(eventRec.id); 
            Event newEventRec = (Event)Trigger.newMap?.get(eventRec.id); 
            if((oldEventRec.WhoId != newEventRec.WhoId) || (oldEventRec.WhatId != newEventRec.WhatId))
            {
                listUpdatedEvent.add(eventRec);
            }
        }
        return listUpdatedEvent;
    }
    
    /* 
Method Name :populateSubjectNameField
Description :This method would be to Update the subjectName of the Event.
*/ 
    public static void populateSubjectNameField(List<Event> listEvents)
    {
        List<String> listContactIds = new List<String>();
        List<String> listLeadIds = new List<String>();
        List<String> listAccountIds = new List<String>();
        List<String> listAppIds = new List<String>();
        List<Contact> listCon = new List<Contact>();
        List<Lead> listLead = new List<Lead>();
        List<Account> listAcc = new List<Account>();
        List<Opportunity> listApp= new List<Opportunity>();
        Map<String,Contact> mapCon = new Map<String,Contact>();
        Map<String,Lead> mapLead = new Map<String,Lead>();
        Map<String,Account> mapAcc = new Map<String,Account>();
        Map<String,Opportunity> mapApp = new Map<String,Opportunity>();
        List<string> listFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_name};
            List<string> list_AppFields = new List<string>{HDFC_DASH_Constants.ID_STRING, HDFC_DASH_Constants.STRING_name,HDFC_DASH_Constants.ACCOUNT_NAME};
                for(Event eventRec : listEvents){
                    if(String.isNotBlank(eventRec.WhoId)) 
                    {
                        if(String.valueOf(eventRec.WhoId)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_CONTACT)
                        {
                            listContactIds.add(eventRec.WhoId);
                        }
                        else if(String.valueOf(eventRec.WhoId)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_Lead)
                        {
                            listLeadIds.add(eventRec.WhoId);
                        }
                        
                    }
                    else if(String.isNotBlank(eventRec.WhatId))
                    {
                        if(String.valueOf(eventRec.WhatId)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_ACCOUNT)
                        {
                            listAccountIds.add(eventRec.WhatId);
                        }
                        else if(String.valueOf(eventRec.WhatId)?.substring(HDFC_DASH_Constants.INT_ZERO,HDFC_DASH_Constants.INT_THREE) == HDFC_DASH_Constants.PREFIX_APPLICATION)
                        {
                            listAppIds.add(eventRec.WhatId);
                        }
                    }
                    else
                    {
                        eventRec.HDFC_DASH_Subject_Name__c = null;
                    }
                }
        if(!listContactIds.isEmpty())
        {
            listCon = HDFC_DASH_Contact_Selector.getContacts(listContactIds, listFields, HDFC_DASH_Constants.ID_STRING);
        }
        if(!listLeadIds.isEmpty())
        {
            listLead = HDFC_DASH_Lead_Selector.getLeadsBasedOnIds(listFields, listLeadIds);
        }
        if(!listAccountIds.isEmpty())
        {
            listAcc = HDFC_DASH_Account_Selector.getAccounts(listAccountIds, listFields, HDFC_DASH_Constants.ID_STRING);
        }
        if(!listAppIds.isEmpty())
        {
            listApp = HDFC_DASH_Application_Selector.getApplication(listAppIds, list_AppFields, HDFC_DASH_Constants.ID_STRING);
        }
        
        if(!listCon.isEmpty())
        {
            for(Contact conRec:listCon)
            {
                mapCon.put(conRec.id,conRec);
            }
        }
        if(!listLead.isEmpty())
        {
            for(Lead leadRec:listLead)
            {
                mapLead.put(leadRec.id,leadRec);
            }
        }
        if(!listAcc.isEmpty())
        {
            for(Account accountRec:listAcc)
            {
                mapAcc.put(accountRec.id,accountRec);
            }
        }
        if(!listApp.isEmpty())
        {
            for(Opportunity appRec:listApp)
            {
                mapApp.put(appRec.id,appRec);
            }
        }
        
        for(Event eventRec : listEvents)
        {
            if(String.isNotBlank(eventRec.WhoId))
            {
                if(!mapCon.isEmpty() && mapCon.containsKey(eventRec.WhoId))
                {
                    eventRec.HDFC_DASH_Subject_Name__c = mapCon.get(eventRec.WhoId).Name;
                }
                else if(!mapLead.isEmpty() && mapLead.containsKey(eventRec.WhoId))
                {
                    eventRec.HDFC_DASH_Subject_Name__c = mapLead.get(eventRec.WhoId).Name;
                }
            }
            else if(String.isNotBlank(eventRec.WhatId))
            {
                if(!mapAcc.isEmpty() && mapAcc.containsKey(eventRec.WhatId))
                {
                    eventRec.HDFC_DASH_Subject_Name__c = mapAcc.get(eventRec.WhatId).name;
                }
                else if(!mapApp.isEmpty() && mapApp.containsKey(eventRec.WhatId))
                {
                    eventRec.HDFC_DASH_Subject_Name__c = mapApp.get(eventRec.WhatId).Account.Name;
                } 
            }
        }
    }
    /* 
Method Name :filterEventsWithActor
Description :This method would be called to filter the list of Events for which have the Actor ExecutiveCode field.
*/
    public static List<Event> filterEventsWithActor()
    {
        List<Event> listEvent = new List<Event>();
        List<Event> listUpdatedEvent = new List<Event>();
        listEvent = trigger.new;
        for(Event eventRec : listEvent)
        {
            Event oldEventRec = (Event)Trigger.oldMap?.get(eventRec.id); 
            Event newEventRec = (Event)Trigger.newMap?.get(eventRec.id); 
            if((oldEventRec.HDFC_DASH_BSA__c != newEventRec.HDFC_DASH_BSA__c) || (oldEventRec.OwnerId != newEventRec.OwnerId))
            {
                listUpdatedEvent.add(eventRec);
            }
        }
        return listUpdatedEvent;
    }
    /* 
Method Name :populateActorExeCode
Description :This method would be to Update the Actor Executive Code Text of the Event.
*/ 
    public static void populateActorExeCode(List<Event> listEvents)
    {
        for(Event eventRec : listEvents)
        {
            //if(String.isNotBlank(eventRec.HDFC_DASH_Actor_Executive_Code__c))
            eventRec.HDFC_DASH_Actor_Executive_Code_Text__c = eventRec.HDFC_DASH_Actor_Executive_Code__c;
        }
    }
}