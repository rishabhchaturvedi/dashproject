/*
Class: HDFC_DASH_EnrichLead_LMSCalloutTest
Author: Punam Marbate
Date: 14 July 2021
Company: Accenture
Description:This is the test class for Queueable Class for Enriched Lead Callout to LMS 
*/
@isTest(SeeAllData=false)
private with sharing class HDFC_DASH_EnrichLead_LMSCalloutTest {
    
   //Getting the object records
   private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
   private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
   private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
   private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
   private static HDFC_DASH_Applicant_Details__c appDetails = 
                               HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
   private static HDFC_DASH_Applicant_Details__c coAppDetails = 
                               HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
   private static HDFC_DASH_Application_Payment_Details__c appPaymentDetail=
                            HDFC_DASH_TestDataFactory_ApplPayDetail.getBasicApplPaymentDetail(app.id);
   private static HDFC_DASH_Contact_Address_Details__c addressDetail=
                            HDFC_DASH_TestDataFactory_ContAddDetail.getBasicContAddressDetail(con.Id);
   private static HDFC_DASH_Applicant_Employment_Details__c applEmpDetail =
                            HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.Id,con.id);
   private static HDFC_DASH_Info_Validation__c applInfoValidation = 
                               HDFC_DASH_TestDataFactory_AppInfoValid.getBasicApplInfoValidation(appDetails.Id,HDFC_DASH_Constants.STRING_B);
   private static HDFC_DASH_Fee_Detail__c feeDetail = HDFC_DASH_TestDataFactory_FeeDetails.getBasicFeeDetail(app.id);
   private static List<HDFC_DASH_Master_Table__c> listMasterTable = 
                               HDFC_DASH_TestDataFactory_MasterTables.getBasicMasterTable();   
   private static HDFC_DASH_City_Master__c cityMaster = HDFC_DASH_TestDataFactory_MasterTables.getBasicCityMaster();
   private static HDFC_DASH_Project_Master__c projectMaster = HDFC_DASH_TestDataFactory_MasterTables.getBasicProjectMaster(); 
   //private static Interface_Framework_Settings__c ifSetting = new Interface_Framework_Settings__c();
   private static Interface_Settings__c interfaceSetting = new Interface_Settings__c();
    
    /* Method Name: testLMS_EnrichCallout
    Method Description: This method will enqueue and test the Enrich Lead callout to LMS.
    */
    static testmethod void testLMS_EnrichCallout(){
        System.runAs(sysAdmin){
            Id recordTypeId1 = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByName().
                                            get(HDFC_DASH_Constants.RECORDTYPE_COAPP).getRecordTypeId();
            coAppDetails.RecordTypeId =recordTypeId1;
            Database.update(coAppDetails);   
            Id recordTypeId2 = Schema.SObjectType.HDFC_DASH_Applicant_Details__c.getRecordTypeInfosByName().
            get(HDFC_DASH_Constants.RECORDTYPE_PRIMARYAPP).getRecordTypeId();
            appDetails.RecordTypeId=recordTypeId2;
            appDetails.HDFC_DASH_Permanent_Address__c = addressDetail.Id;
            update(appDetails);
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockEnrichLeadResponse());
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            //ifSetting = HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_LMS();
            interfaceSetting = HDFC_DASH_TestDataFactory_API.createInterfaceSetting_LMS_EnrichLead();
            HDFC_DASH_EnrichLead_LMSCallout enrichLead_LMSCallout = new HDFC_DASH_EnrichLead_LMSCallout(app.Id);
            
            ID jobID= System.enqueueJob(enrichLead_LMSCallout);
            Test.stopTest(); 
            System.assertNotEquals(null,jobID);
        }
    }   
    /* Method Name: testLMS_EnrichCalloutEcception
    Method Description: This method will test the exception scenario for Enrich Lead callout to LMS.
    */
    static testmethod void testLMS_EnrichCalloutException(){
        System.runAs(sysAdmin){
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockEnrichLeadResponse());
            //HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            HDFC_DASH_EnrichLead_LMSCallout enrichLead_LMSCallout = new HDFC_DASH_EnrichLead_LMSCallout(app.Id);
           
            ID jobID = System.enqueueJob(enrichLead_LMSCallout);
            Test.stopTest();          
            System.assertNotEquals(null,jobID);
        }
    } 
}