/* ================================================
 * @Class Name :  sendDailyFailedLogMails
 * @author : Accenture
 * @Purpose: 
 * @created date:
 * @Last modified date:
 * @Last modified by : Accenture
 ================================================*/
public with sharing class sendDailyFailedLogMails{
    
    private string logHtml;
    private string salesforceBaseUrl; 
    private static final string  LOG_HTML1='</table></body></html>';
    private static final string  URL_SLASH='/';
    private string subject;
    //private static final string SEMICOLON=';';
    /***************************************************************************************************************
    *   @Name        :  sendDailyFailedLogMails                                                              
    *   @Return      :  --                                                                                     
    *   @Description :  It is a constructor for class sendDailyFailedLogMails.                             
    ***************************************************************************************************************/
    public sendDailyFailedLogMails(List<Integration_Message_Log__c> failedLogList){
        Integer count = 1;
        salesforceBaseUrl = System.URL.getSalesforceBaseUrl().toExternalForm() + URL_SLASH;
        logHtml = '<html><body> Hi Team <br/><table border="1" style="border-collapse: collapse">'+
                '<caption>Following are failed log details for ' + system.Today() + 
                '</caption><tr><th>S.No.</th><th>Interface Name</th><th>Id</th></tr>';
        for(Integration_Message_Log__c logObj : failedLogList){
            logHtml += '<tr><td>' + count + '</td><td>' + logObj.Interface_Name__c + '</td><td>' + '<a href="' +
                        salesforceBaseUrl+logObj.id+'">'+logObj.name + '</a></td></tr>';
            count++;
        }
        logHtml += LOG_HTML1;
        subject = 'Daily Failed Integration Log Details';
    }
    /***************************************************************************************************************
    *   @Name        :  sendDailyFailedLogMails                                                              
    *   @Return      :  --                                                                                     
    *   @Description :  It is a constructor for class sendDailyFailedLogMails.                             
    ***************************************************************************************************************/
    public sendDailyFailedLogMails(List<Integration_Message_Log__c> logList, Boolean thresholdMail){
        Integer count = 1;
        salesforceBaseUrl = System.URL.getSalesforceBaseUrl().toExternalForm() + '/';
        logHtml = '<html><body> Hi Team <br/><table border="1" style="border-collapse:'+
            'collapse"><caption>Following Interface breached callout threshold limit ' + system.Today() + 
            '</caption><tr><th>S.No.</th><th>Interface Name</th><th>Id</th></tr>';
        for(Integration_Message_Log__c logObj : logList){
            logHtml += '<tr><td>' + count + '</td><td>' + logObj.Interface_Name__c + 
                    '</td><td>' + '<a href="' +salesforceBaseUrl+logObj.id+'">'+
                    logObj.name + '</a></td></tr>';
            count++;
        }
        logHtml += LOG_HTML1;
        subject = 'API threshold limit crossed';
    }
    /***************************************************************************************************************
    *   @Name        :  splitValues                                                              
    *   @Return      :  list<string>                                                                                   
    *   @Description :  split string values joined by ';'  
    ***************************************************************************************************************/
    private static list<string> splitValues(string strValues){
        list<string> lstString = new list<string>();
        if(strValues <> null){
            lstString = strValues.split(HDFC_DASH_Constants.SEMICOLON);
        }
        return lstString;
    }
     /***************************************************************************************************************
    *   @Name        :  triggerLogEmail                                                              
    *   @Return      :  void                                                                                   
    *   @Description :   
    ***************************************************************************************************************/
    public void triggerLogEmail(){
        //Map<String,interface_settings__c> interfaceCSMap = InterfaceHandler.RetriveInterfaceSetting();
        Map<string,interface_Framework_Settings__c> mapInterfaceFramSt = interface_Framework_Settings__c.getAll();
        if(mapInterfaceFramSt <> null && mapInterfaceFramSt.values().size()>0 && 
                    !mapInterfaceFramSt.values()[0].Stop_All_CallOuts__c){
            list<Messaging.SingleEmailMessage> mailist = new list<Messaging.SingleEmailMessage>();
            for(string userIds :splitValues(mapInterfaceFramSt.values()[0].User_Id_For_Consolidated_Mails__c)){
               
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSubject(subject);
                mail.setHtmlBody(logHtml );
                mail.setTargetObjectId(userIds);
                mail.saveAsActivity = false;
                mailist.add(mail);
            }
            Messaging.sendEmail(mailist);
        }
    }    
}