/**
className: SendemailforILPSLAC_Test
Class Description: This class SendemailforILPSLAC_Test test the functionality of SendemailforILPSLAC.
**/
@isTest
public class SendemailforILPSLAC_Test {
	/**
methodName: SendemailforILPSLACMethod
Method Description: This method SendemailforILPSLACMethod test the functionality of SendemailforILPSLAC.
**/
    @isTest
    public static void SendemailforILPSLACMethod(){
        List<Case> caselist = new List<Case>();
        List<Id> caseIdlist = new List<Id>();
         Id loanqueryTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Query').getRecordTypeId();
         Id loanRequestTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Loan - Request').getRecordTypeId();
        
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(null,null);
        caseRec2.HDFC_DASH_ILPS_LAC__c = true;
        caseRec2.HDFC_DASH_Attachment_Associated__c = false;
        caseRec2.status = 'Closed';
        caseRec2.To_Email_Address__c = 'test123@hdfc.com';
        caseRec2.RecordtypeId = loanqueryTypeId;
        caseRec2.HDFC_DASH_Case_Type__c = 'Query';
        caseRec2.HDFC_DASH_Description__c = 'Description';
        caseRec2.cc_Notification__c = 'testcc@abc.com,testcc1@abc.com';
        caseRec2.Subject = 'Subject';
       
        caselist.add(caseRec2); 
        
        Case caseRec3 = HDFC_DASH_TestDataFactory_Case.createBasicCase(null,null);
        caseRec3.HDFC_DASH_ILPS_LAC__c = true;
        caseRec3.status = 'Closed';
        caseRec3.HDFC_DASH_Attachment_Associated__c = false;
        caseRec3.To_Email_Address__c = 'test123@hdfc.com';
        caseRec3.RecordtypeId = loanRequestTypeId;
        caseRec3.HDFC_DASH_Case_Type__c = 'Request';
        caseRec3.HDFC_DASH_Description__c = 'Description';
        caseRec3.Subject = 'Subject';
        caseRec3.cc_Notification__c = 'testcc@abc.com,testcc1@abc.com';
        caselist.add(caseRec3);
        
       
        Test.startTest();
        try{
            if (Schema.sObjectType.Case.IsCreateable()){ 
            Database.insert(caselist);
            }
            for(Case c : caselist){
                caseIdlist.add(c.Id);
            }
            SendemailforILPSLAC.sendemailforILPSLAC(caseIdlist);
        }catch(Exception e){
            
            
        }
        
        
        Test.stopTest();
    }
}