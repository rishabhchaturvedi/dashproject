/**
className: HDFC_DASH_RestResource_CoApplicantTest
DevelopedBy: Punam Marbate
Date: 12 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_RestResource_CoApplicant
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_RestResource_CoApplicantTest {
    
    private static final string SUCCESS ='Success';
    private static final string STATUS ='Complete';
    private static final string CIBILID = '100032503832';
    private static final string WORKEMAIL = 'workemail@xyz.com';

    //Getting the object records
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    
    //request and response body for testing
    private static final string REQUEST_BODY ='{"CoApplicant":{"experianCibilId":"100032503832","riskScoreCustomerstatus":"EXISTING","riskScoreCustomerflags":"","riskScoreRtrexists":"N","riskScoreCustomergrade":"","riskScoreRiskevent":"","riskScoreAvailedMoratorium":"N","riskScoreAvailedRestruct":"N","riskScoreHdfcStaff":"","riskScoreConfidenceScore":"","riskScoreExistInNegList":"N","applicantType":"CoApplicant","applicationId":"'+ app.id +'","personalDetails":{"salutation":"Mr.","firstName":"SHIVAM","middleName":"","lastName":"MITTAL","PAN":"EIQPS5342N","PanStatus":"ExistingandValid.AadhaarSeedingisSuccessful.","PanApplied":"No","DateOfBirth":"12/06/2000","City_code":"HARAPANAHALLI","State_code":"KA","ResidentType":"Res"},"SkipEmploymentDetails":"Yes","EmploymentDetails":{"Occupation":"E","WorkEmail":"'+WORKEMAIL+'","EmployerCode":"1076600","EmployerName":"ZSASSOCIATESINDIAPRIVATELIMITED"},"FinancialInfo":{"FixedMonthlyIncome":"10001.00","AdditionalIncome":"10001.00","NoAdditionalIncome":"Yes","SalarySlipAttached":"Yes"}},"validationOutcome":[{"User_input_type":"PAN","User_Input_value":"ASD123DSedf","Validated_with":"NSDL","validation_response":{}}]}';
    private static final string STRING_URL = '/services/apexrest/hdfc/CoApplicant/V1.0/';

    /* 
    Method Name :testStoreCoApplicantInfo
    Parameters  :
    Description :This method is for success scenario which call  HDFC_DASH_RestResource_CoApplicant.storeCoApplicantInfo().
    */
    static TestMethod void testStoreCoApplicantInfo() 
    {        
	    HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestUri = STRING_URL;
        request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
        request.requestBody = Blob.valueOf(REQUEST_BODY);
        request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
        RestContext.request = request;
        RestContext.response = response;
        test.startTest();
        System.runAs(sysAdmin)
        {
            HDFC_DASH_RestResource_CoApplicant.storeCoApplicantInfo();
            test.stopTest();
           
            System.assertNotEquals(null, response);
        }
    }
      
    /*Method Name: testException
    parameters: NONE
    Method Description: This method would check if the exception is thrown when the request headers are not sent
    */
    static TestMethod void testException() 
    {
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);  
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
        System.runAs(sysAdmin)
        {
            RestRequest request = new RestRequest();
            RestResponse response = new RestResponse();
            request.requestUri = STRING_URL;
            request.httpMethod = HDFC_DASH_Constants.METHOD_TYPE_PATCH;
            request = HDFC_DASH_TestDataFactory_API.assignPartialHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY);
            RestContext.request = request;
            RestContext.response = response;
            test.startTest();
                	HDFC_DASH_RestResource_CoApplicant.storeCoApplicantInfo();               	
            test.stopTest();
            system.assertEquals(HDFC_DASH_Constants.INT_FIVE_HUNDRED, response.statusCode);
        }
    }
}