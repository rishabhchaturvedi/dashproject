/**
* 	className: HDFC_DASH_ParseLeadDetails
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class is a wrapper class for HDFC_DASH_RestResource_DedupeCheck for fetching the API request .
**/
public inherited sharing class HDFC_DASH_ParseLeadDetails {
    
    public Lead lead;
    /*Consent Node*/ 
    public class Consent {
        public String voiceConsent;
        public Date voiceConsentDate;
        public String sMSConsent;
        public Date sMSConsentDate;
        public String whatsAppConsent;
        public Date whatsAppConsentDate;
        public String consentContent;
    }
    /*Lead Node*/
    public class Lead {
        public String salutation;
        public String firstName;
        public String middleName;
        public String lastName;
        public String genderCode;
        public String pAN;
        public String panApplied;
        public String panStatusFromNSDL;
        public String cityCode;
        public String stateCode;
        public String countryCode;
        public String cityName;
        public String stateName;
        public String countryName;
        public String residentType;
        public String email;
        public Date dateOfBirth;
        public String mobileCountryCode;
        public String mobile;
        public String outSystemsID;
        public String lmsLeadId;
        public boolean onlycustomerdedupe;
        public boolean paymentForFirstApplication;
        public Consent consent;
    }
    
    
    /* 
Method Name :parse
Description :This method would convert the JSON string to HDFC_DASH_ParseLeadDetails class.
*/
    public static HDFC_DASH_ParseLeadDetails parse(RestRequest request) {
        string json = request.requestBody.toString();
        return (HDFC_DASH_ParseLeadDetails) System.JSON.deserialize(json, HDFC_DASH_ParseLeadDetails.class);
    }
}