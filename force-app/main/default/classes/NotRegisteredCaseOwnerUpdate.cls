public class NotRegisteredCaseOwnerUpdate implements Database.Batchable<sObject>{
    
    public static database.QueryLocator start(Database.BatchableContext bc){
        String status1 = 'Not Registered';
        string nullValue = 'null';
        //string caseN = '00098840';
        string query = 'select id,CaseNumber,OwnerId,Status,HDFC_DASH_Skip_AutoAssignment__c,HDFC_DASH_File_Number__c,HDFC_DASH_Service_Centre__c from Case where Status =: status1';
        system.debug('query...'+query);
        return Database.getQueryLocator(query);
    }
                                                                            
    public static void execute(database.batchableContext bc,List<Case> scope){
        List<Case> caseListToBeUpdated = new List<Case>();
        for(Case c : scope){
            if(c.HDFC_DASH_File_Number__c != null && c.HDFC_DASH_Service_Centre__c != ''){
                system.debug('file number...'+c.HDFC_DASH_File_Number__c+'...home branch...'+c.HDFC_DASH_Service_Centre__c);
                String homeBranch = c.HDFC_DASH_Service_Centre__c+' SC-E';
                try{
                    Group queueId = [select Id from Group where Type =: 'Queue' and Name =: homeBranch limit 1];
                    if(queueId != null){
                        c.OwnerId = queueId.Id;
                        caseListToBeUpdated.add(c);
                    }
                }catch(Exception e){
                    system.debug('Exception...'+e+'...case:...'+c.CaseNumber);
                }
                
                system.debug('c.CaseNumber...'+c.CaseNumber+'..owner str..'+homeBranch+'ownerId...'+c.OwnerId);
                
            }
            
        }
        
        if(caseListToBeUpdated.size() > 0)
            update caseListToBeUpdated;
        
        system.debug('caseListToBeUpdated...'+caseListToBeUpdated);         
    } 
                                                                            
   	public static void finish(database.BatchableContext bc){
        
    }                                                                     

}