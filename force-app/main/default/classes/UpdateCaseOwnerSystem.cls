global class UpdateCaseOwnerSystem implements Database.Batchable<sObject>{

    global static Database.QueryLocator start(Database.BatchableContext bc){
        string ownerName = 'System';
        string query = 'select id,HDFC_DASH_Skip_AutoAssignment__c,OwnerId,Status,HDFC_DASH_File_Number__c from Case where Case_Owner_Name__c =: ownerName';
         system.debug('query...'+query);
        return Database.getQueryLocator([select id,HDFC_DASH_Skip_AutoAssignment__c,Owner.Name,HDFC_DASH_File_Number__c,status,ownerid from Case]);
    }
    
    global static void execute(Database.BatchableContext bc,List<Case> scope){
        List<Case> caseToBeUpdated = new List<Case>();
        string username=[select id from user where name='System' limit 1].id;
        for(Case c : scope){
            string ownername =c.owner.name;
          //  system.debug('c.Owner.Name...'+c.Owner.Name);
            system.debug('c.HDFC_DASH_Skip_AutoAssignment__c...'+c.HDFC_DASH_Skip_AutoAssignment__c);
            if(c.HDFC_DASH_File_Number__c !=null && c.ownerid==username && c.Status !='Resolved' && c.status !='Closed' && c.status !='Merged')
            {
               
            c.HDFC_DASH_Skip_AutoAssignment__c = false;   
            caseToBeUpdated.add(c);
            
            }
        }
        system.debug('caseToBeUpdated...'+caseToBeUpdated);
        if(caseToBeUpdated.size() > 0)
            Database.update(caseToBeUpdated) ;
            
    }
    
    global static void finish(Database.BatchableContext bc){
        
    }
}