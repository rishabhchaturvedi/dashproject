/**
className: HDFC_DASH_OrgWideEmailAdd_SelectorTest
DevelopedBy: Sai Suman
Date: 22 Dec 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_OrgWideEmailAddress_Selector class.
**/
@isTest(SeeAllData = false)
public class HDFC_DASH_OrgWideEmailAdd_SelectorTest {
    
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
/* 
Method Name :testGetOrgWideEmailAddress
Description :This method will test the getOrgWideEmailAddress.
*/
    static testmethod void testGetOrgWideEmailAddress(){     
        User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
        list<OrgWideEmailAddress> emailAddressList = new list<OrgWideEmailAddress>();
        System.runAs(sysAdmin){
            List<String> fieldList = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.DISPLAYNAME,HDFC_DASH_Constants.ADDRESS};
            Test.startTest();
                emailAddressList = HDFC_DASH_OrgWideEmailAddress_Selector.getOrgWideEmailAddress(fieldList);
            Test.stopTest();
        }
        system.assertNotEquals( null, emailAddressList);
}
}