@RestResource(urlMapping='/hdfc/storedetails/V1.0')
/*
Author: Anmol Srivastava
Class Name:HDFC_DASH_RestResource_StoreDetail
*/
global with sharing class HDFC_DASH_RestResource_StoreDetail { 
    @HttpPatch
    /* 
Method:storeDetails() 
Description:This method will store the Spot Offer Details data coming from OS
*/
    global static void storeDetails() {
        RestRequest request = RestContext.request;
        RestResponse res=RestContext.response;  
        
        map<string,string> responseheaders;
        HDFC_DASH_APIResponse_StoreDetail response;
        exception exp;
        
        try
        { 
            responseheaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            response = HDFC_DASH_InboundRestService_StoreDetail.processStoreDetails(request);    
            res.responseBody = Blob.valueOf(JSON.serialize(response));
        }
        catch(Exception e){
            
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_STOREDETAILS, HDFC_DASH_Constants.HANDLE_STOREDETAILS_REQUEST, HDFC_DASH_Constants.METHOD_TYPE_PATCH, expDetails);             
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally{	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_STOREDETAILS, request.requestBody.tostring(), (res.responseBody)?.tostring(), HDFC_DASH_Constants.METHOD_TYPE_PATCH, HDFC_DASH_Constants.HANDLE_STOREDETAILS_REQUEST, exp, request.headers);
            
            if(responseheaders!= NULL){
                for(string responseheaderstr : responseheaders.keySet()){
                    res.addHeader(responseheaderstr, responseheaders.get(responseheaderstr));
                }
            }
        }        
        
    }
}