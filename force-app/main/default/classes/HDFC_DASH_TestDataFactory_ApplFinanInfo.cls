/* 
ClassName: HDFC_DASH_TestDataFactory_ApplFinanInfo
DevelopedBy: Sai Suman Kalyanam
Date:28 Aug 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Applicant Bank Detail object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_ApplFinanInfo {
    public static final string NAME = 'Name';
    
    /* Method Name: createBasicApplicantFinancialInfo
   parameters: ID applicantId
   */
   public static HDFC_DASH_Applicant_Financial_Info__c createBasicApplicantFinancialInfo(ID applicantId)
   {
       HDFC_DASH_Applicant_Financial_Info__c ApplicantFinancialInfo = new HDFC_DASH_Applicant_Financial_Info__c();
       //applPaymentDetail.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));    
       ApplicantFinancialInfo.HDFC_DASH_Applicant_details__c =  (applicantId)  ;       
       return ApplicantFinancialInfo; 
   }
   /* Method Name: getBasicApplicantFinancialInfo
   parameters: ID applID
   Method Description: This method is to generate test data for Applicant Financial detail.
   */
   public static HDFC_DASH_Applicant_Financial_Info__c getBasicApplicantFinancialInfo(ID applicantId)
   {
       HDFC_DASH_Applicant_Financial_Info__c ApplicantFinancialInfo=
       HDFC_DASH_TestDataFactory_ApplFinanInfo.createBasicApplicantFinancialInfo(applicantId);
       if(Schema.sObjectType.HDFC_DASH_Applicant_Financial_Info__c.isCreateable()){
        Database.insert(ApplicantFinancialInfo);
       }
       return ApplicantFinancialInfo;
   }
}