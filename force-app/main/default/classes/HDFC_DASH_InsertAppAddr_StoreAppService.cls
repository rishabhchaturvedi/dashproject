/**
className: HDFC_DASH_InsertAppAddr_StoreAppService
DevelopedBy: Janani Mohankumar
Date: 31 Aug 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the applicant Address Details.
**/
public inherited sharing class HDFC_DASH_InsertAppAddr_StoreAppService {  
    /*
    Method Name :mapAppAddrDetails
    Parameters :Id applicantID,HDFC_DASH_ParseApplication.ApplicantAddressInfo parseAppAddr,HDFC_DASH_Contact_Address_Details__c appAddrDet
    Description :This method would map all the applicant Address details node for storeApp API and sends us the application record.
    */
    public static HDFC_DASH_Contact_Address_Details__c mapAppAddrDetails(Id contactId,Id applicantID, HDFC_DASH_ParseApplication.ApplicantAddressInfo parseAppAddr,HDFC_DASH_Contact_Address_Details__c appAddrDet)
    {
         
        //appAddrDet.Id = parseAppAddr?.sfAppAddDetId;
        appAddrDet.HDFC_DASH_Contact__c = contactId;
        appAddrDet.HDFC_DASH_Applicant_Details__c = applicantID;
        //system.debug('Contact Id:'+appAddrDet.HDFC_DASH_Contact__c);
        appAddrDet.HDFC_DASH_Address_Source__c = parseAppAddr?.applicantAddressDetails?.addressSource;
        appAddrDet.HDFC_DASH_Address_Type__c = parseAppAddr?.applicantAddressDetails?.addressType;
        appAddrDet.HDFC_DASH_Address_From__c = parseAppAddr?.applicantAddressDetails?.addressFrom;
        appAddrDet.HDFC_DASH_Address_Changed__c = parseAppAddr?.applicantAddressDetails?.addressChangedByCustomer; 
        appAddrDet.HDFC_DASH_SamePerCurrentAdd__c =  parseAppAddr?.applicantAddressDetails?.samePermanentAndCurrentAddress;
        appAddrDet.HDFC_DASH_ILPS_Contact_Address_Detail_Id__c = parseAppAddr?.applicantAddressDetails?.ILPSConAddDetId;
        appAddrDet.HDFC_DASH_Contact_Address_Match_Percent__c = parseAppAddr?.applicantAddressDetails?.contactAddressMatchPercentage;
        appAddrDet.HDFC_DASH_Name_Match_Percentage_Address__c = parseAppAddr?.applicantAddressDetails?.nameMatchPercentageFromAddress;
        return appAddrDet;
    } 
    /*
    Method Name :mapAppPrePopAddrDetails
    Parameters :HDFC_DASH_ParseApplication.ApplicantAddressInfo parseAppAddr,HDFC_DASH_Contact_Address_Details__c addrDet
    Description :This method would map all the applicant PrePopulated Address details node for storeApp API and sends us the application record.
    */   
    public static HDFC_DASH_Contact_Address_Details__c mapAppPrePopAddrDetails(HDFC_DASH_ParseApplication.ApplicantAddressInfo parseAppAddr,HDFC_DASH_Contact_Address_Details__c addrDet)
    {
              
        addrDet.HDFC_DASH_Address_Line1__c = parseAppAddr?.prePopulatedAddressDetails?.addressLine1;
        addrDet.HDFC_DASH_Address_Line2__c = parseAppAddr?.prePopulatedAddressDetails?.addressLine2;
        addrDet.HDFC_DASH_Address_Line3__c = parseAppAddr?.prePopulatedAddressDetails?.addressLine3;
        addrDet.HDFC_DASH_Address_Line4__c = parseAppAddr?.prePopulatedAddressDetails?.addressLine4;
        addrDet.HDFC_DASH_City__c = parseAppAddr?.prePopulatedAddressDetails?.city;
        addrDet.HDFC_DASH_City_Code__c = parseAppAddr?.prePopulatedAddressDetails?.cityCode;
        addrDet.HDFC_DASH_Country__c = parseAppAddr?.prePopulatedAddressDetails?.country;
        addrDet.HDFC_DASH_Country_Code__c = parseAppAddr?.prePopulatedAddressDetails?.countryCode;
        addrDet.HDFC_DASH_District__c = parseAppAddr?.prePopulatedAddressDetails?.district;
        addrDet.HDFC_DASH_Landmark__c = parseAppAddr?.prePopulatedAddressDetails?.landmark;
        
        addrDet.HDFC_DASH_State__c = parseAppAddr?.prePopulatedAddressDetails?.state;
        addrDet.HDFC_DASH_State_Code__c = parseAppAddr?.prePopulatedAddressDetails?.stateCode;
        //map city,state,country RecordId
        //addrDet = (HDFC_DASH_Contact_Address_Details__c)HDFC_DASH_UtilityClass.generateMasterTabelFields(addrDet);
        addrDet.HDFC_DASH_Taluka__c = parseAppAddr?.prePopulatedAddressDetails?.taluka;

        addrDet.HDFC_DASH_Pincode__c = parseAppAddr?.prePopulatedAddressDetails?.pincode;
        addrDet.HDFC_DASH_Post_Office_Name__c = parseAppAddr?.prePopulatedAddressDetails?.postOfficeName;
        //map pincode RecordId
        //addrDet = (HDFC_DASH_Contact_Address_Details__c)HDFC_DASH_UtilityClass.generatePincodeMasterFields(addrDet);
 
        return addrDet;                
    }
    /*
    Method Name :mapAppCsAddrDetails
    Parameters  :HDFC_DASH_ParseApplication.ApplicantAddressInfo parseAppAddr,HDFC_DASH_Contact_Address_Details__c addrDet
    Description :This method would map all the applicant CustomerSelected Address details node for storeApp API and sends us the application record.
    */   
    public static HDFC_DASH_Contact_Address_Details__c mapAppCsAddrDetails(HDFC_DASH_ParseApplication.ApplicantAddressInfo parseAppAddr,HDFC_DASH_Contact_Address_Details__c addrDet)
    {
        addrDet.HDFC_DASH_Address_Line1_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.addressLine1CustomerSelected;
        addrDet.HDFC_DASH_Address_Line2_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.addressLine2CustomerSelected;
        addrDet.HDFC_DASH_Address_Line3_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.addressLine3CustomerSelected;
        addrDet.HDFC_DASH_Address_Line4_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.addressLine4CustomerSelected;
        addrDet.HDFC_DASH_City_Code_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.cityCodeCustomerSelected;
        addrDet.HDFC_DASH_City_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.cityCustomerSelected;
        
        addrDet.HDFC_DASH_Country_Code_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.countryCodeCustomerSelected;
        addrDet.HDFC_DASH_Country_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.countryCustomerSelected;
        
        addrDet.HDFC_DASH_District_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.districtCustomerSelected;
        addrDet.HDFC_DASH_Landmark_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.landmarkCustomerSelected;
        
        addrDet.HDFC_DASH_State_Code_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.stateCodeCustomerSelected;
        addrDet.HDFC_DASH_State_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.stateCustomerSelected;
        
        addrDet.HDFC_DASH_Taluka_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.talukaCustomerSelected;
        addrDet.HDFC_DASH_Prop_Own_Status__c = parseAppAddr?.customerSelectedAddressDetails?.propertyOwnershipStatus;
        //map cs_city,cs_state_cs_country recordId
        //addrDet = (HDFC_DASH_Contact_Address_Details__c)HDFC_DASH_UtilityClass.generateMasterTabelFields(addrDet);

        addrDet.HDFC_DASH_Pincode_CustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.pincodeCustomerSelected;
        addrDet.HDFC_DASH_PostOfficeNameCustomerSelected__c = parseAppAddr?.customerSelectedAddressDetails?.postOfficeNameCustomerSelected;
        //map cs_pincode recordId
        //addrDet = (HDFC_DASH_Contact_Address_Details__c)HDFC_DASH_UtilityClass.generatePincodeMasterFields(addrDet);

        return addrDet; 
    }
}