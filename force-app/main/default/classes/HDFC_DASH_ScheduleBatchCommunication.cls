/**
* 	className: HDFC_DASH_ScheduleBatchCommunication
DevelopedBy: Koora Raghavendra
Date: 19 Jan 2022
Company: Accenture
Class Description: This class would schedule the HDFC_DASH_ScheduleBatchCommunication.
**/
global with sharing class HDFC_DASH_ScheduleBatchCommunication implements Schedulable {

    /* 
Method Name :HDFC_DASH_ScheduleBatchCommunication
Parameters  :
Description :Empty Constructor
*/ 
global HDFC_DASH_ScheduleBatchCommunication(){
     
}
     /* 
Method Name :execute
Parameters  :SchedulableContext sc
Description :This method will schedule the HDFC_DASH_EmailSMSWACommunicationBatch
*/ 
global void execute(SchedulableContext sc) {
    HDFC_DASH_EmailSMSWACommunicationBatch emailSMSWACommunicationBatch = new HDFC_DASH_EmailSMSWACommunicationBatch(); 
    database.executebatch(emailSMSWACommunicationBatch,1);
 }
}