/*
Class: HDFC_DASH_ILPS_FilePush_Callout
Author: Raghavendra Koora
Date: 13 December 2021
Company: Accenture
Description: Thid class has methods to do callout to ILPS for filepush
*/
public with sharing class HDFC_DASH_ILPS_FilePush_Callout implements Database.AllowsCallouts {
    
    public static Id applicationId;
    public static Boolean calloutToILPS(Id appId){
        applicationId = appId;
        HttpResponse response;
        Map<String,String> requestHeader = new Map<String,String>();
        RestRequest request;
        InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
        //Get the Document details request body
        HDFC_DASH_ParseApplication reqBody = getRequestBody(appId);            
        system.debug('HDFC_DASH_FirstFilePushCallout Request '+ reqBody);            
        requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ILPS_FILE_PUSH);
        system.debug('Request Generated'+ requestHeader);
        response = interfaceObj.doCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ILPS_FILE_PUSH, 
                                            HDFC_DASH_Constants.METHOD_TYPE_POST,string.ValueOf(Json.serialize(reqBody)), 
                                            requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,
                                            requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
        system.debug('Response Generated'+response);
        Boolean firstPushToILPS = processResponse(response);
        return firstPushToILPS;
      
    }
    /*
    Method: calloutToILPSForPreApprovedLoan
    Parameters:HDFC_DASH_ParseApplication reqBody
    Description: This method will call from the StoreApp for preApproved Loan part(ILPSfilePush Callout will be alled synchronously in Storeapp).
    */ 
    public static void calloutToILPSForPreApprovedLoan(HDFC_DASH_ParseApplication reqBody){
       
        HttpResponse response;
        Map<String,String> requestHeader = new Map<String,String>();
        RestRequest request;
        InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();         
        requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ILPS_FILE_PUSH);
        system.debug('Request Generated'+ requestHeader);
        response = interfaceObj.doCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ILPS_FILE_PUSH, 
                                            HDFC_DASH_Constants.METHOD_TYPE_POST,string.ValueOf(Json.serialize(reqBody)), 
                                            requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,
                                            requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
        system.debug('Response Generated'+response);
        processResponse(response);
       

    }
    /*
    Method: getRequestBody
    Parameters:None
    Description: This method will create the requestBody from the request.
    */ 
    public static HDFC_DASH_ParseApplication getRequestBody(Id appId) 
    {
        HDFC_DASH_ParseApplication parser = HDFC_DASH_InboundRestService_GetApp.getApplicationFields(appId);

        return parser;
    }
    /*
    Method: processResponse
    Parameters:HttpResponse response
    Description: This method will update the ILPSFilepush to true and update application and applicant records
    */ 
    public static Boolean processResponse(HttpResponse response){

        Boolean firstPushToILPS = HDFC_DASH_Constants.BOOLEAN_FALSE;
        HDFC_DASH_API_Response_ILPS_FilePush ilpsFilePushResponse = new HDFC_DASH_API_Response_ILPS_FilePush();
        List<Opportunity> appList = new List<Opportunity>();
        //List<Contact> conList = new List<Contact>();
        String applicationId = '';
        List<HDFC_DASH_Applicant_Details__c> applicantList = new List<HDFC_DASH_Applicant_Details__c>();
        ilpsFilePushResponse = HDFC_DASH_API_Response_ILPS_FilePush.parse(response);
        system.debug('ILPS File Push response--'+ilpsFilePushResponse);

        if(ilpsFilePushResponse.serialDetails !=null){
            for(HDFC_DASH_API_Response_ILPS_FilePush.SerialDetails opprec: ilpsFilePushResponse?.serialDetails){
                applicationId = opprec.sfapplicationid;
                opportunity appToUpdate = new opportunity();
                appToUpdate.id = opprec.sfapplicationid;
                appToUpdate.HDFC_DASH_File_Number__c = String.valueOf(opprec.fileno);
                appToUpdate.HDFC_DASH_File_Completeness_Flag__c  = opprec.filecompletenessflag;
                firstPushToILPS = HDFC_DASH_Constants.BOOLEAN_TRUE;
                //appToUpdate.HDFC_DASH_File_First_Push_To_ILPS__c = HDFC_DASH_Constants.BOOLEAN_TRUE;
                system.debug('appToUpdate---'+appToUpdate);
                appList.add(appToUpdate);
                break;
            }
        }
        if(appList != null && appList.size() > HDFC_DASH_Constants.INT_ZERO){
                update appList;
                system.debug('applicationList after update---'+appList);
        }

        //List<String> applicationFields = new List<String>{HDFC_DASH_Constants.Id,HDFC_DASH_Constants.};
        List<String> responseCustNo = new List<String>();
        List<String> contactFieldList = new List<String>{HDFC_DASH_Constants.Id, HDFC_DASH_Constants.HDFC_DASH_CONTACT_ACCOUNT_SFCUSTNO};
        String conditionOn = HDFC_DASH_Constants.HDFC_DASH_CONTACT_ACCOUNT_SFCUSTNO;
        Map<String,String> conIdToILPSCustNo = new Map<String,String>();
        Map<String,String> custNoToILPSCustNo = new Map<String,String>();
        List<String> applFieldList = new List<String>{HDFC_DASH_Constants.Id,HDFC_DASH_Constants.HDFC_DASH_Contact};
        Map<String,String> applicantQuery = new Map<String,String>{HDFC_DASH_Constants.HDFC_DASH_Application => HDFC_DASH_Constants.STRING_EQUALTO + HDFC_DASH_Constants.STRING_QUOTE + applicationId + HDFC_DASH_Constants.STRING_QUOTE };


        if(ilpsFilePushResponse?.applicantDetails != null){

            for(HDFC_DASH_API_Response_ILPS_FilePush.applicantDetails appldetail: ilpsFilePushResponse?.applicantDetails){
                if(appldetail.sfcustno != null){
                    responseCustNo.add((String.valueOf(appldetail.sfcustno)));
                    custNoToILPSCustNo.put((String.valueOf(appldetail.sfcustno)),String.valueOf(applDetail.ilpscustnumber));
                    system.debug('Response and custNo to ILPS--'+responseCustNo+' '+custNoToILPSCustNo);
                }
            }

            List<Contact> conRec = HDFC_DASH_Contact_Selector.getContacts(responseCustNo, contactFieldList, conditionOn);

            for(Contact con : conRec){
                for(String custNo: responseCustNo){
                    if(con.Account.HDFC_DASH_SF_Customer_Number__c == custNo){
                        conIdToILPSCustNo.put(con.Id,custNoToILPSCustNo.get(custNo));
                        system.debug('Con--'+con);
                    }
                }
            }

            List<HDFC_DASH_Applicant_Details__c> applicantRec= HDFC_DASH_Applicant_Detail_Selector.getAppDetailsBasedOnQuery(applFieldList,applicantQuery);
            system.debug('applicant Record--'+applicantRec);

            for(HDFC_DASH_Applicant_Details__c appl : applicantRec){
                system.debug('Con map--'+conIdToILPSCustNo.get(appl.HDFC_DASH_Contact__c));
                if(conIdToILPSCustNo.get(appl.HDFC_DASH_Contact__c) != null){
                    HDFC_DASH_Applicant_Details__c applDetail = new HDFC_DASH_Applicant_Details__c();
                    applDetail.Id = appl.Id;
                    applDetail.HDFC_DASH_ILPS_Customer_Number__c = conIdToILPSCustNo.get(appl.HDFC_DASH_Contact__c);
                    applicantList.add(applDetail);
                    system.debug('applDetail--'+applDetail);
                }
            }

            if(applicantList != null && applicantList.size() > HDFC_DASH_Constants.INT_ZERO){
                update applicantList;
            }
        }

        return firstPushToILPS;

    }

            
}