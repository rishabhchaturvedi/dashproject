/*
Author: Janani Mohankumar
Class:HDFC_DASH_ParseResponseWAIntegration
Description: This class will parse the response body of WhatsApp Integration
*/
public class HDFC_DASH_ParseResponseWAIntegration {
    public String status;
    public String message;
    public Data data;
    /*
Class: Data
Description: Class to parse Data
*/
    public class Data {
        public String id;
    }
    /*
Method: parse
Parameters:String json
Description: This method will parse the Response.
*/
    public static HDFC_DASH_ParseResponseWAIntegration parse(String json) {
        return (HDFC_DASH_ParseResponseWAIntegration) System.JSON.deserialize(json, HDFC_DASH_ParseResponseWAIntegration.class);
    }
}