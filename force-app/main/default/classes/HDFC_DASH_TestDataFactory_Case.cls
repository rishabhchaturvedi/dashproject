/*   
ClassName: HDFC_DASH_TestDataFactory_Case
DevelopedBy: Atul Amritanshu, Adwait Mhatre
Date: 08 October 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
                      used throughout HDFC DASH.
                      In this class we will be creating data for Case object based on parameters received
                      from the calling test class
*/

public class HDFC_DASH_TestDataFactory_Case {
    public static final String ORIGIN = 'Email';
    public static final String DESCRIPTION='Test';
    public static final String PRIORITY='customer.service@hdfc.com';
    public static final String PRIORITY1='Low';
    public static final String STATUSVALUE='New';
    public static final String HDBNUMBER='443';
    public static final String CASETYPE='Query';
    public static final String FROMADDRESS = 'robin@gmail.com';
    public static final String MODE = 'Email';
    public static final String SUBMODE = 'Email';
         
    /* Method Name: createBasicEmailCase
    parameters: None
    Class Description: This method is to generate test data for Case origin Email.
    */
    public static Case createBasicEmailCase()
    {
        Case cas = new Case();
        cas.Origin = ORIGIN;
        cas.Description = DESCRIPTION;
        cas.Priority = PRIORITY1;
        cas.HDFC_DASH_Case_Type__c = CASETYPE;
        cas.HDFC_DASH_Remarks__c = 'Test Remarks';
        cas.HDFC_DASH_Mode__c = MODE;
        cas.HDFC_DASH_Sub_mode__c = SUBMODE;
        return cas;
    }
    
    /* Method Name: createBasicCase
    parameters: AccountId,ApplicationId
    Method Description: This method is used to create case related to account and 
    */
    public static Case createBasicCase(Id accId,ID appId)
    {
        Case caseRec = new Case();
        caseRec.Origin = ORIGIN;
        caseRec.AccountId = accId;
        //caseRec.Status = STATUSVALUE;
        caseRec.HDFC_DASH_HDB_Number__c = HDBNUMBER;
        caseRec.HDFC_DASH_Case_Type__c = CASETYPE;
        caseRec.HDFC_DASH_File_Number__c = appId;
        caseRec.HDFC_DASH_Disable_Trigger_Execution__c = true;
        caseRec.HDFC_DASH_Remarks__c = 'Test Remarks';
        return caseRec;        
    }
    
    /* Method Name: getBasicCase
    parameters: AccountId,ApplicationId
    Method Description: This method is to generate test data for Case by calling createBasicCase method.
    */
    public static Case getBasicCase(Id accId,Id appId)
    {
        Case caseRec = HDFC_DASH_TestDataFactory_Case.createBasicCase(accId,appId);
        Database.insert(caseRec);
        return caseRec;
    }
}