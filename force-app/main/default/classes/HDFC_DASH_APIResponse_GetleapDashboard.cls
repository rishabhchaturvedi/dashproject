/*
	Author: Tejeswari
	Class: HDFC_DASH_APIResponse_GetleapDashboard
	Description: Apex Class for handling the API Response of GetLeapDashboard
*/
public class HDFC_DASH_APIResponse_GetleapDashboard {
public List<LeapDashboard> leapDashboard;
/*
        Class: LeapDashboard
        Description: Class to get LeapDashboard
    */
	public class LeapDashboard {
		public String serialNumber;
        public String sfApplicationID;
		public String lrNumber;
		public String leadNumber;
		public String borrowerName;
		public String serviceCenter;
		public String leapFlag;
		public Decimal loanAmount;
		public String empClass;
		public String status;
		public String onlineStage;
		public String fileCompleteness;
		public String docReceived;
        public String digitalBankSt;
		public String digitalItr;
		public String digitalGst;
		public String perfios;
        public String salesExecutive;
		public Date lrDate;
		public Date loginDate;
		public Date itpdDate;
		public Date rhdfcDate;
		public Date approvalDate;
        public String channelOffice;
		public String appraiser;
        public String dsfExecutive;
		public Boolean spotLetterRequest;
		public String source;
        public List<AllDocsReceived> allDocsReceived;
	}
 /*
Class: AllDocsReceived
Description: Class to parse document Details
*/
    public class AllDocsReceived {
        public String sfCustNo;
        public String sfApplId;
        public String fileNo;
        public String custNo;
        public String docType;
        public String capacity;
        public String monthOfPayslip;
        public String monthOfGstReturn;
        public String assessmentYear;
        public String financialYear;
        public String accountType;
        public String bkAccNo;
        public Date fromDate;
        public Date toDate;
        public String bankCode;
        public String currentEmployer;
        public Decimal totalAmountPaid;
        public Integer noOfQuartersPaid;
        public String proofOfCommunication;
        public String kycDocType;
        public String bcpDocType;
        public String docExtractable;
        public String docScannedBy;
        public String docImageOrigin;
        public Decimal latitute;
        public Decimal longitute;
        public String storageIdentifier;
        public String storageType;
        public String docRecSrno;
    }
	
}