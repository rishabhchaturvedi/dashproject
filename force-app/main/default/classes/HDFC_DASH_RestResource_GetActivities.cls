@RestResource(urlMapping='/hdfc/getactivities/V1.0/*')
/*
Class Name:HDFC_DASH_RestResource_GetActivities
Author: Anisha Arumugam
Date: 24 Sept 2021
Company: Accenture
Class Description: This class is to implement the Get Activities API
*/
global with sharing class HDFC_DASH_RestResource_GetActivities {
    
    /*
Method:getActivities()
Description:This method will fetch the list of Activities
*/
    @HttpGet
    global static void getActivities() {
        
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        Map<string,string> responseHeaders = new Map<string,String>();
        String response;
        Exception exp;
        HDFC_DASH_ParseActivities apiResponse = new HDFC_DASH_ParseActivities();  
        try
        {    
            responseHeaders = HDFC_DASH_UtilityClass.extractAndValidateHeader(request.headers);
            apiResponse = HDFC_DASH_InboundRestSer_GetActivities.getActivities(request);
            res.responseBody = Blob.valueOf(JSON.serialize(apiResponse));
            
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + 
                HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName(); 
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_GETACTIVITIES, HDFC_DASH_Constants.HANDLE_GETACTIVITIES, HDFC_DASH_Constants.METHOD_TYPE_GET, expDetails);
            HDFC_DASH_UtilityClass.generateAPIErrorResponse(e, res);
        }
        finally
        {	  
            //Logging Inbound Callout Request
            InterfaceCallOutProcess.createInboundIntegrationLog(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_GETACTIVITIES, request.requestBody.tostring(), (res.responseBody)?.tostring(), 
                                                                HDFC_DASH_Constants.METHOD_TYPE_GET, HDFC_DASH_Constants.HANDLE_GETACTIVITIES, exp, request.headers);        
            
            if(responseHeaders!= NULL){
                for(string responseHeaderstr : responseHeaders.keySet()){
                    res.addHeader(responseHeaderstr, responseheaders.get(responseHeaderstr));
                }
            }
        }        
    }    
}