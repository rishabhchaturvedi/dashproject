/*
Class: HDFC_DASH_ConfidenceRuleEngineCallout
Author: Sai Suman
Date: 26 Nov 2021
Company: Accenture
Description: Queueable Class to make ConfidenceRuleEngine Callout.
*/
public with sharing class HDFC_DASH_ConfidenceRuleEngineCallout implements Queueable, Database.AllowsCallouts 
{
    Id appId;
    HttpResponse response;
    public Map<String,String> requestHeader = new Map<String,String>();
    RestRequest request;
    Exception exp;
    public HDFC_DASH_ConfidenceRuleEngineCallout(Id appId)
    {
        this.appId = appId;
    }
    /*
Method: execute
Description: This method will Post the details for ConfidenceRuleEngine Callout.
*/
    public void execute(QueueableContext context)
    {
        System.debug('This is ConfidenceRuleEngine Callout'+appId);
        try
        {
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();
            
            //Get the ConfidenceRuleEngine callout request body
            HDFC_DASH_APIRequest_ConfidenceRuleEng reqBody = getRequestBody(); 
            system.debug('HDFC_DASH_APIRequest_ConfidenceRuleEng Request '+ reqBody);
            
            //string test = reqBody.replace('APPLICATION_ID','APPLICATION-ID');
            String jsonReqBody=string.ValueOf(Json.serialize(reqBody)).replace(HDFC_DASH_Constants.APPLICATION_ID,HDFC_DASH_Constants.APPLICATIONID).replace(HDFC_DASH_Constants.INSTITUTION_ID, HDFC_DASH_Constants.INSTITUTIONID).replace(HDFC_DASH_Constants.REQUEST_TIME, HDFC_DASH_Constants.REQUESTTIME).replace(HDFC_DASH_Constants.REQUEST_TYPE, HDFC_DASH_Constants.REQUESTTYPE).replace(HDFC_DASH_Constants.APP_TYPE, HDFC_DASH_Constants.APPTYPE).replace(HDFC_DASH_Constants.APPLICANT_ID, HDFC_DASH_Constants.APPLICANTID);
            
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ConfidenceRuleEngine);
            
            //logic for generating different requestid everytime
            String randomReqId=Integer.valueOf(Math.random() * (9000) + HDFC_DASH_Constants.INT_ONE_THOUSAND) + HDFC_DASH_Constants.RandomRequest;
            system.debug('Random request Id'+ randomReqId);
            requestHeader.put('requestid',randomReqId);
            
            system.debug('Request Generated'+ requestHeader);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ConfidenceRuleEngine, 
                                              HDFC_DASH_Constants.METHOD_TYPE_POST, jsonReqBody, 
                                              requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,
                                              requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
            system.debug('Response Generated'+response);
            Opportunity appRec = new Opportunity();
            appRec.id = appId;
            appRec.HDFC_DASH_PostFeeSFProcessingComplete__c = true;
            update appRec;
            system.debug('appRec'+appRec);
            processResponse(response);     
        }
        catch(Exception e)
        {     
            exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.INTERFACE_SETTING_NAME_ConfidenceRuleEngine, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }
    /*
Method: getRequestBody
Parameters:None
Description: This method will create the requestBody from the request.
*/ 
    public HDFC_DASH_APIRequest_ConfidenceRuleEng getRequestBody() 
    {
        list<String> oppFieldList = new list<String>{HDFC_DASH_Constants.ID_STRING,
            HDFC_DASH_Constants.HDFC_DASH_sLoanType,//sLoanType
            HDFC_DASH_Constants.HDFC_DASH_Loan_Amount,//loanAmt
            HDFC_DASH_Constants.HDFC_DASH_Property_Cost,//totalPropertyCost
            HDFC_DASH_Constants.SO_REQUIRED_LOAN_AMOUNT,//reqLoanAmount;
            HDFC_DASH_Constants.LOAN_TYPE,//loanProductType;
            HDFC_DASH_Constants.HDFC_DASH_MinMandDocRcvd,//minMandDocRcvd;
            //HDFC_DASH_Constants.Stage,//stage;
            HDFC_DASH_Constants.HDFC_DASH_Max_ROI,//roi;
            HDFC_DASH_Constants.HDFC_DASH_Loan_Term,//loanTerm;
            HDFC_DASH_Constants.HDFC_DASH_Max_IIR,//maxIIR
            HDFC_DASH_Constants.HDFC_DASH_SO_Total_Existing_EMI,//totalObligation;
            HDFC_DASH_Constants.HDFC_DASH_Max_FOIR,//maxFOIR;
            //Request body HEADER Fields
            HDFC_DASH_Constants.HDFC_DASH_InstitutionID,//INSTITUTION_ID
            HDFC_DASH_Constants.HDFC_DASH_RequestType,//REQUEST_TYPE
            HDFC_DASH_Constants.HDFC_DASH_AppType,//APP_TYPE
            HDFC_DASH_Constants.HDFC_DASH_CheckBureauFlag,//sCheckBureauFlag
            HDFC_DASH_Constants.HDFC_DASH_CheckScoreFlag//sCheckScoreFlag
            };
                Opportunity app = HDFC_DASH_Application_Selector.getApplicationBasedOnId(oppFieldList,appId); 
        HDFC_DASH_APIRequest_ConfidenceRuleEng confidenceRuleEngineReq = new HDFC_DASH_APIRequest_ConfidenceRuleEng();
        
        //generating APPLICATION Node
        HDFC_DASH_APIRequest_ConfidenceRuleEng.application applicationReqNode =  generateApplicationInfo(app);
        
        //generating List of Applicant Node
        List<HDFC_DASH_APIRequest_ConfidenceRuleEng.applicants> listApplicant= generateApplicant(app);
        
        //generating HEADER Node
        HDFC_DASH_APIRequest_ConfidenceRuleEng.header headerNode = generateReqBodyHeader(app);
        
        //adding all nodes to final request node
        confidenceRuleEngineReq.header=headerNode;
        confidenceRuleEngineReq.application =applicationReqNode;
        confidenceRuleEngineReq.applicants=listApplicant;
        System.debug('Confidence Rule Details '+ confidenceRuleEngineReq);
        return confidenceRuleEngineReq;
    }
    /* 
Method Name :generateReqBodyHeader
Parameters  :Opportunity app
Description :This method would generate the HEADER node for HDFC_DASH_APIRequest_ConfidenceRuleEng .
*/
    public HDFC_DASH_APIRequest_ConfidenceRuleEng.header generateReqBodyHeader(Opportunity app){
        HDFC_DASH_APIRequest_ConfidenceRuleEng.header headerNode = new HDFC_DASH_APIRequest_ConfidenceRuleEng.header();
        headerNode.APPLICATION_ID=HDFC_DASH_Constants.HDFC_DASH_APPLICATION_ID; //'R32920017178';
        headerNode.INSTITUTION_ID=app?.HDFC_DASH_InstitutionID__c;//'3994';
        headerNode.REQUEST_TIME=null;
        headerNode.REQUEST_TYPE =app?.HDFC_DASH_RequestType__c;// 'REQUEST';
        headerNode.APP_TYPE = app?.HDFC_DASH_AppType__c;//'HLTD';
        headerNode.sCheckBureauFlag =app?.HDFC_DASH_CheckBureauFlag__c;// 'N';
        headerNode.sCheckScoreFlag =app?.HDFC_DASH_CheckScoreFlag__c;// 'Y';
        return headerNode;
    }
    /* 
Method Name :generateApplicationInfo
Parameters  :Opportunity app
Description :This method would generate the APPLICATION node for HDFC_DASH_APIRequest_ConfidenceRuleEng .
*/
    public HDFC_DASH_APIRequest_ConfidenceRuleEng.application generateApplicationInfo(Opportunity app){
        
        HDFC_DASH_APIRequest_ConfidenceRuleEng.application applicationReqNode = new HDFC_DASH_APIRequest_ConfidenceRuleEng.application();
        
        //application Nodes
        applicationReqNode.sLoanType= app?.HDFC_DASH_sLoanType__c ;
        applicationReqNode.loanAmt = app?.HDFC_DASH_Loan_Amount__c;
        applicationReqNode.totalPropertyCost = app?.HDFC_DASH_Property_Cost__c;
        applicationReqNode.reqLoanAmount = app?.HDFC_DASH_SO_Required_Loan_Amount__c;
        applicationReqNode.loanProductType = app?.HDFC_DASH_Loan_Type__c;
        applicationReqNode.minMandDocRcvd = app?.HDFC_DASH_MinMandDocRcvd__c;
        applicationReqNode.stage = HDFC_DASH_Constants.PostFee;
        applicationReqNode.roi= app?.HDFC_DASH_Max_ROI__c;
        applicationReqNode.loanTerm= app?.HDFC_DASH_Loan_Term__c;
        applicationReqNode.maxIIR= app?.HDFC_DASH_Max_IIR__c;
        applicationReqNode.totalObligation= app?.HDFC_DASH_SO_Total_Existing_EMI__c;
        applicationReqNode.maxFOIR= app?.HDFC_DASH_Max_FOIR__c;
        return applicationReqNode;
    }
    /* 
Method Name :generateApplicant
Parameters  :Opportunity app
Description :This method would generate the APPLICANTS node for HDFC_DASH_APIRequest_ConfidenceRuleEng .
*/
    public List<HDFC_DASH_APIRequest_ConfidenceRuleEng.applicants>  generateApplicant(Opportunity app){
        List<HDFC_DASH_APIRequest_ConfidenceRuleEng.applicants> applicants=  new List<HDFC_DASH_APIRequest_ConfidenceRuleEng.applicants>();
        HDFC_DASH_APIRequest_ConfidenceRuleEng.request applicantReqNode;
        List<Id> listAppID = new List<Id>{app.Id};
            Id appID = app.Id;
        List<HDFC_DASH_Applicant_Details__c> listAppDetails = new List<HDFC_DASH_Applicant_Details__c>();  
        List<String> listOfFields_AppDetails = new List<String>{HDFC_DASH_Constants.ID_STRING,
            HDFC_DASH_Constants.Contact_Account_HDFC_DASH_Customer_Number,//sfCustNo 
            HDFC_DASH_Constants.HDFC_DASH_nMbAckId,//nMbAckId
            HDFC_DASH_Constants.RECORDTYPE_NAME,//RecordTypeID
            HDFC_DASH_Constants.HDFC_DASH_FirstName,//firstname
            HDFC_DASH_Constants.HDFC_DASH_MiddleName,//middlename
            HDFC_DASH_Constants.HDFC_DASH_LastName,//lastname
            HDFC_DASH_Constants.HDFC_DASH_Salutation,//salutation
            HDFC_DASH_Constants.HDFC_DASH_PAN,//pan
            HDFC_DASH_Constants.HDFC_DASH_Income_Considered,//incomeConsidered
            HDFC_DASH_Constants.HDFC_DASH_Resident_Type,//resiType
            HDFC_DASH_Constants.HDFC_DASH_Nat_Of_Emp,//natOfEmp
            HDFC_DASH_Constants.HDFC_DASH_Self_Emp_Type,//selfEmpType
            HDFC_DASH_Constants.HDFC_DASH_Gender,//gender
            //HDFC_DASH_Constants.HDFC_DASH_RiskScore_Customer_Status,//customerStatus
            HDFC_DASH_Constants.HDFC_DASH_RiskScore_Customer_grade,//customerGrade
            HDFC_DASH_Constants.HDFC_DASH_RiskScore_Hdfc_Staff,//hdfcStaff
            HDFC_DASH_Constants.HDFC_DASH_Confidence_Score,//confidenceScore
            HDFC_DASH_Constants.FIXED_MONTHLY_INCOME,//income
            HDFC_DASH_Constants.ADDITIONAL_INCOME,//addlIncome
            HDFC_DASH_Constants.HDFC_DASH_Age,//age
            HDFC_DASH_Constants.RELATION_WITH_PRIMARYAPPLICANT,//relWithBorr
            HDFC_DASH_Constants.HDFC_DASH_Karza_Photo_Match,//karzaPhotoMatch4
            HDFC_DASH_Constants.HDFC_DASH_Form16_Traces_Format,//form16TracesFormat
            HDFC_DASH_Constants.HDFC_DASH_Form16_Id_Match,//form16IdMatch
            HDFC_DASH_Constants.HDFC_DASH_Sal_Slip_Id_Match,//salSlipIdMatch
            HDFC_DASH_Constants.HDFC_DASH_Bank_stmnt_Id_Match,//bkstmntIdMatch
            HDFC_DASH_Constants.HDFC_DASH_Bank_Stmnt_Extractable,//bkstmntExtractable
            HDFC_DASH_Constants.HDFC_DASH_Current_Employment_Years,//curEmpYears
            HDFC_DASH_Constants.HDFC_DASH_Total_Experience_Years,//totalExpYears
            HDFC_DASH_Constants.APPDETAIL_CONTACT_R_IS_CUSTOMER,//existingCustomer
            HDFC_DASH_Constants.HDFC_DASH_Marital_Status,//maritalStatus
            HDFC_DASH_Constants.HDFC_DASH_Form16_Assessment_Year//form16LatestYear
            };
                listAppDetails = HDFC_DASH_Applicant_Detail_Selector.getApplicationDetails(listAppID, listOfFields_AppDetails, HDFC_DASH_Constants.HDFC_DASH_Application);
        
        // List<String> empFieldsList = new List<String>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS,HDFC_DASH_Constants.HDFC_DASH_Nat_Of_Emp,HDFC_DASH_Constants.HDFC_DASH_Self_Emp_Type};//'Id','HDFC_DASH_Nat_Of_Emp__c','createddate','HDFC_DASH_Applicant_Details__c','HDFC_DASH_Self_Emp_Type__c'};
        // List<HDFC_DASH_Applicant_Employment_Details__c> listEmpDetails = new List<HDFC_DASH_Applicant_Employment_Details__c>();
        // String conditionOn = 'HDFC_DASH_Applicant__r.HDFC_DASH_Application__c';
        // Map<String,String> empCoditionMap = new Map<String,String>{HDFC_DASH_Constants.APPDETAILS_R_APPLICATION => HDFC_DASH_Constants.STRING_EQUALTO +HDFC_DASH_Constants.STRING_QUOTE +  app.Id + HDFC_DASH_Constants.STRING_QUOTE  + HDFC_DASH_Constants.STRING_ORDERBY_CREATEDDATE};
        // Map<Id,HDFC_DASH_Applicant_Employment_Details__c> mapAppIdToEmp = new Map<Id,HDFC_DASH_Applicant_Employment_Details__c>();
        
        // //listEmpDetails = HDFC_DASH_Applicant_Employment_Selector.getEmploymentDetails(listAppId,empFieldsList,HDFC_DASH_Constants.HDFC_DASH_APPDETAILS);
        // listEmpDetails = HDFC_DASH_Applicant_Employment_Selector.getAppEmpDetailsBasedOnQuery(empFieldsList,empCoditionMap);
        // for(HDFC_DASH_Applicant_Employment_Details__c empDet: listEmpDetails){
        //     mapAppIdToEmp.put(empDet.HDFC_DASH_Applicant_Details__c,empDet);
        // }
        
        for(HDFC_DASH_Applicant_Details__c applDetailRec : listAppDetails ){
            //generating APPLICANT Node
            applicantReqNode =generateRequestNode(applDetailRec); 
            HDFC_DASH_APIRequest_ConfidenceRuleEng.Applicants aa = new HDFC_DASH_APIRequest_ConfidenceRuleEng.Applicants();
            //aa.APPLICANT_ID = applDetailRec?.HDFC_DASH_Contact__r?.Account?.HDFC_DASH_Customer_Number__c;
            aa.APPLICANT_ID = applDetailRec?.id;
            
            // applicantReqNode.natOfEmp = mapAppIdToEmp.get(applDetailRec.Id).HDFC_DASH_Nat_Of_Emp__c;
            // applicantReqNode.selfEmpType = mapAppIdToEmp.get(applDetailRec.Id).HDFC_DASH_Self_Emp_Type__c;
            
            aa.request=applicantReqNode;
            applicants.add(aa);
        }
        return applicants;
    }
    /* 
Method Name :generateRequestNode
Parameters  :HDFC_DASH_Applicant_Details__c applDetailRec
Description :This method would generate the applicant details node for HDFC_DASH_APIRequest_ConfidenceRuleEng .
*/
    public HDFC_DASH_APIRequest_ConfidenceRuleEng.request generateRequestNode(HDFC_DASH_Applicant_Details__c applDetailRec){
        HDFC_DASH_APIRequest_ConfidenceRuleEng.request applicantReqNode = new HDFC_DASH_APIRequest_ConfidenceRuleEng.request();
        applicantReqNode.sCustomerId = applDetailRec?.HDFC_DASH_Contact__r?.Account?.HDFC_DASH_Customer_Number__c;
        applicantReqNode.nMbAckId = applDetailRec?.HDFC_DASH_nMbAckId__c;
        applicantReqNode.sApplicantType = applDetailRec?.RecordType.Name;
        applicantReqNode.firstName = applDetailRec?.HDFC_DASH_FirstName__c;
        applicantReqNode.middleName = applDetailRec?.HDFC_DASH_MiddleName__c;
        applicantReqNode.lastName = applDetailRec?.HDFC_DASH_LastName__c;
        applicantReqNode.salutation = applDetailRec?.HDFC_DASH_Salutation__c;
        //applicantReqNode.sName = (applDetailRec?.HDFC_DASH_FirstName__c==null?'':applDetailRec?.HDFC_DASH_FirstName__c)+' ' +(applDetailRec?.HDFC_DASH_MiddleName__c==null?'':applDetailRec?.HDFC_DASH_MiddleName__c+ ' ')+(applDetailRec?.HDFC_DASH_LastName__c==null?'':applDetailRec?.HDFC_DASH_LastName__c);
        applicantReqNode.pan = applDetailRec?.HDFC_DASH_PAN__c;
        applicantReqNode.incomeConsidered = applDetailRec?.HDFC_DASH_Income_Considered__c;
        applicantReqNode.resiType = applDetailRec?.HDFC_DASH_Resident_Type__c;
        applicantReqNode.natOfEmp = applDetailRec?.HDFC_DASH_Nat_Of_Emp__c;
        applicantReqNode.selfEmpType = applDetailRec?.HDFC_DASH_Self_Emp_Type__c;
        applicantReqNode.gender = applDetailRec?.HDFC_DASH_Gender__c;
        //applicantReqNode.customerStatus = applDetailRec?.HDFC_DASH_RiskScore_Customer_Status__c;
        applicantReqNode.customerGrade = applDetailRec?.HDFC_DASH_RiskScore_Customer_grade__c;
        applicantReqNode.hdfcStaff = applDetailRec?.HDFC_DASH_RiskScore_Hdfc_Staff__c;
        applicantReqNode.confidenceScore = applDetailRec?.HDFC_DASH_Confidence_Score__c;
        applicantReqNode.income = applDetailRec?.HDFC_DASH_SO_Fixed_Monthly_Income__c;
        applicantReqNode.addlIncome = applDetailRec?.HDFC_DASH_SO_Additional_Income__c;
        applicantReqNode.age = applDetailRec?.HDFC_DASH_Age__c;
        applicantReqNode.relWithBorr = applDetailRec?.HDFC_DASH_Relation_with_PrimaryApplicant__c;
        applicantReqNode.karzaPhotoMatch = applDetailRec?.HDFC_DASH_Karza_Photo_Match__c;
        applicantReqNode.form16TracesFormat = applDetailRec?.HDFC_DASH_Form16_Traces_Format__c;
        applicantReqNode.form16IdMatch = applDetailRec?.HDFC_DASH_Form16_Id_Match__c;
        applicantReqNode.salSlipIdMatch = applDetailRec?.HDFC_DASH_Sal_Slip_Id_Match__c;
        applicantReqNode.bkstmntIdMatch = applDetailRec?.HDFC_DASH_Bank_stmnt_Id_Match__c;
        applicantReqNode.bkstmntExtractable = applDetailRec?.HDFC_DASH_Bank_Stmnt_Extractable__c;
        applicantReqNode.curEmpYears = applDetailRec?.HDFC_DASH_Current_Employment_Years__c;
        applicantReqNode.totalExpYears = applDetailRec?.HDFC_DASH_Total_Experience_Years__c;
        applicantReqNode.existingCustomer = applDetailRec?.HDFC_DASH_Contact__r.HDFC_DASH_Is_Customer__c;
        applicantReqNode.maritalStatus = applDetailRec?.HDFC_DASH_Marital_Status__c;
        system.debug('applDetailRec?.HDFC_DASH_SO_Fixed_Monthly_Income__c'+applDetailRec?.HDFC_DASH_SO_Fixed_Monthly_Income__c);
        applicantReqNode.grossSalary = (applDetailRec?.HDFC_DASH_SO_Fixed_Monthly_Income__c==null?0:applDetailRec?.HDFC_DASH_SO_Fixed_Monthly_Income__c)*(HDFC_DASH_Constants.INT_TWELVE);
        applicantReqNode.form16LatestYear = applDetailRec?.HDFC_DASH_Form16_Assessment_Year__c;
        return applicantReqNode;
    }
    /*
Method: processResponse
Parameters: Response
Description: This method will process the response and store into ruleEngineCaptures Object.
*/
    public void processResponse(HttpResponse response){
        if(response != null && response.getStatusCode() == HDFC_DASH_Constants.INT_TWO_HUNDRED){
            string jsonbody = response.getbody();
            jsonbody = jsonbody.replace(HDFC_DASH_Constants.APPLICANTRESULT, HDFC_DASH_Constants.APPLICANT_RESULT).replace(HDFC_DASH_Constants.APPLICANTID, HDFC_DASH_Constants.APPLICANT_ID);
            system.debug(jsonbody);
            HDFC_DASH_APIResponse_ConfidenceRuleEng responseapp = new HDFC_DASH_APIResponse_ConfidenceRuleEng();
            responseapp = HDFC_DASH_APIResponse_ConfidenceRuleEng.parse(jsonbody);
            system.debug('response='+responseapp);
            list<HDFC_DASH_Rule_Engine_Capture__c> ruleEngineCaptureList = new list<HDFC_DASH_Rule_Engine_Capture__c>();
            system.debug('responseapp?.response'+responseapp?.response);
            for(HDFC_DASH_APIResponse_ConfidenceRuleEng.Response resp : responseapp?.response){
                system.debug('resp?.response'+resp?.response);
                system.debug('resp?.response?.bre'+resp?.response?.bre);
                system.debug('resp?.response?.bre?.APPLICANT_RESULT'+resp?.response?.bre?.APPLICANT_RESULT);
                if(resp?.response?.bre?.APPLICANT_RESULT !=NULL){
                    for(HDFC_DASH_APIResponse_ConfidenceRuleEng.Applicant_Result appResult : resp?.response?.bre?.APPLICANT_RESULT){
                        if(appResult?.RULES !=NULL){
                            for(HDFC_DASH_APIResponse_ConfidenceRuleEng.Rules listRule : appResult?.RULES){
                                HDFC_DASH_Rule_Engine_Capture__c ruleEngineCapture = new HDFC_DASH_Rule_Engine_Capture__c();
                                system.debug('rulename'+listRule.rulename+' ' +appResult.APPLICANT_ID+' '+appId);
                                ruleEngineCapture.HDFC_DASH_Confidence_Rule_Engine_Outcome__c = HDFC_DASH_Constants.BOOLEAN_TRUE;
                                system.debug('engineoutcome='+ruleEngineCapture.HDFC_DASH_Confidence_Rule_Engine_Outcome__c);
                                ruleEngineCapture.HDFC_DASH_Rule_Id__c = listRule.RuleName;
                                system.debug('rulename='+listRule.RuleName);
                                ruleEngineCapture.HDFC_DASH_Stage__c = HDFC_DASH_Constants.PostFee;
                                system.debug('stage='+ruleEngineCapture.HDFC_DASH_Stage__c);
                                ruleEngineCapture.HDFC_DASH_Application__c = appId;
                                system.debug('appID='+appId);
                                ruleEngineCapture.HDFC_DASH_Applicant_Detail__c = appResult.APPLICANT_ID;
                                system.debug('applicantID='+appResult.APPLICANT_ID);
                                system.debug('ruleEngineCapture inserted');
                                ruleEngineCaptureList.add(ruleEngineCapture);
                                
                            }
                        }
                    }
                }
            }
            if(ruleEngineCaptureList !=NULL){
                insert ruleEngineCaptureList; 
        }
            
        }
    }
}