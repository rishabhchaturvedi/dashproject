/**
className: HDFC_DASH_InsertAppEmp_StoreAppService
DevelopedBy: Janani Mohankumar
Date: 30 Aug 2021
Company: Accenture
Class Description: This class is having all the methods for mapping the applicant employment details.
**/
public inherited sharing class HDFC_DASH_InsertAppEmp_StoreAppService {
    /*
    Method Name :mapAppEmpDetails
    Parameters  :Id applicantID,HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail
    Description :This method would map employmentDetails node for storeApp API and sends us the applicant employment record.
    */
    public static HDFC_DASH_Applicant_Employment_Details__c mapEmploymentDetails(Id applicantID, HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail)
    {
        Map<String,String> getOccupationFromNatMap = HDFC_DASH_Constants.getEmpOccupationToNatMap;
        //appEmpDetail.Id = parseEmp?.sfEmpDetId;
        appEmpDetail.HDFC_DASH_Applicant_Details__c = applicantID;
        appEmpDetail.HDFC_DASH_Work_Email__c = parseEmp?.employmentDetails?.workEmail;
        appEmpDetail.HDFC_DASH_Self_Emp_Type__c =parseEmp?.employmentDetails?.selfEmpType;
        appEmpDetail.HDFC_DASH_Nat_Of_Emp__c = parseEmp?.employmentDetails?.occupation;//as per the final master sheet we decided to map occupation with nature of employment
        appEmpDetail.HDFC_DASH_Nat_Of_Emp__c = parseEmp?.employmentDetails?.natureOfEmp;
        appEmpDetail.HDFC_DASH_mcaActiveStatus__c = parseEmp?.employmentDetails?.mcaActiveStatus;
        appEmpDetail.HDFC_DASH_EmployerType__c = parseEmp?.employmentDetails?.employerType;
        appEmpDetail.HDFC_DASH_CriticalityInd__c = parseEmp?.employmentDetails?.criticalityInd;
        appEmpDetail.HDFC_DASH_ActiveEmployer__c = parseEmp?.employmentDetails?.activeEmployer; 
        appEmpDetail.HDFC_DASH_AreaUnderCultivationInAcres__c = parseEmp?.employmentDetails?.areaUnderCultivationInAcers;
        appEmpDetail.HDFC_DASH_Crops_Being_Cultivated__c = parseEmp?.employmentDetails?.cropsBeingCultivated;
        appEmpDetail.HDFC_DASH_Nature_Of_Activity__c = parseEmp?.employmentDetails?.natureOfActivity;
        appEmpDetail.HDFC_DASH_FiledTaxRetForLastThreeYrs__c = parseEmp?.employmentDetails?.filingReturnsForLast3Years;
        appEmpDetail.HDFC_DASH_NoOfYrsITRFiled__c = parseEmp?.employmentDetails?.noOfYearsITRsFiled;
        appEmpDetail.HDFC_DASH_IncomeSubToGSTOrTDS__c = parseEmp?.employmentDetails?.incomeSubjectToGSTTDS;
        appEmpDetail.HDFC_DASH_StatutoryLicense__c	 = parseEmp?.employmentDetails?.statuatoryLicense; 
        appEmpDetail.HDFC_DASH_EstablishedBusinessPremises__c = parseEmp?.employmentDetails?.establishedBusinessPremises; 
        appEmpDetail.HDFC_DASH_Monthly_Pension__c = parseEmp?.employmentDetails?.monthlyPension;  
        appEmpDetail.HDFC_DASH_Primary_Business__c = parseEmp?.employmentDetails?.primaryBusiness;  
        appEmpDetail.HDFC_DASH_Designation2__c = parseEmp?.employmentDetails?.designation2;
        appEmpDetail.HDFC_DASH_ILPS_Emp_Detail_Id__c = parseEmp?.employmentDetails?.ILPSEmpDetID;
        
        /*if(appEmpDetail.HDFC_DASH_Occupation_Sub_Type_OS__c  != null && appEmpDetail.HDFC_DASH_Occupation_Sub_Type_OS__c.equals(HDFC_DASH_Constants.HDFC_DASH_OCCUPATION_SUBTYPE_AGR_AGRA)){
            appEmpDetail.HDFC_DASH_OccupationSubType__c = HDFC_DASH_Constants.HDFC_DASH_AGRICULTURIST;
        }
        else{
            appEmpDetail.HDFC_DASH_OccupationSubType__c = parseEmp?.employmentDetails?.occupationSubType;
        }*/
        if(parseEmp?.employmentDetails?.occupationSubTypeOS  != null && 
            (parseEmp?.employmentDetails?.occupationSubTypeOS.equals(HDFC_DASH_Constants.HDFC_DASH_AGRICULTURIST) || parseEmp?.employmentDetails?.occupationSubTypeOS.equals(HDFC_DASH_Constants.HDFC_DASH_AGRICULTURIST_ALLIED))){
            appEmpDetail.HDFC_DASH_OccupationSubType__c = 'AGR';
            //system.debug('OccupationSubType-->'+appEmpDetail.HDFC_DASH_OccupationSubType__c);
        }
        else{
            appEmpDetail.HDFC_DASH_OccupationSubType__c = parseEmp?.employmentDetails?.occupationSubType;
        }
        appEmpDetail.HDFC_DASH_Occupation_Sub_Type_OS__c = parseEmp?.employmentDetails?.occupationSubTypeOS;

        //System.debug('xxx Nature Of employment ' + appEmpDetail.HDFC_DASH_Nat_Of_Emp__c);
        //System.debug('xxx occupation to map' + getOccupationFromNatMap);
        String recordTypeName;
        if (parseEmp?.employmentDetails?.natureOfEmp != Null){
          recordTypeName = getOccupationFromNatMap.get(parseEmp?.employmentDetails?.natureOfEmp);
        }
        else if(parseEmp?.employmentDetails?.previousEmployer==null?false:parseEmp?.employmentDetails?.previousEmployer){
            recordTypeName = HDFC_DASH_Constants.HDFC_DASH_Previous_Employer;
        }
        appEmpDetail.HDFC_DASH_Previous_Employer__c = parseEmp?.employmentDetails?.previousEmployer==null?false:parseEmp?.employmentDetails?.previousEmployer;
        appEmpDetail.RecordTypeId = Schema.SObjectType.HDFC_DASH_Applicant_Employment_Details__c.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();      
        return appEmpDetail;
    }
    
    
    public static HDFC_DASH_Applicant_Employment_Details__c mapPPEmployerInfo(Id applicantID, HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail)
    {
        appEmpDetail.HDFC_DASH_Employee_Number__c = parseEmp?.prePopulatedEmployerInformation?.employeeNumber;
        appEmpDetail.HDFC_DASH_Designation__c = parseEmp?.prePopulatedEmployerInformation?.designation;
        appEmpDetail.HDFC_DASH_Department__c = parseEmp?.prePopulatedEmployerInformation?.department;
        appEmpDetail.HDFC_DASH_Job_Duration_In_Months__c = parseEmp?.prePopulatedEmployerInformation?.jobDurationInMonths;
        appEmpDetail.HDFC_DASH_Employer_Code__c  = parseEmp?.prePopulatedEmployerInformation?.employerCode;
        appEmpDetail.HDFC_DASH_Employer_Name__c = parseEmp?.prePopulatedEmployerInformation?.employerName;
        //for employer record id
        //appEmpDetail = (HDFC_DASH_Applicant_Employment_Details__c)HDFC_DASH_UtilityClass.generateEmployerTabelFields(appEmpDetail);
        
        return appEmpDetail;
    }
    /*
    Method Name :mapCSEmployerInfo
    Parameters  :Id applicantID,HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail
    Description :This method would map  CustomerSelectedEmployerInformation node for storeApp API and sends us the applicant Employment record.
    */
    public static HDFC_DASH_Applicant_Employment_Details__c mapCSEmployerInfo(Id applicantID, HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail)
    {
        appEmpDetail.HDFC_DASH_CS_Employee_Number__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedEmployeeNumber;
        appEmpDetail.HDFC_DASH_CS_Designation__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedDesignation;
        appEmpDetail.HDFC_DASH_CS_Department__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedDepartment;
        appEmpDetail.HDFC_DASH_CS_Job_Duration_In_Months__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedJobDurationInMonths;
        appEmpDetail.HDFC_DASH_CS_Employer_Code__c  = parseEmp?.customerSelectedEmployerInformation?.customerSelectedEmployerCode;
        appEmpDetail.HDFC_DASH_CS_Employer_Name__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedEmployerName;
        appEmpDetail.HDFC_DASH_HR_Email_Id__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedHREmailId;
        appEmpDetail.HDFC_DASH_Website__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedWebsite;
        appEmpDetail.HDFC_DASH_CS_Employment_FromDate__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedEmploymentFromDate;
        appEmpDetail.HDFC_DASH_CS_Employment_ToDate__c = parseEmp?.customerSelectedEmployerInformation?.customerSelectedEmploymentToDate;

        //map cs_employer recordId
        //appEmpDetail = (HDFC_DASH_Applicant_Employment_Details__c)HDFC_DASH_UtilityClass.generateEmployerTabelFields(appEmpDetail);
        return appEmpDetail;
    }
    /*
    Method Name :mapEmployerAddressDetails
    Parameters  :Id applicantID,HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail
    Description :This method would map EmployerAddressDetails node for storeApp API and sends us the applicant Employment record.
    */
   public static HDFC_DASH_Applicant_Employment_Details__c mapEmployerAddressDetails(Id applicantID, HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail)
    {
        appEmpDetail.HDFC_DASH_Taluka__c = parseEmp?.prePopulatedEmpAddDetails?.taluka;
        appEmpDetail.HDFC_DASH_State_Code__c = parseEmp?.prePopulatedEmpAddDetails?.stateCode;
        appEmpDetail.HDFC_DASH_State__c = parseEmp?.prePopulatedEmpAddDetails?.state;
        appEmpDetail.HDFC_DASH_Post_Office_Name__c = parseEmp?.prePopulatedEmpAddDetails?.postOfficeName;
        appEmpDetail.HDFC_DASH_Pincode__c = parseEmp?.prePopulatedEmpAddDetails?.pincode;

        //map pincode recordId
        //appEmpDetail = (HDFC_DASH_Applicant_Employment_Details__c)HDFC_DASH_UtilityClass.generatePincodeMasterFields(appEmpDetail);

        appEmpDetail.HDFC_DASH_Landmark__c = parseEmp?.prePopulatedEmpAddDetails?.landmark;
        appEmpDetail.HDFC_DASH_District__c = parseEmp?.prePopulatedEmpAddDetails?.district;
        appEmpDetail.HDFC_DASH_Country_Code__c = parseEmp?.prePopulatedEmpAddDetails?.countryCode;
        appEmpDetail.HDFC_DASH_Country__c = parseEmp?.prePopulatedEmpAddDetails?.country;
        appEmpDetail.HDFC_DASH_City_Code__c = parseEmp?.prePopulatedEmpAddDetails?.cityCode;
        appEmpDetail.HDFC_DASH_City__c = parseEmp?.prePopulatedEmpAddDetails?.city;
        appEmpDetail.HDFC_DASH_Address_Line4__c = parseEmp?.prePopulatedEmpAddDetails?.addressLine4;
        appEmpDetail.HDFC_DASH_Address_Line3__c = parseEmp?.prePopulatedEmpAddDetails?.addressLine3;
        appEmpDetail.HDFC_DASH_Address_Line2__c = parseEmp?.prePopulatedEmpAddDetails?.addressLine2;
        appEmpDetail.HDFC_DASH_Address_Line1__c = parseEmp?.prePopulatedEmpAddDetails?.addressLine1;

        //map city,country,state recordId
        //appEmpDetail = (HDFC_DASH_Applicant_Employment_Details__c)HDFC_DASH_UtilityClass.generateMasterTabelFields(appEmpDetail);
        return appEmpDetail;
    }

    public static HDFC_DASH_Applicant_Employment_Details__c mapEmployerCustSelAddressDetails(Id applicantID, HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail)
    {
        appEmpDetail.HDFC_DASH_CS_Taluka__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedTaluka;
        appEmpDetail.HDFC_DASH_CS_State__c   = parseEmp?.customerSelectedEmpAddDetails.customerSelectedState;
        appEmpDetail.HDFC_DASH_CS_State_Code__c  = parseEmp?.customerSelectedEmpAddDetails.customerSelectedStateCode;
        appEmpDetail.HDFC_DASH_CS_Post_Office_Name__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedppPostOfficeName;
        appEmpDetail.HDFC_DASH_CS_Pincode__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedPincode;
        appEmpDetail.HDFC_DASH_CS_Landmark__c    = parseEmp?.customerSelectedEmpAddDetails.customerSelectedLandmark;
        appEmpDetail.HDFC_DASH_CS_District__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedDistrict;
        appEmpDetail.HDFC_DASH_CS_Country_Code__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedCountryCode;
        appEmpDetail.HDFC_DASH_CS_Country__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedCountry;
        appEmpDetail.HDFC_DASH_CS_City_Code__c   = parseEmp?.customerSelectedEmpAddDetails.customerSelectedCityCode;
        appEmpDetail.HDFC_DASH_CS_City__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedCity;
        appEmpDetail.HDFC_DASH_CS_Address_Line4__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedAddressLine4;
        appEmpDetail.HDFC_DASH_CS_Address_Line3__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedAddressLine3;
        appEmpDetail.HDFC_DASH_CS_Address_Line2__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedAddressLine2;
        appEmpDetail.HDFC_DASH_CS_Address_Line1__c = parseEmp?.customerSelectedEmpAddDetails.customerSelectedAddressLine1;

        return appEmpDetail;

    }

    public static HDFC_DASH_Applicant_Employment_Details__c mapEmployerBussinessDetails(Id applicantId, HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail)
    {
        appEmpDetail.HDFC_DASH_Designation_Entity_Type__c = parseEmp?.PrePopulatedBusinessInformation.designationAndEntityType;
        appEmpDetail.HDFC_DASH_Business_Name__c = parseEmp?.PrePopulatedBusinessInformation.businessName;
        appEmpDetail.HDFC_DASH_Year_Of_Incorporation__c = parseEmp?.PrePopulatedBusinessInformation.yearOfIncorporation;
        appEmpDetail.HDFC_DASH_Annual_Turnover__c = parseEmp?.PrePopulatedBusinessInformation.annualTurnover;
        appEmpDetail.HDFC_DASH_Nature_Of_Business__c = parseEmp?.PrePopulatedBusinessInformation.natureOfBusiness;
        appEmpDetail.HDFC_DASH_Industry_Type__c = parseEmp?.PrePopulatedBusinessInformation.industryType;
        appEmpDetail.HDFC_DASH_Business_PAN__c = parseEmp?.PrePopulatedBusinessInformation.businessPAN;
        appEmpDetail.HDFC_DASH_Udyam_Registration_number__c = parseEmp?.PrePopulatedBusinessInformation.udyamRegistrationNumber;
        appEmpDetail.HDFC_DASH_CIN_Number__c = parseEmp?.PrePopulatedBusinessInformation.cinNumber;

        return appEmpDetail;
    }

    public static HDFC_DASH_Applicant_Employment_Details__c mapEmployerCustSelBussinessDetails(Id applicantId, HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail)
    {
        appEmpDetail.HDFC_DASH_CS_Designation_Entity_Type__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedDesignationAndEntityType;
        appEmpDetail.HDFC_DASH_CS_Business_Name__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedBusinessName;
        appEmpDetail.HDFC_DASH_CS_Year_Of_Incorporation__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedYearOfIncorporation;
        appEmpDetail.HDFC_DASH_CS_Annual_Turnover__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedAnnualTurnover;
        appEmpDetail.HDFC_DASH_CS_Nature_Of_Business__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedNatureOfBusiness;
        appEmpDetail.HDFC_DASH_CS_Industry_Type__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedIndustryType;
        appEmpDetail.HDFC_DASH_CS_Business_PAN__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedBusinessPAN;
        appEmpDetail.HDFC_DASH_CS_Udyam_Registration_number__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedUdyamRegistrationNumber;
        appEmpDetail.HDFC_DASH_CS_CIN_Number__c = parseEmp?.CustomerSelectedBusinessInformation.customerSelectedCINNumber;

        return appEmpDetail;
    }

    public static HDFC_DASH_Applicant_Employment_Details__c mapAdditionalEmploymentDetails(HDFC_DASH_ParseApplication.applicantEmploymentDetails parseEmp,HDFC_DASH_Applicant_Employment_Details__c appEmpDetail){
        
        appEmpDetail.HDFC_DASH_Is_Salary_Getting_Credited__c = parseEmp?.additionalEmploymentDetails?.isSalaryGettingCredited;
        appEmpDetail.HDFC_DASH_StatuatoryDeductionsLikePF_ESI__c = parseEmp?.additionalEmploymentDetails?.statuatoryDeductionsLikePFESI;
        appEmpDetail.HDFC_DASH_On_Third_Party_Payroll__c = parseEmp?.additionalEmploymentDetails?.onThirdPartyPayroll;

        return appEmpDetail;
    }
}