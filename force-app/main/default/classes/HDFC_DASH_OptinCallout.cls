/*
Class: HDFC_DASH_OptinCallout
Author: Tejeswari
Date: 12 Jan 2022
Company: Accenture
Description: Queueable Class to make Optin callout
*/
public with sharing class HDFC_DASH_OptinCallout implements Queueable, Database.AllowsCallouts{
    HttpResponse response;
    RestRequest request; 
    Exception exp;
   	String mobileNumber;
    public Map<String,String> requestHeader = new Map<String,String>();
    
    /**
ConstructorName : HDFC_DASH_OptinCallout
* Parameters : String messageBody
* Description : It is a constructor for class HDFC_DASH_OptinCallout.
*/
    public HDFC_DASH_OptinCallout(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }
    /*
Method: execute
Description: This method will Post the Optin Details to ILPS.
*/
    public void execute(QueueableContext context) 
    {
        try
        {     
            InterfaceCallOutProcess interfaceObj = new InterfaceCallOutProcess();     
            HDFC_DASH_OptinCalloutReq reqBody = getRequestBody();    
            requestHeader = HDFC_DASH_UtilityClass.generateHeader(HDFC_DASH_Constants.HDFC_DASH_OPTIN_ILPSCALLOUT);
            response = interfaceObj.doCallOut(HDFC_DASH_Constants.HDFC_DASH_OPTIN_ILPSCALLOUT, HDFC_DASH_Constants.METHOD_TYPE_POST, string.ValueOf(Json.serialize(reqBody)), requestHeader, HDFC_DASH_Constants.LOG_AFTER_RETRY,requestHeader.get(HDFC_DASH_Constants.TRANSACTION_ID));
        	system.debug('response => ' +response);            
            system.debug('getStatusCode => ' +response?.getStatusCode());
            system.debug('getStatus => ' +response?.getStatus()); 
            system.debug('responsebody => ' +response?.getBody());
        }
        catch(Exception e)
        {     
            Exception exp = e;
            String expDetails = e.getLineNumber() + HDFC_DASH_Constants.SEMICOLON + e.getMessage() + HDFC_DASH_Constants.SEMICOLON + e.getStackTraceString()
                + HDFC_DASH_Constants.SEMICOLON + e.getTypeName();  
            //system.debug('expDetails => ' + expDetails);
            
            HDFC_DASH_ExceptionLogUtil.publishException(HDFC_DASH_Constants.HDFC_DASH_OPTIN_ILPSCALLOUT, HDFC_DASH_Constants.HANDLE_QUEUEABLECLASS, HDFC_DASH_Constants.METHOD_TYPE_POST, expDetails);
        }
    }
    
    /*
Method: getRequestBody
Parameters:None
Description: This method will create the requestBody from the request.
*/ 
    public HDFC_DASH_OptinCalloutReq getRequestBody(){
        HDFC_DASH_OptinCalloutReq optinReq = new HDFC_DASH_OptinCalloutReq();
        List<HDFC_DASH_OptinCalloutReq.Recipients> listOptinRecipients = new List<HDFC_DASH_OptinCalloutReq.Recipients>();
        HDFC_DASH_OptinCalloutReq.Recipients optinRecipient= new HDFC_DASH_OptinCalloutReq.Recipients();
        Interface_Settings__c optin_Integration = Interface_Settings__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_OPTIN_ILPSCALLOUT);
        optinReq.type= HDFC_DASH_Constants.STRING_TYPE_OPTIN;
        optinRecipient.recipient= mobileNumber;
        optinRecipient.source= optin_Integration.Source__c; 
        
        listOptinRecipients.add(optinRecipient);
        optinReq.recipients = listOptinRecipients;
        return optinReq;
    }
}