/* ================================================
 * @Class Name :  IT_Test_SingleRequestMock
 * @author : Accenture
 * @Purpose: This is mock class used to mock single request. 
 * @created date:
 ================================================*/
@isTest(SeeAllData = false)
public class IT_Test_SingleRequestMock implements HttpCalloutMock {
    protected Integer code;
    protected String status;
    protected String bodyAsString;
    protected Blob bodyAsBlob;
    protected Map<String, String> responseHeaders;

    /***************************************************************************************************************
    *   @Name        :  IT_Test_SingleRequestMock                                                              
    *   @Parameters  :  Integer code, String status, String body,Map<String, String> responseHeaders                                                                     
    *   @Description :  It is a constructor for class IT_Test_SingleRequestMock.                             
    ***************************************************************************************************************/
    public IT_Test_SingleRequestMock(Integer code, String status, String body,
                                        Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.bodyAsString = body;
        this.bodyAsBlob = null;
        this.responseHeaders = responseHeaders;
    }
    /***************************************************************************************************************
    *   @Name        :  IT_Test_SingleRequestMock                                                              
    *   @Parameters  :  Integer code, String status, String body,Map<String, String> responseHeaders                                                                     
    *   @Description :  It is a constructor for class IT_Test_SingleRequestMock.                             
    ***************************************************************************************************************/
    public IT_Test_SingleRequestMock(Integer code, String status, Blob body,
                                    Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.bodyAsString = '';
        this.bodyAsBlob = body;
        this.responseHeaders = responseHeaders;
    }
    /***************************************************************************************************************
    *   @Name        :  respond                                                              
    *   @Return      :  HTTPResponse                                                                         
    *   @Description :  This method is used to create response.                      
    ***************************************************************************************************************/
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(code);
        resp.setStatus(status);
        if (bodyAsBlob != null) {
            resp.setBodyAsBlob(bodyAsBlob);
        } else {
            resp.setBody(bodyAsString);
        }

        if (responseHeaders != null) {
                for (String key : responseHeaders.keySet()) {
                    resp.setHeader(key, responseHeaders.get(key));
                }
        }
        return resp;
    }
}