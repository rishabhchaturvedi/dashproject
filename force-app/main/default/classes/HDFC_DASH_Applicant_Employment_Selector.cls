/**
className: HDFC_DASH_Applicant_Employment_Selector
DevelopedBy: Anisha Arumugam
Date: 21 July 2021
Company: Accenture
Class Description: This class would create the select queries for Applicant Employment Details object .
**/
public inherited sharing class HDFC_DASH_Applicant_Employment_Selector {
    
    private static final string SELECT_STRING = 'Select ';
    private static final string FROM_OBJECT = ' from HDFC_DASH_Applicant_Employment_Details__c ';
    private static final string WHERE_STRING = 'where ';
    private static final string ID_STRING = 'ID ';
    private static final string QUERY_PARAMETER = '=:queryParameters';

    /* 
    Method Name :getEmploymentDetailsBasedOnId
    Parameters  :List<string> fieldList, Id ApplicationId
    Description :This method would get a single Applicant Employment details based on the ID.
    */   
    public static HDFC_DASH_Applicant_Employment_Details__c getEmploymentDetailsBasedOnId(List<string> fieldList, Id ApplicationId){
        
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT+ WHERE_STRING+ ID_STRING +HDFC_DASH_Constants.STRING_EQUALTO+ HDFC_DASH_Constants.STRING_QUOTE+ApplicationId+ HDFC_DASH_Constants.STRING_QUOTE;
        HDFC_DASH_Applicant_Employment_Details__c result = database.query(querystring); 
        return result;
    }
    /* 
    Method Name :getEmploymentDetails
    Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
    Description :This method would get the List of Applicant Employment details based on only one condition.
    */
    public static List<HDFC_DASH_Applicant_Employment_Details__c> getEmploymentDetails( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = SELECT_STRING + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_OBJECT + WHERE_STRING + conditionOn + QUERY_PARAMETER;  

        List<HDFC_DASH_Applicant_Employment_Details__c> result = database.query(querystring);        
        
        return result;
    }   
    /* 
    Method Name :getAppEmpDetailsBasedOnQuery
    Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
    Description :This method would get the List of Applicant Employement Details based on fieldsAndparameters sent to it.
    */
    public static List<HDFC_DASH_Applicant_Employment_Details__c> getAppEmpDetailsBasedOnQuery(List<String> fieldList,Map<String,String> fieldsAndparameters){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT + WHERE_STRING +getCondition(fieldsAndparameters) ;

            List<HDFC_DASH_Applicant_Employment_Details__c> appEmpDetList = database.query(querystring); 
            return appEmpDetList;   
    }     
    /* 
    Method Name :getCondition
    Parameters  :Map<String,String> fieldsAndparameters
    Description :This method would be called from getAppEmpDetailsBasedOnQuery .
    */
    public static String getCondition(Map<String,String> fieldsAndparameters){
            String condition ='';
            for(String fieldName : fieldsAndparameters.keySet()){
                condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
            }
         return condition;
    }
    /* 
    Method Name :getEmpDetailsBasedOnAppDetailIds
    Parameters  :List<string> fieldList,List<ID> appDetailIds
    Description :This method would get the HDFC_DASH_Applicant_Employment_Details__c based on HDFC_DASH_Applicant_Details__c IDs.
    
        public static List<HDFC_DASH_Applicant_Employment_Details__c> getEmpDetailsBasedOnAppDetailIds(List<string> fieldList,List<ID> appDetailIds){
            string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +FROM_OBJECT + WHERE_STRING + HDFC_DASH_CONSTANTS.HDFC_DASH_APPDETAILS + HDFC_DASH_CONSTANTS.STRING_IN + HDFC_DASH_Constants.STRING_OPEN_BRACE +HDFC_DASH_Constants.STRING_QUOTE+String.join(appDetailIds, HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.COMMA+HDFC_DASH_Constants.STRING_QUOTE)+HDFC_DASH_Constants.STRING_QUOTE+HDFC_DASH_Constants.STRING_CLOSING_BRACE;
            List<HDFC_DASH_Applicant_Employment_Details__c> appEmpList = database.query(querystring); 
            return appEmpList;
        }*/
}