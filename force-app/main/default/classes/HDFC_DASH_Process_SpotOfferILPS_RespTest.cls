/*
Class: HDFC_DASH_Process_SpotOfferILPS_RespTest
Author: Anmol Srivastava
Date: 30 July 2021
Company: Accenture
Description:This is the test class for HDFC_DASH_Process_SpotOfferILPS_Response.
*/
@isTest(SeeAllData=false)
private with sharing class HDFC_DASH_Process_SpotOfferILPS_RespTest {
    
    private static final string SUCCESS ='Success';
    private static final string CUSTNO ='123454321';
        
     //Getting the object records
     private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
     private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
     private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContactWithAccountId(acc.id);
     private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
     private static HDFC_DASH_Applicant_Details__c appDetails = HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
     private static HDFC_DASH_Applicant_Employment_Details__c empAppDetail = HDFC_DASH_TestDataFactory_ApplEmpDetail.getBasicApplEmploymentDetail(appDetails.id,con.id);
    
    /*
    Method: testdoRetryProcess
    Description: This method will test the doRetryProcess() method in the class HDFC_DASH_Process_SpotOfferILPS_Response.
    */
    static testmethod void  testdoRetryProcess()
    {    
        HttpResponse res = new HttpResponse();
        Map<String, String> mapProcessData = new Map<String, String>();
        
        System.runAs(sysAdmin)
        {
            Account newAcc = [Select id, name, HDFC_DASH_Customer_Number__c from Account where id =: acc.id];
            Opportunity newApplication = [Select id, name, HDFC_DASH_ApplicationNumber__c from opportunity where id =: app.id];

            system.debug('Account CS => ' + newAcc.HDFC_DASH_Customer_Number__c);
            res.setStatusCode(HDFC_DASH_Constants.INT_TWO_HUNDRED);
            res.setBody('{ "sf_appl_id": ' + newApplication.HDFC_DASH_ApplicationNumber__c + ', "file_no": 610123456, "custDetails": [ { "sf_cust_no": ' + newAcc.HDFC_DASH_Customer_Number__c + ', "cust_no": ' + CUSTNO + ', "unq_cust_no": 678901234 } ], "errorMessage":"" }');

            mapProcessData.put(newAcc.HDFC_DASH_Customer_Number__c, appDetails.id);
            string postProcessDataStr = json.serialize(mapProcessData);
            
            Test.startTest();
            HDFC_DASH_Process_SpotOfferILPS_Response spotOfferResp  = new HDFC_DASH_Process_SpotOfferILPS_Response();  
            spotOfferResp.doRetryProcess(res, postProcessDataStr);
            Test.stopTest();   
            //system.assertEquals(1, [select count() from HDFC_DASH_Applicant_Details__c where HDFC_DASH_ILPS_Customer_Number__c =:CUSTNO], SUCCESS);  
            system.assert(true);
        }        
    } 
}