/**
className: HDFC_DASH_SO_BSA_TriggerOpsHelper
DevelopedBy: Tejeswari
Date: 19 November 2021
Company: Accenture 
Class Description:  Helper class for HDFC_DASH_SO_BSA_TriggerOps.
**/
public with sharing class HDFC_DASH_SO_BSA_TriggerOpsHelper {
    /* 
Method Name :filterSoBsaWithAgencyAndFederation
Description :This method would be called to filter the list of SO BSA for which have the related fields.
*/
    public static List<HDFC_DASH_SO_BSA_Association__c > filterSoBsaWithAgencyAndFederation(){
        List<HDFC_DASH_SO_BSA_Association__c> listSoBsa = new List<HDFC_DASH_SO_BSA_Association__c>();
        List<HDFC_DASH_SO_BSA_Association__c> listUpdateSoBsa = new List<HDFC_DASH_SO_BSA_Association__c>();
        listSoBsa = trigger.new;  
        for(HDFC_DASH_SO_BSA_Association__c soBsaRec:listSoBsa){
            if(String.isNotBlank(soBsaRec.HDFC_DASH_Agency_Code__c) || String.isNotBlank(soBsaRec.HDFC_DASH_Federation_Identifier__c)){
                listUpdateSoBsa.add(soBsaRec);
            }
        }
        return listUpdateSoBsa;
    } 
    
    /* 
Method Name :updateSoBsaFields
Description :This method would be to Update the lookup fields of the HDFC_DASH_SO_BSA_Association__c.
*/
    public static void updateSoBsaFields(List<HDFC_DASH_SO_BSA_Association__c> listSoBsa){
        List<String> listAgencyCodes = new List<String>();
        List<String> listFederationIdentifiers = new List<String>();
        List<Account> listAcc = new List<Account>();
        List<User> listUsers = new List<User>();
        Map<String,Id> mapAcc = new Map<String,Id>();
        Map<String,Id> mapUsers = new Map<String,Id>();
        List<String> listUserFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Federation_Id};
            List<String> listAccFields = new List<string>{HDFC_DASH_Constants.ID_STRING,HDFC_DASH_Constants.HDFC_DASH_Agency_Code};
                
                for(HDFC_DASH_SO_BSA_Association__c soBsaRec:listSoBsa){
                    if(String.isNotBlank(soBsaRec.HDFC_DASH_Agency_Code__c)){
                        listAgencyCodes.add(soBsaRec.HDFC_DASH_Agency_Code__c);
                    }
                    if(String.isNotBlank(soBsaRec.HDFC_DASH_Federation_Identifier__c)){
                        listFederationIdentifiers.add(soBsaRec.HDFC_DASH_Federation_Identifier__c);
                    }
                }
        
        if(!listAgencyCodes.isEmpty()){
            listAcc = HDFC_DASH_Account_Selector.getAccounts(listAgencyCodes, listAccFields, HDFC_DASH_Constants.HDFC_DASH_Agency_Code);
        }
        if(!listAcc.isEmpty()){
            for(Account accRec:listAcc){
                mapAcc.put(accRec.HDFC_DASH_Agency_Code__c,accRec.Id);
            }
        }
        
        if(!listFederationIdentifiers.isEmpty()){  
            listUsers = HDFC_DASH_User_Selector.getUser(listFederationIdentifiers, listUserFields, HDFC_DASH_Constants.HDFC_DASH_Federation_Id);
        }
        if(!listUsers.isEmpty()){
            for(User userRec:listUsers){
                mapUsers.put(userRec.FederationIdentifier,userRec.Id);
            }
        }
        
        for(HDFC_DASH_SO_BSA_Association__c soBsaRec : listSoBsa)
        {
            if(String.isNotBlank(soBsaRec.HDFC_DASH_Agency_Code__c)){
                if(mapAcc.containsKey(soBsaRec.HDFC_DASH_Agency_Code__c))
                {
                    soBsaRec.HDFC_DASH_BSA__c = mapAcc.get(soBsaRec.HDFC_DASH_Agency_Code__c);
                }
                else
                {
                    HDFC_DASH_Error_Code__c errorCode_AgencyCode = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_AGENCYCODE_EXP);
                    soBsaRec.adderror(errorCode_AgencyCode.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Agency_Code + HDFC_DASH_Constants.COLON + soBsaRec.HDFC_DASH_Agency_Code__c);
                }
            }
            if(String.isNotBlank(soBsaRec.HDFC_DASH_Federation_Identifier__c)){
                if(mapUsers.containsKey(soBsaRec.HDFC_DASH_Federation_Identifier__c)){
                    soBsaRec.HDFC_DASH_Sales_Officer__c = mapUsers.get(soBsaRec.HDFC_DASH_Federation_Identifier__c ); 
                }
                else
                {
                    HDFC_DASH_Error_Code__c errorCode_FederationIdentifier = HDFC_DASH_Error_Code__c.getInstance(HDFC_DASH_Constants.HDFC_DASH_ERROR_FEDERATIONIDENTIFIER_EXP);
                    soBsaRec.adderror(errorCode_FederationIdentifier.HDFC_DASH_Error_Message__c + HDFC_DASH_Constants.STRING_SPACE + HDFC_DASH_Constants.HDFC_DASH_Federation_Identifier + HDFC_DASH_Constants.COLON + soBsaRec.HDFC_DASH_Federation_Identifier__c);
                }
            }  
        }
    }         
}