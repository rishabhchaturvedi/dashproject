/*
className: HDFC_DASH_EmailTemplate_Selector
DevelopedBy: Sai Suman
Date: 22 December 2021
Company: Accenture
Class Description: This class would create the select queries for Email Template object.
*/
public class HDFC_DASH_EmailTemplate_Selector {
    
    private static final string FROM_EMAILTEMPLATE = ' FROM  EmailTemplate ';
    private static final string WHERE_STRING = 'where ';
    private static final string QUERY_PARAMETER = '=:queryParameters';
    
    /* 
Method Name :getEmailTemplate
Parameters  :List<String> queryParameters, List<string> fieldList, String conditionOn
Description :This method would get the List of Applications based on only one condition.
*/
    public static List<EmailTemplate> getEmailTemplate( List<String> queryParameters, List<string> fieldList, String conditionOn)
    {   
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) + FROM_EMAILTEMPLATE + WHERE_STRING + conditionOn + QUERY_PARAMETER;  
        
        List<EmailTemplate> applicationList = database.query(querystring);        
        
        return applicationList;
    }   
       /* 
Method Name :getEmailTemplateBasedOnQuery
Parameters  :List<String> fieldList,Map<String,String> fieldsAndparameters
Description :This method would get the List of EmailTemplate based on fieldsAndparameters sent to it.
*/
    public static List<EmailTemplate> getEmailTemplateBasedOnQuery(List<String> fieldList, Map<String,String> fieldsAndparameters)
    {
        string querystring = HDFC_DASH_CONSTANTS.STRING_SELECT + String.join(fieldList, HDFC_DASH_Constants.COMMA) +
            FROM_EMAILTEMPLATE + WHERE_STRING +getCondition(fieldsAndparameters) ;
        List<EmailTemplate> emailTemplateList = database.query(querystring); 
        return emailTemplateList;   
    }
    
    /* 
Method Name :getCondition
Parameters  :Map<String,String> fieldsAndparameters
Description :This method would be called from getEmailTemplateBasedOnQuery.
*/
    public static String getCondition(Map<String,String> fieldsAndparameters)
    {
        String condition ='';
        for(String fieldName : fieldsAndparameters.keySet()){
            condition = condition +fieldName+ fieldsAndparameters.get(fieldName);
        }
        return condition;
    }
    
}