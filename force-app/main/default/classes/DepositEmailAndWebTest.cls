@isTest
public class DepositEmailAndWebTest {
    
    @isTest
    public static void method1(){
         Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
        acc.PersonEmail = 'test123@salesforce.com';
        acc.HDFC_DASH_Customer_Number__c = '2345566';
        update acc;
        Test.startTest();
        
        Deposit__x mockedRequest = new Deposit__x(
            BORR_CUST_NO__c = acc.HDFC_DASH_Customer_Number__c ,
            CUST_NAME__c = 'Test Deposit',
            DEPOSIT_NO__c = 'AB/1234567',
            HOME_BRANCH__c = 'NEW DELHI',
            SERVICE_CENTER__c = 'NOIDA'
        );
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,null);
        caseRec2.Subject = 'Deposit : AB/1234567';
        caseRec2.SuppliedEmail = acc.PersonEmail;
        insert caseRec2;
        HDFC_DASH_DepositEmailToCase.mockedRequests.add(mockedRequest);   
        HDFC_DASH_DepositEmailToCase.emailToCaseDepositListener(caseRec2.Id);
        
        Test.stopTest();
        
    }
    
     @isTest
    public static void method2(){
         Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
        acc.PersonEmail = 'test123@salesforce.com';
        acc.HDFC_DASH_Customer_Number__c = '2345566';
        update acc;
        Test.startTest();
        
        Deposit__x mockedRequest = new Deposit__x(
            BORR_CUST_NO__c =  acc.HDFC_DASH_Customer_Number__c ,
            CUST_NAME__c = 'Test Deposit',
            DEPOSIT_NO__c = 'AB/1234567',
            HOME_BRANCH__c = 'NEW DELHI',
            SERVICE_CENTER__c = 'NOIDA'
        );
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,null);
        caseRec2.Subject = 'Fw : Deposit : AB/1234567';
        caseRec2.SuppliedEmail = 'test123@hdfc.com';
        insert caseRec2;
        HDFC_DASH_DepositEmailToCase.mockedRequests.add(mockedRequest);   
        HDFC_DASH_DepositEmailToCase.emailToCaseDepositListener(caseRec2.Id);
        
        Test.stopTest();
        
    }
    
    
     @isTest
    public static void method3(){
         Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();    
         acc.PersonEmail = 'test123@salesforce.com';
        acc.HDFC_DASH_Customer_Number__c = '2345566';
        update acc;
        Test.startTest();       
        Deposit__x mockedRequest = new Deposit__x(
            BORR_CUST_NO__c =  acc.HDFC_DASH_Customer_Number__c ,
            CUST_NAME__c = 'Test Deposit',
            DEPOSIT_NO__c = 'AB/1234567',
            HOME_BRANCH__c = 'NEW DELHI',
            SERVICE_CENTER__c = 'NOIDA'
        );
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,null);
        caseRec2.HDFC_DASH_Deposit_Number__c = 'AB/1234567';
        caseRec2.Origin = 'Phone';
        insert caseRec2;
        HDFC_DASH_DepositEmailToCase.mockedRequests.add(mockedRequest);   
        HDFC_DASH_DepositEmailToCase.emailToCaseDepositListener(caseRec2.Id);
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void method4(){
         Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();    
         acc.PersonEmail = 'test123@salesforce.com';
        acc.HDFC_DASH_Customer_Number__c = '2345566';
        update acc;
        Test.startTest();       
        Deposit__x mockedRequest = new Deposit__x(
            BORR_CUST_NO__c =  acc.HDFC_DASH_Customer_Number__c ,
            CUST_NAME__c = 'Test Deposit',
            DEPOSIT_NO__c = 'AB/1234567',
            HOME_BRANCH__c = 'NEW DELHI',
            SERVICE_CENTER__c = 'NOIDA'
        );
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,null);
        caseRec2.HDFC_DASH_Deposit_Number__c = 'AB/1234567';
        caseRec2.Origin = 'Phone';
        insert caseRec2;
        HDFC_DASH_DepositEmailToCase.mockedRequests.add(mockedRequest);   
        HDFC_DASH_DepositEmailToCase.emailToCaseDepositListener(caseRec2.OwnerId);
        
        Test.stopTest();
        
    }
    
     @isTest
    public static void methodWeb(){
         Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
        acc.PersonEmail = 'test123@salesforce.com';
        acc.HDFC_DASH_Customer_Number__c = '2345566';
        update acc;
        Test.startTest();
        
        Deposit__x mockedRequest = new Deposit__x(
            BORR_CUST_NO__c = acc.HDFC_DASH_Customer_Number__c ,
            CUST_NAME__c = 'Test Deposit',
            DEPOSIT_NO__c = 'AB/1234567',
            HOME_BRANCH__c = 'NEW DELHI',
            SERVICE_CENTER__c = 'NOIDA'
        );
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,null);
        caseRec2.Origin = 'Web';
        caseRec2.HDFC_DASH_Mode__c = 'Website';
        caseRec2.HDFC_DASH_Sub_mode__c = 'Complaints Website';
        caseRec2.Deposit_Number__c = 'AB/1234567';
        caseRec2.SuppliedEmail = acc.PersonEmail;
        caseRec2.HDFC_DASH_Case_Type__c = 'Complaint';
        insert caseRec2;
        HDFC_DASH_CaseCreationForDeposit.mockedRequests.add(mockedRequest);   
        HDFC_DASH_CaseCreationForDeposit.CaseDepositListener(caseRec2.Id);
        
        Test.stopTest();
        
    }
    
     @isTest
    public static void methodException(){
         Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
        acc.PersonEmail = 'test123@salesforce.com';
        acc.HDFC_DASH_Customer_Number__c = '2345566';
        update acc;
        Test.startTest();
        
        Deposit__x mockedRequest = new Deposit__x(
            BORR_CUST_NO__c = acc.HDFC_DASH_Customer_Number__c ,
            CUST_NAME__c = 'Test Deposit',
            DEPOSIT_NO__c = 'AB/1234567',
            HOME_BRANCH__c = 'NEW DELHI',
            SERVICE_CENTER__c = 'NOIDA'
        );
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,null);
        caseRec2.Origin = 'Web';
        caseRec2.HDFC_DASH_Mode__c = 'Website';
        caseRec2.HDFC_DASH_Sub_mode__c = 'Complaints Website';
        caseRec2.HDFC_DASH_Deposit_Number__c = 'AB/1234567';
        caseRec2.SuppliedEmail = acc.PersonEmail;
        caseRec2.HDFC_DASH_Case_Type__c = 'Complaint';
        insert caseRec2;
        HDFC_DASH_CaseCreationForDeposit.mockedRequests.add(mockedRequest);   
        HDFC_DASH_CaseCreationForDeposit.CaseDepositListener(caseRec2.OwnerId);
        
        Test.stopTest();
        
    }
    
     @isTest
    public static void methodPhone(){
         Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicPersonAccount();  
        acc.PersonEmail = 'test123@salesforce.com';
        acc.HDFC_DASH_Customer_Number__c = '2345566';
        update acc;
        Test.startTest();
        
        Deposit__x mockedRequest = new Deposit__x(
            BORR_CUST_NO__c = acc.HDFC_DASH_Customer_Number__c ,
            CUST_NAME__c = 'Test Deposit',
            DEPOSIT_NO__c = 'AB/1234567',
            HOME_BRANCH__c = 'NEW DELHI',
            SERVICE_CENTER__c = 'NOIDA'
        );
        Case caseRec2 = HDFC_DASH_TestDataFactory_Case.createBasicCase(acc.Id,null);
        caseRec2.Origin = 'Phone';
        caseRec2.HDFC_DASH_Mode__c = 'Regulator';
        caseRec2.HDFC_DASH_Sub_mode__c = 'NHB';
        caseRec2.Deposit_Number__c = 'AB/1234567';
        caseRec2.SuppliedEmail = acc.PersonEmail;
        caseRec2.HDFC_DASH_Case_Type__c = 'Complaint';
        caseRec2.ContactId = null;
        insert caseRec2;
        HDFC_DASH_CaseCreationForDeposit.mockedRequests.add(mockedRequest);   
        HDFC_DASH_CaseCreationForDeposit.CaseDepositListener(caseRec2.Id);
        
        Test.stopTest();
        
    }
    
}