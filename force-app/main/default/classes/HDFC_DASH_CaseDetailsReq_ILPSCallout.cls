/*
Class:HDFC_DASH_CaseDetailsReq_ILPSCallout
Author: Pratheeksha
Description: This class will generate the request body for Case Details Callout to ILPS 
*/
public inherited sharing class HDFC_DASH_CaseDetailsReq_ILPSCallout {
	
    public caseDetails caseDetails;
    
        /*
Class: caseDetails
Description: Class to parse case Details
*/
    
    public class caseDetails{
        public String CASE_ID;
        public String REF_TYPE;
        public String REF_NO;
        public String INTENT_DESC;
        public String INTENT_CODE;
        public String INTENT_DIRECTION;
        public String SOURCE_PERSON;
        public String FORM_DATA;

    }
    
    
}