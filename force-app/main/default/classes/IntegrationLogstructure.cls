/* ================================================

* @Class Name :  IntegrationLogstructure
 * @author : Accenture
 * @created date:
 ================================================*/
public class IntegrationLogstructure{
    public string strRresponse{get;set;}
    public string requestBody{get;set;}
    public string statusCode{get;set;}
    public integer noOfRetry{get;set;}
    public boolean isApexError{get;set;} 
    public string errorDescription {get;set;}
    public string errorCodeRetry {get;set;}
    public string methodType {get;set;}
    public string httpHeaderMap{get;set;}
    public boolean InvalidCustomSetting{get;set;}
    public string strPostRetryProcess{get;set;}
    public Decimal strCalloutTime{get;set;}
    public string continuationId{get;set;}
    public string messageId{get;set;}
    /***************************************************************************************************************
    *   @Name        :  IntegrationLogstructure                                                                                                                                                 
    *   @Description :  It is a constructor for class IntegrationLogstructure                             
    ***************************************************************************************************************/
    public IntegrationLogstructure(){
        isApexError= false;
        InvalidCustomSetting =false;
    }
}