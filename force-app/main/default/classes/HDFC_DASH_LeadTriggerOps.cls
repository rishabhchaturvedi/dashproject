/**
className: HDFC_DASH_LeadTriggerOps
DevelopedBy: Sai Shruthi
Date: 06 October 2021
Company: Accenture 
Class Description: This Class contains the trigger operation on Lead object.
**/
public with sharing class HDFC_DASH_LeadTriggerOps {
    
     /**
className: checkForLmsDuplicateLeadId
DevelopedBy: Sai Shruthi
Date: 06 October 2021
Company: Accenture
Class Description: This Class contains the trigger operation to Lead.
**/
  //  public with sharing class checkForLmsDuplicateLeadId implements HDFC_DASH_TriggerOps{
        
        
        /* 
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*//*
        public Boolean isEnabled() {
          return HDFC_DASH_TriggerOpsRecursionFlags.leadDupFieldFirstRun;
        }*/
        /* 
Method Name :filter
Description :This method would filter for the records.
*/
       /* public SObject[] filter(){
           
        List<Lead> duplicateLeadRecs = HDFC_DASH_LeadTriggerOpsHelper.filterLeadsWithSameLMSLeadID();
            return duplicateLeadRecs;  
          //return trigger.new;
        }*/
        /* 
Method Name :execute
Description :This method would execute the logic.
*/
       /* public void execute(SObject[] duplicateLeadRecs){
            HDFC_DASH_LeadTriggerOpsHelper.throwExcForDupLeads(duplicateLeadRecs); 
            //HDFC_DASH_LeadTriggerOpsHelper.bsaFieldsUpdate();
            
            HDFC_DASH_TriggerOpsRecursionFlags.leadDupFieldFirstRun = false;
        }  
    }*/
   
    /**
className: PopulateLeadFieldsInsert
DevelopedBy: Tejeswari
Date: 14 October 2021
Company: Accenture
Class Description: This Class update the lookup fields of lead.
 */ 
  public with sharing class PopulateLeadFieldsInsert implements HDFC_DASH_TriggerOps{
/*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
          return HDFC_DASH_TriggerOpsRecursionFlags.lead_PopulateFields_Insert_FirstRun;
        }
/*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Lead> listLeadRecs = HDFC_DASH_LeadTriggerOpsHelper.filterPopulateLeadFields_Insert();
           	return listLeadRecs;
        }
/*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] leads){
            
            HDFC_DASH_LeadTriggerOpsHelper.populateLeadFields_Insert(leads);   
            HDFC_DASH_TriggerOpsRecursionFlags.lead_PopulateFields_Insert_FirstRun = false;
         
        } 
  }
    
      /**
className: PopulateLeadFieldsUpdate
DevelopedBy: Tejeswari
Date: 14 October 2021
Company: Accenture
Class Description: This Class update the lookup fields of lead.
 */ 
  public with sharing class PopulateLeadFieldsUpdate implements HDFC_DASH_TriggerOps{
/*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
          return HDFC_DASH_TriggerOpsRecursionFlags.lead_PopulateFields_Update_FirstRun;
        }
/*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Lead> listLeadRecs = HDFC_DASH_LeadTriggerOpsHelper.filterPopulateLeadFields_Update();
           	return listLeadRecs;
        }
/*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] leads){
             
            HDFC_DASH_LeadTriggerOpsHelper.populateLeadFields_Update(leads);   
            HDFC_DASH_TriggerOpsRecursionFlags.lead_PopulateFields_Update_FirstRun = false;
         
        } 
  }


   /**
className: PopulateLeadOwnerAgencyFields
DevelopedBy: Tejeswari
Date: 29 December 2021
Company: Accenture
Class Description: This Class update the lookup fields of lead.
 */ 
  public with sharing class PopulateLeadOwnerAgencyFields implements HDFC_DASH_TriggerOps{
/*           
Method Name :isEnabled
Description :This method would check if excecute method's logic needs to run.
*/
        public Boolean isEnabled() {
          return HDFC_DASH_TriggerOpsRecursionFlags.lead_PopulateOwnerAgency_FirstRun;
        }
/*       
Method Name :filter
Description :This method would filter for the records.
*/
        public SObject[] filter(){
            List<Lead> listLeadRecs = HDFC_DASH_LeadTriggerOpsHelper.filterLeadOwnerAgencyFields();
           	return listLeadRecs;
        }
/*      
Method Name :execute
Description :This method would execute the logic.
*/
        public void execute(SObject[] leads){
             
             HDFC_DASH_LeadTriggerOpsHelper.updateLeadOwnerAgencyFields(leads);  
            HDFC_DASH_TriggerOpsRecursionFlags.lead_PopulateOwnerAgency_FirstRun = false;
         
        }  
        
    }


}