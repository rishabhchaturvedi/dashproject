/* 
ClassName:  HDFC_DASH_TestDataFactory_ContentDoc
DevelopedBy: Tejeswari
Date: 21 Oct 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for ContentDocumentLink, ContentVersion objects based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_ContentDoc 
{
    public static final string TITLE = 'sfImage';
    public static final string PATHONCLIENT = 'sfImage.pdf';
    public static final string VERSIONDATA = 'VGhpcyBpcyBUZXh0';    
    
/* Method Name: createBasicConVer
Method Description: This method is to create test data for ContentVersion.
Pass fields of ContentVersion Object
*/
    public static ContentVersion createBasicConVer()
    {
        ContentVersion  conDet = new ContentVersion();
        conDet.title = TITLE;
        conDet.PathOnClient = PATHONCLIENT;
        conDet.VersionData = EncodingUtil.base64Decode(VERSIONDATA);
        return conDet;
    }
 /* Method Name: getContestVersion
Method Description: This method is to generate test data for ContentVersion.
*/
    public static ContentVersion getContestVersion()
    {
        ContentVersion conVerRec = HDFC_DASH_TestDataFactory_ContentDoc.createBasicConVer();
        Database.insert(conVerRec);
        return conVerRec;
    }
/* Method Name: createBasicConDocLink
Method Description: This method is to create test data for ContentDocumentLink.
Pass fields of ContentDocumentLink Object
*/
    public static ContentDocumentLink createBasicConDocLink(String contentDocId, string ContactId)
    {
        ContentDocumentLink conDoc = new ContentDocumentLink();
       	conDoc.ContentDocumentId = contentDocId;
        conDoc.LinkedEntityId = ContactId;
        
        return conDoc;
    }
/* Method Name: getConDocLink
Method Description: This method is to generate test data for ContentDocumentLink.
*/
    public static ContentDocumentLink getConDocLink(String contentDocId,string ContactId)
    {
        ContentDocumentLink conDocRec = HDFC_DASH_TestDataFactory_ContentDoc.createBasicConDocLink(contentDocId, ContactId);
        Database.insert(conDocRec);
        return conDocRec;
    }
}