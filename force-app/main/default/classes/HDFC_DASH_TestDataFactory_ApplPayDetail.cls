/* 
ClassName: HDFC_DASH_TestDataFactory_ApplPayDetail
DevelopedBy: Punam Marbate
Date: 22 July 2021
Company: Accenture
ClassDescription: Test data factory class for maintaining and test data creation which will be
used throughout HDFC DASH.
In this class we will be creating data for Application Payment Detail object based on parameters received
from the calling test class
*/
public with sharing class HDFC_DASH_TestDataFactory_ApplPayDetail {
    public static final string NAME = 'Name';
     
    /* Method Name: createBasicApplPaymentDetail
    parameters: ID applID
    Method Description: This method is to create test data for HDFC_DASH_Application_Payment_Details__c.
    Pass fields of HDFC_DASH_Application_Payment_Details__c, and based on doInsert it will either
    pass an instance of uninserted HDFC_DASH_Application_Payment_Details__c or inserted HDFC_DASH_Application_Payment_Details__c id. 
    */
    public static HDFC_DASH_Application_Payment_Details__c createBasicApplPaymentDetail(ID applID)
    {
        HDFC_DASH_Application_Payment_Details__c applPaymentDetail = new HDFC_DASH_Application_Payment_Details__c();
            
        //applPaymentDetail.Name = NAME+datetime.now().getTime()+Integer.valueof((Math.random() * 1000));
        applPaymentDetail.HDFC_DASH_Application__c =  applID  ;       
        return applPaymentDetail;        
    }
    
    /* Method Name: getBasicApplPaymentDetail
    parameters: ID applID
    Method Description: This method is to generate test data for Application Payment Detail.
    */
    public static HDFC_DASH_Application_Payment_Details__c getBasicApplPaymentDetail(ID applID)
    {
        HDFC_DASH_Application_Payment_Details__c applPaymentDetail = HDFC_DASH_TestDataFactory_ApplPayDetail.createBasicApplPaymentDetail(applID);
        if(Schema.sObjectType.HDFC_DASH_Application_Payment_Details__c.isCreateable()){
            Database.insert(applPaymentDetail);
        }
        return applPaymentDetail;
    } 
}