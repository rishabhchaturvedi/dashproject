/**
className: HDFC_DASH_Task_TriggerOpsTest 
DevelopedBy: Tejeswari
Date: 26 November 2021
Company: Accenture
Class Description: Test class for  HDFC_DASH_Task_TriggerOps class.
*/
@isTest(SeeAllData = false) 
private class HDFC_DASH_Task_TriggerOpsTest {
    private static User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    private static Id sysAdminId = UserInfo.getUserId();
    private static Account acc = HDFC_DASH_TestDataFactory_Accounts.getBasicAccount();
    private static Contact con = HDFC_DASH_TestDataFactory_Contacts.getBasicContact();
    private static Opportunity app = HDFC_DASH_TestDataFactory_Application.getBasicApplication(acc.id);
    private static HDFC_DASH_Applicant_Details__c appDetails = 
        HDFC_DASH_TestDataFactory_AppDetails.getBasicApplicantDetails(con.id, app.id);
    private static final string SMSCOMM_TYPE= 'SMS_25';
    private static final string WACOMM_TYPE= 'WA_25';
    private static final string MOBILE_PHONE= '4567678822';
    /* 
Method Name :testFilterEventsForSms
Description :This method would test the method in trigger helper for the list of Events.
*/
    static testmethod void testFilterEventsForSms(){
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_SMS();  
        
        Exception testException;
        
        Task taskRec = HDFC_DASH_TestDataFactory_Task.createBasicTask(app.id,con.id,sysAdminId);
        taskRec.HDFC_DASH_Is_Outbound_Communication__c =true;
        taskRec.HDFC_DASH_Communication_ID__c = SMSCOMM_TYPE;
        taskRec.HDFC_DASH_Related_Record__c = appDetails.id;
        taskRec.HDFC_DASH_Communication_Type__c = HDFC_DASH_Constants.STRING_SMS;
        taskRec.HDFC_DASH_Target_Number__c = MOBILE_PHONE;
        
        System.runAs(sysAdmin){
            try{
                Test.startTest();
                Database.insert(taskRec);  
               Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            system.assertEquals(null, testException);
            
        }  
    }
    /* 
Method Name :testSMSResponseHandler
Description :This method would test the method in SMS communication for response.
*/
    static testmethod void testSMSResponseHandler()
    {
        Exception testException;
        System.runAs(sysAdmin)
        {
            try{
                HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_SMS();  
                RestRequest request = new RestRequest();   
                HttpResponse response = new HttpResponse();
                response.setStatusCode(200);
                response.setBody('"{\"OBJECT\":[{\"ReturnMsg\":[{\"StatusCode\":\"SENT\",\"Message\":\"Sent.\",\"refno\":\"07012022120728728JzGiTN9442786718100031\"}]}]}"');
                
                Task taskRec= HDFC_DASH_TestDataFactory_Task.createBasicTask();
                taskRec.HDFC_DASH_Communication_Type__c = HDFC_DASH_Constants.STRING_SMS;
                Database.insert(taskRec);   
                String postProcessDataStr= '{"taskId":"'+taskRec.id+'","messageBody":"We have received Your home loan application & processing fee."}';
                Test.startTest();
                
                HDFC_DASH_SMSResponseHandler objSMSResponseHandler = new HDFC_DASH_SMSResponseHandler();
                objSMSResponseHandler.doRetryProcess(response, postProcessDataStr);
                Test.stopTest(); 
            }
            catch (Exception e) 
            { 
                testException=e;
            }
            system.assertEquals(null, testException);
            
        }
    }
    
    /* 
Method Name :testSmsErrorResponseHandler
Description :This method would test the method in Communication wrapper for response without responseBody.
*/
    static testmethod void testSmsErrorResponseHandler()
    {
        Exception testException;
        System.runAs(sysAdmin)
        {
            try{
                HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_SMS();  
                
                RestRequest request = new RestRequest();   
                HttpResponse response = new HttpResponse();
                response.setStatusCode(200);
                
                Task taskRec= HDFC_DASH_TestDataFactory_Task.createBasicTask();
                taskRec.HDFC_DASH_Communication_Type__c = HDFC_DASH_Constants.STRING_SMS;
                Database.insert(taskRec);   
                String postProcessDataStr= '{"taskId":"'+taskRec.id+'","messageBody":"We have received Your home loan application & processing fee."}';
                
                Test.startTest();
                HDFC_DASH_SMSResponseHandler objSMSResponseHandler = new HDFC_DASH_SMSResponseHandler();
                objSMSResponseHandler.doRetryProcess(response, postProcessDataStr);
                Test.stopTest(); 
            }
            catch (Exception e) 
            { 
                testException=e;
            }
            system.assertEquals(null, testException);
        }
    }
    
      /* 
Method Name :testFilterEventsForWhatsApp
Description :This method would test the method in trigger helper for the list of Events.
*/
    static testmethod void testFilterEventsForWhatsApp(){
        HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_WA();  
        
        Exception testException;
        Task taskRec = HDFC_DASH_TestDataFactory_Task.createBasicTask(app.id,con.id,sysAdminId);
        taskRec.HDFC_DASH_Is_Outbound_Communication__c =true;
        taskRec.HDFC_DASH_Communication_ID__c = WACOMM_TYPE;
        taskRec.HDFC_DASH_Related_Record__c = appDetails.id;
        taskRec.HDFC_DASH_Communication_Type__c = HDFC_DASH_Constants.STRING_WA;
        taskRec.HDFC_DASH_Target_Number__c = MOBILE_PHONE;
        
        System.runAs(sysAdmin){
            try{
                Test.startTest();
                Database.insert(taskRec);  
                Test.stopTest();
            }
            catch(Exception exp){
                testException = exp;
            }
            system.assertEquals(null, testException);
            
        }  
    }
      /* 
Method Name :testWhatsAppResponseHandler
Description :This method would test the method in Whatsapp communication for response.
*/
    static testmethod void testWhatsAppResponseHandler()
    {
        Exception testException;
        System.runAs(sysAdmin)
        {
            try{
                HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_WA();  
                RestRequest request = new RestRequest();   
                HttpResponse response = new HttpResponse();
                response.setStatusCode(200);
                response.setBody('{"status":"success","message":"Request received successfully.","data":{"id":"933e0cf9-1647-47e4-806d-5dc022fcfea8"}}');
                
                Task taskRec= HDFC_DASH_TestDataFactory_Task.createBasicTask();
                taskRec.HDFC_DASH_Communication_Type__c = HDFC_DASH_Constants.STRING_WA;
                Database.insert(taskRec);   
                String postProcessDataStr= '{"taskId":"'+taskRec.id+'","messageBody":"We have received Your home loan application & processing fee."}';
                Test.startTest();
                
                HDFC_DASH_WhatsAppResponseHandler objSMSResponseHandler = new HDFC_DASH_WhatsAppResponseHandler();
                objSMSResponseHandler.doRetryProcess(response, postProcessDataStr);
                Test.stopTest(); 
            }
            catch (Exception e) 
            { 
                testException=e;
            }
            system.assertEquals(null, testException);
            
        }
    }
    
     /* 
Method Name :testWhatsAppErrorResponseHandler
Description :This method would test the method in Whats API for response without responseBody.
*/
    static testmethod void testWhatsAppErrorResponseHandler()
    {
        Exception testException;
        System.runAs(sysAdmin)
        {
            try{
                HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_WA();  
                
                RestRequest request = new RestRequest();   
                HttpResponse response = new HttpResponse();
                response.setStatusCode(200);
                
                Task taskRec= HDFC_DASH_TestDataFactory_Task.createBasicTask();
                taskRec.HDFC_DASH_Communication_Type__c = HDFC_DASH_Constants.STRING_WA;
                Database.insert(taskRec);   
                String postProcessDataStr= '{"taskId":"'+taskRec.id+'","messageBody":"We have received Your home loan application & processing fee."}';
                
                Test.startTest();
                HDFC_DASH_WhatsAppResponseHandler objSMSResponseHandler = new HDFC_DASH_WhatsAppResponseHandler();
                objSMSResponseHandler.doRetryProcess(response, postProcessDataStr);
                Test.stopTest(); 
            }
            catch (Exception e) 
            { 
                testException=e;
            }
            system.assertEquals(null, testException);
        }
    }
    
}