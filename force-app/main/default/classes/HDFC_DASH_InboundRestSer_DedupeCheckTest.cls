/**
className: HDFC_DASH_InboundRestSer_DedupeCheckTest
DevelopedBy: Sai Shruthi
Date: 06 July 2021
Company: Accenture
Class Description: This class tests the functionality of HDFC_DASH_InboundRestService_DedupeCheck
**/
@isTest(SeeAllData = false)
private class HDFC_DASH_InboundRestSer_DedupeCheckTest {
    private static final string SUCCESS ='Success';
    private static final string EXCEPTION_OCCURED ='Exception Occured';
    private static final User sysAdmin = HDFC_DASH_TestDataFactory.createUser(HDFC_DASH_Constants.SYSTEM_ADMINISTRATOR);
    //request and response body for testing
    private static final string BASEURL = URL.getOrgDomainUrl().toExternalForm();
    private static final string REQUEST_BODY_WITH_PAN ='{"Lead":{"Salutation":"MR","FirstName":"FirstName","MiddleName":"","LastName":"LastName","GenderCode":"M","PAN":"LKOPS1234L","PanApplied":"No","PanStatusFromNSDL":"Status","CityCode":"HARAPANAHALLI","StateCode":"KA","CountryCode":"INDIA","CityName":"","StateName":"","CountryName":"","ResidentType":"RES","Email":"","DateOfBirth":"1998-08-24","MobileCountryCode":"91","Mobile":"8976565466","OutSystemsID":"100001","consent":{"voiceConsent":"Y","voiceConsentDate":"2000-10-30","SMSConsent":"Y","SMSConsentDate":"2016-10-30","WhatsAppConsent":"N","whatsAppConsentDate":null,"consentContent":"I/We allow HDFC to contact me towards this application"}},"validationOutcome":[{"user_Input_Type":"PAN","user_Input_Value":"ASD123DSedf","validated_With":"NSDL","validation_Response":{}}]}';
    private static final string REQUEST_BODY_WITHOUT_PAN_NAME_MATCH ='{"Lead":{"Salutation":"MR","FirstName":"F","MiddleName":"","LastName":"LastName","GenderCode":"M","PanApplied":"Yes","PanStatusFromNSDL":"Status","CityCode":"HARAPANAHALLI","StateCode":"KA","CountryCode":"INDIA","CityName":"","StateName":"","CountryName":"","ResidentType":"RES","Email":"abc123@gmail.com","DateOfBirth":"1998-08-24","MobileCountryCode":"91","Mobile":"3236777400","OutSystemsID":"100001","lmsLeadId":null,"consent":{"voiceConsent":"Y","voiceConsentDate":"2000-10-30","SMSConsent":"Y","SMSConsentDate":"2016-10-30","WhatsAppConsent":"N","whatsAppConsentDate":null,"consentContent":"I/We allow HDFC to contact me towards this application"}},"ValidationOutcome":[{"user_Input_Type":"PAN","user_Input_Value":"ASD123DSedf","validated_With":"NSDL","validation_Response":{}}]}';
    private static final string REQUEST_BODY_WITH_LMSLEADID = '{"Lead":{"Salutation":"MR","FirstName":"FirstName1","MiddleName":"","LastName":"LastName1","GenderCode":"M","PanApplied":"Yes","PanStatusFromNSDL":"Status","CityCode":"HARAPANAHALLI","StateCode":"KA","CountryCode":"INDIA","CityName":"","StateName":"","CountryName":"","ResidentType":"RES","Email":"abc123@gmail.com","DateOfBirth":"1999-10-24","MobileCountryCode":"91","Mobile":"3236767580","OutSystemsID":"100021","lmsLeadId":"101788909","consent":{"voiceConsent":"Y","voiceConsentDate":"2000-10-30","SMSConsent":"Y","SMSConsentDate":"2016-10-30","WhatsAppConsent":"N","whatsAppConsentDate":null,"consentContent":"I/We allow HDFC to contact me towards this application"}},"ValidationOutcome":[{"user_Input_Type":"PAN","user_Input_Value":"ASD123DSedf","validated_With":"NSDL","validation_Response":{}}]}';
    private static final string SUCCESS_RESPONSE_BODY ='{"sfCustomerNumber":"700000434","sfCustomerId":"0039D00000ENSFlQAP","sfApplicantID":"a0D9D000005MNawUAG","lmsDetails":{"lmsOriginBranchId":"90","lmsOriginBranch":"HDFC_ONLINE","lmsLeadType":"EXISTING","lmsLeadStatus":"NEW","lmsLeadID":"101801090"},"isRegisteredCustomer":false,"isCustomer":false,"eligibleForExistingCustJourney":false,"customerMobile":"34533224209","customerEmail":"gfgfgfg@gmail.com","application":{"sfApplicationNumber":"10000754","sfApplicationId":"0069D00000DD35xQAD"}}';
    private static final string STRING_POST ='POST';
    
    private static final string STRING_URL = BASEURL+'/services/apexrest/hdfc/dedupecheck/V1.0';
     
    private static final string PAN = 'LKOPS1234L';
    private static final string PHONENUMBER = '8976565466';
    private static final Date DATE_OF_BIRTH =Date.valueOf('1998-08-24');
    private static Interface_Framework_Settings__c ifSetting = new Interface_Framework_Settings__c();
    private static Interface_Settings__c interfaceSetting = new Interface_Settings__c();
    
    /* 
    Method Name :testProcessDedupeCheckForCheckForFlag
    Description :This method is for success dedupe senario when we found matching Contact and Is regitered flag is true on matching Contact.
    It would call the method HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request).
    */
    static testmethod void testProcessDedupeCheckForCheckForFlag(){
        System.runAs(sysAdmin){
            //HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            //ifSetting = HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_LMS();
            interfaceSetting = HDFC_DASH_TestDataFactory_API.createInterfaceSetting_LMS();
            Contact databaseConRec = new Contact();
            databaseConRec.FirstName ='FirstName'; 
            databaseConRec.LastName = 'LastName';   
            databaseConRec.MobilePhone= PHONENUMBER;
            databaseConRec.HDFC_DASH_PAN__c = PAN;
            databaseConRec.HDFC_DASH_Date_Of_Birth__c =DATE_OF_BIRTH;
            databaseConRec.HDFC_DASH_Is_Registered_Customer__c = HDFC_DASH_Constants.BOOLEAN_TRUE;
            insert databaseConRec;
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockLMSResponseGenerator());
            RestRequest request = new RestRequest();            
            request.requestUri = STRING_URL;
            request.httpMethod = STRING_POST;
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITH_PAN);
            RestContext.request = request;
            RestResponse response = new RestResponse();
            RestContext.response = response;
                
            test.startTest();
            HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request);
            Test.stopTest();
            system.assertEquals(1,[select count() from contact LIMIT 10] ,SUCCESS);
            
        }
    }
    /* 
    Method Name :testProcessDedupeCheckForMatchingContact
    Description :This method is for success dedupe senario when we found matching Contact.
    It would call the method HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request).
    */
    static testmethod void testProcessDedupeCheckForMatchingContact(){
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
           //ifSetting = HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_LMS();
            interfaceSetting = HDFC_DASH_TestDataFactory_API.createInterfaceSetting_LMS();
           
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockLMSResponseGenerator());
            RestRequest request = new RestRequest();            
            request.requestUri = STRING_URL;
            request.httpMethod = STRING_POST;
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITH_PAN);
            RestContext.request = request;
            RestResponse response = new RestResponse();
            RestContext.response = response;
                
            test.startTest();
            HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request);
            Test.stopTest();
            system.assertEquals(1,[select count() from contact LIMIT 10] ,SUCCESS);
            
        }
    }
     /* 
    Method Name :testProcessDedupeCheckNotMatchingContact
    Description :This method is for success dedupe senario when we do not found matching Contact and when lmsLeadId is null.
    It would call the method HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request).
    */
    static testmethod void testProcessDedupeCheckNotMatchingContact(){ 
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            //ifSetting = HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_LMS();
            interfaceSetting = HDFC_DASH_TestDataFactory_API.createInterfaceSetting_LMS();
           
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockLMSResponseGenerator());
            RestRequest request = new RestRequest();            
            request.requestUri = STRING_URL;
            request.httpMethod = STRING_POST;
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITHOUT_PAN_NAME_MATCH);
            RestContext.request = request;
            RestResponse response = new RestResponse();
            RestContext.response = response;
                
            test.startTest();
            HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request);
            Test.stopTest();
            system.assertEquals(1,[select count() from contact LIMIT 10] ,SUCCESS);
            
        }
    }
    /* 
    Method Name :testProcessDedupeCheckwithLmsLeadId
    Description :This method is for success dedupe senario when we do not found matching Contact 
    and when lmsLeadId is not null and also when request lms lead id and lms response lms lead id are equal.
    It would call the method HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request).
    */
    static testmethod void testProcessDedupeCheckwithLmsLeadId(){
        System.runAs(sysAdmin){
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            //ifSetting = HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_LMS();
            interfaceSetting = HDFC_DASH_TestDataFactory_API.createInterfaceSetting_LMS();     
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockLMSResponseGenerator());
            RestRequest request = new RestRequest();            
            request.requestUri = STRING_URL;
            request.httpMethod = STRING_POST;
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITH_LMSLEADID);
            RestContext.request = request;
            RestResponse response = new RestResponse();
            RestContext.response = response;      
            
            test.startTest();
            HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request);
            Test.stopTest();
            system.assertEquals(1,[select count() from Lead LIMIT 10] ,SUCCESS);
            system.assertEquals(1,[select count() from contact LIMIT 10] ,SUCCESS);
            
        }
    }
    /* 
    Method Name :testRunSalesforceDedupe
    Description :This method is for success scenario for runSalesforceDedupe method .
    It would call the method HDFC_DASH_InboundRestService_DedupeCheck.runSalesforceDedupe(leadDetails).
    */
    static testmethod void testRunSalesforceDedupe(){
        System.runAs(sysAdmin){
            RestRequest request = new RestRequest();       
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITH_PAN);
            HDFC_DASH_ParseleadDetails leadDetails = HDFC_DASH_ParseleadDetails.parse(request);	
            Contact databaseConRec = new Contact();
            databaseConRec.FirstName ='FirstName'; 
            databaseConRec.LastName = 'LastName';   
            databaseConRec.MobilePhone= PHONENUMBER;
            databaseConRec.HDFC_DASH_PAN__c = PAN;
            databaseConRec.HDFC_DASH_Date_Of_Birth__c =DATE_OF_BIRTH;
            databaseConRec.HDFC_DASH_Is_Registered_Customer__c = HDFC_DASH_Constants.BOOLEAN_TRUE;
            insert databaseConRec;
            Test.startTest();
            Contact result = HDFC_DASH_InboundRestService_DedupeCheck.runSalesforceDedupe(leadDetails);        
            Test.stopTest();
            system.assertNotEquals(null, result);
          
        }       
    }
    
    /* 
    Method Name :testRunSalesforceDedupe
    Description :This method is for success scenario for runSalesforceDedupe method .
    It would call the method HDFC_DASH_InboundRestService_DedupeCheck.runSalesforceDedupe(leadDetails).
    */
    static testmethod void testRunSalesforceDedupeWithOnlyCustDedupeFlag(){
        System.runAs(sysAdmin){
            //system.debug('inside testRunSalesforceDedupeWithOnlyCustDedupeFlag');
            HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_OS();
            //ifSetting = HDFC_DASH_TestDataFactory_API.createCustomSettingForHeaders_LMS();
            interfaceSetting = HDFC_DASH_TestDataFactory_API.createInterfaceSetting_LMS();     
            Test.setMock(HttpCalloutMock.class, new HDFC_DASH_MockLMSResponseGenerator());
            RestRequest request = new RestRequest();            
            request.requestUri = STRING_URL;
            request.addParameter(HDFC_DASH_Constants.STRING_ONLY_CUSTOMER_DEDUPE,'true');
            //system.debug('Request URL--'+request.requestUri);
            //system.debug('REquest Param'+request.params.get('OnlyCustomerDedupe'));
            request.httpMethod = STRING_POST;
            request = HDFC_DASH_TestDataFactory_API.assignHeadersForRequest(request);
            request.requestBody = Blob.valueOf(REQUEST_BODY_WITH_LMSLEADID); 
            RestContext.request = request;
            RestResponse response = new RestResponse();
            RestContext.response = response;      
            
            test.startTest(); 
            HDFC_DASH_InboundRestService_DedupeCheck.processDedupeCheck(request); 
            Test.stopTest();
            system.assertEquals(1,[select count() from contact LIMIT 10] ,SUCCESS);   
            
        }
    }
}